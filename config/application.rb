require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Fiveo
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    
    # removed by rails upgrade, may need to uncomment these.  mfm
    config.autoload_paths += %W(#{config.root}/extentions #{config.root}/lib)
    config.autoload_once_paths += %W(#{config.root}/extentions)
    

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    #
    # Changing this to check for env setting value so agencies can change their time
    #   zone without having to change code. If env is not set, will default to UTC.
    unless ENV['RAILS_TIME_ZONE'].blank?
      config.time_zone = ENV['RAILS_TIME_ZONE']
    end

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true
  end
end
