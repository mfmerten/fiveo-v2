Fiveo::Application.routes.draw do
  ####################################
  # Main Menu Routes
  ####################################
  resources :contacts do
    get :merge, on: :member
    collection do
      get :employee_list
      get :contact_list
    end
  end
  scope "forums" do
    resources :forum_topics, except: :index, path: :topics do
      member do
        get :subscribe
        get :unsubscribe
        put :toggle_hide
        put :toggle_lock
        put :toggle_pin
        post :move
      end
      resources :forum_posts, path: :posts
    end
    resources :forum_categories, except: :index, path: :categories
  end
  resources :forums do
    resources :forum_topics, except: :index, path: :topics do
      member do
        get :subscribe
        get :unsubscribe
        put :toggle_hide
        put :toggle_lock
        put :toggle_pin
      end
      resources :forum_posts, path: :posts
    end
  end
  resources :people do
    member do
      get :rap_sheet
      get :links
      post :verify_warrant
      post :warrant_exemption
      post :verify_forbid
      post :forbid_exemption
      match :merge, as: :merge, via: [:get, :post]
      match :front_mugshot, as: :front_mugshot, via: [:get, :post]
      match :side_mugshot, as: :side_mugshot, via: [:get, :post]
    end
    collection do
      get :photo_album
      post :choose
      get :pick_list
    end
    resources :medications
  end
  
  ####################################
  # Court Menu Routes
  ####################################
  resources :dispositions, except: [:new, :create, :destroy] do
    collection do
      get :status_update
      get :status_report
      post :forms
    end
  end
  resources :bonds do
    member do
      post :undo_recall_bond
      post :recall_bond
      post :finalize_bond
      post :unfinalize_bond
      get :bond_form
      get :add_to_queue
      get :remove_from_queue
    end
    collection do
      post :update_companies
      get :active_list
      get :list_form
      get :list_unfinalized
      get :unfinalized_pdf
    end
  end
  resources :court_costs
  resources :court_types
  resources :docket_types
  resources :dockets do
    get :link_charges, on: :member
    collection do
      get :link
      get :letters
      get :dockets
      post :do_link
      get :list_unprocessed
      get :clear_letter_queue
      post :validate
      post :validate_da_charge
    end
    resources :courts, except: :index do
      get :worksheets, on: :member
      resources :sentences, except: :index do
        post :force_processing, on: :member
      end
    end
    resources :revocations, except: :index do
      post :force_processing, on: :member
    end
  end
  resources :garnishments
  scope 'papers' do
    resources :paper_customers, path: 'customers' do
      post :toggle_customer, on: :member
      post :update_fees, on: :collection
      resources :paper_payments, path: 'payments', except: :index
    end
    resources :paper_types, path: 'types'
  end
  resources :papers do
    member do
      post :uninvoice
      get :info_sheet
      match :payment, as: :payment, via: [:get, :post]
      match :invoice, as: :invoice, via: [:get, :post]
    end
    collection do
      get :billing_status
      get :service_status
      get :history
      post :autofill_serve_to
      post :clear_prior
      post :autofill_suit_info
      post :autofill_customer_info
      post :autofill_paper_info
      post :autofill_served_to
      match :invoices, action: :invoice, as: :invoices, via: [:get, :post]
      match :posting_report, as: :posting_report, via: [:get, :post]
    end
  end
  resources :probations do
    collection do
      get :probation_balance
      get :show_probationer
      get :transaction_history
      get :active_list
      post :disable_sentence_info
      match :posting_report, as: :posting_report, via: [:get, :post]
    end
    resources :probation_payments, path: 'payments', except: :index
  end
  
  ####################################
  # Detective Menu Routes
  ####################################
  scope 'investigations' do
    resources :invest_crimes, path: 'crimes', only: :index
    resources :invest_criminals, path: 'criminals', only: :index
    resources :invest_reports, path: 'reports', only: :index
    resources :invest_vehicles, path: 'vehicles', only: :index
    resources :invest_photos, path: 'photos', only: :index
  end
  resources :investigations do
    match :upload_photos, as: :upload_photos, on: :member, via: [:get, :post]
    resources :invest_notes, path: 'notes', except: [:index, :show]
    resources :invest_crimes, path: 'crimes', except: :index do
      resources :invest_notes, path: 'notes', except: [:index, :show]
    end
    resources :invest_criminals, path: 'criminals', except: :index do
      post :update_person, on: :collection
      resources :invest_notes, path: 'notes', except: [:index, :show]
    end
    resources :invest_reports, path: 'reports', except: :index do
      get :print_report, on: :member
      resources :invest_notes, path: 'notes', except: [:index, :show]
    end
    resources :invest_vehicles, path: 'vehicles', except: :index do
      resources :invest_notes, path: 'notes', except: [:index, :show]
    end
    resources :invest_photos, path: 'photos', except: :index do
      match :photo, action: :investigation_photo, as: :photo, on: :member, via: [:get, :post]
      resources :invest_notes, path: 'notes', except: [:index, :show]
    end
  end
  
  ####################################
  # Dispatch Menu Routes
  ####################################
  resources :calls do
    post :new_case_no, on: :member
    get :call_sheet, on: :member
    get :shift_report, on: :collection
  end
  resources :forbids do
    get :forbid_form, on: :member
    post :update_person, on: :collection
  end
  resources :histories, only: [:index, :show, :destroy]
  scope 'pawns' do
    resources :pawn_item_types, path: 'item_types'
  end
  resources :pawns
  resources :signal_codes do
    collection do
      get :signals
      get :validate
    end
  end
  resources :warrants do
    match :photo, as: :photo, on: :member, via: [:get, :post]
    collection do
      get :list_form
      get :photo_album
      post :update_person
    end
  end
  
  ####################################
  # Enforcement Menu Routes
  ####################################
  resources :case_notes do
    get :show_case, on: :collection
  end
  resources :citations
  resources :damages
  resources :evidences do
    member do
      match :photo, as: :photo, via: [:get, :post]
      get :change_location
    end
    get :inventory, on: :collection
  end
  resources :offenses do
    member do
      match :upload_photos, as: :upload_photos, via: [:get, :post]
      get :offense_report
    end
    resources :offense_photos, path: 'photos', except: :index do
      match :photo, as: :photo, on: :member, via: [:get, :post]
    end
  end
  resources :patrols do
    collection do
      get :patrol_summary
      get :patrol_activity
      post :autofill_shift_info
      post :autofill_officer_info
    end
  end
  resources :stolens
  resources :transports do
    get :voucher, on: :member
  end
  
  
  ####################################
  # Jail Menu Routes
  ####################################
  resources :arrests do
    member do
      get :print_72
      get :print_48
      get :id_card
      post :rebook
      match :seventytwo, as: :seventytwo, via: [:get, :post]
    end
    get :arrest_summary, on: :collection
  end
  resources :docs, except: [:new, :create]
  resources :jails do
    resources :bookings do
      member do
        match :temp_release_out, as: :temp_release_out, via: [:get, :post]
        post :temp_release_in
        get :arraignment_notice
        get :time_log
        post :toggle_goodtime
        post :clear_release
        get :jail_card
        match :release, as: :release, via: [:get, :post]
      end
      collection do
        get :photo_album
        get :problems
        get :problem_list
        get :list_form
        get :release_report
        get :in_jail
        get :ssa_report
        get :fcs_report
        get :billing_headcount
        get :month_billing
      end
      resources :holds, except: :index do
        member do
          get :clear
          put :clear_update
          post :toggle_bill
          post :clear_release
          get :adjustment
          put :adjustment_update
        end
      end
      resources :medicals, except: :index do
        get :medical_form, on: :member
      end
      resources :transfers, except: :index do
        post :update_transfer, on: :collection
      end
      resources :visitors, except: :index
    end
    resources :absents do
      member do
        get :checklist
        post :leave
        post :come_back
        post :copy
      end
    end
    resources :commissaries do
      collection do
        get :fund_balance
        match :posting_report, action: :posting_report, via: [:get, :post]
        get :sales_sheet
        get :transaction_report
      end
    end
    resources :commissary_items do
      get :sales_sheet, on: :collection
    end
    resources :holds, only: :index
    resources :transfers, only: :index
    resources :visitors, only: :index do
      member do 
        post :sign_out
        post :reset
      end
    end
  end
  
  ####################################
  # User Menu Routes
  ####################################
  # User Messages
  # Legacy Holdover from RestfulEasyMessages plugin
  controller :messages do
    scope 'user/:user_id' do
      scope 'message' do
        match ':id/print', action: :print_message, as: :print_message, via: :post
        match 'print_digest', action: :print_digest, as: :print_digest, via: :post
        match 'destroy_selected', action: :destroy_selected, as: :destroy_selected, via: :post
        match 'select_messages', action: :select_messages, as: :select_messages, via: :post
        match 'inbox', action: :inbox, as: :inbox_user_messages, via: :get
        match 'outbox', action: :outbox, as: :outbox_user_messages, via: :get
        match 'trashbin', action: :trashbin, as: :trashbin_user_messages, via: :get
        match 'new', action: :new, as: :new_user_message, via: :get
        match ':id/edit', action: :edit, as: :edit_user_message, via: :get
        match ':id/reply', action: :reply, as: :reply_user_message, via: :get
        match ':id', action: :show, as: :user_message, via: :get
        match ':id', action: :update, via: :put
        match ':id', action: :destroy, via: :delete
      end
      match 'messages', action: :index, as: :user_messages, via: :get
      match 'messages', action: :create, via: :post
    end
  end
  controller :users do
    scope 'my' do
      match :photo, as: :my_photo, via: [:get, :post]
      match :preferences, action: :edit_settings, as: :my_preferences, via: [:get, :post]
    end
    match :login, as: :login, via: [:get, :post]
    match :logout, as: :logout, via: [:get, :post]
    match :change_password, as: :change_password, via: [:get, :post]
  end
  
  ####################################
  # Utilities Menu Routes
  ####################################
  resources :announcements do
    get :toggle, on: :member
    get :all, on: :collection
  end
  resources :events
  resources :external_links do
    get :all, on: :collection
  end
  resources :fax_covers do
    get :fax_cover, on: :member
  end
  resources :statutes do
    get :validate_charge, on: :collection
  end
  
  ####################################
  # Admin Menu Routes
  ####################################
  resources :activities, only: [:index]
  controller :database do
    scope 'database' do
      post :clear_cases
      post :clear_sessions
    end
    get :databases, action: :index, as: :databases
  end
  resources :groups
  # Jails resources - see Jail Menu Routes
  resources :message_logs, only: [:index, :show]
  controller :site_configurations do
    scope 'config' do
      get :main, action: :site_main, as: :site_main
      match 'main/edit', action: :edit_site_main, as: :edit_site_main, via: [:get, :post]
      get :admin, action: :site_admin, as: :site_admin
      match 'admin/edit', action: :edit_site_admin, as: :edit_site_admin, via: [:get, :post]
      get :jail, action: :site_jail, as: :site_jail
      match 'jail/edit', action: :edit_site_jail, as: :edit_site_jail, via: [:get, :post]
      get :court, action: :site_court, as: :site_court
      match 'court/edit', action: :edit_site_court, as: :edit_site_court, via: [:get, :post]
      get :dispatch, action: :site_dispatch, as: :site_dispatch
      match 'dispatch/edit', action: :edit_site_dispatch, as: :edit_site_dispatch, via: [:get, :post]
      get :enforcement, action: :site_enforcement, as: :site_enforcement
      match 'enforcement/edit', action: :edit_site_enforcement, as: :edit_site_enforcement, via: [:get, :post]
      get :detective, action: :site_detective, as: :site_detective
      match 'detective/edit', action: :edit_site_detective, as: :edit_site_detective, via: [:get, :post]
      post :upload_logo
      post :upload_ceo_img
      post :upload_header_left_img
      post :upload_header_right_img
    end
  end
  resources :users do
    member do
      post :expire
      post :toggle
      post :reset
      match :photo, as: :photo, via: [:get, :post]
    end
    get :photo_album, on: :collection
  end
  
  controller :root do
    get :tools
    post :h_to_hdmy
    get :get_calendar
    get ':action', action: :index
    get '/', action: :index, as: :root
  end
end
