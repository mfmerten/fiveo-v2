# set up Audit log - should log all instances where the program modifies
# data. This is different from and much more detailed than the user activity logs.
# should be used like:
# Audit.add(Logger.INFO, message, method_name)
# i.e. Audit.add(Logger.INFO, "Updated Probation XXX (UID XXX)", "Sentence.process")
# or it can be called like:
# Audit.info('Sentence.process'){'Updated Probation XXX (UID XXX)'}
class AuditLogFormatter < Logger::Formatter
  def call(severity, time, progname, msg)
    if !progname.is_a?(String) || progname.blank?
      progname = "Unknown"
    end
    "[%s] %4s -> %s: %s\n" % [time.to_s(:us), severity, progname, msg2str(msg)]
  end
end
::Audit = Logger.new(Rails.root.join("log","audit.log"))
::Audit.formatter = AuditLogFormatter.new

# include our custom validations into ActiveRecord
ActiveRecord::Base.class_eval do
   include ApplicationValidations
end

# try to load site configuration
::SiteConfig = Site.try(:new)
SiteConfig.try(:reload)

# try to load site configuration images
::SiteImg = SiteImage.try(:last)
