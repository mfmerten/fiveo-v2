# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

# colorschemes
Rails.application.config.assets.precompile += %w(
   colorscheme_blue_brown.css
   colorscheme_blue_gray.css
   colorscheme_brown_blue.css
   colorscheme_gray_green.css
   colorscheme_green_brown.css
   colorscheme_green_gray.css
   colorscheme_yellow_gray.css
   opentip-jquery.js
   jquery.mask.js
   site.js
   forum.css
   opentip.css
   site.css
)