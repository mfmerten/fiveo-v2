##### Application Cron Jobs

env :PATH, '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin'
set :output, 'log/rake.log'
job_type :rake, "cd :path && RAILS_ENV=:environment bundle exec rake :task --silent :output"

# run the hourly jobs
every :hour do
   rake "app:update_hourly"
end

# run the daily jobs
every 1.days, at: '3:15am' do
  rake "app:update_daily"
end

# run the weekly jobs
every :sunday, at: '3:45am' do
  rake "app:update_weekly"
end

# run the semi-monthly jobs
every '15 4 1,15 * *' do
  rake "app:update_semi_monthly"
end

# run the monthly jobs
every '45 4 1 * *' do
  rake "app:update_monthly"
end
