<!DOCTYPE html> 
<head>
  <title>FiveO Reference Manual</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link href="manual.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class='title'>
  <img src='fiveo.png' style='width:85px;height:85px;float:left;'>
  <img src='fiveo.png' style='width:85px;height:85px;float:right;'>
  FiveO&#8482; Reference Manual</div>
<h1>Introduction</h1>
<blockquote class="alert">
<p class="alert">Editor Note: The <cite>Fiveo&#8482; Referernce Manual</cite> is in a state of heavy development. Many of the manual sections still reference Version 1 of the software. These sections are indicated in the <a href="toc.html"><em>Index</em></a> with <img src="update.png" alt="" />. Many more of the sections in the manual have not yet been written and are indicated in the <a href="toc.html"><em>Index</em></a> with <img src="missing.png" alt="" />. The manual sections that have been completed and are up-to-date are indicated in the <a href="toc.html"><em>Index</em></a> with <img src="ok.png" alt="" />.</p>
</blockquote>
<p>This is the <cite>FiveO&#8482; Reference Manual</cite>. It is valid for the FiveO&#8482; software version with which it is included. It is not intended to be redistributed without the FiveO&#8482; software.</p>
<p>The manual devotes one section to each major function of the FiveO&#8482; software. The sections are presented in (mostly) alphabetical order.</p>
<h2><a id='copyright'>Copyright</a></h2>
<p>Copyright &#169; 2013 Michael Merten. Permission is granted to copy, distribute and/or modify this document under the terms of the <cite><span class="caps">GNU</span> Free Documentation License</cite>, Version 1.3 or any later version published by the <a href="http://www.fsf.org">Free Software Foundation</a> with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.</p>
<h3><a id='gnu'><span class="caps">GNU</span> Free Document License</a></h3>
<p>A copy of the license is included in the section entitled <a href="fdl-1.3-standalone.html"><span class="caps">GNU</span> Free Documentation License</a>.</p>
<h2><a id='conventions'>Conventions</a></h2>
<p>The following conventions are used in this manual:</p>
<ul>
	<li>The word &#8220;FiveO&#8221; refers to the FiveO&#8482; Law Enforcement Software.</li>
	<li>The words &#8220;FiveO Server&#8221; and &#8220;Server&#8221; refer to the specific computer at your agency that is running the FiveO software.</li>
	<li><em>Emphasis</em> is used to signify a special meaning (in FiveO context) to a word or words. For instance, <em>Person</em> refers to a specific entry in the <em>People</em> database. Button, database and record names will be shown in <em>emphasized</em> text.</li>
	<li>Names shown in singular form, like <em>Person,</em> refers to a single database entry or record, and in plural form, like <em>People,</em> refers to the database as a whole. This is also used to distinguish between the main list (&#8220;People List&#8221;) and the individual record page (&#8220;Person Page&#8221;) which shows information about a single record.</li>
	<li>Mono-spaced words <code>like this</code> are used when indicating text you are to type, and for blocks of code.  Keys on the keyboard will be shown as bracketed mono-spaced text like <code>[Enter]</code>.</li>
	<li>Clickable links to other web pages, or other parts of this manual will be shown in blue underlined text, like <a href="index.html">Index</a>. Generally, wherever a FiveO feature is mentioned, it will be a link to the manual section for that feature.</li>
	<li>Important information, such as notes about known broken features and warnings about dangerous actions, will be shown in red text.</li>
	<li>Double quotes are used to indicate verbatim words or phrases. Names of pages in the application or sections in this manual will be enclosed in double quotes, like <a href="person.html#page">&quot;Person Page&quot;</a>.</li>
	<li>Angle brackets are used to indicate substitution. When you see information inside angle brackets (like &lt;your server <span class="caps">URL</span>&gt;), you should mentally substitute the indicated information into the sentence when you read it. For instance, a phrase like &#8220;the manual can be accessed at https://&lt;your server IP address&gt;/manual.html&#8221; should be read &#8220;the manual can be accessed at https://192.168.1.5/manual.html&#8221; if your server IP address is 192.168.1.5.</li>
</ul>
<h3><a id='database'>Database Terminology</a></h3>
<p>All of the information entered into FiveO is stored in a database application<sup class="footnote" id="fnr1"><a href="#fn1">1</a></sup>. We use a lot of database terms in this manual that may not be familiar to all of our readers.</p>
<p>In a nutshell:</p>
<dl>
	<dt>Database or Table</dt>
	<dd>In technical terms, &#8220;table&#8221; refers to one specific set of data, while &#8220;database&#8221; more generally refers to a group of &#8220;tables&#8221; that together provide the information about something. &quot;<a href="person.html">People</a> database&quot; for instance would technically includes the <a href="person.html"><em>People</em></a> table, the <em>Associates</em> table and the <em>Marks</em> table all linked together to store information about a person. However, in this manual we generally use the terms interchangeably, preferring “database” (as in &quot;<a href="person.html"><em>People</em></a> database&quot;) but sometimes slip in the occasional “table”. For the purposes of this manual, consider “database” and “table” to mean the same thing.</dd>
	<dt>Record or Row</dt>
	<dd>Each table has one or more rows (or records). Each row (or record) contains the information about one specific entry in the table. For the <a href="person.html"><em>People</em></a> table, each row (or record) defines one person. We generally use “record” but occasionally may use “row” instead. Consider these the same thing.</dd>
	<dt>Field or Column</dt>
	<dd>Each row (or record) in a table has one or more individual pieces of information called &#8220;columns&#8221; that taken together define one unit of information. For a <a href="person.html"><em>Person</em></a> record, the columns might be &#8220;Name&#8221;, &#8220;Age&#8221;, &#8220;Phone Number&#8221;, &#8220;Address&#8221;, etc. On a data entry form, these individual pieces of information are often called &#8220;fields&#8221;. There is usually a field on the form for each column in the record<sup class="footnote" id="fnr2"><a href="#fn2">2</a></sup>. Whether we use &#8220;column&#8221; or &#8220;field&#8221;, we mean one discrete portion of a table row/record.</dd>
</dl>
<h2><a id='disclaimer'>Disclaimer</a></h2>
<p>The authors of this <cite>FiveO&#8482; Reference Manual</cite> wish to make it clear that the functions and tasks defined in this document describe the operation and use of the FiveO software in the way that it was designed.</p>
<p>We do not specify or define the actual job duties for the employees at any agency. That is the responsibility of their employers.</p>
<hr />
<p style="vertical-align:middle;" class="footnote" id="fn1"><a href="#fnr1"><sup>1</sup></a> <a href="http://www.postgresql.org">PostgreSQL</a> is the only database currently supported. It is a free and open-source database application with all of the features necessary for FiveO.</p>
<p class="footnote" id="fn2"><a href="#fnr2"><sup>2</sup></a> In the &#8220;old days&#8221; there was no real separation between table rows/columns and form records/fields. When you dealt with data in a database you were working with the actual data in the database itself. Now, everything is virtualized to the point where you never actually touch data directly, but instead give instructions to the database program using a &#8220;query&#8221; language and it does everything itself. Old habits die hard though, hence our tendency to think of rows/columns and records/fields as being the same thing.</p>
<div class='nav'><a href="toc.html">Index</a><span class='spacer'>&loz;</span>The Beginning<span class='spacer'>&larr;</span><span class='current'>Introduction</span><span class='spacer'>&rarr;</span><a href='common.html'>Common Features</a></div>
