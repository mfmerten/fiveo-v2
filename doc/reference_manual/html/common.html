<!DOCTYPE html> 
<head>
  <title>FiveO Reference Manual</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link href="manual.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class='title'>
  <img src='fiveo.png' style='width:85px;height:85px;float:left;'>
  <img src='fiveo.png' style='width:85px;height:85px;float:right;'>
  FiveO&#8482; Reference Manual</div>
<h1>Common Features</h1>
<p>The following paragraphs describe features of FiveO that are global in nature (they work the same for all sections). They are included here for reference by the other sections in this manual.</p>
<h2><a id='search'>Searching for Information</a></h2>
<p>Searching for information in FiveO can be accomplished in several ways.</p>
<h3><a id='full-text'>Full Text Search</a></h3>
<p>If you wish to locate records that contains a certain words regardless of where they appear in the record, you can use the &#8220;full text&#8221; search feature. This is done by entering &#8220;search terms&#8221; using one of the &#8220;simple search&#8221; features.</p>
<blockquote>
<p>Information entered into FiveO is not immediately available for “full text” searching. The program builds a “full text” index only once each 30 minutes, so it may take up to 30 minutes for you to be able to find something using the “search” feature (this includes the global search box in the page <em>header</em> and the simple search box found on each “list” page). Filters, on the other hand, generate “queries” against the live database information and will always work regardless of how old (or new) the information is.</p>
</blockquote>
<h4>Global Search</h4>
<p>If you wish to search the entire FiveO database for matching records, no matter the record type, use the global search box located in the page <em>header</em> section. Type your search word(s) in the text box and click the Search button. All matching results will be displayed on the &#8220;Global Search&#8221; page.</p>
<h4>Record Specific</h4>
<p>If you wish to search only one type of record (<a href="person.html"><em>Person</em></a> for example), navigate to the &#8220;&lt;feature&gt;s List&#8221; or &#8220;&lt;feature&gt; Page&#8221; page for that record type and use the search box that appears below the <em>primary button bar</em> (if one is available for that feature). This search box works just like the global search box, except that matching results are limited to the specific feature.</p>
<h3><a id='filter'>Filters</a></h3>
<p>Most features in FiveO have an &#8220;advanced&#8221; search feature (called a &#8220;Filter&#8221;). These &#8220;Filters&#8221; are record specific (meaning there is no &#8220;global filter&#8221;). To access the &#8220;Filter&#8221; for a particular feature, navigate to the &#8220;&lt;feature&gt;s List&#8221; or &#8220;&lt;feature&gt; Page&#8221; page for that feature. Look at the &#8220;search&#8221; box below the <em>primary button bar</em> and find the <em>Show Filter Form</em> link. Clicking this link will display the advanced search &#8220;Filter&#8221; form.</p>
<blockquote>
<p>If you navigate to a feature page and cannot find a &#8220;search&#8221; box, then the feature does not have search capability at all. Some features do not have a &#8220;Search&#8221; box, but show the advanced &#8220;Filter&#8221; form by default. Some features have a &#8220;Search&#8221; box, but have no <em>Show Filter Form</em> link&#8230; these features do not have advanced search &#8220;Filters&#8221;.</p>
</blockquote>
<p>The advanced search &#8220;Filter&#8221; is different from the &#8220;Search&#8221; boxes in that it does not use the &#8220;full text&#8221; search indexes. Instead, the form shows each of the searchable fields for the feature. You must enter your search terms in the appropriate fields on this form. All of the information you enter on this form must exist in a record for the record to be considered a match. In other words, the &#8220;query&#8221; created by the &#8220;Filter&#8221; form uses &#8220;<span class="caps">AND</span>&#8221; instead of &#8220;OR&#8221; when matching records.</p>
<p>Although it may sound counter-intuitive, the more information you enter into a search &#8220;Filter&#8221;, the <strong>less</strong> likely you are to find what you are searching for. You should begin to understand this when you consider that records must match exactly with all of the information you provide. Take street addresses for example&#8230; if you want to find all <a href="person.html"><em>People</em></a> who live on “Main Street”, it is much better to enter “main” (capitalization is ignored on &#8220;Filters&#8221;) in the &#8220;Street:&#8221; search field instead of “main street” because the word “street” might have been entered as “st.” on some records, “street” on others or may have been left off altogether by the <a href="user.html"><em>User</em></a> that originally entered the information. The same is true for names. &#8220;Joe Smith&#8221; might at first seem to be just the thing to enter into the <a href="person.html"><em>Person</em></a> &#8220;Filter&#8221; &#8220;Name:&#8221; field if you are looking for somebody called &#8220;Joe Smith&#8221;.  However, it will not match the correct <a href="person.html"><em>Person</em></a> if they were entered into the database as &#8220;Joseph Smith&#8221;, or if &#8220;Joe&#8221; is actually a &#8220;middle&#8221; name and the <a href="person.html"><em>Person</em></a> was entered something like &#8220;David J. Smith&#8221;. Best practice when searching by names is to search last-name-only to get the most matches, then take the time to look through the results to find the actual record you are looking for.</p>
<h2><a id='standard_text'>Standard Text</a></h2>
<p>Most multi-line text blocks use &#8220;standard text formatting&#8221; rules. This basically means you are limited to paragraphs and simple manual lists for formatting your text.</p>
<p>To break your information into paragraphs, you should type each paragraph without using the <code>[Enter]</code> key to break lines. As you near the edge of the text box, just keep typing and the line will automatically wrap itself as needed. At the end of each paragraph, hit the <code>[Enter]</code> key twice (ends the previous paragraph and adds a blank line before starting the next paragraph).</p>
<p>You should only use the <code>[Enter]</code> key when you absolutely need to end a line at a specific point and start the next text on the next line. The space provided for you to write the report is not the same as that on the generated <span class="caps">PDF</span>. As long as you remember to type your paragraphs without using <code>[Enter]</code> to artificially break the lines, the paragraphs will be correctly reformatted by the <span class="caps">PDF</span> generation process to fit the space provided for them.</p>
<p>Simple lists can be created using the asterisk (*) for &#8220;bullet&#8221; lists and manually entering numbers (1,2,3, etc) for &#8220;itemized&#8221; lists.  You should start your list after a blank line (hit <code>[Enter]</code> twice to end the previous paragraph and provide the recommended blank line). Hit <code>[Enter]</code> after typing each item in the list in its entirety (let the program itself wrap long list items into multiple lines). Use the <code>[Enter]</code> key twice on the last list item to provide a blank line between the list and the next paragraph.</p>
<p>Do not rely on multiple blank spaces to format text. The program will remove these extra spaces when it displays the results on the page.</p>
<p>You cannot use <code>[Tab]</code> to indent paragraphs or other information.  The <code>[Tab]</code> key is used by the form to move to the next input field.</p>
<h2><a id='textile'>Textile Fields</a></h2>
<p>The word “Textile” in FiveO context has nothing to do with the clothing industry&#8230; it instead refers to the <a href="http://textile.thresholdstate.com">Textile Markup Language</a>, which is used to simplify text formatting on web pages (this manual is written using Textile). Many of the forms in the software have one or more text boxes that will accept Textile formatting commands (these are shown with a pale green background). Any valid Textile commands you enter in these special fields will be processed by the software before displaying the information on the screen. This allows you to have headings, bold and italic text, bullet and numbered lists and more.</p>
<p>All references to &#8220;Textile&#8221;, outside this section, will be linked to this section for your convenience.</p>
<p>The following table shows some useful Textile commands:</p>
<table class='normal'>
<tr>
    <th style='width:49%;'>You Type</th>
    <th>Displayed on the Page</th>
</tr>
<tr>
    <td>p. This is a paragraph<br/>p. This is another paragraph</td>
    <td>This is a paragraph<br/><br/>This is another paragraph</td>
</tr>
<tr>
    <td>This is *bold* text</td>
    <td>This is <strong>bold</strong> text</td>
</tr>
<tr>
    <td>This is _emphasized_ text</td>
    <td>This is <em>emphasized</em> text</td>
</tr>
<tr>
    <td>* this is item 1<br/>* this is item 2<br/>* this is item 3</td>
<td>
<ul>
        <li>this is item 1</li>
        <li>this is item 2</li>
        <li>this is item 3</li>
</ul>
</td>
</tr>
<tr>
    <td># this is item 1<br/># this is item 2<br/># this is item 3</td>
<td>
<ol>
        <li>this is item 1</li>
        <li>this is item 2</li>
        <li>this is item 3</li>
</ol>
</td>
</tr>
<tr>
    <td>this is a link to "Google":http://www.google.com</td>
    <td>this is a link to <a href="http://www.google.com">Google</a></td>
</tr>
</table>
<p>For a complete list of formatting rules, check out the <a href="http://www.textism.com/tools/textile">Textile Website</a>.</p>
<h2><a id='remarks'>Remarks</a></h2>
<p>Many records have a “Remarks” field. Unless otherwise stated on the input form for a particular record, “remarks” are to be used for information that is private to the agency. Remarks never appear on any printed documentation and are never shown to anyone that does not have a user account on the system. Do not put actual record information in the “remarks” field. Put the actual record information in the place provided for it on the form.</p>
<p>For instance, when you create a new <a href="call.html"><em>Call</em></a>, the details for the call sheet must be entered in the “Details” field, not in “Remarks”. Every “Remarks” field has a tooltip with the following instructions:</p>
<blockquote>
<p>These remarks will not appear on any printed documentation. They are for internal &lt;agency&gt; use only and are not to be considered part of official record.</p>
</blockquote>
<p>So, what do you use remarks for? A few examples are:</p>
<ul>
	<li>You might enter notes in the remarks section that are only intended for other officers.</li>
	<li>For <a href="call.html"><em>Calls</em></a>, you might want to leave a note for the officers to let them know that you have a particular form or document in your possession that they may need when preparing their offense report.</li>
	<li>Remarks are searchable, so you might want to enter “key words” into remarks that you could later use when searching for this particular record.</li>
</ul>
<p>Consider “Remarks” as a “sticky note” that is attached to the official record to remind you or someone else about information that should not appear in the official record.</p>
<p>All &#8220;Remarks&#8221; fields accept <a href="#textile">Textile</a> formatting commands.</p>
<h2><a id='tooltip'>Tooltips</a></h2>
<p>Tooltips are small informational boxes that pop up whenever you move your mouse pointer over a target area. In FiveO, when words are shown with dotted underlines, it tells you that more information is available. No click is required. Simply move the pointer over the marked words and the box will pop up.  Move away from the marked words and the box will go away.</p>
<p>When you see words with the tooltip indicator, you should (at least once) point your mouse at them and read the extra information. Tooltips have taken the place of many of the &#8220;Tips&#8221; that tended to clutter up many of the pages in FiveO Version 1.</p>
<p>Keep an eye out for these tooltips as you browse through the program. The information they provide is useful and should not be ignored.</p>
<h2><a id='permissions'>About Permissions</a></h2>
<p>Whenever you see the word <em>permission</em> (emphasized text) in this manual it refers to the rights granted to your <a href="user.html"><em>User</em></a> account.</p>
<p>Every page in the FiveO software requires that the <a href="user.html"><em>User</em></a> be assigned a particular set of <a href="group.html#perms"><em>permissions</em></a>. In this reference manual, each feature described in this manual will have a &#8220;Permissions&#8221; section detailing the <a href="group.html#perms"><em>permissions</em></a> required to access the various parts of the feature.</p>
<p>There are three general levels of <a href="group.html#perms"><em>permissions</em></a> that can be assigned for each feature.  They are:</p>
<ul>
	<li>&#8220;View&#8221; &#8211; this allows the <a href="user.html"><em>User</em></a> the ability to see the various pages for the feature.</li>
	<li>&#8220;Update&#8221; &#8211; this allows the <a href="user.html"><em>User</em></a> the ability to create new records and edit existing records for the feature.</li>
	<li>&#8220;Destroy&#8221; &#8211; this allows a <a href="user.html"><em>User</em></a> to delete records.</li>
</ul>
<p>These levels of <a href="group.html#perms"><em>permissions</em></a> are specific, meaning that &#8220;Update&#8221; <a href="group.html#perms"><em>permission</em></a> does not also grant &#8220;View&#8221; <a href="group.html#perms"><em>permission</em></a> automatically. For &#8220;Update&#8221; <a href="group.html#perms"><em>permission</em></a> to mean anything, you must also have &#8220;View&#8221; <a href="group.html#perms"><em>permission</em></a> so you can actually see something to update.</p>
<p>When <a href="user.html"><em>Users</em></a> are granted <a href="group.html#perms"><em>permissions</em></a>, this is taken into consideration.  For instance, if someone is given read-only access to a feature, they are granted &#8220;View&#8221; <a href="group.html#perms"><em>permission</em></a> for that feature. If they are to be given the ability to modify information, then they are granted &#8220;View&#8221; and &#8220;Update&#8221; <a href="group.html#perms"><em>permissions</em></a>. Someone able to delete records would be granted &#8220;View&#8221;, &#8220;Update&#8221; and &#8220;Destroy&#8221; <a href="group.html#perms"><em>permissions</em></a>. When the &#8220;Permissions&#8221; sections for a feature specifies &#8220;Update&#8221; <a href="group.html#perms"><em>permission</em></a> to perform a certain action, it <strong>implies</strong> that &#8220;View&#8221; <a href="group.html#perms"><em>permission</em></a> is also required without actually saying so.  However, when any &#8220;unusual&#8221; <a href="group.html#perms"><em>permission</em></a> is required for any action, it will always be specified explicitly.</p>
<p>For example, consider <a href="bond.html"><em>Bonds</em></a>. In order to add a new <a href="bond.html"><em>Bond</em></a>, the manual states that &#8220;Update Bond&#8221; <a href="group.html#perms"><em>permission</em></a> is required.  This <strong>implies</strong> that &#8220;View Bond&#8221; <a href="group.html#perms"><em>permission</em></a> is also required. However, new <a href="bond.html"><em>Bonds</em></a> can only be created by clearing &#8220;bondable&#8221; <a href="hold.html"><em>Holds</em></a> which requires &#8220;Update Booking&#8221; (again, the &#8220;View Booking&#8221; is implied) <a href="group.html#perms"><em>permission</em></a> and appropriate <a href="jail.html"><em>Jail</em></a> access as well. These are considered &#8220;unusual&#8221; <a href="group.html#perms"><em>permission</em></a> requirements and will always be stated explicitly.</p>
<p>When reading the manual sections, you may see references to features, links or buttons that do not actually appear in the program when you log in. This is because your <a href="user.html"><em>User</em></a> account has not been assigned the <a href="group.html#perms"><em>permissions</em></a> required to access them. If you feel you should have access to these features, you should contact your Site Administrators.</p>
<div class='nav'><a href="toc.html">Index</a><span class='spacer'>&loz;</span><a href='index.html'>Introduction</a><span class='spacer'>&larr;</span><span class='current'>Common Features</span><span class='spacer'>&rarr;</span><a href='absent.html'>Absent</a></div>
