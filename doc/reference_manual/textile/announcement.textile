h1. Announcement

_Announcements_ are notices that are posted to the "Home Page" so that everyone has a chance to view them. It acts like a departmental bulletin board. No special "_permissions_":common.html#permissions are required to view the "Home Page" announcements.

On most announcements you will find a link in the upper right corner labeled _Hide_. Once you have read an _Announcement,_ you can click this link to remove it from your "Home Page" view. We recommend you read the announcement fully before “hiding” it from view.

Some _Announcements_ cannot be hidden from view. On these, there will be a “lock” icon instead of a _Hide_ link. These are special announcements that the Site Administrators feel are important enough to be permanently visible.

At the bottom of the announcement list on the "Home Page" is a _primary button bar_ with a _Show All_ button. Clicking this button will display the "All Announcements" page showing even the ones you have hidden. On this page, hidden _Announcements_ will show a link in the upper right corner labeled _Show_. Clicking the _Show_ link will "unhide" _Announcement_ so it reappears on the "Home Page".

From time to time, an _Announcement_ will get updated with new information. If this happens to an _Announcement_ that you have previously hidden, it will become "unhidden" again, just as if you had clicked the _Show_ link.

h2. <a id='permissions'>Permissions</a>

As mentioned above, there are no "_permissions_":common.html#permissions required to view the _Announcements_ shown on the "Home Page".  However, to access the "Announcement List" and other pages you need "View Announcement", to add or edit _Announcements_ you need "Update Announcement, and to delete _Announcements_ you will need "Destroy Announcement".

h2. <a id='list'>Announcements List</a>

Clicking _Announcements_ in the _Utils drop-down menu_ takes you to the "Announcements List". _Announcements_ are shown here in reverse "updated" order (newer ones first).

There is a _New_ button on this page for creating new _Announcements._

h2. <a id='page'>Announcement Page</a>

Clicking an _Announcement_ in the "Announcements List" displays the "Announcement Page". The "Announcement Page" shows an _Announcement_ much as it would look on the "Home Page", but without the author photo.

This page has the following buttons:
* _Add_ - used to add new _Announcements._
* _Edit_ - used to edit existing _Announcements._
* _Delete_ - used to delete existing _Announcements._

h2. <a id='entry'>Announcement Entry</a>

When creating or editing an _Announcement_, the following fields are provided:

- Date := This is generated automatically by the program and cannot be changed.
- Subject := *(Required)* This is the title of the _Announcement_ and should be short, but should accurately describe the purpose for the announcement. Many people have the tendency to skim through _Announcements_ looking only at the titles until they see something that interests them. Make sure the title conveys the most important point in the announcement.
- Locked := If the "Locked" box is checked, the users will not be able to “Hide” this announcement and it will remain on their "Home Page" permanently. Use this feature sparingly. Only the most critical and permanently valid announcements should ever be “Locked”.
- Details := *(Required)* This is for the text of the announcement. "Textile":common.html#textile formatting commands can be used in this text box to make your _Announcement_ look better.

At any time during the entry of an _Announcement_, you can click the _Preview_ button to see what your announcement will look like (it will be displayed in a popup window). This is particularly useful when you use a lot of "Textile":common.html#textile formatting.

<!-- navigation bar -->
