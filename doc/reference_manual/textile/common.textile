h1. Common Features

The following paragraphs describe features of FiveO that are global in nature (they work the same for all sections). They are included here for reference by the other sections in this manual.

h2. <a id='filter'>Filters</a>

Most features in FiveO have an "advanced" search feature (called a "Filter"). These "Filters" are record specific (meaning there is no "global filter"). To access the "Filter" for a particular feature, navigate to the "&lt;feature&gt;s List" or "&lt;feature&gt; Page" page for that feature. If the feature has a "Filter" form, it will show up below the _primary button bar_.

bq. If you navigate to a feature page and cannot find a "Filter" form, then the feature does not have search capability.

The advanced search "Filter" does not use the "full text" search indexes. Instead, the form shows each of the searchable fields for the feature. You must enter your search terms in the appropriate fields on this form. All of the information you enter on this form must exist in a record for the record to be considered a match. In other words, the "query" created by the "Filter" form uses "AND" instead of "OR" when matching records.

Although it may sound counter-intuitive, the more information you enter into a search "Filter", the *less* likely you are to find what you are searching for. You should begin to understand this when you consider that records must match exactly with all of the information you provide. Take street addresses for example... if you want to find all "_People_":person.html who live on “Main Street”, it is much better to enter “main” (capitalization is ignored on "Filters") in the "Street:" search field instead of “main street” because the word “street” might have been entered as “st.” on some records, “street” on others or may have been left off altogether by the "_User_":user.html that originally entered the information. The same is true for names. "Joe Smith" might at first seem to be just the thing to enter into the "_Person_":person.html "Filter" "Name:" field if you are looking for somebody called "Joe Smith".  However, it will not match the correct "_Person_":person.html if they were entered into the database as "Joseph Smith", or if "Joe" is actually a "middle" name and the "_Person_":person.html was entered something like "David J. Smith". Best practice when searching by names is to search last-name-only to get the most matches, then take the time to look through the results to find the actual record you are looking for.

h2. <a id='standard_text'>Standard Text</a>

Most multi-line text blocks use "standard text formatting" rules. This basically means you are limited to paragraphs and simple manual lists for formatting your text.

To break your information into paragraphs, you should type each paragraph without using the @[Enter]@ key to break lines. As you near the edge of the text box, just keep typing and the line will automatically wrap itself as needed. At the end of each paragraph, hit the @[Enter]@ key twice (ends the previous paragraph and adds a blank line before starting the next paragraph).  

You should only use the @[Enter]@ key when you absolutely need to end a line at a specific point and start the next text on the next line. The space provided for you to write the report is not the same as that on the generated PDF. As long as you remember to type your paragraphs without using @[Enter]@ to artificially break the lines, the paragraphs will be correctly reformatted by the PDF generation process to fit the space provided for them.

Simple lists can be created using the asterisk (*) for "bullet" lists and manually entering numbers (1,2,3, etc) for "itemized" lists.  You should start your list after a blank line (hit @[Enter]@ twice to end the previous paragraph and provide the recommended blank line). Hit @[Enter]@ after typing each item in the list in its entirety (let the program itself wrap long list items into multiple lines). Use the @[Enter]@ key twice on the last list item to provide a blank line between the list and the next paragraph.

Do not rely on multiple blank spaces to format text. The program will remove these extra spaces when it displays the results on the page.

You cannot use @[Tab]@ to indent paragraphs or other information.  The @[Tab]@ key is used by the form to move to the next input field.

h2. <a id='textile'>Textile Fields</a>

The word “Textile” in FiveO context has nothing to do with the clothing industry... it instead refers to the "Textile Markup Language":http://textile.thresholdstate.com, which is used to simplify text formatting on web pages (this manual is written using Textile). Many of the forms in the software have one or more text boxes that will accept Textile formatting commands (these are shown with a pale green background). Any valid Textile commands you enter in these special fields will be processed by the software before displaying the information on the screen. This allows you to have headings, bold and italic text, bullet and numbered lists and more.

All references to "Textile", outside this section, will be linked to this section for your convenience.

The following table shows some useful Textile commands:

<table class='normal'>
  <tr>
    <th style='width:49%;'>You Type</th>
    <th>Displayed on the Page</th>
  </tr>
  <tr>
    <td>p. This is a paragraph<br/>p. This is another paragraph</td>
    <td>This is a paragraph<br/><br/>This is another paragraph</td>
  </tr>
  <tr>
    <td>This is ==*bold*== text</td>
    <td>This is *bold* text</td>
  </tr>
  <tr>
    <td>This is ==_emphasized_== text</td>
    <td>This is _emphasized_ text</td>
  </tr>
  <tr>
    <td>* this is item 1<br/>* this is item 2<br/>* this is item 3</td>
    <td>
      <ul>
        <li>this is item 1</li>
        <li>this is item 2</li>
        <li>this is item 3</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td># this is item 1<br/># this is item 2<br/># this is item 3</td>
    <td>
      <ol>
        <li>this is item 1</li>
        <li>this is item 2</li>
        <li>this is item 3</li>
      </ol>
    </td>
  </tr>
  <tr>
    <td>this is a link to =="Google":http://www.google.com==</td>
    <td>this is a link to "Google":http://www.google.com</td>
  </tr>
</table>

For a complete list of formatting rules, check out the "Textile Website":http://www.textism.com/tools/textile.

h2. <a id='remarks'>Remarks</a>

Many records have a “Remarks” field. Unless otherwise stated on the input form for a particular record, “remarks” are to be used for information that is private to the agency. Remarks never appear on any printed documentation and are never shown to anyone that does not have a user account on the system. Do not put actual record information in the “remarks” field. Put the actual record information in the place provided for it on the form.

For instance, when you create a new "_Call_":call.html, the details for the call sheet must be entered in the “Details” field, not in “Remarks”. Every “Remarks” field has a tooltip with the following instructions:

bq. These remarks will not appear on any printed documentation. They are for internal &lt;agency&gt; use only and are not to be considered part of official record.

So, what do you use remarks for? A few examples are:

* You might enter notes in the remarks section that are only intended for other officers.
* For "_Calls_":call.html, you might want to leave a note for the officers to let them know that you have a particular form or document in your possession that they may need when preparing their offense report.
* Remarks are searchable, so you might want to enter “key words” into remarks that you could later use when searching for this particular record.

Consider “Remarks” as a “sticky note” that is attached to the official record to remind you or someone else about information that should not appear in the official record.

All "Remarks" fields accept "Textile":#textile formatting commands.

h2. <a id='tooltip'>Tooltips</a>

Tooltips are small informational boxes that pop up whenever you move your mouse pointer over a target area. In FiveO, when words are shown with dotted underlines, it tells you that more information is available. No click is required. Simply move the pointer over the marked words and the box will pop up.  Move away from the marked words and the box will go away.

When you see words with the tooltip indicator, you should (at least once) point your mouse at them and read the extra information. Tooltips have taken the place of many of the "Tips" that tended to clutter up many of the pages in FiveO Version 1.

Keep an eye out for these tooltips as you browse through the program. The information they provide is useful and should not be ignored.

h2. <a id='permissions'>About Permissions</a>

Whenever you see the word _permission_ (emphasized text) in this manual it refers to the rights granted to your "_User_":user.html account.

Every page in the FiveO software requires that the "_User_":user.html be assigned a particular set of "_permissions_":group.html#perms. In this reference manual, each feature described in this manual will have a "Permissions" section detailing the "_permissions_":group.html#perms required to access the various parts of the feature.

There are three general levels of "_permissions_":group.html#perms that can be assigned for each feature.  They are:
* "View" - this allows the "_User_":user.html the ability to see the various pages for the feature.
* "Update" - this allows the "_User_":user.html the ability to create new records and edit existing records for the feature.
* "Destroy" - this allows a "_User_":user.html to delete records.

These levels of "_permissions_":group.html#perms are specific, meaning that "Update" "_permission_":group.html#perms does not also grant "View" "_permission_":group.html#perms automatically. For "Update" "_permission_":group.html#perms to mean anything, you must also have "View" "_permission_":group.html#perms so you can actually see something to update.

When "_Users_":user.html are granted "_permissions_":group.html#perms, this is taken into consideration.  For instance, if someone is given read-only access to a feature, they are granted "View" "_permission_":group.html#perms for that feature. If they are to be given the ability to modify information, then they are granted "View" and "Update" "_permissions_":group.html#perms. Someone able to delete records would be granted "View", "Update" and "Destroy" "_permissions_":group.html#perms. When the "Permissions" sections for a feature specifies "Update" "_permission_":group.html#perms to perform a certain action, it *implies* that "View" "_permission_":group.html#perms is also required without actually saying so.  However, when any "unusual" "_permission_":group.html#perms is required for any action, it will always be specified explicitly.

For example, consider "_Bonds_":bond.html. In order to add a new "_Bond_":bond.html, the manual states that "Update Bond" "_permission_":group.html#perms is required.  This *implies* that "View Bond" "_permission_":group.html#perms is also required. However, new "_Bonds_":bond.html can only be created by clearing "bondable" "_Holds_":hold.html which requires "Update Booking" (again, the "View Booking" is implied) "_permission_":group.html#perms and appropriate "_Jail_":jail.html access as well. These are considered "unusual" "_permission_":group.html#perms requirements and will always be stated explicitly.

When reading the manual sections, you may see references to features, links or buttons that do not actually appear in the program when you log in. This is because your "_User_":user.html account has not been assigned the "_permissions_":group.html#perms required to access them. If you feel you should have access to these features, you should contact your Site Administrators.

<!-- navigation bar -->
