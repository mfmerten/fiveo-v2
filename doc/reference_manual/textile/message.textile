h1. Message

p(warning). WARNING: This page is outdated!

Similar to EMail, _Messages_ allow FiveO "_Users_":user.html to communicate with each other. If you look in the _left navigation bar,_ just under the _Logout_ button, you will see your message notifications. It lets you know when you have unread messages in your “inbox”. You have three message “boxes” (they can be accessed in the _User_ section of the _drop-down menu_):

* Inbox: for messages you receive from other users.
* Outbox: for messages you send to other users.
* Trashbin: for messages you have deleted.

When unread _Messages_ are available, the notification will be a link you can click to go directly to your "inbox". Received _Messages_ will stay in your "inbox" until you delete them, which moves them to your "trashbin". When you send a _Message,_ it goes into your "outbox" where it will stay until you delete it, when it will be moved to your "trashbin". _Messages_ remain in your "trashbin" until the other person deletes them on their end.

_Messages_ you send to another user are viewable only by you and the other user. However, you should be aware that all _Messages_ in the FiveO system are recorded in the "_Message Log_":message_log.html where they can be viewed by Site Administrators, so you should not consider them private. That information, and more, is included in the “site message policy”. The policy can be accessed at any time by clicking the _Click Here_ link in the warning displayed on the "New Message" page. If you do not agree with any of the conditions stated in the "site message policy" statement, do not use the _Message_ system.

h2. <a id='permissions'>Permissions</a>

To Be Completed

h2. <a id='list'>Messages List</a>

To Be Completed

h2. <a id='page'>Message Page</a>

To Be Completed

h2. <a id='entry'>Message Entry</a>

To Be Completed

h2. <a id='deleting'>Deleting Messages</a>

You should delete _Messages_ you no longer need from your "inbox" and "outbox" on a routine basis (the "trashbin" will take care of itself). There are two ways to do this.

First, you can delete any individual _Message_ by clicking on the _Delete_ button displayed on the "Message Page".

Second, you can delete multiple _Messages_ at a time from the "Messages List". On the list page you can select each _Message_ you want to delete by clicking the checkbox for that _Message._ When all _Messages_ to delete are selected, click the _Delete_ button.

h2. <a id='printing'>Printing Messages</a>

To print a single _Message,_ click the _Display_ button on the "Message Page". This will redisplay the _Message_ without the usual _header_ and _left navigation bar._ Then click the _Print_ button.

To print multiple _Messages_ at the same time (digest), go to the "inbox" (or "outbox") and select each _Message_ you want to include, then click the _Digest_ button. Like _Display_, this will show all of the selected _Messages_ on the same page without the page _header_ and _left navigation bar._ Then click the _Print_ button.

<!-- navigation bar -->
