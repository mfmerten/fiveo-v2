h1. Charge

p(warning). WARNING: This page is outdated!

"_Arrests_":arrest.html, "_Citations_":citation.html and "_Warrants_":warrant.html all record _Charges_ being made against an individual. These _Charges_ can be written in "free-form" but usually are looked up in the "_Statutes_":statute.html database. Each _Charge_ has a designated "Type":
# District: A "District" "_Charge_":charge.html is one that will be prosecuted in District Court. "_Arrests_":arrest.html, "_Citations_":citation.html and "_Warrants_":warrant.html may all include "District" _Charges._
# City: If a charge is for Mayor's Court for a local (inside the Parish) city, it is classified as a "City" "_Charge_":charge.html. "City" _Charges_ cannot be included on "_Citations_":citation.html.
# Other: An “Other” "_Charge_":charge.html is for charges that originate outside the Parish and are not for local courts. "Other" _Charges_ cannot be inlcuded on "_Citations_":citation.html.

h2. <a id='entry'>Charge Entry</a>

The input forms for "_Arrests_":arrest.html, "_Warrants_":warrant.html and "_Citations_":citation.html use dynamic fields for charges (this means you can have as many charges on a single form as you want). Click the "add" icon (!add.png!) to add as many blank lines as you need. To remove a line, click the "delete" icon (!delete.png!). Each charge must be entered on a different line (although multiple counts of the same charge is entered on one line by entering the charge count in the “Count” box provided). For "_Arrests_":arrest.html and "_Warrants_":warrant.html, the _Charge_ entry lines include a "Charge Type" select control so that you can specify the type of charge. "_Citations_":citation.html do not have this type select control because they are always considered "District" charges.

When entering the actual charge, the best way is to enter just the RS (&lt;title&gt;:&lt;section&gt;.&lt;paragraph&gt;) code. For instance, if you enter “14:103” in the charge field and press @[Tab]@, it will automatically update to “RS 14:103 Disturbing the Peace”. Don't enter the “RS” part, just enter the numeric part. The text you enter in the charge box is searched in the "_Statutes_":statute.html database and the first statute that matches is displayed. If you try to enter words like “Burglary”, you will likely not match the correct statute because there are quite a few statutes that include the word “Burglary”.

If you need to enter a charge that is not in the "_Statutes_":statute.html database, you should always include the RS code from the current ??Louisiana Revised Statutes?? so that ambiguity can be prevented. You should also add the missing _Charge_ to the "_Statutes_":statute.html database if you have the proper "_permissions_":common.html#permissions to do so, or contact your Site Administrators to do it for you.

If the actual statute name displayed when you press @[Tab]@ is too long (some of the statute names are rather long-winded), it is perfectly acceptable for you to edit the name to a more commonly used version, as long as you do not remove the RS code. For instance, it would be OK for you to change “RS 14:98 Operating a vehicle while intoxicated” to “RS 14:98 DWI”, but not to change it to just “DWI”. Always provide the code reference!

bq. If the full "_Statute_":statute.html name is too long to work properly with certain forms, the "_Statutes_":statute.html have an extra "common name" field that can be used to provide a shorter name for the "_Statute_":statute.html. You can (if you have "Update Statute" "_permission_":common.html#permissions) provide a shorter "common name" that the _Charges_ fields will use by default. If you do not have the required "_permission_":common.html#permissions to do this, contact your Site Administrators for help.

"City" Charges entered on "_Arrests_":arrest.html have an extra select control that allows you to select the City that is “pressing” the charge. The cities that show up in the City select control are dependent on a setting in the "_Site Configuration_":site_configuration.html. There is also a "write-in" field for the City name that can be used in a pinch if the appropriate City does not show up in the select control.  However, you should always use the select control if at all possible. All Cities within the "local" jurisdiction should be available in the select control. If any are missing, you should contact your Site Administrators and get them to add it to the configuration setting.

"Other" Charges for "_Arrests_":arrest.html can only be placed by serving "_Warrants_":warrant.html, and the agency for the charge is taken directly from the "_Warrant_":warrant.html.

<!-- navigation bar -->
