h1. Absent

_Absents_ (a.k.a. Work Crews) record the periods of time when inmates are not in jail, but remain in custody. The _Absent_ may be for multiple inmates, such as when a group of prisoners is taken out to do yard work or pick up trash on the highways, but it can also be used for a single inmate for things like doctor visits, court appearances, etc.

Generally, _Absents_ should be used for all occasions when one or more inmates are absent from the jail, as long as custody does not change (they are escorted by one or more officers the entire time they are gone). When inmates leave the jail and are placed into the custody of another agency, use a "_Transfer_":transfer.html instead of an _Absent_.

Several things happen to the “current location” (as shown on the "&quot;Booking Page&quot;":booking.html#page) of each inmate throughout the _Absent_ process:

* When the _Absent_ is first created, it is in the “Pending” state. The “current location” for all inmates listed on the _Absent_ changes to say they are “Pending: Absent: &lt;name of Absent&gt;”.
* When the Leave button on an _Absent_ is clicked, this signifies that the inmate(s) have left the building. Their “current location” changes to say “Absent: &lt;name of Absent&gt;”. Also, the “Booking List”, when printed, clearly shows when inmates are currently “absent”.
* When the inmates return to the jail, you click the _Return_ button. Their “current location” then changes back to the normal “&lt;facility&gt;: &lt;cell&gt;”.

There are two ways to create an _Absent._ Use whichever is most convenient for you:
# The first way is to navigate to the "Absents List" by selecting a jail facility in the _Jail_ _drop-down menu_ to display the "&quot;Bookings List&quot;":booking.html#list and then clicking the _Absents_ button found there. Then click the _New_ button on the "Absents List". You then fill out the requested information and list all of the inmates involved.
# The second way may be quicker if you take a work crew out on a regular basis, and particularly when the work crew has nearly the same list of inmates each time. Navigate to your last _Absent_ and click the _Copy_ button. This creates a new _Absent_ with all the information from the previous one already filled in. You merely need to make the few (if any) necessary changes and submit it.

h2. <a id='permissions'>Permissions</a>

The _Absent_ feature makes use of both "_Booking_":booking.html and "_Jail_":jail.html "_permissions_":common.html#permissions.  There are three basic levels of access:
# View - this allows you to see the _Absent_ records and requires "View Booking" "_permission_":common.html#permissions and "View" access to the appropriate "_Jail_":jail.html.
# Update (Add/Edit) - This allows you to add new _Absents_ and update existing ones. It requires "Update Booking" "_permission_":common.html#permissions and "Update" access for the appropriate "_Jail_":jail.html.
# Delete - This allows you to delete _Absents_. It requires "Destroy Booking" "_permission_":common.html#permissions and "Update" "_permission_":common.html#permissions for the appropriate "_Jail_":jail.html.

Because of the way "_permissions_":common.html#permissions are assigned, you may well be able to add and edit _Absents_ in one "_Jail_":jail.html but not even view them in another.

h2. <a id='list'>Absents List</a>

The "Absents List" is accessed by clicking _Absents_ on the "&quot;Booking List&quot;":booking.html#list for a particular "_Jail_":jail.html. The most recent _Absents_ are listed first. By default, the "Absents List" shows only the active "not yet returned" _Absents_. You can switch the list to show all _Absents_ by clicking the provided _Show All_ button.

 The "Absents List" has the following buttons:
* _Show All_ - causes the list to show all _Absent_ records.  Only available when viewing the "Active" list.
* _Show Active_ - causes the list to show only the active _Absent_ records (this is the default). This button is only shown when the list is showing "all" _Absents._
* _New_ - creates a new _Absent_

h2. <a id='page'>Absent Page</a>

Click on an _Absent_ shown on the "Absents List" takes you to the "Absent Page". The details for the _Absent_ are shown along with a list of the inmates selected for the _Absent_.

The buttons available on the "Absent Page" are:
* _New_ - used to create new _Absents_
* _Copy_ - used to duplicate the currently displayed absent as a shortcut for repeated work crews
* _Edit_ - used to edit the displayed _Absent_
* _Delete_ - used to delete _Absent_ records
* _Leave_ - this button only shows up for pending _Absents_ (have not yet left the building). Clicking this changes the status of the _Absent_ to “Left”.
* _Return_ - this button only shows up for _Absents_ that have already left, but not yet returned. Clicking it will change the status of the _Absent_ to “Returned”.
* _Checklist_ - used to generate a PDF document listing all selected inmates with boxes for check-out and check-in.

h2. <a id='entry'>Absent Entry</a>

When creating or editing _Absent_ records, the following fields are available:

- Facility := This is the facility housing the inmates for the _Absent._ It is preset to the "_Jail_":jail.html you were viewing when you clicked the _Absents_ button. It cannot be changed. If you need to include inmates from another "_Jail_":jail.html, you will have to navigate to that "&quot;Booking Page&quot;":booking.html#page to access the _Absents_ feature for that "_Jail_":jail.html. All inmates on a single _Absent_ are from the same facility.
- Absent Type := Select the type of the _Absent._ Available absent types can be customized by the "_Jail_":jail.html Administrator.
- Escort Officer := This is the primary officer in charge of escorting the prisoners during their absence. All valid "_Officer Contacts_":contact.html#officer should show up in this select control. Write-in fields are available if the officer is not listed.
- Left := Normally, this is set to the time the _Absent_ crew leaves the facility by clicking the _Leave_ button on the "Absent Page". However, it can be set while editing the _Absent_ record if necessary.
- Returned := Normally, this is set to the time the _Absent_ crew returns tot he facility by clicking the _Return_ button on the "Absent Page". However, it can be set while editing the _Absent_ record if necessary.
- Select Prisoners := This is the list of inmates participating in the _Absent_. See "&quot;Adding Inmates&quot;":#inmates below.

h3. <a id='inmates'>Adding Inmates</a>

When you create (or edit) an _Absent_, you must select the inmates that will be leaving the jail. You do this using a dynamic field in the “Select Prisoners” section of the form. You can add as many inmates as you wish by clicking the "add" icon (!add.png!) and you can remove an inmate by clicking the "delete" icon (!delete.png!).

You can add an inmate in one of two ways:

# By "Booking ID": Enter the inmate's "Booking ID" (not their "Person ID"!) in the field provided and press @[Tab]@. If the "Booking ID" is valid and current, the information for the inmate will appear on the form. The Booking ID field is only enabled (selectable) for new inmate lines on the form.
# _Find By Name_: If you click the _Find By Name_ button, a pop-up dialog box will appear asking you to enter the name. It is highly recommended that you enter only the last name. You will have a very hard time finding inmates if you enter full names in this dialog window. All active inmate records for your facility that match the name you enter will then be displayed in a pop-up window. Locate the correct inmate in the list and click anywhere on that line. This will insert the "Booking ID" into the _Absent_ form. The _Find By Name_ button will only appear on new inmate lines on the form.

Once the _Absent_ form has been submitted, all inmates listed on it are "fixed" and cannot be changed to a different inmate (the Booking ID field is disabled and the _Find by Name_ button is missing). However, you can still delete them (by clicking !delete.png!). In the event that a wrong inmate was entered for an _Absent,_ you will need to delete them and add a new entry for the correct inmate.

<!-- navigation bar -->
