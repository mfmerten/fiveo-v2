h1. Transfer

p(warning). WARNING: This page is outdated!

_Transfers_ are entered when an inmate changes custody (to or from a different agency). Inmates can be transferred into or out of the local jail, and the transfers can be permanent or temporary. The conditions required to be able to transfer an inmate are:
# the inmate must be currently "_Booked_":booking.html.
# there must be no active “Temporary Transfer” "_Hold_":hold.html.

h2. <a id='permissions'>Permissions</a>

To Be Completed

h2. <a id='list'>Transfers List</a>

Clicking _Transfer Log_ in the _drop-down menu_ (_Jail_ section) takes you to the "Transfers List" page. The list shows the Transfer ID number, the date and time, the person transferred, the type of transfer and the source and destination for the _Transfer._ The _Transfers_ are listed in reverse order (newest first). There are no buttons available on this page. _Transfers_ can only be added from the "&quot;Booking Page&quot;":booking.html#page.

h2. <a id='page'>Transfer Page</a>

Clicking a _Transfer_ in the "Transfers List" displays the "Transfer Page". This page shows the transfer information, the starting location (agency and officer), the ending location (agency and officer) and any remarks that may have been entered. This page has only a _Delete_ button (if you have _permission_ to use it). _Transfers_ cannot be edited once entered and can only be added from the "&quot;Booking Page&quot;":booking.html#page. However, if an _Transfer_ was added incorrectly, it can be deleted and re-entered.

h2. <a id='entry'>Transfer Entry</a>

When adding a _Transfer,_ the form operation is a little different than most cases. Initially, the form has the following fields:

- Person := This is the Person ID and name of the inmate transferred. It cannot be changed.
- Location := This is the current physical location of the inmate. It is obtained by existing information and cannot be changed.
- Current Holds := A list of the currently active "_Holds_":hold.html is shown as a reminder to the jailer.
- Transfer Type := Although not labeled as such, there is only one select control shown. The choices available may be limited by the status of a particular "_Booking_":booking.html, but usually include the options shown in the "&quot;Transfer Types&quot;":#types section below.

The desired _Transfer_ type is selected using the select control and _Submit_ is clicked. This does not add the _Transfer,_ it merely changes the form displayed to allow the entry of the appropriate information for the selected type. The form now displays additional form fields (the following list is for the “DOC Transfer (IN)” _Transfer_ type):

- Date/Time := *(Required)* This is the date/time of the _Transfer._
- From Agency := *(Required)* This is the Agency the transfer started at. The options listed here follow the standard agency requirements (business "_Contact_":contact.html marked "Active" and "Law Enforcement").
- From Officer := *(Required)* This is the officer giving up custody (from the other Agency) during the _Transfer._
- To Agency := *(Required)* In this example, the transfer is incoming and the "To Agency" should already be filled in with the local Agency name.
- To Officer := *(Required)* This is the officer (local Agency in this example) taking custody of the prisoner during the _Transfer._
- Remarks := *(Required)* This is a standard "&quot;Remarks&quot;":common.html#remarks field but in the particular case of a _Transfer,_ the officer is required to enter something here.

h2. <a id='types'>Transfer Types</a>

The following _Transfer_ types are available, although not all will be available in all situations:

h3. <a id='temp-out'>Temporary Transfer (Out)</a>

This type of _Transfer_ is used when an inmate is transferred to another agency for a temporary period of time. The _Transfer_ adds a "_Hold_":hold.html to the "_Booking_":booking.html indicating that the inmate is temporarily transferred and the "_Booking_":booking.html stays active. The “headcount” report will show the inmate in the “total headcount”, but not in the “physical headcount”. When the inmate is returned to the local jail, the jailer need only clear the “Temporary Transfer” "_Hold_":hold.html.

h3. <a id='temp-in'>Temporary Housing (In)</a>

The “Temporary Housing (In)” _Transfer_ is used when the local jail is providing temporary housing for an inmate from another jail. The inmate would be "_Booked_":booking.html as normal, and then the “Temporary Housing (In)” _Transfer_ would be added. This adds a “Temporary Housing” "_Hold_":hold.html to the "_Booking_":booking.html. When the inmate is returned to the other jail, the jailer needs to clear the “Temporary Housing” "_Hold_":hold.html and close the "_Booking_":booking.html.

bq. Note that the “Temporary Housing (In)” _Transfer_ *must not be used for D.O.C. inmates.* For these inmates, you should use the “DOC Transfer (In)” _Transfer_ instead. This is the only way to create the proper records necessary to track DOC "_Sentences_":sentence.html.

h3. <a id='perm-out'>Permanent Transfer (Out)</a>

The “Permanent Transfer (Out)” _Transfer_ is used when the inmate is transferred to another jail permanently (will not be coming back to the local jail). This type of _Transfer_ will automatically clear all active "_Holds_":hold.html on the "_Booking_":booking.html and then close it. An example use of this type of Transfer would be a D.O.C. inmate being moved to another facility to serve his/her remaining sentence.

h3. <a id='doc-in'>DOC Transfer (IN)</a>

This _Transfer_ is used when a DOC inmate is transferred into the jail to serve his/her sentence. It will create a "_DOC_":doc.html record which then adds a “Serving Sentence” "_Hold_":hold.html to the "_Booking_":booking.html. For these transfers, the jailer needs to update the "_DOC_":doc.html record with the DOC Number and sentencing information after the _Transfer_ is completed. The "_Booking_":booking.html will show up in the “&quot;Problems List&quot;”:booking.html#problems until that information is entered.

<!-- navigation bar -->
