h1. Statute

p(warning). WARNING: This page is outdated!

The _Statutes_ database contains a list of all known State (Title 14, Title 32, Title 40 and Title 56 from the ??Louisiana Revised Statutes??) and Local statutes (laws). This database is referenced whenever a charge is entered on a form.

The _Statutes_ database should be updated as new laws are passed and old laws are repealed. Updating the _Statutes_ database requires “Update Option” permission which is usually reserved for the Site Administrators. If you find that a particular _Statute_ is no longer valid, or a new law is not in the _Statutes_ database, you should contact your Site Administrators to have it updated.

h2. <a id='permissions'>Permissions</a>

To Be Completed

h2. <a id='list'>Statutes List</a>

Clicking _Statutes_ in the _drop-down menu_ (_Utilities_ section) displays the "Statutes List". The list shows the Statute ID and the statute name (the name includes the RS code). There is a _New_ button here allowing you to enter new _Statutes._

h2. <a id='page'>Statute Page</a>

Clicking a _Statute_ shown on the "Statutes List" displays the "Statute Page". There really is not much to a _Statute,_ just a name. This page has the usual _New,_ _Edit_ and _Delete_ buttons if you have _permission_ to use them.

h2. <a id='entry'>Statute Entry</a>

The _Statute_ form has only one field, "Name:". When a statute name is entered, it must contain the absolute identifying information (code number) for that statute so that ambiguity does not arise. For State statutes that is the RS code (i.e. “RS 14:103 Disturbing the peace.”). Also, unless absolutely necessary, the name should be entered exactly like it is shown in the official statute.

<!-- navigation bar -->
