h1. Hold

p(warning). WARNING: This page is outdated!

_Holds_ can be thought of as the “reasons” a "_Booking_":booking.html is open (and a "_Person_":person.html is in jail). When a "_Booking_":booking.html has no active _Holds,_ it should either be closed (and the prisoner released) or a _Hold_ should be added (if the prisoner shouldn't be released).

Normally, _Holds_ are added by performing other actions, such as adding "_Arrests_":arrest.html, "transferring":transfer.html inmates, revoking "_Bonds_":bond.html, etc. However, from time to time it is appropriate to manually add a _Hold_ for a particular reason. See the "&quot;Manual Holds&quot;":#manual-holds section below for details about the available manual _Holds._

Sometimes, a _Hold_ may be added by clearing another _Hold._ For instance, clearing a “72 Hour” _Hold_ will add either a “Bondable” or a “No Bond” _Hold,_ depending on the information entered into the “72 Hour” section of an "_Arrest_":arrest.html form.

h2. <a id='permissions'>Permissions</a>

To Be Completed

h2. <a id='list'>Holds List</a>

Although it is possible to display the "Holds List" by clicking _Holds_ in the _drop-down menu_ (_Jail_ section), it is not terribly useful. Without the context of a "_Booking_":booking.html record, the list of _Holds_ does not make much sense. It is better to look up a particular "_Booking_":booking.html record and view only the _Holds_ related to it. It is not possible to create a new _Hold_ from the "Holds List". That must be done from the "&quot;Booking Page&quot;":booking.html#page.

_Holds_ that are shown on the "Holds List" look exactly like they do from the "&quot;Booking Page&quot;":booking.html#page.

h2. <a id='page'>Hold Page</a>

Clicking a _Hold_ in the "Holds List" takes you to the "Hold Page". The first section shows the person held, the hold date and time, the hold type, who placed the hold and whether any Fines or Bond Amount was entered on the _Hold._ The second section will look different for different types of _Holds._ It may show sentencing and time counting information (for “Serving Sentence” _Holds_) or it may show change of custody information (for _Holds_ created by "_Transfer_":transfer.html) or it may not show a second section at all. The last section shown will be the "&quot;Remarks&quot;":common.html#remarks section, if any remarks were entered on the _Hold_ form.

This page generally has the following buttons available (depending on _Hold_ type and _Hold_ status):
* _Edit_ - This is the standard button for editing a _Hold._ It may not appear if the "_Booking_":booking.html the _Hold_ is attached to has been cleared.
* _Delete_ - This is the standard button for deleting a _Hold._
* _Reactivate_ - This button may appear on a _Hold_ that has been cleared, but only as long as the "_Booking_":booking.html is still open.
* _Clear_ - This button may appear on a _Hold_ that has not yet been cleared. Clicking it does various things depending on the type of _Hold._
* _Clear W/O Bond_ - This button may appear on “Bondable” _Holds_ where the normal clear process creates a "_Bond_":bond.html because in some cases it is necessary to clear a “Bondable” _Hold_ without actually creating a "_Bond_":bond.html.
* _Billable_ (or _Not Billable_) - One of these buttons may appear on certain types of _Holds_ but only if you have "Update Billing" _permission_":common.html#permissions.
* _Add Bond_ - This may appear on certain types of _Holds_ where bonding may be allowed by the Judge but is not normally the case. For instance, “Hold for City” and “Hold for Other Agency”.
* _Return_ - This button usually appears on “Temporary Transfer”, “Temporary Housing” and “Temporary Release” _Holds_ and is used to clear the _Hold._ It is not called “Clear” because it usually does other things as well and, in the case of the “Temporary Housing” _Hold,_ it actually closes the "_Booking_":booking.html record after clearing all active _Holds._
* _Adjustment_ - This button only appears on the types of _Holds_ that track the number of hours served on a sentence (“Serving Sentence” and “Fines/Costs”). Clicking it allows adjusting the amount of time recorded by the _Hold_ using a "&quot;Hold Adjustment&quot;":#adjustment.

h2. <a id='entry'>Hold Entry</a>

When adding or editing a _Hold,_ the following fields appear on the form:

- Person := This shows the Person ID and Name for the "_Person_":person.html linked to the _Hold._ This cannot be changed.
- Date := *(Required)* This is the date and time for the Hold.
- Type of Hold := *(Required)* This sets the type of _Hold_ (for manual _Holds..._ it is set automatically for all others). The type of _Hold_ can only be set on new records. After the record is saved the first time, this field is no longer available for change.
- Placed By := *(Required)* This is the officer that is placing the _Hold._ It defaults to the officer creating the _Hold_ but may be changed if creating it on the authority of another officer. For instance, when a jailer is adding a “Probation” _Hold,_ "Placed By:" would normally be set to the probation officer.
- Can Pay := This is used for different purposes on different _Hold_ types. For "Bondable" type of _Holds,_ this is the bond amount set by the Judge or other judicial authority. For “Fines/Costs” _Holds,_ this is the amount of fines or costs due that can be paid to get out of jail. Certain types of _Holds_ require that this be set, others do not.
- Deny Goodtime? := Only used for "Serving Sentence" and "Fines/Costs" _Holds..._ Sometimes the Judge will issue a "_Sentence_":sentence.html with a condition that “Good Time” does not apply. In those cases this box should be checked. It tells the automatic time counting process to skip “Good Time” for this _Hold._
- Total Sentence := Only used for "Serving Sentence" and "Fines/Costs" _Holds..._ This is the total number of hours sentenced to serve.
- Hours Served := Only used for "Serving Sentence" and "Fines/Costs" _Holds..._ This is the total number of hours served on this "_Sentence_":sentence.html so far.
- Good Time Hours := Only used for "Serving Sentence" and "Fines/Costs" _Holds..._ This is the total number of “Good Time” hours earned on this "_Sentence_":sentence.html so far.
- Time Served Credit := Only used for "Serving Sentence" and "Fines/Costs" _Holds..._ This is the total number of hours granted as credit for time served when the "_Sentence_":sentence.html is initially entered. It is calculated by the program from existing _Hold_ records related to the "_Sentence_":sentence.html and may very well be in error. This value should always be verified by the jailers when a new "_Sentence_":sentence.html is added.
- Agency := Only used on certain types of _Holds_ that require agency information, such as “Hold For City”, “Hold For Other Agency”, “Temporary Transfer”, “Temporary Housing”, etc... This is used in different ways by different types of _Holds._ For instance, with “Hold For City” or “Hold For Other Agency” this will be the City or Other Agency. For “Temporary Transfer” this will be the Agency "_Transferred_":transfer.html to. For “Temporary Housing” this will be the Agency "_Transferred_":transfer.html from.
- Remarks := This is a standard "&quot;Remarks&quot;":common.html#remarks field.

h2. <a id='manual'>Manual Holds</a>

One way to add _Holds_ to a "_Booking_":booking.html is to use the _New Hold_ button (on the "&quot;Booking Page&quot;":booking.html#page. This allows you to enter special “manual” _Holds_ for various purposes:

- Fines/Costs := This _Hold_ is added to keep a prisoner in jail until they pay Fines and/or Court Costs levied by a judicial authority. Typically these _Holds_ are cleared as soon as the prisoner pays the specified amount, or has served the indicated “default” time.
- Hold For City := This is added when the police department for a particular local City (within the Parish) wants the prisoner held for them. They are usually removed when the City picks up the prisoner or when the City requests the hold be removed. More commonly, this _Hold_ type is added automatically when a “City” "_Arrest_":arrest.html is entered.
- Hold For Other Agency := Same as for “Hold For City”, except for jurisdictions outside the Parish. As with City holds, these are commonly added automatically when an “Other” "_Arrest_":arrest.html is entered.
- Hold_For_Questioning := This is used when a suspect is picked up for questioning and held temporarily in the jail. This should be considered a special-purpose “48 Hour” _Hold._
_Editor Note: We do not know if this is ever actually done, but we watch TV the same as everybody else._
- Pending Court := Use this type of _Hold_ when a subject is picked up and held pending a court hearing, trial, sentencing, etc.
- Pending Sentence := When a person goes to court, is convicted and sentenced to server jail time, they are delivered to the jail for "_Booking_":booking.html. However, the "_Sentence_":sentence.html is rarely entered immediately, so the jailer would add this _Hold_ to signify that "_Sentence_":sentence.html entry is pending. When the "_Sentence_":sentence.html is entered, the processing will remove this _Hold_ automatically.
- Probation (Parish) := This indicates that the prisoner should be held in jail until his Parish probation officer says otherwise. “Probation” _Holds_ may be added automatically upon "_Arrest_":arrest.html entry if they have an active "_Probation_":probation.html that is flagged “hold if arrested”. This _Hold_ can also be placed semi-manually at the time of "_Arrest_":arrest.html entry by checking the "Parish Probation" checkbox on the "_Arrest_":arrest.html form.
- Probation/Parole (State) := Same as Probation(Parish) except for the State Probation/Parole officer.

h2. <a id='clearing'>Clearing Holds</a>

_Holds_ should generally be cleared using the _Clear_ button (_Return_ button for "_Transfers_":transfer.html). This performs different actions depending on the type of _Hold,_ but always does the normally correct action for each. Some _Holds_ have an extra button that allows clearing without going through the normal process. For instance, a “Bondable” _Hold_ has a _Clear_ button (normal process by creating a "_Bond_":bond.html) and a _Clear W/O Bond_ button that avoids the "_Bond_":bond.html entry process (not normal but sometimes necessary).

h3. Standard Clear Hold Page

For most _Hold_ types, the _Clear_ button displays the "Clear Hold" page. The fields available on the form are:

- Cleared := *(Required)* This is the date and time the _Hold_ is cleared.
- Cleared Because := *(Required)* This is the reason for clearing the _Hold._ The choices here are configurable. If you need to add or remove an option, contact your Site Administrators for help.
- Cleared By := *(Required)* This is the officer clearing the _Hold_ (or the officer authorizing the clearing, if different).
- Explanation := *(Required)* Clearing a _Hold_ requires the officer to enter a short explanation in addition to setting the reason above.
- Agency := Usually only entered in the event of a "_Transfer_":transfer.html, this is the “other” agency.
- Transfer Officer := Usually only entered in the event of a "_Transfer_":transfer.html, this is the officer involved for the “other” agency.
- Remarks := This is a standard "&quot;Remarks&quot;":common.html#remarks field.

h3. 72 Hour Hold Clearing

For “72 Hour” _Holds,_ the clearing process displays the “72 Hour” clearing page (actually a part of the "_Arrest_":arrest.html form) instead of the standard "Clear Hold" page. The fields available are:

- Judge := *(Required)* This is the Judge setting the bond information. Judges are set in the "Options" database. If the appropriate Judge does not show up in this select control, contact your Site Administrators for help.
- 72 Hour := *(Required)* Enter the date and time the “72 Hour” form was signed by the Judge.
- Attorney := Enter the name of the attorney if one was appointed by the Judge on the “72  Hour” form.
- Bondable? := Check this if bond was set. Leave it unchecked if bond was denied.
- Bond Conditions := Enter any special bond conditions set by the Judge (other than the bond amount).
- Bond Amount := Enter the amount of the bond set by the Judge. Leave set to zero if bond was denied.

h3. Bondable Hold Clearing

For “Bondable” _Holds,_ the clearing process involves adding sufficient "_Bonds_":bond.html to total the amount specified by the Judge.

h2. <a id='adjustment'>Hold Adjustment</a>

“Serving Sentence” and “Fines/Costs” _Holds_ sometimes need to have one or more of the hour counters adjusted. This is done by clicking on the _Hold_ in the "Hold List" or the "&quot;Booking Page&quot;":booking.html#page and then clicking the _Adjustment_ button on the "Hold Page".

On the "Adjustment Page", the _Hold_ information is displayed at the top. Underneath this is the “Adjustments” section. Each of the four hour fields (“Total Sentence”, “Hours Served”, “Hours Good Time” and “Time Served Credit”) has their own set of adjustment fields allowing you to add or subtract “Hours”, “Days”, “Months” and/or “Years” from the current value.

Enter the value to be added or subtracted in the appropriate boxes for each hour field and then click the _Preview_ button. This will redisplay the page showing the adjusted value for each of the hour fields. When you have entered the adjustments necessary to correct all of the hour fields, click _Submit._ When the "Hold Page" is once again displayed, double-check the values shown and verify they are correct.

It should be noted that “Months” on the "Adjustment Page" makes no consideration for calendar months... they are always considered to be equal to thirty days. This leads to a slight inconsistency between “months” and “years” because “12 months = 360 days”, while “1 year = 365 days”. You should take this into account when making your adjustment.

<!-- navigation bar -->
