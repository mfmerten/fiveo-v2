h1. Booking

p(warning). WARNING: This page is outdated!

When the jail takes custody of a person, the jailer creates a _Booking_ record. When the person leaves the custody of the jail, the jailer closes (releases) the _Booking_ record. The number of people currently in the custody of the jail can be determined by simply counting the active (open) _Booking_ records. Any particular "_Person_":person.html can only have one open _Booking_ at a time and because of the design of the FiveO program, it must be the last (newest) _Booking_ for that "_Person_":person.html.

An open _Booking_ indicates custody of a "_Person_":person.html. It does not specify why the "_Person_":person.html is in custody. The reason is specified by attaching "_Holds_":hold.html to the _Booking._ "_Holds_":hold.html can attached to the _Booking_ in various ways and for various reasons, but the main thing to remember is that a _Booking_ without active "_Holds_":hold.html is incomplete and should either be closed (prisoner released) or should have the appropriate "_Holds_":hold.html added to it.

h2. <a id='permissions'>Permissions</a>

_Booking_ has its own "_permissions_":common.html#permissions. You need "View Booking" to veiw _Booking_ records, "Update Booking" to add or edit records and "Destroy Booking" to delete records.

In addition, you must have the appropriate level of "_permissions_":common.html#permissions for the desired "_Jail_":jail.html.

bq. In FiveO Version 1, the _Booking_ records for all facilities were lumped together and you had to set a filter to limit your view to one facility.  In Version 2 this has changed. Each "_Jail_":jail.html facility can see only its own _Booking_ records.

h2. <a id='list'>Booking List</a>

Clicking on a "_Jail_":jail.html option (identified by the "_Jail_":jail.html acronym) in the _drop-down menu_ (_Jail_ section) takes you to the "Bookings List". _Bookings_ listed on this page are in inmate name order. The "Bookings List" is somewhat different than normal for the FiveO program in that the _Bookings_ in the list may appear in different colors to signify special conditions:
* Blue: _Bookings_ shown in blue in the list show the inmates currently on “Temporary Release” (they have temporarily been released from custody, for various reasons).
* Red: _Bookings_ shown in red indicate that the program has detected certain problems with the _Booking_ that need immediate attention by the jailers.

There are several buttons available on this page:
* _Show All_ - normally, only the active _Booking_ records are shown on the "Bookings List". Clicking the _Show All_ button will switch to showing *all* _Bookings_ on record.
* _Absents_ - this gives access to the "_Absents_":absent.html feature for this "_Jail_":jail.html.
* _Visitors_ - this displays the "&quot;Visitors List&quot;":visitor.html#list for this "_Jail_":jail.html.
* _Photos_ - When clicked, the _Photos_ button displays the front mugshot for all _Booking_ records selected by the current filter and Active/Show All setting. Clicking one of the displayed mugshots will navigate right to the appropriate "Booking Page".
* _Problems_ - When "problem" _Bookings_ are detected, this button will appear and when clicked will display the _Booking_ "&quot;Problems List&quot":#problems page.
* _Commissary_ - When "&quot;Internal Commissary&quot;":commissary.html#internal has been enabled in "_Site Configuration_":site_configuration.html, this button will appear and when clicked will display the "&quot;Commissaries List&quot;":commissary.html#list page for this "_Jail_":jail.html.
* _&lt;jail&gt; Admin_ - when this button is clicked (assuming you have the "Admin" permission for this "_Jail_":jail.html), you will be taken directly to the appropriate "&quot;Jail Page&quot;":jail.html#page where you can update the configuration for this "_Jail_":jail.html facility.

There are several reports available on the "Bookings List" page. Each of these reports is described in the "&quot;Booking Reports&quot;":#reports section below.

h2. <a id='page'>Booking Page</a>

Clicking a _Booking_ entry on the "Bookings List" displays the "Booking Page". The information about the inmate (and the front mugshot) is shown at the top of this page. This is followed by the “booking information” section showing information from the booking process. Next is the “status section” showing the current status for the booking and a list of all active "_Holds_":hold.html (with a _Show All_ button in case you need to see cleared "_Holds_":hold.html as well). The following list explains each of the "status section" items:
* Current Location: Shows the last known location for the inmate. This is usually “&lt;facility&gt;: &lt;cell&gt;”, but can change if an inmate is temporarily transferred, out on a work crew ("_Absent_":absent.html) or temporarily released.
* Commissary Balance: This is visible only if you have “View Commissary” "_permission_":common.html#permissions and if the "_Site Configuration_":site_configuration.html has been set to enable the "internal commissary":commissary.html#internal system. It shows the current balance for the inmate.
* Scheduled Release: This is the date the inmate is scheduled to be released, if one is known. It is manually entered by the jailer (not calculated by the program).
* Good Time: This indicates whether the inmate is currently receiving “Good Time” and includes a button allowing the jailer to turn this on or off for the inmate. It should be noted that "_DOC_":doc.html sentences do not receive “Good Time” automatically, regardless of this setting.
* Time Log: This shows whether time counting for this _Booking_ is enabled or disabled. When disabled, time is not being counted for this _Booking._ The "_Time Log_":time_log.html is generally disabled when an inmate is placed on “Temporary Release” and is enabled again when they return. Also shown here is the total time recorded for this _Booking._
* Remaining To Serve - this appears whenever the _Booking_ has an active "sentence" "_Hold_":hold.html and shows the calculated time the inmate must server to satisfy all "sentence" type "_Holds_":hold.html.
* Projected Release - this only appears when the _Booking_ has an active "sentence" "_Hold_":hold.html and shows the calculated date that the inmate will be released (not considering any future "Good Time", sentences or "&quot;Hold Adjustments&quot;":hold.html#adjustment. This date is calculated by the program using the "Remaining To Serve" information.
* Holds: The status section shows a list of currently active "_Holds_":hold.html for the _Booking._ There is a _Show All_ button available that switches the list to also show inactive (cleared) "_Holds_":hold.html, which can be useful when troubleshooting problems with the _Booking._
* Medications: This list will show up just below the "_Holds_":hold.html list if there are any "_Medications_":medication.html entered for the inmate.

Following the "status section", any "&quot;Remarks&quot;":common.html#remarks entered for the _Booking_ record will be displayed.

There is a _secondary button bar_ just above the _primary button bar_ that gives access to certain reports:
* _Jail Card_ - This prints the “Jail Card” for the _Booking._
* _Medical Report_ - This button is only available after the first "_Medical_":medical.html screening is entered and produces a report of the screening details.
* _Arraignment_ - This button is only available when the _Booking_ has a “Bondable” "_Hold_":hold.html. It prints the “Arraignment Notice” form.
* _Time Log_ - This button produces the “Time Log” report listing all "_Time Log_":time_log.html entries for the _Booking._

The _primary button bar_ for the "Booking Page" has the following buttons:
* _Edit_ - used to edit existing _Booking_ records.
* _Delete_ - used to delete existing _Booking_ records.
* _Medications_ - This is used to access the "_Medication_":medication.html schedule for this inmate.
* _Transfer_ - This is used to initiate a temporary or permanent "_Transfer_":transfer.html of an inmate.
* _Temp Release_ - An inmate is placed on “Temporary Release” by clicking this button (time counting is stopped during the release period).
* _New Arrest_ - The _New Arrest_ button shown here is the only way to add new "_Arrest_":arrest.html records (new charges against a person).
* _New Hold_ - This button is used to add "&quot;Manual Holds&quot;":hold.html#manual to the _Booking._
* _New Visitor_ - When someone visits an inmate, the visitation should be recorded in the "_Visitors_":visitor.html database using this _New Visitor_ button.
* _New Commis._ - This button only shows up if you have "Update Commissary" "_permission_":common.html#permissions and if "&quot;Internal Commissary&quot;":commissary.html#internal has been enabled in "_Site Configuration_":site_configuration.html. It is used to add a new "_Commissary_":commissary.html transaction for the inmate.

Following the _primary button bar_, additional information is provided in several lists:
* Visitation Log - This list shows all of the people that have visited this inmate. Current visitors are shown in red text and will have a _Sign Out_ button for convenience.
* Absents Log - When an inmate participates in an "_Absent_":absent.html, it shows up in this list.
* Booking Time Log - This list shows the last five entries from the "_Time Log_":time_log.html. To view all entries, use the _Time Log_ report button mentioned above.
* Commissary Log - The “Commissary Log” only appears if you have “View Commissary” "_permission_":common.html#permissions and the "&quot;Internal Commissary&quot;":commissary.html#internal feature has been enabled in "_Site Configuration_":site_configuration.html. This log list shows all commissary activity for the inmate over the past thirty days.

h2. <a id='entry'>Booking Entry</a>

When adding or editing a _Booking_ entry, the form displayed on the screen has the following data fields:

- Person := This is the "_Person_":person.html being _Booked_. It cannot be changed.
- Mother := This is the "Mother" associate for the _Booked_ "_Person_":person.html (you must add a "Known Associate" with a relationship of "Mother" for it to show up here).  You cannot change this on the "Booking Page".
- Father := This is the "Father" associate for the _Booked_ "_Person_":person.html (you must add a "Known Associate" with a relationship of "Father" for it to show up here).  You cannot change this on the "Booking Page".
- Emerg. Contact := This is the "Emergency Contact" as recorded on the "_Person_":person.html record.  It cannot be changed here.
- Attorney := This is for recording the name of the inmate's Attorney if known.
- Booked := *(Required)* This is the date and time of _Booking._
- Phone Called := When a prisoner makes a phone call during booking, the number called should be entered here.
- Cash Taken := If cash is taken from the prisoner during the booking process, the amount is to be entered here and the cash receipt given to them should be recorded in the "Receipt Number" box below.
- Receipt Number := This is the serial number of the cash receipt given to the prisoner at booking time when cash is taken from them.
- Booking Officer := *(Required)* The officer performing the booking process should be entered here. If at all possible, the officer should be selected from the officer select control, but may be written in manually if absolutely necessary. In reality, anyone serving as a booking officer should have a properly configured "Officer" "_Contact_":contact.html so that he/she appears in the officer select control on this form.
- &lt;jail&gt; Cell := *(Required)* The "Cell" where the prisoner is to be housed is selected here. If the proper entries do not show up in these select controls, contact your Jail Administrator or Site Administrator to have them added to the "_Jail_":jail.html configuration.
- Scheduled Release := Enter this date if a scheduled release date has been determined for the prisoner. Otherwise leave it blank.
- Flags := Any of these check boxes should be checked if they apply:

** Intoxicated - this box should be checked if the prisoner being booked is intoxicated.
** Entered In AFIS - this box should be checked if the prisoner is entered into the AFIS system during booking.
** Receive Good Time - indicates whether the inmate is eligible to receive "Good Time". This defaults to checked, but this default can be changed by the Jail Administrator or Site Administrator in the "_Jail_":jail.html configuration. The checkbox on this form only sets the initial value of the “Good Time” option. It can be changed later at any time by clicking a button on the "&quot;Booking Page&quot;":booking.html#page.

- Property := When items of personal property are taken from a prisoner during the booking process, they should be recorded in the "Property" field(s). These fields are dynamic, meaning that you can add as many lines as you wish, and you should record each individual piece of property on a separate line. The "Property" recorded here is listed on the "Jail Card" when it is printed. Accurate recording of property can help prevent accusations of theft at a later time. To add a blank property line, click the "add" icon (!add.png!). To remove a property line, click the "delete" icon (!delete.png!) for that line.
- Remarks := This is a standard "&quot;Remarks&quot;":common.html#remarks field.

h2. <a id='reports'>Booking Reports</a>

The "Bookings List" page has several available report options:

- Filtered List := This report will list all _Booking_ records selected by the currently set filter and Active/Show All settings.
- In-Jail := This report lists all inmates currently in jail along with their current status, "_Charges_":charge.html and any special "_Holds_":hold.html. It is intended as a monthly report for the Judge. To generate this report, click the _PDF_ button. The report will include only the _Bookings_ for this "_Jail_":jail.html.
- Release Report := This report details all inmate releases for this "_Jail_":jail.html during the selected date range. Enter the appropriate "Start" and "Stop" dates, and click the _PDF_ button.
- S.S.A. Report := The SSA report shows inmates who were booked during a particular month and were still in jail at the end of that month. This is a monthly report for the ??Social Security Administration??. To generate the SSA report, select a report date and click the _PDF_ button. The report that is generated will be for the month prior to the report date (in other words, if the report date is in July, the report will cover the month of June). It will include only the inmates for this "_Jail_":jail.html.
- F.C.S. Report := This report shows inmates who were booked during a particular month. This is a monthly report intended for the office of ??Fraud and Child Support??. To generate the FCS report, select a report date and click the _PDF_ button. The report that is generated will be for the month prior to the report date (if the report date is in July, the month of June will be used as the reporting period). it will include only the inmates for this "_Jail_":jail.html.
- Head Count := This report is intended to be run nightly (after midnight) and lists all current inmates in this "_Jail_":jail.html facility (as of midnight the previous day). To print the “Nightly Head Count Report”, select the date for the headcount and the “Facility” and click _PDF._ Since the head count is taken at midnight (actually at 23:59), you cannot print a head count for today. However it can be for any day up to and including yesterday.
- Sales Sheets := This report generates the "_Commissary_":commissary.html#internal sales sheets. It will not appear in this list if the "&quot;Internal Commissary&quot;":commissary.html#internal feature is disabled in "_Site Configuration_":site_configuration.html.
- Billing Report := This report is intended to be generated monthly and includes a headcount for every day during the month for this "_Jail_":jail.html along with the inmate classifications for each day. It is used by the ??Civil Department?? when preparing billing statements for inmate housing. To generate the “Monthly Billing Report”, select the desired month and year, and click the _PDF_ button. You should be aware that this requires a lot of processing and may take a long time to produce the report, so be patient.

h2. <a id='problems'>Problems List</a>

The "Bookings List" will, from time to time, include a _Problems_ button which when clicked will display the “Problems List”. _Bookings_ show up in the “Problems List” for several reasons.

bq. Note that there is a "_User_":user.html preference setting that allows individuals to elect to receive a notification "_Message_":message.html about _Booking_ problems.  These messages are sent periodically on a schedule that is determined by individual "_Jail_":jail.html facility configuration settings. These messages are sent only when a facility has _Booking_ problems that need attention.

h3. No Active Holds

A _Booking_ must have active "_Holds_":hold.html if it is to remain open. When a _Booking_ appears in the “Problems List” because of this, the jailer has two options (we do not count “ignore it” as an option because ignoring _Bookings_ with problems is not an option for a jailer):
# If the inmate is no longer in jail, the jailer should close the _Booking_ record (by clicking the _Release_ button).
# If the inmate is still in jail (and not in the process of being released), the jailer must add appropriate "_Holds_":hold.html to the _Booking._

h3. Expired Holds

A _Booking_ may appear in the “Problems List” if one or more "_Holds_":hold.html have “expired”. There are currently three types of "_Holds_":hold.html that expire:

- 48 Hour := These Holds expire 48 hours after they are created. That is the time limit allowed by ??Louisiana Code of Criminal Procedure?? Article 230.2 for a justice to determine probable cause for an arrest without a warrant. If probable cause has not been determined within 48 hours, the detained person must be released on their own recognizance. Jailers need to consider the following points when dealing with “48 Hour” Holds:

** The 48 hours specified by law concerns the time between the actual arrest and the time the Judge determines probable cause. The “48 Hour” "_Hold_":hold.html in the FiveO program is not the authority to release the prisoner when it expires. This "_Hold_":hold.html is merely a reminder to the jailers that paperwork needs to be done.
** The "48 Hour" "_Hold_":hold.html in FiveO Version 2 is a "&quot;Manual Hold&quot;":hold.html#manual  and will not appear unless a jailer decides to add it to the _Booking._

- 72 Hour := These "_Holds_":hold.html expire 72 hours after they are created. This is the time limit allowed by ??Louisiana Code of Criminal Procedure?? Article 230.1 for appointment of counsel and determination of bond. Jailers should keep in mind that the “72 Hour” "_Hold_":hold.html in FiveO is merely a reminder that the paperwork needs to be done. It is not the authority to release the prisoner when it expires.
- Serving Sentence := These "_Holds_":hold.html expire after the sentenced length of time has elapsed. Before closing an expired “Serving Sentence” "_Hold_":hold.html, and possibly releasing the prisoner, the jailer should review the inmate's records to verify that he/she has actually served the required sentence. If the hours shown on a “Serving Sentence” "_Hold_":hold.html are incorrect, they should be fixed using a "&quot;Hold Adjustment&quot;":hold.html#adjustment. The jailer should keep in mind that expiration of a “Serving Sentence” "_Hold_":hold.html is not the authority to release a prisoner. It is merely a reminder that the paperwork needs to be done.

h3. Missing DOC Information

When "_DOC_":doc.html records are created by "&quot;DOC Transfer (In)&quot;":transfer.html#types or by "_Sentencing_":sentence.html, they are not complete. The jailer needs to update these records with information obtained from official ??Department of Corrections?? paperwork. In the real world, receipt of the DOC paperwork occurs some time (days or weeks) after the prisoner is transferred or convicted, so this information is not required initially. However, the _Booking_ will remain on the “Problems List” until the "_DOC_":doc.html record is completed.

<!-- navigation bar -->
