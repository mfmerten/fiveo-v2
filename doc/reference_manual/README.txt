This manual is written in Textile format, using the RedCloth gem. The Textile
syntax as implemented in RedCloth can be found at:

  http://redcloth.org

Textmate is our preferred editor for writing the manual files. It comes with
a Textile bundle pre-installed. Textmate is only available for the Mac.  If
your development platform is not a Mac, you are on your own.

The manual files are organized as:

doc/
  reference_manual/
    README.txt - this file
    template.txt - copy this to start a new page
    html/
      manual.css - stylesheet for the manual
      *.html - these are the generated HTML manual files
      fdl-1.3-standalone.html - this is the standalone HTML version of the
            GNU Free Documentation License (unmodified) obtained directly
            from http://www.gnu.org/copyleft/fdl.html
    textile/
      *.textile - these are the source files for the manual
public/
  manual/ -> ../doc/reference_manual/html/
    index.html - the start of the reference manual

With this setup, it is not required to log in to access the manual.  Simply
type in your normal server URL with "/manual" added.

When modifying the manual source files, do not remove or modify the
   <!-- navigation bar -->
line. The script used to generate the HTML pages looks for this in the source
files and replaces it with the proper navigation links for each page. The
navigation links can be placed anywhere on the page, in as many places as
desired.  Current document pages have a single instance of the navigation
links at the bottom of the page.

To add an additional page, create the file with the desired name and the
.textile extension in the textile/ directory.  If the name of the file does
not translate properly to the name of the page (using titleize), you will
need to modify the conversion script.  Look for the "proper_names" hash in the
get_proper_name method and add your file. The hash already contains a few
entries that do not convert due to capitalization, punctuation or name so use
existing entries as an example.  Your new file will be placed into the
navigation sequence in alphabetical order.  You will have to add it manually
to the toc.textile file in the proper place because that is not generated
automatically.

If you remove a .textile page from textile/, you will need to also remove the
corresponding .html page from the html/ directory.  The update script
does not do any housekeeping there.

To generate a new version of the HTML manual after modifying or adding a
.textile file, run the "app:update_manual" rake task.

Michael Merten <mikemerten@suddenlink.net>

