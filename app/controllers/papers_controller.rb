class PapersController < ApplicationController
  
  def index
    options = {
      active: true,
      scopes: [:by_reverse_id],
      paginate: true
    }
    @papers = get_records(options)
    
    if can?(:update_civil)
      @links.push(['New', new_paper_path])
    end
    @links.push(['Customers', paper_customers_path])
    @links.push(['Paper Types', paper_types_path])
    
    @payments = PaperPayment.unposted
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @paper = Paper.find(params[:id])
    @page_title = "Paper #{@paper.id}"
    
    if can?(:update_civil)
      @links.push(['New', new_paper_path])
      unless @paper.invoice_datetime?
        @links.push(['Edit', edit_paper_path(@paper)])
      end
    end
    if can?(:destroy_civil) && !@paper.invoice_datetime?
      @links.push(['Delete', @paper, {method: :delete, data: {confirm: confirm_msg_for(:paper)}}])
    end
    @links.push(['Info Sheet', info_sheet_paper_path(id: @paper.id), pdf: true])
    if can?(:update_civil)
      if @paper.invoice_datetime?
        @links.push(['Invoice', invoice_paper_path(id: @paper.id), pdf: true])
        if @paper.can_uninvoice?
          @links.push(['Uninvoice', uninvoice_paper_path(id: @paper.id), {method: :post, data: {confirm: 'This will delete all payment transactions for this paper. Are you sure?'}}])
        end
        if @paper.paid_date? && @paper.paid_in_full?
          @links.push(['Receipt', invoice_paper_path(id: @paper.id, receipt: 1), pdf: true])
        else
          @links.push(['Payment', new_paper_customer_paper_payment_path(@paper.customer, paper_id: @paper.id)])
        end
      else
        unless @paper.service_type.blank?
          if (@paper.service_type == 'No Service' && @paper.returned_date? and !@paper.noservice_type.blank?) || (@paper.served_datetime? && @paper.served_to?)
            @links.push(['Invoice', invoice_paper_path(id: @paper.id), {method: :post}])
          end
        end
      end
    end
    
    @page_links.push([@paper.customer,'customer'],[@paper.creator,'creator'],[@paper.updater,'updater'])
  end
  
  def new
    can!(:update_civil)

    @paper = Paper.new
    @page_title = "New Paper"
    @paper.state = SiteConfig.agency_state
  end
  
  def edit
    can!(:update_civil)

    @paper = Paper.find(params[:id])

    if @paper.invoice_datetime?
      flash.alert = "Already Invoiced! Delete Invoice Transaction First!"
      redirect_to @paper and return
    end
    
    @page_title = "Edit Paper #{@paper.id}"
    @paper.officer_id = nil
  end
  
  def create
    can!(:update_civil)

    redirect_to papers_url and return if params[:commit] == 'Cancel'

    @paper = Paper.new(paper_params)
    @page_title = "New Paper"
    @paper.created_by = session[:user]
    @paper.updated_by = session[:user]
    
    success = @paper.save
    user_action "Paper ##{@paper.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @paper, notice: 'Paper was successfully created.' }
        format.json { render :show, status: :created, location: @paper }
      else
        format.html { render :new }
        format.json { render json: @paper.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_civil)
    
    @paper = Paper.find(params[:id])
    
    redirect_to @paper and return if params[:commit] == 'Cancel'
    
    if @paper.invoice_datetime?
      flash.alert = "Already Invoiced! Delete Invoice Transaction First!"
      redirect_to @paper and return
    end
    
    @page_title = "Edit Paper #{@paper.id}"
    @paper.updated_by = session[:user]
    
    success = @paper.update(paper_params)
    user_action "Paper ##{@paper.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @paper, notice: 'Paper was successfully updated.' }
        format.json { render :show, status: :ok, location: @paper }
      else
        format.html { render :edit }
        format.json { render json: @paper.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_civil)
    
    @paper = Paper.find(params[:id])
    
    if reasons = @paper.reasons_not_destroyable
      flash.alert = "Cannot delete: #{reasons}"
      redirect_to @paper and return
    end
    
    @paper.destroy
    user_action "Paper ##{@paper.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to papers_url, notice: 'Paper was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def invoice
    if request.post?
      can!(:update_civil)
      
      if params[:customer_id]
        @customer = PaperCustomer.find(params[:customer_id])
        @papers = Paper.belongs_to_customer(@customer.id).billable
      else
        paper = Paper.find(params[:id])
        
        if paper.invoice_datetime?
          flash.alert = 'Paper already invoiced!'
          redirect_to paper and return
        end
        
        @papers = [paper]
      end
      
      if @papers.nil? || @papers.empty?
        flash.alert = 'Nothing to report!'
        redirect_to papers_url and return
      end
      
      # this checks to ensure that the paper customer for each invoice passes
      # validations or the invoice transaction will fail
      @papers.each do |p|
        unless p.customer.valid?
          # customer fails validations
          flash.alert = "Customer for Paper ID #{p.id} must be edited to fix invalid information!"
          redirect_to p.customer and return
        end
      end
      
      invoice_datetime = Time.now
      
      # update necessary invoice information for each of the papers
      @papers.each do |p|
        p.invoice_datetime = invoice_datetime
        if p.mileage_only?
          p.invoice_total = BigDecimal.new("0.0",2)
        else
          if p.served_datetime?
            p.invoice_total = p.service_fee
          else
            p.invoice_total = p.noservice_fee
            p.service_type = "No Service"
            p.noservice_type = "Unable to Locate"
          end
        end
        p.invoice_total += p.mileage * p.mileage_fee
        p.save(validate: false)
        
        trans = PaperPayment.new(
          paper_customer_id: p.paper_customer_id,
          paper_id: p.id,
          transaction_date: invoice_datetime.to_date,
          officer: session[:user_name],
          officer_unit: session[:user_unit],
          officer_badge: session[:user_badge],
          charge: p.invoice_total,
          memo: "Invoicing Paper ID: #{p.id}",
          created_by: session[:user],
          updated_by: session[:user]
        )
        
        unless trans.save(validate: false)
          # something bad happened with the transactaion, clear
          # invoice information so it can be fixed
          p.invoice_total = 0
          p.invoice_datetime = nil
          unless p.remarks?
            p.remarks = ""
          end
          p.remarks << "WARNING: Invoice transaction failed!"
          p.save(validate: false)
          # remove paper from papers list
          @papers.delete(p)
        end
      end
    else
      paper = Paper.find(params[:id])
      
      unless paper.invoice_datetime?
        flash.alert = 'Paper Not Invoiced - cannot reprint!'
        redirect_to paper and return
      end
      
      # since this is a single paper, need to set @customer nil
      @customer = nil
      @papers = [paper]
      if params[:receipt] && paper.paid_date?
        @receipt = true
      end
    end
    
    # now generate invoices for all of the @papers
    if pdf_url = make_pdf_file("invoice", Papers::Invoice.new(@papers, @receipt))
      redirect_to pdf_url
      user_action("Printed Invoice(s).")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to papers_url
    end
  end
  
  def uninvoice
    can!(:update_civil)
    
    @paper = Paper.find(params[:id])
    
    unless @paper.can_uninvoice?
      flash.alert = "Cannot Uninvoice... has posted transactions!"
      redirect_to @paper and return
    end
    
    if @paper.uninvoice
      flash.notice = "Paper Uninvoiced."
      user_action("Uninvoiced Paper #{@paper.id}.")
    else
      flash.alert = "Uninvoice Failed!"
    end
    redirect_to @paper
  end
  
  def autofill_serve_to
    @name = ''
    @defendant = "#{params[:defendant]}"
    @street = ''
    @city = ''
    @state = ''
    @zip = ''
    @phone = ''
    @mileage = '0.0'
    @prior = ''
    if can?(:view_civil)
      if params[:name] && paper = Paper.find_by_name(params[:name])
        @street = "#{paper.street}"
        @city = "#{paper.city}"
        @state = "#{paper.state}"
        @zip = "#{paper.zip}"
        @phone = "#{paper.phone}"
        @mileage = "#{paper.mileage}"
        if @mileage.blank?
          @mileage = "0.0"
        end
        unless paper.noservice_type.blank?
          @prior = "<span class='red'>Prior Attempt: #{paper.noservice_type} #{paper.noservice_extra}</span>"
        end
      end
      if @defendant.blank?
        @defendant = "#{params[:name]}"
      end
    else
      @name = "Permission Denied!"
    end
    
    respond_to :js
  end
  
  def clear_prior
    respond_to :js
  end
  
  def autofill_suit_info
    @plaintiff = ''
    @defendant = ''
    @customer_id = ''
    if can?(:view_civil)
      if paper = Paper.find_by_suit(params[:suit])
        @plaintiff = paper.plaintiff
        @defendant = paper.defendant
        @customer_id = paper.paper_customer_id
      end
    else
      @plaintiff = "Permission Denied!"
    end
    
    respond_to :js
  end
  
  def autofill_customer_info
    @noservice = "0.00"
    @mileage = "0.00"
    if customer = PaperCustomer.find_by_id(params[:customer_id])
      @noservice = number_to_currency(customer.noservice_fee, format: "%n")
      @mileage = number_to_currency(customer.mileage_fee, format: "%n")
    end
    
    respond_to :js
  end
  
  def autofill_paper_info
    @service = "0.00"
    if ptype = PaperType.find_by_id(params[:paper_type_id])
      @service = number_to_currency(ptype.cost, format: "%n")
    end
    
    respond_to :js
  end
      
  def autofill_served_to
    @name = ""
    @valid = false
    if params[:service_type] == 'Personal' && params[:paper][:serve_to]
      @name = params[:paper][:serve_to]
      @valid = true
    end
    
    respond_to :js
  end
  
  def info_sheet
    @paper = Paper.find(params[:id])
    if pdf_url = make_pdf_file("info_sheet", Papers::InfoSheet.new(@paper))
      redirect_to pdf_url
      user_action("Printed Information Sheet.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to @paper
    end
  end

  def service_status
    @court_date = AppUtils.valid_date(params[:court_date])
    
    if @court_date.nil?
      flash.alert = "Invalid Court Date"
      redirect_to papers_url and return
    end
    
    @papers = Paper.where({court_date: @court_date}).all
    
    if @papers.empty?
      flash.alert = "Nothing to Report!"
      redirect_to papers_url and return
    end
    
    if pdf_url = make_pdf_file("status", Papers::ServiceStatus.new(@court_date, @papers))
      redirect_to pdf_url
      user_action("Printed Service Status Report.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to papers_url
    end
  end

  def history
    @customer = PaperCustomer.find(params[:customer_id])
    @from_date = AppUtils.valid_date(params[:from_date])
    @to_date = AppUtils.valid_date(params[:to_date])
    conditions = "paper_customer_id = '#{@customer.id}' AND payment IS NOT NULL AND payment > '0'"
    unless @from_date.nil?
      conditions << " AND transaction_date >= '#{@from_date.to_s(:db)}'"
    end
    unless @to_date.nil?
      conditions << " AND transaction_date <= '#{@to_date.to_s(:db)}'"
    end
    @payments = PaperPayment.where(conditions).all
    
    if @payments.empty?
      flash.alert = 'Nothing to report!'
      redirect_to papers_url and return
    end
    
    if pdf_url = make_pdf_file("history", Papers::PaymentHistory.new(@customer, @from_date, @to_date, @payments))
      redirect_to pdf_url
      user_action("Printed Payment History.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to papers_url
    end
  end

  def billing_status
    @status = params[:status]
    unless ['billable','payable'].include?(@status)
      @status = 'billable'
    end
    
    @customer = PaperCustomer.find_by_id(params[:customer_id])
    if @status == 'billable'
      if @customer.nil?
        @papers = Paper.billable
      else
        @papers = Paper.belongs_to_customer(@customer.id).billable
      end
    else
      if @customer.nil?
        @papers = Paper.payable
      else
        @papers = Paper.belongs_to_customer(@customer.id).payable
      end
    end
    
    if @papers.empty?
      flash.alert = "Nothing To Report!"
      redirect_to papers_url and return
    end
    
    if pdf_url = make_pdf_file("billing_status", Papers::BillingStatus.new(@status, @papers))
      redirect_to pdf_url
      user_action("Printed Billing Status Report.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to papers_url
    end
  end

  def posting_report
    @status = params[:status]
    
    if @status == 'post'
      can!(:update_civil)
    end
    
    unless ['post','preview','reprint'].include?(@status)
      @status = 'preview'
    end
    
    if @status == 'reprint'
      unless report_datetime = AppUtils.valid_datetime(params[:post_datetime])
        flash.alert = "Valid Posting Date required for Reprints"
        redirect_to papers_url and return
      end
      
      @payments = PaperPayment.by_date.posted_on(report_datetime).includes(:customer)
      @report_time = report_datetime
    else
      @payments = PaperPayment.by_date.unposted.includes(:customer)
      @report_time = Time.now
    end
    
    if @payments.empty?
      flash.alert = "Nothing to Report!"
      redirect_to papers_url and return
    end
    
    if pdf_url = make_pdf_file("posting", Papers::PostingReport.new(@status,@report_time,@payments))
      redirect_to pdf_url
      if @status == 'post' || @status == 'reprint'
        user_action("#{params[:status] == 'reprint' ? 'Reprinted' : 'Printed'} Paper Posting Report.")
      else
        user_action("Printed Paper Posting Preview Report.")
      end
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to papers_url
    end
  end
  
  private
  
  def paper_params
    params.require(:paper).permit(:suit_no, :paper_type_id, :received_date, :court_date, :plaintiff, :defendant, :serve_to, :street,
      :city, :state, :zip, :phone, :service_type, :served_datetime, :served_to, :returned_date, :noservice_type, :noservice_extra,
      :officer_id, :officer_badge, :officer_unit, :officer, :mileage, :paper_customer_id, :billable, :mileage_only, :service_fee,
      :noservice_fee, :mileage_fee, :comments, :remarks
    )
  end
end
