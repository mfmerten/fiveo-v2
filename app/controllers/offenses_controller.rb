class OffensesController < ApplicationController

  def index
    options = {
      scopes: [:by_date],
      includes: [:offense_photos, :victims, :witnesses, :suspects, :wreckers, :call],
      paginate: true
    }
    @offenses = get_records(options)
        
    if can?(:update_offense)
      @links.push(['New', new_offense_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    if params[:info_id]
      @info_id = params[:info_id]
      @offense = Offense.find_by_id(params[:id])
    else
      @offense = Offense.find(params[:id])
      @page_title = "Offense #{@offense.id}"
      
      if can?(:update_offense)
        @links.push(['New', new_offense_path])
        if check_author(@offense)
          @links.push(['Edit', edit_offense_path(id: @offense.id)])
        end
      end
      if can?(:destroy_offense) && check_author(@offense)
        @links.push(['Delete', @offense, {method: 'delete', data: {confirm: confirm_msg_for(:offense)}}])
      end
      if can?(:update_offense) && check_author(@offense)
        @links.push(['New Photo', new_offense_offense_photo_path(@offense)])
        @links.push(['Batch Photos', upload_photos_offense_path(@offense)])
      end
      @links.push(['Offense Report', offense_report_offense_path(@offense), pdf: true])
      if can?(:update_citation) && check_author(@offense)
        @links.push(['New Citation', new_citation_path(offense_id: @offense.id)])
      end
      if can?(:update_evidence) && check_author(@offense)
        @links.push(['New Evidence', new_evidence_path(offense_id: @offense.id)])
      end
      if can?(:update_forbid) && check_author(@offense)
        @links.push(['New Forbid', new_forbid_path(offense_id: @offense.id)])
      end

      @page_links.push(@offense.call,[@offense.creator,'creator'],[@offense.updater,'updater'])
    end
    
    respond_to :html, :json, :js
  end
  
  def new
    can!(:update_offense)

    @offense = Offense.new
    @offense.call_id = params[:call_id]
    set_officer_to_session(@offense, :officer)
    @offense.offense_datetime = Time.now
    @page_title = 'New Offense Report'
    unless @offense.call.nil?
      if @offense.call.arrival_datetime?
        @offense.offense_datetime = @offense.call.arrival_datetime
      else
        @offense.offense_datetime = @offense.call.call_datetime
      end
    end
  end
    
  def edit
    can!(:update_offense)

    @offense = Offense.find(params[:id])

    enforce_author(@offense)

    @offense.officer_id = nil
    @page_title = "Edit Offense #{@offense.id}"
  end
  
  def create
    redirect_to offenses_url and return if params[:commit] == 'Cancel'

    can!(:update_offense)

    @offense = Offense.new(offense_params)
    @offense.created_by = session[:user]
    @offense.updated_by = session[:user]
    @page_title = 'New Offense Report'

    success = @offense.save
    user_action "Offense ##{@offense.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @offense, notice: 'Offense was successfully created.' }
        format.json { render :show, status: :created, location: @offense }
      else
        format.html { render :new }
        format.json { render json: @offense.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_offense)
    
    @offense = Offense.find(params[:id])
    
    redirect_to @offense and return if params[:commit] == 'Cancel'
    
    enforce_author(@offense)
    
    @offense.officer_id = nil
    @offense.updated_by = session[:user]
    @offense.legacy = false
    @page_title = "Edit Offense #{@offense.id}"
    
    success = @offense.update(offense_params)
    user_action "Offense ##{@offense.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @offense, notice: 'Offense was successfully updated.' }
        format.json { render :show, status: :ok, location: @offense }
      else
        format.html { render :edit }
        format.json { render json: @offense.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:destroy_offense)
    
    @offense = Offense.find(params[:id])
    
    enforce_author(@offense)
    
    @offense.destroy
    user_action "Offense ##{@offense.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to offenses_url, notice: 'Offense was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def upload_photos
    can!(:update_offense)
    
    @offense = Offense.find(params[:id])
    
    enforce_author(@offense)
    
    @page_title = "Upload Photos For Offense #{@offense.id}"
    @offense.photo_date = Date.today
    @offense.photo_batch = true
    @offense.photo_loc = @offense.location
    @offense.updated_by = session[:user]
    
    if request.post?
      if params[:commit] == 'Cancel'
        redirect_to @offense and return
      end
      
      params[:offense][:new_photos] ||= {}
      @offense.attributes = offense_params

      if @offense.save
        redirect_to @offense
        user_action("Uploaded Photo(s) to Offense #{@offense.id}.")
      end
    end
  end

  def offense_report
    @offense = Offense.find(params[:id])
    
    if pdf_url = make_pdf_file("offense", Offenses::Report.new(@offense))
      redirect_to pdf_url
      user_action("Printed Offense #{@offense.id} Report.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to @offense
    end
  end
  
  private
  
  def offense_params
    params.require(:offense).permit(:offense_datetime, :location, :officer_id, :officer_badge, :officer_unit, :officer, :call_id,
      :details, :pictures, :perm_to_search, :medical_release, :restitution_form, :misd_summons, :victim_notification, :fortyeight_hour,
      :other_documents, :remarks, :photo_date, :photo_taken_by_id, :photo_taken_by_badge, :photo_taken_by_unit, :photo_taken_by,
      :photo_loc, :photo_desc, new_photos: [:crime_scene_photo],
      offense_victims_attributes: [:id, :person_id, :_destroy],
      offense_suspects_attributes: [:id, :person_id, :_destroy],
      wreckers_attributes: [:id, :name, :by_request, :_destroy, :location, :tag, :vin],
      offense_arrests_attributes: [:id, :arrest_id, :_destroy], offense_evidences_attributes: [:id, :evidence_id, :_destroy]
    )
  end
end
