class PawnItemTypesController < ApplicationController
  
  def index
    options = {
      scopes: [:by_type],
      paginate: true
    }
    @pawn_item_types = get_records(options)
    
    if can?(:update_option)
      @links.push(['New', new_pawn_item_type_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @pawn_item_type = PawnItemType.find(params[:id])
    @page_title = "Pawn Item Type #{@pawn_item_type.id}"
    
    if can?(:update_option)
      @links.push(['New', new_pawn_item_type_path])
      @links.push(['Edit', edit_pawn_item_type_path(@pawn_item_type)])
      @links.push(['Delete', @pawn_item_type, {method: :delete, data: {confirm: confirm_msg_for(:pawn_item_type)}}])
    end
    
    @page_links.push([@pawn_item_type.creator,'creator'],[@pawn_item_type.updater,'updater'])
  end
  
  def new
    can!(:update_option)

    @pawn_item_type = PawnItemType.new
    @page_title = "New Pawn Item Type"
  end
  
  def edit
    can!(:update_option)

    @pawn_item_type = PawnItemType.find(params[:id])
    @page_title = "Edit PawnItemType #{@pawn_item_type.id}"
  end
  
  def create
    can!(:update_option)

    redirect_to pawn_item_types_url and return if params[:commit] == 'Cancel'

    @pawn_item_type = PawnItemType.new(pawn_item_type_params)
    @page_title = "New Pawn Item Type"
    @pawn_item_type.created_by = session[:user]
    @pawn_item_type.updated_by = session[:user]

    success = @pawn_item_type.save
    user_action "Pawn Item Type ##{@pawn_item_type.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @pawn_item_type, notice: 'Pawn Item Type was successfully created.' }
        format.json { render :show, status: :created, location: @pawn_item_type }
      else
        format.html { render :new }
        format.json { render json: @pawn_item_type.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_option)

    @pawn_item_type = PawnItemType.find(params[:id])

    redirect_to @pawn_item_type and return if params[:commit] == 'Cancel'

    @page_title = "Edit PawnItemType #{@pawn_item_type.id}"
    @pawn_item_type.updated_by = session[:user]

    success = @pawn_item_type.update(pawn_item_type_params)
    user_action "Pawn Item Type ##{@pawn_item_type.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @pawn_item_type, notice: 'Pawn Item Type was successfully updated.' }
        format.json { render :show, status: :ok, location: @pawn_item_type }
      else
        format.html { render :edit }
        format.json { render json: @pawn_item_type.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:update_option)
    
    @pawn_item_type = PawnItemType.find(params[:id])
    
    @pawn_item_type.destroy
    user_action "Pawn Item Type ##{@pawn_item_type.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to pawn_item_types_url, notice: 'Pawn Item Type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  
  def pawn_item_type_params
    params.require(:pawn_item_type).permit(:item_type)
  end
end