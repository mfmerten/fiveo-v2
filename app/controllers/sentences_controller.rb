class SentencesController < ApplicationController
  before_action :load_docket
  
  # no index
  
  def show
    @sentence = @court.sentences.find(params[:id])
    @page_title = "Sentence #{@sentence.id}"
    
    if can?(:update_court)
      @links.push(['Edit', edit_docket_court_sentence_path(@docket, @court, @sentence)])
    end
    if can?(:destroy_court)
      @links.push(['Delete', [@docket, @court, @sentence], {method: :delete, data: {confirm: confirm_msg_for(:sentence)}}])
    end
    
    @page_links.push(@docket.person)
    @page_links.push(@docket)
    @page_links.push([@docket, @court])
    @page_links.push(@sentence.doc_record)
    unless @sentence.hold.nil?
      @page_links.push([@sentence.hold.jail, @sentence.hold.booking, @sentence.hold])
    end
    @page_links.push(@sentence.probation,[@sentence.creator,'creator'],[@sentence.updater,'updater'])
  end
  
  def new
    can!(:update_court)
    
    @da_charge = @docket.da_charges.find(params[:da_charge_id])
    
    @sentence = @court.sentences.build(docket_id: @docket.id, da_charge_id: @da_charge.id, conviction: @da_charge.charge, conviction_count: @da_charge.count, court_cost_amount: nil)
    @page_title = "New Sentence"
  end
  
  def edit
    can!(:update_court)

    @sentence = @court.sentences.find(params[:id])
    @page_title = "Edit Sentence #{@sentence.id}"
    @sentence.court_cost_amount = nil
    @sentence.consecutives = @sentence.consecutive_dockets.collect{|d| d.docket_no}.join(',')
  end
  
  def create
    can!(:update_court)

    redirect_to [@docket, @court] and return if params[:commit] == 'Cancel'

    @sentence = @court.sentences.build(sentence_params)
    @page_title = "New Sentence"
    @sentence.created_by = session[:user]
    @sentence.updated_by = session[:user]
    
    success = @sentence.save
    user_action "Sentence ##{@sentence.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@docket, @court, @sentence], notice: 'Sentence was successfully created.' }
        format.json { render :show, status: :created, location: [@docket, @court, @sentence] }
      else
        format.html { render :new }
        format.json { render json: @sentence.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_court)
    
    @sentence = @court.sentences.find(params[:id])
    
    redirect_to [@docket, @court, @sentence] and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Sentence #{@sentence.id}"
    @sentence.updated_by = session[:user]
    
    success = @sentence.update(sentence_params)
    user_action "Sentence ##{@sentence.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@docket, @court, @sentence], notice: 'Sentence was successfully updated.' }
        format.json { render :show, status: :ok, location: [@docket, @court, @sentence] }
      else
        format.html { render :edit }
        format.json { render json: @sentence.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:destroy_court)
    
    @sentence = @court.sentences.find(params[:id])
    
    @sentence.destroy
    user_action "Sentence ##{@sentence.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to [@docket, @court], notice: 'Sentence was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def force_processing
    can!(:update_court)
    
    @sentence = @court.sentences.find(params[:id])
    @sentence.force = true
    @sentence.save(validate: false)
    user_action("Forced processing for Sentence #{@sentence.id}.")
    redirect_to [@docket, @court, @sentence]
  end
  
  protected
  
  def load_docket
    @docket = Docket.find(params[:docket_id])
    @court = @docket.courts.find(params[:court_id])
  end
  
  def sentence_params
    params.require(:sentence).permit(:result, :conviction_count, :conviction, :doc, :fines, :court_cost_amount, :costs, :pay_by_date,
      :pay_during_prob, :default_hours, :default_days, :default_months, :default_years, :sentence_hours, :sentence_days, :sentence_months,
      :sentence_years, :sentence_life, :sentence_death, :parish_jail, :suspended, :suspended_except_hours, :suspended_except_days,
      :suspended_except_months, :suspended_except_years, :suspended_except_life, :probation_type, :probation_hours, :probation_days,
      :probation_months, :probation_years, :probation_reverts, :probation_revert_conditions, :delay_execution, :deny_goodtime,
      :credit_time_served, :wo_benefit, :community_service_days, :community_service_hours, :substance_abuse_program, :driver_improvement,
      :probation_fees, :idf_amount, :dare_amount, :restitution_amount, :restitution_date, :anger_management, :substance_abuse_treatment,
      :random_drug_screens, :no_victim_contact, :art_893, :art_894, :art_895, :sentence_consecutive, :sentence_notes, :fines_consecutive,
      :fines_notes, :costs_consecutive, :costs_notes, :consecutive_type, :consecutives, :notes, :remarks, :docket_id, :court_id,
      :da_charge_id, :process_status
    )
  end
end
