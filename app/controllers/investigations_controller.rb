class InvestigationsController < ApplicationController
  
  def index
    options = {
      active: true,
      scopes: [:by_reverse_id],
      includes: [:notes, {owner: :contact}],
      paginate: true
    }
    @investigations = get_records(options)
    
    if can?(:update_investigation)
      @links.push(['New', new_investigation_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @investigation = Investigation.find(params[:id])
    @page_title = "Investigation #{@investigation.id}"
    
    enforce_privacy(@investigation)
    
    if can?(:update_investigation)
      @links.push(['New', new_investigation_path])
      if check_author(@investigation)
        @links.push(['Edit', edit_investigation_path(@investigation)])
      end
    end
    if can?(:destroy_investigation) && check_author(@investigation)
      @links.push(['Delete', @investigation, {method: 'delete', data: {confirm: confirm_msg_for(:investigation)}}])
    end
    if can?(:update_investigation) && check_author(@investigation)
      @links.push(['New Contact', new_contact_path(investigation_id: @investigation.id)])
      @links.push(['New Crime', new_investigation_invest_crime_path(@investigation)])
      @links.push(['New Criminal', new_investigation_invest_criminal_path(@investigation)])
      @links.push(['New Event', new_event_path(investigation_id: @investigation.id)])
      @links.push(['New Evidence', new_evidence_path(investigation_id: @investigation.id)])
      @links.push(['New Photo', new_investigation_invest_photo_path(@investigation)])
      @links.push(['Batch Photos', upload_photos_investigation_path(@investigation)])
      @links.push(['New Report', new_investigation_invest_report_path(@investigation)])
      @links.push(['New Vehicle', new_investigation_invest_vehicle_path(@investigation)])
      @links.push(['New Warrant', new_warrant_path(investigation_id: @investigation.id)])
      @links.push(['New Stolen', new_stolen_path(investigation_id: @investigation.id)])
    end
    
    @page_links.push([@investigation.creator, 'creator'],[@investigation.updater, 'updater'])
  end

  def new
    can!(:update_investigation)

    @investigation = Investigation.new(investigation_datetime: Time.now, owned_by: session[:user], status: 'Active', private: true)
    @page_title = "New Investigation"
  end

  def edit
    can!(:update_investigation)

    @investigation = Investigation.find(params[:id])

    enforce_privacy(@investigation)
    enforce_author(@investigation)

    @page_title = "Edit Investigation #{@investigation.id}"
  end
  
  def create
    redirect_to investigations_url and return if params[:commit] == 'Cancel'

    can!(:update_investigation)

    @investigation = Investigation.new(investigation_params)
    @page_title = "New Investigation"
    @investigation.created_by = session[:user]
    @investigation.updated_by = session[:user]
    
    success = @investigation.save
    user_action "Investigation ##{@investigation.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @investigation, notice: 'Investigation was successfully created.' }
        format.json { render :show, status: :created, location: @investigation }
      else
        format.html { render :new }
        format.json { render json: @investigation.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_investigation)

    @investigation = Investigation.find(params[:id])

    enforce_privacy(@investigation)
    enforce_author(@investigation)

    @page_title = "Edit Investigation #{@investigation.id}"
    @investigation.updated_by = session[:user]

    success = @investigation.update(investigation_params)
    user_action "Investigation ##{@investigation.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @investigation, notice: 'Investigation was successfully updated.' }
        format.json { render :show, status: :ok, location: @investigation }
      else
        format.html { render :edit }
        format.json { render json: @investigation.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_investigation)
    
    @investigation = Investigation.find(params[:id])
    
    enforce_privacy(@investigation)
    enforce_author(@investigation)
    
    @investigation.destroy
    user_action "Investigation ##{@investigation.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to investigations_url, notice: 'Investigation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def upload_photos
    can!(:update_investigation)
    
    @investigation = Investigation.find(params[:id])
    
    enforce_author(@investigation)
    enforce_privacy(@investigation)
    
    @page_title = "Upload Photos For Investigation #{@investigation.id}"
    @investigation.photo_date = Date.today
    @investigation.photo_batch = true
    @investigation.updated_by = session[:user]
    
    if request.post?
      if params[:commit] == 'Cancel'
        redirect_to @investigation and return
      end
      
      params[:investigation][:new_photos] ||= {}
      
      if @investigation.update(investigation_params)
        redirect_to @investigation
        user_action("Uploaded Photo(s) to Investigation #{@investigation.id}.")
      end
    end
  end
  
  private
  
  def investigation_params
    params.require(:investigation).permit(:investigation_datetime, :status, :details, :private, :owned_by, :remarks,
      :photo_date, :photo_taken_by_id, :photo_taken_by_badge, :photo_taken_by_unit, :photo_taken_by,
      :photo_loc, :photo_desc, new_photos: [:invest_photo],
      invest_cases_attributes: [:id, :call_id, :_destroy], invest_offenses_attributes: [:id, :offense_id, :_destroy],
      invest_victims_attributes: [:id, :person_id, :_destroy], invest_witnesses_attributes: [:id, :person_id, :_destroy],
      invest_sources_attributes: [:id, :person_id, :_destroy], invest_evidences_attributes: [:id, :evidence_id, :_destroy],
      invest_stolens_attributes: [:id, :stolen_id, :_destroy], invest_arrests_attributes: [:id, :arrest_id, :_destroy],
      invest_contacts_attributes: [:id, :contact_id, :_destroy], invest_warrants_attributes: [:id, :warrant_id, :_destroy]
    )
  end
end
