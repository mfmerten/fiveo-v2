class BookingsController < ApplicationController
  before_action :load_jail

  def index
    options = {
      path_prefix: 'jail',
      path_options: {jail_id: @jail.id},
      active: true,
      scopes: [:by_name],
      paginate: true,
      includes: [:holds, :absents]
    }
    @bookings = get_records(options)
    
    @links.push(['Absents', jail_absents_path(@jail)])
    @links.push(['Holds', jail_holds_path(@jail)])
    @links.push(['Visitors', jail_visitors_path(@jail)])
    @links.push(['Photo Album', photo_album_jail_bookings_path(@jail)])
    @links.push(["Transfers", jail_transfers_path(@jail)])
    psize = @jail.problems.size
    unless psize == 0
      @links.push(["Problems (#{psize})", problems_jail_bookings_path(@jail), {red: true}])
    end
    if SiteConfig.enable_internal_commissary
      if can?(:view_commissary)
        @links.push(["Commissary", jail_commissaries_path(@jail)])
      end
      if can?(:update_commissary, :update_option)
        @links.push(['Commis. Items', jail_commissary_items_path(@jail)])
      end
    end
    if can?(:view_jail) || can?(@jail.admin_permission)
      @links.push(["#{@jail.acronym} Admin", jail_path(@jail)])
    end

    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end
  
  def show
    if params[:info_id]
      @info_id = params[:info_id]
      @booking = @jail.bookings.find_by_id(params[:id])
    else
      @booking = @jail.bookings.find(params[:id])
      @page_title = "#{@jail.acronym} Booking #{@booking.id}"
      @commissary = []
      unless @booking.person.nil? || !SiteConfig.enable_internal_commissary
        @commissary = @booking.person.past_days(30).reverse
      end
      
      if can?(:update_booking, @jail.update_permission)
        @links.push(["Edit", edit_jail_booking_path(@jail, @booking)])
      end
      if can?(:destroy_booking, @jail.update_permission)
        @links.push(['Delete', [@jail, @booking], {method: 'delete', data: {confirm: confirm_msg_for(:booking)}}])
      end
      if @booking.release_datetime?
        if @booking == @booking.person.bookings.last && can?(:reopen_booking, @jail.update_permission)
          @links.push(['Reopen', clear_release_jail_booking_path(@jail, @booking), {method: :post}])
        end
      else
        if can?(:update_booking, @jail.update_permission)
          if @booking.releasable?
            @links.push(['Release', release_jail_booking_path(@jail, @booking)])
          end
          if @booking.allow_arraignment_notice?
            @links.push(['Arraignment', arraignment_notice_jail_booking_path(@jail, @booking), pdf: true])
          end
          @links.push([@booking.good_time? ? "Goodtime Off" : "Goodtime On", toggle_goodtime_jail_booking_path(@jail, @booking), {method: :post}])
          @links.push(['Jail Card', jail_card_jail_booking_path(@jail,@booking), pdf: true])
          @links.push(['Manual Hold', new_jail_booking_hold_path(@jail, @booking)])
          @links.push(['Medical Screen', new_jail_booking_medical_path(@jail, @booking)])
          unless @booking.medicals.empty?
            @links.push(['Medical Screen', medical_form_jail_booking_medical_path(@jail, @booking, @booking.medicals.last), pdf: true])
          end
          @links.push(['Medications', person_medications_path(@booking.person)])
          if can?(:update_arrest)
            @links.push(['New Arrest', new_arrest_path(booking_id: @booking.id)])
          end
          if can?(:view_commissary) && SiteConfig.enable_internal_commissary
            @links.push(["New Commis.", new_jail_commissary_path(@jail, person_id: @booking.person_id)])
          end
          @links.push(["New Visitor", new_jail_booking_visitor_path(@jail, @booking)])
          unless @booking.booking_logs.empty?
            @links.push(['Time Log', time_log_jail_booking_path(@jail, @booking), pdf: true])
          end
          @links.push(['Transfer', new_jail_booking_transfer_path(@jail, @booking)])
          unless @booking.temporary_release?
            @links.push(['Temp Release', temp_release_out_jail_booking_path(@jail, @booking)])
          end
        end
      end
      @page_links.push(@booking.person, [@booking.creator,'creator'],[@booking.updater,'updater'])
    end
    
    respond_to :html, :json, :js
  end

  def new
    can!(:update_booking, @jail.update_permission)
    @person = Person.find(params[:person_id])
    
    reasons = []
    reasons.push("Person sex unknown!") if (@person.sex.blank? || @person.sex < 1 || @person.sex > 2) && !(@jail.male? && @jail.female?)
    reasons.push("Males not allowed!") if !@jail.male? && @person.sex == 1
    reasons.push("Females not allowed!") if !@jail.female? && @person.sex == 2
    reasons.push("Juveniles not allowed!") if !@jail.juvenile? && @person.date_of_birth? && @person.age < SiteConfig.adult_age
    unless reasons.empty?
      flash.alert = "Cannot Book into #{@jail.acronym}: #{reasons.join(', ')}"
      redirect_to @person and return
    end
    
    unless @person.bookings.empty? || @person.bookings.last.release_datetime?
      flash.alert = "Person Already Booked!"
      redirect_to [@jail, @person.bookings.last] and return
    end
    
    @page_title = "New #{@jail.acronym} Booking"
    @booking = @jail.bookings.build(person_id: @person.id, cash_at_booking: BigDecimal.new('0',2), booking_datetime: Time.now, good_time: @jail.goodtime_by_default)
    set_officer_to_session(:booking_officer)
    @booking.properties.build
  end
  
  def edit
    can!(:update_booking, @jail.update_permission)

    @booking = @jail.bookings.find(params[:id])
    @person = @booking.person
    @page_title = "Edit #{@jail.acronym} Booking #{@booking.id}"
    @booking.booking_officer_id = nil
    @booking.release_officer_id = nil
  end
  
  def create
    can!(:update_booking, @jail.update_permission)

    @person = Person.find(params[:person_id])

    redirect_to @person and return if params[:commit] == 'Cancel'

    @booking = @jail.bookings.build(booking_params)
    @booking.created_by = session[:user]
    @booking.updated_by = session[:user]
    @page_title = "New #{@jail.acronym} Booking"

    success = @booking.save
    user_action("Booking ##{@booking.id} Created.") if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@jail, @booking], notice: 'Booking was successfully created.' }
        format.json { render :show, status: :created, location: [@jail, @booking] }
      else
        format.html { render :new }
        format.json { render json: @booking.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_booking, @jail.update_permission)

    @booking = @jail.bookings.find(params[:id])

    redirect_to [@jail, @booking] and return if params[:commit] == 'Cancel'

    @person = @booking.person
    @page_title = "Edit #{@jail.acronym} Booking #{@booking.id}"
    @booking.updated_by = session[:user]
    @booking.legacy = false

    success = @booking.update(booking_params)
    user_action("Booking ##{@booking.id} Updated") if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@jail, @booking], notice: 'Booking was successfully updated.' }
        format.json { render :show, status: :ok, location: [@jail, @booking] }
      else
        format.html { render :edit }
        format.json { render json: @booking.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_booking, @jail.update_permission)

    @booking = @jail.bookings.find(params[:id])

    if reasons = @booking.reasons_not_destroyable
      flash.alert = "Cannot Delete: #{reasons}"
      redirect_to [@jail, @booking] and return
    end

    @booking.destroy
    user_action("Booking ##{@booking.id} Deleted.")
    
    respond_to do |format|
      format.html { redirect_to jail_bookings_url(@jail), notice: 'Booking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def release
    can!(:update_booking, @jail.update_permission)
    
    @booking = @jail.bookings.find(params[:id])
    @person = @booking.person
    @page_title = "Release Booking #{@booking.id}"
    @booking.updated_by = session[:user]
    @booking.legacy = false
    @booking.release_datetime = Time.now
    set_officer_to_session(:release_officer)
    
    if request.post?
      if params[:commit] == "Cancel"
        redirect_to [@jail, @booking] and return
      end
      
      if @booking.update(booking_params)
        redirect_to [@jail, @booking]
        user_action("Booking ##{@booking.id} Released")
      end
    end
  end

  def toggle_goodtime
    can!(:update_booking, @jail.update_permission)
    
    @booking = @jail.bookings.find(params[:id])
    @booking.update_attribute(:updated_by, session[:user])
    @booking.toggle(:good_time)
    
    if @booking.good_time?
      user_action("Turned on Good Time for Booking #{@booking.id}.")
    else
      user_action("Turned off Good Time for Booking #{@booking.id}.")
    end
    redirect_to [@jail, @booking]
  end

  def problems
    @problems = @jail.problems
    @page_title = "Booking Problems"
    
    @links.push(['Booking', jail_bookings_path(@jail)])
    @links.push(['Problems PDF', problem_list_jail_bookings_path(@jail), {red: true}])
  end
  
  def clear_release
    can!(:reopen_booking, @jail.update_permission)
    
    @booking = @jail.bookings.find(params[:id])
    
    unless @booking == @booking.person.bookings.last
      flash.alert = "Cannot Reopen Booking, Newer Booking Exists"
      redirect_to [@jail, @booking] and return
    end
    
    @booking.release_officer = ''
    @booking.release_officer_unit = ''
    @booking.release_officer_badge = ''
    @booking.release_datetime = nil
    @booking.updated_by = session[:user]
    @booking.released_because = ''
    @booking.save(validate: false)
    
    redirect_to [@jail, @booking]
    flash.notice = "Booking ##{@booking.id} Reopened."
    user_action("Booking ##{@booking.id} Reopened.")
  end

  def temp_release_out
    can!(:update_booking, @jail.update_permission)

    @booking = @jail.bookings.find(params[:id])

    if @booking.release_datetime?
      flash.alert = "Temporary Release Not Possible, Booking is Closed"
      redirect_to [@jail, @booking] and return
    end

    if @booking.temporary_release?
      flash.alert = "Temporary Release Not Possible, Already On Temporary Release"
      redirect_to [@jail, @booking] and return
    end
    
    @page_title = 'Temporary Release'
    
    if request.post?
      redirect_to [@jail,@booking]
      
      return if params[:commit] == "Cancel"
            
      unless Hold::TempReleaseReason.include?(params[:reason])
        flash.alert = 'Invalid Reason Specified!'
        return
      end
      
      @hold = @booking.holds.build(hold_type: "Temporary Release", temp_release_reason: params[:reason], created_by: session[:user], updated_by: session[:user], hold_datetime: Time.now)
      set_officer_to_session(:hold_by, object: @hold)
      
      if @hold.save
        @booking.stop_time(session[:user])
        user_action("Placed #{@jail.acronym} Booking #{@booking.id} on Temporary Release")
      else
        flash.alert = "Failed to save hold! #{@hold.error_messages}"
      end
    end
  end

  def temp_release_in
    can!(:update_booking, @jail.update_permission)
    
    @booking = @jail.bookings.find(params[:id])
    @hold = @booking.holds.find(params[:hold_id])
    
    if @hold.cleared_datetime?
      flash.alert = "Hold is not active!"
      redirect_to @hold and return
    end
    
    set_officer_to_session(:cleared_by, object: @hold)
    @hold.cleared_because = "Manual Release"
    @hold.cleared_datetime = Time.now
    @hold.explanation = "Returned from Temporary Release"

    unless @hold.save
      flash.alert = "Failed to Save Hold! #{@hold.errors.full_messages}"
      redirect_to [@jail, @booking] and return
    end

    @booking.start_time(session[:user])
    user_action("Returned #{@jail.acronym} Booking #{@booking.id} from Temporary Release.")
    redirect_to [@jail, @booking]
  end

  def jail_card
    @booking = @jail.bookings.find(params[:id])

    if @booking.person.nil?
      flash.alert = "Booking has Invalid Person!"
      redirect_to [@jail, @booking] and return
    end

    @arrests = @booking.holds.active.reject{|h| h.arrest.nil?}
    @sentences = @booking.holds.active.of_type(['Serving Sentence', 'Fines/Costs'])
    @other = @booking.holds.active.reject{|h| !h.arrest.nil? || h.hold_type == "Serving Sentence" || h.hold_type == "Fines/Costs" || h.hold_type == "Temporary Transfer" || h.hold_type == "Temporary Release"}
    
    if pdf_url = make_pdf_file("jail_card", Bookings::JailCard.new(@booking, @arrests, @sentences, @other))
      redirect_to pdf_url
      user_action("Printed Jail Card for #{@jail.acronym} Booking #{params[:booking_id]}.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to [@jail, @booking]
    end
  end

  def photo_album
    options = {
      scopes: [:by_name, :with_mugshot, :active]
    }
    @bookings = get_records(options)

    @links.push(["#{@jail.acronym}", jail_bookings_path(jail_id: @jail.id)])
    
    if @bookings.empty?
      flash.alert = "No #{@jail.acronym} Inmate Photos Available!"
      redirect_to jail_bookings_url(@jail) and return
    end
    
    @page_title = "Photo Album: #{@jail.acronym} Inmates"
  end

  def list_form
    # note: using index method of retrieving records because this report is filterable
    options = {
      scopes: [:active, :by_name]
    }
    @bookings = get_records(options)
    
    if @bookings.empty?
      flash.alert = "Nothing to report!"
      redirect_to jail_bookings_url(@jail) and return
    end
    
    if pdf_url = make_pdf_file("booking_list", Bookings::List.new(@bookings.to_a, @jail.id, params[:title]))
      redirect_to pdf_url
      user_action("Printed #{@jail.acronym} Booking List.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to jail_bookings_url(@jail)
    end
  end

  def in_jail
    @bookings = @jail.bookings.active.by_name.includes({holds: [{arrest: {charges: :warrant}}, :sentence, :doc_record]}).all
    
    if @bookings.empty?
      flash.alert = "Nothing to report!"
      redirect_to jail_bookings_url(@jail) and return
    end
    
    if pdf_url = make_pdf_file("in_jail", Bookings::InJail.new(@jail.id, @bookings))
      redirect_to pdf_url
      user_action("Printed #{@jail.acronym} In-Jail Report.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to jail_bookings_url(@jail)
    end
  end

  def ssa_report
    unless report_date = AppUtils.valid_date(params[:ssa_date])
      report_date = Date.today
    end
    if report_date > Date.today
      report_date = Date.today
    end
    @start_date = (report_date - 1.month).beginning_of_month
    @stop_date = (report_date - 1.month).end_of_month
    # bookings uses times instead of dates
    @bookings = @jail.bookings.by_name.in_jail_between(@start_date.to_time, @stop_date.to_time.end_of_day).all
    
    if @bookings.empty?
      flash.alert = "Nothing to report!"
      redirect_to jail_bookings_url(@jail) and return
    end
    
    if pdf_url = make_pdf_file("ssa_report", Bookings::SsaReport.new(@start_date,@stop_date,@bookings,@jail.id))
      redirect_to pdf_url
      user_action("Printed #{@jail.acronym} SSA Report.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to jail_bookings_url(@jail)
    end
  end

  def fcs_report
    report_date = AppUtils.valid_date(params[:fcs_date])
    if report_date.nil? || report_date > Date.today
      report_date = Date.today
    end
    @start_date = (report_date - 1.month).beginning_of_month
    @stop_date = (report_date - 1.month).end_of_month
    # bookings uses times instead of dates
    @bookings = @jail.bookings.by_name.with_booking_date(@start_date.to_time, @stop_date.to_time.end_of_day).all
    
    if @bookings.empty?
      flash.alert = "Nothing to report!"
      redirect_to jail_bookings_url(@jail) and return
    end
    
    if pdf_url = make_pdf_file("fcs_report", Bookings::FcsReport.new(@start_date, @stop_date, @bookings,@jail.id))
      redirect_to pdf_url
      user_action("Printed #{@jail.acronym} FCS Report.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to jail_bookings_url(@jail)
    end
  end

  def arraignment_notice
    @booking = @jail.bookings.find(params[:id])
    
    if @booking.person.nil?
      flash.alert = "Booking Has Invalid Person"
      redirect_to [@jail, @booking] and return
    end
    
    holds = @booking.holds.reject{|h| h.hold_type != 'Bondable'}
    @arrests = holds.collect{|h| h.arrest}.compact.uniq
    @charges = @arrests.reject{|a| a.district_charges.empty?}.collect{|a| a.district_charges.join(', ')}.compact.join(' -- ')
    
    if pdf_url = make_pdf_file("arraignment_notice", Bookings::ArraignmentNotice.new(@booking,@arrests,@charges))
      redirect_to pdf_url
      user_action("Printed Arraignment notice for #{@jail.acronym} Booking #{params[:id]}.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to [@jail, @booking]
    end
  end

  def billing_headcount
    unless @report_date = AppUtils.valid_date(params[:report_date])
      @report_date = Date.yesterday
    end
    unless @report_date < Date.today
      @report_date = Date.yesterday
    end
    
    if pdf_url = make_pdf_file("billing_headcount", Bookings::Headcount.new(@report_date, @jail.id))
      redirect_to pdf_url
      user_action("Printed #{@jail.acronym} Nightly Headcount Report.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to jail_bookings_url(@jail)
    end
  end

  def month_billing
    unless @start_date = AppUtils.valid_date("#{params[:month]}/01/#{params[:year]}")
      @start_date = Date.today.beginning_of_month
    end

    if @start_date == Date.today
      flash.alert = "Start date is today, no data available!"
      redirect_to jail_bookings_url(@jail) and return
    end

    if @start_date.end_of_month > Date.today
      @stop_date = Date.yesterday
    else
      @stop_date = @start_date.end_of_month
    end
    
    if pdf_url = make_pdf_file("month_billing", Bookings::Billing.new(@jail.id, @start_date, @stop_date))
      redirect_to pdf_url
      user_action("Printed #{@jail.acronym} Monthly Billing Headcount Report for #{params[:month]}/#{params[:year]}.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to jail_bookings_url(@jail)
    end
  end
  
  def release_report
    @start_date = AppUtils.valid_date(params[:start_date])
    @stop_date = AppUtils.valid_date(params[:stop_date])
    
    # if both blank, do a prior month report
    if @start_date.nil? && @stop_date.nil?
      @start_date = (Date.today - 1.month).beginning_of_month
      @stop_date = (Date.today - 1.month).end_of_month
    elsif @start_date.nil?
      @start_date = @stop_date.beginning_of_month
    elsif @stop_date.nil?
      @stop_date = @start_date.end_of_month
    end
    
    # if they are reversed, swap them
    if @start_date > @stop_date
      @start_date, @stop_date = [@stop_date, @start_date]
    end
    
    # bookings uses times instead of dates
    @bookings = @jail.bookings.with_release_date(@start_date.to_time, @stop_date.to_time.end_of_day).all
    
    if @bookings.empty?
      flash.alert = "Nothing to report!"
      redirect_to jail_bookings_url(@jail) and return
    end
    
    if pdf_url = make_pdf_file("release_report", Bookings::ReleaseReport.new(@start_date, @stop_date, @jail, @bookings))
      redirect_to pdf_url
      user_action("Printed Release Report for #{@jail.acronym} (#{AppUtils.format_date(@start_date)} to #{AppUtils.format_date(@stop_date)}).")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to jail_bookings_url(@jail)
    end
  end

  def time_log
    @booking = @jail.bookings.find(params[:id])
    
    if pdf_url = make_pdf_file("time_log", Bookings::TimeLog.new(@booking))
      redirect_to pdf_url
      user_action("Printed Time Log for #{@jail.acronym} Booking #{params[:id]}.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to [@jail, @booking]
    end
  end

  def problem_list
    @booking_problems = @jail.problems
    
    if @booking_problems.empty?
      flash.alert = "Nothing to report!"
      redirect_to jail_bookings_url(@jail) and return
    end
    
    if pdf_url = make_pdf_file("problem_list", Bookings::Problems.new(@booking_problems))
      redirect_to pdf_url
      user_action("Printed Problem List for #{@jail.acronym}.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to jail_bookings_url(@jail)
    end
  end

  protected
  
  def load_jail
    @jail = Jail.find(params[:jail_id])
    can!(@jail.view_permission)
  end
  
  def booking_params
    params.require(:booking).permit(:person_id, :attorney, :booking_datetime, :phone_called, :cash_at_booking, :cash_receipt,
      :booking_officer_id, :booking_officer_badge, :booking_officer_unit, :booking_officer, :cell, :sched_release_date, :intoxicated,
      :afis, :good_time, :remarks, :release_officer, :release_officer_unit, :release_officer_badge, :release_officer_id,
      :release_datetime, :released_because, properties_attributes: [:id, :quantity, :description, :_destroy])
  end
end
