class CommissaryItemsController < ApplicationController
  before_action :check_if_enabled
  before_action :load_jail
  
  def index
    options = {
      scopes: [:by_description],
      paginate: true
    }
    @commissary_items = get_records(options)
    
    if can?(:update_option)
      @links.push(["New", new_jail_commissary_item_path(@jail)])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end
  
  def show
    @commissary_item = @jail.commissary_items.find(params[:id])
    @page_title = "#{@jail.acronym} Commissary Item #{@commissary_item.id}"
    
    if can?(:update_option)
      @links.push(["New", new_jail_commissary_item_path(@jail)])
      @links.push(["Edit", edit_jail_commissary_item_path(@jail, @commissary_item)])
      @links.push(["Delete", [@jail, @commissary_item], {method: 'delete', data: {confirm: confirm_msg_for(:commissary_item)}}])
    end
  end

  def new
    can!(:update_option)

    @commissary_item = @jail.commissary_items.build
    @page_title = "New Commissary Item"
  end

  def edit
    can!(:update_option)

    @commissary_item = @jail.commissary_items.find(params[:id])
    @page_title = "Edit #{@jail.acronym} Commissary Item #{@commissary_item.id}"
  end
  
  def create
    can!(:update_option)

    redirect_to jail_commissary_items_url(@jail) if params[:commit] == 'Cancel'

    @commissary_item = @jail.commissary_items.build(commissary_item_params)
    @page_title = "New Commissary Item"
    @commissary_item.created_by = session[:user]
    @commissary_item.updated_by = session[:user]

    success = @commissary_item.save
    user_action "Commissary Item ##{@commissary_item.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@jail, @commissary_item], notice: 'Commissary Item was successfully created.' }
        format.json { render :show, status: :created, location: [@jail, @commissary_item] }
      else
        format.html { render :new }
        format.json { render json: @commissary_item.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_option)

    @commissary_item = @jail.commissary_items.find(params[:id])

    redirect_to [@jail, @commissary_item] and return if params[:commit] == 'Cancel'

    @page_title = "Edit #{@jail.acronym} Commissary Item #{@commissary_item.id}"
    @commissary_item.updated_by = session[:user]
    
    success = @commissary_item.update(commissary_item_params)
    user_action "Commissary Item ##{@commissary.item} Updated."
    
    respond_to do |format|
      if success
        format.html { redirect_to [@jail, @commissary_item], notice: 'Commissary Item was successfully updated.' }
        format.json { render :show, status: :ok, location: [@jail, @commissary_item] }
      else
        format.html { render :edit }
        format.json { render json: @commissary_item.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:update_option)

    @commissary_item = @jail.commissary_items.find(params[:id])

    @commissary_item.destroy
    user_action "Commissary Item ##{@commissary_item} Deleted."

    respond_to do |format|
      format.html { redirect_to jail_commissary_items_url(@jail), notice: 'Commissary Item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def sales_sheet
    if !params[:person_id].blank?
      unless cperson = Person.find_real_identity(params[:person_id])
        flash.alert = "Invalid Person Specified!"
        redirect_to jail_commissary_items_url(@jail) and return
      end
    end
    
    if cperson.nil?
      @people = Person.by_name.in_jail(@jail.id)
    else
      @people = [cperson]
    end
    
    if @people.empty?
      flash.alert = "No People Selected!"
      redirect_to jail_commissary_items_url(@jail) and return
    end
    
    @commissary_items = @jail.commissary_items.by_description.all
    
    if @commissary_items.empty?
      flash.alert = "Nothing to report!"
      redirect_to jail_commissary_items_url(@jail) and return
    end
    
    if pdf_url = make_pdf_file("sales_sheet", CommissaryItems::Sales.new(@jail, @people, @commissary_items))
      redirect_to pdf_url
      user_action("Printed #{@jail.acronym} Sales Sheets")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to jail_commissary_items_url(@jail)
    end
  end
  
  private
  
  def load_jail
    @jail = Jail.find(params[:jail_id])
    can!(@jail.view_permission)
  end

  def check_if_enabled
    unless SiteConfig.enable_internal_commissary
      redirect_to root_url and return
    end
  end
  
  def commissary_item_params
    params.require(:commissary_item).permit(:description, :price)
  end
end