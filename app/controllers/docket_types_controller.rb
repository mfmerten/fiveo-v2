class DocketTypesController < ApplicationController

  def index
    options = {
      scopes: [:by_name],
      paginate: true
    }
    @docket_types = get_records(options)
    
    if can?(:update_option)
      @links.push(['New', new_docket_type_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @docket_type = DocketType.find(params[:id])
    @page_title = "Docket Type #{@docket_type.id}"
    
    if can?(:update_option)
      @links.push(['New', new_docket_type_path])
      @links.push(['Edit', edit_docket_type_path(@docket_type)])
      @links.push(['Delete', @docket_type, {method: 'delete', data: {confirm: confirm_msg_for(:docket_type)}}])
    end
    
    @page_links.push([@docket_type.creator,'creator'],[@docket_type.updater,'updater'])
  end

  def new
    can!(:update_option)

    @docket_type = DocketType.new
    @page_title = "New Docket Type"
  end
  
  def edit
    can!(:update_option)

    @docket_type = DocketType.find(params[:id])
    @page_title = "Edit Docket Type #{@docket_type.id}"
  end
  
  def create
    redirect_to docket_types_path and return if params[:commit] == 'Cancel'

    can!(:update_option)

    @docket_type = DocketType.new(docket_type_params)
    @page_title = "New Docket Type"
    @docket_type.created_by = session[:user]
    @docket_type.updated_by = session[:user]

    success = @docket_type.save
    user_action "Docket Type ##{@docket_type.id} Created." if success

    respond_to do |format|
      if success
        format.html { redirect_to @docket_type, notice: 'Docket Type was successfully created.' }
        format.json { render :show, status: :created, location: @docket_type }
      else
        format.html { render :new }
        format.json { render json: @docket_type.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_option)
    
    @docket_type = DocketType.find(params[:id])
    
    redirect_to @docket_type and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Docket Type #{@docket_type.id}"
    @docket_type.updated_by = session[:user]
    
    success = @docket_type.update(docket_type_params)
    user_action "Docket Type ##{@docket_type.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @docket_type, notice: 'Docket Type was successfully updated.' }
        format.json { render :show, status: :ok, location: @docket_type }
      else
        format.html { render :edit }
        format.json { render json: @docket_type.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:update_option)
    
    @docket_type = DocketType.find(params[:id])
    
    @docket_type.destroy
    user_action "Docket Type ##{@docket_type.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to docket_types_url, notice: 'Docket Type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  
  def docket_type_params
    params.require(:docket_type).permit(:name)
  end
end