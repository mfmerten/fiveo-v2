# History records are derived from legacy data exported from old computer
# systems once used by the agency.  These are static records added through
# migrations (no add/edit capability) and are included to improve the
# upon the information available for people.
class HistoriesController < ApplicationController
  before_action :check_enabled
  skip_before_action :require_base_perms, only: :check_enabled
  
  def index
    options = {
      scopes: [:by_name],
      paginate: true
    }
    @histories = get_records(options)
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @history = History.find(params[:id])
    
    if can?(:destroy_history)
      @links.push(['Delete', @history, {method: 'delete', data: {confirm: confirm_msg_for(:history)}}])
    end
  end

  # no new/edit/create/update actions needed

  def destroy
    can!(:destroy_history)

    @history = History.find(params[:id])

    @history.destroy
    user_action "History ##{@history.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to histories_url, notice: 'History was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  protected

  def check_enabled
    unless SiteConfig.show_history
      flash.alert = "History Section Is Disabled By Site Administrator"
      redirect_to root_url and return
    end
  end
end
