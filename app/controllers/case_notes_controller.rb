class CaseNotesController < ApplicationController
  
  def index
    options = {
      scopes: [:by_reverse_date],
      includes: [:creator, :updater],
      paginate: true
    }
    @case_notes = get_records(options)
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @case_note = CaseNote.find(params[:id])
    @page_title = "Note #{@case_note.id} for Case #{@case_note.case_no}"
    
    @links.push(["Back", show_case_case_notes_path(case: @case_note.case_no)])
    if can?(:update_case)
      @links.push(["New", new_case_note_path(case: @case_note.case_no)])
      if @case_note.created_by == session[:user] || can?(:author_all)
        @links.push(["Edit", edit_case_note_path(@case_note)])
      end
    end
    if can?(:destroy_case) && (@case_note.created_by == session[:user] || can?(:author_all))
      @links.push(['Delete', @case_note, {method: 'delete', data: {confirm: confirm_msg_for(:case_note)}}])
    end

    @page_links.push([@case_note.creator, 'Creator'],[@case_note.updater, 'Updater'])
  end

  def new
    can!(:update_case)

    if params[:case].blank?
      flash.alert = "Must Specify Case No!"
      redirect_to case_notes_url and return
    end
    
    @case_note = CaseNote.new(case_no: params[:case])
    @page_title = "New Note for Case #{@case_note.case_no}"
  end
    
  def edit
    can!(:update_case)

    @case_note = CaseNote.find(params[:id])
    enforce_author(@case_note)
    @page_title = "Edit Note #{@case_note.id} for Case #{@case_note.case_no}"
  end
  
  def create
    can!(:update_case)

    if params[:case].blank?
      flash.alert = "Must Specify Case No!"
      redirect_to case_notes_url and return
    end
    
    redirect_to case_url(case: params[:case]) and return if params[:commit] == 'Cancel'

    @case_note = CaseNote.new(case_note_params)
    @page_title = "New Note for Case #{@case_note.case_no}"
    @case_note.created_by = session[:user]
    @case_note.updated_by = session[:user]

    success = @case_note.save
    user_action "Case Note ##{@case_note.id} Created." if success

    if success
      format.html { redirect_to @case_note, notice: 'Case Note was successfully created.' }
      format.json { render :show, status: :created, location: @case_note }
    else
      format.html { render :new }
      format.json { render json: @case_note.errors, status: :unprocessable_entity }
    end
  end
  
  def update
    can!(:update_case)
    
    @case_note = CaseNote.find(params[:id])
    
    redirect_to @case_note and return if params[:commit] == 'Cancel'
    
    enforce_author(@case_note)
    @page_title = "Edit Note #{@case_note.id} for Case #{@case_note.case_no}"
    @case_note.updated_by = session[:user]
    
    success = @case_note.update(case_note_params)
    
    respond_to do |format|
      if success
        format.html { redirect_to @case_note, notice: 'Case Note was successfully updated.' }
        format.json { render :show, status: :ok, location: @case_note }
      else
        format.html { render :edit }
        format.json { render json: @case_note.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_case)

    @case_note = CaseNote.find(params[:id])
    enforce_author(@case_note)

    @case_note.destroy
    user_action "Case Note ##{@case_note.id} Deleted."

    respond_to do |format|
      format.html { redirect_to case_notes_url(case: @case_note.case), notice: 'Case Note was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def show_case
    if params[:case].blank?
      flash.alert = "Must specify Case No!"
      redirect_to case_notes_url and return
    end
    
    @case = params[:case]
    @page_title = "Case No. #{@case}"
    @case_notes = CaseNote.with_case_no(@case)
    
    @links.push(["New Note", new_case_note_path(case: @case)])
    
    models = []
    Dir.chdir(File.join(Rails.root, "app/models")) do 
      models = Dir["*.rb"]
    end
    @items = []
    models.each do |m|
      next if m == 'case_note.rb' || m == 'invest_case.rb'
      model_name = m.sub(/\.rb$/,'')
      class_name = model_name.camelize
      if class_name.constantize.respond_to?('find_all_by_case_no')
        @items.push({
          'name' => class_name.pluralize,
          'view' => model_name.pluralize,
          'partial' => model_name,
          'records' => class_name.constantize.find_all_by_case_no(@case).reject{|r| !check_privacy(r)}
        })
      elsif class_name.constantize.respond_to?('with_case_no')
        @items.push({
          'name' => class_name.pluralize,
          'view' => model_name.pluralize,
          'partial' => model_name,
          'records' => class_name.constantize.with_case_no(@case).reject{|r| !check_privacy(r)}
        })
      end
    end
    @related = Investigation.related_cases(@case)
    
    @docket_references = []
    Docket.find_all_by_case_no(@case).each do |d|
      unless d.case_numbers.reject{|c| c == @case}.empty?
        @docket_references.push(d)
      end
    end
  end
  
  private
  
  def case_note_params
    params.require(:case_note).permit(:case_no, :note)
  end
end
