class UsersController < ApplicationController
  skip_before_action :login_required, only: [:login, :change_password, :logout]
  skip_before_action :update_session_active, only: [:login, :logout]
  skip_before_action :require_base_perms, only: [:login, :logout, :change_password, :photo, :edit_settings]
  
  def index
    options = {
      active: true,
      scopes: [:by_login],
      includes: [:groups, {contact: :agency}],
      paginate: true
    }
    @users = get_records(options)
    
    @links.push(['New', new_user_path])
    @links.push(['Photos', photo_album_users_path])
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end
  
  def show
    @user = User.find(params[:id])
    @page_title = "User #{@user.id}"
    
    unless @contact = @user.contact
      flash.now.alert = 'User has no profile! Please edit user to fix it!'
    end
    
    if can?(:reset_user)
      @links.push(['Reset Pswd', reset_user_path(@user), {method: :post, data: {confirm: 'Are you sure you want to reset this users Password?'}}])
    end
    if can?(:update_user)
      @links.push(['Expire Pswd', expire_user_path(@user), {method: :post, data: {confirm: 'Are you sure you want to expire this users Password?'}}])
      @links.push([(@user.locked ? "Unlock" : "Lock"), toggle_user_path(@user), {method: :post}])
      @links.push(["New", new_user_path])
      @links.push(["Edit", edit_user_path(@user)])
    end
    if can?(:destroy_user)
      @links.push(['Delete', @user, {method: :delete, data: {confirm: confirm_msg_for(:user)}}])
    end
    if can?(:update_user)
      @links.push(["Photo", photo_user_path(@user)])
    end
    @page_links.push(@user.contact, [@user.creator,'creator'], [@user.updater,'updater'])
  end
  
  def new
    can!(:update_user)
    
    @user = User.new
    @contact = Contact.new
    @contact.state = SiteConfig.agency_state
    @page_title = "New User"
    @user.groups.build
  end
  
  def edit
    can!(:update_user)
    
    @user = User.find(params[:id])
    @page_title = "Edit User #{@user.id}"
    @user.updated_by = session[:user]
    
    unless @contact = @user.contact
      @contact = Contact.new(created_by: session[:user], updated_by: session[:user])
    end
    
    @otp = nil
  end

  def create
    can!(:update_user)
    
    redirect_to users_url and return if params[:commit] == 'Cancel'
    
    @user = User.new(user_params)
    @contact = Contact.new(contact_params)
    @otp = generate_onetime_password
    @user.password = @otp
    @user.password_confirmation = @otp
    @user.expires = Date.yesterday
    @page_title = "New User"
    @user.created_by = session[:user]
    @user.updated_by = session[:user]
    @contact.created_by = session[:user]
    @contact.updated_by = session[:user]
    
    record_saved = false
    if @user.valid? && @contact.valid?
      @contact.save
      @user.contact_id = @contact.id
      @user.save
      user_action("Added User #{@user.id}.")
      record_saved = true
    end
    
    respond_to do |format|
      if record_saved
        format.html { redirect_to @user, notice: "User was successfully created. One Time Password: '#{@otp}" }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @user = User.find(params[:id])
    @page_title = "Edit User #{@user.id}"
    
    # if updater cannot :view_user, send them to root
    # instead of to @user
    if params[:commit] == 'Cancel'
      if can?(:view_user)
        redirect_to @user
      else
        redirect_to root_url
      end
      return
    end
    
    # perms are complicated here by the fact that individual users may be allowed to update
    # their own profile photo.  Must allow this to succeed if this update is via photo form
    unless (SiteConfig.allow_user_photos && session[:user] == @user.id) && params[:user][:photo_form] == '1' || can?(:update_user)
      raise PermissionDeniedError.new("Users#photo: Only someone with Update User permission #{SiteConfig.allow_user_photos ? '(or the user) ' : ''} can update this photo.")
    end
    
    unless @contact = @user.contact
      @contact = @user.create_contact(created_by: session[:user], updated_by: session[:user])
    end
    @user.updated_by = session[:user]
    @contact.updated_by = session[:user]
    @otp = nil
    
    # remove photo if requested
    if params[:commit] == 'Delete'
      @user.user_photo = nil
    end
    
    record_saved = false
    if @contact.update(contact_params)
      if @user.update(user_params)
        record_saved = true
        if params[:commit] == 'Delete'
          user_action("Removed Photo for User #{@user.id}.")
        else
          user_action("User ##{@user.id} Updated.")
        end
      end
    end

    respond_to do |format|
      if record_saved
        # can only redirect to @user if submitter has view_user permission
        if can?(:view_user)
          format.html { redirect_to @user, notice: 'User was successfully updated.' }
        else
          format.html { redirect_to root_url, notice: 'Your Profile successfully updated.' }
        end
        
        format.json { render :show, status: :ok, location: @user }
      else
        if params[:user][:photo_form] == '1'
          format.html { render :photo }
        else
          format.html { render :edit }
        end
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:destroy_user)
    @user = User.find(params[:id])
    
    # destroy user record, leave the contact as a normal individual
    @user.destroy
    
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def login
    unless flash['notice'] || flash['alert']
      flash.now.notice = "Please Log In To Continue."
    end
    unless session[:user].nil?
      flash.alert = "You are already logged in."
      redirect_to root_url and return
    end
    if request.post?
      if temp = User.authenticate(params[:user][:login], params[:user][:password])
        # authenticated correctly, check account settings
        if temp.locked?
          user_action("Failed Login to locked account for #{params[:user][:login]} from #{request.remote_ip}!")
          flash.alert = "Account Locked! Contact Your Supervisor"
          redirect_to login_url and return
        end
        if temp.disabled_text?
          user_action("Failed Login to disabled account for #{params[:user][:login]} from #{request.remote_ip}!")
          flash.alert = "Account Disabled because '#{temp.disabled_text}'!"
          redirect_to login_url and return
        end
        # not locked or disabled
        session[:started_at] = Time.now
        session[:active_at] = Time.now
        session[:user] = temp.id
        session[:user_name] = temp.sort_name
        session[:user_unit] = temp.unit
        session[:user_badge] = temp.badge
        session[:user_login] = temp.login
        session[:user_expires] = temp.expires
        session[:items_per_page] = temp.items_per_page
        session[:user_show_tips] = temp.show_tips
        session[:pids] = temp.pids
        user_action("Logged In from #{request.remote_ip}!")
        if temp.expires <= Date.today
          flash.alert = "Password Expired!"
          redirect_to change_password_url
        else
          redirect_to root_url
        end
      else
        reset_session
        user_action("Failed Login for #{params[:user][:login]} from #{request.remote_ip}!")
        flash.alert = 'Login Unsuccessful!'
        redirect_to login_url
      end
    end
  end

  def logout
    if session[:user]
      user_action("Logged Out from #{request.remote_ip}!")
      reset_session
    end
    if params[:message]
      flash.notice = params[:message]
    elsif params[:alert]
      flash.alert = params[:alert]
    else
      flash.notice = "Please Log In To Continue."
    end
    redirect_to login_url
  end

  def change_password
    unless session[:user]
      redirect_to login_url
      return
    end
    @user = current_user
    @page_title = "Change Password For: " + @user.login

    if request.post?
      if params[:commit] == 'Cancel'
        redirect_to root_url
        return
      end
      if User.authenticate(@user.login, params[:user][:old_password])
        if params[:user][:old_password] == params[:user][:password]
          @user.errors.add(:password, "cannot be the same as your old password!")
        elsif params[:user][:password] =~ /[Pp][Aa][Ss$][Ss$]?[Ww][Oo0]?[Rr]?[Dd]/
          @user.errors.add(:password, "cannot be any form of the word 'password'")
        elsif params[:user][:password] =~ permutate_login(@user.login)
          @user.errors.add(:password, "cannot be any form of your login!")
        else
          @user.update_attributes(password: params[:user][:password], password_confirmation: params[:user][:password_confirmation])
          if @user.save
            session[:user_expires] = @user.expires
            redirect_to root_url
            user_action("Changed Their Password.")
          end
        end
      else
        flash.alert = "Invalid Current Password"
      end
    end
  end

  def reset
    can!(:reset_user)
    
    @user = User.find(params[:id])
    otp = generate_onetime_password
    @user.password = otp
    @user.password_confirmation = otp
    @user.expires = Date.yesterday
    
    @user.save
    
    user_action("Reset Password for User #{@user.id}.")
    flash.notice = "Password Reset. Temporary password is '#{otp}'"
    redirect_to users_url
  end

  def expire
    can!(:update_user)
    
    @user = User.find(params[:id])
    
    @user.update_attribute(:expires, Date.yesterday)
    
    user_action("Expired Password for User #{@user.id}.")
    flash.notice = "Password Expired. User will be required to change it on next login."
    redirect_to users_url
  end

  def photo
    if params[:id]
      @user = User.find(params[:id])
      @page_title = "User Photo"
    else
      @user = current_user
      @page_title = "My Photo"
    end
    @contact = @user.contact
    
    # special perms situation because in some cases, individual users may
    # be allowed to edit their own profile photo
    unless (SiteConfig.allow_user_photos && session[:user] == @user.id) || can?(:update_user)
      raise PermissionDeniedError.new("Users#photo: Only someone with Update User permission #{SiteConfig.allow_user_photos ? '(or the user) ' : ''} can update this photo.")
    end
    
    @user.updated_by = session[:user]
  end

  def photo_album
    options = {
      scopes: [:by_login, :with_photo]
    }
    @users = get_records(options)
    
    if @users.empty?
      flash.alert = 'No Photos Available!'
      redirect_to users_url and return
    end
    
    @page_title = "Photo Album: Users"
  end

  def edit_settings
    @user = current_user
    @page_title = "Edit Preferences For: #{@user.login}"
    
    if request.post?
      if params[:commit] == "Cancel"
        redirect_to root_url and return
      end
      
      if @user.update(user_params)
        session[:items_per_page] = @user.items_per_page
        session[:show_tips] = @user.show_tips
        redirect_to root_url
        user_action("Edited Their User Preferences.")
      end
    end
  end

  def toggle
    can!(:update_user)
    
    @user = User.find(params[:id])
    
    @user.toggle!(:locked)
    
    if @user.locked?
      user_action("Locked User #{@user.id}.")
    else
      user_action("Unlocked User #{@user.id}.")
    end
    redirect_to @user
  end
  
  private
  
  def user_params
    params.require(:user).permit(:login, :locked, :keep_pass, :disabled_text, :items_per_page, :forum_auto_subscribe, :send_email,
      :message_retention, :show_tips, :notify_arrest_new, :notify_booking_new, :notify_booking_release, :notify_booking_problems,
      :notify_call_new, :notify_forbid_new, :notify_forbid_recalled, :notify_offense_new, :notify_pawn_new, :notify_warrant_new,
      :notify_warrant_resolved, :user_photo, :photo_form, user_groups_attributes: [:id, :group_id, :_destroy]
    )
  end
  
  def contact_params
    params.require(:contact).permit(:created_by, :updated_by, :legacy, :sort_name, :business, :street, :street2, :city, :state, :zip, :title,
      :notes, :active, :officer, :detective, :judge, :bondsman, :bonding_company, :pawn_co, :physician, :pharmacy, :printable, :agency_id,
      :unit, :badge_no, :phone1_type, :phone1, :phone1_unlisted, :phone2_type, :phone2, :phone2_unlisted, :phone3_type, :phone3, :phone3_unlisted,
      :pri_email, :alt_email, :web_site, :remarks
    )
  end
end
