class PawnsController < ApplicationController
  
  def index
    options = {
      scopes: [:by_reverse_date],
      includes: [:pawn_items],
      paginate: true
    }
    @pawns = get_records(options)

    if can?(:update_pawn)
      @links.push(['New', new_pawn_path])
      if can?(:update_option)
        @links.push(['Item Types', pawn_item_types_path])
      end
    end

    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @pawn = Pawn.find(params[:id])
    @page_title = "Pawn #{@pawn.id}"
    
    if can?(:update_pawn)
      @links.push(['New', new_pawn_path])
      @links.push(['Edit', edit_pawn_path(@pawn)])
    end
    if can?(:destroy_pawn)
      @links.push(['Delete', @pawn, {method: :delete, data: {confirm: confirm_msg_for(:pawn_ticket)}}])
    end
    
    @page_links.push([@pawn.creator,'creator'],[@pawn.updater,'updater'])
  end
  
  def new
    can!(:update_pawn)

    @pawn = Pawn.new
    @page_title = "New Pawn"
    @pawn.pawn_items.build
  end
  
  def edit
    can!(:update_pawn)

    @pawn = Pawn.find(params[:id])
    @page_title = "Edit Pawn #{@pawn.id}"
  end
  
  def create
    can!(:update_pawn)

    redirect_to pawns_url and return if params[:commit] == 'Cancel'

    @pawn = Pawn.new(pawn_params)
    @page_title = "New Pawn"
    @pawn.created_by = session[:user]
    @pawn.updated_by = session[:user]

    success = @pawn.save
    user_action "Pawn ##{@pawn.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @pawn, notice: 'Pawn was successfully created.' }
        format.json { render :show, status: :created, location: @pawn }
      else
        format.html { render :new }
        format.json { render json: @pawn.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_pawn)
    
    @pawn = Pawn.find(params[:id])
    
    redirect_to @pawn and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Pawn #{@pawn.id}"
    @pawn.updated_by = session[:user]
    @pawn.legacy = false
    
    success = @pawn.update(pawn_params)
    user_action "Pawn ##{@pawn.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @pawn, notice: 'Pawn was successfully updated.' }
        format.json { render :show, status: :ok, location: @pawn }
      else
        format.html { render :edit }
        format.json { render json: @pawn.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:destroy_pawn)
    
    @pawn = Pawn.find
    
    @pawn.destroy
    user_action "Pawn ##{@pawn.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to pawns_url, notice: 'Pawn was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  
  def pawn_params
    params.require(:pawn).permit(:pawn_co, :ticket_no, :ticket_date, :ticket_type, :sort_name, :address1, :address2, :ssn, :oln,
      :oln_state, :case_no, :remarks, pawn_items_attributes: [:id, :item_type, :model_no, :serial_no, :_destroy, :description]
    )
  end
end
