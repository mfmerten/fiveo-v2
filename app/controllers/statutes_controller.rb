class StatutesController < ApplicationController

  def index
    options = {
      scopes: [:by_name],
      paginate: true
    }
    @statutes = get_records(options)
    
    if can?(:update_statute)
      @links.push(['New', new_statute_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @statute = Statute.find(params[:id])
    @page_title = "Statute #{@statute.id}"
    
    if can?(:update_statute)
      @links.push(['New', new_statute_path])
      @links.push(['Edit', edit_statute_path(@statute)])
    end
    if can?(:destroy_statute)
      @links.push(['Delete', @statute, {method: :delete, data: {confirm: confirm_msg_for(:statute)}}])
    end
    
    @page_links.push([@statute.creator,'creator'],[@statute.updater,'updater'])
  end

  def new
    can!(:update_statute)

    @statute = Statute.new
    @page_title = "New Statute"
  end
  
  def edit
    can!(:update_statute)

    @statute = Statute.find(params[:id])
    @page_title = "Edit Statute #{@statute.id}"
  end
  
  def create
    can!(:update_statute)

    redirect_to statutes_url and return if params[:commit] == 'Cancel'

    @statute = Statute.new(statute_params)
    @page_title = "New Statute"
    @statute.created_by = session[:user]
    @statute.updated_by = session[:user]
    
    success = @statute.save
    user_action "Statute ##{@statute.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @statute, notice: 'Statute was successfully created.' }
        format.json { render :show, status: :created, location: @statute }
      else
        format.html { render :new }
        format.json { render json: @statute.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_statute)

    @statute = Statute.find(params[:id])

    redirect_to @statute and return if params[:commit] == 'Cancel'

    @page_title = "Edit Statute #{@statute.id}"
    @statute.updated_by = session[:user]
    
    success = @statute.update(statute_params)
    user_action "Statute ##{@statute.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @statute, notice: 'Statute was successfully updated.' }
        format.json { render :show, status: :ok, location: @statute }
      else
        format.html { render :edit }
        format.json { render json: @statute.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:destroy_statute)
    
    @statute = Statute.find(params[:id])
    
    @statute.destroy
    user_action "Statute ##{@statute.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to statutes_url, notice: 'Statute was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def validate_charge
    @field = params[:update]
    @text = ''
    @valid = false
    
    if !@field.blank? && !params[:u].blank?

      if statute = Statute.find_first_by_code(params[:u].upcase)
        # matched by code
        if statute.common_name?
          @text = statute.common_name
        else
          @text = statute.name
        end
        @valid = true

      elsif statute = Statute.find_first_by_name_partial(params[:u].upcase)
        # matched by name
        if statute.common_name?
          @text = statute.common_name
        else
          @text = statute.name
        end
        @valid = true

      elsif statute = Statute.find_first_by_common_name_partial(params[:u].upcase)
        # matched by common name
        @text = statute.common_name
        @valid = true
      end
    end

    respond_to :js
  end
  
  private
  
  def statute_params
    params.require(:statute).permit(:code, :name, :common_name)
  end
end