# adapted from "forem" engine (https://github.com/radar/forem)
#
# Copyright 2011 Ryan Bigg, Philip Arndt and Josh Adams
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
class ForumTopicsController < ApplicationController
  before_action :set_page_title
  
  # no index action

  def show
    @forum = Forum.find(params[:forum_id])
    
    scope = can?(:admin_forum) ? @forum.forum_topics : @forum.forum_topics.visible
    @forum_topic = scope.find(params[:id])
    register_view
    @forum_posts = @forum_topic.forum_posts.page(params[:page]).per(session[:items_per_page])
    
    respond_to :html, :json, :js
  end

  def new
    can!(:update_forum)
    
    @forum = Forum.find(params[:forum_id])
    @forum_topic = @forum.forum_topics.build
    @forum_topic.initial_post.build(user_id: session[:user])
    @page_title = "New Forum Topic"
  end

  def edit
    can!(:admin_forum)

    @forum_topic = ForumTopic.find(params[:id])
    @forum = @forum_topic.forum
    @page_title = "Edit Forum Topic #{@forum_topic.id}"
  end

  def create
    redirect_to forums_url and return if params[:commit] == 'Cancel'
    
    can!(:update_forum)
    
    @forum = Forum.find(params[:forum_id])
    @forum_topic = @forum.forum_topics.build(forum_topic_params)
    @page_title = "New Forum Topic"
    @forum_topic.user_id = session[:user]
    
    success = @forum_topic.save
    user_action "Forum Topic ##{@forum_topic.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@forum, @forum_topic], notice: 'Forum Topic was successfully created.' }
        format.json { render :show, status: :created, location: [@forum, @forum_topic] }
      else
        format.html { render :new }
        format.json { render json: @forum_topic.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:admin_forum)
    
    @forum_topic = ForumTopic.find(params[:id])
    @forum = @forum_topic.forum
    
    redirect_to forum_forum_topic_url(@forum, @forum_topic) and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Forum Topic #{@forum_topic.id}"
    
    success = @forum_topic.update(forum_topic_params)
    user_action "Forum Topic ##{@forum_topic.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@forum, @forum_topic], notice: 'Forum Topic was successfully updated.' }
        format.json { render :show, status: :ok, location: [@forum, @forum_topic] }
      else
        format.html { render :edit }
        format.json { render json: @forum_topic.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:update_forum)
    
    @forum = Forum.find(params[:forum_id])
    @forum_topic = @forum.forum_topics.find(params[:id])
    
    if @forum_topic.locked? && !can?(:admin_forum)
      flash.alert = "This Topic is locked! You cannot delete it!"
      redirect_to forum_forum_topic_url(@forum, @topic) and return
    end
    
    if @forum_topic.hidden? && !can?(:admin_forum)
      flash.alert = "You are not allowed to view or delete this Topic!"
      redirect_to forum_forum_topic_url(@forum, @topic) and return
    end
    
    enforce_author(@forum_topic)
    
    @forum_topic.destroy
    user_action "Forum Topic ##{@forum_topic.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to @forum, notice: 'Forum Topic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def subscribe
    @forum = Forum.find(params[:forum_id])
    scope = can?(:admin_forum) ? @forum.forum_topics : @forum.forum_topics.visible
    @forum_topic = scope.find(params[:id])
    @forum_topic.subscribe_user(session[:user_id])
    flash.notice = "You have successfully subscribed to this Forum Topic."
    redirect_to forum_forum_topic_url(@forum, @forum_topic)
  end

  def unsubscribe
    @forum = Forum.find(params[:forum_id])
    scope = can?(:admin_forum) ? @forum.forum_topics : @forum.forum_topics.visible
    @forum_topic = scope.find(params[:id])
    @forum_topic.unsubscribe_user(session[:user_id])
    flash.notice = "You have successfully unsubscribed from this Forum Topic."
    redirect_to forum_forum_topic_url(@forum, @forum_topc)
  end

  def toggle_hide
    can!(:admin_forum)
    
    @forum_topic = ForumTopic.find(params[:id])
    @forum_topic.toggle!(:hidden)
    
    if @forum_topic.hidden?
      user_action "Forum Topic ##{@forum_topic.id} Hidden."
      flash.notice = "Forum Topic was successfully hidden."
    else
      user_action "Forum Topic ##{@forum_topic.id} Unhidden."
      flash.notice = "Forum Topic was successfully unhidden."
    end
    redirect_to forum_forum_topic_url(@forum_topic.forum, @forum_topic)
  end
  
  def toggle_lock
    can!(:admin_forum)

    @forum_topic = ForumTopic.find(params[:id])
    @forum_topic.toggle!(:locked)

    if @forum_topic.locked?
      user_action "Forum Topic ##{@forum_topic.id} Locked."
      flash.notice = "Forum Topic was successfully locked."
    else
      user_action "Forum Topic ##{@forum_topic.id} Unlocked."
      flash.notice = "Forum Topic was successfully unlocked."
    end
    redirect_to forum_forum_topic_url(@forum_topic.forum, @forum_topic)
  end
  
  def toggle_pin
    can!(:admin_forum)

    @forum_topic = ForumTopic.find(params[:id])
    @forum_topic.toggle!(:pinned)

    if @forum_topic.pinned?
      user_action "Forum Topic ##{@forum_topic.id} Pinned."
      flash.notice = "Forum Topic was successfully pinned."
    else
      user_action "Forum Topic ##{@forum_topic.id} Unpinned."
      flash.notice = "Forum Topic was successfully unpinned."
    end
    redirect_to forum_forum_topic_url(@forum_topic.forum, @forum_topic)
  end
  
  def move
    can!(:admin_forum)
    
    @forum_topic = ForumTopic.find(params[:id])
    @new_forum = Forum.find(params[:forum_id])
    @forum_topic.update_attribute(:forum_id, @new_forum.id)
    
    user_action "Forum Topic ##{@forum_topic.id} Moved."
    flash.notice = "Forum Topic was successfully moved."
    
    redirect_to forum_forum_topic_url(@new_forum, @forum_topic)
  end
  
  private
  
  def register_view
    @forum_topic.register_view_by(current_user)
  end
  
  def set_page_title
    @page_title = "FiveO Discussion Forums"
  end
  
  def forum_topic_params
    params.require(:forum_topic).permit(:forum_id, :subject, :locked, :pinned, :hidden, initial_post_attributes: [:id, :text, :user_id])
  end
end