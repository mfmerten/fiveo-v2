class VisitorsController < ApplicationController
  before_action :load_jail
  before_action :load_booking, except: [:index, :sign_out, :reset]
  
  def index
    options = {
      path_prefix: 'jail',
      path_options: {jail_id: @jail.id},
      active: true,
      scopes: [:by_date],
      includes: [:booking],
      paginate: true
    }
    @visitors = get_records(options)
    
    @page_title = "Current #{@jail.acronym} Visitors"
    
    @links.push([@jail.acronym, jail_bookings_path(@jail)])
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end
  
  def show
    @visitor = @booking.visitors.find(params[:id])
    @page_title = "#{@jail.acronym} Visitor #{@visitor.id}"
    
    if can?(:update_booking, @jail.update_permission)
      @links.push(['Edit', edit_jail_booking_visitor_path(@jail, @booking, @visitor)])
      @links.push(['Delete', [@jail, @booking, @visitor], method: :delete, data: {confirm: confirm_msg_for(:visitor)}])
    end
    
    @page_links.push([@jail, @booking], @booking.person)
  end
  
  def new
    can!(:update_booking, @jail.update_permission)
    
    @visitor = @booking.visitors.build(visit_date: Date.today, sign_in_time: AppUtils.current_time)
    @page_title = "New Visitor"
  end
  
  def edit
    can!(:update_booking, @jail.update_permission)

    @visitor = @booking.visitors.find(params[:id])
    @page_title = "Edit Visitor #{@visitor.id}"
  end
  
  def create
    redirect_to [@jail, @booking] if params[:commit] == 'Cancel'

    can!(:update_booking, @jail.update_permission)

    @visitor = @booking.visitors.build(visitor_params)
    @page_title = "New Visitor"
    
    success = false
    if @visitor.save
      success = true
      user_action "Visitor ##{@visitor.id} Created."
      
      unless @booking.person.associates.where(name: @visitor.name, relationship: @visitor.relationship).all.size > 0
        unless @booking.person.associates.create(relationship: @visitor.relationship, name: @visitor.name, remarks: "Jail Visitor")
          flash.alert = "Warning: Could not associate visitor to person: Associate Creation Failed!"
        end
      end
    end
    
    respond_to do |format|
      if success
        format.html { redirect_to [@jail, @booking], notice: 'Visitor was successfully created.' }
        format.json { render :show, status: :created, location: [@jail, @booking, @visitor] }
      else
        format.html { render :new }
        format.json { render json: @visitor.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_booking, @jail.update_permission)
    
    @visitor = @booking.visitors.find(params[:id])
    
    redirect_to [@jail, @booking, @visitor] and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Visitor #{@visitor.id}"
    
    success = @visitor.update(visitor_params)
    user_action "Visitor ##{@visitor.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@jail, @booking], notice: 'Visitor was successfully updated.' }
        format.json { render :show, status: :ok, location: [@jail, @booking, @visitor] }
      else
        format.html { render :edit }
        format.json { render json: @visitor.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:update_booking, @jail.update_permission)
    
    @visitor = @booking.visitors.find(params[:id])
    
    @visitor.destroy
    user_action "Visitor ##{@visitor.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to [@jail, @booking], notice: 'Visitor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def sign_out
    can!(:update_booking, @jail.update_permission)

    @visitor = @jail.visitors.find(params[:id])

    if @visitor.sign_out_time?
      flash.alert = "Visitor already signed out!"
      redirect_to jail_visitors_url(@jail) and return
    end
    
    @visitor.update_attribute(:sign_out_time, AppUtils.current_time)
    redirect_to jail_visitors_url(@jail)
    user_action("Signed Out Visitor for #{@jail.acronym} Booking #{@visitor.booking.id}")
  end

  def reset
    can!(:update_booking, @jail.update_permission)
    
    @visitor = @jail.visitors.find(params[:id])
    
    unless @visitor.sign_out_time?
      flash.alert = "Visitor not signed out!"
      redirect_to jail_visitors_url(@jail) and return
    end
    
    @visitor.update_attribute(:sign_out_time, nil)
    redirect_to jail_visitors_url(@jail)
    user_action("Reset Visitor for Booking #{@visitor.booking.id}.")
  end

  protected
  
  def load_jail
    @jail = Jail.find(params[:jail_id])
    can!(@jail.view_permission)
  end
  
  def load_booking
    @booking = @jail.bookings.find(params[:booking_id])
  end
  
  def visitor_params
    params.require(:visitor).permit(:name, :relationship, :street, :city, :state, :zip, :phone, :visit_date, :sign_in_time, :remarks,
      :sign_out_time, :legacy
    )
  end
end