class AbsentsController < ApplicationController
  before_action :load_jail
  
  def index
    options = {
      paginate: true,
      active: true,
      path_prefix: 'jail',
      path_options: {jail_id: @jail.id},
      includes: [:jail, :prisoners, :people],
      scopes: [:by_reverse_id],
    }
    @absents = get_records(options)
    
    if can?(:update_booking, @jail.update_permission)
      @links.push(['New', new_jail_absent_path(@jail)])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @absent = @jail.absents.find(params[:id])
    @page_title = "#{@jail.acronym} Absent #{@absent.id}"
    
    if can?(:update_booking, @jail.update_permission)
      @links.push(['New', new_jail_absent_path(@jail)])
      @links.push(['Copy', copy_jail_absent_path(@jail, @absent), {method: 'post'}])
      @links.push(['Edit', edit_jail_absent_path(@jail, @absent)])
    end
    if can?(:destroy_booking, @jail.update_permission)
      @links.push(['Delete', [@jail, @absent], {method: 'delete', data: {confirm: confirm_msg_for(:absent)}}])
    end
    if can?(:update_booking, @jail.update_permission)
      unless @absent.leave_datetime?
        @links.push(['Leave', leave_jail_absent_path(@jail, @absent), {method: 'post'}])
      end
      if @absent.leave_datetime? && !@absent.return_datetime?
        @links.push(['Return', come_back_jail_absent_path(@jail, @absent), {method: 'post'}])
      end
    end
    @links.push(['Checklist', checklist_jail_absent_path(@jail, @absent), pdf: true])
    
    @page_links.push([@absent.creator,'creator'],[@absent.updater,'updater'])
  end
  
  def new
    can!(:update_booking, @jail.update_permission)
    
    @absent = @jail.absents.build
    @page_title = "New #{@jail.acronym} Absent"
    set_officer_to_session(:escort_officer)
    @absent.absent_prisoners.build
  end

  def edit
    can!(:update_booking, @jail.update_permission)
    
    @absent = @jail.absents.find(params[:id])
    @page_title = "Edit #{@jail.acronym} Absent #{@absent.id}"
    @absent.escort_officer_id = nil
  end
  
  def create
    can!(:update_booking, @jail.update_permission)
    
    redirect_to jail_absents_url(@jail) and return if params[:commit] == 'Cancel'
    
    @absent = @jail.absents.build(absent_params)
    @page_title = "New Absent"
    @absent.created_by = session[:user]
    @absent.updated_by = session[:user]
    
    success = @absent.save
    user_action("Absent ##{@absent.id} created.") if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@jail, @absent], notice: 'Absent was successfully created.' }
        format.json { render :show, status: :created, location: [@jail, @absent] }
      else
        format.html { render :new }
        format.json { render json: @absent.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_booking, @jail.update_permission)
    
    @absent = @jail.absents.find(params[:id])
    
    redirect_to [@jail, @absent] and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Absent #{@absent.id}"
    @absent.updated_by = session[:user]
    
    success = @absent.update(absent_params)
    user_action("Absent ##{@absent.id} updated.") if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@jail, @absent], notice: 'Absent was successfully updated.' }
        format.json { render :show, status: :ok, location: [@jail, @absent] }
      else
        format.html { render :edit }
        format.json { render json: @absent.errors, status: :unprocessable_entity }
      end
    end
  end

  def copy
    can!(:update_booking, @jail.update_permission)
    
    old = @jail.absents.find(params[:id])
    @absent = @jail.absents.build(absent_type: old.absent_type)
    @page_title = "New #{@jail.acronym} Absent"
    set_officer_to_session(:escort_officer)
    @absent.prisoner_ids = old.prisoner_ids
    
    render 'new'
  end

  def leave
    can!(:update_booking, @jail.update_permission)
    
    @absent = @jail.absents.find(params[:id])
    @absent.update_attribute(:leave_datetime, Time.now)
    @absent.update_attribute(:updated_by, session[:user])
    
    user_action("Absent ##{@absent.id} Left")
    flash.notice = "Absent ##{@absent.id} Left"
    
    redirect_to [@jail, @absent]
  end

  def come_back
    can!(:update_booking, @jail.update_permission)
    
    @absent = @jail.absents.find(params[:id])
    @absent.update_attribute(:return_datetime, Time.now)
    @absent.update_attribute(:updated_by, session[:user])
    
    user_action("Absent ##{@absent.id} Returned")
    flash.notice = "Absent ##{@absent.id} Returned"
    
    redirect_to [@jail, @absent]
  end

  def destroy
    can!(:update_booking, @jail.update_permission)
    
    @absent = @jail.absents.find(params[:id])
    @absent.destroy

    respond_to do |format|
      format.html { redirect_to jail_absents_url(@jail), notice: 'Absent was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def checklist
    @absent = @jail.absents.find(params[:id])
    
    if @absent.prisoners.empty?
      flash.alert = "Nothing to report!"
      redirect_to [@jail, @absent] and return
    end
    
    if pdf_url = make_pdf_file("absent_checklist", Absents::Checklist.new(@absent))
      redirect_to pdf_url
      user_action("Printed Prisoner Checklist for #{@jail.acronym} Absent #{@absent.id}")
    else
      flash.alert = "Count not create temporary PDF file for download!"
      redirect_to [@jail, @absent]
    end
  end
  
  protected

  def load_jail
    @jail = Jail.find(params[:jail_id])
    can!(@jail.view_permission)
  end
  
  def absent_params
    params.require(:absent).permit(:absent_type, :escort_officer_id, :escort_officer_badge, :escort_officer_unit, :escort_officer,
      :leave_datetime, :return_datetime, absent_prisoners_attributes: [:id, :booking_id, :_destroy])
  end
end
