class RevocationsController < ApplicationController
  before_action :load_docket
  
  # no index
  
  def show
    @revocation = @docket.revocations.find(params[:id])
    @page_title = "Revocation #{@revocation.id}"
    
    if can?(:update_court)
      @links.push(['New', new_docket_revocation_path(@docket)])
      @links.push(['Edit', edit_docket_revocation_path(@docket, @revocation)])
    end
    if can?(:destroy_court)
      @links.push(['Delete', [@docket, @revocation], {method: :delete, data: {confirm: confirm_msg_for(:revocation)}}])
    end
    
    @page_links.push(@revocation.docket.person,@revocation.docket)
    @page_links.concat(@revocation.docket.probations)
    @page_links.push(@revocation.doc_record,[@revocation.hold.jail, @revocation.hold.booking, @revocation.hold],@revocation.arrest,[@revocation.creator,'creator'],[@revocation.updater,'updater'])
  end
  
  def new
    can!(:update_court)

    @revocation = @docket.revocations.build
    @revocation.total_hours, @revocation.total_days, @revocation.total_months, @revocation.total_years = @revocation.total_of_sentences
    @page_title = "New Revocation"
  end

  def edit
    can!(:update_court)

    @revocation = @docket.revocations.find(params[:id])
    @page_title = "Edit Revocation #{@revocation.id}"
    @revocation.consecutives = @revocation.consecutive_dockets.collect{|d| d.docket_no}.join(',')
  end
  
  def create
    can!(:update_court)

    redirect_to @docket and return if params[:commit] == 'Cancel'

    @revocation = @docket.revocations.build(revocation_params)
    @page_title = "New Revocation"
    @revocation.created_by = session[:user]
    @revocation.updated_by = session[:user]

    success = @revocation.save
    user_action "Revocation ##{@revocation.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@docket, @revocation], notice: 'Revocation was successfully created.' }
        format.json { render :show, status: :created, location: [@docket, @revocation] }
      else
        format.html { render :new }
        format.json { render json: @revocation.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_court)
    
    @revocation = @docket.revocations.find(params[:id])
    
    redirect_to [@docket, @revocation] and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Revocation #{@revocation.id}"
    @revocation.updated_by = session[:user]
    
    success = @revocation.update_attributes(revocation_params)
    user_action "Revocation ##{@revocation.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@docket, @revocation], notice: 'Revocation was successfully updated.' }
        format.json { render :show, status: :ok, location: [@docket, @revocation] }
      else
        format.html { render :edit }
        format.json { render json: @revocation.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:destroy_court)

    @revocation = @docket.revocations.find(params[:id])

    @revocation.destroy
    user_action "Revocation ##{@revocation.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to @docket, notice: 'Revocation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def force_processing
    can!(:update_court)
    
    @revocation = @docket.revocations.find(params[:id])
    @revocation.force = true
    @revocation.save(validate: false)
    user_action("Forced processing for Revocation #{@revocation.id}.")
    redirect_to [@docket, @revocation]
  end
  
  protected
  
  def load_docket
    @docket = Docket.find(params[:docket_id])
  end
  
  def revocation_params
    params.require(:revocation).permit(:revocation_date, :arrest_id, :judge, :doc, :revoked, :total_hours, :total_days, :total_months,
      :total_years, :total_life, :total_death, :consecutive_type, :consecutives, :parish_hours, :parish_days, :parish_months, :parish_years,
      :time_served, :deny_goodtime, :delay_execution, :remarks
    )
  end
end
