class RootController < ApplicationController
  
  def index
    @page_title = "Home Page"
    @announcements = Announcement.includes(:ignorers, {updater: [:contact]}).order(updated_at: :desc).all.reject{|a| a.ignorers.include?(current_user)}
    unless session[:calendar].is_a?(Date)
      session[:calendar] = Date.today
    end
    @links.push(["Show All", all_announcements_path])
  end
  
  def get_calendar
    unless session[:calendar].is_a?(Date)
      session[:calendar] = Date.today
    end
    case params[:period]
    when "next_month"
      session[:calendar] = session[:calendar] + 1.month
    when "next_year"
      session[:calendar] = session[:calendar] + 1.year
    when "previous_month"
      session[:calendar] = session[:calendar] - 1.month
    when "previous_year"
      session[:calendar] = session[:calendar] - 1.year
    else
      session[:calendar] = Date.today
    end
    
    respond_to :js
  end
  
  def h_to_hdmy
    @diff = @total_hours = @hours = @days = @months = @years = @start_date = @stop_date = start_datetime = stop_datetime = nil
    
    if params[:start_date]
      @start_date = AppUtils.valid_date(params[:start_date], allow_nil: false)
      start_datetime = AppUtils.get_datetime(@start_date,"00:00")
    else
      @start_date = AppUtils.valid_date(params[:conv_start_date], allow_nil: false)
      start_datetime = AppUtils.get_datetime(@start_date,"00:00")
      @stop_date = current_user.AppUtils.valid_date(params[:conv_stop_date], allow_nil: false)
      stop_datetime = AppUtils.get_datetime(@stop_date,"00:00")
    end
    @valid = false
    @section = 0
    
    if params[:conv_total_hours]
      # first section changed
      @total_hours = params[:conv_total_hours].to_i
      if @total_hours >= 0
        @hours, @days, @months, @years = AppUtils.hours_to_sentence(@total_hours)
        @stop_date = (start_datetime + @total_hours.hours).to_date
        @valid = true
        @section = 1
      end
    
    elsif params[:conv_hours]
      # middle section changed
      @hours = params[:conv_hours]
      @days = params[:conv_days]
      @months = params[:conv_months]
      @years = params[:conv_years]
      @total_hours = AppUtils.sentence_to_hours(@hours,@days,@months,@years)
      @stop_date = (start_datetime + @total_hours.hours).to_date
      @valid = true
      @section = 2
    
    else
      # assume bottom section changed
      @diff = AppUtils.hours_diff(start_datetime, stop_datetime)
      @hours, @days, @months, @years = AppUtils.hours_to_sentence(@diff)
      @valid = true
      @section = 3
    end
    
    respond_to :js
  end

  def tools
    @page_title = "Tools"
  end

  def preview_popup
    @text = params[:value]
    @page_title = 'Preview'
    render layout: 'popup'
  end
end
