class MessageLogsController < ApplicationController

  def index
    options = {
      scopes: [:by_reverse_id],
      includes: [:sender, :receiver],
      paginate: true
    }
    @message_logs = get_records(options)
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @message_log = MessageLog.find(params[:id])
    @page_title = "Message Log #{@message_log.id}"
    
    unless @message_log.receiver.nil?
      @page_links.push([@message_log.receiver.contact,'receiver'])
    end
    unless @message_log.sender.nil?
      @page_links.push([@message_log.sender.contact,'sender'])
    end
  end
end
