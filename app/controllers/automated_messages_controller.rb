class AutomatedMessagesController < AbstractController::Base
  include AbstractController::Rendering
  include AbstractController::Helpers
  include AbstractController::Logger
  include Rails.application.routes.url_helpers
  
  helper ApplicationHelper
  
  self.view_paths = "app/views/automated_messages"
  
  # render notification message body
  def notify(object, style)
    @record = object
    @style = style
    render template: "#{object.class.to_s.tableize.singularize}_#{style}"
  end
end