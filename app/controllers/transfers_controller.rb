class TransfersController < ApplicationController
  before_action :load_jail, except: :update_transfer
  before_action :load_booking, except: [:index, :search, :update_transfer]

  def index
    options = {
      scopes: [:by_reverse_id],
      includes: [:person, :booking],
      paginate: true
    }
    @transfers = get_records(options)
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @transfer = @booking.transfers.find(params[:id])
    @page_title = "Transfer #{@transfer.id}"
    
    if can?(:destroy_booking)
      @links.push(["Delete", [@jail, @booking, @transfer], {method: :delete, data: {confirm: confirm_msg_for(:transfer)}}])
    end
    
    @page_links.push(@transfer.person, [@jail, @transfer.booking], [@transfer.creator,'creator'],[@transfer.updater,'updater'])
  end
  
  # this displays a generic 'select transfer type' form which then calls a 
  # remote function (update_transfer) to generate the correct transfer form
  # depending on type of transfer selected.
  def new
    can!(:update_booking)
    # determine all valid transfer types for the current booking status
    # and set @transfer_types accordingly
    if @booking.is_transferred
      # currently transferred out, can only transfer back
      tname = 'Temporary Transfer Return (IN)'
      @types = [[tname,tname]]
    elsif @booking.is_housing
      # currently temporary housing, can return (out) or temp transfer (out)
      @types = []
      ['Temporary Housing Return (OUT)','Temporary Transfer (OUT)'].each do |t|
        @types.push([t,t])
      end
    else
      # anything goes except the returns
      @types = []
      ['DOC Transfer (IN)', 'Temporary Housing (IN)', 'Temporary Transfer (OUT)', 'Permanent Transfer (OUT)'].each do |t|
        @types.push([t,t])
      end
    end
    @types.unshift(['---',nil])
    @transfer_selector = @booking.transfers.build(person_id: @booking.person_id)
    @page_title = "New Transfer"
  end
  
  # no edit
  
  def create
    redirect_to [@jail, @booking] and return if params[:commit] == "Cancel"
    
    @transfer = @booking.transfers.build(transfer_params)
    @page_title = "New Transfer"
    @transfer.created_by = session[:user]
    @transfer.updated_by = session[:user]
    
    success = @transfer.save
    user_action "Transfer ##{@transfer.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@jail, @booking], notice: 'Transfer was successfully created.' }
        format.json { render :show, status: :created, location: [@jail, @booking, @transfer] }
      else
        format.html { render :new }
        format.json { render json: @transfer.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # no update

  def destroy
    can!(:destroy_booking)
    
    @transfer = @booking.transfers.find(params[:id])
    
    unless @transfer.can_be_deleted?
      flash.alert = "Cannot delete this transfer!"
      redirect_to [@jail, @booking] and return
    end
    
    @transfer.destroy
    user_action "Transfer ##{@transfer.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to [@jail, @booking], notice: 'Transfer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  # this is a remote function for the 'new' transfer action that generates the
  # correct form depending on the type of transfer selected.
  def update_transfer
    valid = true
    unless can?(:update_booking) && params[:transfer_type] && params[:booking_id] && (@booking = Booking.find_by_id(params[:booking_id]))
      valid = false
    end
    unless Transfer::TransferType.include?(params[:transfer_type])
      valid = false
    end
    if valid
      @jail = @booking.jail
      @person = @booking.person
      @transfer = @booking.transfers.build(
        person_id: @booking.person_id,
        transfer_type: params[:transfer_type],
        transfer_datetime: Time.now,
        from_agency: '',
        from_officer: '',
        from_officer_unit: '',
        from_officer_badge: '',
        to_agency: '',
        to_officer: '',
        to_officer_unit: '',
        to_officer_badge: '',
        remarks: ''
      )
      case @transfer.transfer_type
      when "DOC Transfer (IN)", "Temporary Housing (IN)", "Temporary Transfer Return (IN)"
        @transfer.to_agency = SiteConfig.agency_acronym
        @transfer.to_officer = session[:user_name]
        @transfer.to_officer_unit = session[:user_unit]
        @transfer.to_officer_badge = session[:user_badge]
        if @transfer.transfer_type == "Temporary Transfer Return (IN)"
          if hold = @booking.holds.reject{|h| h.cleared_datetime? || h.hold_type != 'Temporary Transfer'}.first
            @transfer.from_agency = hold.agency
          end
        end
      when "Permanent Transfer (OUT)", "Temporary Transfer (OUT)", "Temporary Housing Return (OUT)"
        @transfer.from_agency = SiteConfig.agency_acronym
        @transfer.from_officer = session[:user_name]
        @transfer.from_officer_unit = session[:user_unit]
        @transfer.from_officer_badge = session[:user_badge]
        if @transfer.transfer_type == "Temporary Housing Return (OUT)"
          if hold = @booking.holds.reject{|h| h.cleared_datetime? || h.hold_type != 'Temporary Housing'}.first
            @transfer.to_agency = hold.agency
          end
        end
      end
    else
      @transfer = nil
    end

    respond_to :js
  end
  
  protected
  
  def load_jail
    @jail = Jail.find(params[:jail_id])
    can!(@jail.view_permission)
  end
  
  def load_booking
    @booking = Booking.find(params[:booking_id])
  end
  
  def transfer_params
    params.require(:transfer).permit(:transfer_type, :person_id, :transfer_datetime, :from_agency_id, :from_agency, :from_officer_id,
      :from_officer_badge, :from_officer_unit, :from_officer, :to_agency_id, :to_agency, :to_officer_id, :to_officer_badge,
      :to_officer_unit, :to_officer, :remarks
    )
  end
end