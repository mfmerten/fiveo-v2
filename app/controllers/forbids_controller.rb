class ForbidsController < ApplicationController
  
  def index
    options = {
      active: true,
      scopes: [:by_name],
      includes: [:person],
      paginate: true
    }
    @forbids = get_records(options)
    
    if can?(:update_forbid)
      @links.push(['New', new_forbid_path ])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @forbid = Forbid.find(params[:id])
    @page_title = "Forbid #{@forbid.id}"
   
    if can?(:update_forbid)
      @links.push(['Edit', edit_forbid_path(@forbid)])
    end
    if can?(:destroy_forbid)
      @links.push(['Delete', @forbid, {method: 'delete', data: {confirm: confirm_msg_for(:forbid)}}])
    end
    @links.push(['Forbid', forbid_form_forbid_path(id: @forbid.id), pdf: true])
    
    @page_links.push(@forbid.person,@forbid.call)
    @page_links.concat(@forbid.offenses)
    @page_links.push([@forbid.creator,'creator'],[@forbid.updater,'updater'])
    
    @matches = Person.for_forbid(@forbid.id).reject{|p| p.forbid_exemptions.include?(@forbid)}
  end

  def new
    can!(:update_forbid)

    if params[:offense_id]
      offense = Offense.find(params[:offense_id])
      @forbid = offense.forbids.new(call_id: offense.call_id, case_no: offense.case_no)
    elsif params[:call_id]
      call = Call.find(params[:call_id])
      @forbid = call.forbids.new(case_no: call.case_no)
    else
      @forbid = Forbid.new
    end
    set_officer_to_session(@forbid, :receiving_officer)
    @forbid.request_date = Date.today
    
    unless @forbid.call.nil?
      unless @forbid.call.call_subjects.nil?
        @forbid.call.call_subjects.each do |s|
          if s.subject_type == 'Complainant'
            @forbid.complainant = s.sort_name
            @forbid.comp_street = s.address1
            @forbid.comp_city_state_zip = s.address2
            @forbid.comp_phone = s.home_phone
          end
          if s.subject_type == 'Subject'
            @forbid.full_name = s.sort_name
            @forbid.forbid_street = s.address1
            @forbid.forbid_city_state_zip = s.address2
            @forbid.forbid_phone = s.home_phone
          end
        end
      end
    end
    
    if params[:person_id]
      @forbid.forbidden_id = params[:person_id]
      unless @forbid.person.nil?
        @forbid.full_name = @forbid.person.sort_name
        @forbid.forbid_street = @forbid.person.physical_line1
        @forbid.forbid_city_state_zip = @forbid.person.physical_line2
        @forbid.forbid_phone = @forbid.person.home_phone
      end
    end
    
    @page_title = "New Forbid"
  end

  def edit
    can!(:update_forbid)

    @forbid = Forbid.find(params[:id])
    @page_title = "Edit Forbid #{@forbid.id}"
    @forbid.full_name = @forbid.sort_name
    @forbid.receiving_officer_id = nil
    @forbid.serving_officer_id = nil
  end
  
  def create
    redirect_to forbids_url and return if params[:commit] == 'Cancel'

    can!(:update_forbid)

    @forbid = Forbid.new(forbid_params)
    @page_title = "New Forbid"
    @forbid.created_by = session[:user]
    @forbid.updated_by = session[:user]

    success = @forbid.save
    user_action "Forbid ##{@forbid.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @forbid, notice: 'Forbid was successfully created.' }
        format.json { render :show, status: :created, location: @forbid }
      else
        format.html { render :new }
        format.json { render json: @forbid.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_forbid)

    @forbid = Forbid.find(params[:id])

    redirect_to @forbid if params[:commit] == 'Cancel'

    @page_title = "Edit Forbid #{@forbid.id}"
    @forbid.updated_by = session[:user]
    @forbid.legacy = false

    success = @forbid.update(forbid_params)
    user_action "Forbid ##{@forbid.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @forbid, notice: 'Forbid was successfully updated.' }
        format.json { render :show, status: :ok, location: @forbid }
      else
        format.html { render :edit }
        format.json { render json: @forbid.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_forbid)

    @forbid = Forbid.find(params[:id])

    @forbid.destroy
    user_action "Forbid ##{@forbid.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to forbids_url, notice: 'Forbid was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def forbid_form
    @forbid = Forbid.find(params[:id])
    
    if pdf_url = make_pdf_file("forbid", Forbids::Form.new(@forbid))
      redirect_to pdf_url
      user_action("Printed Forbid.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to @forbid
    end
  end
  
  def update_person
    @name = ""
    @addr1 = ""
    @addr2 = ""
    @phone = ""
    @valid = false
    if can?(:view_person)
      @name = "Invalid Person ID"
      if params[:c] && person = Person.find_by_id(params[:c])
        @name = person.sort_name
        @addr1 = person.physical_line1
        @addr2 = person.physical_line2
        @phone = person.home_phone
        @valid = true
      end
    else
      @name = "Permission Denied!"
    end
    
    respond_to :js
  end
  
  private
  
  def forbid_params
    params.require(:forbid).permit(:forbidden_id, :full_name, :forbid_street, :forbid_city_state_zip, :forbid_phone, :signed_date,
      :recall_date, :serving_officer_id, :serving_officer_badge, :serving_officer_unit, :serving_officer, :details, :complainant,
      :comp_street, :comp_city_state_zip, :comp_phone, :request_date, :receiving_officer_id, :receiving_officer_badge,
      :receiving_officer_unit, :receiving_officer, :call_id, :case_no, :remarks
    )
  end
end
