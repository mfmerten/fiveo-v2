class ActivitiesController < ApplicationController
  
  def index
    options = {
      scopes: [:by_reverse_id],
      includes: [:user],
      paginate: true
    }
    @activities = get_records(options)
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end
end
