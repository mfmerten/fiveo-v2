class PaperCustomersController < ApplicationController
  
  def index
    options = {
      active: true,
      scopes: [:by_name],
      includes: [:papers, :payments],
      paginate: true
    }
    @paper_customers = get_records(options)

    if can?(:update_civil)
      @links.push(['New', new_paper_customer_path])
    end

    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end
  
  def show
    @paper_customer = PaperCustomer.find(params[:id])
    @page_title = "Paper Customer #{@paper_customer.id}"
    
    if can?(:update_civil)
      @links.push(['New', new_paper_customer_path])
      @links.push(['Edit', edit_paper_customer_path(@paper_customer)])
    end
    if can?(:destroy_civil)
      @links.push(['Delete', @paper_customer, {method: 'delete', data: {confirm: confirm_msg_for(:paper_customer)}}])
    end
    if can?(:update_civil)
      @links.push([(@paper_customer.disabled? ? "Enable" : "Disable"), toggle_customer_paper_customer_path(id: @paper_customer.id), {method: 'post'}])
      unless @paper_customer.papers.reject{|p| p.invoice_datetime? }.empty?
        @links.push(['Invoice', invoices_papers_path(customer_id: @paper_customer.id), {method: 'post'}])
      end
    end
    
    @payments = @paper_customer.past_days(30).reverse
    @invoices = @paper_customer.papers.is_invoice
  end

  def new
    can!(:update_civil)

    @paper_customer = PaperCustomer.new
    @page_title = "New Customer"
    @paper_customer.state = SiteConfig.agency_state
    @paper_customer.noservice_fee = SiteConfig.noservice_fee
    @paper_customer.mileage_fee = SiteConfig.mileage_fee
  end
  
  def edit
    can!(:update_civil)

    @paper_customer = PaperCustomer.find(params[:id])
    @page_title = "Edit Customer #{@paper_customer.id.to_s}"
  end
  
  def create
    can!(:update_civil)

    redirect_to paper_customers_url and return if params[:commit] == 'Cancel'

    @paper_customer = PaperCustomer.new(paper_customer_params)
    @page_title = "New Customer"
    @paper_customer.created_by = session[:user]
    @paper_customer.updated_by = session[:user]

    success = @paper_customer.save
    user_action "Paper Customer ##{@paper_customer.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @paper_customer, notice: 'Paper Customer was successfully created.' }
        format.json { render :show, status: :created, location: @paper_customer }
      else
        format.html { render :new }
        format.json { render json: @paper_customer.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_civil)

    @paper_customer = PaperCustomer.find(params[:id])

    redirect_to @paper_customer and return if params[:commit] == 'Cancel'

    @page_title = "Edit Customer #{@paper_customer.id.to_s}"
    @paper_customer.updated_by = session[:user]

    success = @paper_customer.update(paper_customer_params)
    user_action "Paper Customer ##{@paper_customer.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @paper_customer, notice: 'Paper Customer was successfully updated.' }
        format.json { render :show, status: :ok, location: @paper_customer }
      else
        format.html { render :edit }
        format.json { render json: @paper_customer.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_civil)

    @paper_customer = PaperCustomer.find(params[:id])

    if reasons = @paper_customer.reasons_not_destroyable
      flash.alert = "Cannot delete: #{reasons}"
      redirect_to @paper_customer and return
    end
    
    @paper_customer.destroy
    user_action "Paper Customer ##{@paper_customer.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to paper_customers_url, notice: 'Paper Customer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def update_fees
    can!(:update_civil)

    status = params[:status]

    unless ['noservice','mileage','all'].include?(status)
      flash.alert = 'Invalid status specified!'
      redirect_to paper_customers_url and return
    end
    
    if params[:id]
      @paper_customer = PaperCustomer.find(params[:id])
      if status == 'noservice'
        @paper_customer.update_noservice_fee(SiteConfig.noservice_fee)
      end
      if status == 'mileage'
        @paper_customer.update_mileage_fee(SiteConfig.mileage_fee)
      end
      user_action("Updated Customer #{@paper_customer.id} - #{status.titleize} Fee(s).")
      redirect_to @paper_customer
    else
      if status == 'noservice' || status == 'all'
        PaperCustomer.update_noservice_fee(SiteConfig.noservice_fee)
      end
      if status == 'mileage' || status == 'all'
        PaperCustomer.update_mileage_fee(SiteConfig.mileage_fee)
      end
      user_action("Updated All Customers #{status.titleize} Fee(s).")
      redirect_to paper_customers_url
    end
  end

  def toggle_customer
    can!(:update_civil)
    
    @paper_customer = PaperCustomer.find(params[:id])
    @paper_customer.toggle!(:disabled)
    
    if @paper_customer.disabled?
      user_action("Disabled Customer #{@paper_customer.id}.")
    else
      user_action("Enabled Customer #{@paper_customer.id}.")
    end
    redirect_to @paper_customer
  end

  private
  
  def paper_customer_params
    params.require(:paper_customer).permit(:name, :disabled, :street, :street2, :city, :state, :zip, :phone, :fax, :attention,
      :noservice_fee, :mileage_fee, :remarks
    )
  end
end