class HoldsController < ApplicationController
  before_action :load_jail
  before_action :load_booking, except: [:index, :search]

  def index
    options = {
      path_prefix: 'jail',
      path_options: {jail_id: @jail.id},
      active: true,
      scopes: [:by_reverse_date],
      includes: [:booking, :arrest, :blockers, :sentence, :doc_record],
      paginate: true
    }
    @holds = get_records(options)
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @hold = @booking.holds.find(params[:id])
    @page_title = "Hold #{@hold.id}"
    
    if can?(:update_booking) && @hold.hold_type != 'Temporary Release'
      @links.push(['Edit', edit_jail_booking_hold_path(@jail, @booking, @hold)])
    end
    if can?(:destroy_booking) && (@hold.hold_type != 'Temporary Release' || (@hold.hold_type == 'Temporary Release' && @hold.cleared_datetime?))
      @links.push(['Delete', [@jail, @booking, @hold], {method: 'delete', data: {confirm: confirm_msg_for(:hold)}}])
    end
    unless @booking.release_datetime?
      if can?(:update_booking) && @hold.hold_type != 'Temporary Release' && @hold.cleared_datetime? && !@booking.release_datetime?
        @links.push(['Reactivate', clear_release_jail_booking_hold_path(@jail, @booking, @hold), {method: 'post'}])
      end
      if can?(:update_booking)
        unless @hold.cleared_datetime? || @hold.hold_type.blank?
          if @hold.can_bond?
            if @hold.satisfied?
              @links.push(['Clear', clear_jail_booking_hold_path(@jail, @booking, @hold, cause: 'Bonded')])
            else
              @links.push(['Add Bond', new_bond_path(hold_id: @hold.id)])
              @links.push(['Clear WO Bond', clear_jail_booking_hold_path(@jail, @booking, @hold, cause: 'Manual Release')])
            end
          else
            if @hold.hold_type == '72 Hour'
              @links.push(['Clear', seventytwo_arrest_path(@hold.arrest, booking_id: @booking.id)])
            elsif @hold.hold_type == 'Temporary Transfer' || @hold.hold_type == 'Temporary Housing'
              if @hold.hold_type == 'Temporary Transfer' && can?(:update_billing)
                @links.push([(@hold.other_billable? ? 'Not Billable' : 'Billable'), toggle_bill_jail_booking_hold_path(@jail, @booking, @hold)])
              end
              @links.push(['Clear', new_jail_booking_transfer_path(@jail, @booking)])
            elsif @hold.hold_type == 'Temporary Release'
              @links.push(['Return', temp_release_in_jail_booking_path(@jail, @booking, hold_id: @hold.id), {method: 'post'}])
            else
              @links.push(['Clear', clear_jail_booking_hold_path(@jail, @booking, @hold)])
            end
          end
        end
        if !@hold.hold_type.blank? && (@hold.hold_type == 'Serving Sentence' || @hold.hold_type == 'Fines/Costs')
          @links.push(['Adjustment', adjustment_jail_booking_hold_path(@jail, @booking, @hold)])
        end
      end
    end

    @page_links.push(@hold.arrest, [@jail, @booking], @booking.person, [@jail, @booking, @hold.transfer], @hold.doc_record, @hold.sentence, @hold.revocation, @hold.probation,[@hold.creator,'creator'],[@hold.updater,'updater'])
  end

  def new
    can!(:update_booking)

    @hold = @booking.holds.build(hold_datetime: Time.now, hold_by_badge: session[:user_badge], hold_by: session[:user_name], hold_by_unit: session[:user_unit])
    @page_title = "New Hold"
  end
  
  def edit
    can!(:update_booking)

    @hold = @booking.holds.find(params[:id])
    @page_title = "Edit Hold #{@hold.id}"
  end
  
  def create
    can!(:update_booking)

    redirect_to [@jail, @booking] and return if params[:commit] == 'Cancel'

    @hold = @booking.holds.build(hold_params)
    @page_title = "New Hold"
    @hold.created_by = session[:user]
    @hold.updated_by = session[:user]
    
    success = @hold.save
    user_action "Hold ##{@hold.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@jail, @booking], notice: 'Hold was successfully created.' }
        format.json { render :show, status: :created, location: [@jail, @booking] }
      else
        format.html { render :new }
        format.json { render json: @hold.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_booking)
    
    @hold = @booking.holds.find(params[:id])
    
    redirect_to [@jail, @booking, @hold] and return if params[:commit] == 'Cancel'
    
    @hold.updated_by = session[:user]
    @hold.legacy = false
    @page_title = "Edit Hold #{@hold.id}"
    
    success = @hold.update(hold_params)
    user_action "Hold ##{@hold.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@jail, @booking], notice: 'Hold was successfully updated.' }
        format.json { render :show, status: :ok, location: [@jail, @booking] }
      else
        format.html { render :edit }
        format.json { render json: @hold.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:destroy_booking)
    
    @hold = @booking.holds.find(params[:id])
    
    if reasons = @hold.reasons_not_destroyable
      flash.alert = "Cannot delete: #{reasons}"
      redirect_to [@jail, @booking, @hold] and return
    end
    
    @hold.destroy
    user_action "Hold ##{@hold.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to [@jail, @booking], notice: 'Hold was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def clear
    can!(:update_booking)
    
    @hold = @booking.holds.find(params[:id])
    @page_title = "Clear Hold #{@hold.id}"
    if params[:cause] && Hold::ClearType.include?(params[:cause])
      @hold.cleared_because = params[:cause]
    end
    @hold.cleared_datetime = Time.now
    set_officer_to_session(@hold, :cleared_by)
  end

  def clear_update
    can!(:update_booking)

    @hold = @booking.holds.find(params[:id])

    redirect_to [@jail, @booking] and return if params[:commit] == 'Cancel'

    @page_title = "Clear Hold #{@hold.id}"
    @hold.updated_by = session[:user]
    @hold.legacy = false

    if @hold.update_attributes(hold_params)
      user_action "Hold ##{@hold.id} Cleared."
      flash.notice = "Hold was successfully cleared."
      redirect_to [@jail, @booking] and return
    end
    render 'clear'
  end
  
  def toggle_bill
    can!(:update_billing)

    @hold = @booking.holds.find(params[:id])
    @hold.toggle!(:other_billable)

    if @hold.other_billable?
      user_action("Flagged Hold #{@hold.id} as Billable!")
    else
      user_action("Flagged Hold #{@hold.id} as Not Billable!")
    end
    redirect_to [@jail, @booking]
  end

  def clear_release
    can!(:update_booking)

    @hold = @booking.holds.find(params[:id]) 

    unless @hold.cleared_datetime?
      flash.alert = 'This hold already active!'
      redirect_to [@jail, @booking, @hold] and return
    end
    
    if @hold.booking.release_datetime?
      flash.alert = 'Booking must be open before reactivating a hold!'
      redirect_to [@jail, @booking] and return
    end
    
    if @hold.clear_release(session[:user])
      user_action "Hold ##{@hold.id} Reactivated."
      flash.notice = "Hold was successfully reactivated."
    else
      flash.alert = "Could not reactivate Hold!"
    end
    redirect_to [@jail, @booking, @hold]
  end

  def adjustment
    can!(:update_booking)

    @hold = @booking.holds.find(params[:id])
    
    if @hold.hold_type.blank?
      flash.alert = "Hold has invalid Hold Type"
      redirect_to [@jail, @booking, @hold] and return
    end
    
    if @hold.hold_type != 'Serving Sentence' && @hold.hold_type != 'Fines/Costs'
      flash.alert = "Only Serving Sentence or Fines/Costs Holds may be adjusted!"
      redirect_to [@jail, @booking, @hold] and return
    end
    
    @hold_adjustment = HoldAdjustment.new
    @page_title = "Adjusting Time Counters for Hold #{@hold.id}"
  end
  
  def adjustment_update
    can!(:update_booking)
    
    @hold = @booking.holds.find(params[:id])
    
    if @hold.hold_type.blank?
      flash.alert = "Hold has invalid Hold Type"
      redirect_to [@jail, @booking, @hold] and return
    end
    
    if @hold.hold_type != 'Serving Sentence' && @hold.hold_type != 'Fines/Costs'
      flash.alert = "Only Serving Sentence or Fines/Costs Holds may be adjusted!"
      redirect_to [@jail, @booking, @hold] and return
    end
    
    @page_title = "Adjusting Time Counters for Hold #{@hold.id}"
    # this looks a bit wierd but HoldAdjustment is ActiveAttr model, not
    # ActiveRecord and has no table
    
    @hold_adjustment = HoldAdjustment.new(params[:hold_adjustment])
    if @hold_adjustment.valid?
      @hold.hours_total += @hold_adjustment.hours_total_adjustment
      @hold.hours_served += @hold_adjustment.hours_served_adjustment
      @hold.hours_goodtime += @hold_adjustment.hours_goodtime_adjustment
      @hold.hours_time_served += @hold_adjustment.hours_time_served_adjustment
      if params[:commit] == 'Preview'
        render 'adjustment'
      else
        if @hold.save
          redirect_to [@jail, @booking, @hold]
          user_action "Hold ##{@hold.id} Adjusted."
          flash.notice = "Hold was successfully adjusted."
        end
      end
    end
    render 'adjustment'
  end

  protected
  
  def load_jail
    @jail = Jail.find(params[:jail_id])
    can!(@jail.view_permission)
  end
  
  def load_booking
    @booking = @jail.bookings.find(params[:booking_id])
  end
  
  def hold_params
    params.require(:hold).permit(:cleared_datetime, :cleared_because, :cleared_by_id, :cleared_by_badge, :cleared_by_unit, :cleared_by,
      :explanation, :agency_id, :agency, :transfer_officer_id, :transfer_officer_badge, :transfer_officer_unit, :transfer_officer, :remarks
    )
  end
end
