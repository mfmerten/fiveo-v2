class AnnouncementsController < ApplicationController
  
  def index
    options = {
      includes: [:creator],
      paginate: true,
      scopes: [:by_reverse_updated]
    }
    @announcements = get_records(options)
    
    if can?(:update_announcement)
      @links.push(["New", new_announcement_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @announcement = Announcement.find(params[:id])
    @page_title = "Announcement #{@announcement.id}"
    
    if can?(:update_announcement)
      @links.push(["New", new_announcement_path])
      @links.push(["Edit", edit_announcement_path(@announcement)])
    end
    if can?(:destroy_announcement)
      @links.push(["Delete", @announcement, {method: 'delete', data: { confirm: confirm_msg_for(:announcement)}}])
    end
    
    @page_links.push([@announcement.creator, 'Creator'], [@announcement.updater, 'Updater'])
  end

  def all
    @announcements = Announcement.by_reverse_updated
    @page_title = "All Announcements"
    
    @links.push(["Home", root_path])
  end
  
  def new
    can!(:update_announcement)
    
    @announcement = Announcement.new
    @page_title = "New Announcement"
  end
  
  def edit
    can!(:update_announcement)
    
    @announcement = Announcement.find(params[:id])
    @page_title = "Edit Announcement #{@announcement.id}"
  end
  
  def create
    can!(:update_announcement)
    
    redirect_to announcements_url and return if params[:commit] == 'Cancel'
    
    @announcement = Announcement.new(announcement_params)
    @page_title = "New Announcement"
    @announcement.created_by = session[:user]
    @announcement.updated_by = session[:user]

    success = @announcement.save
    user_action("Announcement ##{@announcement.id} Created.") if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @announcement, notice: 'Announcement was successfully created.' }
        format.json { render :show, status: :created, location: @announcement }
      else
        format.html { render :new }
        format.json { render json: @announcement.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_announcement)
    
    @announcement = Announcement.find(params[:id])
    
    redirect_to @announcement and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Announcement #{@announcement.id}"
    @announcement.updated_by = session[:user]

    success = @announcement.update(announcement_params)
    user_action("Announcement ##{@announcement.id} Updated.") if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @announcement, notice: 'Announcement was successfully updated.' }
        format.json { render :show, status: :ok, location: @announcement }
      else
        format.html { render :edit }
        format.json { render json: @announcement.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:destroy_announcement)
    
    @announcement = Announcement.find(params[:id])
    
    @announcement.destroy
    user_action("Announcement ##{@announcement.id} Deleted.")

    respond_to do |format|
      format.html { redirect_to announcements_url, notice: 'Announcement was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def toggle
    @announcement = Announcement.find(params[:id])
    unless @announcement.unignorable?
      if @announcement.ignorers.include?(current_user)
        @announcement.ignorers.delete(current_user)
        user_action("Unignored Announcement #{@announcement.id}!")
      else
        @announcement.ignorers << current_user
        @announcement.save(validate: false)
        user_action("Ignored Announcement #{@announcement.id}!")
      end
    end
    if params[:origin] == "announcements"
      redirect_to all_announcements_url
    else
      redirect_to root_url
    end
  end
  
  private
  
  def announcement_params
    params.require(:announcement).permit(:subject, :unignorable, :body)
  end
end
