class ProbationPaymentsController < ApplicationController
  before_action :load_probation
  
  # no index
  
  def show
    @probation_payment = @probation.probation_payments.find(params[:id])
    @page_title = "Probation Transaction #{@probation_payment.id}"
    
    if can?(:update_probation) && !@probation_payment.report_datetime?
      @links.push(["Edit", edit_probation_probation_payment_path(@probation, @probation_payment)])
      @links.push(["Delete", [@probation, @probation_payment], {method: :delete, data: {confirm: confirm_msg_for(:probation_payment)}}])
    end
    
    @page_links.push(@probation_payment.probation, @probation_payment.probation.person)
  end
  
  def new
    can!(:update_probation)

    @probation_payment = @probation.probation_payments.build(transaction_date: Date.today)
    set_officer_to_session(@probation_payment, :officer)
    
    # set names from probation
    (1..9).each do |x|
      @probation_payment.send("fund#{x}_name=",@probation.send("fund#{x}_name"))
    end
    
    @page_title = "New Transaction"
  end
  
  def edit
    can!(:update_probation)

    @probation_payment = @probation.probation_payments.(params[:id])
    @probation_payment.officer_id = nil
    @page_title = "Edit Probation Transaction #{params[:id]}"
  end
  
  def create
    can!(:update_probation)

    redirect_to @probation and return if params[:commit] == 'Cancel'

    @probation_payment = @probation.probation_payments.build(probation_payment_params)
    @page_title = "New Transaction"
    @probation_payment.created_by = session[:user]
    @probation_payment.updated_by = session[:user]
    
    success = @probation_payment.save
    user_action "Probation Payment ##{@probation_payment.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@probation, @probation_payment], notice: 'Probation Payment was successfully created.' }
        format.json { render :show, status: :created, location: [@probation, @probation_payment] }
      else
        format.html { render :new }
        format.json { render json: @probation_payment.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_probation)
    
    @probation_payment = @probation.probation_payments.find(params[:id])
    
    redirect_to [@probation, @probation_payment] and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Probation Transaction #{params[:id]}"
    @probation_payment.updated_by = session[:user]
    
    success = @probation_payment.update(probation_payment_params)
    user_action "Probation Payment ##{@probation_payment.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@probation, @probation_payment], notice: 'Probation Payment was successfully updated.' }
        format.json { render :show, status: :ok, location: [@probation, @probation_payment] }
      else
        format.html { render :edit }
        format.json { render json: @probation_payment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:update_probation)
    
    @probation_payment = @probation.probation_payments.find(params[:id])
    
    if reasons = @probation_payment.reasons_not_destroyable
      flash.alert = "Cannot delete: #{reasons}"
      redirect_to [@probation, @probation_payment] and return
    end
    
    @probation_payment.destroy
    user_action "Probation Payment ##{@probation_payment.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to @probation, notice: 'Probation Payment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  protected
  
  def load_probation
    @probation = Probation.find(params[:probation_id])
  end
  
  def probation_payment_params
    params.require(:probation_payment).permit(:transaction_date, :officer_id, :officer_badge, :officer_unit, :officer, :receipt_no,
      :memo, :fund1_name, :fund1_charge, :fund1_payment, :fund2_name, :fund2_charge, :fund2_payment, :fund3_name, :fund3_charge,
      :fund3_payment, :fund4_name, :fund4_charge, :fund4_payment, :fund5_name, :fund5_charge, :fund5_payment, :fund6_name, :fund6_charge,
      :fund6_payment, :fund7_name, :fund7_charge, :fund7_payment, :fund8_name, :fund8_charge, :fund8_payment, :fund9_name, :fund9_charge,
      :fund9_payment
    )
  end
end
