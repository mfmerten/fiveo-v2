class MessagesController < ApplicationController
  # most of this was taken from restful_easy_messages plugin so this
  # controller does not follow the same pattern as the other controllers.
  # this will change in the future when I have time to rewrite it.
  
  def index
    redirect_to inbox_user_messages_url
  end

  def show
    @message = Message.find(params[:id])
    @page_title = "Message #{@message.id}"
    
    unless (!@message.sender.nil? && @message.sender.disabled_text?) || @message.body =~ /DO NOT REPLY/
      # can't reply to disabled users or messages containing "DO NOT REPLY" text
      @links.push(['Reply', reply_user_message_path(current_user, @message)])
    end
    @links.push(['Delete', user_message_path(current_user, @message), {method: 'delete', data: {confirm: confirm_msg_for(:message)}}])
    @links.push(['Display', print_message_path(user_id: session[:user], id: @message.id)])
    
    unless can_view(@message)
      flash.alert = "You cannot view that message!"
      redirect_to current_mailbox and return
    end
    
    @message.mark_message_read(current_user)
    
    @page_links.push([@message.sender,'sender'],[@message.receiver,'receiver'])
  end

  def new
    @page_title = "New Message"
    @message = Message.new
  end

  def create
    @message = Message.new((message_params || {}).merge(sender_id: session[:user]))
    @page_title = "New Message"
    
    if params[:commit] == "Cancel"
      redirect_to inbox_user_messages_url and return
    end
    
    success = false
    if @message.save
      success = true
      user_action("Sent message to #{@message.receiver.id}.")
    end
    
    respond_to do |format|
      if success
        format.html { redirect_to outbox_user_messages_url, notice: 'Message was successfully created.' }
        format.json { render :show, status: :created, location: @message }
      else
        format.html { render :new }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @message = Message.find(params[:id])

    unless can_view(@message)
      flash.alert = 'You cannot destroy that message!'
      redirect_to current_mailbox and return
    end
    
    user_action("Destroyed Message #{@message.id}.")
    mark_message_for_destruction(@message)
    
    respond_to do |format|
      format.html { redirect_to current_mailbox, notice: 'Message was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def inbox
    session[:mail_box] = "inbox"
    @messages = current_user.messages.inbox
    @page_title = "Inbox For: " + session[:user_name]
    
    @links.push(['New Message', new_user_message_path])
    @links.push(['Outbox', outbox_user_messages_path])
    @links.push(['Trashbin', trashbin_user_messages_path])

    render "index"
  end

  def outbox
    session[:mail_box] = "outbox"
    @messages = current_user.sent_messages.outbox
    @page_title = "Outbox For: " + session[:user_name]

    @links.push(['New Message', new_user_message_path])
    @links.push(['Inbox', inbox_user_messages_path])
    @links.push(['Trashbin', trashbin_user_messages_path])

    render "index"
  end

  def trashbin
    session[:mail_box] = "trashbin"
    @messages = current_user.messages.trashbin
    @page_title = "Trashbin For: " + session[:user_name]
    
    @links.push(['New Message', new_user_message_path])
    @links.push(['Inbox', inbox_user_messages_path])
    @links.push(['Outbox', outbox_user_messages_path])
    
    render "index"
  end

  def reply
    @message = Message.find(params[:id])
    
    if @message.body =~ /DO NOT REPLY/
      flash.alert = "You cannot reply to this message."
      redirect_to @message and return
    end
    
    @page_title = "Reply To Message"
    
    unless can_view(@message)
      flash.alert = "You cannot view that message!"
      redirect_to current_mailbox and return
    end
    
    @message.receiver_id = @message.sender_id
    @message.subject = "Re: " + @message.subject unless @message.subject =~ /^Re: /
    @message.body = "\n\n\n\n-- Type your reply above this line! --\n\nOn #{AppUtils.format_date(@message.created_at)}, #{@message.sender.display_name} wrote:\n\n" + @message.body
    
    render "new"
  end

  def select_messages
    unless ['Digest', 'Delete'].include?(params[:commit])
      flash.alert = 'You Must click either Delete or Digest buttons!'
      redirect_to current_mailbox and return
    end
    
    if params[:commit] == 'Digest'
      @messages = params[:selected].map { |m| Message.find_by_id(m) }
      render 'messages/print_digest.html.erb', layout: 'print'
    else
      messages = params[:selected].map { |m| Message.find_by_id(m) } 
      messages.each do |message| 
        mark_message_for_destruction(message)
      end
      redirect_to current_mailbox
    end
  end

  def print_message
    @message = Message.find(params[:id])

    unless can_view(@message)
      flash.alert = 'You cannot view that message!'
      redirect_to current_mailbox and return
    end
    
    render layout: 'print'
  end

  protected

  def can_view(message)
    true if !message.nil? and (session[:user] == message.sender_id or session[:user] == message.receiver_id)
  end

  def current_mailbox
    case session[:mail_box]
    when "inbox"
      inbox_user_messages_url
    when "outbox"
      outbox_user_messages_url
    when "trashbin"
      trashbin_user_messages_url
    else
      inbox_user_messages_url
    end
  end

  def mark_message_for_destruction(message)
    if can_view(message)
      # "inbox"
      if session[:user] == message.receiver_id and !message.receiver_deleted
        message.receiver_deleted = true             
      # "outbox"
      elsif session[:user] == message.sender_id and !message.sender_deleted
        message.sender_deleted = true
        message.sender_purged = true
      # "trash_bin"
      elsif session[:user] == message.receiver_id and message.receiver_deleted
        message.receiver_purged = true
      end
      message.save(validate: false) 
      message.purge
    end
  end
  
  def message_params
    params.require(:message).permit(:receiver_id, :subject, :body)
  end
end
