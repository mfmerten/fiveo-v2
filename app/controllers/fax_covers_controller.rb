class FaxCoversController < ApplicationController
  # note: no permissions required for any of this
  
  def index
    options = {
      scopes: [:by_name],
      paginate: true
    }
    @fax_covers = get_records(options)
    
    @links.push(["New", new_fax_cover_path])
  end

  def show
    @fax_cover = FaxCover.find(params[:id])
    @page_title = "Fax Cover #{@fax_cover.id}"

    @links.push(["New", edit_fax_cover_path],["Edit", edit_fax_cover_path(@fax_cover.id)])
    if check_author(@fax_cover)
      @links.push(["Delete", @fax_cover, 'delete', true])
    end
    @links.push(["Cover Sheet", fax_cover_fax_cover_path(@fax_cover), pdf: true])

    @page_links.push([@fax_cover.creator,'creator'],[@fax_cover.updater,'updater'])
  end

  def new
    @fax_cover = FaxCover.new
    @page_title = "New Fax Cover"
  end
  
  def edit
    @fax_cover = FaxCover.find(params[:id])
    @page_title = "Edit Fax Cover #{@fax_cover.id}"
  end
  
  def create
    redirect_to fax_covers_url and return if params[:commit] == 'Cancel'

    @fax_cover = FaxCover.new(fax_cover_params)
    @page_title = "New Fax Cover"
    @fax_cover.created_by = session[:user]
    @fax_cover.updated_by = session[:user]

    success = @fax_cover.save
    user_activity "Fax Cover ##{@fax_cover.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @fax_cover, notice: 'Fax Cover was successfully created.' }
        format.json { render :show, status: :created, location: @fax_cover }
      else
        format.html { render :new }
        format.json { render json: @fax_cover.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @fax_cover = FaxCover.find(params[:id])
    
    redirect_to @fax_cover and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Fax Cover #{@fax_cover.id}"
    @fax_cover.updated_by = session[:user]
    
    success = @fax_cover.update(fax_cover_params)
    user_action "Fax Cover ##{@fax_cover.id} Update." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @fax_cover, notice: 'Fax Cover was successfully updated.' }
        format.json { render :show, status: :ok, location: @fax_cover }
      else
        format.html { render :edit }
        format.json { render json: @fax_cover.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @fax_cover = FaxCover.find(params[:id])
    
    enforce_author(@fax_cover)
    
    @fax_cover.destroy
    user_action "Fax Cover ##{@fax_cover.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to fax_covers_url, notice: 'Fax Cover was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def fax_cover
    @fax_cover = FaxCover.find(params[:id])
    
    if pdf_url = make_pdf_file("fax_cover", FaxCovers::Page.new(@fax_cover))
      redirect_to pdf_url
      user_action("Printed fax cover.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to @fax_cover
    end
  end
  
  private
  
  def fax_cover_params
    params.require(:fax_cover).permit(:name, :from_name, :from_phone, :from_fax, :include_notice, :notice_title, :notice_body)
  end
end
