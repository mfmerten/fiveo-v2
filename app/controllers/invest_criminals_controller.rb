class InvestCriminalsController < ApplicationController
  before_action :load_investigation, except: [:index, :search, :update_person]

  def index
    options = {
      scopes: [:by_reverse_id],
      includes: [:investigation, :crimes],
      paginate: true
    }
    @invest_criminals = get_records(options)

    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @invest_criminal = @investigation.criminals.find(params[:id])
    @page_title = "Investigation Criminal #{@invest_criminal.id}"
    
    enforce_privacy(@invest_criminal)
    
    if can?(:update_investigation) && check_author(@invest_criminal)
      @links.push(['New', new_investigation_invest_criminal_path(@investigation)])
      @links.push(['Edit', edit_investigation_invest_criminal_path(@investigation, @invest_criminal)])
      @links.push(['Delete', [@investigation, @invest_criminal], {method: 'delete', data: {confirm: confirm_msg_for(:invest_criminal)}}])
    end

    @page_links.push(@invest_criminal.person,[@investigation,'invest'],[@invest_criminal.creator, 'creator'],[@invest_criminal.updater,'updater'])
  end

  def new
    can!(:update_investigation)

    @invest_criminal = @investigation.criminals.build
    @page_title = "New Investigation Criminal"
  end
  
  def edit
    can!(:update_investigation)

    @invest_criminal = @investigation.criminals.find(params[:id])

    enforce_privacy(@invest_criminal)
    enforce_author(@invest_criminal)

    @page_title = "Edit Investigation Criminal #{@invest_criminal.id}"
  end
  
  def create
    can!(:update_investigation)
    
    redirect_to @investigation and return if params[:commit] == 'Cancel'
    
    @invest_criminal = @investigation.criminals.build(invest_criminal_params)
    @page_title = "New Investigation Criminal"
    @invest_criminal.private = @investigation.private?
    @invest_criminal.owned_by = @investigation.owned_by
    @invest_criminal.created_by = session[:user]
    @invest_criminal.updated_by = session[:user]

    success = @invest_criminal.save
    user_action "Invest Criminal ##{@invest_criminal.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@investigation, @invest_criminal], notice: 'Invest Criminal was successfully created.' }
        format.json { render :show, status: :created, location: [@investigation, @invest_criminal] }
      else
        format.html { render :new }
        format.json { render json: @invest_criminal.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_investigation)

    @invest_criminal = @investigation.criminals.find(params[:id])

    redirect_to [@investigation, @invest_criminal] and return if params[:commit] == 'Cancel'

    enforce_privacy(@invest_criminal)
    enforce_author(@invest_criminal)

    @page_title = "Edit Investigation Criminal #{@invest_criminal.id}"
    @invest_criminal.updated_by = session[:user]
    params[:invest_criminal][:linked_arrests] ||= []
    params[:invest_criminal][:linked_crimes] ||= []
    params[:invest_criminal][:linked_evidences] ||= []
    params[:invest_criminal][:linked_photos] ||= []
    params[:invest_criminal][:linked_stolens] ||= []
    params[:invest_criminal][:linked_vehicles] ||= []
    params[:invest_criminal][:linked_warrants] ||= []

    success = @invest_criminal.update(invest_criminal_params)
    user_action "Invest Criminal ##{@invest_criminal.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@investigation, @invest_criminal], notice: 'Invest Criminal was successfully updated.' }
        format.json { render :show, status: :ok, location: [@investigation, @invest_criminal]}
      else
        format.html { render :edit }
        format.json { render json: @invest_criminal.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:update_investigation)
    
    @invest_criminal = @investigation.criminals.find(params[:id])
    
    enforce_privacy(@invest_criminal)
    enforce_author(@invest_criminal)
    
    @invest_criminal.destroy
    user_action "Invest Criminal ##{@invest_criminal.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to @investigation, notice: 'Invest Criminal was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def update_person
    @name = ""
    @valie = false
    if can?(:view_person)
      @name = "Invalid Person ID"
      if params[:c] && person = Person.find_by_id(params[:c])
        @name = person.sort_name
        @valid = true
      end
    else
      @name = "Permission Denied!"
    end
    
    respond_to :js
  end
  
  protected

  def load_investigation
    @investigation = Investigation.find(params[:investigation_id])
    enforce_privacy(@investigation)
  end
  
  def invest_criminal_params
    params.require(:invest_criminal).permit(:person_id, :sort_name, :description, :fingerprint_class, :dna_profile, :height, :weight,
      :shoes, :remarks, linked_arrests: [], linked_crimes: [], linked_evidences: [], linked_photos: [], linked_stolens: [],
      linked_vehicles: [], linked_warrants: []
    )
  end
end
