class DocketsController < ApplicationController

  def index
    options = {
      scopes: [:by_docket_no],
      includes: [:person, :courts, {da_charges: [:arrest, {arrest_charge: :arrest}]}],
      paginate: true
    }
    @dockets = get_records(options)
    
    @unprocessed = Sentence.unprocessed.all
    @unprocessed.concat(Revocation.unprocessed.all)
    @unfinalized = Bond.satisfied_but_unfinalized
    
    if can?(:update_court)
      @links.push(['New', new_docket_path])
      if can?(:update_option)
        @links.push(['Court Costs', court_costs_path])
        @links.push(['Court Types', court_types_path])
        @links.push(['Docket Types', docket_types_path])
      end
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @docket = Docket.includes([:sentences, {da_charges: [:arrest, {arrest_charge: [:arrest, :citation]}, :sentence]}]).find(params[:id])
    @page_title = "Docket #{@docket.id}"

    if can?(:update_court)
      @links.push(['New', new_docket_path])
      @links.push(['Edit', edit_docket_path(@docket)])
    end
    if can?(:destroy_court)
      @links.push(['Delete', @docket, {method: 'delete', data: {confirm: confirm_msg_for(:docket)}}])
    end
    if can?(:update_court)
      @links.push(['Link Charges', link_charges_docket_path(@docket)])
      @links.push(['New Court', new_docket_court_path(@docket)])
    end

    @page_links.push(@docket.person)
    @page_links.concat(@docket.probations)
    @page_links.push([@docket.creator,'creator'],[@docket.updater,'updater'])
  end

  def new
    can!(:update_court)
    
    @docket = Docket.new
    @page_title = "New Docket"
  end
  
  def edit
    can!(:update_court)

    @docket = Docket.find(params[:id])
    @page_title = "Edit Docket #{@docket.id}"
  end
  
  def link_charges
    can!(:update_court)

    @docket = Docket.find(params[:id])

    if @docket.person.nil?
      flash.alert = "Docket Has Invalid Person"
      redirect_to @docket and return
    end
    
    @page_title = "Select Charges for Docket #{@docket.id}"
    @records = []
    @docket.person.arrests.each do |a|
      @records.push(a) unless a.district_charges.reject{|c| !c.da_charges.reject{|d| d.info_only?}.empty?}.empty?
    end
    @docket.person.citations.each do |a|
      @records.push(a) unless a.charges.reject{|c| !c.da_charges.reject{|d| d.info_only?}.empty?}.empty?
    end
    
    if @records.empty?
      flash.alert = "No available charges!"
      redirect_to @docket and return
    end
    
    @records = @records.sort{|a,b| (b.is_a?(Arrest) ? b.arrest_datetime : b.citation_datetime) <=> (a.is_a?(Arrest) ? a.arrest_datetime : a.citation_datetime)}
    @docket.updated_by = session[:user]
  end
  
  def create
    can!(:update_court)

    redirect_to dockets_url and return if params[:commit] == 'Cancel'

    @docket = Docket.new(docket_params)
    @page_title = "New Docket"
    @docket.created_by = session[:user]
    @docket.updated_by = session[:user]

    success = @docket.save
    user_action "Docket ##{@docket.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @docket, notice: 'Docket was successfully created.' }
        format.json { render :show, status: :created, location: @docket }
      else
        format.html { render :new }
        format.json { render json: @docket.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_court)

    @docket = Docket.find(params[:id])

    redirect_to @docket and return if params[:commit] == 'Cancel'

    @page_title = "Edit Docket #{@docket.id}"
    @docket.updated_by = session[:user]

    success = @docket.update(docket_params)
    user_action "Docket ##{@docket.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @docket, notice: 'Docket was successfully updated.' }
        format.json { render :show, status: :ok, location: @docket }
      else
        format.html { render :edit }
        format.json { render json: @docket.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_court)
    
    @docket = Docket.find(params[:id])
    
    @docket.destroy
    user_action "Docket ##{@docket.id} Deleted."

    respond_to do |format|
      format.html { redirect_to dockets_url, notice: 'Docket was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def list_unprocessed
    @unprocessed = Sentence.unprocessed.all
    @unprocessed << Revocation.unprocessed.all
    @unprocessed = @unprocessed.flatten.compact
    @page_title = "Unprocessed Sentences/Revocations"
  end

  def link
    can!(:update_court)
    
    @da_charge = DaCharge.find(params[:da_charge_id])
    @page_title = 'Select Arrest or Charge for Link'
    @records = @da_charge.docket.person.arrests.collect{|a| [a.arrest_datetime,a]} + @da_charge.docket.person.citations.collect{|c| [c.citation_datetime,c]}
    
    if @records.empty?
      flash.alert = "No available arrests or citations!"
      redirect_to @da_charge.docket and return
    end
    
    @records = @records.sort{|a,b| b[0] <=> a[0]}
    
    @links.push(["Cancel", @da_charge.docket])
  end

  def do_link
    can!(:update_court)
    
    @da_charge = DaCharge.find(params[:da_charge_id])
    if params[:arrest_id]
      @arrest = Arrest.find(params[:arrest_id])
      @da_charge.update_attribute(:arrest_id, @arrest.id)
    else
      @charge = Charge.find(params[:charge_id])
      @da_charge.update_attribute(:charge_id, @charge.id)
    end
    
    redirect_to @da_charge.docket
    user_action("Updated Charge Link for Docket #{@da_charge.docket.id}.")
  end

  def validate
    # following line added because I do not belive this function is actually
    # used any more, but I'm not sure enough to actually remove it yet.
    raise "Dockets#validate called!"
    
    if docket = Docket.find_by_id(params[:c])
      render text: docket.person.sort_name
    else
      render text: "Invalid Docket!"
    end
  end

  def validate_da_charge
    # following line added because I do not belive this function is actually
    # used any more, but I'm not sure enough to actually remove it yet.
    raise "Dockets#validate_da_charge called!"
    
    if da_charge = DaCharge.find_by_id(params[:c])
      render text: "#{da_charge.count} count #{da_charge.charge}"
    else
      render text: "Invalid Charge!"
    end
  end

  def letters
    if !params[:court_id].blank?
      one_court = Court.find(params[:court_id])
      @court_date = one_court.court_date
      
      if one_court.docket_type.blank?
        flash.alert = "Specified Court has invalid Docket Type!"
        redirect_to one_court and return
      end
      
      @dt = one_court.docket_type
      
      if one_court.judge.blank?
        flash.alert = "Specified Court has invalid Judge!"
        redirect_to one_court and return
      end
      j = one_court.judge
    else
      unless @court_date = AppUtils.valid_date(params[:court_date])
        flash.alert = "Invalid Court Date Specified!"
        redirect_to dockets_url and return
      end
      @dt = params[:docket_type]
      j = params[:judge]
    end

    unless @time = AppUtils.valid_time(params[:time])
      flash.alert = "Invalid Time Specified!"
      redirect_to dockets_url and return
    end

    @time = AppUtils.mil_to_civ(@time)

    if !session[:letters].nil? && !session[:letters].empty?
      @bonds = session[:letters].collect{|bid| [Bond.find_by_id(bid),nil]}.compact.reject{|b| b[0].status? || b[0].final?}
    else
      if one_court.nil?
        courts = Court.for_letters(@court_date,j,@dt).all
      else
        courts = [one_court]
      end
      @bonds = []
      courts.each do |c|
        if c.court_type.blank?
          flash.alert = "Court #{c.id} (Docket #{c.docket_id}) is missing a valid Court Type!"
          redirect_to c and return
        end
        c.docket.bonds.compact.uniq.each do |b|
          unless b.status? || b.final?
            @bonds.push([b,c])
          end
        end
      end
    end

    if @bonds.empty?
      flash.alert = "Nothing to report!"
      redirect_to dockets_url and return
    end

    if pdf_url = make_pdf_file("bond_letters", Dockets::Letters.new(@court_date, @dt, @time, @bonds))
      redirect_to pdf_url
      user_action("Printed Bond Letters")
    else
      flash.alert = "Could not create PDF file to download!"
      redirect_to dockets_url
    end
  end

  def clear_letter_queue
    session[:letters] = nil
    user_action("Cleared their letter queue.")
    redirect_to dockets_url
  end

  def dockets
    unless @court_date = AppUtils.valid_date(params[:court_date])
      flash.alert = "Invalid court date specified!"
      redirect_to dockets_url and return
    end
    
    @j = params[:judge]
    @courts = Court.where(['court_date = ? and judge like ? and docket_type = ?',@court_date,@j,params[:docket_type]]).all
    
    if @courts.empty?
      flash.alert = "Nothing to Report!"
      redirect_to dockets_url and return
    end
    
    @courts = @courts.sort{|a,b| "#{a.docket.person.sort_name} #{a.docket.docket_no}" <=> "#{b.docket.person.sort_name} #{b.docket.docket_no}"}
    if pdf_url = make_pdf_file("docket", Dockets::List.new(@court_date, params[:docket_type], @j, @courts))
      redirect_to pdf_url
      user_action("Printed Docket")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to dockets_url
    end
  end

  private
  
  def docket_params
    params.require(:docket).permit(:docket_no, :misc, :person_id, :notes, :remarks, :updated_by, linked_charges: [])
  end
end
