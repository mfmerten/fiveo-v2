class GroupsController < ApplicationController
  
  def index
    options = {
      scopes: [:by_name],
      paginate: true
    }
    @groups = get_records(options)

    if can?(:update_group)
      @links.push(['New', new_group_path])
    end

    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @group = Group.find(params[:id])
    @page_title = "Group #{@group.id}"
    
    if can?(:update_group) 
      @links.push(['New', new_group_path])
      unless @group.name == 'Admin'
        @links.push(['Edit', edit_group_path(@group)])
      end
    end
    if can?(:destroy_group) && @group.name != 'Admin'
      @links.push(['Delete', @group, {method: 'delete', data: {confirm: confirm_msg_for(:group)}}])
    end

    @page_links.push([@group.creator,'creator'],[@group.updater,'updater'])
  end

  def new
    can!(:update_group)

    @group = Group.new
    @page_title = "New Group"
  end
  
  def edit
    can!(:update_group)

    @group = Group.find(params[:id])

    if @group.name == "Admin"
      flash.alert = "Cannot Edit Admin Group"
      redirect_to @group and return
    end
    
    @page_title = "Edit Group #{@group.id}"
  end
  
  def create
    redirect_to groups_url and return if params[:commit] == 'Cancel'

    can!(:update_group)

    if params[:group][:name] == 'Admin'
      flash.alert = "Admin Group is special! You cannot create a new one!"
      redirect_to groups_url and return
    end

    @group = Group.new(group_params)
    @page_title = "New Group"
    @group.created_by = session[:user]
    @group.updated_by = session[:user]
    
    success = @group.save
    user_action "Group ##{@group.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @group, notice: 'Group was successfully created.' }
        format.json { render :show, status: :created, location: @group }
      else
        format.html { render :new }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_group)

    @group = Group.find(params[:id])

    redirect_to @group and return if params[:commit] == 'Cancel'

    if @group.name == "Admin"
      flash.alert = "Cannot Edit Admin Group"
      redirect_to @group and return
    end
    
    @page_title = "Edit Group #{@group.id}"
    @group.updated_by = session[:user]

    success = @group.update(group_params)
    user_action "Group ##{@group.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @group, notice: 'Group was successfully updated.' }
        format.json { render :show, status: :ok, location: @group }
      else
        format.html { render :edit }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_group)

    @group = Group.find(params[:id])

    if reasons = @group.reasons_not_destroyable
      flash.alert = "Cannot Delete: #{reasons}"
      redirect_to @group and return
    end

    @group.destroy
    user_action "Group ##{group.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to groups_url, notice: 'Group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  
  def group_params
    params.require(:group).permit(:name, :description, permissions: [])
  end
end
