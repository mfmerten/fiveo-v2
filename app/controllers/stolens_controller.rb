class StolensController < ApplicationController

  def index
    options = {
      active: true,
      scopes: [:by_reverse_date],
      includes: [:person],
      paginate: true
    }
    @stolens = get_records(options)
    
    if can?(:update_stolen)
      @links.push(['New', new_stolen_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    if params[:info_id]
      @info_id = params[:info_id]
      @stolen = Stolen.find_by_id(params[:id])
    else
      @stolen = Stolen.find(params[:id])
      @page_title = "Stolen #{@stolen.id}"
      
      if can?(:update_stolen)
        @links.push(['New', new_stolen_path])
        @links.push(['Edit', edit_stolen_path(@stolen)])
      end
      if can?(:destroy_stolen)
        @links.push(['Delete', @stolen, {method: :delete, data: {confirm: confirm_msg_for(:stolen)}}])
      end
      
      @page_links.push(@stolen.person,[@stolen.creator,'creator'],[@stolen.updater,'updater'])
    end
    respond_to :html, :json, :js
  end
  
  def new
    can!(:update_stolen)

    @stolen = Stolen.new
    @page_title = "New Stolen Item"
  end
  
  def edit
    can!(:update_stolen)

    @stolen = Stolen.find(params[:id])
    @page_title = "Edit Stolen #{@stolen.id.to_s}"
  end
  
  def create
    can!(:update_stolen)

    redirect_to stolens_url and return if params[:commit] == 'Cancel'

    @stolen = Stolen.new(stolen_params)
    @page_title = "New Stolen Item"
    @stolen.created_by = session[:user]
    @stolen.updated_by = session[:user]
    
    success = @stolen.save
    user_action "Stolen ##{@stolen.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @stolen, notice: 'Stolen was successfully created.' }
        format.json { render :show, status: :created, location: @stolen }
      else
        format.html { render :new }
        format.json { render json: @stolen.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_stolen)
    
    @stolen = Stolen.find(params[:id])
    
    redirect_to @stolen and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Stolen #{@stolen.id.to_s}"
    @stolen.updated_by = session[:user]
    
    success = false
    if @stolen.update(stolen_params)
      success = true
      user_action "Stolen ##{@stolen.id} Updated."
      if params[:investigation_id] && inv = Investigation.find_by_id(params[:investigation_id])
        inv.invest_stolens.create(stolen_id: @stolen.id)
      end
    end
    
    respond_to do |format|
      if success
        format.html { redirect_to @stolen, notice: 'Stolen was successfully updated.' }
        format.json { render :show, status: :ok, location: @stolen }
      else
        format.html { render :edit }
        format.json { render json: @stolen.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:destroy_stolen)
    
    @stolen = Stolen.find(params[:id])
    
    @stolen.destroy
    user_action "Stolen ##{@stolen.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to stolens_url, notice: 'Stolen was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  
  def stolen_params
    params.require(:stolen).permit(:case_no, :stolen_date, :person_id, :model_no, :serial_no, :item_value, :item, :item_description,
      :recovered_date, :returned_date, :remarks
    )
  end
end
