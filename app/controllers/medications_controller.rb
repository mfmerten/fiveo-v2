class MedicationsController < ApplicationController
  before_action :load_person

  def index
    options = {
      scopes: [:by_drug],
      includes: [:person],
      paginate: true
    }
    @medications = get_records(options).belonging_to_person(@person.id)
    @page_title = "Medications for: #{AppUtils.show_person(@person)}"
    
    @links.push(['Person', person_path(@person)])
    if can?(:update_person)
      @links.push(['New', new_person_medication_path(@person)])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @medication = @person.medications.find(params[:id])
    @page_title = "Medication #{@medication.id}"

    if can?(:update_person)
      @links.push(['New', new_person_medication_path(@person)])
      @links.push(['Edit', edit_person_medication_path(@person, @medication)])
      @links.push(['Delete', [@person, @medication], {method: 'delete', data: {confirm: confirm_msg_for(:medication)}}])
    end

    @page_links.push(@medication.person,[@medication.physician,'physician'],[@medication.pharmacy,'pharmacy'],[@medication.creator,'creator'],[@medication.updater,'updater'])
  end
  
  def new
    can!(:update_person)

    @medication = @person.medications.build
    @medication.schedules.build
    @page_title = "New Medication"
  end

  def edit
    can!(:update_person)

    @medication = @person.medications.find(params[:id])
    @page_title = "Edit Medication #{@medication.id}"
  end

  def create
    redirect_to @person and return if params[:commit] == 'Cancel'
    
    can!(:update_person)
    
    @medication = @person.medications.build(medication_params)
    @page_title = "New Medication"
    @medication.created_by = session[:user]
    @medication.updated_by = session[:user]
    
    success = @medication.save
    user_action "Medication ##{@medication.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@person, @medication], notice: 'Medication was successfully created.' }
        format.json { render :show, status: :created, location: [@person, @medication] }
      else
        format.html { render :new }
        format.json { render json: medication.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    can!(:update_person)

    @medication = @person.medications.find(params[:id])

    redirect_to [@person, @medication] and return if params[:commit] == 'Cancel'

    @page_title = "Edit Medication #{@medication.id}"
    @medication.updated_by = session[:user]
    
    success = @medication.update(medication_params)
    user_action "Medication ##{@medication.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@person, @medication], notice: 'Medication was successfully updated.' }
        format.json { render :show, status: :ok, location: [@person, @medication] }
      else
        format.html { render :edit }
        format.json { render json: @medication.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:update_person)
    
    @medication = @person.medications.find(params[:id])
    
    unless @medication.can_be_deleted?
      flash.alert = "Cannot delete this medication!"
      redirect_to [@person, @medication] and return
    end
    
    @medication.destroy
    user_action "Medication ##{@medication.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to @person, notice: 'Medication was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  protected
  
  def load_person
    @person = Person.find(params[:person_id])
  end
  
  def medication_params
    params.require(:medication).permit(:person_id, :drug_name, :dosage, :warnings, :prescription_no, :physician_id, :pharmacy_id,
      schedules_attributes: [:id, :dose, :time, :_destroy]
    )
  end
end