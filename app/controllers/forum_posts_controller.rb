# adapted from "forem" engine (https://github.com/radar/forem)
#
# Copyright 2011 Ryan Bigg, Philip Arndt and Josh Adams
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
class ForumPostsController < ApplicationController
  before_action :set_page_title
  before_action :get_forum_topic
  
  # no index or show actions
  
  def new
    can!(:update_forum)
    
    if @forum_topic.locked? && !can?(:admin_forum)
      flash.alert = "You canot post here, this Forum Topic is locked!"
      redirect_to forum_forum_topic_url(@forum, @forum_topic) and return
    end
    
    @forum_post = @forum_topic.forum_posts.build
    @reply_to_post = @forum_topic.forum_posts.find_by_id(params[:reply_to_id])
    if params[:quote]
      @forum_post.text = view_context.forum_quote(@reply_to_post.text)
    end
  end

  def create
    can!(:update_forum)

    redirect_to forum_forum_topic_url(@forum, @forum_topic) and return if params[:commit] == 'Cancel'

    if @forum_topic.locked? && !can?(:admin_forum)
      flash.alert = "You canot post here, this Forum Topic is locked!"
      redirect_to forum_forum_topic_url(@forum, @forum_topic) and return
    end

    @forum_post = @forum_topic.forum_posts.build(forum_post_params)
    @forum_post.user = current_user

    success = false
    if @forum_post.save
      success = true
      user_action "Forum Post ##{@forum_post.id} Created."
    else
      params[:reply_to_id] = params[:forum_post][:reply_to_id]
    end
    
    respond_to do |format|
      if success
        format.html { redirect_to forum_forum_topic_url(@forum, @forum_topic, page: @forum_topic.last_page(current_user)), notice: 'Forum Post was successfully created.' }
        format.json { render :show, status: :created, location: @forum_post }
      else
        format.html { render :new }
        format.json { render json: @forum_post.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    can!(:update_forum)
    
    if @forum_topic.locked? && !can?(:admin_forum)
      flash.alert = "You canot edit posts here, this Forum Topic is locked!"
      redirect_to forum_forum_topic_url(@forum, @forum_topic) and return
    end
    
    @forum_post = ForumPost.find(params[:id])

    enforce_author(@forum_post)
  end

  def update
    can!(:update_forum)

    redirect_to forum_forum_topic_url(@forum, @forum_topic) and return if params[:commit] == 'Cancel'

    if @forum_topic.locked? && !can?(:admin_forum)
      flash.alert = "You canot edit posts here, this Forum Topic is locked!"
      redirect_to forum_forum_topic_url(@forum, @forum_topic) and return
    end
    
    @forum_post = ForumPost.find(params[:id])
    
    enforce_author(@forum_post)
    
    success = @forum_post.update(forum_post_params)
    user_action "Forum Post ##{@forum_post.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to forum_forum_topic_url(@forum, @forum_topic), notice: 'Forum Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @forum_post }
      else
        format.html { render :edit }
        format.json { render json: @forum_post.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:update_forum)
    
    if @forum_topic.locked? && !can?(:admin_forum)
      flash.alert = "You canot delete posts here, this Forum Topic is locked!"
      redirect_to forum_forum_topic_url(@forum, @forum_topic) and return
    end
    
    @forum_post = @forum_topic.forum_posts.find(params[:id])
    
    enforce_author(@forum_post)
    
    @forum_post.destroy
    user_action "Forum Post ##{@forum_post.id} Deleted."
    
    respond_to do |format|
      if @forum_post.forum_topic.forum_posts.count == 0 && @forum_post.forum_topic.destroy
        format.html { redirect_to @forum, notice: 'Forum Post was successfully destroyed.' }
      else
        format.html { redirect_to [@forum, @forum_topic], notice: 'Forum Post was successfully destroyed.' }
      end
      format.json { head :no_content }
    end
  end

  private
  
  def get_forum_topic
    @forum_topic = ForumTopic.find(params[:forum_topic_id])
    @forum = @forum_topic.forum
  end
  
  def set_page_title
    @page_title = "FiveO Discussion Forums"
  end
  
  def forum_post_params
    params.require(:forum_post).permit(:text)
  end
end