class DamagesController < ApplicationController
  
  def index
    options = {
      includes: [:damage_items, :person],
      paginate: true
    }
    @damages = get_records(options)

    if can?(:update_damage)
      @links.push(['New', new_damage_path])
    end

    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @damage = Damage.find(params[:id])
    @page_title = "Criminal Damage #{@damage.id}"
    
    if can?(:update_damage)
      @links.push(['New', new_damage_path])
      @links.push(['Edit', edit_damage_path(@damage)])
    end
    if can?(:destroy_damage)
      @links.push(['Delete', @damage, {method: 'delete', data: {confirm: confirm_msg_for(:damage)}}])
    end

    @page_links.push([@damage.person,'Victim'],[@damage.creator,'creator'],[@damage.updater,'updater'])

    respond_to :html, :json, :js
  end

  def new
    can!(:update_damage)
    
    if params[:offense_id]
      off = Offense.find(params[:offense_id])
      @damage = off.damages.build(case_no: off.case_no)
    else
      @damage = Damage.new
    end
    @damage.damage_items.build
    @page_title = "New Criminal Damage"
  end
  
  def edit
    can!(:update_damage)

    @damage = Damage.find(params[:id])
    @page_title = "Edit Criminal Damage #{@damage.id}"
  end
  
  def create
    redirect_to damages_url and return if params[:commit] == 'Cancel'

    can!(:update_damage)

    @damage = Damage.new(damage_params)
    @page_title = "New Criminal Damage"
    @damage.created_by = session[:user]
    @damage.updated_by = session[:user]
    
    success = false
    if @damage.save
      success = true
      user_action "Damage ##{@damage.id} Created."
      if params[:offense_id] && offense = Offense.find_by_id(params[:offense_id])
        offense.damages << @damage
      end
      if params[:investigation_id] && investigation = Investigation.find_by_id(params[:investigation_id])
        investigation.damages << @damage
      end
    end
    
    respond_to do |format|
      if success
        format.html { redirect_to @damage, notice: 'Damage was successfully created.' }
        format.json { render :show, status: :created, location: @damage }
      else
        format.html { render :new }
        format.json { render json: @damage, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_damage)
    
    @damage = Damage.find(params[:id])
    
    redirect_to @damage and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Criminal Damage #{@damage.id}"
    @damage.updated_by = session[:user]
    
    success = @damage.update(damage_params)
    user_action "Damage ##{@damage.id} Updated" if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @damage, notice: 'Damage was successfully updated.' }
        format.json { render :show, status: :ok, location: @damage }
      else
        format.html { render :edit }
        format.json { render json: @damage.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_damage)
    
    @damage = Damage.find(params[:id])
    
    @damage.destroy
    user_action "Damage ##{@damage.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to damages_url, notice: 'Damage was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  
  def damage_params
    params.require(:damage).permit(:damage_datetime, :case_no, :person_id, :sort_name, :victim_address, :victim_phone, :crimes,
      damage_items_attributes: [:id, :description, :estimate, :_destroy]
    )
  end
end
