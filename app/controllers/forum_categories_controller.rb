# adapted from "forem" engine (https://github.com/radar/forem)
#
# Copyright 2011 Ryan Bigg, Philip Arndt and Josh Adams
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
class ForumCategoriesController < ApplicationController
  before_action :set_page_title
  
  # no index view
  
  def show
    @forum_category = ForumCategory.find(params[:id])
  end
  
  def new
    can!(:admin_forum)

    @forum_category = ForumCategory.new
    @page_title = "New Forum Category"
  end
  
  def edit
    can!(:admin_forum)

    @forum_category = ForumCategory.find(params[:id])
    @page_title = "Edit Forum Category #{@forum_category.id}"
  end
  
  def create
    can!(:admin_forum)

    redirect_to forum_categories_url and return if params[:commit] == 'Cancel'

    @forum_category = ForumCategory.new(forum_category_params)
    @page_title = "New Forum Category"

    success = @forum_category.save
    user_action "Forum Category ##{@forum_category.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @forum_category, notice: 'Forum Category was successfully created.' }
        format.json { render :show, status: :created, location: @forum_category }
      else
        format.html { render :new }
        format.json { render json: @forum_category.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:admin_forum)
    
    redirect_to forum_categories_url and return if params[:commit] == 'Cancel'
    
    @forum_category = ForumCategory.find(params[:id])
    @page_title = "Edit Forum Category #{@forum_category.id}"
    
    success = @forum_category.update(forum_category_params)
    user_action "Forum Category ##{@forum_category.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @forum_category, notice: 'Forum Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @forum_category }
      else
        format.html { render :edit }
        format.json { render json: @forum_category.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:admin_forum)
    
    @forum_category = ForumCategory.find(params[:id])
    
    @forum_category.destroy
    user_action "Forum Category ##{@forum_category.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to forums_url, notice: 'Forum Category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  
  def set_page_title
    @page_title = "FiveO Discussion Forums"
  end
  
  def forum_category_params
    params.require(:forum_category).permit(:name)
  end
end
