class DatabaseController < ApplicationController
  
  def index
    can!(:view_database)
    
    @page_title = "Database Administration"
    
    # build the model list
    models = []
    Dir.chdir(File.join(Rails.root, "app/models")) do 
      models = Dir["*.rb"]
    end
    @tables = []
    models.sort.each do |m|         
      class_name = m.sub(/\.rb$/,'').camelize
      unless SiteConfig.enable_internal_commissary
        next if class_name == 'Commissary' || class_name == 'CommissaryItem'
      end
      # only want the active record models
      if class_name.constantize.superclass == ActiveRecord::Base
        if class_name.constantize.respond_to?('desc')
          desc = class_name.constantize.desc
          next if desc.nil?
        else
          desc = "desc method missing for model #{class_name}"
        end
        @tables.push({model_name: "#{class_name}", records: class_name.constantize.count, description: desc})
      end
    end
  end
  
  # no table, so CRUD is skipped

  def clear_sessions
    can!(:update_database)

    user_action("Cleared the session table!")
    Session.reset_table
    redirect_to databases_url
  end

  def clear_cases
    can!(:update_database)

    user_action("Reset Case Numbers!")
    sql = Case.connection
    sql.execute "truncate table cases"
    redirect_to databases_url
  end
end