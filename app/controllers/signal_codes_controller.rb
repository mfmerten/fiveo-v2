class SignalCodesController < ApplicationController

  def index
    options = {
      scopes: [:by_code],
      paginate: true
    }
    @signal_codes = get_records(options)
    
    if can?(:update_option)
      @links.push(['New', new_signal_code_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end
  
  def show
    @signal_code = SignalCode.find(params[:id])
    @page_title = "Signal Code #{@signal_code.id}"
    
    if can?(:update_option)
      @links.push(['New', new_signal_code_path])
      @links.push(['Edit', edit_signal_code_path(@signal_code)])
      @links.push(['Delete', @signal_code, {method: :delete, data: {confirm: confirm_msg_for(:signal_code)}}])
    end
    
    @page_links.push([@signal_code.creator,'creator'],[@signal_code.updater,'updater'])
  end
  
  def new
    can!(:update_option)

    @signal_code = SignalCode.new
    @page_title = "New Signal Code"
  end
    
  def edit
    can!(:update_option)

    @signal_code = SignalCode.find(params[:id])
    @page_title = "Edit Signal Code #{@signal_code.id}"
  end
  
  def create
    redirect_to signal_codes_url and return if params[:commit] == 'Cancel'

    can!(:update_option)

    @signal_code = SignalCode.new(signal_code_params)
    @page_title = "New Signal Code"
    @signal_code.created_by = session[:user]
    @signal_code.updated_by = session[:user]
    
    success = @signal_code.save
    user_action "Signal Code ##{@signal_code.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @signal_code, notice: 'Signal Code was successfully created.' }
        format.json { render :show, status: :created, location: @signal_code }
      else
        format.html { render :new }
        format.json { render json: @signal_code.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_option)
    
    @signal_code = SignalCode.find(params[:id])
    
    redirect_to @signal_code and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Signal Code #{@signal_code.id}"
    @signal_code.updated_by = session[:user]
    
    success = @signal_code.update(signal_code_params)
    user_action "Signal Code ##{@signal_code.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @signal_code, notice: 'Signal Code was successfully updated.' }
        format.json { render :show, status: :ok, location: @signal_code }
      else
        format.html { render :edit }
        format.json { render json: @signal_code.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:update_option)
    
    @signal_code = SignalCode.find(params[:id])
    
    @signal_code.destroy
    user_action "Signal Code ##{@signal_code.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to signal_codes_url, notice: 'Signal Code was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def signals
    @signal_codes = SignalCode.by_name.all
    @page_title = "Signal Code Reference"
    
    respond_to do |format|
      format.html { render layout: 'popup' }
    end
  end
  
  def validate
    unless @signal_code = SignalCode.find_by(code: params[:code])
      @signal_code = SignalCode.new(code: params[:code], name: 'Not On File')
    end
    @code_id = params[:code_id]
    @name_id = params[:name_id]
    @display_id = params[:display_id]

    respond_to :js
  end
  
  private
  
  def signal_code_params
    params.require(:signal_code).permit(:code, :name)
  end
end
