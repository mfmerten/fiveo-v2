class EvidencesController < ApplicationController
  
  def index
    options = {
      active: true,
      scopes: [:by_reverse_id],
      includes: [:evidence_owner, :offenses, :evidence_locations],
      paginate: true
    }
    @evidences = get_records(options)
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    if params[:info_id] && request.format == :js
      @info_id = params[:info_id]
      @evidence = Evidence.find_by_id(params[:id])
    else
      @evidence = Evidence.find(params[:id])
      @page_title = "Evidence #{@evidence.id}"
      
      if can?(:update_evidence) && check_author(@evidence)
        @links.push(['Edit', edit_evidence_path(@evidence)])
      end
      if can?(:destroy_evidence) && check_author(@evidence)
        @links.push(['Delete', @evidence, {method: 'delete', data: {confirm: confirm_msg_for(:evidence)}}])
      end
      if can?(:update_evidence)
        @links.push(['Move', change_location_evidence_path(@evidence)])
        if check_author(@evidence)
          @links.push(["Photo", photo_evidence_path(@evidence)])
        end
      end
      
      @page_links.push([@evidence.evidence_owner,'Owner'],[@evidence.creator,'creator'],[@evidence.updater,'updater'])
    end
    
    respond_to :html, :json, :js
  end

  def new
    can!(:update_evidence)
    
    if params[:offense_id]
      off = Offense.find(params[:offense_id])
      @evidence = off.evidences.build(case_no: off.case_no)
    else
      @evidence = Evidence.new
    end
    @evidence.evidence_locations.build(location_from: 'Initial Storage')
    @page_title = "New Evidence"
  end
  
  def edit
    can!(:update_evidence)

    @evidence = Evidence.find(params[:id])

    enforce_author(@evidence)

    @page_title = "Edit Evidence #{@evidence.id}"
  end
  
  def change_location
    can!(:update_evidence)

    @evidence = Evidence.find(params[:id])
    @evidence.updated_by = session[:user]
    @evidence.evidence_locations.build(location_from: @evidence.location)
    @page_title = "Move Evidence #{@evidence.id}"
  end
  
  def create
    redirect_to evidences_url and return if params[:commit] == 'Cancel'

    can!(:update_evidence)

    @evidence = Evidence.new(evidence_params)
    @page_title = "New Evidence"
    @evidence.created_by = session[:user]
    @evidence.updated_by = session[:user]

    success = false
    if @evidence.save
      success = true
      user_action "Evidence ##{@evidence.id} Created."
      if params[:offense_id] && offense = Offense.find_by_id(params[:offense_id])
        offense.evidences << @evidence
      end
      if params[:investigation_id] && investigation = Investigation.find_by_id(params[:investigation_id])
        investigation.evidences << @evidence
      end
    end
    
    respond_to do |format|
      if success
        format.html { redirect_to @evidence, notice: 'Evidence was successfully created.' }
        format.json { render :show, status: :created, location: @evidence }
      else
        format.html { render :new }
        format.json { render json: @evidence.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_evidence)
    
    @evidence = Evidence.find(params[:id])
    
    redirect_to @evidence and return if params[:commit] == 'Cancel'
    
    enforce_author(@evidence)
    
    @page_title = "Edit Evidence #{@evidence.id}"
    @evidence.updated_by = session[:user]
    @evidence.legacy = false
    
    # remove photo if requested
    if params[:commit] == 'Delete'
      @evidence.evidence_photo = nil
    end
    
    success = false
    if @evidence.update(evidence_params)
      success = true
      if params[:commit] == 'Delete'
        user_action("Removed Evidence #{@evidence.id} Photo.")
      else
        user_action "Evidence ##{@evidence.id} Updated"
      end
    end
    
    respond_to do |format|
      if success
        format.html { redirect_to @evidence, notice: 'Evidence was successfully updated.' }
        format.json { render :show, status: :ok, location: @evidence }
      else
        if params[:evidence][:photo_form] == '1'
          format.html { render :photo }
        else
          format.html { render :edit }
        end
        format.json { render json: @evidence.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_evidence)
    
    @evidence = Evidence.find(params[:id])
    
    enforce_author(@evidence)
    
    @evidence.destroy
    user_action "Evidence ##{@evidence.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to evidences_url, notice: 'Evidence was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def photo
    can!(:update_evidence)
    
    @evidence = Evidence.find(params[:id])
    
    enforce_author(@evidence)
    
    @page_title = "Evidence Photo"
    @evidence.updated_by = session[:user]
  end
  
  def inventory
    @evidences = Evidence.by_location

    if @evidences.empty?
      flash.alert = "Nothing to report!"
      redirect_to evidences_url and return
    end

    if pdf_url = make_pdf_file("evidence_inventory", Evidences::Inventory.new(@evidences))
      redirect_to pdf_url
      user_action("Printed Evidence Inventory List")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to evidences_url
    end
  end
  
  private
  
  def evidence_params
    params.require(:evidence).permit(:evidence_number, :case_no, :fingerprint_class, :dna_profile, :owner_id, :description, :disposed,
      :disposition, :remarks, :evidence_photo, :photo_form
    )
  end
end
