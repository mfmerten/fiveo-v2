class JailsController < ApplicationController
  skip_before_action :require_base_perms, only: [:show, :edit, :update]
  
  def index
    options = {
      scopes: [:by_name],
      paginate: true
    }
    @jails = get_records(options)
    
    if can?(:update_jail)
      @links.push(['New', new_jail_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @jail = Jail.find(params[:id])
    
    unless can?(@jail.admin_permission)
      can!(:view_jail)
    end
    
    @page_title = "Jail #{@jail.id}"
    
    if can?(:update_jail)
      @links.push(['New', new_jail_path])
    end
    if can?(:update_jail) || can?(@jail.admin_permission)
      @links.push(['Edit', edit_jail_path(@jail)])
    end
    if can?(:destroy_jail)
      @links.push(['Delete', @jail, {method: 'delete', data: {confirm: confirm_msg_for(:jail)}}])
    end
    if can?(:view_booking) && can?(@jail.view_permission)
      @links.push(['Bookings', jail_bookings_path(@jail)])
    end
    
    @page_links.push([@jail.creator,'creator'],[@jail.updater,'updater'])
  end

  def new
    can!(:update_jail)

    @jail = Jail.new
    @page_title = "New Jail"
    @jail.cells.build
  end
    
  def edit
    @jail = Jail.find(params[:id])

    unless can?(@jail.admin_permission)
      can!(:update_jail)
    end

    @page_title = "Edit Jail #{@jail.id}"
  end
  
  def create
    redirect_to jails_url and return if params[:commit] == 'Cancel'

    can!(:update_jail)

    @jail = Jail.new(jail_params)
    @jail.created_by = session[:user]
    @jail.updated_by = session[:user]
    @page_title = "New Jail"

    success = @jail.save
    user_action "Jail ##{@jail.id} Created." if success

    respond_to do |format|
      if success
        format.html { redirect_to @jail, notice: 'Jail was successfully created.' }
        format.json { render :show, status: :created, location: @jail }
      else
        format.html { render :new }
        format.json { render json: @jail.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @jail = Jail.find(params[:id])
    
    redirect_to @jail and return if params[:commit] == 'Cancel'
    
    unless can?(@jail.admin_permission)
      can!(:update_jail)
    end
    
    @page_title = "Edit Jail #{@jail.id}"
    @jail.updated_by = session[:user]
    
    success = @jail.update(jail_params)
    user_action "Jail ##{@jail.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @jail, notice: 'Jail was successfully updated.' }
        format.json { render :show, status: :ok, location: @jail }
      else
        format.html { render :edit }
        format.json { render json: @jail.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_jail)
    
    @jail = Jail.find(params[:id])
    
    if reasons = @jail.reasons_not_destroyable
      flash.alert = "Cannot delete: #{reasons}"
      redirect_to @jail and return
    end
    
    @jail.destroy
    user_action "Jail ##{@jail.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to jails_url, notice: 'Jail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  
  def jail_params
    params.require(:jail).permit(:name, :acronym, :street, :city, :state, :zip, :phone, :fax, :male, :female, :juvenile, :permset,
      :afis_enabled, :goodtime_by_default, :goodtime_hours_per_day, :notify_frequency, :remarks,
      cells_attributes: [:id, :name, :description, :_destroy], absent_types_attributes: [:id, :name, :description, :_destroy]
    )
  end
end