class ExternalLinksController < ApplicationController
  skip_before_action :require_base_perms, only: :all # viewable by everyone
  
  def index
    options = {
      scopes: [:by_name],
      includes: [:creator],
      paginate: true
    }
    @external_links = get_records(options)
    
    if can?(:update_link)
      @links.push(["New", new_external_link_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @external_link = ExternalLink.find(params[:id])
    @page_title = "External Link #{@external_link.id}"
    
    if can?(:update_link)
      @links.push(["New", new_external_link_path])
      @links.push(["Edit", edit_external_link_path(@external_link)])
    end
    if can?(:destroy_link)
      @links.push(["Delete", @external_link, {method: 'delete', data: {confirm: confirm_msg_for(:external_link)}}])
    end
    
    @page_links.push([@external_link.creator, 'Creator'], [@external_link.updater, 'Updater'])
  end

  def all
    @external_links = ExternalLink.by_name.all
    @page_title = "Other Websites"
    
    @links.push(["Home", root_path])
  end
  
  def new
    can!(:update_link)

    @external_link = ExternalLink.new
    @page_title = "New External Link"
  end

  def edit
    can!(:update_link)

    @external_link = ExternalLink.find(params[:id])
    @page_title = "Edit External Link #{@external_link.id}"
  end

  def create
    redirect_to external_links_url and return if params[:commit] == 'Cancel'
    can!(:update_link)
    
    @external_link = ExternalLink.new(external_link_params)
    
    @page_title = "New External Link"
    @external_link.created_by = session[:user]
    @external_link.updated_by = session[:user]
    
    success = @external_link.save
    user_action "External Link ##{@external_link.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @external_link, notice: 'External Link was successfully created.' }
        format.json { render :show, status: :created, location: @external_link }
      else
        format.html { render :new }
        format.json { render json: @external_link.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_link)

    @external_link = ExternalLink.find(params[:id])

    redirect_to @external_link and return if params[:commit] == 'Cancel'

    @page_title = "Edit External Link #{@external_link.id}"
    @external_link.updated_by = session[:user]

    success = @external_link.update(external_link_params)
    user_action "External Link ##{@external_link.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @external_link, notice: 'External Link was successfully updated.' }
        format.json { render :show, status: :ok, location: @external_link }
      else
        format.html { render :edit }
        format.json { render json: @external_link.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_link)
    
    @external_link = ExternalLink.find(params[:id])
    
    @external_link.destroy
    user_action "External Link ##{@external_link.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to external_links_url, notice: 'External Link was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  
  def external_link_params
    params.require(:external_link).permit(:name, :url, :front_page, :short_name, :description)
  end
end