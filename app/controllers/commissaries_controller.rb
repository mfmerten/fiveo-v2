class CommissariesController < ApplicationController
  before_action :check_if_enabled
  before_action :load_jail
  
  def index
    options = {
      scopes: [:unposted, :by_name],
      paginate: true
    }
    @commissaries = get_records(options)
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @commissary = @jail.commissaries.find(params[:id])
    @page_title = "#{@jail.acronym} Commissary Transaction #{@commissary.id}"
    
    if can?(:update_commissary, @jail.update_permission)
      @links.push(["New", new_jail_commissary_path(@jail, person_id: @commissary.person_id)])
      unless @commissary.report_datetime?
        @links.push(["Edit", edit_jail_commissary_path(@jail,@commissary)])
        @links.push(["Delete", [@jail,@commissary], {method: 'delete', data: {confirm: confirm_msg_for(:commissary_transaction)}}])
      end
    end

    @page_links.push(@commissary.person)
    if !@commissary.person.bookings.empty? && !@commissary.person.bookings.last.release_datetime?
      @page_links.push([@jail, @commissary.person.bookings.last])
    end
  end

  def new
    can!(:update_commissary, @jail.update_permission)

    unless @person = Person.find_real_identity(params[:person_id])
      flash.alert = 'No valid person specified.'
      redirect_to jail_commissaries_url(@jail) and return
    end

    @commissary = @jail.commissaries.build(transaction_date: Date.today, person_id: @person.id)
    set_officer_to_session(:officer)
    @page_title = "New #{@jail.acronym} Commissary Transaction"
  end

  def edit
    can!(:update_commissary, @jail.update_permission)

    @commissary = @jail.commissaries.find(params[:id])

    if @commissary.report_datetime?
      flash.alert = 'Cannot edit posted transactions.'
      redirect_to [@jail, @commissary] and return
    end
    
    @person = @commissary.person
    @commissary.officer_id = nil
    @page_title = "Edit #{@jail.acronym} Commissary Transaction #{params[:id]}"
  end
  
  def create
    can!(:update_commissary, @jail.update_permission)
    
    redirect_to jail_commissaries_url(@jail) and return if params[:commit] == 'Cancel'
    
    @commissary = @jail.commissaries.build(commissary_params)
    @person = @commissary.person
    @page_title = "New #{@jail.acronym} Commissary Transaction"
    @commissary.created_by = session[:user]
    @commissary.updated_by = session[:user]

    success = @commissary.save
    user_action "Commissary ##{@commissary.id} Created." if success

    respond_to do |format|
      if success
        format.html { redirect_to [@jail, @commissary], notice: 'Commissary was successfully created.' }
        format.json { render :show, status: :created, location: [@jail, @commissary] }
      else
        format.html { render :new }
        format.json { render json: @commissary.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_commissary, @jail.update_permission)
    
    @commissary = @jail.commissaries.find(params[:id])
    
    redirect_to [@jail, @commissary] and return if params[:commit] == 'Cancel'
    
    if @commissary.report_datetime?
      flash.alert = 'Cannot edit posted transactions.'
      redirect_to [@jail, @commissary] and return
    end
    
    @person = @commissary.person
    @page_title = "Edit #{@jail.acronym} Commissary Transaction #{params[:id]}"
    @commissary.updated_by = session[:user]
    
    success = @commissary.update(commissary_params)
    user_action "Commissary ##{@commissary.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@jail, @commissary], notice: 'Commissary was successfully updated.' }
        format.json { render :show, status: :ok, location: [@jail, @commissary] }
      else
        format.html { render :edit }
        format.json { render json: @commissary.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:update_commissary, @jail.update_permission)
    
    @commissary = @jail.commissaries.find(params[:id])
    
    if reasons = @commissary.reasons_not_destroyable
      flash.alert = "Cannot delete: #{reasons}"
      redirect_to [@jail, @commissary] and return
    end
    
    @commissary.destroy
    user_action "Commissary ##{@commissary.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to jail_commissaries_url(@jail), notice: 'Commissary was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def fund_balance
    @trans = @jail.commissaries.for_balance.all
    
    if @trans.empty?
      flash.alert = "Nothing to report!"
      redirect_to jail_commissaries_url(@jail) and return
    end

    if pdf_url = make_pdf_file("fund_balance", Commissaries::FundBalance.new(@jail, @trans))
      redirect_to pdf_url
      user_action("Printed #{@jail.acronym} Commissary Fund Balance Report.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to jail_commissaries_url(@jail)
    end
  end

  def posting_report
    @status = params[:status]
    
    if @status == 'post'
      can!(:update_commissary, @jail.update_permission)
    end
    
    valid_status = ['post','reprint','preview']
    unless valid_status.include?(@status)
      @status = 'preview'
    end
    
    if @status == 'reprint'
      unless report_datetime = AppUtils.valid_datetime(params[:post_datetime])
        flash.alert = "Valid posting date/time is required for reprints!"
        redirect_to jail_commissaries_url(@jail) and return
      end
      @report_time = report_datetime
    else
      @report_time = Time.now
    end
    
    if @status == 'reprint'
      @deposits = @jail.commissaries.posted_on(@report_time).deposits.by_name
      @withdrawals = @jail.commissaries.posted_on(@report_time).withdrawals.by_name
    else
      @deposits = @jail.commissaries.unposted.deposits.by_name
      @withdrawals = @jail.commissaries.unposted.withdrawals.by_name
    end
    
    if @deposits.empty? && @withdrawals.empty?
      flash.alert = "Nothing to report!"
      redirect_to jail_commissaries_url(@jail) and return
    end
    
    if pdf_url = make_pdf_file("commissary_posting", Commissaries::PostingReport.new(@report_time, @status, @jail, @withdrawals, @deposits))
      redirect_to pdf_url
      user_action("Printed #{@jail.acronym} Posting Report (#{@status.titleize}).")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to jail_commissaries_url(@jail)
    end
  end

  def transaction_report
    @report_type = params[:status]
    unless ['detailed','summary'].include?(@report_type)
      @report_type = 'summary'
    end
    # note: defaulting to a month-to-date report
    unless @start_date = AppUtils.valid_date(params[:start_date])
      @start_date = Date.today.beginning_of_month
    end
    unless @stop_date = AppUtils.valid_date(params[:end_date])
      @end_date = Date.today
    end
    if @start_date > @stop_date
      # wrong order, swap
      @start_date, @stop_date = @stop_date, @start_date
    end
    @person = nil
    if !params[:person_id].blank?
      @person = Person.find(params[:person_id])
    end
    @commissaries = @jail.commissaries.belonging_to_person(@person.id).between(@start_date,@stop_date).by_date

    if @commissaries.empty?
      flash.alert = "Nothing to report!"
      redirect_to jail_commissaries_url(@jail) and return
    end
    
    status = ['summary','detailed'].include?(params[:status]) ? params[:status].titleize : 'Summary'
    
    if pdf_url = make_pdf_file("commissary_transactions", Commissaries::TransactionReport.new(@report_type, @jail, @commissaries, @start_date, @stop_date, @person))
      redirect_to pdf_url
      user_action("Printed #{@jail.acronym} #{status} Commissary Report")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to jail_commissaries_url(@jail)
    end
  end

  private
  
  def load_jail
    @jail = Jail.find(params[:jail_id])
    can!(@jail.view_permission)
  end

  def check_if_enabled
    unless SiteConfig.enable_internal_commissary
      flash.alert = "Internal Commissary Is Disabled By Administrator."
      redirect_to root_url and return
    end
  end
  
  def commissary_params
    params.require(:commissary).permit(:person_id, :transaction_date, :category, :officer_id, :officer_badge, :officer_unit,
      :officer, :deposit, :withdraw, :receipt_no, :memo)
  end
end