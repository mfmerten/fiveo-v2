class WarrantsController < ApplicationController

  def index
    options = {
      active: true,
      scopes: [:by_name],
      includes: [:person, :charges],
      paginate: true
    }
    @warrants = get_records(options)
    
    if can?(:update_warrant)
      @links.push(['New', new_warrant_path])
    end
    if Warrant.photos_available?
      @links.push(['Photo Album', photo_album_warrants_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    if params[:info_id]
      @info_id = params[:info_id]
      @warrant = Warrant.find_by_id(params[:id])
    else
      @warrant = Warrant.find(params[:id])
      @page_title = "Warrant #{@warrant.id}"
      
      if can?(:update_warrant)
        @links.push(['New', new_warrant_path])
        @links.push(['Edit', edit_warrant_path(@warrant)])
      end
      if can?(:destroy_warrant)
        @links.push(['Delete', @warrant, {method: :delete, data: {confirm: confirm_msg_for(:warrant)}}])
      end
      if can?(:update_warrant)
        @links.push(["Photo", photo_warrant_path(@warrant)])
      end
      unless @warrant.person.nil? || @warrant.person.bookings.empty?
        unless @warrant.person.bookings.last.release_datetime?
          if can?(:update_arrest)
            @links.push(['Serve', new_arrest_path(person_id: @warrant.person_id, warrant_id: @warrant.id, booking_id: @warrant.person.bookings.last.id)])
          end
        end
      end
      
      @page_links.push(@warrant.person, @warrant.arrest, [@warrant.creator,'creator'],[@warrant.updater,'updater'])
      
      @matches = Person.for_warrant(@warrant.id).reject{|p| p.warrant_exemptions.include?(@warrant)}
    end
    respond_to :html, :json, :js
  end

  def new
    can!(:update_warrant)
    
    @warrant = Warrant.new(received_date: Date.today, issued_date: Date.today, name: "", state: SiteConfig.agency_state)
    if params[:person_id]
      @warrant.person_id = params[:person_id]
      unless @warrant.person.nil?
        @warrant.given_name = @warrant.person.given_name
        @warrant.family_name = @warrant.person.family_name
        @warrant.suffix = @warrant.person.suffix
        @warrant.street = @warrant.person.physical_line1
        @warrant.city = @warrant.person.phys_city
        @warrant.state = @warrant.person.phys_state
        @warrant.zip = @warrant.person.phys_zip
        @warrant.dob = @warrant.person.date_of_birth
        @warrant.ssn = @warrant.person.ssn
        @warrant.oln = @warrant.person.oln
        @warrant.oln_state = @warrant.person.oln_state
        @warrant.full_name = @warrant.person.sort_name
        @warrant.sex = @warrant.person.sex
        @warrant.race = @warrant.person.race
      end
    end
    @page_title = "New Warrant"
    @warrant.charges.build
  end
    
  def edit
    can!(:update_warrant)

    @warrant = Warrant.find(params[:id])
    @page_title = "Edit Warrant #{@warrant.id}"
    @warrant.jurisdiction_id = nil
    @warrant.full_name = @warrant.sort_name
    if @warrant.charges.empty?
      @warrant.charges.build(charge_type: @warrant.charge_type)
    end
  end
  
  def create
    redirect_to warrants_url and return if params[:commit] == 'Cancel'

    can!(:update_warrant)

    @warrant = Warrant.new(warrant_params)
    @page_title = "New Warrant"
    @warrant.created_by = session[:user]
    @warrant.updated_by = session[:user]
    success = @warrant.save
    user_action "Warrant ##{@warrant.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @warrant, notice: 'Warrant was successfully created.' }
        format.json { render :show, status: :created, location: @warrant }
      else
        format.html { render :new }
        format.json { render json: @warrant.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_warrant)
    
    @warrant = Warrant.find(params[:id])
    
    redirect_to @warrant and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Warrant #{@warrant.id}"
    @warrant.full_name = @warrant.sort_name
    @warrant.updated_by = session[:user]
    @warrant.legacy = false
    
    # remove photo if requested
    if params[:commit] == 'Delete'
      @warrant.photo = nil
    end
    
    success = false
    if @warrant.update(warrant_params)
      success = true
      if params[:commit] == 'Delete'
        user_action("Removed Photo for Warrant #{@warrant.id}.")
      else
        user_action "Warrant ##{@warrant.id} Updated."
      end
      if params[:warrant][:photo_form] == '0' && params[:investigation_id] && investigation = Investigation.find_by_id(params[:investigation_id])
        investigation.invest_warrants.create(warrant_id: @warrant.id)
      end
    end
    
    respond_to do |format|
      if success
        format.html { redirect_to @warrant, notice: 'Warrant was successfully updated.' }
        format.json { render :show, status: :ok, location: @warrant }
      else
        if params[:warrant][:photo_form] == '1'
          format.html { render :photo }
        else
          format.html { render :edit }
        end
        format.json { render json: @warrant.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_warrant)
    
    @warrant = Warrant.find(params[:id])
    
    if reasons = @warrant.reasons_not_destroyable
      flash.alert = "Cannot delete: #{reasons}"
      redirect_to @warrant and return
    end
    
    @warrant.destroy
    user_action "Warrant ##{@warrant.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to warrants_url, notice: 'Warrant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def photo
    can!(:update_warrant)
    
    @warrant = Warrant.find(params[:id])
    @warrant.updated_by = session[:user]
    @page_title = "Warrant Photo"
  end
  
  def photo_album
    options = {
      scopes: [:active, :photos_available, :by_name]
    }
    @warrants = get_records(options)
        
    if @warrants.empty?
      flash.alert = 'No Photos Available!'
      redirect_to warrants_url and return
    end
    
    @page_title = 'Photo Album: Warrants'
    
    @links.push(["Warrants", warrants_path])
  end
  
  def list_form
    options = {
      scopes: [:active, :by_name],
      includes: [:person]
    }
    @warrants = get_records(opitons)
    
    if @warrants.empty?
      flash.alert = "Nothing to report!"
      redirect_to warrants_url and return
    end

    if pdf_url = make_pdf_file("list", Warrants::List.new(@warrants))
      redirect_to pdf_url
      user_action("Printed Warrant List.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to warrants_url
    end
  end

  def update_person
    @name = ''
    @person = Person.new
    @valid = false
    
    if can?(:view_person)
      if params[:c] && p = Person.find_by(id: params[:c])
        @person = p
        @name = p.sort_name
        @valid = true
      else
        @name = "Invalid Person ID"
      end
    else
      @name = "Permission Denied!"
    end
    
    respond_to :js
  end
  
  private
  
  def warrant_params
    params.require(:warrant).permit(:person_id, :full_name, :ssn, :dob, :oln, :oln_state, :race, :sex, :street, :city, :state, :zip, :issued_date,
      :received_date, :jurisdiction_id, :jurisdiction, :phone1_type, :phone1, :phone1_unlisted, :phone2_type, :phone2, :phone2_unlisted,
      :phone3_type, :phone3, :phone3_unlisted, :special_type, :warrant_no, :payable, :bond_amt, :disposition, :dispo_date, :remarks,
      :photo, :photo_form, charges_attributes: [:id, :charge_type, :count, :charge, :agency_id, :agency, :_destroy]
    )
  end
end
