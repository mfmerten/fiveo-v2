class DispositionsController < ApplicationController
  
  def index
    options = {
      active: true,
      scopes: [:by_id],
      includes: [:person],
      paginate: true
    }
    
    @dispositions = get_records(options)
    
    @links.push(["Update Status", status_update_dispositions_path])
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @disposition = Disposition.find(params[:id])
    @page_title = "Disposition #{@disposition.id}"

    if can?(:update_court)
      @links.push(['Edit', edit_disposition_path(@disposition)])      
    end
    if @disposition.disposition?
      @links.push(['Report', forms_dispositions_path(id: @disposition.id), {method: 'post', pdf: true}])
    end

    @page_links.push(@disposition.person, [@disposition.creator,'creator'],[@disposition.updater,'updater'])
  end
  
  # no new/create needed
  
  def edit
    can!(:update_court)

    @disposition = Disposition.find(params[:id])
    @page_title = "Edit Disposition #{@disposition.id}"
    unless @disposition.disposition?
      @disposition.disposition = @disposition.suggested_disposition(afis: true)
      @disposition.report_date = nil
    end
  end
  
  def update
    can!(:update_court)

    @disposition = Disposition.find(params[:id])

    redirect_to @disposition and return if params[:commit] == 'Cancel'

    @disposition.updated_by = session[:user]

    success = @disposition.update(disposition_params)
    user_action "Disposition ##{@disposition.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @disposition, notice: 'Disposition was successfully updated.' }
        format.json { render :show, status: :ok, location: @disposition }
      else
        format.html { render :edit }
        format.json { render json: @disposition.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def status_report
    @dispositions = Disposition.active.includes(:person).all
    
    if @dispositions.empty?
      flash.alert = "Nothing to report!"
      redirect_to dispositions_url and return
    end
    
    if pdf_url = make_pdf_file("status", Dispositions::Status.new(@dispositions))
      redirect_to pdf_url
      user_action("Printed Status Report")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to dispositions_url
    end
  end
  
  def forms
    if SiteConfig.afis_ori.blank? || SiteConfig.afis_agency.blank? || SiteConfig.afis_street.blank? || SiteConfig.afis_city.blank? || SiteConfig.afis_state.blank? || SiteConfig.afis_zip.blank?
      flash.alert = "Missing Site Configuration settings required for this report"
      redirect_to dispositions_url and return
    end
    
    disposition = Disposition.find_by_id(params[:id])
    if disposition.nil?
      @dispositions = Disposition.final.with_disposition.joins(:person).all
    else
      if disposition.disposition? && !disposition.person.nil?
        @dispositions = [disposition]
      else
        @dispositions = []
      end
    end
    
    if @dispositions.empty?
      flash.alert = "Nothing to report!"
      if disposition.nil?
        redirect_to dispositions_url
      else
        redirect_to disposition
      end
      return
    end
    
    if pdf_url = make_pdf_file("dispositions", Dispositions::Forms.new(@dispositions))
      redirect_to pdf_url
      user_action("Printed Disposition Forms")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to dispositions_url
    end
  end
  
  def status_update
    Disposition.update_status
    redirect_to dispositions_url
    user_action("Initiated manual status update.")
  end

  private
  
  def disposition_params
    params.require(:disposition).permit(:created_by, :updated_by, :disposition, :remarks)
  end
end