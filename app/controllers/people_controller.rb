class PeopleController < ApplicationController
  
  def index
    options = {
      scopes: [:by_name],
      includes: [:active_warrants, :active_forbids, :possible_warrants, :possible_forbids, {bookings: :jail}],
      paginate: true
    }
    @people = get_records(options)
    
    @links.push(['New', new_person_path],['Photos', photo_album_people_path])
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end
  
  def show
    if params[:info_id]
      @info_id = params[:info_id]
      @id_id = params[:id_id]
      @name_id = params[:name_id]
      if params[:alias_ok]
        @person = Person.find_by_id(params[:id])
      else
        @person = Person.find_real_identity(params[:id])
      end
    else
      @person = Person.find(params[:id])
      if @person.alias_for.nil?
        @alias = @person
      else
        @alias = @person.alias_for
      end
      @page_title = "Person #{@person.id}"
      
      if !@person.bookings.empty? && !@person.bookings.last.release_datetime?
        @page_links.push([@person.bookings.last.jail, @person.bookings.last])
      end
      unless @person.alias_for.nil?
        @page_links.push([@person.real_identity,'real i d'])
      end
      @person.aliases.each do |p|
        @page_links.push([p,'alias'])
      end
      
      if can?(:update_person)
        @links.push(["New", new_person_path])
        @links.push(["Edit", edit_person_path(@person)])
      end
      if can?(:destroy_person) && @person.reasons_not_destroyable.nil?
        @links.push(["Delete", @person, {method: :delete, data: {confirm: confirm_msg_for(:person)}}])
      end
      if can?(:update_person)
        @links.push(["Front Mugshot", front_mugshot_person_path(id: @person.id)])
        @links.push(["Side Mugshot", side_mugshot_person_path(id: @person.id)])
      end
      @links.push(["Medications", person_medications_path(@person)])
      @links.push(["Rap Sheet", rap_sheet_person_path(id: @person.id), pdf: true])
      @links.push(["View Links", links_person_path(id: @person.id)])
      if can?(:update_person) && !@person.alias_for.nil?  && @person.reasons_not_destroyable.nil?
        @links.push(["Merge", merge_person_path(id: @person.id), {red: true}])
      end
      if can?(:update_citation) && @person.alias_for.nil?
        @links.push(["New Citation", new_citation_path(person_id: @person.id)])
      end
      if can?(:update_commissary) && @person.alias_for.nil? && !@person.bookings.empty? && !@person.bookings.last.jail.nil? && SiteConfig.enable_internal_commissary
        jail = @person.bookings.last.jail
        @links.push(["#{jail.acronym} Commis.", new_jail_commissary_path(jail, person_id: @person.id)])
      end
      if can?(:update_forbid) && @person.alias_for.nil?
        @links.push(["New Forbid", new_forbid_path(person_id: @person.id)])
      end
      if can?(:update_warrant) && @person.alias_for.nil?
        @links.push(["New Warrant", new_warrant_path(person_id: @person.id)])
      end
      if can?(:update_transport) && @person.alias_for.nil?
        @links.push(["New Transport", new_transport_path(person_id: @person.id)])
      end
      # if not currently in jail, see which jails are appropriate
      if @person.bookings.empty? || @person.bookings.last.release_datetime?
        if can?(:update_booking) && @person.alias_for.nil?
          jails = []
          Jail.all.each do |j|
            jok = true
            unless @person.sex == 1 || @person.sex == 2
              jok = false
            end
            if !j.male? && @person.sex == 1
              jok = false
            end
            if !j.female? && @person.sex == 2
              jok = false
            end
            if !j.juvenile? && @person.date_of_birth? && !@person.age.nil? && @person.age < SiteConfig.adult_age
              jok = false
            end
            jails.push(j) if jok
          end
          jails.each do |j|
            if can?(j.update_permission)
              @links.push(["Book In #{j.acronym}", new_jail_booking_path(j, person_id: @person.id)])
            end
          end
        end
      end
      
      @commissary = []
      if can?(:view_commissary) && SiteConfig.enable_internal_commissary
        @commissary = @person.past_days(30).reverse
      end
      
      @data = People::RapSheet.data(@person.id)
      
      @page_links.push([@person.creator, 'creator'],[@person.updater, 'updater'])
    end
    respond_to :html, :json, :js
  end
  
  def new
    can!(:update_person)
    
    @person = Person.new(phys_state: SiteConfig.agency_state, mail_state: SiteConfig.agency_state, full_name: "")
    @person.marks.build
    @person.associates.build
    @page_title = "New Person"
  end
  
  def edit
    can!(:update_person)

    @person = Person.find(params[:id])
    @person.full_name = @person.sort_name
    @page_title = "Edit Person #{@person.id}"
  end
  
  def create
    can!(:update_person)

    redirect_to people_url and return if params[:commit] == 'Cancel'

    @person = Person.new(person_params)
    @page_title = "New Person"
    @person.created_by = session[:user]
    @person.updated_by = session[:user]

    success = @person.save
    user_action "Person ##{@person.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @person, notice: 'Person was successfully created.' }
        format.json { render :show, status: :created, location: @person }
      else
        format.html { render :new }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_person)
    
    @person = Person.find(params[:id])
    
    redirect_to @person and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Person #{@person.id}"
    @person.updated_by = session[:user]
    @person.legacy = false
    
    # remove photo if requested
    if params[:commit] == 'Delete'
      if params[:person][:photo_form] == '1'
        @person.front_mugshot = nil
      elsif params[:person][:photo_form] == '2'
        @person.side_mugshot = nil
      end
    end
    
    success = false
    if @person.update(person_params)
      success = true
      if params[:commit] == 'Delete'
        if params[:person][:photo_form] == '1'
          user_action("Removed Front Mugshot for Person #{@person.id}.")
        elsif params[:person][:photo_form] == '2'
          user_action("Removed Side Mugshot for Person #{@person.id}.")
        end
      else
        user_action "Person ##{@person.id} Updated."
      end
    end
    
    respond_to do |format|
      if success
        format.html { redirect_to @person, notice: 'Person was successfully updated.' }
        format.json { render :show, status: :ok, location: @person }
      else
        if params[:person][:photo_form] == '1'
          format.html { render :front_mugshot }
        elsif params[:person][:photo_form] == '2'
          format.html { render :side_mugshot }
        else
          format.html { render :edit }
        end
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  def front_mugshot
    can!(:update_person)
    
    @person = Person.find(params[:id])
    @page_title = "Front Mugshot"
  end

  def side_mugshot
    can!(:update_person)
    
    @person = Person.find(params[:id])
    @page_title = "Side Mugshot"
  end

  def photo_album
    options = {
      scopes: [:by_name, :with_front_mugshot]
    }
    @people = get_records(options)
    
    if @people.empty?
      flash.alert = 'No Photos Available!'
      redirect_to people_url and return
    end
    
    @page_title = 'Photo Album: People'
    
    @links.push(["People", people_path])
  end

  def destroy
    can!(:destroy_person)
    
    @person = Person.find(params[:id])
    
    if reasons = @person.reasons_not_destroyable
      flash.alert = "Cannot delete: #{reasons}"
      redirect_to @person and return
    end
    
    @person.destroy
    user_action "Person ##{@person.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to people_url, notice: 'Person was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def merge
    can!(:update_person)
    
    @old_person = Person.find(params[:id])
    @old_person.full_name = @old_person.sort_name
    
    # make sure alias is set properly
    if @old_person.alias_for.nil?
      flash.alert = 'Cannot Merge: It is not an alias'
      redirect_to @old_person and return
    end
    
    # do not allow merge if this person cannot be deleted
    # because of linked records
    unless @old_person.can_be_deleted?
      flash.alert = "Cannot Merge: This person has linked records or aliases!"
      redirect_to @old_person and return
    end
    
    @person = @old_person.alias_for
    @person.full_name = @person.sort_name
    
    # make sure alias is not a loop
    if @old_person == @person
      flash.alert = "Cannot Merge: This person is aliased to itself!"
      redirect_to @old_person and return
    end
    
    @person.legacy = false
    @page_title = "Merging #{@old_person.sort_name} Into #{@person.sort_name}"
    @person.updated_by = session[:user]
    
    if request.post?
      if params[:commit] == "Cancel"
        redirect_to @old_person and return
      end
      
      params[:person][:existing_mark_attributes] ||= {}
      params[:person][:existing_associate_attributes] ||= {}
      @person.attributes = person_params
      if @person.is_alias_for? && @person.is_alias_for = @person.id
        @person.is_alias_for = nil
      end
      
      if @person.valid?
        user_action("Merged Person: #{AppUtils.show_person(@old_person)} into Person: #{AppUtils.show_person(@person)}!")
        if @person.sort_name != @old_person.sort_name
          @person.aka = [@person.aka, @old_person.display_name].join(", ")
        end
        @person.save(validate: false)
        
        # shouldn't be any, but just in case...
        @old_person.invest_victims.each{|v| v.update_attribute(:person_id, @person.id)}
        @old_person.invest_witnesses.each{|w| w.update_attribute(:person_id, @person.id)}
        @old_person.invest_sources.each{|w| w.update_attribute(:person_id, @person.id)}
        
        if @old_person.destroy
          flash.notice = "Merge Succeeded: old person deleted!"
          redirect_to(@person)
        else
          flash.alert = "Merge failed: old person not deleted!"
          redirect_to(@old_person)
        end
      end
    end
  end

  def choose
    @id_field = params[:id_field]
    @name_field = params[:name_field]
    @name = params[:name]
    @people = nil
    
    unless @name.blank?
      @people = Person.with_name(@name)
    end
    
    respond_to :js
  end

  def pick_list
    @people = Person.with_name(params[:name]).includes(:marks)
    @page_title = "Results for '#{params[:name]}'"
    @id_field = params[:id_field]
    @name_field = params[:name_field]
    render layout: 'popup'
  end

  def verify_warrant
    can!(:update_person)
    
    vperson = Person.find(params[:id])
    warrant = Warrant.find(params[:warrant_id])
    warrant.update_attribute(:person_id, vperson.id)
    vperson.update_warrant_matches
    redirect_to vperson
    user_action("Verified Warrant #{warrant.id} is for Person #{vperson.id}")
  end

  def warrant_exemption
    can!(:update_person)
    
    vperson = Person.find(params[:id])
    warrant = Warrant.find(params[:warrant_id])
    if warrant.person_id == vperson.id
      warrant.update_attribute(:person_id, nil)
    end
    if params[:restore] == '1'
      vperson.exempted_warrants.delete(warrant)
      user_action("Removed Exemption for Warrant #{warrant.id} from Person #{vperson.id}")
    else
      vperson.exempted_warrants << warrant unless vperson.exempted_warrants.include?(warrant)
      user_action("Added Exemption for Warrant #{warrant.id} to Person #{vperson.id}")
    end
    vperson.update_warrant_matches
    redirect_to vperson
  end

  def verify_forbid
    can!(:update_person)
    
    vperson = Person.find(params[:id])
    forbid = Forbid.find(params[:forbid_id])
    forbid.update_attribute(:forbidden_id, vperson.id)
    vperson.update_forbid_matches
    redirect_to vperson
    user_action("Verified Forbid #{forbid.id} is for Person #{vperson.id}")
  end

  def forbid_exemption
    can!(:update_person)
    
    vperson = Person.find(params[:id])
    forbid = Forbid.find(params[:forbid_id])
    if forbid.forbidden_id == vperson.id
      forbid.update_attribute(:forbidden_id, nil)
    end
    if params[:restore] == '1'
      vperson.exempted_forbids.delete(forbid)
      user_action("Removed Exemption for Forbid #{forbid.id} from Person #{vperson.id}")
    else
      vperson.exempted_forbids << forbid unless vperson.exempted_forbids.include?(forbid)
      user_action("Added Exemption for Forbid #{forbid.id} to Person #{vperson.id}")
    end
    vperson.update_forbid_matches
    redirect_to vperson
  end

  def links
    @person = Person.find(params[:id])
    @page_title = "Links for #{AppUtils.show_person(@person)}"
    
    @links.push(['Return', person_path(@person)])
  end

  def rap_sheet
    @person = Person.find(params[:id])
    
    if pdf_url = make_pdf_file("rap-sheet", People::RapSheet.new(@person.id))
      redirect_to pdf_url
      user_action("Printed Rap Sheet for Person #{params[:id]}.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to @person
    end
  end

  private
  
  def person_params
    params.require(:person).permit(:full_name, :aka, :is_alias_for, :home_phone, :cell_phone, :email, :phys_street, :phys_city, :phys_state,
      :phys_zip, :mail_street, :mail_city, :mail_state, :mail_zip, :occupation, :employer, :date_of_birth, :place_of_birth, :ssn, :oln,
      :oln_state, :sid, :doc_number, :fbi, :fingerprint_class, :dna_profile, :dna_swab_date, :dna_blood_date, :sex_offender, :race, :sex,
      :build_type, :skin_tone, :complexion, :height_ft, :height_in, :weight, :hair_type, :hair_color, :beard, :mustache, :eyebrow,
      :eye_color, :glasses, :shoe_size, :emergency_contact, :em_relationship, :emergency_phone, :emergency_address, :med_allergies,
      :hepatitis, :hiv, :tb, :deceased, :gang_affiliation, :remarks, :front_mugshot, :side_mugshot, :photo_form,
      marks_attributes: [:id, :description, :location, :_destroy], associates_attributes: [:id, :relationship, :name, :remarks, :_destroy]
    )
  end
end
