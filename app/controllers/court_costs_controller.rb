class CourtCostsController < ApplicationController

  def index
    options = {
      scopes: [:by_category, :by_name],
      paginate: true
    }
    @court_costs = get_records(options)
    
    if can?(:update_option)
      @links.push(['New', new_court_cost_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @court_cost = CourtCost.find(params[:id])
    @page_title = "Court Cost #{@court_cost.id}"
    
    if can?(:update_option)
      @links.push(['New', new_court_cost_path])
      @links.push(['Edit', edit_court_cost_path(@court_cost)])
      @links.push(['Delete', @court_cost, {method: 'delete', data: {confirm: confirm_msg_for(:court_cost)}}])
    end
    
    @page_links.push([@court_cost.creator,'creator'],[@court_cost.updater,'updater'])
  end

  def new
    can!(:update_option)

    @court_cost = CourtCost.new
    @page_title = "New Court Cost"
  end
  
  def edit
    can!(:update_option)

    @court_cost = CourtCost.find(params[:id])
    @page_title = "Edit Court Cost #{@court_cost.id}"
  end
  
  def create
    can!(:update_option)

    redirect_to court_costs_url and return if params[:commit] == 'Cancel'

    @court_cost = CourtCost.new(court_cost_params)
    @page_title = "New Court Cost"
    @court_cost.created_by = session[:user]
    @court_cost.updated_by = session[:user]

    success = @court_cost.save
    user_action "Court Cost ##{@court_cost.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @court_cost, notice: 'Court Cost was successfully created.' }
        format.json { render :show, status: :created, location: @court_cost }
      else
        format.html { render :new }
        format.json { render json: @court_cost.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_option)

    @court_cost = CourtCost.find(params[:id])

    redirect_to @court_cost and return if params[:commit] == 'Cancel'

    @page_title = "Edit Court Cost #{@court_cost.id}"
    @court_cost.updated_by = session[:user]

    success = @court_cost.update(court_cost_params)
    user_action "Court Cost ##{@court_cost.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @court_cost, notice: 'Court Cost was successfully updated.' }
        format.json { render :show, status: :ok, location: @court_cost }
      else
        format.html { render :edit }
        format.json { render json: @court_cost.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:update_option)

    @court_cost = CourtCost.find(params[:id])

    @court_cost.destroy
    user_action "Court Cost ##{@court_cost.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to court_costs_url, notice: 'Court Cost was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  
  def court_cost_params
    params.require(:court_cost).permit(:name, :category, :amount)
  end
end
