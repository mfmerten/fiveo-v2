class SiteConfigurationsController < ApplicationController
  
  def site_main
    @page_title = "Main Site Configuration"
    
    @links.push(['Edit', edit_site_main_path])
  end
  
  def edit_site_main
    @page_title = 'Edit Main Site Configuration'
    
    if request.post?
      if params[:commit] == 'Cancel'
        SiteConfig.reload
        redirect_to site_main_url and return
      end
      
      SiteConfig.attributes = site_params
      if SiteConfig.valid?
        SiteConfig.save
        user_action("Updated Main Site Configuration.")
        flash.notice = "Main Site Configuration Updated."
        redirect_to site_main_url and return
      end
    end
  end
  
  def upload_logo
    if params[:commit] == 'Delete'
      SiteImg.logo = nil
    else
      SiteImg.attributes = site_image_params
    end
    
    unless SiteImg.save
      SiteImg.reload
      flash.alert = 'Warning! Could not save Logo'
      redirect_to site_main_url and return
    end
    
    redirect_to site_main_url
    if params[:commit] == 'Delete'
      user_action("Removed Logo Image")
    else
      user_action("Updated Logo Image")
    end
  end
  
  def upload_ceo_img
    if params[:commit] == 'Delete'
      SiteImg.ceo_img = nil
    else
      SiteImg.attributes = site_image_params
    end
    
    unless SiteImg.save
      SiteImg.reload
      flash.alert = 'Warning! Could not save CEO Image'
      redirect_to site_main_url and return
    end
    
    redirect_to site_main_url
    if params[:commit] == 'Delete'
      user_action("Removed CEO Image")
    else
      user_action("Updated CEO Image")
    end
  end
  
  def upload_header_left_img
    if params[:commit] == 'Delete'
      SiteImg.header_left_img = nil
    else
      SiteImg.attributes = site_image_params
    end
    
    unless SiteImg.save
      SiteImg.reload
      flash.alert = 'Warning! Could not save Header Left Image'
      redirect_to site_main_url and return
    end
    
    redirect_to site_main_url
    if params[:commit] == 'Delete'
      user_action("Removed Header Left Image")
    else
      user_action("Updated Header Left Image")
    end
  end
  
  def upload_header_right_img
    if params[:commit] == 'Delete'
      SiteImg.header_right_img = nil
    else
      SiteImg.attributes = site_image_params
    end
    
    unless SiteImg.save
      SiteImg.reload
      flash.alert = 'Warning! Could not save Header Right Image'
      redirect_to site_main_url and return
    end
    
    redirect_to site_main_url
    if params[:commit] == 'Delete'
      user_action("Removed Header Right Image")
    else
      user_action("Updated Header Right Image")
    end
  end
  
  def site_admin
    @page_title = "Admin Options"
    
    @links.push(['Edit', edit_site_admin_path])
  end
  
  def edit_site_admin
    @page_title = 'Edit Admin Options'
    
    if request.post?
      if params[:commit] == 'Cancel'
        SiteConfig.reload
        redirect_to site_admin_url and return
      end
      
      SiteConfig.attributes = site_params
      
      if SiteConfig.valid?
        SiteConfig.save
        user_action("Updated Admin Options.")
        flash.notice = "Admin Options Updated."
        redirect_to site_admin_url and return
      end
    end
  end
  
  def site_jail
    @page_title = "Jail Options"
    
    @links.push(['Edit', edit_site_jail_path])
  end
  
  def edit_site_jail
    @page_title = 'Edit Jail Options'
    
    if request.post?
      if params[:commit] == 'Cancel'
        SiteConfig.reload
        redirect_to site_jail_url and return
      end
      
      SiteConfig.attributes = site_params
      
      if SiteConfig.valid?
        SiteConfig.save
        user_action("Updated Jail Options.")
        flash.notice = "Jail Options Updated."
        redirect_to site_jail_url and return
      end
    end
  end

  def site_court
    @page_title = "Court Options"
    
    @links.push(['Edit', edit_site_court_path])
  end
  
  def edit_site_court
    @page_title = 'Edit Court Options'
    
    if request.post?
      if params[:commit] == 'Cancel'
        SiteConfig.reload
        redirect_to site_court_url and return
      end
      
      SiteConfig.attributes = site_params
      
      if SiteConfig.valid?
        SiteConfig.save
        user_action("Updated Court Options.")
        flash.notice = "Court Options Updated."
        redirect_to site_court_url and return
      end
    end
  end

  def site_dispatch
    @page_title = "Dispatch Options"
    
    @links.push(['Edit', edit_site_dispatch_path])
  end
  
  def edit_site_dispatch
    @page_title = 'Edit Dispatch Options'
    if request.post?
      if params[:commit] == 'Cancel'
        SiteConfig.reload
        redirect_to site_dispatch_url and return
      end
      
      SiteConfig.attributes = site_params
      
      if SiteConfig.valid?
        SiteConfig.save
        user_action("Updated Dispatch Options.")
        flash.notice = "Dipsatch Options Updated."
        redirect_to site_dispatch_url and return
      end
    end
  end

  def site_enforcement
    @page_title = "Enforcement Options"
    
    @links.push(['Edit', edit_site_enforcement_path])
  end
  
  def edit_site_enforcement
    @page_title = 'Edit Enforcement Options'
    if request.post?
      if params[:commit] == 'Cancel'
        SiteConfig.reload
        redirect_to site_enforcement_url and return
      end
      
      SiteConfig.attributes = site_params
      
      if SiteConfig.valid?
        SiteConfig.save
        user_action("Updated Enforcement Options.")
        flash.notice = "Enforcement Options Updated."
        redirect_to site_enforcement_url and return
      end
    end
  end
  
  def site_detective
    @page_title = "Detective Options"
    
    @links.push(['Edit', edit_site_detective_path])
  end
  
  def edit_site_detective
    @page_title = 'Edit Detective Options'
    if request.post?
      if params[:commit] == 'Cancel'
        SiteConfig.reload
        redirect_to site_detective_url and return
      end
      
      SiteConfig.attributes = site_params
      
      if SiteConfig.valid?
        SiteConfig.save
        user_action("Updated Detective Options.")
        flash.notice = "Detective Options Updated."
        redirect_to site_detective_url and return
      end
    end
  end
  
  private
  
  def site_image_params
    params.require(:site_image).permit(:logo, :ceo_img, :header_left_img, :header_right_img)
  end
  
  def site_params
    params.require(:site).permit(:agency, :agency_street, :agency_city, :agency_state, :agency_zip, :agency_acronym, :motto, :ceo,
      :ceo_title, :header_title, :header_subtitle1, :header_subtitle2, :header_subtitle3, :footer_text, :colorscheme, :district_values,
      :zone_values, :ward_values, :local_zips, :local_agencies, :show_history, 
      :site_email, :message_log_retention, :activity_retention, :pdf_heading_bg, :pdf_heading_txt, :pdf_header_bg,
      :pdf_header_txt, :pdf_section_bg, :pdf_section_txt, :pdf_em_bg, :pdf_em_txt, :pdf_row_odd, :pdf_row_even, :jurisdiction_title,
      :court_title, :prosecutor_title, :chief_justice_title, :court_clerk_name, :court_clerk_title, :dispatch_shift1_start,
      :dispatch_shift1_stop, :dispatch_shift2_start, :dispatch_shift2_stop, :dispatch_shift3_start, :dispatch_shift3_stop,
      :patrol_shift1_start, :patrol_shift1_stop, :patrol_shift1_supervisor, :patrol_shift2_start, :patrol_shift2_stop,
      :patrol_shift2_supervisor, :patrol_shift3_start, :patrol_shift3_stop, :patrol_shift3_supervisor, :patrol_shift4_start,
      :patrol_shift4_stop, :patrol_shift4_supervisor, :patrol_shift5_start, :patrol_shift5_stop, :patrol_shift5_supervisor,
      :patrol_shift6_start, :patrol_shift6_stop, :patrol_shift6_supervisor, :adult_age, :enable_internal_commissary,
      :enable_cashless_systems, :csi_ftp_user, :csi_ftp_password, :csi_ftp_url, :csi_ftp_filename, :csi_provide_ssn, :csi_provide_dob,
      :csi_provide_race, :csi_provide_sex, :csi_provide_addr, :afis_ori, :afis_agency, :afis_street, :afis_city, :afis_state,
      :afis_zip, :mileage_fee, :noservice_fee, :paper_pay_type1, :paper_pay_type2, :paper_pay_type3, :paper_pay_type4, :paper_pay_type5,
      :probation_fund1, :probation_fund2, :probation_fund3, :probation_fund4, :probation_fund5, :probation_fund6, :probation_fund7,
      :probation_fund8, :probation_fund9, :allow_user_photos, :user_update_contact, :message_retention, :message_policy, :session_life,
      :session_active, :session_max, :posts_per_star
    )
  end
end