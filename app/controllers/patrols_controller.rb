class PatrolsController < ApplicationController
  
  def index
    options = {
      scopes: [:by_reverse_date],
      paginate: true
    }
    @patrols = get_records(options)
    
    if can?(:update_offense)
      @links.push(['New', new_patrol_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @patrol = Patrol.find(params[:id])
    @page_title = "Patrol #{@patrol.id}"
    
    if can?(:update_offense)
      @links.push(['New', new_patrol_path])
      @links.push(['Edit', edit_patrol_path(@patrol)])
    end
    if can?(:destroy_offense)
      @links.push(['Delete', @patrol, {method: :delete, data: {confirm: confirm_msg_for(:patrol)}}])
    end
    
    @page_links.push([@patrol.creator,'creator'],[@patrol.updater,'updater'])
  end

  def new
    can!(:update_offense)

    @patrol = Patrol.new
    set_officer_to_session(@patrol, :officer)
    @page_title = "New Patrol"
  end
  
  def edit
    can!(:update_offense)

    @patrol = Patrol.find(params[:id])
    @page_title = "Edit Patrol #{@patrol.id}"
    @patrol.officer_id = nil
  end
  
  def create
    can!(:update_offense)
    
    redirect_to patrols_url and return if params[:commit] == 'Cancel'
    
    @patrol = Patrol.new(patrol_params)
    @page_title = "New Patrol"
    @patrol.created_by = session[:user]
    @patrol.updated_by = session[:user]
    
    success = @patrol.save
    user_action "Patrol ##{@patrol.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @patrol, notice: 'Patrol was successfully created.' }
        format.json { render :show, status: :created, location: @patrol }
      else
        format.html { render :new }
        format.json { render json: @patrol.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_offense)

    @patrol = Patrol.find(params[:id])

    redirect_to @patrol and return if params[:commit] == 'Cancel'

    @page_title = "Edit Patrol #{@patrol.id}"
    @patrol.updated_by = session[:user]

    success = @patrol.update(patrol_params)
    user_action "Patrol ##{@patrol.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @patrol, notice: 'Patrol was successfully updated.' }
        format.json { render :show, status: :ok, location: @patrol }
      else
        format.html { render :edit }
        format.json { render json: @patrol.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:destroy_offense)

    @patrol = Patrol.find(params[:id])

    @patrol.destroy
    user_action "Patrol ##{@patrol.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to patrols_url, notice: 'Patrol was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def autofill_shift_info
    @supervisor = ''
    @shift_start = ''
    @shift_stop = ''
    @started = ''
    @valid = false
    shift = params[:shift]
    
    if !shift.blank? && SiteConfig.patrol_shifts.include?(shift.to_i)
      @supervisor = SiteConfig.send("patrol_shift#{shift}_supervisor")
      @shift_start = SiteConfig.send("patrol_shift#{shift}_start")
      @shift_stop = SiteConfig.send("patrol_shift#{shift}_stop")
      @started = "#{Date.today.to_s(:us)} #{@shift_start}"
      @valid = true
    end
    
    respond_to :js
  end
  
  def autofill_officer_info
    @officer = ''
    @unit = ''
    @badge = ''
    
    if officer = Contact.find_by_id(params[:oid])
      @officer = officer.sort_name
      @unit = officer.unit
      @badge = officer.badge_no
    end
    
    respond_to :js
  end
  
  def patrol_activity
    @start_date = AppUtils.valid_date(params[:start_date])
    @stop_date = AppUtils.valid_date(params[:stop_date])
    @patrols = Patrol.between(@start_date, @stop_date).all
    
    if @patrols.empty?
      flash.alert = "Nothing to report!"
      redirect_to patrols_url and return
    end
    
    if pdf_url = make_pdf_file("patrol-report", Patrols::ActivityReport.new(@start_date,@stop_date,@patrols))
      redirect_to pdf_url
      user_action("Printed Patrol Activity Report.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to patrols_url
    end
  end
  
  def patrol_summary
    @start_date = AppUtils.valid_date(params[:start_date])
    @stop_date = AppUtils.valid_date(params[:stop_date])
    @patrols = Patrol.between(@start_date, @stop_date).all
    
    if @patrols.empty?
      flash.alert = "Nothing to report!"
      redirect_to patrols_url and return
    end
    
    if pdf_url = make_pdf_file("patrol-report", Patrols::ActivitySummary.new(@start_date,@stop_date,@patrols))
      redirect_to pdf_url
      user_action("Printed Patrol Activity Summary.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to patrols_url
    end
  end
  
  private
  
  def patrol_params
    params.require(:patrol).permit(:officer_id, :officer_badge, :officer_unit, :officer, :shift, :supervisor, :shift_start, :shift_stop,
      :patrol_started_datetime, :patrol_ended_datetime, :beginning_odometer, :ending_odometer, :roads_patrolled, :complaints,
      :traffic_stops, :citations_issued, :papers_served, :narcotics_arrests, :other_arrests, :public_assists, :warrants_served,
      :funeral_escorts, :remarks
    )
  end
end