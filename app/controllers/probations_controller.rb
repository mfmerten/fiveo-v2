class ProbationsController < ApplicationController
  
  def index
    options = {
      active: true,
      scopes: [:by_name],
      includes: [:probation_payments],
      paginate: true
    }
    @probations = get_records(options)
    
    if can?(:update_probation)
      @links.push(['New', new_probation_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end
  
  def show
    @probation = Probation.find(params[:id])
    @page_title = "Probation #{@probation.id}"
    @pending = false
    
    if @probation.docket.nil?
      if Docket.where({docket_no: @probation.docket_no, person_id: @probation.person_id}).first
        @pending = true
      end
    end

    if can?(:update_probation)
      @links.push(["New", new_probation_path])
      @links.push(["Edit", edit_probation_path(@probation)])
    end
    if can?(:destroy_probation)
      @links.push(['Delete', @probation, {method: :delete, data: {confirm: confirm_msg_for(:probation)}}])
    end
    if can?(:update_probation)
      @links.push(['Transaction', new_probation_probation_payment_path(@probation)])
    end
    
    @page_links.push(@probation.person, @probation.docket, [@probation.court.docket, @probation.court], [@probation.sentence.docket, @probation.sentence.court, @probation.sentence], @probation.hold,[@probation.creator,'creator'],[@probation.updater, 'updater'])
  end
  
  def new
    can!(:update_probation)
    
    @page_title = "New Probation"
    @probation = Probation.new
    @probation.probation_officer = session[:user_name]
    @probation.probation_officer_unit = session[:user_unit]
    @probation.probation_officer_badge = session[:user_badge]
    @probation.status = "Active"
    @probation.start_date = Date.today
    @charge = nil
  end

  def edit
    can!(:update_probation)
    
    @probation = Probation.find(params[:id])
    @page_title = "Edit Probation #{@probation.id}"
    @probation.probation_officer_id = nil
    @charge = @probation.da_charge
  end
  
  def create
    redirect_to probations_url and return if params[:commit] == "Cancel"
    
    can!(:update_probation)
    
    @probation = Probation.new(probation_params)
    @probation.created_by = session[:user]
    @probation.updated_by = session[:user]
    
    success = @probation.save
    user_action "Probation ##{@probation.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @probation, notice: 'Probation was successfully created.' }
        format.json { render :show, status: :created, location: @probation }
      else
        format.html { render :new }
        format.json { render json: @probation.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @probation = Probation.find(params[:id])
    
    redirect_to @probation and return if params[:commit] == "Cancel"
    
    can!(:update_probation)
    
    @probation.updated_by = session[:user]
    
    success = @probation.update(probation_params)
    user_action "Probation ##{@probation.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @probation, notice: 'Probation was successfully updated.' }
        format.json { render :show, status: :ok, location: @probation }
      else
        format.html { render :edit }
        format.json { render json: @probation.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_probation)
    
    @probation = Probation.find(params[:id])
    
    @probation.destroy
    user_action "Probation ##{@probation.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to probations_url, notice: 'Probation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def probation_balance
    @probations = Probation.includes(:person).order('people.family_name, people.given_name, probations.id').all.reject{|p| p.total_balance == 0}
    
    if @probations.empty?
      flash.alert = 'Nothing to Report!'
      redirect_to probations_url and return
    end
    
    if pdf_url = make_pdf_file("balance", Probations::Balance.new(@person.id))
      redirect_to pdf_url
      user_action("Printed Balance Due Report.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to probations_url
    end
  end

  def posting_report
    @status = params[:status]
    if @status == 'post'
      can!(:update_probation)
    end
    unless ['post','preview','reprint'].include?(@status)
      @status = 'preview'
    end
    if @status == 'reprint'
      unless report_datetime = AppUtils.valid_datetime(params[:post_datetime])
        flash.alert = "Invalid posting date specified!"
        redirect_to probations_url and return
      end
      
      @payments = ProbationPayment.posted_at(report_datetime)
      @report_time = report_datetime
    else
      @payments = ProbationPayment.unposted
      @report_time = Time.now
    end
    
    if @payments.empty?
      flash.alert = "No Unposted Payments!"
      redirect_to probations_url and return
    end
    
    if pdf_url = make_pdf_file("posting-report", Probations::PostingReport.new(@status, @report_time, @payments))
      redirect_to pdf_url
      user_action("Printed Posting Report (#{@status})")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to probations_url
    end
  end

  def transaction_history
    @start_date = AppUtils.valid_date(params[:start_date])
    @stop_date = AppUtils.valid_date(params[:end_date])
    if !@start_date.nil? && !@stop_date.nil? && @start_date > @stop_date
      @start_date, @stop_date = @stop_date, @start_date
    end
    @probation_payments = ProbationPayment.by_name
    if @start_date && @stop_date
      @probation_payments = @probation_payments.between(@start_date, @stop_date)
    elsif @start_date
      @probation_payments = @probation_payments.on_or_after(@start_date)
    elsif @stop_date
      @probation_payments = @probation_payments.on_or_before(@start_date)
    end
    if params[:probation_id] && (@probation = Probation.find_by_id(params[:probation_id]))
      @person = nil
      @probation_payments = @probation_payments.where('probation_id = ?',@probation.id)
    elsif params[:person_id] && (@person = Person.find_by_id(params[:person_id]))
      @probation = nil
      @probation_payments = ProbationPayment.where('person_id = ?',@person.id)
    end
    
    if @probation_payments.empty?
      flash.alert = 'Nothing to Report!'
      redirect_to probations_url and return
    end
    
    if pdf_url = make_pdf_file("posting-report", Probations::History.new(@start_date, @stop_date, @probation, @person, @probation_payments))
      redirect_to pdf_url
      user_action("Printed Transaction History")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to probations_url
    end
  end

  def active_list
    @probations = Probation.by_name.current
    
    if @probations.empty?
      flash.alert = 'Nothing to Report!'
      redirect_to probations_url and return
    end
    
    if pdf_url = make_pdf_file("posting-report", Probations::ActiveList.new(@probations))
      redirect_to pdf_url
      user_action("Printed Active Probation Listing")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to probations_url
    end
  end

  private
  
  def probation_params
    params.require(:probation).permit(:person_id, :docket_no, :probation_officer_id, :probation_officer_badge, :probation_officer_unit,
      :probation_officer, :da_charge_id, :trial_result, :conviction_count, :conviction, :doc, :fines, :costs, :pay_by_date,
      :pay_during_prob, :default_hours, :default_days, :default_months, :default_years, :sentence_hours, :sentence_days, :sentence_months,
      :sentence_years, :sentence_life, :sentence_death, :parish_jail, :sentence_suspended, :suspended_except_hours, :suspended_except_days,
      :suspended_except_months, :suspended_except_years, :suspended_except_life, :probation_hours, :probation_days, :probation_months,
      :probation_years, :reverts_to_unsup, :reverts_conditions, :credit_served, :service_days, :service_hours, :service_served, :sap,
      :sap_complete, :driver, :driver_complete, :probation_fees, :anger, :anger_complete, :sat, :sat_complete, :random_screens,
      :no_victim_contact, :restitution_amount, :restitution_date, :dare_amount, :idf_amount, :art893, :art894, :art895, :sentence_notes,
      :hold_if_arrested, :status, :terminate_reason, :start_date, :end_date, :revoked_date, :revoked_for, :completed_date, :notes, :remarks
    )
  end
end
