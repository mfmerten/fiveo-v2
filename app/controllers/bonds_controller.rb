class BondsController < ApplicationController

  def index
    options = {
      scopes: [:by_reverse_date],
      includes: [:person, {arrest: :charges}, :surety, :holds],
      paginate: true,
      active: true
    }
    @bonds = get_records(options)
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @bond = Bond.find(params[:id])
    @page_title = "Bond #{@bond.id}"
    
    if can?(:update_bond)
      @links.push(["Edit", edit_bond_path(@bond)])
    end
    if can?(:destroy_bond)
      @links.push(['Delete', @bond, {method: 'delete', data: {confirm: confirm_msg_for(:bond)}}])
    end
    @links.push(["Bond", bond_form_bond_path(id: @bond.id), pdf: true])
    if can?(:update_court) && !@bond.status? && !@bond.final?
      if session[:letters].nil? || session[:letters].empty? || !session[:letters].include?(@bond.id)
        @links.push(["Queue Letter", add_to_queue_bond_path(id: @bond.id)])
      end
      if !session[:letters].nil? && session[:letters].include?(@bond.id)
        @links.push(["Unqueue Letter", remove_from_queue_bond_path(id: @bond.id)])
      end
    end

    if can?(:update_bond)
      if @bond.check_recallable("surrender").nil?
        @linksx.push(["Surrender", recall_bond_bond_path(id: @bond.id, status: 'surrender'), {method: :post, data: {confirm: 'Really Surrender This Bond?'}}])
      end
      if @bond.check_recallable("revoke").nil?
        @linksx.push(["Revoke", recall_bond_bond_path(id: @bond.id, status: 'revoke'), {method: :post, data: {confirm: 'Really Revoke This Bond?'}}])
      end
      if @bond.check_recallable("forfeit").nil?
        @linksx.push(["Forfeit", recall_bond_bond_path(id: @bond.id, status: 'forfeit'), {method: :post, data: {confirm: 'Really Forfeit This Bond?'}}])
      end
      if @bond.check_undo_recallable.nil?
        @linksx.push(["Reactivate", undo_recall_bond_bond_path(id: @bond.id), {method: :post, data: {confirm: 'Really Reactivate This Bond?'}}])
      end
      if can?(:update_court)
        if @bond.final?
          @linksx.push(["Unfinalize", unfinalize_bond_bond_path(id: @bond.id), {method: :post, data: {confirm: 'Really UnFinalize This Bond?'}}])
        else
          @linksx.push(["Finalize", finalize_bond_bond_path(id: @bond.id), {method: :post, data: {confirm: 'Really Finalize This Bond?'}}])
        end
      end
    end

    @page_links.push(@bond.person, [@bond.surety,'Surety'], @bond.arrest)
    unless @bond.holds.empty?
      hold = @bond.holds.last
      unless hold.nil?
        @page_links.push([hold.booking.jail, hold.booking, hold], [hold.booking.jail, hold.booking])
      end
    end
    @page_links.push([@bond.creator,'creator'],[@bond.updater, 'updater'])
  end

  def new
    can!(:update_bond)

    @hold = Hold.find(params[:hold_id])

    if @hold.cleared_datetime?
      flash.alert = "Cannot add a bond to a cleared hold!"
      redirect_to @hold and return
    end

    @page_title = "New Bond"
    @bond = Bond.new(issued_datetime: Time.now, arrest_id: @hold.arrest_id, bond_amt: @hold.fines, person_id: @hold.booking.person_id)
    @bond.holds << @hold
    if @hold.hold_type == 'Hold for City' || @hold.hold_type == 'Hold for Other Agency'
      @bond.status = "For Other Agency"
      @bond.status_datetime = Time.now
      @bond.final = true
    else
      @bond.final = false
    end
    @companies = []
  end
    
  def edit
    can!(:update_bond)
    
    @bond = Bond.find(params[:id])
    
    if @bond.holds.empty?
      flash.alert = "Cannot edit this Bond: no longer attached to a Hold!"
      redirect_to @bond and return
    end
    
    @hold = @bond.holds.last
    @page_title = "Edit Bond #{@bond.id}"
    @companies = []
  end
  
  def create
    can!(:update_bond)
    
    redirect_to bonds_url and return if params[:commit] == 'Cancel'
    
    @hold = Hold.find(params[:hold_id])
    @bond = @hold.bonds.build(bond_params)
    @page_title = "Add Bond"
    @bondsman = Contact.find_by_id(@bond.bondsman_id)
    if @bondsman.nil?
      @companies = []
    else
      @companies = @bondsman.bond_companies
    end
    @bond.created_by = session[:user]
    @bond.updated_by = session[:user]

    success = @bond.save
    user_action("Bond ##{@bond.id} Created.") if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @bond, notice: 'Bond was successfully created.' }
        format.json { render :show, status: :created, location: @bond }
      else
        format.html { render :new }
        format.json { render json: @bond.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_bond)
    
    @bond = Bond.find(params[:id])
    
    redirect_to @bond and return if params[:commit] == 'Cancel'
    
    if @bond.holds.empty?
      flash.alert = "Cannot update this Bond: no longer attached to a Hold!"
      redirect_to @bond and return
    end
    
    @page_title = "Edit Bond #{@bond.id}"
    @bondsman = Contact.find_by_id(@bond.bondsman_id)
    if @bondsman.nil?
      @companies = []
    else
      @companies = @bondsman.bond_companies
    end
    @bond.updated_by = session[:user]
    
    success = @bond.update(bond_params)
    user_action("Bond ##{@bond.id} Updated.") if success

    respond_to do |format|
      if success
        format.html { redirect_to @bond, notice: 'Bond was successfully updated.' }
        format.json { render :show, status: :ok, location: @bond }
      else
        format.html { render :edit }
        format.json { render json: @bond.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_bond)
    
    @bond = Bond.find(params[:id])
    
    @bond.destroy
    user_action("Bond ##{@bond.id} Deleted.")
    
    respond_to do |format|
      format.html { redirect_to bonds_url, notice: 'Bond was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def undo_recall_bond
    can!(:update_bond)
    
    @bond = Bond.find(params[:id])
    
    if reason = @bond.check_undo_recallable
      flash.alert = "Cannot Undo: #{reason}"
      redirect_to @bond and return
    end
    
    oldstatus = @bond.status
    
    if reason = @bond.undo_recall(session[:user])
      flash.alert = "Undo Failed: #{reason}"
      redirect_to @bond and return
    end
    
    user_action("Bond ##{@bond.id} Reactivated")
    flash.notice = "Bond ##{@bond.id} Reactivated"
    
    redirect_to @bond
  end
  
  def recall_bond
    can!(:update_bond)
    
    @bond = Bond.find(params[:id])
    status = params[:status]
    
    if reason = @bond.check_recallable(status)
      flash.alert = "Cannot #{status.titleize}: #{reason}"
      redirect_to @bond and return
    end
    
    if result = @bond.recall(status, session[:user])
      flash.alert = "Bond #{status} Failed: #{result}!"
      redirect_to @bond and return
    end
    
    user_action("Bond ##{@bond.id} Recalled")
    flash.notice = "Bond ##{@bond.id} Recalled"

    redirect_to @bond
  end

  def finalize_bond
    can!(:update_bond)
    
    @bond = Bond.find(params[:id])
    
    if params[:docket_id]
      unless @docket = Docket.find_by_id(params[:docket_id])
        flash.alert = "Specified Docket is Invalid!"
        redirect_to @bond and return
      end
    end
    
    if @bond.final?
      flash.alert = "Bond is already marked Final!"
      redirect_to @bond and return
    end
    
    if @bond.person.nil?
      flash.alert = "Bonded Person is Not Valid!"
      redirect_to @bond and return
    end
    
    @bond.update_attribute(:updated_by, session[:user])
    @bond.update_attribute(:final, true)
    
    user_action("Bond ##{@bond.id} Finalized")
    flash.notice = "Bond ##{@bond.id} Finalized"
    
    if @docket.nil?
      redirect_to @bond
    else
      redirect_to @docket
    end
  end
  
  def unfinalize_bond
    can!(:update_bond)
    
    @bond = Bond.find(params[:id])
    
    if params[:docket_id]
      unless @docket = Docket.find_by_id(params[:docket_id])
        flash.alert = "Specified Docket is Invalid!"
        redirect_to @bond and return
      end
    end
    
    unless @bond.final?
      flash.alert = "Bond is not marked Final!"
      redirect_to @bond and return
    end
    
    if @bond.person.nil?
      flash.alert = "Bonded Person is Not Valid!"
      redirect_to @bond and return
    end
    
    @bond.updated_by = session[:user]
    @bond.final = false
    @bond.save(validate: false)
    
    user_action("Bond ##{@bond.id} Unfinalized")
    flash.notice = "Bond ##{@bond.id} Unfinalized"
    
    if @docket.nil?
      redirect_to @bond
    else
      redirect_to @docket
    end
  end
  
  def bond_form
    @bond = Bond.find(params[:id])
    
    if pdf_url = make_pdf_file("bond", Bonds::Form.new(@bond))
      redirect_to pdf_url
      user_action("Printed Bond #{params[:id]}.")
    else
      flash.alert = "Could not create temporary PDF file for download!"
      redirect_to @bond
    end
  end

  def add_to_queue
    @bond = Bond.find(params[:id])
    
    if @bond.status? || @bond.final?
      flash.alert = 'Bond not active, Cannot Queue!'
      redirect_to @bond and return
    end
    
    if !session[:letters].nil? && session[:letters].include?(@bond.id)
      flash.alert = 'Bond already in letter queue!'
      redirect_to @bond and return
    end
    
    if session[:letters].nil?
      session[:letters] = [@bond.id]
    else
      session[:letters].push(@bond.id)
    end
    
    user_action("Bond ##{@bond.id} Queued")
    flash.notice = "Bond ##{@bond.id} Queued"

    redirect_to @bond
  end

  def remove_from_queue
    @bond = Bond.find(params[:id])
    
    if session[:letters].nil? || !session[:letters].include?(@bond.id)
      flash.alert = 'Bond is not queued for printing!'
      redirect_to @bond and return
    end
    
    session[:letters].delete(@bond.id)
    
    if session[:letters].empty?
      session[:letters] = nil
    end
    
    user_action("Bond ##{@bond.id} Unqueued")
    flash.notice = "Bond ##{@bond.id} Unqueued"

    redirect_to @bond
  end
  
  def active_list
    @bonds = Bond.active.by_name.all
    
    if @bonds.empty?
      flash.alert = 'Nothing to Report!'
      redirect_to bonds_url and return
    end
    
    if pdf_url = make_pdf_file("active_bonds", Bonds::ActiveList.new(@bonds))
      redirect_to pdf_url
      user_action("Printed Active Bond List.")
    else
      flash.alert = 'Could not create PDF file for download!'
      redirect_to bonds_url
    end
  end
  
  def list_form
    options = {
      scopes: [:by_reverse_date],
      includes: [:person, {arrest: :charges}, :bondsman, :surety]
    }
    @bonds = get_records(options)

    if @bonds.empty?
      flash.alert = "No Bonds selected for report!"
      redirect_to bonds_url and return
    end
    
    if pdf_url = make_pdf_file("bond_list", Bonds::ListForm.new(@bonds, cond))
      redirect_to pdf_url
      user_action("Printed Bond List.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to bonds_url
    end
  end
  
  # for js partial to update bond companies when bondsman changes
  def update_companies
    if bondsman = Contact.find_by_id(params[:b])
      @companies = bondsman.bond_companies
    else
      @companies = []
    end
  end
  
  def list_unfinalized
    @unfinalized = Bond.satisfied_but_unfinalized
    @page_title = "Unfinalized Bonds"
    
    @links.push(['PDF', unfinalized_pdf_bonds_path])
  end
  
  def unfinalized_pdf
    @unfinalized = Bond.satisfied_but_unfinalized
    
    if pdf_url = make_pdf_file("unfinalized_list", Bonds::UnfinalizedList.new(@unfinalized))
      redirect_to pdf_url
      user_action("Printed Unfinalized List.")
    else
      flash.alert = 'Could not create PDF file for download!'
      redirect_to list_unfinalized_bonds_url
    end
  end
  
  private
  
  def bond_params
    params.require(:bond).permit(:bond_no, :bond_type, :bondsman_id, :bonding_company_id, :surety_id, :issued_datetime, :power,
      :bond_amt, :status, :status_datetime, :final, :person_id, :arrest_id, :remarks)
  end
end
