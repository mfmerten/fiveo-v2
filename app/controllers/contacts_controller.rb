class ContactsController < ApplicationController

  def index
    options = {
      scopes: [:by_name],
      includes: [:agency, :user],
      paginate: true
    }
    @contacts = get_records(options)

    if can?(:update_contact)
      @links.push(['New', new_contact_path])
    end

    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    if params[:info_id]
      @info_id = params[:info_id]
      @contact = Contact.find_by_id(params[:id])
    else
      @contact = Contact.find(params[:id])
      @page_title = "Contact #{@contact.id}"
      
      if can?(:update_contact)
        @links.push(["New", new_contact_path])
      end
      if (can?(:update_contact) && @contact.user.nil?) || (!@contact.user.nil? && (can?(:update_user) || (@contact.user.id == session[:user] && SiteConfig.user_update_contact)))
        @links.push(['Edit', edit_contact_path(@contact)])
      end
      if can?(:destroy_contact) && @contact.user.nil?
        @links.push(['Delete', @contact, {method: 'delete', data: {confirm: confirm_msg_for(:contact)}}])
      end

      @page_links.push([@contact.agency,'Agency'])
    end
    respond_to :html, :json, :js
  end

  def new
    can!(:update_contact)

    @contact = Contact.new(state: SiteConfig.agency_state)
    @page_title = "New Contact"
    @contact.bond_companies.build
  end

  def edit
    @contact = Contact.find(params[:id])
    if @contact.user.nil?
      can!(:update_contact)
    else
      unless can?(:update_user) || (@contact.user.id == session[:user] && SiteConfig.user_update_contact)
        raise PermissionDeniedError.new("Contacts#edit: Only someone with Update User permission #{SiteConfig.user_update_contact ? '(or the profile owner) ' : ''}may update this profile!")
      end
    end
    @page_title = "Edit Contact #{@contact.id}"
  end

  def merge
    # merge is destructive, hence the destroy permission requirements
    can!(:destroy_contact)

    @old_contact = Contact.find(params[:old_id])
    @contact = Contact.find(params[:id])

    if @old_contact.business?
      # agency contact
      unless @contact.business?
        flash.alert = "Cannot merge business contact into person contact."
        redirect_to @old_contact and return
      end
    else
      # individual contact
      if @contact.business?
        flash.alert = "Cannot merge person contact into business contact."
        redirect_to @old_contact and return
      end
    end
    
    unless @old_contact.user.nil? && @contact.user.nil?
      flash.alert = "Cannot merge User profiles!"
      redirect_to @old_contact and return
    end
    
    @page_title = "Merge Contacts (#{@old_contact.sort_name} &rarr; #{@contact.sort_name})"
    @contact.title = @old_contact.title unless @contact.title?
    @contact.unit = @old_contact.unit unless @contact.unit?
    @contact.agency_id = @old_contact.agency_id unless @contact.agency.nil?
    @contact.street = @old_contact.street unless @contact.street?
    @contact.city = @old_contact.city unless @contact.city?
    @contact.state = @old_contact.state unless @contact.state?
    @contact.zip = @old_contact.zip unless @contact.zip?
    @contact.notes = @old_contact.notes unless @contact.notes?
    if @contact.phone1? || @contact.phone2? || @contact.phone3?
      # don't want to overwrite any possibly valid phone numbers
      # so save merged contact numbers in notes.
      if @contact.notes?
        @contact.notes << " - #{@old_contact.phones}"
      else
        @contact.notes = @old_contact.phones
      end
    else
      @contact.phone1 = @old_contact.phone1
      @contact.phone1_type = @old_contact.phone1_type
      @contact.phone1_unlisted = @old_contact.phone1_unlisted
      @contact.phone2 = @old_contact.phone2
      @contact.phone2_type = @old_contact.phone2_type
      @contact.phone2_unlisted = @old_contact.phone2_unlisted
      @contact.phone3 = @old_contact.phone3
      @contact.phone3_type = @old_contact.phone3_type
      @contact.phone3_unlisted = @old_contact.phone3_unlisted
    end
    @contact.pri_email = @old_contact.pri_email unless @contact.pri_email?
    @contact.alt_email = @old_contact.alt_email unless @contact.alt_email?
    @contact.web_site = @old_contact.web_site unless @contact.web_site?
    @contact.officer = true if @old_contact.officer?
    @contact.street2 = @old_contact.street2 unless @contact.street2?
    @contact.badge_no = @old_contact.badge_no unless @contact.badge_no?
    @contact.active = true if @old_contact.active?
    @contact.detective = true if @old_contact.detective?
    @contact.bondsman = true if @old_contact.bondsman?
    @contact.bonding_company = true if @old_contact.bonding_company?
    @contact.pawn_co = true if @old_contact.pawn_co?
    @contact.physician = true if @old_contact.physician?
    @contact.pharmacy = true if @old_contact.pharmacy?
    @contact.printable = true if @old_contact.printable?
  end
  
  def create
    can!(:update_contact)
    
    redirect_to contacts_path and return if params[:commit] == 'Cancel'
    
    @contact = Contact.new(contact_params)
    @page_title = "New Contact"
    @contact.created_by = session[:user]
    @contact.updated_by = session[:user]
    
    success = false
    if @contact.save
      success = true
      user_action "Contact ##{@contact.id} Created."
      if params[:investigation_id] && investigation = Investigation.find_by_id(params[:investigation_id])
        investigation.invest_contacts.create(contact_id: @contact.id)
      end
    end

    respond_to do |format|
      if success
        format.html { redirect_to @contact, notice: 'Contact was successfully created.' }
        format.json { render :show, status: :created, location: @contact }
      else
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @contact = Contact.find(params[:id])
    
    redirect_to @contact and return if params[:commit] == 'Cancel'
    
    if @contact.user.nil?
      can!(:update_contact)
    else
      unless can?(:update_user) || (@contact.user.id == session[:user] && SiteConfig.user_update_contact)
        raise PermissionDeniedError.new("Contacts#update: Only someone with Update User permission #{SiteConfig.user_update_contact ? '(or the profile owner) ' : ''}may update this profile!")
      end
    end
    
    if params[:old_id]
      # actually doing a merge
      @old_contact = Contact.find(params[:old_id])
      if @old_contact.business?
        # agency contact
        unless @contact.business?
          flash.alert = "Cannot merge business contact into person contact."
          redirect_to @old_contact and return
        end
      else
        # individual contact
        if @contact.business?
          flash.alert = "Cannot merge person contact into business contact."
          redirect_to @old_contact and return
        end
      end
      
      unless @old_contact.user.nil? && @contact.user.nil?
        flash.alert = "Cannot merge User profiles!"
        redirect_to @old_contact and return
      end
    end
    
    @page_title = "Edit Contact #{@contact.id}"
    @contact.updated_by = session[:user]
    
    success = false
    if @contact.update(contact_params)
      success = true
      if params[:old_id]
        # merge processing
        if @old_contact.reasons_not_destroyable.nil?
          @old_contact.destroy
          user_action("Merged Contact ID: #{@old_contact.id} into Contact ID: #{@contact.id}.")
          flash.notice = "Merge Succeeded: old Contact deleted!"
          redirect_to @contact and return
        else
          user_action("Merged Contact ID: #{@old_contact.id} into Contact ID: #{@contact.id}, but delete of old contact failed!")
          flash.notice = "Merge Succeeded"
          flash.alert = "Delete of old Contact failed!"
          redirect_to @old_contact and return
        end
      else
        user_action "Contact ##{@contact.id} Updated."
      end
    end
    
    respond_to do |format|
      if success
        format.html { redirect_to @contact, notice: 'Contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact }
      else
        format.html { render :edit }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:destroy_contact)
    
    @contact = Contact.find(params[:id])
    
    if reasons = @contact.reasons_not_destroyable
      flash.alert = "Cannot delete: #{reasons}"
      redirect_to @contact and return
    end
    
    @contact.destroy
    user_action "Contact ##{@contact.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to contacts_url, notice: 'Contact was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def employee_list
    @contact = Contact.find(params[:id])
    
    unless @contact.business?
      flash.alert = "Contact must be a business, not a person."
      redirect_to contacts_url and return
    end
    
    @contacts = Contact.by_unit.is_active.is_printable.for_agency(@contact.id).all
    
    if @contacts.empty?
      flash.alert = "Nothing to report!"
      redirect_to contacts_url and return
    end
    
    if pdf_url = make_pdf_file("employee_list", Contacts::EmployeeList.new(@contact, @contacts))
      redirect_to pdf_url
      user_action("Printed Employee List for #{params[:id]}.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to contacts_url
    end
  end
  
  def contact_list
    # Note: using index method of getting records because this report is filterable
    options = {
      scopes: [:by_name],
      includes: [:agency]
    }
    @contacts = get_records(options)

    if @contacts.empty?
      flash.alert = "Nothing to report!"
      redirect_to contacts_url and return
    end
    
    if pdf_url = make_pdf_file("contact_list", Contacts::List.new(@contacts.to_a, cond, params[:title]))
      redirect_to pdf_url
      user_action("Printed Contact List.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to contacts_url
    end
  end
  
  private
  
  def contact_params
    params.require(:contact).permit(:created_by, :updated_by, :legacy, :sort_name, :business, :street, :street2, :city, :state, :zip,
      :title, :notes, :active, :officer, :detective, :judge, :bondsman, :bonding_company, :pawn_co, :physician, :pharmacy, :printable,
      :agency_id, :unit, :badge_no, :phone1_type, :phone1, :phone1_unlisted, :phone2_type, :phone2, :phone2_unlisted, :phone3_type,
      :phone3, :phone3_unlisted, :pri_email, :alt_email, :web_site, :remarks
    )
  end
end
