class CallsController < ApplicationController

  def index
    options = {
      scopes: [:by_reverse_id],
      includes: [:call_units, :call_subjects],
      paginate: true
    }
    @calls = get_records(options)
    
    if can?(:update_call)
      @links.push(['New', new_call_path])
      if can?(:update_option)
        @links.push(['Signal Codes', signal_codes_path])
      end
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    if params[:info_id]
      @info_id = params[:info_id]
      @call = Call.find_by_id(params[:id])
    else
      @call = Call.find(params[:id])
      @page_title = "Call #{@call.id}"
      
      if can?(:update_call)
        @links.push(["New", new_call_path])
        @links.push(["Edit", edit_call_path(@call)])
      end
      if can?(:destroy_call)
        @links.push(['Delete', @call, {method: :delete, data: {confirm: confirm_msg_for(:call_sheet)}}])
        @links.push(['New Case No.', new_case_no_call_path(@call.id), {method: :post, data: {confirm: "Are you sure you want to reassign this call sheet with the next available case number?"}}])
      end
      @links.push(['Call Sheet', call_sheet_call_path(@call.id), pdf: true])
      if can?(:update_offense)
        @links.push(['New Offense', new_offense_path(call_id: @call.id)])
      end
      if can?(:update_forbid)
        @links.push(['New Forbid', new_forbid_path(call_id: @call.id)])
      end
      
      @page_links.push([@call.creator,'creator'],[@call.updater,'updater'])
    end
    
    respond_to :html, :json, :js
  end

  def new
    can!(:udpate_call)
    
    @page_title = "New Call"
    @call = Call.new(call_datetime: Time.now, followup_no: "")
    set_officer_to_session(:dispatcher)
    set_officer_to_session(:received_by)
    @call.call_units.build
    @call.call_subjects.build
  end
  
  def edit
    can!(:update_call)
    
    @call = Call.find(params[:id])
    @page_title = "Edit Call " + @call.id.to_s
    @call.received_by_id = nil
    @call.dispatcher_id = nil
    @call.detective_id = nil
  end
  
  def create
    can!(:update_call)
    
    @call = Call.new(call_params)
    @page_title = "New Call"
    @call.created_by = session[:user]
    @call.updated_by = session[:user]
    
    success = @call.save
    user_action "Call ##{@call.id} Created." if success
        
    respond_to do |format|
      if success
        format.html { redirect_to @call, notice: 'Call was successfully created.' }
        format.json { render :show, status: :created, location: @call }
      else
        format.html { render :new }
        format.json { render json: @call.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_call)
    
    @call = Call.find(params[:id])
    
    redirect_to @call and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Call #{@call.id}"
    @call.updated_by = session[:user]
    @call.legacy = false
    
    success = @call.update(call_params)
    user_action "Call ##{@call.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @call, notice: 'Call was successfully updated.' }
        format.json { render :show, status: :ok, location: @call }
      else
        format.html { render :edit }
        format.json { render json: @call.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:destroy_call)
    
    @call = Call.find(params[:id])
    
    if reasons = @call.reasons_not_destroyable
      flash.alert = "Cannot delete: #{reasons}"
      redirect_to @call and return
    end
    
    @call.destroy
    user_action "Call ##{@call.id} Deleted."

    respond_to do |format|
      format.html { redirect_to calls_url, notice: 'Call was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def new_case_no
    can!(:destroy_call)
    
    @call = Call.find(params[:id])
    
    unless @call.valid?
      flash.alert = "Call Sheet is not valid. Please edit it and fix problems before reassinging the case number!"
      redirect_to @call and return
    end
    
    # blank the case no and save it - this will generate a new number
    @call.case_no = ""
    @call.updated_by = session[:user]
    @call.save
    
    user_action "Reassigned new case number to Call ##{@call.id}."
    
    redirect_to @call
  end
  
  def call_sheet
    @call = Call.find(params[:id])
    
    if pdf_url = make_pdf_file("call", Calls::Sheet.new(@call))
      redirect_to pdf_url
      user_action("Printed Call Sheet for Call #{params[:id]}.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to @call
    end
  end

  def shift_report
    unless @date = AppUtils.valid_date(params[:date])
      @date = Date.yesterday
    end
    @shift = params[:shift].to_i
    
    unless SiteConfig.dispatch_shifts.include?(@shift)
      flash.alert = "Shift #{@shift} is not defined in Site Configuration!"
      redirect_to calls_url and return
    end
    
    @calls = Call.for_shift(@shift, @date).all
    
    if @calls.empty?
      flash.alert = "Nothing to report!"
      redirect_to calls_url and return
    end
    
    if pdf_url = make_pdf_file("shift_report", Calls::ShiftReport.new(@date,@shift,@calls))
      redirect_to pdf_url
      user_action("Printed Shift Report for Shift #{params[:shift]}.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to @call
    end
  end
  
  private
  
  def call_params
    params.require(:call).permit(:case_no, :call_datetime, :followup_no, :received_via, :received_by_id, :received_by_badge,
      :received_by_unit, :received_by, :ward, :district, :zone, :signal_code, :signal_name, :details, :dispatch_datetime,
      :arrival_datetime, :concluded_datetime, :disposition, :detective_id, :detective_badge, :detective_unit, :detective,
      :dispatcher_id, :dispatcher_badge, :dispatcher_unit, :dispatcher, :remarks,
      call_subjects_attributes: [:id, :subject_type, :_destroy, :name, :address1, :address2, :home_phone, :cell_phone, :work_phone],
      call_units_attributes: [:id, :officer_id, :officer_badge, :officer_unit, :officer, :_destroy])
  end
end
