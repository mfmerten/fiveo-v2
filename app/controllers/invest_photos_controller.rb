class InvestPhotosController < ApplicationController
  before_action :load_investigation, except: [:index, :search]
  
  def index
    options = {
      scopes: [:by_reverse_id],
      paginate: true
    }
    @invest_photos = get_records(options)
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @invest_photo = @investigation.photos.find(params[:id])
    @page_title = "Investigation Photo #{@invest_photo.id}"
    
    enforce_privacy(@invest_photo)
    
    if can?(:update_investigation) && check_author(@invest_photo)
      @links.push(['New', new_investigation_invest_photo_path(@investigation)])
      @links.push(['Edit', edit_investigation_invest_photo_path(@investigation, @invest_photo)])
      @links.push(['Delete', [@investigation, @invest_photo], {method: 'delete', data: {confirm: confirm_msg_for(:invest_photo)}}])
      @links.push(['Photo', photo_investigation_invest_photo_path(@investigation, @invest_photo)])
    end

    @page_links.push([@investigation,'invest'],[@invest_photo.taken_by_id,'taken by'],[@invest_photo.creator,'creator'],[@invest_photo.updater,'updater'])
  end

  def new
    can!(:update_investigation)

    @invest_photo = @investigation.photos.build(taken_date: Date.today)
    set_officer_to_session(@invest_photo, :taken_by)
    @page_title = "New Investigation Photo"
  end
    
  def edit
    can!(:update_investigation)

    @invest_photo = @investigation.photos.find(params[:id])

    enforce_privacy(@invest_photo)
    enforce_author(@invest_photo)

    @page_title = "Edit Investigation Photo #{@invest_photo.id}"
  end
  
  def create
    redirect_to @investigation and return if params[:commit] == 'Cancel'

    can!(:update_investigation)

    @invest_photo = @investigation.photos.build(invest_photo_params)
    @page_title = "New Investigation Photo"
    @invest_photo.private = @investigation.private?
    @invest_photo.owned_by = @investigation.owned_by
    @invest_photo.created_by = session[:user]
    @invest_photo.updated_by = session[:user]

    success = @invest_photo.save
    user_action "Invest Photo ##{@invest_photo.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@investigation, @invest_photo], notice: 'Invest Photo was successfully created.' }
        format.json { render :show, status: :created, location: [@investigation, @invest_photo] }
      else
        format.html { render :new }
        format.json { render json: @invest_photo.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_investigation)
    
    @invest_photo = @investigation.photos.find(params[:id])
    
    redirect_to [@investigation, @invest_photo] and return if params[:commit] == 'Cancel'
    
    enforce_privacy(@invest_photo)
    enforce_author(@invest_photo)
    
    @page_title = "Edit Investigation Photo #{@invest_photo.id}"
    @invest_photo.updated_by = session[:user]
    
    if params[:invest_photo][:photo_form] == 0
      params[:invest_photo][:linked_crimes] ||= []
      params[:invest_photo][:linked_criminals] ||= []
      params[:invest_photo][:linked_vehicles] ||= []
    end
    
    # remove photo if requested
    if params[:commit] == 'Delete'
      @invest_photo.invest_photo = nil
    end
    
    success = false
    if @invest_photo.update(invest_photo_params)
      success = true
      if params[:commit] == 'Delete'
        user_action("Removed Photo for Investigation Photo #{@invest_photo.id}.")
      else
        user_action "Invest Photo ##{@invest_photo.id} Updated." if success
      end
    end
    
    respond_to do |format|
      if success
        format.html { redirect_to [@investigation, @invest_photo], notice: 'Invest Photo was successfully updated.' }
        format.json { render :show, status: :ok, location: [@investigation, @invest_photo] }
      else
        if params[:invest_photo][:photo_form] == '1'
          format.html { render :investigation_photo }
        else
          format.html { render :edit }
        end
        format.json { render json: @invest_photo.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:update_investigation)
    
    @invest_photo = @investigation.photos.find(params[:id])
    
    enforce_privacy(@invest_photo)
    enforce_author(@invest_photo)
    
    @invest_photo.destroy
    user_action "Invest Photo ##{@invest_photo.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to @investigation, notice: 'Invest Photo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def investigation_photo
    can!(:update_investigation)
    
    @invest_photo = InvestPhoto.find(params[:id])
    
    enforce_privacy(@invest_photo)
    enforce_author(@invest_photo)
    
    @page_title = "Investigation Photo #{@invest_photo.id}"
    
    @page_links.push([@investigation,'invest'])
  end
  
  protected
  
  def load_investigation
    @investigation = Investigation.find(params[:investigation_id])
    enforce_privacy(@investigation)
  end
  
  def invest_photo_params
    params.require(:invest_photo).permit(:date_taken, :location_taken, :taken_by_id, :taken_by_badge, :taken_by_unit, :taken_by,
      :description, :remarks, :photo_form, :invest_photo, linked_crimes: [], linked_criminals: [], linked_vehicles: []
    )
  end
end