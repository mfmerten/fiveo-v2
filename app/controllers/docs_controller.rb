class DocsController < ApplicationController
  
  def index
    options = {
      scopes: [:by_name],
      active: true,
      paginate: true,
      includes: [:person, :hold, :booking]
    }
    @docs = get_records(options)

    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @doc = Doc.find(params[:id])
    @page_title = "DOC #{@doc.id}"
    
    @links.push(["Booking", jail_booking_path(@doc.booking.jail, @doc.booking)])
    if can?(:update_booking)
      @links.push(["Edit", edit_doc_path(id: @doc.id)])
    end
    if can?(:destroy_booking)
      @links.push(['Delete', @doc, {method: 'delete', data: {confirm: confirm_msg_for(:doc)}}])
    end

    @page_links.push(@doc.person, [@doc.booking.jail, @doc.booking])
    unless @doc.transfer.nil?
      @page_links.push([@doc.booking.jail, @doc.booking, @doc.transfer])
    end
    unless @doc.sentence.nil?
      @page_links.push([@doc.sentence.docket, @doc.sentence.court, @doc.sentence])
    end
    unless @doc.revocation.nil?
      @page_links.push([@doc.revocation.docket, @doc.revocation])
    end
    unless @doc.hold.nil?
      @page_links.push([@doc.hold.jail, @doc.hold.booking, @doc.hold])
    end
    @page_links.push([@doc.creator,'creator'],[@doc.updater,'updater'])
  end
  
  # no new/create actions needed
  
  def edit
    can!(:update_booking)

    @doc = Doc.find(params[:id])
    @page_title = "Edit DOC #{@doc.id}"
  end
  
  def update
    can!(:update_booking)

    @doc = Doc.find(params[:id])

    redirect_to @doc and return if params[:commit] == 'Cancel'

    @page_title = "Edit DOC #{@doc.id}"
    @doc.updated_by = session[:user]

    success = @doc.update(doc_params)
    user_action "DOC ##{@doc.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @doc, notice: 'DOC was successfully updated.' }
        format.json { render :show, status: :ok, location: @doc }
      else
        format.html { render :edit }
        format.json { render json: @doc.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_booking)
    
    @doc = Doc.find(params[:id])
    
    @doc.destroy
    
    respond_to do |format|
      format.html { redirect_to docs_url, notice: 'DOC was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  
  def doc_params
    params.require(:doc).permit(:doc_number, :conviction, :sentence_hours, :sentence_days, :sentence_months, :sentence_years, :sentence_life,
      :sentence_death, :to_serve_hours, :to_serve_days, :to_serve_months, :to_serve_years, :to_serve_life, :to_serve_death, :billable,
      :bill_from_date, :remarks
    )
  end
end