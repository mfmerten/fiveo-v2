class InvestCrimesController < ApplicationController
  before_action :load_investigation, except: [:index, :search]

  def index
    options = {
      scopes: [:by_reverse_date],
      includes: [:investigation],
      paginate: true
    }
    @invest_crimes = get_records(options)

    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @invest_crime = @investigation.crimes.find(params[:id])
    @page_title = "Investigation Crime #{@invest_crime.id}"
    
    enforce_privacy(@invest_crime)
    
    if can?(:update_investigation) && check_author(@invest_crime)
      @links.push(['New', new_investigation_invest_crime_path(@investigation)])
      @links.push(['Edit', edit_investigation_invest_crime_path(@investigation, @invest_crime)])
      @links.push(['Delete', [@investigation,@invest_crime], {method: 'delete', data: {confirm: confirm_msg_for(:invest_crime)}}])
    end

    @page_links.push([@investigation,'invest'],[@invest_crime.creator,'creator'],[@invest_crime.updater,'updater'])
  end
  
  def new
    can!(:update_investigation)

    @invest_crime = @investigation.crimes.build
    @page_title = "New Investigation Crime"
  end
  
  def edit
    can!(:update_investigation)

    @invest_crime = @investigation.crimes.find(params[:id])

    enforce_privacy(@invest_crime)
    enforce_author(@invest_crime)

    @page_title = "Edit Investigation Crime #{@invest_crime.id}"
  end
  
  def create
    can!(:update_investigation)

    redirect_to @investigation and return if params[:commit] == 'Cancel'

    @invest_crime = @investigation.crimes.build(invest_crime_params)
    @page_title = "New Investigation Crime"
    @invest_crime.private = @investigation.private?
    @invest_crime.owned_by = @investigation.owned_by
    @invest_crime.created_by = session[:user]
    @invest_crime.updated_by = session[:user]

    success = @invest_crime.save
    user_action = "Invest Crime ##{@invest_crime.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@investigation, @invest_crime], notice: 'Invest Crime was successfully created.' }
        format.json { render :show, status: :created, location: [@investigation, @invest_crime]}
      else
        format.html { render :new }
        format.json { render json: @invest_crime.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_investigation)
    
    @invest_crime = @investigation.crimes.find(params[:id])
    
    redirect_to [@investigation, @invest_crime] and return if params[:commit] == 'Cancel'
    
    enforce_privacy(@invest_crime)
    enforce_author(@invest_crime)
    
    @page_title = "Edit Investigation Crime #{@invest_crime.id}"
    @invest_crime.updated_by = session[:user]
    params[:invest_crime][:linked_arrests] ||= []
    params[:invest_crime][:linked_cases] ||= []
    params[:invest_crime][:linked_contacts] ||= []
    params[:invest_crime][:linked_criminals] ||= []
    params[:invest_crime][:linked_evidences] ||= []
    params[:invest_crime][:linked_offenses] ||= []
    params[:invest_crime][:linked_photos] ||= []
    params[:invest_crime][:linked_stolens] ||= []
    params[:invest_crime][:linked_vehicles] ||= []
    params[:invest_crime][:linked_victims] ||= []
    params[:invest_crime][:linked_warrants] ||= []
    params[:invest_crime][:linked_witnesses] ||= []
    
    success = @invest_crime.update(invest_crime_params)
    user_action "Invest Crime ##{@invest_crime.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@investigation, @invest_crime], notice: 'Invest Crime was successfully updated.' }
        format.json { render :show, status: :ok, location: @test }
      else
        format.html { render :edit }
        format.json { render json: @invest_crime.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:update_investigation)
    
    @invest_crime = @investigation.crimes.find(params[:id])
    
    enforce_privacy(@invest_crime)
    enforce_author(@invest_crime)
    
    @invest_crime.destroy
    user_action "Invest Crime ##{@invest_crime.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to @investigation, notice: 'Invest Crime was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  protected
  
  def load_investigation
    @investigation = Investigation.find(params[:investigation_id])
    enforce_privacy(@investigation)
  end
  
  def invest_crime_params
    params.require(:invest_crime).permit(:location, :occupied, :occurred_datetime, :details, :motive, :entry_gained, :security,
      :impersonated, :weapons_used, :victim_relationship, :facts_prior, :actions_taken, :threats_made, :targets, :facts_after,
      :remarks, linked_stolens: [], linked_vehicles: [], linked_victims: [], linked_warrants: [], linked_witnesses: [],
      linked_evidences: [], linked_arrests: [], linked_cases: [], linked_contacts: [], linked_criminals: [], linked_offenses: [], linked_photos: []
    )
  end
end