class PaperTypesController < ApplicationController
  
  def index
    options = {
      scopes: [:by_name],
      paginate: true
    }
    @paper_types = get_records(options)
    
    if can?(:update_civil)
      @links.push(['New', new_paper_type_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @paper_type = PaperType.find(params[:id])
    @page_title = "Paper Type #{@paper_type.id}"
    
    if can?(:update_civil)
      @links.push(['New', new_paper_type_path])
      @links.push(['Edit', edit_paper_type_path(@paper_type)])
      @links.push(['Delete', @paper_type, {method: 'delete', data: {confirm: confirm_msg_for(:paper_type)}}])
    end
  end
  
  def new
    can!(:update_civil)

    @paper_type = PaperType.new
    @page_title = "New Paper Type"
  end

  def edit
    can!(:update_civil)

    @paper_type = PaperType.find(params[:id])
    @page_title = "Edit Paper Type #{@paper_type.id.to_s}"
  end
  
  def create
    can!(:update_civil)

    redirect_to paper_types_url and return if params[:commit] == 'Cancel'

    @paper_type = PaperType.new(paper_type_params)
    @page_title = "New Paper Type"
    @paper_type.created_by = session[:user]
    @paper_type.updated_by = session[:user]

    success = @paper_type.save
    user_action "Paper Type ##{@paper_type.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @paper_type, notice: 'Paper Type was successfully created.' }
        format.json { render :show, status: :created, location: @paper_type }
      else
        format.html { render :new }
        format.json { render json: @paper_type.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_civil)

    @paper_type = PaperType.find(params[:id])

    redirect_to @paper_type and return if params[:commit] == 'Cancel'

    @page_title = "Edit Paper Type #{@paper_type.id.to_s}"
    @paper_type.updated_by = session[:user]

    success = @paper_type.update(paper_type_params)
    user_action "Paper Type ##{@paper_type.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @paper_type, notice: 'Paper Type was successfully updated.' }
        format.json { render :show, status: :ok, location: @paper_type }
      else
        format.html { render :edit }
        format.json { render json: @paper_type.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:update_civil)
    
    @paper_type = PaperType.find(params[:id])
    
    @paper_type.destroy
    user_action "Paper Type ##{@paper_type.id} Deleted."

    respond_to do |format|
      format.html { redirect_to paper_types_url, notice: 'Paper Type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  
  def paper_type_params
    params.require(:paper_type).permit(:name, :cost)
  end
end
