class TransportsController < ApplicationController
  
  def index
    options = {
      scopes: [:by_reverse_date],
      includes: [:person],
      paginate: true
    }
    @transports = get_records(options)
    
    if can?(:update_transport)
      @links.push(['New', new_transport_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @transport = Transport.find(params[:id])
    @page_title = "Transport #{@transport.id}"
    
    if can?(:update_transport)
      @links.push(["New", new_transport_path])
      @links.push(["Edit", edit_transport_path(id: @transport.id)])
    end
    if can?(:destroy_transport)
      @links.push(['Delete', @transport, {method: :delete, data: {confirm: confirm_msg_for(:transport)}}])
    end
    @links.push(["Voucher", voucher_transport_path(@transport), pdf: true])
    
    @page_links.push(@transport.person, [@transport.creator,'creator'],[@transport.updater,'updater'])
  end
  
  def new
    can!(:update_transport)

    @page_title = "New Transport"
    @transport = Transport.new(from_state: SiteConfig.agency_state, to_state: SiteConfig.agency_state, person_id: params[:person_id])
    set_officer_to_session(@transport, :officer1)
  end
  
  def edit
    can!(:update_transport)

    @transport = Transport.find(params[:id])
    @page_title = "Edit Transport #{@transport.id}"
    @transport.from_id = nil
    @transport.to_id = nil
    @transport.officer1_id = nil
    @transport.officer2_id = nil
  end
  
  def create
    can!(:update_transport)
    
    if params[:commit] == 'Cancel'
      if !params[:person_id].blank?
        redirect_to person_url(id: params[:person_id])
      else
        redirect_to transports_url
      end
      return
    end
    
    @page_title = "New Transport"
    @transport = Transport.new(transport_params)
    @transport.created_by = session[:user]
    @transport.updated_by = session[:user]
    
    success = @transport.save
    user_action "Transport ##{@transport.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @transport, notice: 'Transport was successfully created.' }
        format.json { render :show, status: :created, location: @transport }
      else
        format.html { render :new }
        format.json { render json: @transport.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_transport)
    
    @transport = Transport.find(params[:id])
    
    redirect_to @transport and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Transport #{@transport.id}"
    @transport.updated_by = session[:user]
    
    success = @transport.update(transport_params)
    user_action "Transport ##{@transport.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @transport, notice: 'Transport was successfully updated.' }
        format.json { render :show, status: :ok, location: @transport }
      else
        format.html { render :edit }
        format.json { render json: @transport.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_transport)
    
    @transport = Transport.find(params[:id])
    
    @transport.destroy
    user_action "Transport ##{@transport.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to transports_url, notice: 'Transport was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def voucher
    @transport = Transport.find(params[:id])
    
    if pdf_url = make_pdf_file("voucher", Transports::Voucher.new(@transport))
      redirect_to pdf_url
      user_action("Printed Voucher for Transport #{params[:id]}.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to @transport
    end
  end
  
  private
  
  def transport_params
    params.require(:transport).permit(:person_id, :officer1_id, :officer1_badge, :officer1_unit, :officer1, :officer2_id, :officer2_badge,
      :officer2_unit, :officer2, :from_id, :from, :from_street, :from_city, :from_state, :from_zip, :to_id, :to, :to_street, :to_city,
      :to_state, :to_zip, :begin_datetime, :begin_miles, :end_datetime, :end_miles, :remarks
    )
  end
end
