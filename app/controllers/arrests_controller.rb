class ArrestsController < ApplicationController
  
  def index
    options = {
      scopes: [:by_reverse_id],
      includes: [:person, :officers, :charges, {bookings: :holds}, :warrants],
      paginate: true
    }
    @arrests = get_records(options)
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    if params[:info_id]
      @info_id = params[:info_id]
      @arrest = Arrest.find_by_id(params[:id])
    else
      @arrest = Arrest.find(params[:id])
      @page_title = "Arrest #{@arrest.id}"
      
      if can?(:update_arrest)
        @links.push(["Edit", edit_arrest_path(@arrest)])
      end
      if can?(:destroy_arrest)
        @links.push(['Delete', @arrest, {method: 'delete', data: {confirm: confirm_msg_for(:arrest)}} ])
      end
      @links.push(['ID Card', id_card_arrest_path(id: @arrest.id), pdf: true])
      @links.push(['48 Hour', print_48_arrest_path(id: @arrest.id), pdf: true])
      @links.push(['72 Hour', print_72_arrest_path(id: @arrest.id), pdf: true])
      unless @arrest.not_rebookable
        @links.push(['Rebook', rebook_arrest_path(id: @arrest.id), {method: 'post'}])
      end
      
      @page_links.push(@arrest.person)
      @page_links.concat(@arrest.holds.includes(:booking).collect{|h| [view_context.url_for([h.booking.jail, h.booking]), "Booking"]}.uniq)
      @page_links.push([@arrest.creator,'Creator'], [@arrest.updater, 'Updater'])
    end
    respond_to :html, :json, :js
  end

  def new
    can!(:update_arrest)
    
    @booking = Booking.find(params[:booking_id])
    @page_title = "New Arrest"
    @arrest = Arrest.new(person_id: @booking.person_id, bond_amt: 0, arrest_datetime: Time.now)
    @arrest.officers.build
    @arrest.arrest_charges.build
  end
  
  def edit
    can!(:update_arrest)
    
    @arrest = Arrest.find(params[:id])
    
    if @arrest.person.bookings.empty?
      flash.alert = "Arrested Person Not Booked. Cannot edit this arrest."
      redirect_to @arrest and return
    end
    
    @booking = @arrest.person.bookings.last
    
    if @booking.release_datetime?
      flash.now.alert = "Warning... the booking record is closed. Editing this arrest may have unintended consequences."
    elsif !@arrest.holds.empty? && @arrest.holds.first.booking != @booking
      flash.now.alert = "Warning... Arrested person has a new booking. Editing this arrest may have unintended consequences."
    end
    
    @page_title = "Edit Arrest #{@arrest.id}"
    @arrest.dwi_test_officer_id = nil
    @arrest.agency_id = nil
    if @arrest.officers.empty?
      @arrest.officers.build
    end
    if @arrest.arrest_charges.empty?
      @arrest.arrest_charges.build
    end
  end
    
  def create
    can!(:update_arrest)
    
    redirect_to arrests_url and return if params[:commit] == 'Cancel'
    
    @arrest = Arrest.new(arrest_params)
    @booking = Booking.find(params[:booking_id])
    @page_title = "New Arrest"
    @arrest.created_by = session[:user]
    @arrest.updated_by = session[:user]
    
    success = @arrest.save
    user_action("Arrest ##{@arrest.id} Created.") if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @arrest, notice: 'Arrest was successfully created.' }
        format.json { render :show, status: :created, location: @arrest }
      else
        format.html { render :new }
        format.json { render json: @arrest.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_arrest)
    
    @arrest = Arrest.find(params[:id])
    @booking = Booking.find(params[:booking_id])
    @page_title = "Edit Arrest #{@arrest.id}"
    
    redirect_to @arrest and return if params[:commit] == 'Cancel'
    
    @arrest.updated_by = session[:user]
    @arrest.legacy = false
    
    success = @arrest.update(arrest_params)
    user_action("Arrest ##{@arrest.id} Updated.") if success

    respond_to do |format|
      if success
        format.html { redirect_to @arrest, notice: 'Arrest was successfully updated.' }
        format.json { render :show, status: :ok, location: @arrest }
      else
        format.html { render :edit }
        format.json { render json: @arrest.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:destroy_arrest)
    
    @arrest = Arrest.find(params[:id])
    
    if reasons = @arrest.reasons_not_destroyable
      flash.alert = "Cannot delete: #{reasons}"
      redirect_to @arrest and return
    end
    
    @arrest.destroy
    user_action("Arrest ##{@arrest.id} Deleted.")
    
    respond_to do |format|
      format.html { redirect_to arrests_url, notice: 'Arrest was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def rebook
    can!(:update_arrest)
    
    @arrest = Arrest.find(params[:id])
    
    if reasons = @arrest.not_rebookable
      flash.alert = "Cannot Rebook: #{reasons}"
      redirect_to @arrest and return
    end
    
    if errors = @arrest.rebook
      flash.alert = "Rebook failed: #{errors}!"
      redirect_to @arrest and return
    end
    
    @booking = @arrest.person.bookings.last
    
    flash.notice = "Arrest ##{arrest.id} Rebooked."
    user_action "Arrest ##{arrest.id} Rebooked."

    redirect_to [@booking.jail, @booking] and return
  end
  
  def seventytwo
    can!(:update_arrest)

    @arrest = Arrest.find(params[:id])

    if @arrest.district_charges.empty?
      flash.alert = "No District Charges, 72 Hour Not Applicable"
      redirect_to @arrest and return
    end
    
    unless hold = @arrest.holds.of_type("72 Hour").last
      flash.alert = "No 72 Hour hold found for this Arrest.  72 Hour Not Applicable"
      redirect_to @arrest and return
    end
    
    if hold.cleared_datetime?
      flash.alert = "72 Hour hold has already been cleared.  72 Hour not Applicable"
      redirect_to [hold.jail, hold.booking, hold] and return
    end
    
    if hold.booking.nil?
      flash.alert = "72 Hour Hold has no valid booking.  72 Hour Not Applicable"
      redirect_to [hold.jail, hold.booking, hold] and return
    end
    
    @booking = hold.booking
    
    if @booking.release_datetime?
      flash.alert = "Booking record is closed. 72 Hour Not Applicable"
      redirect_to [@booking.jail, @booking] and return
    end
    
    @page_title = "72 Hour Information for Arrest #{@arrest.id}"
    @arrest.updated_by = session[:user]
    
    if request.post?
      if params[:commit] == "Cancel"
        redirect_to [@booking.jail, @booking] and return
      end
      
      if @arrest.update(arrest_params)
        redirect_to [@booking.jail, @booking]
        user_action("Updated 72 Hour information for Arrest #{@arrest.id}.")
      end
    end
  end

  def id_card
    @arrest = Arrest.find(params[:id])
    
    if pdf_url = make_pdf_file("arrest_id_card", Arrests::IdCard.new(@arrest))
      redirect_to pdf_url
      user_action("Printed Jail ID Card for Arrest #{params[:id]}.")
    else
      flash.alert = "Could not create temporary PDF file for download!"
      redirect_to @arrest
    end
  end

  def print_72
    @arrest = Arrest.find(params[:id])
    
    if pdf_url = make_pdf_file("arrest_72_hour", Arrests::SeventyTwoHour.new(@arrest))
      redirect_to pdf_url
      user_action("Printed 72 Hour Form for Arrest #{params[:id]}.")
    else
      flash.alert = "Could not create temporary PDF file for download!"
      redirect_to @arrest
    end
  end

  def print_48
    @arrest = Arrest.find(params[:id])
    
    if pdf_url = make_pdf_file("arrest_48_hour", Arrests::FortyEightHour.new(@arrest))
      redirect_to pdf_url
      user_action("Printed 48 Hour Form for Arrest #{params[:id]}.")
    else
      flash.alert = "Could not create temporary PDF file for download!"
      redirect_to @arrest
    end
  end
  
  def arrest_summary
    unless @start_date = AppUtils.valid_date(params[:start_date])
      @start_date = Date.today.beginning_of_month
    end
    unless @stop_date = AppUtils.valid_date(params[:end_date])
      @stop_date = Date.today
    end
    if !@start_date.nil? && !@stop_date.nil? && @start_date > @stop_date
      # wrong order, swap
      @start_date, @stop_date = @stop_date, @start_date
    end
    @agency = Contact.find_by_id(params[:agency_id])
    if @agency.nil?
      @arrests = Arrest.arrest_summary(@start_date, @stop_date)
      aname = nil
    else
      @arrests = Arrest.arrest_summary(@start_date, @stop_date, @agency.sort_name)
      aname = @agency.sort_name
    end
    
    if @arrests.empty?
      flash.alert = "Nothing to report!"
      redirect_to arrests_url and return
    end
    
    if pdf_url = make_pdf_file("arrest_summary", Arrests::Summary.new(@arrests, @start_date, @stop_date, aname))
      redirect_to pdf_url
      user_action("Printed Arrest Summary Report.")
    else
      flash.alert = "Could not create temporary PDF file for download!"
      redirect_to arrests_url
    end
  end
  
  private
  
  def arrest_params
    params.require(:arrest).permit(:person_id, :case_no, :agency, :ward, :district, :victim_notify, :dwi_test_officer,
      :dwi_test_officer_unit, :dwi_test_results, :attorney, :condition_of_bond, :remarks, :bond_amt, :bondable,
      :dwi_test_officer_badge, :legacy, :atn, :arrest_datetime, :hour_72_datetime, :hour_72_judge, :zone, :agency_id,
      :dwi_test_officer_id, :parish_probation, :state_probation, assigned_warrants: [],
      officers_attributes: [:id, :officer_id, :officer_unit, :officer_badge, :officer, :_destroy],
      arrest_charges_attributes: [:id, :charge_type, :count, :charge, :agency, :_destroy])
  end
end
