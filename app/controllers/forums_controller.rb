# adapted from "forem" engine (https://github.com/radar/forem)
#
# Copyright 2011 Ryan Bigg, Philip Arndt and Josh Adams
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
class ForumsController < ApplicationController
  before_action :set_page_title
  
  def index
    @forum_categories = ForumCategory.all
  end

  def show
    @forum = Forum.find(params[:id])
    register_view
    @forum_topics = (can?(:admin_forum) ? @forum.forum_topics : @forum.forum_topics.visible)
    @forum_topics = @forum_topics.by_pinned_or_most_recent_post.page(params[:page]).per(session[:items_per_page])
    
    if can?(:admin_forum)
      @links.push(["Add", new_forum_url])
      @links.push(["Edit", edit_forum_url(@forum)])
      @links.push(["Delete", @forum, {method: 'delete', data: {confirm: confirm_msg_for(:forum)}}])
    end
  end

  def new
    can!(:admin_forum)

    @forum = Forum.new(created_by: session[:user], updated_by: session[:user])
    if params[:forum_category_id]
      @forum.forum_category_id = params[:forum_category_id]
    end
    @page_title = "New Forum"
  end

  def edit
    can!(:admin_forum)

    @forum = Forum.find(params[:id])
    @forum.updated_by = session[:user]
    @page_title = "Edit Forum #{@forum.id}"
  end

  def create
    redirect_to forums_url and return if params[:commit] == 'Cancel'

    can!(:admin_forum)

    @forum = Forum.new(forum_params)
    @page_title = "New Forum"
    
    success = @forum.save
    user_action "Forum ##{@forum.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @forum, notice: 'Forum was successfully created.' }
        format.json { render :show, status: :created, location: @forum }
      else
        format.html { render :new }
        format.json { render json: @forum.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    redirect_to forums_url and return if params[:commit] == 'Cancel'

    can!(:admin_forum)

    @forum = Forum.find(params[:id])
    @page_title = "Edit Forum #{@forum.id}"

    success = @forum.update(forum_params)
    user_action "Forum ##{@forum.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @forum, notice: 'Forum was successfully updated.' }
        format.json { render :show, status: :ok, location: @forum }
      else
        format.html { render :edit }
        format.json { render json: @forum.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:admin_forum)

    @forum = Forum.find(params[:id])

    @forum.destroy
    user_action "Forum ##{@forum.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to forums_url, notice: 'Forum was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  
  def register_view
    @forum.register_view_by(current_user)
  end
  
  def set_page_title
    @page_title = "FiveO Discussion Forums"
  end
  
  def forum_params
    params.require(:forum).permit(:created_by, :updated_by, :forum_category_id, :name, :description)
  end
end