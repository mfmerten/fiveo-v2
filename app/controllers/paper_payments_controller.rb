class PaperPaymentsController < ApplicationController
  before_action :load_customer
  
  # no index
  
  def show
    @paper_payment = @paper_customer.payments.find(params[:id])
    if !@paper_payment.paper.nil? && @paper_payment == @paper_payment.paper.payments.first
      @page_title = "Paper Invoice Transaction #{@paper_payment.id}"
    elsif @paper_payment.adjustment?
      @page_title = "Paper Adjustment Transaction #{@paper_payment.id}"
    else
      @page_title = "Paper Payment Transaction #{@paper_payment.id}"
    end
    
    if can?(:update_paper)
      if !@paper_payment.report_datetime?
        if !@paper_payment.is_invoice?
          @links.push(["Edit", edit_paper_customer_paper_payment_path(@paper_customer, @paper_payment)])
        end
        unless @paper_payment.is_invoice? && @paper_payment.paper.payments.size > 1
          @links.push(["Delete", [@paper_customer, @paper_payment], {method: 'delete', data: {confirm: confirm_msg_for(:payment)}}])
        end
      end
    end
    
    @page_links.push([@paper_payment.customer,'customer'],[@paper_payment.paper,'Paper'],[@paper_payment.creator,'creator'],[@paper_payment.updater,'updater'])
  end

  def new
    can!(:update_civil)

    if params[:paper_id]
      @paper = @paper_customer.papers.find(params[:paper_id])
      
      if @paper.paid_date?
        flash.alert = "Paper #{@paper.id} has already been paid!"
        redirect_to @paper and return
      end
      
      unless @paper.invoice_due > 0
        flash.alert = "Paper #{@paper.id} has no due amount!"
        redirect_to @paper and return
      end
    end
    
    @paper_payment = @paper_customer.payments.build
    if params[:paper_id]
      @paper_payment.paper_id = @paper.id
      @paper_payment.payment = @paper.invoice_total
    else
      @paper_payment.adjustment = true
    end
    @paper_payment.transaction_date = Date.today
    set_officer_to_session(@paper_payment, :officer)
    if @paper_payment.adjustment?
      @page_title = "New Paper Adjustment Transaction #{@paper_payment.id}"
    else
      @page_title = "New Paper Payment Transaction #{@paper_payment.id}"
    end
  end
  
  def edit
    can!(:update_civil)

    @paper_payment = @paper_customer.payments.find(params[:id])

    if @paper_payment.report_datetime?
      flash.alert = "Cannot Edit... already posted!"
      redirect_to [@paper_customer, @paper_payment] and return
    end
    
    if @paper_payment.is_invoice?
      flash.alert = "Cannot Edit... Invoice Transaction!"
      redirect_to [@paper_customer, @paper_payment] and return
    end
    
    if @paper_payment.adjustment?
      @page_title = "Edit Paper Adjustment Transaction #{@paper_payment.id}"
    else
      @page_title = "Edit Paper Payment Transaction #{@paper_payment.id}"
    end
  end
  
  def create
    can!(:update_civil)

    if params[:paper_id]
      @paper = @paper_customer.papers.find(params[:paper_id])
      
      redirect_to @paper and return if params[:commit] == 'Cancel'
      
      if @paper.paid_date?
        flash.alert = "Paper #{@paper.id} has already been paid!"
        redirect_to @paper and return
      end
      
      unless @paper.invoice_due > 0
        flash.alert = "Paper #{@paper.id} has no due amount!"
        redirect_to @paper and return
      end
    else
      redirect_to @paper_customer and return if params[:commit] == 'Cancel'
    end
    
    @paper_payment = @paper_customer.payments.build(paper_payment_params)
    if @paper_payment.adjustment?
      @page_title = "New Paper Adjustment Transaction #{@paper_payment.id}"
    else
      @page_title = "New Paper Payment Transaction #{@paper_payment.id}"
    end
    @paper_payment.created_by = session[:user]
    @paper_payment.updated_by = session[:user]
    
    success = @paper_payment.save
    user_action "Paper Payment ##{@paper_payment.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@paper_customer, @paper_payment], notice: 'Paper Payment was successfully created.' }
        format.json { render :show, status: :created, location: [@paper_customer, @paper_payment] }
      else
        format.html { render :new }
        format.json { render json: @paper_payment.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    can!(:update_civil)

    @paper_payment = @paper_customer.payments.find(params[:id])

    if @paper_payment.paper.nil?
      redirect_to @paper_customer and return if params[:commit] == 'Cancel'
    else
      redirect_to @paper_payment.paper and return if params[:commit] == 'Cancel'
    end

    if @paper_payment.report_datetime?
      flash.alert = "Cannot Edit... already posted!"
      redirect_to [@paper_customer, @paper_payment] and return
    end
    
    if @paper_payment.is_invoice?
      flash.alert = "Cannot Edit... Invoice Transaction!"
      redirect_to [@paper_customer, @paper_payment] and return
    end
    
    if @paper_payment.adjustment?
      @page_title = "Edit Paper Adjustment Transaction #{@paper_payment.id}"
    else
      @page_title = "Edit Paper Payment Transaction #{@paper_payment.id}"
    end
    @paper_payment.updated_by = session[:user]
    
    success = @paper_payment.update(paper_payment_params)
    user_action "Paper Payment ##{@paper_payment.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@paper_customer, @paper_payment], notice: 'Paper Payment was successfully updated.' }
        format.json { render :show, status: :ok, location: [@paper_customer, @paper_payment] }
      else
        format.html { render :edit }
        format.json { render json: @paper_payment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:update_civil)

    @paper_payment = @paper_customer.payments.find(params[:id])

    if reasons = @paper_payment.reasons_not_destroyable
      flash.alert = "Cannot delete: #{reasons}"
      redirect_to [@paper_customer, @paper_payment] and return
    end

    @paper_payment.destroy
    user_action "Paper Payment ##{@paper_payment.id} Deleted."
    
    ret_loc = @paper_customer
    unless @paper_payment.paper.nil?
      ret_loc = @paper_payment.paper
    end

    respond_to do |format|
      format.html { redirect_to ret_loc, notice: 'Paper Payment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  protected
  
  def load_customer
    @paper_customer = PaperCustomer.find(params[:paper_customer_id])
  end
  
  def paper_payment_params
    params.require(:paper_payment).permit(:paper_id, :adjustment, :transaction_date, :officer_id, :officer_badge, :officer_unit, :officer,
      :payment_method, :payment, :paid_in_full, :charge, :receipt_no, :memo
    )
  end
end