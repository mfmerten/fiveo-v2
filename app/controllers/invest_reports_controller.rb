class InvestReportsController < ApplicationController
  before_action :load_investigation, except: [:index, :search]
  
  def index
    options = {
      scopes: [:by_reverse_id],
      paginate: true
    }
    @invest_reports = get_records(options)

    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @invest_report = @investigation.reports.find(params[:id])
    @page_title = "Investigation Report #{@invest_report.id}"
    
    enforce_privacy(@invest_report)
    
    if can?(:update_investigation) && check_author(@invest_report)
      @links.push(['New', new_investigation_invest_report_path(@investigation)])
      @links.push(['Edit', edit_investigation_invest_report_path(@investigation, @invest_report)])
      @links.push(['Delete', [@investigation, @invest_report], {method: 'delete', data: {confirm: confirm_msg_for(:invest_report)}}])
      @links.push(['Report', print_report_investigation_invest_report_path(@investigation, @invest_report), pdf: true])
    end

    @page_links.push([@investigation,'invest'],[@invest_report.creator,'creator'],[@invest_report.updater,'updater'])
  end

  def new
    can!(:update_investigation)

    @invest_report = @investigation.reports.build(report_datetime: Time.now)
    @page_title = "New Investigation Report"
  end
  
  def edit
    can!(:update_investigation)

    @invest_report = @investigation.reports.find(params[:id])

    enforce_privacy(@invest_report)
    enforce_author(@invest_report)

    @page_title = "Edit Investigation Report #{@invest_report.id}"
  end
  
  def create
    redirect_to @investigation and return if params[:commit] == 'Cancel'
    
    can!(:update_investigation)
    
    @invest_report = @investigation.reports.build(invest_report_params)
    @page_title = "New Investigation Report"
    @invest_report.private = @investigation.private?
    @invest_report.created_by = session[:user]
    @invest_report.updated_by = session[:user]
    @invest_report.owned_by = @investigation.owned_by

    success = @invest_report.save
    user_action "Invest Report ##{@invest_report.id} Created." if success
    
    if success
      format.html { redirect_to [@investigation, @invest_report], notice: 'Invest Report was successfully created.' }
      format.json { render :show, status: :created, location: [@investigation, @invest_report] }
    else
      format.html { render :new }
      format.json { render json: @invest_report.errors, status: :unprocessable_entity }
    end
  end
  
  def update
    can!(:update_investigation)
    
    @invest_report = @investigation.reports.find(params[:id])
    
    redirect_to [@investigation, @invest_report] and return if params[:commit] == 'Cancel'
    
    enforce_privacy(@invest_report)
    enforce_author(@invest_report)
    
    @page_title = "Edit Investigation Report #{@invest_report.id}"
    @invest_report.updated_by = session[:user]
    params[:invest_report][:linked_arrests] ||= []
    params[:invest_report][:linked_criminals] ||= []
    params[:invest_report][:linked_offenses] ||= []
    params[:invest_report][:linked_photos] ||= []
    params[:invest_report][:linked_vehicles] ||= []
    params[:invest_report][:linked_victims] ||= []
    params[:invest_report][:linked_witnesses] ||= []
    
    success = @invest_report.update(invest_report_params)
    user_action "Invest Report ##{@invest_report.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@investigation, @invest_report], notice: 'Invest Report was successfully updated.' }
        format.json { render :show, status: :ok, location: [@investigation, @invest_report] }
      else
        format.html { render :edit }
        format.json { render json: @invest_report.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:update_investigation)
    
    @invest_report = @investigation.reports.find(params[:id])
    
    enforce_privacy(@invest_report)
    enforce_author(@invest_report)
    
    @invest_report.destroy
    user_action "Invest Report ##{@invest_report.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to @investigation, notice: 'Invest Report was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def print_report
    @invest_report = @investigation.reports.find(params[:id])
    
    enforce_privacy(@invest_report)
    
    if pdf_url = make_pdf_file("report", InvestReports::Form.new(@invest_report))
      redirect_to pdf_url
      user_action("Printed Investigation Report.")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to [@investigation, @invest_report]
    end
  end
  
  protected
  
  def load_investigation
    @investigation = Investigation.find(params[:investigation_id])
    enforce_privacy(@investigation)
  end
  
  def invest_report_params
    params.require(:invest_report).permit(:report_datetime, :supplemental, :details, :remarks, linked_arrests: [], linked_criminals: [],
      linked_offenses: [], linked_photos: [], linked_vehicles: [], linked_victims: [], linked_witnesses: []
    )
  end
end