class ApplicationController < ActionController::Base

  protect_from_forgery

  before_action :login_required, except: 'login_required'
  before_action :set_help_page
  before_action :require_base_perms, except: 'login_required'
  before_action :update_session_active
  before_action :setup_links

  helper_method :generate_onetime_password, :user_action, :current_user,
    :can?, :check_privacy, :check_author, :confirm_msg_for

  helper :all # include all helpers, all the time

  # this picks out 404 error codes and displays a special template
  def render_optional_error_file(status_code)
    if status_code == :not_found
      render_404
    else
      super
    end
  end

  def render_404
    @page_title = "Page Not Found!"
    respond_to do |type|
      type.html { render "error_codes/page_not_found_error", layout: 'application', status: 404 } 
      type.all  { render nothing: true, status: 404 }
    end
    true
  end

  protected
  
  ####################
  # Custom Error Section
  ####################

  class PermissionDeniedError < StandardError; end
  class InvalidMethodError < StandardError; end

  #rescue_from ArgumentError, with: :invalid_argument_error
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found_error
  rescue_from PermissionDeniedError, with: :permission_denied_error
  rescue_from ActionController::InvalidAuthenticityToken, with: :invalid_token_error

  def invalid_argument_error(e)
    @message = e.message
    @page_title = "Invalid Argument!"
    logger.info("\nInvalid Argument Error: #{@message} (UID #{session[:user]})\n\n")
    respond_to do |format|
      format.html { render 'error_codes/invalid_argument_error.html.erb', layout: 'application' }
      format.json { render json: "Invalid Argument!", status: :unprocessable_entity }
    end
  end
  
  def permission_denied_error(e)
    @message = e.message
    @page_title = "Permission Denied!"
    logger.info("\nPermission Denied Error: #{@message} (UID #{session[:user]})\n\n")
    respond_to do |format|
      format.html { render 'error_codes/permission_denied_error.html.erb', layout: 'application' }
      format.json { render json: "Permission Denied!", status: :forbidden }
    end
  end
  
  def record_not_found_error(e)
    @message = e.message
    @page_title = "Record Not Found!"
    # don't log these
    respond_to do |format|
      format.html { render 'error_codes/record_not_found_error.html.erb', layout: 'application' }
      format.json { render json: "Record Not Found!", status: :not_found }
    end
  end
  
  def invalid_token_error(e)
    # no message needed
    # don't log these
    respond_to do |format|
      format.html { render 'error_codes/invalid_token_error.html.erb', layout: 'application' }
      format.json { render json: "Invalid Authenticity Token!", status: :unauthorized }
    end
  end

  ####################
  
  def user_action(description,*args)
    options = args.extract_options!
    section = options[:section] || self.controller_name.titleize
    if !description.is_a?(String) || description.blank?
      logger.error("user_action: description (arg2) is required and must be a string")
    else
      Activity.add(section, description, session[:user], session[:user_login])
      AppUtils.audit_log(description, self.controller_name + "_controller." + self.action_name, session[:user])
    end
  end

  # true if user has all of the specified permissions
  def can?(*args)
    options = args.extract_options!
    perms = args
    return false unless session[:user]
    unless options[:noadmin]
      return true if session[:pids].include?(Perm.get(:admin))
    end
    retval = true
    perms.each do |p|
      retval = false unless session[:pids].include?(Perm.get(p))
    end
    retval
  end
  
  # raise permission denied error exception unless user has all of the specified permissions
  def can!(*args)
    return true if can?(*args)
    raise PermissionDeniedError.new("#{self.controller_name}##{self.action_name}: You do not have permission to perform this action.")
  end
  
  # returns true if object is not capable of privacy, or if the record is
  # not private, or if the record is private and the current_user "owns" the
  # record.  Otherwise it returns false.
  def check_privacy(object)
    return true if can?(:no_privacy, noadmin: true)
    return true unless object.is_a?(ActiveRecord::Base) && object.respond_to?('private') && (object.respond_to?('owned_by') || object.respond_to?('created_by'))
    return true unless object.private?
    if object.respond_to?('owned_by')
      return true if object.owned_by == session[:user] || (object.owned_by.nil? && object.created_by == session[:user])
    end
    if object.respond_to?('created_by')
      return true if object.created_by == session[:user]
    end
    return false
  end

  # uses check_privacy method and if returns false, raises permission denied error
  def enforce_privacy(object)
    return true if check_privacy(object)
    raise PermissionDeniedError.new("#{self.controller_name}##{self.action_name}: You do not have permission to access this private information.")
  end

  # returns true if object is owned by current_user or current user has
  # author override permissions.
  def check_author(object)
    return true if can?(:author_all)
    return true unless object.is_a?(ActiveRecord::Base) && (object.respond_to?('owned_by') || object.respond_to?('created_by') || object.is_a?(ForumPost) || object.is_a?(ForumTopic)
    if object.is_a?(ForumPost) || object.is_a(ForumTopic)
      return object.owner_or_admin?(current_user)
    end
    return true if object.respond_to?('owned_by') && object.owned_by == session[:user]
    return true if object.respond_to?('created_by') && object.created_by == session[:user])
    return false
  end

  # uses check_author method and raises exception if false
  def enforce_author(object)
    return true if check_author(object)
    raise PermissionDeniedError.new("#{self.controller_name}##{self.action_name}: Only the owner of this record can perform that action.")
  end

  def current_user
    User.find(session[:user])
  end

  # called before each action to update the session active time
  def update_session_active
    if session[:user]
      session[:active_at] = Time.now
    end
  end

  # called to test whether session has expired according to site config
  # settings
  def session_expired?
    return true if session[:started_at].nil? || session[:active_at].nil?
    if SiteConfig.session_max > 0 && SiteConfig.session_max.minutes.ago > session[:started_at]
      # beyond maximum life - expire it
      return true
    end
    unless SiteConfig.session_life > 0 && SiteConfig.session_life.minutes.ago < session[:started_at]
      # no grace period or it has expired
      if SiteConfig.session_active > 0 && SiteConfig.session_active.minutes.ago > session[:active_at]
        # inactive - expire it
        return true
      end
    end
    return false
  end

  # login_required is used as a default controller filter that ensures that the
  # user has a valid session and that the session is not expired.  Actions that
  # do not require login for access should manually skip this filter.
  def login_required
    if session[:user]
      if session_expired?
        # session expired
        flash.alert = "Your Session Has Expired. Please Log In Again."
        redirect_to logout_url
      elsif session[:user_expires] <= Date.today
        # password expired
        flash.alert = "Your password has expired."
        redirect_to change_password_url
      end
      return
    end
    flash.notice = 'Please Log In To Continue'
    redirect_to login_url
    return false 
  end
  
  # used to filter records for index and other actions considering filter settings,
  #  active record specification, record privacy, jail settings, user settings, and
  #  condition presets
  # uses model scopes to perform all filtering, so if this is not working, first
  #   check that the model defines the proper scopes and a class method named
  #   self.process_query
  # Note, this replaces the old 'get_conditions' , 'process_filter' and 'process_active'
  #   helpers for those controllers that have been updated to use scopes
  #
  # Valid Options:
  #          active: if set to true, will comply with show_all_... parameter
  #      model_name: if set to name of model, will use that name instead of controller name
  #                    when persisting options to session.
  #      query_skip: if set to a string list of fields, will remove those fields from a
  #                    supplied query hash when considering 'active' records - used to
  #                    avoid fields that will interfere with resulting relation.
  #          scopes: if set to list of scope names, will apply those scopes in addition to
  #                    any others that are appropriate.
  #     path_prefix: used to specify path prefix for generated links if necessary
  #    path_options: used to specify options for generated links if necessary
  #      table_name: used to specify a table name if different from self.controller_name
  #        paginate: specifies whether or not to paginate results (by :page and :per methods)
  #        includes: array of associations to include
  #           joins: array of relationships to join (done prior to query processing)
  #   ignore_filter: skips process_query method for model if true
  #
  def get_records(*args)
    options = HashWithIndifferentAccess.new(ignore_filter: false, joins: nil, paginate: false, active: false, model_name: nil, query_skip: nil, scopes: nil, path_prefix: nil, path_options: nil, table_name: nil)
    options.update(args.extract_options!)
    
    # determine associated model class
    mname = self.controller_name.classify

    if options[:model_name]
      begin
        model_class = options[:model_name].constantize
        model_name = options[:model_name]
        table_name = model_name.pluralize
      rescue
        model_class = nil
      end
    else
      begin
        model_class = mname.constantize
        table_name = self.controller_name
        model_name = table_name.singularize
      rescue
        model_class = nil
      end
    end
    
    # can't continue unless we have a model
    return nil if model_class.nil?
    
    # determine if current request includes a filter query or request
    #   and set up the appropriate instance variable
    session_name = "#{self.controller_name.singularize}_filter".to_sym
    if params[:filter] == 'clear'
      @query = HashWithIndifferentAccess.new
      session[session_name] = nil
    else
      if @query = params[:query]
        session[session_name] = @query
      else
        @query = session[session_name] || HashWithIndifferentAccess.new
      end
    end
    
    # obey show_all_... setting if options[:active] is true
    if options[:active]
      # 1. set up session
      if session["show_all_#{table_name}".to_sym].nil?
        session["show_all_#{table_name}".to_sym] = false
      end
      if params[:show_all]
        session["show_all_#{table_name}".to_sym] = (params[:show_all] == "1")
      end
      
      # 2. add button
      list_path = self.controller_name + "_path"
      if options[:path_prefix]
        list_path = options[:path_prefix] + "_" + list_path
      end
      list_options = options[:path_options] || {}
      if session["show_all_#{table_name}".to_sym]
        @links.push(['Show Active', self.send("#{list_path}", list_options.merge({show_all: '0'}))])
      else
        @links.push(['Show All', self.send("#{list_path}", list_options.merge({show_all: '1'}))])
      end
    end
    
    # process any filter query that exists
    if model_class.respond_to?('process_query') && !@query.empty? && !options[:ignore_filter]
      # first remove any fields that are determined to conflict
      # this is only done if there is actually a filter query
      if options[:active] && session["show_all_#{table_name}".to_sym] == false
        if options[:query_skip] && options[:query_skip].is_a?(String)
          @query.delete(options[:query_skip])
        elsif options[:query_skip].is_a?(Array)
          options[:query_skip].each{|s| @query.delete(s)}
        end
      end
      
      # now apply results of model method :process_query
      model_relation = model_class.joins(options[:joins]).includes(options[:includes]).process_query(@query)
    else
      model_relation = model_class.joins(options[:joins]).includes(options[:includes])
    end
    
    # limit records if show_all_... is false
    if options[:active]
      unless session["show_all_#{table_name}".to_sym]
        if model_class.respond_to?('active')
          model_relation = model_relation.active
        end
      end
    end
    
    # apply any controller-specified scopes
    if options[:scopes]
      if options[:scopes].is_a?(Symbol)
        # specified as a single scope symbol. Convert to array
        options[:scopes] = [options[:scopes]]
      end
      for s in options[:scopes]
        if model_class.respond_to?(s)
          model_relation = model_relation.send(s)
        end
      end
    end
    
    # hide private records unless user is owner (or user has no_privacy permission)
    # this depends on the model defining a :without_private_records scope that accepts
    #   a user_id that represents the current user and returns relation containing all
    #   records accessible by that user
    unless can?(:no_privacy, noadmin: true)
      # user is not flagged for skipping privacy checks
      
      # if the model defines a :without_private_records scope, apply it to the model_relation
      if model_class.respond_to?('without_private_records')
        model_relation = model_relation.without_private_records
      end
    end
    
    # if the model defines a 'belonging_to_jail' scope and there is an instance variable @jail 
    # set to a jail record, we need to limit all queries to those records which
    # match that jail_id
    if model_class.respond_to?('belonging_to_jail') && !@jail.nil?
      model_relation = model_relation.belonging_to_jail(@jail.id)
    end
    
    # handle pagination
    if options[:paginate]
      model_relation = model_relation.page(params[:page]).per(session[:items_per_page])
    end
    
    # return records
    model_relation
  end
    
  # this requires the permission(s) defined in the associated model as base_perms
  # it has no effect if the controller has no model or if the model does not
  # define "base_perms" method.
  def require_base_perms
    mname = self.controller_name.classify
    if Object.const_defined?(mname)
      model_class = Object.const_get(mname)
      if model_class.respond_to?("base_perms")
        [model_class.base_perms].flatten.each{|p| can!(p)}
      end
    end
    true
  end
  
  # defines help_page instance variable from controller name
  def set_help_page
    @help_page = self.controller_name.tableize.singularize
    # handle controllers not named same as help pages
    case self.controller_name.tableize.singularize
    when 'root'
      @help_page = 'index'
    end
  end
  
  # generates a random string to use as a one-time password (new accounts,
  # password resets, etc.
  def generate_onetime_password(size = 6)
    charset = [('a'..'z').to_a,('0'..'9').to_a].flatten
    (0...size).map{ charset.to_a[rand(charset.size)] }.join
  end
  
  # builds a regex matching permutations of login.  this is used to detect
  # when someone attempts to use some form of their login as a password using
  # "133t 5p33k" or mixed caps.
  def permutate_login(text)
    permutations = {
      'b' => '[Bb8]', 
      'e' => '[Ee3]',
      'h' => '[Hh#]',
      'i' => '[Ii1l]',
      'l' => '[Ll1Ii]',
      'o' => '[Oo0]',
      'q' => '[Qq0]',
      's' => '[Ss5]',
      'z' => '[Zz2]',
      '0' => '[0oO]',
      '1' => '[1Iil]',
      '2' => '[2zZ]',
      '3' => '[3E]',
      '5' => '[5sS]',
      '8' => '[8B]'
    }
    string = ''
    text.each_char do |char|
      if !permutations[char].nil?
        string << permutations[char]
      elsif char.to_i > 0 || char.upcase == char.downcase
        string << char
      else
        string << "[#{char.upcase}#{char.downcase}]"
      end
    end
    /#{string}/
  end
  
  # accepts a file base name and a data object and creates a temporary
  # pdf file (in public/system/pdf_tmp directory). returns url to the file.
  # returns nil if data object does not respond to .to_pdf method.
  def make_pdf_file(base='',data=nil)
    return nil if data.nil? || !data.respond_to?('to_pdf') # bad data object
    unless base.is_a?(String)
      # can only work with strings
      base = ''
    end
    # only allow letters and numbers for file names
    base = base.gsub(/\./,'_').gsub(/[^a-zA-Z0-9_]/,'')
    unless base.blank?
      base << '-'
    end
    # temporary PDF file directory
    tempdir = "public/files"
    charset = [('a'..'z').to_a,('0'..'9').to_a,"_"].flatten
    # loop up to 10 times trying to open a new temp file with a random name
    # component, return nil if can't get file opened.
    file = nil
    10.times do
      if file.nil?
        fname = tempdir + "/" + base + (0...15).map{ charset.to_a[rand(charset.size)] }.join + '.pdf'
        begin
          file = File.open(fname, File::WRONLY|File::CREAT|File::EXCL)
        rescue
          file = nil
        end
      end
    end
    return nil if file.nil? # failed to open file
    # set file mode to binmode
    file = file.binmode
    file.print(data.to_pdf)
    file_url = url_for(file.path.sub(/^public/,''))
    file_url = "#{ENV['RAILS_RELATIVE_URL_ROOT']}#{file_url}"
    file.close
    return file_url
  end
  
  # standard confirm message for destroy
  def confirm_msg_for(name)
    "Are you sure you want to delete this #{name.to_s.titleize}? This cannot be undone!"
  end
  
  # sets up empty @links, @linksx and @page_links
  def setup_links
    @links = []
    @linksx = []
    @page_links = []
  end
  
  # sets indicated officer fields to current user
  # if object is not a standard singular instance variable named for the
  # controller, set it using the :object option
  # i.e. set_officer_to_session(:officer, object: @some_instance_variable)
  # returns: nil if problem, true if ok
  def set_officer_to_session(base, *args)
    options = args.extract_options!
    # get instance variable
    object = options[:object] || self.instance_variable_get("@#{self.controller_name.singularize}")
    return nil if object.nil? || !session[:user]
    # set column names from base
    officer_column = "#{base}"
    badge_column = "#{base}_badge"
    unit_column = "#{base}_unit"
    # update officer name
    if object.respond_to?(officer_column)
      object.send("#{officer_column}=", session[:user_name])
    end
    # update officer badge
    if object.respond_to?(badge_column)
      object.send("#{badge_column}=", session[:user_badge])
    end
    # update officer unit
    if object.respond_to?(unit_column)
      object.send("#{unit_column}=", session[:user_unit])
    end
    return true
  end
end