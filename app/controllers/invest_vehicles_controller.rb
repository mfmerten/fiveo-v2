class InvestVehiclesController < ApplicationController
  before_action :load_investigation, except: [:index, :search]
  
  def index
    options = {
      scopes: [:by_reverse_id],
      paginate: true
    }
    @invest_vehicles = get_records(options)

    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end
  
  def show
    @invest_vehicle = InvestVehicle.find(params[:id])
    @page_title = "Investigation Vehicle #{@invest_vehicle.id}"
    
    enforce_privacy(@invest_vehicle)
    
    if can?(:update_investigation) && check_author(@invest_vehicle)
      @links.push(['New', new_investigation_invest_vehicle_path(@investigation)])
      @links.push(['Edit', edit_investigation_invest_vehicle_path(@investigation, @invest_vehicle)])
      @links.push(['Delete', [@investigation, @invest_vehicle], {method: 'delete', data: {confirm: confirm_msg_for(:invest_vehicle)}}])
    end
    
    @page_links.push([@investigation,'invest'],[@invest_vehicle.registered_owner,'owned by'],[@invest_vehicle.creator,'creator'],[@invest_vehicle.updater,'updater'])
  end
  
  def new
    can!(:update_investigation)

    @invest_vehicle = @investigation.vehicles.build
    @page_title = "New Investigation Vehicle"
  end
  
  def edit
    can!(:update_investigation)

    @invest_vehicle = @investigation.vehicles.find(params[:id])

    enforce_privacy(@invest_vehicle)
    enforce_author(@invest_vehicle)

    @page_title = "Edit Investigation Vehicle #{@invest_vehicle.id}"
  end
  
  def create
    redirect_to @investigation if params[:commit] == 'Cancel'
    
    can!(:update_investigation)
    
    @invest_vehicle = @investigation.vehicles.build(invest_vehicle_params)
    @page_title = "New Investigation Vehicle"
    @invest_vehicle.private = @investigation.private?
    @invest_vehicle.owned_by = @investigation.owned_by
    @invest_vehicle.created_by = session[:user]
    @invest_vehicle.updated_by = session[:user]
    
    success = @invest_vehicle.save
    user_action "Invest Vehicle ##{@invest_vehicle.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@investigation, @invest_vehicle], notice: 'Invest Vehicle was successfully created.' }
        format.json { render :show, status: :created, location: [@investigation, @invest_vehicle] }
      else
        format.html { render :new }
        format.json { render json: @invest_vehicle.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_investigation)
    
    @invest_vehicle = @investigation.vehicles.find(params[:id])
    
    redirect_to [@investigation, @invest_vehicle] and return if params[:commit] == 'Cancel'
    
    enforce_privacy(@invest_vehicle)
    enforce_author(@invest_vehicle)
    
    @page_title = "Edit Investigation Vehicle #{@invest_vehicle.id}"
    @invest_vehicle.updated_by = session[:user]
    params[:invest_vehicle][:linked_crimes] ||= []
    params[:invest_vehicle][:linked_criminals] ||= []
    params[:invest_vehicle][:linked_photos] ||= []
    
    success = @invest_vehicle.update(invest_vehicle_params)
    user_action "Invest Vehicle ##{@invest_vehicle.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@investigation, @invest_vehicle], notice: 'Invest Vehicle was successfully updated.' }
        format.json { render :show, status: :ok, location: [@investigation, @invest_vehicle] }
      else
        format.html { render :edit }
        format.json { render json: @invest_vehicle.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:update_investigation)
    
    @invest_vehicle = @investigation.vehicles.find(params[:id])
    
    enforce_privacy(@invest_vehicle)
    enforce_author(@invest_vehicle)
    
    @invest_vehicle.destroy
    user_action "Invest Vehicle ##{@invest_vehicle.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to @investigation, notice: 'Invest Vehicle was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  protected

  def load_investigation
    @investigation = Investigation.find(params[:investigation_id])
    enforce_privacy(@investigation)
  end
  
  def invest_vehicle_params
    params.require(:invest_vehicle).permit(:owner_id, :vehicle_type, :color, :make, :model, :model_year, :vin, :license, :license_state,
      :tires, :description, :remarks, linked_crimes: [], linked_criminals: [], linked_photos: []
    )
  end
end