class CitationsController < ApplicationController

  def index
    options = {
      scopes: [:by_reverse_date],
      includes: [:person, :charges, :offenses],
      paginate: true
    }
    @citations = get_records(options)

    if can?(:update_citation)
      @links.push(['New', new_citation_path])
    end

    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    if params[:info_id]
      @info_id = params[:info_id]
      @citation = Citation.find_by_id(params[:id])
    else
      @citation = Citation.find(params[:id])
      @page_title = "Citation #{@citation.id}"
      
      if can?(:update_citation)
        @links.push(['New', new_citation_path])
        @links.push(['Edit', edit_citation_path(@citation)])
      end
      if can?(:destroy_citation)
        @links.push(['Delete', @citation, {method: 'delete', data: {confirm: confirm_msg_for(:citation)}}])
      end
      
      @page_links.push(@citation.person,[@citation.creator,'creator'],[@citation.updater,'updater'])
    end
    
    respond_to :html, :json, :js
  end

  def new
    can!(:update_citation)
    
    @citation = Citation.new
    @page_title = "New Citation"
    if params[:person_id]
      @citation.person_id = params[:person_id]
    end
    if params[:offense_id]
      @citation.offense_id = params[:offense_id]
      unless @citation.offense.nil?
        @citation.citation_datetime = @citation.offense.offense_datetime
        @citation.officer = @citation.offense.officer
        @citation.officer_unit = @citation.offense.officer_unit
        @citation.officer_badge = @citation.offense.officer_badge
      end
    else
      set_officer_to_session(:officer)
    end
    @citation.charges.build
  end
    
  def edit
    can!(:update_citation)

    @citation = Citation.find(params[:id])
    @page_title = "Edit Citation #{@citation.id}"
    @citation.officer_id = nil
  end
    
  def create
    can!(:update_citation)

    redirect_to citations_url and return if params[:commit] == 'Cancel'

    @citation = Citation.new(citation_params)
    @page_title = "New Citation"
    @citation.created_by = session[:user]
    @citation.updated_by = session[:user]

    success = @citation.save
    user_action "Citation ##{@citation.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @citation, notice: 'Citation was successfully created.' }
        format.json { render :show, status: :created, location: @citation }
      else
        format.html { render :new }
        format.json { render json: @citation.errors, status: :unprocessable_entity }
      end
    end
  end
    
  def update
    can!(:update_citation)

    @citation = Citation.find(params[:id])

    redirect_to @citation and return if params[:commit] == 'Cancel'

    @page_title = "Edit Citation #{@citation.id}"
    @citation.updated_by = session[:user]
    
    success = @citation.update(citation_params)
    user_action "Citation ##{@citation.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @citation, notice: 'Citation was successfully updated.' }
        format.json { render :show, status: :ok, location: @citation }
      else
        format.html { render :edit }
        format.json { render json: @citation.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_citation)

    @citation = Citation.find(params[:id])

    if reasons = @citation.reasons_not_destroyable
      flash.alert = "Cannot delete: #{reasons}"
      redirect_to @citation and return
    end
    
    @citation.destroy
    user_action "Citation ##{@citation.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to citations_url, notice: 'Citation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  
  def citation_params
    params.require(:citation).permit(:person_id, :citation_datetime, :officer_id, :officer_badge, :officer_unit, :officer, :ticket_no,
      :paid_date, :recalled_date, charges_attributes: [:id, :charge_type, :count, :charge, :agency_id, :agency, :_destroy])
  end
end
