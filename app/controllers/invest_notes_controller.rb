class InvestNotesController < ApplicationController
  before_action :get_objects
  
  # no index
  
  # no show
  
  def new
    @invest_note = @parent.notes.build
    
    enforce_author(@parent)
    
    @page_title = "New #{@parent.class.name} Note"
  end
  
  def edit
    raise ActiveRecord::RecordNotFound.new("Cannot edit note without note id") if @invest_note.nil?
    
    enforce_author(@invest_note)
    enforce_privacy(@invest_note)
    
    @page_title = "Edit #{@parent.class.name} Note"
  end
  
  def create
    redirect_to @object and return if params[:commit] == 'Cancel'
    
    @invest_note = InvestNote.new(invest_note_params)
    @page_title = "New #{@parent.class.name} Note"
    @invest_note.created_by = session[:user]
    @invest_note.updated_by = session[:user]
    
    success = @invest_note.save
    user_action("Invest Note ##{@invest_note.id} Created", section: @parent.class.name.titleize.pluralize) if success

    respond_to do |format|
      if success
        format.html { redirect_to @object, notice: 'Invest Note was successfully created.' }
        format.json { render :show, status: :created, location: [@object, @invest_note].flatten }
      else
        format.html { render :new }
        format.json { render json: @invest_note.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    raise ActiveRecord::RecordNotFound.new("Cannot edit note without note id") if @invest_note.nil?

    enforce_author(@invest_note)
    enforce_privacy(@invest_note)

    @page_title = "Edit #{@parent.class.name} Note"
    @invest_note.updated_by = session[:user]
    
    success = @invest_note.update(invest_note_params)
    user_action("Invest Note ##{@invest_note.id} Updated", section: @parent.class.name.titleize.pluralize) if success

    respond_to do |format|
      if success
        format.html { redirect_to @object, notice: 'Invest Note was successfully updated.' }
        format.json { render :show, status: :ok, location: [@object, @invest_note].flatten }
      else
        format.html { render :edit }
        format.json { render json: @invest_note.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    raise ActiveRecord::RecordNotFound.new("Cannot edit note without note id") if @invest_note.nil?
    
    enforce_author(@invest_note)
    enforce_privacy(@invest_note)
    
    @invest_note.destroy
    user_action "Invest Note ##{@invest_note.id} Deleted.", section: @parent.class.name.titleize.pluralize
    
    respond_to do |format|
      format.html { redirect_to @object, notice: 'Invest Note was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  
  def get_objects
    if params[:id]
      @invest_note = InvestNote.find(params[:id])
      @parent = @invest_note.invest_record
    elsif params[:invest_crime_id]
      @parent = InvestCrime.find(params[:invest_crime_id])
    elsif params[:invest_criminal_id]
      @parent = InvestCriminal.find(params[:invest_criminal_id])
    elsif params[:invest_photo_id]
      @parent = InvestPhoto.find(params[:invest_photo_id])
    elsif params[:invest_report_id]
      @parent = InvestReport.find(params[:invest_report_id])
    elsif params[:invest_vehicle_id]
      @parent = InvestVehicle.find(params[:invest_vehicle_id])
    end
    if params[:investigation_id]
      @investigation = Investigation.find(params[:investigation_id])
      if @parent
        @object = [@investigation, @parent]
      else
        @parent = @investigation
        @object = @investigation
      end
    end
    enforce_privacy(@parent)
  end
  
  def invest_note_params
    params.require(:invest_note).permit(:invest_crime_id, :invest_criminal_id, :invest_photo_id, :invest_report_id, :invest_vehicle_id,
      :investigation_id, :note
    )
  end
end