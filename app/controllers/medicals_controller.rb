class MedicalsController < ApplicationController
  before_action :load_booking
  
  # no index view
  
  def show
    @medical = @booking.medicals.find(params[:id])
    @page_title = "Medical Screening #{@medical.id}"
    
    if can?(:update_booking)
      @links.push(['New', new_jail_booking_medical_path(@jail,@booking)])
      @links.push(['Edit', edit_jail_booking_medical_path(@jail,@booking,@medical)])
      @links.push(['Delete', [@jail,@booking,@medical], {method: :delete, data: {confirm: confirm_msg_for(:medical_screening)}}])
      @links.push(['Medical Screen', medical_form_jail_booking_medical_path(@jail,@booking,@medical), pdf: true])
    end

    @page_links.push([@jail, @medical.booking], @medical.booking.person)
  end
  
  def new
    can!(:update_booking, @jail.update_permission)

    @medical = @booking.medicals.build
    set_officer_to_session(@medical, :officer)
    @medical.screen_datetime = Time.now
    @page_title = "New Medical Screening"
  end
    
  def edit
    can!(:update_booking, @jail.update_permission)

    @medical = @booking.medicals.find(params[:id])
    @page_title = "New Medical Screening"
  end
  
  def create
    redirect_to [@jail, @booking] and return if params[:commit] == 'Cancel'

    can!(:update_booking, @jail.update_permission)

    @medical = @booking.medicals.build(medical_params)
    @medical.created_by = session[:user]
    @medical.updated_by = session[:user]
    @page_title = "New Medical Screening"
    
    success = @medical.save
    user_action "Medical ##{@medical.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@jail, @booking, @medical], notice: 'Medical was successfully created.' }
        format.json { render :show, status: :created, location: [@jail, @booking, @medical] }
      else
        format.html { render :new }
        format.json { render json: @medical.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_booking, @jail.update_permission)
    
    @medical = @booking.medicals.find(params[:id])
    
    redirect_to [@jail, @booking, @medical] and return if params[:commit] == 'Cancel'
    
    @page_title = "New Medical Screening"
    @medical.updated_by = session[:user]
    
    success = @medical.update(medical_params)
    user_action "Medical ##{@medical.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@jail, @booking, @medical], notice: 'Medical was successfully updated.' }
        format.json { render :show, status: :ok, location: [@jail, @booking, @medical] }
      else
        format.html { render :edit }
        format.json { render json: @medical.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:update_booking, @jail.update_permission)
    
    @medical = @booking.medicals.find(params[:id])
    
    @medical.destroy
    user_action "Medical ##{@medical.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to [@jail, @booking], notice: 'Medical was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def medical_form
    @medical = @booking.medicals.find(params[:id])
    
    if pdf_url = make_pdf_file("medical", Medicals::Form.new(@medical))
      redirect_to pdf_url
      user_action("Printed Medical Screening")
    else
      flash.alert = "Could not create PDF file for download!"
      redirect_to [@jail, @booking, @medical]
    end
  end
  
  protected
  
  def load_booking
    @jail = Jail.find(params[:jail_id])
    can!(@jail.view_permission)
    @booking = @jail.bookings.find(params[:booking_id])
  end
  
  def medical_params
    params.require(:medical).permit(:screen_datetime, :officer_id, :officer_badge, :officer_unit, :officer, :arrest_injuries, :rec_med_tmnt,
      :rec_dental_tmnt, :rec_mental_tmnt, :current_meds, :allergies, :injuries, :infestation, :alcohol, :drug, :withdrawal, :attempt_suicide,
      :suicide_risk, :danger, :pregnant, :deformities, :heart_disease, :blood_pressure, :diabetes, :epilepsy, :hepatitis, :hiv, :tb,
      :ulcers, :venereal, :disposition
    )
  end
end