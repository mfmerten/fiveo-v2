class CourtTypesController < ApplicationController

  def index
    options = {
      scopes: [:by_name],
      paginate: true
    }
    @court_types = get_records(options)
    
    if can?(:update_option)
      @links.push(['New', new_court_type_path])
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @court_type = CourtType.find(params[:id])
    @page_title = "Court Type #{@court_type.id}"
    
    if can?(:update_option)
      @links.push(['New', new_court_type_path])
      @links.push(['Edit', edit_court_type_path(@court_type)])
      @links.push(['Delete', @court_type, {method: 'delete', data: {confirm: confirm_msg_for(:court_type)}}])
    end

    @page_links.push([@court_type.creator,'creator'],[@court_type.updater,'updater'])
  end

  def new
    can!(:update_option)

    @court_type = CourtType.new
    @page_title = "New Court Type"
  end
  
  def edit
    can!(:update_option)

    @court_type = CourtType.find(params[:id])
    @page_title = "Edit Court Type #{@court_type.id}"
  end
  
  def create
    can!(:update_option)

    redirect_to court_types_url and return if params[:commit] == 'Cancel'

    @court_type = CourtType.new(court_type_params)
    @page_title = "New Court Type"
    @court_type.created_by = session[:user]
    @court_type.updated_by = session[:user]

    success = @court_type.save
    user_action "Court Type ##{@court_type.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @court_type, notice: 'Court Type was successfully created.' }
        format.json { render :show, status: :created, location: @court_type }
      else
        format.html { render :new }
        format.json { render json: @court_type.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_option)
    
    @court_type = CourtType.find(params[:id])
    
    redirect_to @court_type and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Court Type #{@court_type.id}"
    @court_type.updated_by = session[:user]
    
    success = @court_type.update(court_type_params)
    user_action "Court Type ##{@court_type.id} Updated." if success

    respond_to do |format|
      if success
        format.html { redirect_to @court_type, notice: 'Court Type was successfully updated.' }
        format.json { render :show, status: :ok, location: @court_type }
      else
        format.html { render :edit }
        format.json { render json: @court_type.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:update_option)
    
    @court_type = CourtType.find(params[:id])
    
    @court_type.destroy
    user_action "Court Type ##{@court_type.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to court_types_url, notice: 'Court Type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
  
  def court_type_params
    params.require(:court_type).permit(:name)
  end
end