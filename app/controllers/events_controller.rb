class EventsController < ApplicationController

  def index
    options = {
      scopes: [:by_date],
      includes:[creator: :contact],
      active: true,
      paginate: true
    }
    @events = get_records(options)

    if can?(:update_event)
      @links.push(["New", new_event_path])
    end

    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @event = Event.find(params[:id])
    @page_title = "Event #{@event.id}"
    
    enforce_privacy(@event)
    
    if can?(:update_event)
      @links.push(["New", new_event_path])
      @links.push(["Edit", edit_event_path(@event)])
    end
    if can?(:destroy_event)
      @links.push(["Delete", @event, {method: 'delete', data: {confirm: confirm_msg_for(:event)}}])
    end

    @page_links.push([@event.investigation,'invest'],[@event.owner,'Owner'],[@event.creator,'creator'],[@event.updater,'updater'])
  end
  
  def new
    can!(:update_event)

    @event = Event.new
    @event.start_date = AppUtils.valid_date(params[:start_date]) || Date.today
    @page_title = "New Event"
  end

  def edit
    can!(:update_event)

    @event = Event.find(params[:id])

    enforce_privacy(@event)

    @page_title = "Edit Event #{@event.id}"
  end
  
  def create
    if params[:commit] == 'Cancel'
      if !params[:start_date].blank?
        redirect_to root_url
      else
        redirect_to events_url
      end
      return
    end
    
    can!(:update_event)
    
    @event = Event.new(event_params)
    @page_title = "New Event"
    @event.created_by = session[:user]
    @event.updated_by = session[:user]
    @event.owned_by = session[:user]
    
    investigation = nil
    success = false
    ret_loc = @event
    if @event.save
      success = true
      user_action "Event ##{@event.id} Created."
      
      if !params[:start_date].blank?
        ret_loc = root_url
      elsif params[:investigation_id] && investigation = Investigation.find_by_id(params[:investigation_id])
        @event.update_attribute(:investigation_id, investigation.id)
        @event.update_attribute(:private, investigation.private?)
        @event.update_attribute(:owned_by, investigation.owned_by)
        ret_loc = investigation and return
      end
    end
        
    respond_to do |format|
      if success
        format.html { redirect_to ret_loc, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_event)
    
    @event = Event.find(params[:id])
    
    redirect_to @event and return if params[:commit] == 'Cancel'
    
    enforce_privacy(@event)
    
    @page_title = "Edit Event #{@event.id}"
    @event.updated_by = session[:user]
    @event.owned_by = session[:user]
    
    success = @event.update(event_params)
    user_action "Event ##{@event.id} Update." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def destroy
    can!(:destroy_event)
    
    @event = Event.find(params[:id])
    
    enforce_privacy(@event)
    
    @event.destroy
    user_action "Event ##{@event.id} Delete."

    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  
  def event_params
    params.require(:event).permit(:start_date, :end_date, :name, :private, :details)
  end
end
