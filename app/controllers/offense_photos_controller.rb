class OffensePhotosController < ApplicationController
  before_action :load_offense
  
  # no index
  
  def show
    @offense_photo = @offense.offense_photos.find(params[:id])
    @page_title = "Offense Photo #{@offense_photo.id}"
    
    if can?(:update_offense)
      @links.push(["New", new_offense_offense_photo_path(@offense)])
      @links.push(["Edit", edit_offense_offense_photo_path(@offense, @offense_photo)])
    end
    if can?(:destroy_offense)
      @links.push(['Delete', [@offense, @offense_photo], {method: 'delete', data: {confirm: confirm_msg_for(:offense_photo)}}])
    end
    if can?(:update_offense)
      @links.push(['Photo', photo_offense_offense_photo_path(@offense, @offense_photo)])
    end

    @page_links.push(@offense_photo.offense,[@offense_photo.creator, 'creator'],[@offense_photo.updater, 'updater'])
  end
  
  def new
    can!(:update_offense)

    @page_title = "New Offense Photo"
    @offense_photo = @offense.offense_photos.build(case_no: @offense.case_no)
    set_officer_to_session(@offense_photo, :taken_by)
  end
  
  def edit
    can!(:update_offense)

    @offense_photo = @offense.offense_photos.find(params[:id])
    @page_title = "Edit Offense Photo #{@offense_photo.id}"
    @offense_photo.taken_by_id = nil
  end
  
  def create
    redirect_to @offense and return if params[:commit] == 'Cancel'

    can!(:update_offense)

    @page_title = "New Offense Photo"
    @offense_photo = @offense.offense_photos.build(offense_photo_params)
    @offense_photo.created_by = session[:user]
    @offense_photo.updated_by = session[:user]

    success = @offense_photo.save
    user_action "Offense Photo ##{@offense_photo.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@offense, @offense_photo], notice: 'Offense Photo was successfully created.' }
        format.json { render :show, status: :created, location: [@offense, @offense_photo] }
      else
        format.html { render :new }
        format.json { render json: @offense_photo.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_offense)
    
    @offense_photo = @offense.offense_photos.find(params[:id])
    
    redirect_to [@offense, @offense_photo] and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Offense Photo #{@offense_photo.id}"
    @offense_photo.updated_by = session[:user]
    
    # remove photo if requested
    if params[:commit] == 'Delete'
      @offense_photo.crime_scene_photo = nil
    end
    
    success = false
    if @offense_photo.update(offense_photo_params)
      success = true
      if params[:commit] == 'Delete'
        user_action("Removed Photo for Offense Photo #{@offense_photo.id}.")
      else
        user_action "Offense Photo ##{@offense_photo.id} Updated."
      end
    end
    
    respond_to do |format|
      if success
        format.html { redirect_to [@offense, @offense_photo], notice: 'Offense Photo was successfully updated.' }
        format.json { render :show, status: :ok, location: [@offense, @offense_photo] }
      else
        if params[:offense_photo][:photo_form] == '1'
          format.html { render :photo }
        else
          format.html { render :edit }
        end
        format.json { render json: @offense_photo.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def photo
    can!(:update_offense)
    
    @offense_photo = @offense.offense_photos.find(params[:id])
    @page_title = "Offense Photo"
  end
  
  def destroy
    can!(:update_offense)
    
    @offense_photo = @offense.offense_photos.find(params[:id])
    
    @offense_photo.destroy
    user_action "Offense Photo ##{@offense_photo.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to @offense, notice: 'Offense Photo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  protected
  
  def load_offense
    @offense = Offense.find(params[:offense_id])
  end
  
  def offense_photo_params
    params.require(:offense_photo).permit(:crime_scene_photo, :offense_id, :case_no, :date_taken, :taken_by_id, :taken_by_badge,
      :taken_by_unit, :taken_by, :location_taken, :description, :remarks, :photo_form
    )
  end
end
