class CourtsController < ApplicationController
  before_action :load_docket

  # no index action

  def show
    @court = @docket.courts.find(params[:id])
    @page_title = "Court #{@court.id}"
    
    if can?(:update_court)
      @links.push(['New', new_docket_court_path(@docket)])
      @links.push(['Edit', edit_docket_court_path(@docket, @court)])
    end
    if can?(:destroy_court)
      @links.push(['Delete', [@docket, @court], {method: 'delete', data: {confirm: confirm_msg_for(:court)}}])
    end
    unless @docket.misc?
      if can?(:update_court)
        @links.push(['New Revocation', new_docket_revocation_path(@court.docket)])
      end
      @links.push(['Sent. Sheets', worksheets_docket_court_path(@docket, @court, type: 'sentence'), pdf: true])
      @links.push(['Revoc. Sheet', worksheets_docket_court_path(@docket, @court, type: 'revocation'), pdf: true])
    end
    
    @page_links.push(@docket.person, @docket)
    @page_links.push([@court.creator,'creator'],[@court.updater,'updater'])
  end
  
  def new
    can!(:update_court)

    @court = @docket.courts.build
    @page_title = "New Court"
  end
  
  def edit
    can!(:update_court)

    @court = @docket.courts.find(params[:id])
    @page_title = "Edit Court #{@court.id}"
  end
  
  def create
    can!(:update_court)

    redirect_to @docket and return if params[:commit] == 'Cancel'

    @court = @docket.courts.build(court_params)
    @page_title = "New Court"
    @court.created_by = session[:user]
    @court.updated_by = session[:user]

    success = @court.save
    user_action "Court ##{@court.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to [@docket, @court], notice: 'Court was successfully created.' }
        format.json { render :show, status: :created, location: [@docket, @court] }
      else
        format.html { render :new }
        format.json { render json: @court.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_court)

    @court = @docket.courts.find(params[:id])

    redirect_to [@docket, @court] and return if params[:commit] == 'Cancel'

    @page_title = "Edit Court #{@court.id}"
    @court.updated_by = session[:user]

    success = @court.update(court_params)
    user_action "Court ##{@court.id} Updated."
    
    respond_to do |format|
      if success
        format.html { redirect_to [@docket, @court], notice: 'Court was successfully updated.' }
        format.json { render :show, status: :ok, location: [@docket, @court] }
      else
        format.html { render :edit }
        format.json { render json: @court.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can!(:destroy_court)
    
    @court = @docket.courts.find(params[:id])
    
    @court.destroy
    user_action "Court ##{@court.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to @docket, notice: 'Court was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def worksheets
    @court = @docket.courts.find(params[:id])
    valid_types = ["sentence","revocation"]
    unless valid_types.include?(params[:type])
      flash.alert = "Invalid Type Specified!"
      redirect_to [@docket, @court] and return
    end
    
    if params[:type] == 'sentence'
      if pdf_url = make_pdf_file("sentence", Courts::Sentence.new(@court))
        redirect_to pdf_url
        user_action("Printed Sentence Worksheets for Court #{params[:id]}")
      else
        flash.alert = "Could not create PDF file for download!"
        redirect_to [@docket, @court]
      end
    else
      if pdf_url = make_pdf_file("revocation", Courts::Revocation.new(@court))
        redirect_to pdf_url
        user_action("Printed Revocation Worksheet for Court #{params[:id]}")
      else
        flash.alert = "Could not create PDF file for download!"
        redirect_to [@docket, @court]
      end
    end
  end

  protected

  def load_docket
    @docket = Docket.find(params[:docket_id])
  end
  
  def court_params
    params.require(:court).permit(:docket_id, :court_date, :court_type, :judge, :docket_type, :not_present, :bench_warrant_issued,
      :writ_of_attach, :bond_forfeiture_issued, :psi, :is_dismissed, :is_upset_wo_refix, :is_refixed, :refix_reason, :plea_type,
      :fines_paid, :next_court_date, :next_court_type, :next_judge, :next_docket_type, :is_overturned, :overturned_date, :notes, :remarks
    )
  end
end