class GarnishmentsController < ApplicationController
  
  def index
    options = {
      active: true,
      scopes: [:by_name],
      paginate: true
    }
    @garnishments = get_records(options)

    if can?(:update_civil)
      @links.push(['New', new_garnishment_path])
    end

    respond_to do |format|
      format.html
      format.json
      format.js { render 'common/ajax_list' }
    end
  end

  def show
    @garnishment = Garnishment.find(params[:id])
    @page_title = "Garnishment #{@garnishment.id}"
    
    if can?(:update_civil)
      @links.push(['New', new_garnishment_path])
      @links.push(['Edit', edit_garnishment_path(@garnishment)])
    end
    if can?(:destroy_civil)
      @links.push(['Delete', @garnishment, {method: 'delete', data: {confirm: confirm_msg_for(:garnishment)}}])
    end

    @page_links.push([@garnishment.creator,'creator'],[@garnishment.updater,'updater'])
  end

  def new
    can!(:update_civil)

    @garnishment = Garnishment.new
    @page_title = "New Garnishment"
    @garnishment.received_date = Date.today
  end
  
  def edit
    can!(:update_civil)

    @garnishment = Garnishment.find(params[:id])
    @page_title = "Edit Garnishment #{@garnishment.id}"
  end
  
  def create
    redirect_to garnishments_url and return if params[:commit] == 'Cancel'

    can!(:update_civil)

    @garnishment = Garnishment.new(garnishment_params)
    @page_title = "New Garnishment"
    @garnishment.created_by = session[:user]
    @garnishment.updated_by = session[:user]

    success = @garnishment.save
    user_action "Garnishment ##{@garnishment.id} Created." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @garnishment, notice: 'Garnishment was successfully created.' }
        format.json { render :show, status: :created, location: @garnishment }
      else
        format.html { render :new }
        format.json { render json: @garnishment.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    can!(:update_civil)
    
    @garnishment = Garnishment.find(params[:id])
    
    redirect_to @garnishment and return if params[:commit] == 'Cancel'
    
    @page_title = "Edit Garnishment #{@garnishment.id}"
    @garnishment.updated_by = session[:user]
    
    success = @garnishment.update(garnishment_params)
    user_action "Garnishment ##{@garnishment.id} Updated." if success
    
    respond_to do |format|
      if success
        format.html { redirect_to @garnishment, notice: 'Garnishment was successfully updated.' }
        format.json { render :show, status: :ok, location: @garnishment }
      else
        format.html { render :edit }
        format.json { render json: @garnishment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    can! :destroy_civil
    
    @garnishment = Garnishment.find(params[:id])
    
    @garnishment.destroy
    user_action "Garnishment ##{@garnishment.id} Deleted."
    
    respond_to do |format|
      format.html { redirect_to garnishments_url, notice: 'Garnishment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  
  def garnishment_params
    params.require(:garnishment).permit(:sort_name, :suit_no, :received_date, :description, :attorney, :satisfied_date, :details, :remarks)
  end
end