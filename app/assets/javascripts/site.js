// site javascript file

String.prototype.capitalize = function(){
    return this.toLowerCase().replace( /(^|\s)([a-z])/g , function(m,p1,p2){ return p1+p2.toUpperCase(); } );
};

var newwindow = '';

// jquery mask configuration
$(document).ready(function() {
  $('.date_mask').mask('00/00/0000', {placeholder: "__/__/____"});
  $('.date_time_mask').mask('00/00/0000 00:00', {placeholder: "__/__/____ __:__"});
  $('.phone_mask').mask('000-000-0000', {placeholder: "___-___-____"});
  $('.ssn_mask').mask('000-00-0000', {placeholder: "___-__-____"});
  $('.time_mask').mask('00:00', {placeholder: "__:__"});
});

// looks up addresses on yahoo maps and displays in a new window
function lookupMapQuest(street,city,state,agency_street,agency_city,agency_state) {
  var c1 = encodeURIComponent(agency_city);
  var s1 = encodeURIComponent(agency_state);
  var a1 = encodeURIComponent(agency_street);
  var c2 = encodeURIComponent(city);
  var s2 = encodeURIComponent(state);
  var a2 = encodeURIComponent(street);
  var url='http://mapquest.com/maps?1c=' + c1 + '&1s=' + s1 + '&1a=' + a1 + '&2c=' + c2 + '&2s=' + s2 + '&2a=' + a2;
  window.open(url);
  return false;
}

function getCalendar(url, period) {
  var adjusted_url = url + '.js?period=' + encodeURIComponent(period);
  jQuery.ajax(adjusted_url);
  return false;
}

// looks for the first element in a form that has class 'preview', grabs
// the content and passes it to root controller to display in a popup as textile
function displayPreviewPopup(url, caller) {
  var preview_element = caller.up('form').down('.preview');
  if (preview_element != undefined) {
    var path = url + '/preview_popup?value=' + encodeURIComponent(preview_element.val());
    window.open(path,'Preview','width=1024,height=500,menubar=no,status=no,toolbar=no,scrollbars=yes,location=no');
  }
  return false;
}

// Switches the filter from advanced (filter style) to simple (search style)
function selectSimpleFilter() {
  $('#simple-search').show();
  $('#advanced-search').hide();
  return false;
}

// Switches the filter from simple (search style) to advanced (filter style)
function selectAdvancedFilter() {
  $('#simple-search').hide();
  $('#advanced-search').show();
  return false;
}

// Displays form information
function showFormInformation() {
  $('#info-shown').show();
  $('#hide-link').show();
  $('#show-link').hide();
  return false;
}

// Hides form information
function hideFormInformation() {
  $('#info-shown').hide();
  $('#hide-link').hide();
  $('#show-link').show();
  return false;
}

// functions for partials
function remove_fields(link) {
  $(link).previous("input[type=hidden]").val("1");
  $(link).up(".fields").hide();
}

function add_fields(association, content) {
  var content_div = document.getElementById(association);
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g")
  content_div.append(content.replace(regexp, new_id));
}

// used on contact form (cedit) to show or hide bonding companies depending
// on whether bondsman is checked or not.
function toggleBondCompanies(value) {
  if (value == true) {
    $('#bond-company').show();
  }
  else {
    $('#bond-company').hide();
  }
  return false;
}

// used to validate various ids entered on forms
// type is determined by supplied validate_url
// specify:
//    1. url for validation (usually list url for model to validate, i.e. people_path)
//    2. element containing model ID to be validated (caller)
//    3. people only, pass true to third argument to skip "real identity" lookup
// expects:
//    * set of fields to be enclosed in a div with class='fields' or class='view-data'
//    * element to be updated having a class='info_id'
//    * show view responding to js format in controller specified in url
//    * optional for people, a text field with class='person-name'
function validateInfo(validate_url, caller, alias_allowed) {
  var id_id = caller.uniqueId().attr('id');
  var enclosure = caller.up('.fields');
  var alias_ok = false;
  if (alias_allowed == true) alias_ok = true;
  if (enclosure == undefined){
    enclosure = caller.up('.view-data');
  }
  if (enclosure != undefined){
    var info_field = enclosure.down('.info_id');
    var info_id = '';
    if (info_field != undefined){
      info_id = enclosure.down('.info_id').uniqueId().attr('id');
    }
    var record_id = caller.val();
    if (record_id != '' && record_id != undefined) {
      var path = encodeURI(validate_url) + '/' + encodeURIComponent(record_id) + '.js?info_id=' + encodeURIComponent(info_id) + '&id_id=' + encodeURIComponent(id_id);
      // handle person name field
      var name_element = enclosure.down('.person-name');
      if (name_element != undefined) {
        name_id = name_element.uniqueId().attr('id');
        path = path + '&name_id=' + encodeURIComponent(name_id);
        if (alias_ok == true) {
          path = path + '&alias_ok=1';
        }
      }
      // make the call
      jQuery.ajax(path);
    }
  }
  return false;
}

// used to validate signal codes by code
// specify
//    1. url to validate (validate_signal_codes_path),
//    2. element containing code to validate (caller)
//    3. id of field receiving the signal name (receiver)
//    3. id of element to display the name (display)
function validateCode(validate_url, caller, receiver, display) {
  caller_id = caller.uniqueId().attr('id');
  code = caller.val();
  var path = encodeURI(validate_url) + '.js?code=' + encodeURIComponent(code) + '&code_id=' + encodeURIComponent(caller_id) + '&name_id=' + encodeURIComponent(receiver) + '&display_id=' + encodeURIComponent(display);
  jQuery.ajax(path);
  return false;
}

// when a signal code is clicked on the signals popup, this inserts the selected
// information into the call sheet.
function selectSignal(code, name) {
  window.opener.document.getElementById('call_signal_code').val(code);
  window.opener.document.getElementById('call_signal_name').val(name);
  window.opener.document.getElementById('info_id').html(name);
  window.close();
  return false;
}

// used on custom builder contact fields to disable (and clear) the type-in
// fields whenever a contact is chosen with the select-control.
function contactFieldToggle(select_id,name_id,unit_id,badge_id) {
  if($F(select_id) != '') {
     $(name_id).clear();
     $(name_id).prop('disabled', true);
     if(unit_id != '') {
       $(unit_id).clear();
       $(unit_id).prop('disabled', true);
     }
     if(badge_id != '') {
       $(badge_id).clear();
       $(badge_id).prop('disabled', true);
     }
  }
  else {
     $(name_id).prop('disabled', false);
     if(unit_id != '') $(unit_id).prop('disabled', false);
     if(badge_id != '') $(badge_id).prop('disabled', false);
  }
}

// used on charge partials to enable/disable agency field
function updateChargeAgency(field_id) {
  var charge_type = $(field_id).val();
  var city_span = $(field_id).up('.fields').down('.city');
  var city_select = city_span.down('.agency-select');
  var city_name = city_span.down('.agency-name');
  var other_span = $(field_id).up('.fields').down('.other');
  var other_select = other_span.down('.agency-select');
  var other_name = other_span.down('.agency-name');
  if (charge_type == 'District') {
    city_span.hide();
    city_select.prop('disabled', true);
    city_name.prop('disabled', true);
    other_span.hide();
    other_select.prop('disabled', true);
    other_name.prop('disabled', true);
  }
  if (charge_type == 'City') {
    city_span.show();
    city_select.prop('disabled', false);
    city_name.prop('disabled', false);
    other_span.hide();
    other_select.prop('disabled', true);
    other_name.prop('disabled', true);
  }
  if (charge_type == 'Other') {
    other_span.show();
    other_select.prop('disabled', false);
    other_name.prop('disabled', false);
    city_span.hide();
    city_select.prop('disabled', true);
    city_name.prop('disabled', true);
  }
  return false;
}

// used by custom form builder charge_field
function chargeLookup(lookup_url, lookup) {
  var adjusted_url = lookup_url + '?u=' + encodeURIComponent(lookup.val()) + '&update=' + encodeURIComponent(lookup.uniqueId().attr('id'));
  new jQuery.ajax(adjusted_url);
  return false;
}

// looks up a person by name
// used by person_field method in CustomFormBuilder among others
// fields must be enclosed by div with class of 'fields' or 'view-data'
function personChoose(lookup_url,button) {
  var enclosure = button.up('.fields');
  if (enclosure == undefined){
    enclosure = button.up('.view-data');
  }
  if (enclosure != undefined){
    var id_id = enclosure.down('.person-id').uniqueId().attr('id');
    var name_element = enclosure.down('.person-name')
    var name_id = name_element.uniqueId().attr('id');
    var lookup_name = name_element.val();
    var adjusted_url = lookup_url + '?name=' + encodeURIComponent(lookup_name) + '&id_field=' + encodeURIComponent(id_id) + '&name_field=' + encodeURIComponent(name_id);
    jQuery.ajax(adjusted_url);
  }
  return false;
}

// inserts person id and name when picked from a list
// called from the pick_list view.
function personChosen(person_id,id_field) {
  id_element = window.opener.document.getElementById(id_field);
  id_element.val(person_id);
  id_element.onblur();
  enclosure = id_element.up('.fields');
  if (enclosure == undefined){
    enclosure = id_element.up('.view-data');
  }
  enclosure.down('.person-name').focus();
  window.close();
  return false;
}
