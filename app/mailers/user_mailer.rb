# handles all user email
class UserMailer < ActionMailer::Base
  # email notification of new messages
  def message_email(message)
    @message = message
    fromname = 'monitor@midlaketech.com'
    unless SiteConfig.site_email.blank?
      fromname = SiteConfig.site_email
    end
    mail(to: message.receiver.contact.pri_email, subject: "You have a new message!", from: fromname)
  end
end
