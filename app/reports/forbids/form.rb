module Forbids
  class Form < ReportBase
    def initialize(forbid)
      super
      @forbid = forbid
      unless @forbid.is_a?(Forbid)
        raise ArgumentError.new("Forbids::Form.initialize: First argument must be a valid Forbid object.")
      end
    end
  
    def to_pdf
      standard_report(footer_no: @forbid.id.to_s) do
        text("Entry After Being Forbidden Notice", align: :center, size: 18, style: :bold)
        move_down(2)
        text(AppUtils.format_date(@forbid.request_date), size: 14, style: :bold, align: :center)
        text("Case Number: <b>#{@forbid.case_no? ? @forbid.case_no : 'Not On File'}</b>", inline_format: true, size: 14, align: :center)
        data = [
          ["FROM:","#{@forbid.complainant}","TO:","#{@forbid.sort_name}"],
          ["","#{@forbid.comp_street}","","#{@forbid.forbid_street}"],
          ["","#{@forbid.comp_city_state_zip}","","#{@forbid.forbid_city_state_zip}"]
        ]
        cols = (bounds.width / 6).to_i - 1
        table(data, header: :false) do
          columns(0..-1).style(borders: [], align: :left, padding: 1)
          column(0).style(width: cols, align: :right, font_style: :bold)
          column(1).style(width: cols * 2, padding_left: 5)
          column(2).style(width: cols, align: :right, font_style: :bold)
          column(3).style(width: cols * 2, padding_left: 5)
        end
        pad(10){ stroke_horizontal_rule }
        text("This letter is to serve as your one and only notice from <b>#{@forbid.complainant.upcase}</b> that from hence forth, you are forbidden to go into or upon or remain in or upon the property(ies) and/or building(s) of <b>#{@forbid.complainant.upcase}</b> located at:", inline_format: true, size: 14)
        move_down(10)
        indent(25,25) do
          text("#{@forbid.details}", size: 14)
        end
        move_down(10)
        text("This is pursuant to LA - R.S. 14:63.3 ENTRY ON OR REMAINING IN PLACES OR ON LAND AFTER BEING FORBIDDEN. Failure of the unwanted person to comply with this written notice will result in the complainant, listed herein, seeking a warrant of arrest for the unwanted person along with any other charges should he/she choose to ignore this notice and return to the above listed property(ies) and/or building(s).", size: 14)
        move_down(40)
        y_position = cursor
        signature_line("Complainant's Signature", placement: :left)
        move_cursor_to(y_position)
        signature_line("Unwanted Person's Signature")
        move_down(40)
        y_position = cursor
        signature_line("Signature of Deputy Serving Notice", placement: :left)
        move_cursor_to(y_position)
        signature_line("Date and Time Notice Served On Above")
      end
    end
  end
end