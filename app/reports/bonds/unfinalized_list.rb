module Bonds
  class UnfinalizedList < ReportBase
    def initialize(unfinalized)
      super(page_layout: :landscape)
      @unfinalized = unfinalized
      unless @unfinalized.is_a?(Hash)
        raise ArgumentError.new("Bonds::UnfinalizedList.initialize: First argument must be an Hash of Bond collections.")
      end
      @unfinalized[:probation].each do |b|
        unless b.is_a?(Bond)
          raise ArgumentError.new("Bonds::UnfinalizedList.initialize: First argument must be an Hash of Bond collections: supplied collection contains invalid object type(s)!")
        end
      end
      @unfinalized[:district].each do |b|
        unless b.is_a?(Bond)
          raise ArgumentError.new("Bonds::UnfinalizedList.initialize: First argument must be an Hash of Bond collections: supplied collection contains invalid object type(s)!")
        end
      end
      @unfinalized[:no_holds].each do |b|
        unless b.is_a?(Bond)
          raise ArgumentError.new("Bonds::UnfinalizedList.initialize: First argument must be an Hash of Bond collections: supplied collection contains invalid object type(s)!")
        end
      end
    end
    
    def to_pdf
      # note: special page orientation, layout and header/footer
      repeat(:all) do
        bounding_box([margin_box.left, margin_box.top - 10], width: margin_box.width, height: 30) do
          font("Helvetica")
          text("Unfinalized Bond List", align: :center, size: 18, style: :bold)
          move_up(16)
          text(Date.today.to_s(:us), size: 14, style: :bold)
          stroke_horizontal_rule
          font("Times-Roman")
        end
        bounding_box([margin_box.left, margin_box.bottom + 20], width: margin_box.width, height: 20) do
          font("Helvetica")
          stroke_horizontal_rule
          move_down(5)
          text("No: #{Date.today.to_s(:timestamp)}", align: :left, size: 9)
          move_up(10)
          text(SiteConfig.agency, align: :center, size: 12)
          font("Times-Roman")
        end
      end
      bounding_box([margin_box.left, margin_box.top - 45], width: margin_box.width, height: margin_box.height - 75) do
        move_down(5)
        # process @unfinalized hash keys (separate list for each)
        [:probation, :district, :no_holds].each do |key|
          case key
          when :probation
            text("Probation Bonds", align: :center, size: 18)
          when :district
            text("Bonds for satisfied District Arrests", align: :center, size: 18)
          else
            text("Bonds with No Holds", align: :center, size: 18)
          end
          move_down(5)
          data = []
          @unfinalized[key].each do |bond|
            surety = ''
            if bond.bondsman.nil?
              if bond.surety.nil?
                surety = "UNKNOWN"
              else
                surety = bond.surety.sort_name
              end
            else
              surety = bond.bondsman.display_name
            end
            data.push([bond.id, AppUtils.format_date(bond.issued_datetime), AppUtils.show_person(bond.person), bond.arrest_id, "#{bond.bond_type.blank? ? 'UNK' : bond.bond_type}", surety, currency(bond.bond_amt), bond.status_name])
          end
          if data.empty?
            move_down(10)
            text("Nothing To Report", align: :center, style: :bold, size: 14)
            move_down(20)
          else
            data.unshift(["ID", "Date", "Person", "Arrest", "Type", "Surety", "Amt", "Status"])
            table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom] 
              rows(1..-1).padding = 3
              columns(0..-1).style(align: :left, size: 12)
              column(0).style(width: 55)
              column(1).style(width: 65)
              column(2).style(width: 175)
              column(3).style(width: 55)
              column(4).style(width: 100)
              column(5).style(width: 125)
              column(6).style(width: 75, align: :right)
              # column(7) gets whats left
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
          end
          if cursor <= 50
            start_new_page
          end
        end
        move_down(8)
        text("END OF REPORT", style: :bold)
      end
      number_pages("Page: <page> of <total>", at: [bounds.right - 65, 15])
      render
    end
  end
end