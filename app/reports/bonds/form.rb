module Bonds
  class Form < ReportBase
    def initialize(bond)
      super(page_size: 'LEGAL')
      @bond = bond
      unless @bond.is_a?(Bond)
        raise ArgumentError.new('Bonds::Form.initialize: First argument must be a valid Bond object.')
      end
    end
    
    def to_pdf
      standard_report(footer_no: @bond.id.to_s) do
        text("Criminal Bond", align: :center, size: 18, style: :bold)
        move_up(5)
        text(AppUtils.show_person(@bond.person), size: 14, style: :bold, align: :left)
        move_up(16)
        text("Bond Number: <b>#{@bond.bond_no}</b>", size: 14, align: :right, inline_format: true)
        text("Arrest ID: <b>#{@bond.arrest.nil? ? 'Invalid Arrest' : @bond.arrest.id}</b>      Race: <b>#{@bond.person.nil? ? '' : @bond.person.race_abbreviation}</b>      Sex: <b>#{@bond.person.nil? ? '' : @bond.person.sex_name(true)}</b>      DOB: <b>#{@bond.person.nil? ? '' : AppUtils.format_date(@bond.person.date_of_birth)}</b>      SSN: <b>#{@bond.person.nil? ? '' : @bond.person.ssn}</b>", size: 12, align: :center, inline_format: true)
        move_down(15)
        text("STATE OF #{State.get(SiteConfig.agency_state).upcase}", style: :bold, align: :left, size: 14)
        move_up(16)
        text("#{SiteConfig.court_title.upcase} COURT", style: :bold, align: :right, size: 14)
        text("#{SiteConfig.jurisdiction_title.upcase}", style: :bold, align: :left, size: 14)
        move_down(10)
        if !@bond.bondsman_name?
          if @bond.surety.nil?
            surety = "UNKNOWN"
          else
            surety = @bond.surety.display_name
            surety_line1 = @bond.surety.physical_line1
            surety_line2 = @bond.surety.physical_line2
            surety_line3 = ""
            surety_line4 = ""
          end
        else
          surety = @bond.bond_company
          surety_line1 = @bond.bondsman_name
          surety_line2 = @bond.bondsman_address1
          if @bond.bondsman_address2.blank?
            surety_line3 = @bond.bondsman_address3
            surety_line4 = ''
          else
            surety_line3 = @bond.bondsman_address2
            surety_line4 = @bond.bondsman_address3
          end
        end
        text("KNOWN ALL MEN BY THESE PRESENTS, that we   <b>#{AppUtils.show_person(@bond.person,false).upcase}</b>   as principal (defendent) and   <b>#{surety.blank? ? '____________________________' : surety.upcase}</b>   as surety, are bound in solido, and acknowledge ourselves to be indebted unto the State of #{State.get(SiteConfig.agency_state)}, #{SiteConfig.jurisdiction_title}, in the sum of   <b>#{money(@bond.bond_amt)}</b>   Dollars for the faithful payment of which we bind ourselves, our heirs, executors and assigns, and in faith whereof we have signed these presents this date   <b>#{AppUtils.format_date(@bond.issued_datetime)}</b>   at #{SiteConfig.agency_city}, #{SiteConfig.jurisdiction_title}, #{State.get(SiteConfig.agency_state)}.", size: 12, inline_format: true)
        move_down(5)
        text("<b>#{AppUtils.show_person(@bond.person,false).upcase}</b>, having been arrested for the crime of:", size: 12, inline_format: true)
        move_down(5)
        span(bounds.right - 15, position: 15) do
          chgs = []
          unless @bond.arrest.nil?
            if @bond.holds.last.hold_type == 'Bondable'
              chgs = @bond.arrest.district_charges
            else
              chgs = @bond.arrest.charges.reject{|c| c.agency != @bond.holds.last.agency}
            end
          end
          chgs.each do |c|
            text("#{c.agency.blank? ? '' : c.agency + ': '}(#{c.count}) #{c.charge}", size: 12)
          end
          if chgs.empty? || @bond.arrest.nil?
            text(@bond.holds.last.hold_type, size: 12)
          end
        end
        move_down(5)
        text("and having been admitted to bail in the aforesaid amount, we hereby undertake that the above named principal will appear at all stages of the proceedings in the #{SiteConfig.court_title.upcase} COURT to answer that charge or any related charge, and will at all times hold himself amenable to the orders and process of the Court, and if convicted will appear for the pronouncement of the verdict and sentence, and will not leave the State without written permission of the Court, and that if he fails to perform any of these conditions, we or I will pay unto the State of #{State.get(SiteConfig.agency_state)} the aforesaid amount.", size: 12)
        move_down(25)
        y_position = cursor
        signature_line("Signature of Surety", placement: :left)
        move_cursor_to(y_position)
        signature_line("Signature of Principal")
        move_down(5)
        y_position = cursor
        span(col(2), position: col(2,1)) do
          text(surety, size: 12)
          text(surety_line1, size: 12)
          text(surety_line2, size: 12)
          text(surety_line3, size: 12)
          text(surety_line4, size: 12)
        end
        move_cursor_to(y_position)
        span(col(2), position: col(2,2)) do
          text(AppUtils.show_person(@bond.person, false), size: 12)
          text("#{@bond.person.nil? ? '' : @bond.person.physical_line1} ", size: 12)
          text("#{@bond.person.nil? ? '' : @bond.person.physical_line2} ", size: 12)
          text(" ", size: 12)
        end
        pad(10){ stroke_horizontal_rule }
        text("STATE OF #{State.get(SiteConfig.agency_state).upcase}", style: :bold, align: :left, size: 14)
        move_up(16)
        text(SiteConfig.jurisdiction_title.upcase, style: :bold, align: :right, size: 14)
        move_down(10)
        text("Personally appeared before me the undersigned authority. Who, being by me duly sworn in, did depose and say that he is a citizen and resident of the State of #{State.get(SiteConfig.agency_state)} that after the payment of all his debts he is owner in his own right of property, real or personal or both, liable to seizure in an amount equal to the sum named in the above bond, that he is not an attorney at law, a judge, or ministerial officer of my court, and that he is not a surety on any undischarged bail bonds for which the below described property was used as surety.", size: 12, style: :bold)
        move_down(5)
        text("Legal Description of Property Assigned:", size: 12)
        move_down(5)
        text("(SEE ATTACHED)", size: 12, align: :center)
        move_down(10)
        text("LOUISIANA REVISED STATUTE 14:351:", size: 9)
        span(bounds.right - 15, position: 15) do
          text("No person shall, with intent to defraud, sell, transfer, donate, give, mortgage, hypothecate, or in any way encumber in the predjudice of the State any real estate offered as security to the State on any bail or appearance bond for the release of any person charged with crime.  Whoever violates this Section shall be imprisoned with or without hard labor for not less than six months nor more than twelve months.", size: 9)
        end
        move_down(25)
        y_position = cursor
        signature_line("Signature of Witness", placement: :left)
        move_cursor_to(y_position)
        signature_line("Signature of Surety")
        move_down(25)
        y_position = cursor
        signature_line("Signature of Deputy Sheriff", placement: :left)
        move_cursor_to(y_position)
        signature_line("Signature of Surety")
        move_down(25)
        text("Sworn To and Before Me Subscribed This ________ Day Of _____________________________")
        move_down(25)
        signature_line("NOTARY")
        move_up(14)
        text("Bond Date/Time: #{AppUtils.format_date(@bond.issued_datetime)}")
        move_down(5)
        text("Conditions of Bond: #{@bond.arrest.nil? ? "" : (@bond.arrest.condition_of_bond? ? @bond.arrest.condition_of_bond.upcase : '')}")
      end
    end
  end
end