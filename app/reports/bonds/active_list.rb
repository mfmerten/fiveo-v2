module Bonds
  class ActiveList < ReportBase
    def initialize(bonds)
      super(page_layout: :landscape)
      @bonds = bonds
      unless @bonds.is_a?(Array)
        raise ArgumentError.new('Bonds::ActiveList.initialize: First argument must be an Array of Bond objects.')
      end
      @bonds.each do |b|
        unless b.is_a?(Bond)
          raise ArgumentError.new('Bonds::ActiveList.initialize: First argument must be an Array of Bond objects: supplied Array contains invalid object type(s)!')
        end
      end
    end
    
    def to_pdf
      standard_report(header: false, footer_txt: SiteConfig.agency) do
        text("Active Bonds", size: 18, style: :bold, align: :center)
        move_up(16)
        y_position = cursor
        text(Date.today.to_s(:us), size: 14, style: :bold, align: :right)
        move_cursor_to(y_position)
        text(Date.today.to_s(:us), size: 14, style: :bold)
        data = [["Bond ID","Pers ID","Person","Bond No.", "Issue Date", "Type","Surety", "Arrest ID", "Amount"]]
        @bonds.each do |b|
          if !b.bondsman.nil?
            surety = b.bondsman.display_name
          elsif !b.surety.nil?
            surety = b.surety.display_name
          else
            surety = "Unknown"
          end
          data.push([b.id, b.person.id, b.person.sort_name, b.bond_no, AppUtils.format_date(b.issued_datetime), "#{b.bond_type.blank? ? 'Unknown' : b.bond_type}",surety,"#{b.arrest.nil? ? '' : b.arrest.id}",currency(b.bond_amt)])
        end
        table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom] 
          rows(1..-1).padding = 3
          columns(0..-1).style(align: :left)
          column(0).style(width: 50)
          column(1).style(width: 50)
          column(3).style(width: 75)
          column(4).style(width: 65)
          column(5).style(width: 105)
          column(7).style(width: 50)
          column(8).style(width: 95, align: :right)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
        end
        move_down(5)
        text("END OF REPORT", size: 12)
      end
    end
  end
end