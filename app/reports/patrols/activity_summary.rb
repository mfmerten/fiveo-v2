module Patrols
  class ActivitySummary < ReportBase
    def initialize(start_date, stop_date, patrols)
      super
      @start_date = start_date
      unless @start_date.nil? || @start_date.is_a?(Date)
        raise ArgumentError.new("Patrols::ActivitySummary.initialize: First argument must be a Date object or Nil.")
      end
      @stop_date = stop_date
      unless @stop_date.nil? || @stop_date.is_a?(Date)
        raise ArgumentError.new("Patrols::ActivitySummary.initialize: Second argument must be a Date object or Nil.")
      end
      @patrols = patrols
      unless @patrols.is_a?(Array)
        raise ArgumentError.new("Patrols::ActivitySummary.initialize: Third argument must be an Array of Patrol objects.")
      end
      @patrols.each do |p|
        unless p.is_a?(Patrol)
          raise ArgumentError.new("Patrols::ActivitySummary.initialize: Third argument must be an Array of Patrol objects: supplied Array contains invalid object type(s).")
        end
      end
    end
    
    def to_pdf
      standard_report do
        text("Patrol Division Activity Summary", size: 18, align: :center, style: :bold)
        if @stop_date.nil? && @start_date.nil?
          str = "All Records"
        elsif @stop_date.nil?
          str = "On or After #{AppUtils.format_date(@start_date)}"
        elsif @start_date.nil?
          str = "On or Before #{AppUtils.format_date(@stop_date)}"
        else
          str = "#{AppUtils.format_date(@start_date)} To #{AppUtils.format_date(@stop_date)}"
        end
        move_down(10)
        text(str, size: 14, align: :center, style: :bold)
        activities = %w(roads_patrolled complaints traffic_stops citations_issued papers_served narcotics_arrests other_arrests public_assists warrants_served funeral_escorts mileage)
        totals = {'total' => 0}
        activities.each{|a| totals[a] = 0}
        @patrols.each do |p|
          activities.each do |a|
            unless p.send(a).nil?
              totals[a] += p.send(a)
              unless a == 'mileage'
                totals['total'] += p.send(a)
              end
            end
          end
        end
        data = []
        activities.each do |a|
          next if a == 'mileage'
          data.push([a.titleize + ":", totals[a]])
        end
        data.push(['Total Activities:', totals['total']])
        data.push(['Total Miles Recorded:', totals['mileage']])
        move_down(20)
        table(data, position: 175) do
          columns(0..-1).style(align: :right, size: 16, borders: [])
          column(1).style(font_style: :bold)
          row(-2).style(borders: [:top])
        end
      end
    end
  end
end