module Patrols
  class ActivityReport < ReportBase
    def initialize(start_date, stop_date, patrols)
      super(page_layout: :landscape)
      @start_date = start_date
      unless @start_date.nil? || @start_date.is_a?(Date)
        raise ArgumentError.new("Patrols::ActivityReport.initialize: First argument must be a Date object or Nil.")
      end
      @stop_date = stop_date
      unless @stop_date.nil? || @stop_date.is_a?(Date)
        raise ArgumentError.new("Patrols::ActivityReport.initialize: Second argument must be a Date object or Nil.")
      end
      @patrols = patrols
      unless @patrols.is_a?(Array)
        raise ArgumentError.new("Patrols::ActivityReport.initialize: Third argument must be an Array of Patrol objects.")
      end
      @patrols.each do |p|
        unless p.is_a?(Patrol)
          raise ArgumentError.new("Patrols::ActivityReport.initialize: Third argument must be an Array of Patrol objects: supplied Array contains invalid object type(s).")
        end
      end
    end
    
    def to_pdf
      standard_report do
        activities = %w(roads_patrolled complaints traffic_stops citations_issued papers_served narcotics_arrests other_arrests public_assists warrants_served funeral_escorts mileage)
        activity_headers = activities.reject{|a| a == 'mileage'}.collect{|a| a.slice(0,3).upcase}
        activity_headers.push('Total')
        activity_headers.push('Mileage')
        totals = {'total' => 0}
        activities.each do |a|
          totals[a] = 0
        end
        officers = []
        officers_units = []
        shifts = {}
        @patrols.each do |p|
          officer_unit_name = "#{p.officer_unit.blank? ? '' : p.officer_unit + ' '}#{p.officer}"
          officers.push(officer_unit_name)
          officers_units.push([p.officer,p.officer_unit])
          if shifts[p.shift].nil?
            # for shift, look up current supervisor name if possible, fallback
            # to supervisor name stored in patrol record
            if !SiteConfig.send("patrol_shift#{p.shift}_supervisor").blank?
              shifts[p.shift] = SiteConfig.send("patrol_shift#{p.shift}_supervisor")
            else
              shifts[p.shift] = p.supervisor
            end
          end
          # add any hashes that are missing
          # totals by activity
          if totals[p.shift].nil?
            totals[p.shift] = {}
            activities.each do |a|
              totals[p.shift][a] = 0
            end
            totals[p.shift]['total'] = 0
          end
          # totals by officer
          if totals[officer_unit_name].nil?
            totals[officer_unit_name] = {}
            activities.each do |a|
              totals[officer_unit_name][a] = 0
            end
            totals[officer_unit_name]['total'] = 0
          end
          # totals by shift/officer
          if totals[p.shift][officer_unit_name].nil?
            totals[p.shift][officer_unit_name] = {}
            activities.each do |a|
              totals[p.shift][officer_unit_name][a] = 0
            end
            totals[p.shift][officer_unit_name]['total'] = 0
          end
          # add values for each activity
          activities.each do |a|
            if p.respond_to?(a) && !p.send(a).nil?
              # activity totals
              totals[a] += p.send(a)
              # shift totals
              totals[p.shift][a] += p.send(a)
              # shift/officer totals
              totals[p.shift][officer_unit_name][a] += p.send(a)
              # officer totals
              totals[officer_unit_name][a] += p.send(a)
              unless a == 'mileage'
                # grand totals for activities do not include mileage
                totals['total'] += p.send(a)
                totals[p.shift]['total'] += p.send(a)
                totals[p.shift][officer_unit_name]['total'] += p.send(a)
                totals[officer_unit_name]['total'] += p.send(a)
              end
            end
          end
        end
        shift_names = shifts.collect{|key,value| "#{key}: #{value}"}
        
        # details by Officer
        text("Patrol Division Activity Report", size: 18, align: :center, style: :bold)
        if @stop_date.nil? && @start_date.nil?
          str = "All Records"
        elsif @stop_date.nil?
          str = "On or After #{AppUtils.format_date(@start_date)}"
        elsif @start_date.nil?
          str = "On or Before #{AppUtils.format_date(@stop_date)}"
        else
          str = "#{AppUtils.format_date(@start_date)} To #{AppUtils.format_date(@stop_date)}"
        end
        text(str, size: 14, align: :center, style: :bold)
        text("By Officer", size: 16, align: :center, style: :bold)
        data = []
        data_headers = activity_headers.dup
        data_headers.unshift("Officer")
        data.push(data_headers)
        officers = officers.uniq.sort{|a,b| a[0] <=> b[0]}
        officers.each do |officer|
          line = []
          activities.reject{|a| a == 'mileage'}.each do |a|
            line.push(totals[officer][a])
          end
          line.push(totals[officer]['total'])
          line.push(totals[officer]['mileage'])
          line.unshift(officer)
          data.push(line)
        end
        line = []
        line.push("Grand Totals:")
        activities.reject{|a| a == 'mileage'}.each do |a|
          line.push(totals[a])
        end
        line.push(totals['total'])
        line.push(totals['mileage'])
        data.push(line)
        # draw the thing
        table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], position: :center) do
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom] 
          rows(1..-1).padding = 3
          rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
          column(0).style(width: 190, align: :left)
          column(1..-3).style(width: 44, align: :right)
          column(-2).style(width: 55, align: :right)
          column(-1).style(width: 65, align: :right)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom], align: :center, valign: :bottom)
        end
        move_down(3)
        text("ROA: roads patrolled, COM: complaints, TRA: traffic stops, CIT: citations issued, PAP: papers served, NAR: narcotics arrests, OTH: other arrests, PUB: public assists, WAR: warrants, FUN: funeral escorts.", size: 9) 
        
        # Report by shift
        start_new_page
        text("Patrol Division Activity Report", size: 18, align: :center, style: :bold)
        if @stop_date.nil? && @start_date.nil?
          str = "All Records"
        elsif @stop_date.nil?
          str = "On or After #{AppUtils.format_date(@start_date)}"
        elsif @start_date.nil?
          str = "On or Before #{AppUtils.format_date(@stop_date)}"
        else
          str = "#{AppUtils.format_date(@start_date)} To #{AppUtils.format_date(@stop_date)}"
        end
        text(str, size: 14, align: :center, style: :bold)
        text("By Shift", size: 16, align: :center, style: :bold)
        
        shifts.keys.sort.each do |shift|
          heading("Shift #{shift} -- #{shifts[shift]}")
          data = []
          data_headers = activity_headers.dup
          data_headers.unshift("Officer")
          totals[shift].keys.reject{|k| !officers.include?(k)}.sort.each do |officer|
            line = []
            activities.reject{|a| a == 'mileage'}.each do |a|
              line.push(totals[shift][officer][a])
            end
            line.push(totals[shift][officer]['total'])
            line.push(totals[shift][officer]['mileage'])
            line.unshift(officer)
            data.push(line)
          end
          data = data.sort{|a,b| a[0] <=> b[0]}
          line = []
          line.push("Shift Totals:")
          activities.reject{|a| a == 'mileage'}.each do |a|
            line.push(totals[shift][a])
          end
          line.push(totals[shift]['total'])
          line.push(totals[shift]['mileage'])
          data.push(line)
          data.unshift(data_headers)
          # draw the thing
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], position: :center) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
            column(0).style(width: 190, align: :left)
            column(1..-3).style(width: 44, align: :right)
            column(-2).style(width: 55, align: :right)
            column(-1).style(width: 65, align: :right)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom], align: :center, valign: :bottom)
          end
          move_down(3)
          text("ROA: roads patrolled, COM: complaints, TRA: traffic stops, CIT: citations issued, PAP: papers served, NAR: narcotics arrests, OTH: other arrests, PUB: public assists, WAR: warrants, FUN: funeral escorts.", size: 9) 
        end
        data = []
        data_headers = activity_headers.dup
        data_headers.unshift("Officer")
        data.push(data_headers)
        line = []
        line.push("Grand Totals:")
        activities.reject{|a| a == 'mileage'}.each do |a|
          line.push(totals[a])
        end
        line.push(totals['total'])
        line.push(totals['mileage'])
        data.push(line)
        # draw the thing
        table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], position: :center) do
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom] 
          rows(1..-1).padding = 3
          rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
          column(0).style(width: 190, align: :left)
          column(1..-3).style(width: 44, align: :right)
          column(-2).style(width: 55, align: :right)
          column(-1).style(width: 65, align: :right)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom], align: :center, valign: :bottom)
        end
        move_down(3)
        text("ROA: roads patrolled, COM: complaints, TRA: traffic stops, CIT: citations issued, PAP: papers served, NAR: narcotics arrests, OTH: other arrests, PUB: public assists, WAR: warrants, FUN: funeral escorts.", size: 9) 
        
        # Arrest Detail (from arrests - will not necessarily agree with the arrest details above)
        start_new_page
        text("Patrol Division Activity Report", size: 18, align: :center, style: :bold)
        if @stop_date.nil? && @start_date.nil?
          str = "All Records"
        elsif @stop_date.nil?
          str = "On or After #{AppUtils.format_date(@start_date)}"
        elsif @start_date.nil?
          str = "On or Before #{AppUtils.format_date(@stop_date)}"
        else
          str = "#{AppUtils.format_date(@start_date)} To #{AppUtils.format_date(@stop_date)}"
        end
        text(str, size: 14, align: :center, style: :bold)
        text("Arrest Detail", size: 16, align: :center, style: :bold)
        data = [["Date","Name","Charge(s)","Officer(s)"]]
        @arrests = []
        @warrants = []
        officers_units.compact.uniq.each do |officer,unit|
          temp = Arrest.arrests_by_officer(officer, unit, nil, start: @start_date, stop: @stop_date)
          temp.each do |a|
            @warrants.push(a.warrants)
          end
          @arrests.push(temp)
        end
        @arrests = @arrests.flatten.compact.uniq
        @warrants = @warrants.flatten.compact.uniq
        @arrests.sort{|a,b| a.person.sort_name <=> b.person.sort_name}.each do |a|
          officer_names = a.officers.collect{|o| "#{o.officer_unit? ? o.officer_unit + ' ' : ''}#{o.officer}"}.join("\n")
          data.push([AppUtils.format_date(a.arrest_datetime.to_date), a.person.sort_name, a.charges.join(', '), officer_names])
        end
        data.push([" ","Total Arrested: #{@arrests.size}"," "," "])
        table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], position: :center) do
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom] 
          rows(1..-1).padding = 3
          rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
          columns(0..-1).style(size: 10, align: :left)
          column(0).style(width: 75)
          column(1).style(width: 150)
          column(2).style(width: 375)
          column(3).style(width: 150)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom], align: :center, valign: :bottom)
        end
        move_down(3)
        text("This list includes only arrests on file where one or more of the Officers shown in the Patrol Activity section were also shown as an arresting officer on the arrest.", size: 9)
        
        # Warrant Detail (from arrests/warrants - will not necessarily agree with the warrant details above)
        start_new_page
        text("Patrol Division Activity Report", size: 18, align: :center, style: :bold)
        if @stop_date.nil? && @start_date.nil?
          str = "All Records"
        elsif @stop_date.nil?
          str = "On or After #{AppUtils.format_date(@start_date)}"
        elsif @start_date.nil?
          str = "On or Before #{AppUtils.format_date(@stop_date)}"
        else
          str = "#{AppUtils.format_date(@start_date)} To #{AppUtils.format_date(@stop_date)}"
        end
        text(str, size: 14, align: :center, style: :bold)
        text("Warrant Detail", size: 16, align: :center, style: :bold)
        data = [["Date","Name","Warrant No", "Charge(s)","Officer(s)"]]
        @warrants.sort{|w| w.sort_name <=> w.sort_name}.each do |w|
          officer_names = w.arrest.officers.collect{|o| "#{o.officer_unit? ? o.officer_unit + ' ' : ''}#{o.officer}"}.join("\n")
          data.push([AppUtils.format_date(w.arrest.arrest_datetime.to_date), w.sort_name, w.warrant_no, w.charge_summary, officer_names])
        end
        data.push([" ","Total Warrants: #{@warrants.size}"," "," "," "])
        table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], position: :center) do
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom] 
          rows(1..-1).padding = 3
          rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
          columns(0..-1).style(size: 10, align: :left)
          column(0).style(width: 75)
          column(1).style(width: 150)
          column(2).style(width: 75)
          column(3).style(width: 325)
          column(4).style(width: 125)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom], align: :center, valign: :bottom)
        end
        move_down(3)
        text("This list includes only warrants that are linked to the arrests shown in the Arrest Detail section. It does not include warrants that are not linked to arrest records (i.e. served manually).", size: 9)
      end
    end
  end
end