module People
  module RapSheet
    # alias new to the actual report class
    def self.new(person_id)
      People::RapSheet::Report.new(person_id)
    end
    
    # alias data to the report data class
    def self.data(person_id)
      People::RapSheet::ReportData.new(person_id)
    end
  end
end