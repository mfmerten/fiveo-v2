module People
  module RapSheet
    class ReportData
      def initialize(person_id)
        @person = Person.includes(:active_warrants, :possible_warrants, :active_forbids, :possible_forbids).find_by(id: person_id)
        if @person.nil?
          raise ArgumentError.new("People::RapSheet::ReportData.initialize: only argument must be valid Person ID.")
        end
        @history = init_history
        @alias_history = init_alias_history
        @vwarrants = @person.active_warrants
        @pwarrants = @person.possible_warrants
        @vforbids = @person.active_forbids
        @pforbids = @person.possible_forbids
        @bonds = @person.bonds.reject{|b| b.final?}
        @probations = @person.active_probations
        @wexempt = @person.exempted_warrants.reject(&:dispo_date?)
        @fexempt = @person.exempted_forbids.reject(&:recall_date?)
      end
      
      def person
        @person
      end
      
      def person_history
        @history
      end
        
      def alias_history
        @alias_history
      end
      
      def verified_warrants
        @vwarrants
      end
      
      def possible_warrants
        @pwarrants
      end
      
      def verified_forbids
        @vforbids
      end
      
      def possible_forbids
        @pforbids
      end
      
      def bonds
        @bonds
      end
      
      def probations
        @probations
      end
      
      def warrant_exemptions
        @wexempt
      end
      
      def forbid_exemptions
        @fexempt
      end
      
      private
      
      # returns array of History objects for person
      def init_history
        People::RapSheet::History.new(@person)
      end
    
      # returns array of History objects for all aliases 
      def init_alias_history
        gathered = []
        @person.real_identity.all_aliases.each do |p|
          next if @person == p
          ahist = People::RapSheet::History.new(p)
          unless ahist.rows.empty?
            gathered.push(ahist)
          end
        end
        gathered
      end
    end
  end
end