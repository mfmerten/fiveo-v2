module People
  module RapSheet
    class History
      def initialize(person)
        @person = person
        unless @person.is_a?(Person)
          raise ArgumentError.new("People::RapSheet::History.initialize: only argument must be valid Person object.")
        end
        @data = gather_data
      end
      
      def person
        @person
      end
      
      def person_id
        @person.id
      end
      
      def full_name
        @person.sort_name
      end
      
      def rows
        @data
      end
      
      private
      
      def gather_data
        gathered = []
        @person.arrests.includes(:charges, :da_charges).reject{|a| a.charges.empty?}.each do |a|
          sequence = 0
          a.district_charges.each do |c|
            sequence += 1
            gathered.push(Row.new(a,sequence,c))
          end
          a.da_charges.each do |c|
            sequence += 1
            gathered.push(Row.new(a,sequence,c,"Added By D.A."))
          end
          a.city_charges.each do |c|
            sequence += 1
            gathered.push(Row.new(a,sequence,c,c.agency))
          end
          a.other_charges.each do |c|
            sequence += 1
            gathered.push(Row.new(a,sequence,c,c.agency))
          end
        end
        @person.citations.each do |cit|
          sequence = 0
          cit.charges.each do |c|
            sequence += 1
            gathered.push(Row.new(cit,sequence,c))
          end
        end
        @person.transfers.reject{|t| t.transfer_type.blank? || t.transfer_type =~ /^Temporary/}.each do |t|
          extra = ""
          unless t.transfer_type.blank?
            extra = t.transfer_type
          end
          gathered.push(Row.new(t))
        end
        @person.probations.each do |p|
          gathered.push(Row.new(p))
        end
        gathered.sort_by{|g| [g.row_date, g.row_name, g.original_id, g.row_sequence]}
      end
    end
  end
end
  