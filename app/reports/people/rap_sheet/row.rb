module People
  module RapSheet
    class Row
      include ActiveAttr::Model
    
      attribute :original_id, type: Integer
      attribute :row_name, type: String
      attribute :row_subname, type: String
      attribute :row_date, type: Date
      attribute :row_sequence, type: Integer
      attribute :column_1_text, type: String
      attribute :column_2_text, type: String
      attribute :column_3_text, type: String
      
      def initialize(record,row_sequence=0,charge=nil,extra="")
        case
        when record.is_a?(Arrest)
          init_arrest(record,row_sequence,charge,extra)
        when record.is_a?(Citation)
          init_citation(record,row_sequence,charge)
        when record.is_a?(Probation)
          init_probation(record)
        when record.is_a?(Transfer)
          init_transfer(record)
        end
      end
      
      private
      
      def init_arrest(record,row_sequence,charge,extra="")
        self.original_id = record.id
        self.row_name = "Arrest"
        self.row_subname = ""
        self.row_date = record.arrest_datetime.to_date
        self.row_sequence = row_sequence
        self.column_1_text = charge.count
        self.column_2_text = charge.charge + (extra.blank? ? '' : " (#{extra})")
        status = ""
        if record.legacy?
          status = '[Legacy] '
        end
        if charge.is_a?(Charge) && charge.da_charges.empty?
          status << '[Pending]'
        elsif charge.is_a?(DaCharge)
          status << PeopleHelper.charge_status_string(charge)
        else
          status << charge.da_charges.collect{|d| PeopleHelper.charge_status_string(d,charge)}.join("\n")
        end
        self.column_3_text = status
      end
      
      def init_citation(record,row_sequence,charge)
        self.original_id = record.id
        self.row_name = "Citation"
        self.row_subname = nil
        self.row_date = record.citation_datetime.to_date
        self.row_sequence = row_sequence
        self.column_1_text = charge.count
        self.column_2_text = charge.charge
        if charge.da_charges.empty?
          self.column_3_text = "[Pending]"
        else
          self.column_3_text = charge.da_charges.collect{|d| PeopleHelper.charge_status_string(d,charge)}.join("\n")
        end
      end
      
      def init_probation(record)
        if record.status.blank?
          active = false
        else
          active = ['Active','State','Unsupervised'].include?(record.status)
        end
        if active
          if record.status == 'Active'
            ptype = 'Parish'
          else
            ptype = record.status
          end
          pstat = 'Active'
          if record.hold_if_arrested?
            pstat << " [HOLD IF ARRESTED]"
          end
        else
          if !record.sentence.nil? && !record.sentence.probation_type.blank?
            ptype = record.sentence.probation_type
          else
            ptype = ""
          end
          pstat = record.status
          if record.status == 'Terminated'
            pstat << ": #{record.terminate_reason}"
          end
        end
        self.original_id = record.id
        self.row_name = "Probation"
        self.row_subname = "#{ptype} #{record.probation_string}"
        if record.start_date?
          self.row_date = record.start_date
        else
          self.row_date = record.created_at.to_date
        end
        self.row_sequence = 1
        self.column_1_text = record.conviction_count
        self.column_2_text = record.conviction
        self.column_3_text = pstat
      end
      
      def init_transfer(record)
        self.original_id = record.id
        self.row_name = "Transfer"
        self.row_subname = record.transfer_type
        self.row_date = record.transfer_datetime.to_date
        self.row_sequence = 1
        self.column_1_text = ""
        self.column_2_text = record.from_agency
        self.column_3_text = record.to_agency
      end
    end
  end
end
