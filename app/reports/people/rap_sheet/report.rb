module People
  module RapSheet
    class Report < ReportBase
      def initialize(person_id)
        super(page_size: 'LEGAL')
        unless @person = Person.find_by_id(person_id)
          raise ArgumentError.new("People::RapSheet::Report: First argument must be a valid Person ID!")
        end
        @data = ReportData.new(person_id)
      end
    
      def data
        @data
      end
    
      def to_pdf
        standard_report(footer_no: @data.person.id.to_s) do
          text("RAP SHEET", align: :center, size: 18, style: :bold)
          text(Time.now.to_s(:us), size: 14, style: :bold, align: :center)
          move_down(10)
          quot = '"'
          text("#{AppUtils.show_person @data.person}", size: 18, style: :bold, align: :left)
          if @data.person.sex_offender?
            move_up(20)
            text("SEX OFFENDER", size: 18, style: :bold, align: :right, color: "ff0000")
          end
          image(get_img_path(@data.person.front_mugshot, :medium), at: [306, bounds.top - 70], fit: [100,150])
          image(get_img_path(@data.person.side_mugshot, :medium), at: [456, bounds.top - 70], fit: [100,150])
          row_position = cursor
          row_position -= text_field([col(2,1),row_position], col(2), "Date of Birth:", AppUtils.format_date(@data.person.date_of_birth))
          row_position -= text_field([col(2,1),row_position], col(2), "Place of Birth:", @data.person.place_of_birth)
          row_position -= text_field([col(2,1),row_position], col(2), "SSN:", @data.person.ssn)
          row_position -= text_field([col(2,1),row_position], col(2), "OLN:", "#{@data.person.oln} #{@data.person.oln_state}")
          row_position -= text_field([col(2,1),row_position], col(2), "SID:", @data.person.sid)
          row_position -= text_field([col(2,1),row_position], col(2), "FBI ID:", @data.person.fbi)
          row_position -= text_field([col(2,1),row_position], col(2), "DOC No:", @data.person.doc_number)
          text_field([col(2,1),row_position], col(2), "Physical Address:", "#{@data.person.physical_line1}\n#{@data.person.physical_line2}", rows: 2)
          row_position -= text_field([col(2,2),row_position], col(2), "Mailing Address:", "#{@data.person.mailing_line1}\n#{@data.person.mailing_line2}", rows: 2)
          text_field([col(2,2),row_position], col(2), "Emergency Contact:", "#{@data.person.emergency_contact}\n#{@data.person.em_relationship}\n#{@data.person.emergency_phone}",pdf_color_scheme: 'em', rows: 3)
          text_field([col(4,1),row_position], col(4), "Home Phone:", @data.person.home_phone)
          row_position -= text_field([col(4,2),row_position], col(4), "Cell Phone:", @data.person.cell_phone)
          row_position -= text_field([col(2,1),row_position], col(2), "Occupation:", @data.person.occupation)
          row_position -= text_field([col(2,1),row_position], col(2), "Employer:", @data.person.employer)
          text_field([col(2,1),row_position], col(2), "Description:", @data.person.person_description, rows: 4)
          row_position -= text_field([col(2,2),row_position], col(2), "Distinguishing Marks:", @data.person.marks_description, rows: 4)
          row_position -= text_field([col(1,1),row_position], col(1), "AKA:", @data.person.aka)
          known_aliases = @data.person.all_aliases.collect{|p| p.sort_name}.compact.uniq.reject{|n| n == @data.person.sort_name}
          if known_aliases.empty?
            known_aliases = " "
          else
            known_aliases = known_aliases.sort.collect{|n| "[#{n}]"}.join(", ")
          end
          row_position -= text_field([col(1,1),row_position], col(1), "Other Names Used:", known_aliases)
          move_down(5)
    
          heading("Active Records")
          unless @data.verified_warrants.empty?
            fill_color = SiteConfig.pdf_em_txt
            text("Verified Warrants:", size: 14, style: :bold)
            fill_color = '000000'
            data = []
            @data.verified_warrants.each do |w|
              data.push([w.id, w.sort_name, AppUtils.format_date(w.dob), w.ssn, w.jurisdiction, w.charge_summary])
            end
            data.unshift(['Warrant#','Name','DOB','SSN','Agency','Charge(s)'])
            table(data, width: bounds.width - 20, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], position: 20) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom] 
              rows(1..-1).padding = 3
              columns(0..-1).style(align: :left, size: 10, padding: 2)
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
            move_down(10)
          end
    
          unless @data.possible_warrants.empty?
            fill_color = SiteConfig.pdf_em_txt
            text("Possible Warrants:", size: 14, style: :bold)
            fill_color = '000000'
            data = []
            @data.possible_warrants.each do |w|
              data.push([w.id, w.sort_name, AppUtils.format_date(w.dob), w.ssn, w.jurisdiction, w.charge_summary])
            end
            data.unshift(['Warrant#','Name','DOB','SSN','Agency','Charge(s)'])
            table(data, width: bounds.width - 20, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], position: 20) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom] 
              rows(1..-1).padding = 3
              columns(0..-1).style(align: :left, size: 10, padding: 2)
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
            move_down(10)
          end
    
          unless @data.verified_forbids.empty?
            text("Verified Forbids:", size: 14, style: :bold)
            data = []
            @data.verified_forbids.each do |f|
              if !f.signed_date?
                status = "#{AppUtils.format_date(f.request_date)} (NOT SIGNED)"
              else
                status = "#{AppUtils.format_date(f.signed_date)} (ACTIVE)"
              end
              data.push([f.id, f.sort_name, f.complainant, status, f.details])
            end
            data.unshift(['Forbid No','Name','Complainant','Status','Forbidden Locations'])
            table(data, width: bounds.width - 20, header: true, row_colors: [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even], position: 20) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom] 
              rows(1..-1).padding = 3
              columns(0..-1).style(align: :left, size: 9, padding: 2)
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
            move_down(10)
          end
    
          unless @data.possible_forbids.empty?
            text("Possible Forbids:", size: 14, style: :bold)
            data = []
            @data.possible_forbids.each do |f|
              if !f.signed_date?
                status = "#{AppUtils.format_date(f.request_date)} (NOT SIGNED)"
              else
                status = "#{AppUtils.format_date(f.signed_date)} (ACTIVE)"
              end
              data.push([f.id, f.sort_name, f.complainant, status, f.details])
            end
            data.unshift(['Forbid No','Name','Complainant','Status','Forbidden Locations'])
            table(data, width: bounds.width - 20, header: true, row_colors: [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even], position: 20) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom] 
              rows(1..-1).padding = 3
              columns(0..-1).style(align: :left, size: 9, padding: 2)
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
            move_down(10)
          end
    
          unless @data.bonds.empty?
            text("Bonds:", size: 14, style: :bold)
            data = []
            @data.bonds.each do |b|
              data.push([b.id, AppUtils.format_date(b.issued_datetime), "#{b.arrest.nil? ? '' : b.arrest.id}", b.bond_type, "#{b.bondsman.nil? ? '' : b.bondsman.display_name}#{b.surety.nil? ? '' : b.surety.sort_name}", currency(b.bond_amt), b.status_name])
            end
            data.unshift(['ID','Date/Time','Arrest','Type','Surety','Amount','Status'])
            table(data, width: bounds.width - 20, header: true, row_colors: [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even], position: 20) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom] 
              rows(1..-1).padding = 3
              columns(0..-1).style(align: :left, size: 9, padding: 2)
              column(5).style(align: :right)
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
            move_down(10)
          end
    
          unless @data.probations.empty?
            text("Probations:", size: 14, style: :bold)
            data = []
            @data.probations.each do |p|
              active = ['Active','State','Unsupervised'].include?(p.status)
              if active
                if p.status == 'Active'
                  ptype = 'Parish'
                else
                  ptype = p.status
                end
                pstat = 'Active'
                if p.hold_if_arrested?
                  pstat << " [Hold If Arrested]"
                end
              else
                if p.doc?
                  ptype = 'State'
                else
                  ptype = 'Parish'
                end
                if !p.sentence.nil? && p.sentence.unsupervised?
                  ptype << " (Unsupervised)"
                end
                pstat = p.status
              end
              data.push([p.id, ptype, p.probation_string, pstat, "[#{p.conviction_count}] #{p.conviction}]"])
            end
            data.unshift(['ID','Type','Length','Status','Conviction'])
            table(data, header: true, row_colors: [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even], position: 20, width: bounds.width - 20) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom]
              rows(1..-1).padding = 3
              columns(0..-1).style(align: :left, size: 9, padding: 2)
              column(0).style(width: 50)
              column(1).style(width: 40)
              column(2).style(width: 65)
              column(3).style(width: 125)
              # column 4 gets whats left
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
            move_down(10)
          end
          
          # criminal history section(s)
          history_section("Criminal History", @data.person_history)
          @data.alias_history.each do |h|
            history_section("Alias: #{AppUtils.show_person(h.person)}", h)
          end
          move_down(5)
          text("END OF REPORT", size: 12)
        end
      end
      
      def history_section(title, history)
        move_down(10)
        if cursor <= 50
          start_new_page
        end
        heading(title)
        if history.rows.empty?
          text("Not On File", size: 14, style: :bold, align: :center)
        else
          original_id = nil
          history.rows.each_with_index do |row,index|
            # force pagination if necessary
            if cursor <= 50
              start_new_page
            end
            # check to see if we are on a new record
            if original_id != row.original_id
              # reset data and print a new header
              data = []
              original_id = row.original_id
              move_down(10)
              text("#{AppUtils.format_date(row.row_date)}: <b>#{row.row_name}</b> ID #{row.original_id}", size: 14, inline_format: true)
            end
            case row.row_name
            when 'Arrest','Citation'
              span(bounds.width - 20, position: 20) do
                text("#{row.column_1_text} cnt #{row.column_2_text}", size: 12)
              end
              span(bounds.width - 40, position: 40) do
                text(row.column_3_text, size: 10)
              end
            when 'Probation'
              span(bounds.width - 20, position: 20) do
                text("#{row.column_1_text} cnt #{row.column_2_text}", size: 12)
              end
              span(bounds.width - 40, position: 40) do
                text("#{row.row_subname.blank? ? '' : row.row_subname + ' -- '}#{row.column_3_text}", size: 10)
              end
            when 'Transfer'
              span(bounds.width - 20, position: 20) do
                text("<b>From:</b> #{row.column_2_text}    <b>To:</b> #{row.column_3_text}", size: 12)
              end
            end
            move_down(10)
          end
        end
      end
    end
  end
end