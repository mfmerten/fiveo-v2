module Evidences
  class Inventory < ReportBase
    def initialize(evidences)
      super(page_layout: :landscape)
      @evidences = evidences
      unless @evidences.is_a?(Array)
        raise ArgumentError.new("Evidences::Inventory.initialize: Argument 1 must be an Array of [Location, Evidence object] Arrays.")
      end
      @evidences.each do |e|
        unless (e[0].nil? || e[0].is_a?(String)) && e[1].is_a?(Evidence)
          raise ArgumentError.new("Evidences::Inventory.initialize: Argument 1 must be an Array of [Location, Evidence object] Arrays: supplied Array has invalid contents.")
        end
      end
    end
  
    def to_pdf
      # custom header/footer
      repeat(:all) do
        bounding_box([margin_box.left, margin_box.top - 10], width: margin_box.width, height: 26) do
          text("Evidence Inventory", align: :center, size: 20, style: :bold)
          move_up(16)
          text(Time.now.to_s(:us), size: 14, style: :bold)
        end
        bounding_box([margin_box.left, margin_box.bottom + 20], width: margin_box.width, height: 20) do
          font("Helvetica")
          stroke_horizontal_rule
          move_down(5)
          text("No: #{Time.now.to_s(:timestamp)}", align: :left, size: 9, style: :bold)
          move_up(10)
          text("#{SiteConfig.agency}", align: :center, size: 12)
          font("Times-Roman")
        end
      end
      bounding_box([margin_box.left, margin_box.top - 36], width: margin_box.width, height: margin_box.height - 56) do
        data = [["OK","Location","ID", "Number","Description","Problem"]]
        @evidences.each do |e|
          data.push([{image: "#{Rails.root}/app/assets/images/checkbox.png", image_height: 14}, e[0], e[1].id.to_s, e[1].evidence_number, e[1].description.gsub(/\n/, ' '), "\n\n\n\n"])
        end
        table(data, header: true, row_colors: [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even], width: bounds.width) do
          rows(1..-1).borders = [:left, :right, :bottom]
          rows(1..-1).padding = 3
          columns(1..-1).style(align: :left)
          column(0).style(width: 25)
          column(1).style(width: 100)
          column(2).style(width: 55)
          column(3).style(width: 75)
          column(5).style(width: 200)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
        end
        if cursor <= 60
          start_new_page
        end
        move_down(40)
        y_position = cursor
        signature_line("Officer", placement: :left)
        move_cursor_to(y_position)
        signature_line("Date/Time")
      end
      number_pages("Page: <page> of <total>", at: [bounds.right - 65, 15])
      render
    end
  end
end