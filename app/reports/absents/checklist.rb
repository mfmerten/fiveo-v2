# AbsentPrisonerChecklist is an extention of Prawn::Document that is used to
# generate a PDF document for a prisoner checklist used when escorting a work crew.
#
# When creating new documents using this class, pass it an instance of Absent:
#    @absent = Absent.find(1)
#    pdf = AbsentPrisonerChecklist.new(@absent).to_pdf
#
module Absents
  class Checklist < ReportBase
    def initialize(absent)
      super
      @absent = absent
      unless @absent.is_a?(Absent)
        raise ArgumentError.new("Absents::Checklist.initialize: Argument 1 must be a valid Absent object.")
      end
    end
  
    def to_pdf
      standard_report do
        bookings = @absent.prisoners.sort{|a,b| a.person.sort_name <=> b.person.sort_name}
        text(@absent.jail.name, size: 16, align: :center, style: :bold)
        text("Prisoner Checklist", size: 18, align: :center, style: :bold)
        move_down(20)
        text("Leave Date/Time: __________________________________________", size: 14, align: :center, style: :bold)
        move_down(5)
        text("<b>For:</b> #{@absent.absent_type.blank? ? 'Unknown Absent Type' : @absent.absent_type}   <b>Escort:</b> #{AppUtils.show_contact(@absent, field: :escort_officer)}", size: 16, align: :center, inline_format: true)
        if bookings.empty?
          move_down(40)
          text("No Prisoners Selected", size: 14, style: :bold, align: :center)
        else
          data = [["Booking","Name","ID","Cell","Out","In"]]
          bookings.each do |b|
            data.push([b.id, b.person.sort_name, b.person.id, b.cell,{image: "#{Rails.root}/app/assets/images/checkbox.png", image_height: 14},{image: "#{Rails.root}/app/assets/images/checkbox.png", image_height: 14}])
          end
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even], width: bounds.width) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            columns(0..-3).style(align: :left)
            column(0).style(width: 65)
            column(2).style(width: 65)
            column(3).style(width: 85)
            column(4).style(width: 25)
            column(5).style(width: 25)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
        end
        if cursor <= 60
          start_new_page
        end
        move_down(40)
        y_position = cursor
        signature_line("Escort Officer", placement: :left)
        move_cursor_to(y_position)
        signature_line("Return Date/Time")
      end
    end
  end
end