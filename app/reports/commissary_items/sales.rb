module CommissaryItems
  class Sales < ReportBase
    def initialize(facility, people=[], items=[])
      super(page_size: 'LEGAL')
      @facility = facility
      unless @facility.is_a?(Jail)
        raise ArgumentError.new("CommissaryItems::Sales.initialize: First argument must be a valid Jail object.")
      end
      @people = people
      unless @people.is_a?(Array)
        raise ArgumentError.new("CommissaryItems::Sales.initialize: Second argument must be an Array of Person objects.")
      end
      @people.each do |p|
        unless p.is_a?(Person)
          raise ArgumentError.new("CommissaryItems::Sales.initialize: Second argument must be an Array of Person objects: supplied Array contains invalid object type(s)!")
        end
      end
      @commissary_items = items
      unless @commissary_items.is_a?(Array)
        raise ArgumentError.new("CommissaryItems::Sales.initialize: Third argument must be an Array of CommissaryItem objects.")
      end
      @commissary_items.each do |i|
        unless i.is_a?(CommissaryItem)
          raise ArgumentError.new("CommissaryItems::Sales.initialize: Third argument must be an Array of CommissaryItem objects: supplied array contains invalid object type(s)!")
        end
      end
    end
  
    def to_pdf
      # special page header, no footer
      this_instant = Time.now
      repeat(:all) do
        bounding_box([margin_box.left, margin_box.bottom + 20], width: margin_box.width, height: 20) do
          font("Helvetica")
          stroke_horizontal_rule
          move_down(5)
          text("No: #{Time.now.to_s(:timestamp)}", align: :left, size: 9)
          move_up(10)
          text("#{SiteConfig.agency}", align: :center, size: 12)
          font("Times-Roman")
        end
      end
      bounding_box([margin_box.left, margin_box.top - 10], width: margin_box.width, height: margin_box.height - 35) do
        # build the data array once
        lpage = @commissary_items.slice!(0, @commissary_items.size / 2)
        rpage = @commissary_items.slice!(0..-1)
        data = [["Item", "Price", "Qty", "Total", "", "Item", "Price", "Qty", "Total"]]
        (0..lpage.size-1).each do |r|
          data.push([lpage[r].description, money(lpage[r].price), " "," "," ","#{rpage[r].nil? ? '' : rpage[r].description}", "#{rpage[r].nil? ? '' : money(rpage[r].price)}", " ", " "])
        end
        (1..3).each do |r|
          data.push([" "," "," "," "," "," "," "," "," "])
        end
        # print a page for each booking
        @people.each do |p|
          if p.bookings.empty?
            b = nil
          else
            b = p.bookings.last
          end
          text("Commissary Sales Sheet", align: :center, size: 24, style: :bold)
          move_down(5)
          text("#{@facility.name}", align: :center, size: 14, style: :bold)
          text(AppUtils.format_date(this_instant), size: 12, style: :bold, align: :center)
          move_down(5)
          row_pos = bounds.top - 65
          text_field([col(2,1), row_pos], col(2), "Name:", p.sort_name)
          text_field([col(4,3), row_pos], col(4), "Account:", p.id.to_s)
          row_pos -= text_field([col(4,4), row_pos], col(4), "Location:", "#{b.nil? ? 'No Booking' : b.cell}")
          text_field([col(4,1), row_pos], col(4), "Balance:", currency(p.commissary_balance))
          text_field([col(4,2), row_pos], col(4), "Less Medical:", "$ ")
          text_field([col(4,3), row_pos], col(4), "Adjusted Balance:", "$ ")
          text_field([col(4,4), row_pos], col(4), "Total Cost:","$ ")
          font("Helvetica")
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], position: :center) do
            rows(1..-1).padding = 3
            columns(0..-1).style(align: :left, size: 11, borders: [:left, :right], border_color: SiteConfig.pdf_header_bg)
            columns([0,5]).style(width: 150)
            columns([1,6]).style(align: :right, width: 35)
            columns([2,7]).style(width: 30)
            columns([3,8]).style(width: 65)
            row(0).style(align: :left, size: 9, borders: [:bottom], font_style: :bold)
            row(-1).borders = [:left, :right, :bottom]
            column(4).style(borders: [], background_color: 'FFFFFF')
          end
          font("Times-Roman")
          move_down(10)
          text("*** ALL PRICES SUBJECT TO CHANGE WITHOUT NOTICE ***", size: 10, style: :bold, align: :center)
          text("*** LARGER SIZED ITEMS MAY HAVE HIGHER PRICE THAN SHOWN ***", size: 10, style: :bold, align: :center)
          move_down(35)
          y_position = cursor
          signature_line("Inmate Signature", placement: :left)
          move_cursor_to(y_position)
          signature_line("Jailer Signature")
          unless p == @people.last
            start_new_page
          end
        end
      end
      render
    end
  end
end