# Notes about report classes and data initialization:
# 1. ALL report classes should inherit from ReportBase class (this file)!
# 2. All user input for reports must be validated in the controller. Only
#    validated data should be used to initialize a report class.
# 3. Ideally, all database access (and updates in particular) will be be done
#    in the controller. However, in certain situations it only makes
#    sense to load or update data during report processing, so I leave it as
#    a judgement call on a case by case basis.
# 4. All data passed to a report class should be validated by the report class
#    in the initialize method. In most cases validation of the data at this point
#    consists of making sure the correct object types were supplied, or verifying
#    an option falls within a certain subset or range of values. Validation
#    failures in the report class should raise ArgumentError exception. If the
#    wrong data is passed to a report, the report should fail in initialization.
# 5. Data that fails validation in the to_pdf (or other utility) method should
#    not raise an exception. Once the to_pdf method is called, it should produce
#    a PDF document.
# 6. Data records that are skipped in the to_pdf method because of invalid or missing
#    data should be included in a 'list of errors' at the end of the report,
#    whenever possible and sensible.
#
# standard report header and footer
class ReportBase < Prawn::Document
  
  def initialize(*args)
    options = HashWithIndifferentAccess.new(margin: [20,20], page_size: 'LETTER', page_layout: :portrait)
    options.update(args.extract_options!)
    super(options)
  end

  def standard_letterhead(*args)
    options = args.extract_options!
    repeat(:all) do
      if options[:header]
        bounding_box([margin_box.left, margin_box.top - 10], width: margin_box.width, height: 80) do
          standard_header(options)
        end
      end
      bounding_box([margin_box.left, margin_box.bottom + 20], width: margin_box.width, height: 20) do
        standard_footer(options)
      end
    end
  end
  
  def standard_header(*args)
    options = args.extract_options!
    head1 = options[:header1] || SiteConfig.header_title
    head2 = options[:header2] || SiteConfig.header_subtitle1
    head3 = options[:header3] || SiteConfig.header_subtitle2
    head4 = options[:header4] || SiteConfig.header_subtitle3
    font("Helvetica")
    text("#{head1}", align: :center, size: 20, style: :bold)
    text("#{head2}", align: :center, size: 14)
    text("#{head3}", align: :center, size: 10)
    text("#{head4}", align: :center, size: 10)
    move_up(62)
    image(get_img_path(SiteImg.header_left_img), position: :left)
    move_up(75)
    image(get_img_path(SiteImg.header_right_img), position: :right)
    move_down(5)
    stroke_horizontal_rule
    font("Times-Roman")
  end
  
  def standard_footer(*args)
    options = args.extract_options!
    if options[:footer_no].nil?
      foot_no = Time.now.to_s(:timestamp)
    else
      foot_no = options[:footer_no]
    end
    foot_txt = options[:footer_txt] || "'" + SiteConfig.footer_text + "'"
    font("Helvetica")
    stroke_horizontal_rule
    move_down(5)
    unless foot_no == false
      text("No: #{foot_no}", align: :left, size: 9)
      move_up(10)
    end
    text("#{foot_txt}", align: :center, size: 12)
    font("Times-Roman")
  end
  
  def standard_report(*args, &block)
    options = HashWithIndifferentAccess.new(header: true)
    options.update(args.extract_options!)
    standard_letterhead(options)
    if options[:header]
      top_spacing = 100
    else
      top_spacing = 0
    end
    bounding_box([margin_box.left, margin_box.top - top_spacing], width: margin_box.width, height: margin_box.height - (top_spacing + 25)) do
      yield
    end
    number_pages("Page: <page> of <total>", at: [bounds.right - 65, 15])
    render
  end
  
  def col(number, pos = nil)
    return 0 if number.nil? || !number.is_a?(Integer)
    one_col_width = (bounds.width / 4).to_i * 4
    two_col_width = (one_col_width / 2).to_i
    four_col_width = (one_col_width / 4).to_i
    case number
    when 1
      case pos
      when nil
        return one_col_width
      when 1
        return 0
      else
        return nil
      end
    when 2
      case pos
      when nil
        return two_col_width
      when 1
        return 0
      when 2
        return two_col_width
      else
        return nil
      end
    when 4
      case pos
      when nil
        return four_col_width
      when 1
        return 0
      when 2
        return four_col_width
      when 3
        return two_col_width
      when 4
        return two_col_width + four_col_width
      else
        return nil
      end
    else
      return nil
    end
  end

  def text_field(field_position = [0,0], field_width = 100, field_label = 'Test', field_text = 'Test Results', *args)
    options = HashWithIndifferentAccess.new(rows: 1, pdf_color_scheme: 'header')
    options.update(args.extract_options!)
    if options[:pdf_color_scheme] == 'section'
      field_border_color = SiteConfig.pdf_section_bg
      field_txt_color = SiteConfig.pdf_section_txt
    elsif options[:pdf_color_scheme] == 'em'
      field_border_color = SiteConfig.pdf_em_bg
      field_txt_color = SiteConfig.pdf_em_txt
    else
      field_border_color = SiteConfig.pdf_header_bg
      field_txt_color = SiteConfig.pdf_header_txt
    end
    if options[:rows].nil? || options[:rows].to_i <= 0
      rows_covered = 1
    else
      rows_covered = options[:rows].to_i
    end
    field_text = field_text.to_s
    bounding_box(field_position, width: field_width, height: 25 * rows_covered) do
      # draw the bounding box from coords width wide and 20 high
      stroke_color(field_border_color)
      stroke_bounds
      stroke_color('000000')
      # Write the label (tiny) in the upper left corner
      fill_color(field_txt_color)
      draw_text(field_label, at: [bounds.left + 1, bounds.top - 6], size: 8)
      fill_color('000000')
      # Write the text in the box
      unless field_text.nil?
        bounding_box([bounds.left + 10, bounds.top - 8], width: bounds.width - 10, height: bounds.height - 8) do
          lines = field_text.split(/\n/).compact
          lines.each do |l|
            if rows_covered == 1 || (rows_covered > 1 && lines.size > 1)
              while width_of(l, size: 14) > bounds.width - 12
                l = l.chop
              end
            end
            if ['section','em'].include?(options[:pdf_color_scheme])
              text(l, size: 14, color: field_txt_color, inline_format: true)
            else
              text(l, size: 14, inline_format: true)
            end
          end
        end
      end
      stroke_color('000000')
    end
    return 25 * rows_covered
  end
  
  def check_box_span(field_width, *args, &block)
    options = args.extract_options!
    cb_image = "#{Rails.root}/app/assets/images/checkbox.png"
    y_position = cursor
    if options[:position].blank?
      options[:position] = 0
    end
    image(cb_image, width: 10, position: options[:position])
    move_cursor_to(y_position)
    span(field_width, position: options[:position] + 15) do
      yield
    end
  end
  
  def signature_line(*args, &block)
    options = args.extract_options!
    sig_placement = options[:placement]
    options.delete(:placement)
    if sig_placement.nil?
      sig_placement = :right
    end
    sig_width = col(2) - 3
    if sig_placement == :left
      sig_position = col(2,1)
    else
      sig_position = col(2,2) + 6
    end
    if options[:size].nil?
      options[:size] = 10
    end
    stroke_horizontal_line(sig_position, sig_position + sig_width)
    move_down(2)
    span(sig_width, position: sig_position) do
      if args.empty?
        yield
      else
        args.each do |sig_text|
          text(sig_text, options)
        end
      end
    end
  end
  
  def heading(text)
    font('Helvetica')
    cell(content: text.upcase.split(//).join('  '), width: bounds.width, background_color: SiteConfig.pdf_heading_bg, text_color: SiteConfig.pdf_heading_txt, size: 11, font_style: :bold, align: :center, borders: [], padding: 1)
    move_down(25)
    font('Times-Roman')
  end
  
  def money(value)
    sprintf("%.2f", value)
  end
  
  def currency(value)
    "$" + sprintf("%.2f", value)
  end
  
  def get_img_path(image, thumbnail=nil)
    if thumbnail.nil?
      thumbnail = :original
    end
    unless image.path(thumbnail).blank?
      if File.exists?(image.path(thumbnail))
        return image.path(thumbnail)
      end
    end
    # no image - using URL to generate path for default image
    img_url = image.url(thumbnail)
    # if RAILS_RELATIVE_URL_ROOT is set, need to remove it from image.url
    unless ENV['RAILS_RELATIVE_URL_ROOT'].blank?
      img_url = img_url.sub(/^#{ENV['RAILS_RELATIVE_URL_ROOT']}/,'')
    end
    if File.exists?("#{Rails.root}/public#{img_url}")
      return "#{Rails.root}/public#{img_url}"
    end
    # not found - bogus image data - use no-image default file
    return "#{Rails.root}/public/system/default_images/no_image_small.jpg"
  end
end