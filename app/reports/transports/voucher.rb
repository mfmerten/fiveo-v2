module Transports
  class Voucher < ReportBase
    def initialize(transport)
      super
      @transport = transport
      unless @transport.is_a?(Transport)
        raise ArgumentError.new("Transports::Voucher.initialize: First argument must be a valid Transport object.")
      end
    end
  
    def to_pdf
      standard_report(footer_no: @transport.id.to_s) do
        text("Transportation Voucher", size: 18, align: :center, style: :bold)
        text(AppUtils.format_date(@transport.begin_datetime.to_date), size: 14, style: :bold, align: :center)
        move_down(30)
        data = []
        unless @transport.person.nil?
          data.push(["Person:", "#{AppUtils.show_person(@transport.person)}" ])
        end
        if @transport.officer1?
          data.push(["Officer:", "#{AppUtils.show_contact(@transport, field: :officer1)}"])
        end
        if @transport.officer2?
          data.push(["Officer:", "#{AppUtils.show_contact(@transport, field: :officer2)}"])
        end
        unless data.empty?
          table(data, header: false) do
            columns(0..-1).style(borders: [], padding: 2, size: 14)
            column(0).style(align: :right, font_style: :bold)
            column(1).style(align: :left)
          end
        end
        move_down(20)
        data = [
          ["Transport From","Transport To"],
          [@transport.from,@transport.to],
          [@transport.from_address_line1,@transport.to_address_line1],
          [@transport.from_address_line2, @transport.to_address_line2]
        ]
        width = bounds.right / 2
        table(data, header: true) do
          columns(0..-1).style(borders: [], size: 14, align: :left, width: width)
          row(0).style(borders: [:bottom], size: 9, font_style: :bold)
        end
        move_down(30)
        data = [
          ["","Beginning", "Ending", "Difference"],
          ["Date/Time:",AppUtils.format_date(@transport.begin_datetime), AppUtils.format_date(@transport.end_datetime), "#{@transport.duration}"],
          ["Odometer:", @transport.begin_miles, @transport.end_miles, "#{@transport.mileage} miles"]
        ]
        width = bounds.right / 7
        table(data, header: true, width: bounds.width) do
          columns(0..-1).style(borders: [], size: 12, align: :left)
          column(0).style(align: :right, width: 100, font_style: :bold)
          column(1).style(width: 125)
          column(2).style(width: 125)
          row(0).style(borders: [:bottom], size: 9, font_style: :bold)
        end
        move_down(30)
        heading("Remarks")
        text(@transport.remarks, size: 12)
      end
    end
  end
end
