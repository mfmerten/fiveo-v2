module InvestReports
  class Form < ReportBase
    def initialize(invest_report)
      super(margin: [20,20])
      @invest_report = invest_report
      unless @invest_report.is_a?(InvestReport)
        raise ArgumentError.new("InvestReports::Form.initialize: First argument must be a valid InvestReport object.")
      end
    end
  
    def to_pdf
      standard_report(footer_no: @invest_report.id.to_s) do
        if @invest_report.supplemental?
          text("Supplemental Report", size: 18, align: :center, style: :bold)
        else
          text("Investigative Report", size: 18, align: :center, style: :bold)
        end
        text(AppUtils.format_date(@invest_report.report_datetime), size: 14, style: :bold, align: :center)
        data = []
        unless @invest_report.investigation.nil? || @invest_report.investigation.cases.empty?
          cases = @invest_report.investigation.cases.collect{|c| c.case_no}.compact.uniq.join(', ')
          data.push(["Case No(s):", cases])
        end
        unless @invest_report.victims.empty?
          @invest_report.victims.each do |v|
            address = (v.physical_line1.blank? ? ' ' : v.physical_line1) + (v.physical_line2.blank? ? '' : ', ' + v.physical_line2)
            data.push(["Victim:", "#{v.sort_name}, #{address}"])
          end
        end
        unless @invest_report.criminals.empty?
          @invest_report.criminals.each do |c|
            if c.person.nil?
              name = c.sort_name
              if c.description?
                address = "Description: #{c.description}"
              else
                address = " "
              end
            else
              name = AppUtils.show_person(c.person)
              address = (c.person.physical_line1.blank? ? ' ' : c.person.physical_line1) + (c.person.physical_line2.blank? ? '' : ', ' + c.person.physical_line2)
            end
            data.push(["Suspect:", "#{name}, #{address}"])
          end
        end
        unless @invest_report.arrests.empty?
          @invest_report.arrests.each do |a|
            data.push(["Arrest:", "#{AppUtils.format_date(a.arrest_datetime)}  Name: #{a.person.sort_name}\nCharges: #{a.charges.join(', ')}"])
          end
        end
        unless @invest_report.vehicles.empty?
          @invest_report.vehicles.each do |v|
            data.push(["Vehicle:", "#{v.desc}"])
          end
        end
        unless data.empty?
          table(data, header: false) do
            columns(0..-1).style(size: 12, borders: [], align: :left, padding: 1)
            column(0).style(align: :right, font_style: :bold)
            column(1).style(padding_left: 5)
          end
        end
        move_down(5)
        heading("Report Details")
        text(@invest_report.details, size: 12, indent_paragraphs: 20)
        move_down(10)
        data = []
        unless @invest_report.witnesses.empty?
          @invest_report.witnesses.each do |w|
            address = (w.physical_line1.blank? ? ' ' : w.physical_line1) + (w.physical_line2.blank? ? '' : ', ' + w.physical_line2)
            data.push(["Witness:", "#{w.sort_name}, #{address}"])
          end
        end
        unless data.empty?
          table(data, header: false) do
            columns(0..-1).style(size: 12, borders: [], align: :left, padding: 1)
            column(0).style(align: :right, font_style: :bold)
            column(1).style(padding_left: 5)
          end
        end
        move_down(40)
        signature_line(AppUtils.show_user(@invest_report.creator))
        unless @invest_report.photos.empty?
          start_new_page
          text("Investigation Photos", size: 24, style: :bold, align: :center)
          @invest_report.photos.each do |p|
            move_down(20)
            image(get_img_path(p.invest_photo, :medium), position: :center)
            text(p.description, size: 12, style: :bold, align: :center)
          end
        end
      end
    end
  end
end