module Calls
  class ShiftReport < ReportBase
    def initialize(date, shift, calls=[])
      super
      @date = date
      unless @date.is_a?(Date)
        raise ArgumentError.new("Calls::ShiftReport.initialize: First argument must be a Date object.")
      end
      @shift = shift
      unless @shift.is_a?(Integer)
        raise ArgumentError.new("Calls::ShiftReport.initialize: Second argument must be an Integer.")
      end
      @calls = calls
      unless @calls.is_a?(Array) || @calls.is_a?(ActiveRecord::Relation)
        raise ArgumentError.new("Calls::ShiftReport.initialize: Third argument must be an Array of Call objects.")
      end
      @calls.each do |c|
        unless c.is_a?(Call)
          raise ArgumentError.new("Calls::ShiftReport.initialize: Third argument must be an Array of Call objects: supplied Array contains invalid object type(s)!")
        end
      end
    end
  
    def to_pdf
      standard_report do
        text("IN-HOUSE MEMO", align: :center, size: 18, style: :bold)
        move_down(5)
        text("Dispatcher Shift Report", align: :center, size: 16, style: :bold)
        start = SiteConfig.send("dispatch_shift#{@shift}_start")
        stop = SiteConfig.send("dispatch_shift#{@shift}_stop")
        text("#{AppUtils.format_date(@date)} - Shift #{@shift} (#{start}-#{stop})", size: 14, style: :bold, align: :center)
        data = []
        sep = "\n"
        @calls.each do |c|
          txt = "<b>Type:</b> #{c.received_via.blank? ? 'Unknown' : c.received_via}     <b>Signal:</b> #{c.signal_code}     <b>Dispatcher:</b> #{AppUtils.show_contact(c, field: :dispatcher)}#{sep}"
          c.call_subjects.each do |s|
            txt << "<b>#{s.subject_type.blank? ? 'Subject' : s.subject_type}:</b> #{s.sort_name}#{s.home_phone? ? ' (<b>Home:</b> ' + s.home_phone + ')' : ''}#{s.cell_phone? ? ' (<b>Cell:</b> ' + s.cell_phone + ')' : ''}#{s.work_phone? ? ' (<b>Work:</b> ' + s.work_phone + ')' : ''}#{sep}"
          end
          unless c.units.blank?
            txt << "<b>Units Dispatched:</b> #{c.units}#{sep}"
          end
          unless c.disposition.blank?
            txt << "<b>Disposition:</b> #{c.disposition}#{sep}"
          end
          if c.detective?
            txt << "<b>Detective Assigned:</b> #{c.detective? ? AppUtils.show_contact(c, field: :detective) : ''}#{sep}"
          end
          txt << sep + "<b>Details:</b> " + c.details
          data.push(["#{AppUtils.format_date(c.call_datetime)}",txt])
        end
        data.unshift(["Date/Time",'Call'])
        table(data, header: true) do
          rows(1..-1).borders = [:left, :right, :top, :bottom]
          rows(1..-1).padding = 3
          columns(0..-1).style(align: :left, size: 12)
          column(0).style(width: 75)
          column(1).style(inline_format: true)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
        end
        move_down(5)
        text("End Of Report", size: 12, style: :bold)
      end
    end
  end
end
