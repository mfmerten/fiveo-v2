module Calls
  class Sheet < ReportBase
    def initialize(call)
      super
      @call = call
      unless @call.is_a?(Call)
        raise ArgumentError.new("Calls::Sheet.initialize: First argument must be a valid Call object.")
      end
    end
  
    def to_pdf
      standard_report(footer_no: @call.id.to_s) do 
        text("Case Number", size: 9)
        move_up(10)
        text("Case Number", size: 9, align: :right)
        text(@call.case_no, size: 18, style: :bold)
        move_up(19)
        text(@call.case_no, size: 18, style: :bold, align: :right)
        move_up(29)
        text("Initial Report", align: :center, size: 24, style: :bold)
        location = []
        if !SiteConfig.wards.empty? && @call.ward?
          location.push("Ward: #{@call.ward}")
        end
        if !SiteConfig.districts.empty? && @call.district?
          location.push("District: #{@call.district}")
        end
        if !SiteConfig.zones.empty? && @call.zone?
          location.push("Zone: #{@call.zone}")
        end
        row_position = cursor
        text_field([col(2,1), row_position], col(2), "Date/Time:", AppUtils.format_date(@call.call_datetime))
        row_position -= text_field([col(2,2), row_position], col(2), "Location:", "#{location.join("  ")}")
        row_position -= text_field([col(1,1), row_position], col(1), "Signal:", "#{@call.signal_code}  #{@call.signal_name}")
        text_field([col(2,1), row_position], col(2), "Received VIA:", @call.received_via)
        row_position -= text_field([col(2,2), row_position], col(2), "Received By:", AppUtils.show_contact(@call, field: :received_by))
        @call.call_subjects.each do |s|
          text_field([col(2,1), row_position], col(2), "#{s.subject_type.blank? ? 'Subject' : s.subject_type}:", "<b>#{s.sort_name}</b>\n#{s.address}", rows: 2)
          text_field([col(4,3), row_position], col(4), "Home Phone:", s.home_phone)
          row_position -= text_field([col(4,4), row_position], col(4), "Cell Phone:", s.cell_phone)
          text_field([col(4,3), row_position], col(4), "Work Phone:", s.work_phone)
          row_position -= text_field([col(4,4), row_position], col(4), "Other:", " ")
        end
        row_position -= text_field([col(1,1), row_position], col(1), "Unit(s) Sent:", @call.units, rows: 2)
        text_field([col(2,1), row_position], col(2), "Dispatched:", AppUtils.format_date(@call.dispatch_datetime))
        row_position -= text_field([col(2,2), row_position], col(2), "Arrived:", AppUtils.format_date(@call.arrival_datetime))
        text_field([col(2,1), row_position], col(2), "Concluded:", AppUtils.format_date(@call.concluded_datetime))
        row_position -= text_field([col(2,2), row_position], col(2), "Disposition:", @call.disposition)
        text_field([col(2,1), row_position], col(2), "Dispatcher:", AppUtils.show_contact(@call, field: :dispatcher))
        row_position -= text_field([col(2,2), row_position], col(2), "Assigned To:", AppUtils.show_contact(@call, field: :detective))
        
        move_down(5)
        text("Explanation of Call", size: 18, align: :center, style: :bold)
        text(@call.details, size: 12)
      end
    end
  end
end