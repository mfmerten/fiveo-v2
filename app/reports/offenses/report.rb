module Offenses
  class Report < ReportBase
    def initialize(offense)
      super(page_size: 'LEGAL')
      @offense = offense
      unless @offense.is_a?(Offense)
        raise ArgumentError.new("Offenses::Report.initialize: First argument must be a valid Offense object.")
      end
    end
  
    def to_pdf
      standard_report(footer_no: @offense.id.to_s) do
        text("Offense Report", size: 18, align: :center, style: :bold)
        text(AppUtils.format_date(@offense.offense_datetime), size: 14, style: :bold, align: :center)
        data = [["CASE:", @offense.case_no]]
        unless @offense.victims.empty?
          @offense.victims.each do |v|
            address = v.physical_line1 + ", " + v.physical_line2
            data.push(["Victim:", "#{v.sort_name}, #{address}"])
          end
        end
        data.push(["Location:", @offense.location])
        unless @offense.suspects.empty?
          @offense.suspects.each do |s|
            address = s.physical_line1 + ", " + s.physical_line2
            data.push(["Suspect:", "#{s.sort_name}, #{address}"])
          end
        end
        table(data, header: false, width: bounds.width) do
          columns(0..-1).style(size: 12, borders: [], align: :left, padding: 1)
          column(0).style(align: :right, font_style: :bold, width: 75)
          column(1).style(padding_left: 5)
        end
        move_down(5)
        
        heading("Details Of Offense")
        text(@offense.details, size: 12)
        move_down(5)
        
        unless @offense.wreckers.empty?
          heading("Wreckers Used")
          data = []
          @offense.wreckers.each do |w|
            data.push([w.name, "#{w.by_request? ? 'Yes' : 'No'}", w.location, w.tag, w.vin])
          end
          unless data.empty?
            data.unshift(['Wrecker Service','Req?','Storage Location','Vehicle Tag','VIN'])
            table(data, header: true, width: bounds.width, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]) do
              rows(1..-1).padding = 1
              columns(0..-1).style(align: :left, size: 12, borders: [])
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
          end
          move_down(5)
        end
        
        heading("Documentation")
        counter = 1
        if @offense.pictures? || !@offense.offense_photos.empty?
          text("#{counter.to_s}: Pictures (misc)")
          counter += 1
        end
        if @offense.restitution_form?
          text("#{counter.to_s}: Restitution Form")
          counter += 1
        end
        if @offense.victim_notification?
          text("#{counter.to_s}: Victim Notification")
          counter += 1
        end
        if @offense.perm_to_search?
          text("#{counter.to_s}: Permission To Search Form")
          counter += 1
        end
        if @offense.misd_summons?
          text("#{counter.to_s}: Misdemeanor Summons")
          counter += 1
        end
        if @offense.fortyeight_hour?
          text("#{counter.to_s}: 48 Hour")
          counter += 1
        end
        if @offense.medical_release?
          text("#{counter.to_s}: Medical Release")
          counter += 1
        end
        @offense.witnesses.each do |w|
          text("#{counter.to_s}: Witness Statement: #{w.sort_name}")
          counter += 1
        end
        @offense.citations.each do |c|
          text("#{counter.to_s}: Citation ##{c.ticket_no} -- To: #{AppUtils.show_person c.person} -- For: #{c.charge_summary}")
          counter += 1
        end
        @offense.forbids.each do |f|
          text("#{counter.to_s}: Forbidden Form #{f.id} -- By: #{f.complainant}  For: #{f.sort_name}")
          counter += 1
        end
        if @offense.other_documents?
          text("#{counter.to_s}: Other Documents: #{@offense.other_documents}")
          counter += 1
        end
        move_down(40)
        signature_line(AppUtils.show_contact(@offense, field: :officer), size: 14)
      end
    end
  end
end
