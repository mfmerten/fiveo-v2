module Probations
  class ActiveList < ReportBase
    def initialize(probations)
      super
      @probations = probations
      unless @probations.is_a?(Array)
        raise ArgumentError.new("Probations::ActiveList.initialize: First argument must be an Array of Probation objects.")
      end
      @probations.each do |p|
        unless p.is_a?(Probation)
          raise ArgumentError.new("Probations::ActiveList.initialize: First argument must be an Array of Probation objects: supplied Array contains invalid object type(s).")
        end
      end
    end
  
    def to_pdf
      standard_report do
        text("Active Probation Listing", align: :center, size: 18, style: :bold)
        text(Time.now.to_s(:us), align: :center, size: 14, style: :bold)
        text("Names shown in red have <b>active</b> warrants!", align: :center, size: 10, inline_format: true)
        # group the records by person
        people = {bad: BigDecimal.new("0.0",2)}
        @probations.each do |p|
          if p.person.nil?
            people[:bad] += 1
            next
          end
          if people[p.person_id].nil?
            people[p.person_id] = {records: [], name: p.person.sort_name, dob: p.person.date_of_birth, wanted: p.person.is_wanted?}
          end
          people[p.person_id][:records].push(p)
        end
        move_down(5)
        # print table for each person
        people.keys.reject{|k| !k.is_a?(Integer)}.collect{|k| [k,people[k][:name],people[k][:dob],people[k][:wanted],people[k][:records]]}.sort{|a,b| a[1] <=> b[1]}.each do |person_id, name, dob, wanted, records|
          data = []
          records.each do |p|
            data.push([p.id, "#{p.conviction} ","#{AppUtils.format_date(p.start_date)} ","#{AppUtils.format_date(p.end_date)} "])
          end
          if cursor <= 50
            start_new_page
          else
            move_down(10)
          end
          if wanted
            fill_color(SiteConfig.pdf_em_txt)
          end
          text("<b>#{name}</b> [#{person_id}] -- Date of Birth: #{AppUtils.format_date(dob)}", size: 14, inline_format: true)
          fill_color('000000')
          unless data.empty?
            data.unshift(["Prob ID", "Conviction", "Start", "End"])
            table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width - 20, position: :center) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom] 
              rows(1..-1).padding = 3
              columns(0..-1).style(align: :left, size: 12, padding: 2)
              column(0).style(width: 75)
              # column 1 gets remaining
              column(2).style(width: 65)
              column(3).style(width: 65)
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
          end
        end
        move_down(5)
        text("END OF REPORT", size: 12)
      end
    end
  end
end