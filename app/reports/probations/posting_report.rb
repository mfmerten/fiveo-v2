module Probations
  class PostingReport < ReportBase
    def initialize(status, report_time, payments)
      super(page_layout: :landscape)
      @status = status
      unless @status.is_a?(String)
        raise ArgumentError.new("Probations::PostingReport.initialize: First argument must be a String object.")
      end
      unless ['post','preview','reprint'].include?(@status)
        raise ArgumentError.new("Probations::PostingReport.initialize: First argument must be a String object and must be one of 'post', 'preview' or 'reprint'.")
      end
      @report_time = report_time
      unless @report_time.is_a?(Time)
        raise ArgumentError.new("Probations::PostingReport.initialize: Second argument must be a Time object.")
      end
      @payments = payments
      unless @payments.is_a?(Array)
        raise ArgumentError.new("Probations::PostingReport.initialize: Third argument must be an Array of ProbationPayment objects.")
      end
      @payments.each do |p|
        unless p.is_a?(ProbationPayment)
          raise ArgumentError.new("Probations::PostingReport.initialize: Third argument must be an Array of ProbationPayment objects: supplied Array contains invalid object type(s).")
        end
      end
    end
  
    def to_pdf
      # special header/footer
      repeat :all do
        bounding_box([margin_box.left, margin_box.top - 10], width: margin_box.width, height: 24) do
          if @status == 'preview'
            page_title = "Probation Posting Report (PREVIEW)"
          else
            page_title = "Probation Posting Report"
          end
          font("Helvetica")
          text(page_title, align: :center, size: 16, style: :bold)
          move_up(14)
          text(AppUtils.format_date(@report_time), align: :right, size: 12, style: :bold)
          stroke_horizontal_rule
          font("Times-Roman")
        end
        bounding_box([margin_box.left, margin_box.bottom + 20], width: margin_box.width, height: 20) do
          font("Helvetica")
          stroke_horizontal_rule
          move_down(5)
          text("No: #{Time.now.to_s(:timestamp)}", align: :left, size: 9, style: :bold)
          move_up(10)
          text("#{SiteConfig.agency}", align: :center, size: 12)
          font("Times-Roman")
        end
      end
      bounding_box([margin_box.left, margin_box.top - 35], width: margin_box.width, height: margin_box.height - 60) do
        @funds = @payments.collect{|p| [p.fund1_name, p.fund2_name, p.fund3_name, p.fund4_name, p.fund5_name, p.fund6_name, p.fund7_name, p.fund8_name, p.fund9_name]}.flatten.compact.uniq.reject{|f| f.blank?}
        grand_charges = grand_payments = grand_net = BigDecimal.new("0.00",2)
        @funds.each do |f|
          total_charges = total_payments = total_net = BigDecimal.new("0.00",2)
          data = []
          @payments.each do |p|
            person_name = " "
            unless p.probation.nil?
              unless p.probation.person.nil?
                person_name = "#{p.probation.person.sort_name}"
              end
            end
            (1..9).each do |x|
              if p.send("fund#{x}_name") == f && (p.send("fund#{x}_charge") != 0 || p.send("fund#{x}_payment") != 0)
                pay = p.send("fund#{x}_payment")
                if pay.nil?
                  pay = BigDecimal.new("0.00",2)
                end
                chg = p.send("fund#{x}_charge")
                if chg.nil?
                  chg = BigDecimal.new("0.00",2)
                end
                total_charges += chg
                total_payments += pay
                total_net += (chg - pay)
                data.push([p.id, p.probation_id, AppUtils.format_date(p.transaction_date), person_name, p.memo, p.receipt_no, money(chg), money(-pay), money(total_net)])
              end
            end
          end
          if @status == 'post'
            # post the transactions
            @payments.each{|p| p.update_attribute(:report_datetime, @report_time)}
          end
          unless data.empty?
            if cursor <= 50
              start_new_page
            elsif f != @funds.first
              move_down(10)
            end
            text("#{f}", size: 12, style: :bold)
            data.unshift(["Trans","Prob", "Date", "Person", "Memo", "Receipt No", "Charge", "Credit", "Net"])
            table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom] 
              rows(1..-1).padding = 3
              columns(0..-1).style(align: :left, size: 11, padding: 2)
              column(0).style(width: 55)
              column(1).style(width: 55)
              column(2).style(width: 60)
              column(3).style(width: 150)
              # columns(4) gets the difference
              column(5).style(width: 100)
              column(6).style(width: 65, align: :right)
              column(7).style(width: 65, align: :right)
              column(8).style(width: 75, align: :right)
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
          end
          grand_charges += total_charges
          grand_payments += total_payments
        end
  
        grand_net = grand_charges - grand_payments
        if cursor <= 50
          start_new_page
        else
          move_down(10)
        end
        stroke_horizontal_rule
        move_down(2)
        stroke_horizontal_rule
        move_down(5)
        data = [
          ["Trans","Prob", "Date", "Person", "Memo", "Receipt No", "Charge", "Credit", "Net"],
          [" "," "," "," "," ","Grand Totals:",money(grand_charges), money(-grand_payments), money(grand_net)]
        ]
        table(data, header: true, width: bounds.width) do
          rows(1..-2).borders = [:left, :right]
          rows(-1).style(borders: [:left, :right, :bottom], font_style: :bold)
          rows(1..-1).padding = 3
          columns(0..-1).style(align: :left, size: 11, padding: 2)
          column(0).style(width: 55)
          column(1).style(width: 55)
          column(2).style(width: 60)
          column(3).style(width: 150)
          # column(4) gets the difference
          column(5).style(width: 100)
          column(6).style(width: 65, align: :right)
          column(7).style(width: 65, align: :right)
          column(8).style(width: 75, align: :right)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          row(-1).column(5).style(align: :right, borders: [:top, :bottom, :right])
          row(-1).column(0).borders = [:left, :top, :bottom]
          row(-1).columns(1..4).borders = [:top, :bottom]
        end
        move_down(5)
        text("END OF REPORT", size: 12)
      end
      number_pages("Page: <page> of <total>", at: [bounds.right - 65, 15])
      render
    end
  end
end
