module Probations
  class History < ReportBase
    def initialize(start_date, stop_date, probation, person, probation_payments)
      super
      @start_date = start_date
      unless @start_date.nil? || @start_date.is_a?(Date)
        raise ArgumentError.new("Probations::History.initialize: First argument must be a Date object or Nil.")
      end
      @stop_date = stop_date
      unless @stop_date.nil? || @stop_date.is_a?(Date)
        raise ArgumentError.new("Probations::History.initialize: Second argument must be a Date object or Nil.")
      end
      @probation = probation
      unless @probation.is_a?(Probation)
        raise ArgumentError.new("Probations::History.initialize: Third argument must be a Probation object or Nil.")
      end
      @person = person
      unless @person.is_a?(Person)
        raise ArgumentError.new("Probations::History.initialize: Fourth argument must be a Person object or Nil.")
      end
      @probation_payments = probation_payments
      unless @probation_payments.is_a?(Array)
        raise ArgumentError.new("Probations::History.initialize: Fifth argument must be an Array of ProbationPayment objects.")
      end
      @probation_payments.each do |p|
        unless p.is_a?(ProbationPayment)
          raise ArgumentError.new("Probations::History.initialize: Fifth argument must be an Array of ProbationPayment objects: supplied Array contains invalid object type(s).")
        end
      end
    end
  
    def to_pdf
      standard_report do
        if !@probation.nil?
          page_title = "Probation Transaction History (Probation #{@probation.id})"
        elsif !@person.nil?
          page_title = "Probation Transaction History (Person #{@person.id})"
        else
          page_title = "Probation Transaction History"
        end
        text(page_title, align: :center, size: 18, style: :bold)
        if @start_date.nil? && @stop_date.nil?
          text("Period:  All Recorded Transactions", align: :center, size: 14, style: :bold)
        elsif @start_date.nil?
          text("Period:  On or Before #{AppUtils.format_date(@stop_date)}", align: :center, size: 14, style: :bold)
        elsif @stop_date.nil?
          text("Period:  On or After #{AppUtils.format_date(@start_date)}", align: :center, size: 14, style: :bold)
        else
          text("Period:  #{AppUtils.format_date(@start_date)} -- #{AppUtils.format_date(@stop_date)}", align: :center, size: 14, style: :bold)
        end
        move_down(8)
        totals = {bad: 0}
        people = {}
        probations = {}
        @probation_payments.each do |p|
          if p.probation.nil? || p.probation.person.nil?
            totals[:bad] += 1
            next
          end
          person = p.probation.person
          probation = p.probation
          if people[person.id].nil?
            people[person.id] = person.sort_name
          end
          if probations[probation.id].nil?
            probations[probation.id] = {
              conviction: probation.conviction,
              status: (probation.status.blank? ? 'Unknown' : (probation.status == 'Terminated' ? probation.status + ': ' + probation.terminate_reason : probation.status))
            }
          end
          if totals[person.id].nil?
            totals[person.id] = {}
          end
          if totals[person.id][probation.id].nil?
            totals[person.id][probation.id] = {charges: BigDecimal.new("0.0",2),payments: BigDecimal.new("0.0",2),records: []}
          end
          totals[person.id][probation.id][:charges] += p.total_charges
          totals[person.id][probation.id][:payments] -= p.total_payments
          totals[person.id][probation.id][:records].push(p)
        end
        if people.keys.empty?
          move_down(40)
          text("Nothing To Report", size: 14, style: :bold, align: :center)
        else
          people.keys.collect{|k| [k, people[k]]}.sort{|a,b| a[1] <=> b[1]}.each do |person_id, name|
            if cursor <= 75
              start_new_page
            end
            text("<b>#{name}</b> [#{person_id}]", size: 14, inline_format: true)
            totals[person_id].keys.reject{|k| !k.is_a?(Integer)}.sort.each do |probation_id|
              data = [[probation_id,"Status:",probations[probation_id][:status],"Conviction:",probations[probation_id][:conviction]]]
              indent(25) do
                text("Probation:", size: 12, style: :bold)
              end
              move_up(21)
              table(data, header: false, width: bounds.width - 85, position: 85) do
                columns(0..-1).style(borders: [], size: 12, padding: 5, align: :left)
                column(0).style(width: 50)
                column(1).style(width: 45, align: :right, font_style: :bold)
                column(2).style(width: 55)
                column(3).style(width: 70, align: :right, font_style: :bold)
                # last column gets remaining width
              end
              # generate table data
              if @start_date.nil?
                pbal = BigDecimal.new("0.00",2)
              else
                pbal = probation.balance_on(@start_date)
              end
              data = []
              totals[person_id][probation_id][:records].each do |p|
                pbal = pbal + p.total_charges - p.total_payments
                data.push(["#{p.report_datetime? ? '*' : ' '}", p.id, AppUtils.format_date(p.transaction_date), p.memo, p.receipt_no, money(p.total_charges), money((p.total_payments == 0? 0.0 : -p.total_payments)), money(pbal)])
              end
              data.push([" ", " ", " ", " ", "Totals:", money(totals[person_id][probation_id][:charges]), money(totals[person_id][probation_id][:payments]), money(data[-1][-1])])
              data.unshift([" ", "Trans ID", "Date", "Memo", "Receipt No.", "Charged", "Credited", "Balance"])
              move_up(5)
              table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width - 25, position: 25) do
                rows(1..-2).borders = [:left, :right]
                rows(-1).borders = [:left, :right, :bottom] 
                rows(1..-1).padding = 3
                rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
                columns(0..-1).style(align: :left, size: 11, padding: 2)
                row(-1).column(0).borders = [:left, :top, :bottom]
                row(-1).columns(1..3).borders = [:top, :bottom]
                row(-1).column(4).style(borders: [:top, :bottom, :right], align: :right)
                column(0).style(width: 12)
                column(1).style(width: 55)
                column(2).style(width: 65)
                # column 3 gets remaining
                column(4).style(width: 90)
                column(5..7).style(width: 65, align: :right)
                rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
              end
              move_down(5)
            end
            move_down(10)
          end
        end
        if totals[:bad] > 0
          text("Note: #{totals[:bad]} transactions skipped because of invalid probation or person links!", size: 10)
          move_down(5)
        end
        text("END OF REPORT", size: 12)
      end
    end
  end
end