module Probations
  class Balance < ReportBase
    def initialize(probations)
      super
      @probations = probations
      unless @probations.is_a?(Array)
        raise ArgumentError.new("Probations::Balance.initialize: First argument must be an Array of Probation objects.")
      end
      @probations.each do |p|
        unless p.is_a?(Probation)
          raise ArgumentError.new("Probations::Balance.initialize: First argument must be an Array of Probation objects: supplied Array contains invalid object type(s).")
        end
      end
    end
  
    def to_pdf
      standard_report do
        text("Probation Balances Due", align: :center, size: 18, style: :bold)
        text(Time.now.to_s(:us), align: :center, size: 14, style: :bold)
        total_charges = total_payments = total_due = BigDecimal.new("0.0",2)
        data = []
        @probations.each do |p|
          total_charges += p.total_charged
          total_payments -= p.total_paid
          total_due += p.total_balance
          data.push([p.id, "#{AppUtils.show_person p.person}", money(p.total_charged), money(-p.total_paid), money(p.total_balance)])
        end
        data.push(["","Grand Totals:", money(total_charges), money(total_payments), money(total_due)])
        data.unshift(["ID", "Probationer", "Charged", "Credited", "Due"])
        table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom] 
          rows(1..-1).padding = 3
          row(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
          row(-1).column(0).borders = [:left, :top, :bottom]
          columns(0..-1).style(align: :left, size: 12, padding: 2)
          row(-1).column(1).style(borders: [:right, :top, :bottom], align: :right)
          column(2..4).style(align: :right)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
        end
        move_down(5)
        text("END OF REPORT", size: 12)
      end
    end
  end
end