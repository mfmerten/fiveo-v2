module Contacts
  class List < ReportBase
    def initialize(contacts, query=nil, title=nil)
      super
      @contacts = contacts
      unless @contacts.is_a?(Array)
        raise ArgumentError.new("Contacts::List.initialize: First argument must be an Array of Contact objects.")
      end
      @contacts.each do |c|
        unless c.is_a?(Contact)
          raise ArgumentError.new("Contacts::List.initialize: First argument must be an Array of Contact objects: supplied Array contains invalid object type(s)!")
        end
      end
      @query = query
      unless @query.nil? || @query.is_a?(String)
        raise ArgumentError.new("Contacts::List.initialize: Second argument must be a String object or Nil.")
      end
      @title = title
      unless @title.nil? || @title.is_a?(String)
        raise ArgumentError.new("Contacts::List.initialize: Third argument must be a String object or Nil.")
      end
    end
    
    def to_pdf
      standard_report do
        if @title.blank?
          if @query.blank?
            text("Contact List", align: :center, size: 18, style: :bold)
          else
            text("Contact List (Filtered)", align: :center, size: 18, style: :bold)
          end
        else
          text(@title, align: :center, size: 18, style: :bold)
        end
        text(Date.today.to_s(:us), align: :center, size: 12, style: :bold)
        unless @query.blank? || !@title.blank?
          move_down(5)
          text("Filter:", size: 12, style: :bold)
          move_up(12)
          span(bounds.width - 40, position: 40) do
            text("#{@query}", size: 12)
          end
        end
        move_down(5)
        data = []
        @contacts.each do |c|
          data.push([c.sort_name, c.phones])
        end
        if data.empty?
          move_down(40)
          text("Nothing To Report!", size: 14, style: :bold, align: :center)
        else
          # print the list
          data.unshift(["Name", "Phones"])
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
            rows(1..-1).padding = 3
            columns(0..-1).style(align: :left, size: 12, borders: [])
            column(0).style(width: 175)
            # column(1) gets whats left
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
        end
      end
    end
  end
end