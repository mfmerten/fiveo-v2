module Contacts
  class EmployeeList < ReportBase
    def initialize(contact, contacts, numeric_sort=true)
      super
      @contact = contact
      unless @contact.is_a?(Contact)
        raise ArgumentError.new("Contacts::EmployeeList.initialize: First argument must be a valid Contact object.")
      end
      @contacts = contacts
      unless @contacts.is_a?(Array)
        raise ArgumentError.new("Contacts::EmployeeList.initialize: Second argument must be an Array of Contact objects.")
      end
      @contacts.each do |c|
        unless c.is_a?(Contact)
          raise ArgumentError.new("Contacts::EmployeeList.initialize: Second argument must be an Array of Contact objects: supplied Array contains invalid object type(s)!")
        end
      end
      @numeric_sort = (numeric_sort == true)
    end
    
    def to_pdf
      standard_report do
        text("Contact List for #{@contact.sort_name}", align: :center, size: 18, style: :bold)
        text(Date.today.to_s(:us), align: :center, size: 12, style: :bold)
        data = []
        @contacts.each do |c|
          data.push([c.unit, c.sort_name, c.phones])
        end
        if data.empty?
          move_down(40)
          text("Nothing To Report!", size: 14, style: :bold, align: :center)
        else
          # sort the list (missing units get 'zzzzzz' so they should end up on the bottom)
          if @numeric_sort
            # need to pick out the number string and sort by that numerically first
            data = data.sort_by do |d|
              # ensure blanks or nils come last instead of first
              d_not_blank = d[0].blank? ? 'zzzzzz' : d[0]
              # check for a number string embedded in text
              d_number = d_not_blank.sub(/^[^\d]*/,'').to_i
              # remove found number from original
              d_wo_number_start, d_wo_number_finish = d_not_blank.split(d_number.to_s)
              [d_wo_number_start, d_number, d_wo_number_finish, d[1]]
            end
          else
            # straight ascii sort - still want blank units last though
            data = data.sort do |a,b|
              a_not_blank = a[0].blank? ? 'zzzzzz' : a[0]
              b_not_blank = b[0].blank? ? 'zzzzzz' : b[0]
              a_not_blank + a[1] <=> b_not_blank + b[1]
            end
          end
          # print the list
          data.unshift(["Unit", "Name", "Phones"])
          table(data, row_colors: [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even], header: true) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            rows(1..-1).style(align: :left, size: 12)
            column(0).width = 55
            column(1).width = 125
            column(2).style(width: 390, inline_format: true)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
        end
      end
    end
  end
end