module Medicals
  class Form < ReportBase
    def initialize(medical)
      super
      @medical = medical
      unless @medical.is_a?(Medical)
        raise ArgumentError.new("Medicals::Form.initialize: First argument must be a valid Medical object.")
      end
    end
  
    def to_pdf
      standard_report(footer_no: @medical.booking_id.to_s) do
        text("#{@medical.booking.nil? ? 'Invalid Booking!' : (@medical.booking.jail.nil? ? 'Invalid Jail!' : @medical.booking.jail.name)}", align: :center, size: 16, style: :bold)
        text("Medical Screening Report", align: :center, size: 18, style: :bold)
        text(AppUtils.format_date(@medical.screen_datetime), align: :center, size: 14, style: :bold)
        move_down(8)
        text("#{@medical.booking.nil? ? 'Invalid Booking' : AppUtils.show_person(@medical.booking.person)}", size: 16, style: :bold, align: :left)
        move_up(18)
        text("Booking ID: <b>#{@medical.booking.nil? ? '' : @medical.booking.id}</b>", inline_format: true, size: 14, align: :right)
        data = [
          ['Race','Sex','Date of Birth','Social Security'],
          ["#{@medical.booking.nil? ? '' : (@medical.booking.person.nil? ? '' : @medical.booking.person.race)} ", "#{@medical.booking.nil? ? '' : (@medical.booking.person.nil? ? '' : @medical.booking.person.sex_name)} ", "#{@medical.booking.nil? ? '' : (@medical.booking.person.nil? ? '' : AppUtils.format_date(@medical.booking.person.date_of_birth))} ", "#{@medical.booking.nil? ? '' : (@medical.booking.person.nil? ? '' : @medical.booking.person.ssn)} "]
        ]
        table(data, header: true, width: bounds.width) do
          columns(0..-1).style(borders: [], padding: 1)
          row(0).style(background_color: SiteConfig.pdf_header_bg, text_color: SiteConfig.pdf_header_txt, size: 9, font_style: :bold)
        end
        move_down(5)
  
        data = [
          ['Emergency Contact','Relationship','Address','Phone'],
          ["#{@medical.booking.nil? ? '' : (@medical.booking.person.nil? ? '' : @medical.booking.person.emergency_contact)} ","#{@medical.booking.nil? ? '' : (@medical.booking.person.nil? ? '' : @medical.booking.person.em_relationship)} ", "#{@medical.booking.nil? ? '' : (@medical.booking.person.nil? ? '' : @medical.booking.person.emergency_address)} ", "#{@medical.booking.nil? ? '' : (@medical.booking.person.nil? ? '' : @medical.booking.person.emergency_phone)} "]
        ]
        table(data, header: true, width: bounds.width) do
          columns(0..-1).style(borders: [], padding: 1)
          row(0).style(background_color: SiteConfig.pdf_em_bg, text_color: SiteConfig.pdf_em_txt, size: 9, font_style: :bold)
        end
        move_down(5)
        
        heading("Conditions Identified")
        data = []
        if @medical.arrest_injuries?
          data.push(["Injuries During Arrest", @medical.arrest_injuries])
        end
        if @medical.rec_med_tmnt?
          data.push(["Receiving Medical Treatment", @medical.rec_med_tmnt])
        end
        if @medical.rec_dental_tmnt?
          data.push(["Receiving Dental Treatment", @medical.rec_dental_tmnt])
        end
        if @medical.rec_mental_tmnt?
          data.push(["Receiving Mental Treatment", @medical.rec_mental_tmnt])
        end
        if @medical.current_meds?
          data.push(["Current Medications", @medical.current_meds])
        end
        if @medical.allergies?
          data.push(["Drug Allergies", @medical.allergies])
        end
        if @medical.injuries?
          data.push(["Bleeding/Injuries/Illness/Pain", @medical.injuries])
        end
        if @medical.infestation?
          data.push(["Infestations/Lesions", @medical.infestation])
        end
        if @medical.alcohol?
          data.push(["Under the Influence of Alcohol", @medical.alcohol])
        end
        if @medical.drug?
          data.push(["Under the Influence of Drugs", @medical.drug])
        end
        if @medical.withdrawal?
          data.push(["Exhibits Withdrawal Symptoms", @medical.withdrawal])
        end
        if @medical.attempt_suicide?
          data.push(["Has Considered or Attempted Suicide", @medical.attempt_suicide])
        end
        if @medical.suicide_risk?
          data.push(["Possible Suicide Risk", @medical.suicide_risk])
        end
        if @medical.danger?
          data.push(["Possible Danger to Self or Others", @medical.danger])
        end
        if @medical.pregnant?
          data.push(["Pregnant", @medical.pregnant])
        end
        if @medical.deformities?
          data.push(["Requires Cane/Crutches/Cast/Braces", @medical.deformities])
        end
        if data.empty?
          indent(10) do
            text("Nothing To Report", size: 12)
          end
          contact = false
        else
          contact = true
          data.unshift(["Condition", "Details"])
          move_up(5)
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], position: 10, width: bounds.width - 20) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            columns(0..-1).style(align: :left, padding: 1)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
        end
        move_down(5)
        
        heading("Additional Conditions Identified")
        data = []
        data << (@medical.heart_disease? ? "Heart Disease" : nil)
        data << (@medical.blood_pressure? ? "High Blood Pressure" : nil)
        data << (@medical.diabetes? ? "Diabetes" : nil)
        data << (@medical.epilepsy? ? "Epilepsy" : nil)
        data << (@medical.hepatitis? ? "Hepatitis" : nil)
        data << (@medical.hiv? ? "HIV/AIDS" : nil)
        data << (@medical.tb? ? "Tuberculosis" : nil)
        data << (@medical.ulcers? ? "Ulcers" : nil)
        data << (@medical.venereal? ? "Venereal Disease" : nil)
        if data.compact.empty?
          indent(10) do
            text("Nothing To Report", size: 12)
          end
        else
          indent(10,10) do
            text(data.compact.join(', '), size: 12)          
          end
          contact = true
        end
        move_down(5)
  
        stroke_horizontal_rule
  
        move_down(20)
        text("Disposition: #{@medical.disposition}", size: 14, style: :bold)
        move_down(10)
        text("Screening Officer: #{AppUtils.show_contact(@medical, field: :officer)}", size: 14)
        if contact == true
          move_down(10)
          cell(content: "Contact the medic!", borders: [], size: 20, font_style: :bold, text_color: SiteConfig.pdf_em_txt)
        end
      end
    end
  end
end