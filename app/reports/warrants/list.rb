module Warrants
  class List < ReportBase
    def initialize(warrants)
      super(page_size: 'LEGAL', page_layout: :landscape)
      @warrants = warrants
      unless @warrants.is_a?(Array)
        raise ArgumentError.new("Warrants::List.initialize: First argument must be an Array of Warrant objects.")
      end
      @warrants.each do |w|
        unless w.is_a?(Warrant)
          raise ArgumentError.new("Warrants::List.initialize: First argument must be an Array of Warrant objects: supplied Array contains invalid object type(s).")
        end
      end
    end
  
    def to_pdf
      # custom header/footer
      repeat(:all) do
        bounding_box([margin_box.left, margin_box.top - 10], width: margin_box.width, height: 26) do
          text("Warrant List", align: :center, size: 20, style: :bold)
          move_up(16)
          text(Time.now.to_s(:us), size: 14, style: :bold)
        end
        bounding_box([margin_box.left, margin_box.bottom + 20], width: margin_box.width, height: 20) do
          font("Helvetica")
          stroke_horizontal_rule
          move_down(5)
          text("No: #{Time.now.to_s(:timestamp)}", align: :left, size: 9, style: :bold)
          move_up(10)
          text("#{SiteConfig.agency}", align: :center, size: 12)
          font("Times-Roman")
        end
      end
      bounding_box([margin_box.left, margin_box.top - 36], width: margin_box.width, height: margin_box.height - 56) do
        data = []
        @warrants.each do |w|
          data.push([w.sort_name, "#{w.address1}, #{w.address2}", "#{w.race_abbreviation}#{!w.race? || w.sex_name.blank? ? '' : "/"}#{w.sex_name(true)}",AppUtils.format_date(w.dob),w.jurisdiction,w.warrant_no,"#{w.charge_summary.blank? ? '' : w.charge_summary}",currency(w.bond_amt),currency(w.payable)])
        end
        data.unshift(["Name", "Address", "R/S","DOB","Jurisdiction","War#","Charge","Bond Amt","Payable"])
        table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom] 
          rows(1..-1).padding = 3
          columns(0..-1).style(align: :left, size: 10, padding: 2)
          column(0).style(width: 150)
          column(1).style(width: 220)
          column(2).style(width: 35)
          column(3).style(width: 65)
          column(4).style(width: 100)
          column(5).style(width: 70)
          # column(6) gets rest
          column(7).style(width: 65, align: :right)
          column(8).style(width: 65, align: :right)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
        end
        move_down(5)
        text("Total Warrants: #{@warrants.size}", style: :bold)
      end
      number_pages("Page: <page> of <total>", at: [bounds.right - 75, 15])
      render
    end
  end
end