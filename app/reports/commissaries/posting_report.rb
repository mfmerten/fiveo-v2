module Commissaries
  class PostingReport < ReportBase
    def initialize(datetime, status, facility, withdrawals=[], deposits=[])
      super
      @report_time = datetime
      unless @report_time.is_a?(Time)
        raise ArgumentError.new("Commissaries::PostingReport.initialize: First argument must be a Time object.")
      end
      @status = status
      unless @status.is_a?(String)
        raise ArgumentError.new("Commissaries::PostingReport.initialize: Second argument must be a String object.")
      end
      unless ["preview","post","reprint"].include?(@status)
        raise ArgumentError.new("Commissaries::PostingReport.initialize: Second argument must be one of 'preview','post','reprint'.")
      end
      @facility = facility
      unless @facility.is_a?(Jail)
        raise ArgumentError.new("Commissaries::PostingReport.initialize: Third argument must be a valid Jail object.")
      end
      @withdrawals = withdrawals
      unless @withdrawals.is_a?(Array)
        raise ArgumentError.new("Commissaries::PostingReport.initialize: Fourth argument must be an Array of Commissary objects.")
      end
      @withdrawals.each do |w|
        unless w.is_a?(Commissary)
          raise ArgumentError.new("Commissaries::PostingReport.initialize: Fourth argument must be an Array of Commissary objects: supplied Array contains invalid object type(s)!")
        end
      end
      @deposits = deposits
      unless @deposits.is_a?(Array)
        raise ArgumentError.new("Commissaries::PostingReport.initialize: Fifth argument must be an Array of Commissary objects.")
      end
      @deposits.each do |d|
        unless d.is_a?(Commissary)
          raise ArgumentError.new("Commissaries::PostingReport.initialize: Fifth argument must be an Array of Commissary objects: supplied Array contains invalid object type(s)!")
        end
      end
    end
  
    def to_pdf
      standard_report do
        subtitle = @facility.name
        title = "Commissary Posting Report"
        if @status == "preview"
          title << " (PREVIEW)"
        end
  
        # print withdrawals first
        withdraw_count = @withdrawals.size
        withdraw_total = BigDecimal.new('0.00',2)
        text(title, align: :center, size: 18, style: :bold)
        move_down(5)
        text(subtitle, align: :center, size: 16, style: :bold)
        text(AppUtils.format_date(@report_time), size: 12, style: :bold, align: :center)
        move_down(10)
        text("Withdrawals", size: 18, style: :bold, align: :center)
        move_down(5)
        if @withdrawals.empty?
          move_down(40)
          text("No Withdrawals To Report", size: 12, style: :bold, align: :center)
        else
          until @withdrawals.empty?
            if cursor <= 65
              start_new_page
            end
            p = @withdrawals[0].person_id
            account_total = BigDecimal.new('0.00',2)
            data = []
            move_down(5)
            text("#{@withdrawals[0].person.sort_name} [#{@withdrawals[0].person.id}]#{@withdrawals[0].person.jail.nil? ? '' : ' -- ' + @withdrawals[0].person.jail.acronym}", size: 12, style: :bold)
            move_up(5)
            while !@withdrawals.empty? && @withdrawals[0].person_id == p
              trans = @withdrawals.shift
              data.push([AppUtils.format_date(trans.transaction_date),trans.memo,trans.receipt_no,(trans.category.blank? ? 'Invalid' : trans.category),money(-trans.withdraw)])
              if @status == 'post'
                trans.update_attribute(:report_datetime, @report_time)
              end
              withdraw_total -= trans.withdraw
              account_total -= trans.withdraw
            end
            data.push(['','','','***Account Total:',money(account_total)])
            data.unshift(["Date", "Memo", "Receipt", "Type", "Amount"])
            table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom] 
              rows(1..-1).padding = 3
              rows(-1).style(font_style: :bold, borders: [:top], background_color: 'FFFFFF')
              columns(0..-1).style(align: :left, size: 12)
              row(-1).columns(3).style(align: :right)
              column(0).style(width: 65)
              column(3).style(width: 115)
              column(4).style(align: :right, width: 75)
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
          end
          move_down(5)
          stroke_horizontal_rule
          move_down(2)
          stroke_horizontal_rule
          move_down(3)
          data = [["Withdrawal Transactions: #{withdraw_count}",'','Withdrawn:', money(withdraw_total)]]
          table(data, header: false, width: bounds.width) do
            row(0).style(font_style: :bold, borders: [])
            columns(0..-1).style(align: :left, size: 14)
            row(0).columns(2).style(align: :right)
            column(2).style(width: 115)
            column(3).style(align: :right, width: 75)
          end
        end
  
        # next, do the deposits
        deposit_count = @deposits.size
        start_new_page
        deposit_total = BigDecimal.new('0.00',2)
        account_total = BigDecimal.new('0.00',2)
        text(title, align: :center, size: 18, style: :bold)
        move_down(5)
        text(subtitle, align: :center, size: 16, style: :bold)
        text(AppUtils.format_date(@report_time), size: 12, style: :bold, align: :center)
        move_down(10)
        text("Deposits", size: 18, style: :bold, align: :center)
        move_down(5)
        if @deposits.empty?
          move_down(40)
          text("No Deposits To Report", size: 12, style: :bold, align: :center)
        else
          until @deposits.empty?
            if cursor <= 65
              start_new_page
            end
            p = @deposits[0].person_id
            account_total = BigDecimal.new('0.00',2)
            data = []
            move_down(5)
            text("#{@deposits[0].person.sort_name} [#{@deposits[0].person.id}]#{@deposits[0].person.jail.nil? ? '' : ' -- ' + @deposits[0].person.jail.acronym}", size: 12, style: :bold)
            move_up(5)
            while !@deposits.empty? && @deposits[0].person_id == p
              trans = @deposits.shift
              data.push([AppUtils.format_date(trans.transaction_date),trans.memo,trans.receipt_no,(trans.category.blank? ? 'Invalid' : trans.category),money(trans.deposit)])
              if @status == 'post'
                trans.update_attribute(:report_datetime, @report_time)
              end
              deposit_total += trans.deposit
              account_total += trans.deposit
            end
            data.push(['','','','***Account Total:',money(account_total)])
            data.unshift(["Date", "Memo", "Receipt", "Type", "Amount"])
            table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom] 
              rows(1..-1).padding = 3
              rows(-1).style(font_style: :bold, borders: [:top], background_color: 'FFFFFF')
              columns(0..-1).style(align: :left, size: 12)
              row(-1).columns(3).style(align: :right)
              column(0).style(width: 65)
              column(3).style(width: 115)
              column(4).style(align: :right, width: 75)
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
          end
          move_down(5)
          stroke_horizontal_rule
          move_down(2)
          stroke_horizontal_rule
          move_down(3)
          data = [["Deposit Transactions: #{deposit_count}",'','Deposited:', money(deposit_total)]]
          table(data, header: false, width: bounds.width) do
            row(0).style(font_style: :bold, borders: [])
            columns(0..-1).style(align: :left, size: 14)
            row(0).columns(2).style(align: :right)
            column(2).style(width: 115)
            column(3).style(align: :right, width: 75)
          end
        end
  
        # now spit out a last page with summary
        start_new_page
        text(title, align: :center, size: 18, style: :bold)
        move_down(5)
        text(subtitle, align: :center, size: 16, style: :bold)
        text(AppUtils.format_date(@report_time), size: 12, style: :bold, align: :center)
        move_down(10)
        text("Summary", size: 18, style: :bold, align: :center)
        move_down(20)
        data = [
          ["Withdrawal Transactions:",withdraw_count],
          ["Deposit Transactions:",deposit_count],
          ["Total Transactions:",withdraw_count + deposit_count],
          [" ",""],
          ["Total Withdrawn:",money(withdraw_total)],
          ["Total Deposited:",money(deposit_total)],
          ["Net Change:",money(deposit_total + withdraw_total)]
        ]
        table(data, header: false, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], position: :center) do
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom]
          rows(0).borders = [:left, :right, :top]
          rows(1..-1).padding = 3
          columns(0..-1).style(align: :left, size: 12)
          column(1).style(align: :right)
        end
        move_down(5)
        text("END OF REPORT", style: :bold)
      end
    end
  end
end