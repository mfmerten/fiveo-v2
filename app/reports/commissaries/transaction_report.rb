module Commissaries
  class TransactionReport < ReportBase
    def initialize(report_type, jail, commissaries, start_date, stop_date, person=nil)
      super
      @report_type = report_type
      unless @report_type.is_a?(String)
        raise ArgumentError.new("Commissaries::TransactionReport.initialize: First argument must be a String object.")
      end
      unless ['summary','detailed'].include?(@report_type)
        raise ArgumentError.new("Commissaries::TransactionReport.initialize: First argument must be one of 'detailed', 'summary'.")
      end
      @jail = jail
      unless @jail.is_a?(Jail)
        raise ArgumentError.new("Commissaries::TransactionReport.initialize: Second argument must be a valid Jail object.")
      end
      @commissaries = commissaries
      unless @commissaries.is_a?(Array)
        raise ArgumentError.new("Commissaries::TransactionReport.initialize: Third argument must be an Array of Commissary objects.")
      end
      @commissaries.each do |c|
        unless c.is_a?(Commissary)
          raise ArgumentError.new("Commissaries::TransactionReport.initialize: Third argument must be an Array of Commissary objects: supplied array contains invalid object types(s)!")
        end
      end
      @start_date = start_date
      unless @start_date.is_a?(Date)
        raise ArgumentError.new("Commissaries::TransactionReport.initialize: Fourth argument must be a Date object.")
      end
      @stop_date = stop_date
      unless @stop_date.is_a?(Date)
        raise ArgumentError.new("Commissaries::TransactionReport.initialize: Fifth argument must be a Date object.")
      end
      @person = person
      unless @person.is_a?(Person) || @Person.nil?
        raise ArgumentError.new("Commissaries::TransactionReport.initialize: Sixth argument must be a Person object or Nil.")
      end
    end
  
    def page_header
      text("#{@jail.acronym} Commissary#{@person.nil? ? '' : ': ' + AppUtils.show_person(@person)}", align: :center, size: 16, style: :bold)
      text("#{@start_date.to_s(:us)} -- #{@stop_date.to_s(:us)}", align: :center, size: 14, style: :bold)
      move_down(5)
      text(Time.now.to_s(:us), size: 12, style: :bold, align: :center)
      move_down(5)
    end
  
    def to_pdf
      standard_report do
        if @report_type == 'detailed'
          page_header
          data = []
          running_balance = BigDecimal.new('0.0',2)
          @commissaries.each do |c|
            running_balance += (c.deposit - c.withdraw)
            data.push([c.id,AppUtils.format_date(c.transaction_date),c.person_id,c.category,c.memo,c.receipt_no, money(c.deposit > 0 ? c.deposit : -c.withdraw)])
          end
          data.push(['','','','','','Net Change =>',money(running_balance)])
          data.unshift(["ID", "Date/Time", "Inmate", "Type","Memo","Receipt", "Amount"])
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
            row(-1).column(0).borders = [:left, :top, :bottom]
            row(-1).columns(1..4).borders = [:top, :bottom]
            row(-1).columns(5).borders = [:top, :bottom, :right]
            columns(0..-1).style(align: :left, size: 12)
            columns(6).style(align: :right)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
  
          # per-inmate breakdown
          grand_withdraw = BigDecimal.new('0.0',2)
          grand_deposit = BigDecimal.new('0.0',2)
          inmates = 0
          num_dep = 0
          num_wit = 0
          categories = Hash.new
          Commissary::CommissaryType.each do |c|
            categories[c] = 0
          end
          # do the first header
          start_new_page
          page_header
          text("Breakdown By Inmate", size: 16, style: :bold, align: :center)
          move_down(5)
          @people = @commissaries.collect{|c| c.person}.compact.uniq
          # add a section for each person
          @people.each do |p|
            commissaries = @commissaries.reject{|c| c.person_id != p.id}
            unless commissaries.empty?
              inmates += 1
              # get balance on day prior to start of transactions
              begin_bal = p.commissary_balance(@start_date - 1.day)
              data = []
              data.push(['','','','','','Begin =>',money(begin_bal)])
              commissaries.each do |c|
                categories[c.category] += 1
                if c.deposit?
                  num_dep += 1
                  begin_bal += c.deposit
                  grand_deposit += c.deposit
                end
                if c.withdraw?
                  num_wit += 1
                  begin_bal -= c.withdraw
                  grand_withdraw -= c.withdraw
                end
                data.push([c.id, AppUtils.format_date(c.transaction_date), c.category, c.memo, c.receipt_no, money(c.deposit > 0 ? c.deposit : -c.withdraw), money(begin_bal)])
              end
              data.push(['','','','','','End =>',money(begin_bal)])
              # do we need a new page?
              if y < 100 # estimate... adjust as needed
                start_new_page
              end
              text("#{AppUtils.show_person(p)}", size: 12, style: :bold)
              move_down(5)
              data.unshift(["ID", "Date/Time", "Type","Memo","Receipt", "Amount", "Balance"])
              table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
                rows(2..-2).borders = [:left, :right]
                rows(-1).borders = [:left, :right, :bottom] 
                rows(1..-1).padding = 3
                row(1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
                row(1).column(0).borders = [:left, :top, :bottom]
                row(1).columns(1..4).borders = [:top, :bottom]
                row(1).columns(5).borders = [:top, :bottom, :right]
                row(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
                row(-1).column(0).borders = [:left, :top, :bottom]
                row(-1).columns(1..4).borders = [:top, :bottom]
                row(-1).columns(5).borders = [:top, :bottom, :right]
                columns(0..-1).style(align: :left, size: 12)
                columns([5,6]).style(align: :right)
                row(0).style(size: 9, font_style: :bold, borders: [:bottom])
              end
              move_down(10)
            end
          end
          start_new_page
        end
  
        if @report_type == 'summary'
          grand_withdraw = BigDecimal.new('0.0',2)
          grand_deposit = BigDecimal.new('0.0',2)
          inmates = 0
          num_dep = 0
          num_wit = 0
          categories = Hash.new
          Commissary::CommissaryType.each do |c|
            categories[c] = 0
          end
          @people.each do |p|
            commissaries = @commissaries.reject{|c| c.person_id != p.id}
            unless commissaries.empty?
              inmates += 1
              commissaries.each do |c|
                categories[c.category] += 1
                if c.deposit?
                  num_dep += 1
                  grand_deposit += c.deposit
                end
                if c.withdraw?
                  num_wit += 1
                  grand_withdraw -= c.withdraw
                end
              end
            end
          end
        end
  
        # do summary page
        page_header
        text("Report Summary", size: 18, style: :bold, align: :center)
        move_down(10)
        tot_trans = num_wit + num_dep
        net_change = grand_deposit + grand_withdraw
        data = [["Deposits:",num_dep,money(grand_deposit)],
        ["Withdrawals:",num_wit,money(grand_withdraw)],
        ["Net:",num_dep + num_wit,money(net_change)]]
        data.unshift([" ","Trans","Total"])
        table(data, header: true, position: :center, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]) do
          rows(1..-2).borders = [:left, :right]
          rows(1..-1).padding = 3
          row(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
          columns(0..-1).style(align: :right, size: 14)
          columns(1).style(align: :center)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
        end
        move_down(10)
        text "Totals", size: 18, style: :bold, align: :center
        data = [["Inmates:",inmates],["Transactions:",tot_trans]]
        table(data, position: :center, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]) do
          rows(0).borders = [:left, :right, :top]
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom] 
          rows(0..-1).padding = 3
          columns(0..-1).style(align: :right, size: 14)
        end
        move_down(10)
        text("Totals by Category", size: 18, style: :bold, align: :center)
        data = []
        categories.keys.each do |c|
          data.push(["#{c}:",categories[c]])
        end
        table(data, position: :center, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]) do
          rows(0).borders = [:left, :right, :top]
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom] 
          rows(0..-1).padding = 3
          columns(0..-1).style(align: :right, size: 14)
        end
      end
    end
  end
end