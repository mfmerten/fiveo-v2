module Commissaries
  class FundBalance < ReportBase
    def initialize(jail,trans=[])
      super
      @jail = jail
      unless @jail.is_a?(Jail)
        raise ArgumentError.new("Commissaries::FundBalance.initialize: First argument must be a valid Jail object.")
      end
      @trans = trans
      unless @trans.is_a?(Array)
        raise ArgumentError.new("Commissaries::FundBalance.initialize: Second argument must be an Array of Commissary objects.")
      end
      @trans.each do |t|
        unless t.is_a?(Commissary)
          raise ArgumentError.new("Commissaries::FundBalance.initialize: Second argument must be an Array of Commissary objects: supplied Array contains invalid object type(s)!")
        end
      end
    end
  
    def to_pdf
      standard_report do
        text("Inmate Account Balances", align: :center, size: 18, style: :bold)
        move_down(5)
        text(@jail.name, align: :center, size: 16, style: :bold)
        move_down(5)
        text(Time.now.to_s(:us), size: 12, style: :bold, align: :center)
        move_down(5)
        end_balance = BigDecimal.new('0.00',2)
        data = []
        @trans.each do |t|
          if t.person.nil?
            data.push(["#{t.person_id} ","Invalid Person", " ", " "])
          else
            b = t.person.bookings.last
            end_balance += t.balance.to_d
            data.push([t.person.id, t.person.sort_name, "#{b.nil? ? 'No Bookings' : (b.release_datetime? ? 'Released ' + AppUtils.format_date(b.release_datetime) : 'Active')}", money(t.balance.to_d)])
          end
        end
        if data.empty?
          move_down(40)
          text("Nothing To Report", size: 14, style: :bold, align: :center)
        else
          data = data.sort{|a,b| a[1] <=> b[1]}
          data.push(['','','Total =>',money(end_balance)])
          data.unshift(["ID", "Inmate", "Booking Status", "Balance"])
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
            rows(1..-2).borders = [:left, :right]
            rows(1..-1).padding = 3
            rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
            row(-1).column(0).borders = [:left, :top, :bottom]
            row(-1).columns(1).borders = [:top, :bottom]
            columns(0..-1).style(align: :left, size: 12)
            row(-1).columns(2).style(borders: [:top, :bottom, :right], align: :right)
            column(3).style(align: :right)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
        end
        move_down(5)
        text("END OF REPORT", style: :bold)
      end
    end
  end
end