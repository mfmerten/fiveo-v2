module FaxCovers
  class Page < ReportBase
    def initialize(fax_cover)
      super
      @fax_cover = fax_cover
      unless @fax_cover.is_a?(FaxCover)
        raise ArgumentError.new("FaxCovers::Page.initialize: First argument must be a valid FaxCover object.")
      end
    end
  
    def to_pdf
      standard_report(footer_no: @fax_cover.id.to_s) do
        move_down(20)
        span(bounds.right - 50, position: 50) do
          text("Date: _____________", size: 18)
          move_down(5)
          text("Attention:", size: 18)
          move_up(5)
          stroke_horizontal_line(80, bounds.right - 50)
          move_down(9)
          text("FAX Number: _________________", size: 18)
          move_down(5)
          text("From:", size: 18)
          move_up(5)
          stroke_horizontal_line(50, bounds.right - 50)
          move_down(9)
          text("Number of Pages Including This Cover Sheet: _______", size: 18)
          move_down(20)
          text(@fax_cover.from_name, size: 18, style: :bold)
          text("Phone: #{@fax_cover.from_phone}", size: 18)
          text("FAX: #{@fax_cover.from_fax}", size: 18)
          move_down(10)
          text("Message:", size: 18)
          move_up(5)
          stroke_horizontal_line(78, bounds.right - 50)
          (1..4).each do
            move_down(25)
            stroke_horizontal_line(0, bounds.right - 50)
          end
        end
        if @fax_cover.include_notice?
          move_down(30)
          text(@fax_cover.notice_title, size: 18, style: :bold, align: :center)
          move_down(5)
          text(@fax_cover.notice_body, size: 14)
        end
      end
    end
  end
end