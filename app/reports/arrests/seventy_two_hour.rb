module Arrests
  class SeventyTwoHour < ReportBase
    def initialize(arrest)
      super(page_size: 'LEGAL')
      @arrest = arrest
      unless @arrest.is_a?(Arrest)
        raise ArgumentError.new("Arrests::SeventyTwoHour.initialize: First argument must be a valid Arrest object.")
      end
    end
    
    def to_pdf
      standard_report(footer_no: @arrest.id) do
        data = [
          ["<b>STATE OF #{State.get(SiteConfig.agency_state).upcase}</b>"," ","Docket No. ______-72-______"],
          ["VERSUS"," "," "],
          ["<b>#{AppUtils.show_person(@arrest.person, false).upcase}</b>"," ", "Case: <b>#{@arrest.case_no}</b>"]
        ]
        width = ((bounds.width / 5) - 2).to_i
        table(data) do
          rows(0..-1).padding = 1
          columns(0..2).style(size: 14, borders: [], inline_format: true)
          column(0).style(align: :left, width: width * 2)
          column(1).style(align: :center, width: width - 2)
          column(2).style(align: :right, width: width * 2)
        end
        move_down(10)
        text("Notice Of Custody Of Arrested Person", align: :center, size: 18, style: :bold)
        move_down(5)
        text("Pursuant to the provisions and requirements of Article 230.1 of the LA code of Criminal Procedure (Act 700 of 1972), notice is hereby given to the Honorable #{SiteConfig.court_title} Court and the Honorable #{SiteConfig.prosecutor_title}, #{SiteConfig.court_title}, that the #{SiteConfig.ceo_title}, #{SiteConfig.jurisdiction_title}, #{State.get(SiteConfig.agency_state)}, has custody of the arrested person identified as follows:", size: 10)
        move_down(10)
        race = sex = dob = ssn = "Unknown"
        unless @arrest.person.nil?
          race = @arrest.person.race
          sex = @arrest.person.sex_name if @arrest.person.sex?
          dob = @arrest.person.date_of_birth if @arrest.person.date_of_birth?
          ssn = @arrest.person.ssn if @arrest.person.ssn?
        end
        data = [
          ['Address:',"#{@arrest.person.nil? ? '' : @arrest.person.physical_line1}, #{@arrest.person.nil? ? '' : @arrest.person.physical_line2}","SSN:","#{ssn}"],
          ['Phone(s):',"#{@arrest.person.nil? ? '' : (@arrest.person.home_phone? ? @arrest.person.home_phone : '') + '  ' + (@arrest.person.cell_phone? ? @arrest.person.cell_phone : '')}","DOB:","#{dob}"],
          ['Arrest ID:',"#{@arrest.id}","Race:","#{race}"],
          ['Date/Time:',AppUtils.format_date(@arrest.arrest_datetime),"Sex:","#{sex}"]
        ]
        bonds = @arrest.holds.collect{|h| h.bonds}.flatten.compact.uniq.sort{|a,b| a.issued_datetime <=> b.issued_datetime}
        unless bonds.empty?
          bonds.each do |b|
            data.push(["Bonded:","#{AppUtils.format_date(b.issued_datetime)} -- #{currency(b.bond_amt)} (#{b.status_name})",'',''])
          end
        end
        if @arrest.dwi_test_officer?
          data.push(["DWI Test:","#{@arrest.dwi_test_results} (#{AppUtils.show_contact(@arrest, field: :dwi_test_officer)}","",""])
        end
        text(AppUtils.show_person(@arrest.person).upcase, size: 12, style: :bold, align: :left)
        table(data) do
          rows(0..-1).padding = 1
          columns(0..-1).style(size: 12, borders: [], padding_left: 2)
          column(0).style(align: :right, width: 75)
          column(1).style(align: :left, font_style: :bold, width: 300)
          column(2).style(align: :right, width: 55)
          column(3).style(align: :left, font_style: :bold, width: 100)
        end
        move_down(10)
        unless @arrest.district_charges.empty?
          text("R.S. Code and Offense(s) [District]:", size: 12, style: :bold)
          span(bounds.width - 10, position: 10) do
            @arrest.district_charges.reject{|c| !c.warrant.nil?}.each do |c|
              text("#{c.count} count: #{c.charge}", size: 12)
            end
          end
          @arrest.warrants.with_district_charges.each do |w|
            span(bounds.width - 10, position: 10) do
              text("#{w.jurisdiction + ' '}Warrant Number #{w.warrant_no}:", size: 12)
            end
            span(bounds.width - 20, position: 20) do
              w.charges.each do |c|
                text("#{c.count} count: #{c.charge}", size: 12)
              end
            end
          end
        end
        unless @arrest.city_charges.empty?
          text("R.S. Code and Offense(s) [City]:", size: 12, style: :bold)
          span(pdf.bounds.width - 10, position: 10) do
            @arrest.city_charges.reject{|c| !c.warrant.nil?}.each do |c|
              text("#{c.count} count: #{c.charge}#{c.agency.blank? ? '' : ' [' + c.agency + ']'}", size: 12)
            end
          end
          @arrest.warrants.with_city_charges.each do |w|
            span(bounds.width - 10, position: 10) do
              text("#{w.jurisdiction + ' '}Warrant Number #{w.warrant_no}:", size: 12)
            end
            span(bounds.width - 20, position: 20) do
              w.charges.each do |c|
                text("#{c.count} count: #{c.charge}", size: 12)
              end
            end
          end
        end
        unless @arrest.other_charges.empty?
          text("R.S. Code and Offense(s) [Other Parish/State]:", size: 12, style: :bold)
          span(bounds.width - 10, position: 10) do
            @arrest.other_charges.reject{|c| !c.warrant.nil?}.each do |c|
              text("#{c.count} count: #{c.charge}", size: 12)
            end
          end
          @arrest.warrants.with_other_charges.each do |w|
            span(bounds.width - 10, position: 10) do
              text("#{w.jurisdiction + ' '}Warrant Number #{w.warrant_no}:", size: 12)
            end
            span(bounds.width - 20, position: 20) do
              w.charges.each do |c|
                text("#{c.count} count: #{c.charge}", size: 12)
              end
            end
          end
        end
        move_down(10)
        data = []
        unless @arrest.officers.empty?
          data.push(['Officer(s):',"#{@arrest.officers.join(' - ')}"])
        end
        if @arrest.agency?
          data.push(['Agency:',"#{@arrest.agency}"])
        end
        table(data) do
          rows(0..-1).padding = 1
          columns(0..1).style(size: 12, borders: [], padding_left: 2)
          column(0).style(align: :right, width: 75)
          column(1).style(align: :left, width: 300, font_style: :bold)
        end
        move_down(10)
        text("Respectfully signed and submitted on this ______ day of ___________, _______.", size: 12)
        move_down(40)
        signature_line("SHERIFF-DEPUTY")
        pad(10){ stroke_horizontal_rule }
        text("O R D E R", size: 18, style: :bold, align: :center)
        move_down(10)
        text("Pursuant to the above and foregoing notice and the accused having been brought before the court as required by law, the following order is hereby entered:", size: 10)
        width = bounds.right - 15
        move_down(20)
        check_box_span(width) do
          text("The accused does not qualify to have counsel appointed by the court and none is appointed.", size: 12)
        end
        move_down(10)
        check_box_span(width) do
          text("The Court finds that the accused qualified as an indigent person and has the right to have", size: 12)
          move_down(5)
          text("the Court appoint legal counsel and ____________________________, Attorney at Law is hereby appointed to defend the accused.", size: 12)
        end
        move_down(10)
        check_box_span(width) do
          text("The amount of bail not having been determined previously, bail is hereby set at the ammount of:", size: 12)
          move_down(5)
          text("$_______________.", size: 12)
        end
        move_down(10)
        check_box_span(width) do
          text("Special Condition of Bond:", size: 12)
          stroke_horizontal_line(135, bounds.right)
          move_down(20)
          stroke_horizontal_rule
        end
        move_down(10)
        check_box_span(width) do
          text("The amount of bail having been set previously at $_____________ upon review of the prior bail", size: 12)
          move_down(5)
          text("determination it is ordered by the Court that the amount of bail be set at $_____________.", size: 12)
        end
        move_down(10)
        check_box_span(width) do
          text("Bond is not set, pending hearing which is hereby fixed for ____________ AT ________M.", size: 12)
        end
        move_down(10)
        check_box_span(width) do
          text("Unsecured Personal Surety meeting the requirements of C.Cr.P.Art317 approved.", size: 12)
        end
        move_down(10)
        check_box_span(width) do
          text("By Video", size: 12)
        end
        move_down(20)
        text("DONE AND SIGNED at Many, Louisiana, on this ______ day of ___________, _______.", size: 12, style: :bold)
        move_down(50)
        signature_line("#{SiteConfig.chief_justice_title.upcase}, #{SiteConfig.court_title.upcase} COURT")
      end
    end
  end
end