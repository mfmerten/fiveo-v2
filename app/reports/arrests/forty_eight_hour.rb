module Arrests
  class FortyEightHour < ReportBase
    def initialize(arrest)
      super(page_size: 'LEGAL')
      @arrest = arrest
      unless @arrest.is_a?(Arrest)
        raise ArgumentError.new('Arrests::FortyEightHour.initialize: First argument must be a valid Arrest object.')
      end
    end
    
    def to_pdf
      standard_report(footer_no: @arrest.id) do
        text("Case: <b>#{@arrest.case_no}</b>", size: 14, align: :right, inline_format: true)
        move_down(10)
        text("Affidavit In Support Of Arrest Without A Warrant", align: :center, size: 20, style: :bold)
        move_down(5)
        text("BEFORE ME, the undersigned authority, personally appeared the undersigned Officer, who being duly sworn, deposed and swears that the following facts in support of the instant Arrest(s) without a warrant of:", size: 12)
        move_down(10)
        span(bounds.width - 10, position: 10) do
          text(AppUtils.show_person(@arrest.person), size: 16, style: :bold, align: :left)
          text("Booking ID: <b>#{@arrest.person.nil? ? '' : @arrest.person.bookings.last.id}</b>      Date and Time of Incarceration: <b>#{AppUtils.format_date(@arrest.arrest_datetime)}</b>", size: 14, inline_format: true)
          race = sex = dob = ssn = "Unknown"
          unless @arrest.person.nil?
            race = @arrest.person.race
            sex = @arrest.person.sex_name if @arrest.person.sex?
            dob = AppUtils.format_date(@arrest.person.date_of_birth) if @arrest.person.date_of_birth?
            ssn = @arrest.person.ssn if @arrest.person.ssn?
          end
          text("Race: <b>#{race}</b>      Sex: <b>#{sex}</b>      DOB: <b>#{dob}</b>      SSN: <b>#{ssn}</b>", size: 14, inline_format: true)
        end
        move_down(10)
        text("For (R.S. Code and Offense):", size: 12, style: :bold)
        span(bounds.width - 10, position: 10) do
          @arrest.district_charges.reject{|c| !c.warrant.nil?}.each do |c|
            text("    #{c.count} count: #{c.charge}", size: 12)
          end
        end
        @arrest.warrants.with_district_charges.each do |w|
          span(bounds.width - 10, position: 10) do
            text("    #{w.jurisdiction + ' '}Warrant Number #{w.warrant_no}:", size: 12)
          end
          span(bounds.width - 20, position: 20) do
            w.charges.each do |c|
              text("        #{c.count} count: #{c.charge}", size: 12)
            end
          end
        end
        move_down(10)
        text("That the probable cause for the arrest(s) without a warrant was as follows:", size: 12, style: :bold)
        move_down(10)
        text("On the ______ day of ___________, _______, at approximately ____________, in the", size: 14)
        move_down(5)
        text("vicinity of _____________________________________________________, #{SiteConfig.jurisdiction_title}, #{State.get(SiteConfig.agency_state)}, on the above named arrestee:", size: 14)
        move_down(10)
        7.times{ pad(10){ stroke_horizontal_rule } }
        move_down(30)
        signature_line("AFFIANT")
        move_down(10)
        text("Sworn to and subscribed before me this ______ day of ___________, _______.", size: 14)
        move_down(40)
        signature_line("NOTARY PUBLIC")
        pad(10){ stroke_horizontal_rule }
        text("O R D E R", size: 18, style: :bold, align: :center)
        move_down(10)
        text("The foregoing affidavit having been duly considered, IT IS HEREBY ORDERED, ADJUDGED AND DECREED that the defendant has been lawfully arrested upon probable cause, without a warrant.", size: 12)
        move_down(10)
        text("#{SiteConfig.agency_city}, #{State.get(SiteConfig.agency_state)}, this ______ day of ___________, _______.", size: 14)
        move_down(40)
        signature_line("#{SiteConfig.chief_justice_title.upcase}, #{SiteConfig.court_title.upcase} COURT", "#{SiteConfig.jurisdiction_title.upcase}, #{State.get(SiteConfig.agency_state).upcase}")
      end
    end
  end
end