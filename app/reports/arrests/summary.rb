module Arrests
  class Summary < ReportBase
    def initialize(arrests,start_date,stop_date,agency)
      super
      @arrests = arrests
      unless @arrests.is_a?(Array)
        raise ArgumentError.new('Arrests::Summary.initialize: First argument must be an Array.')
      end
      @start_date = start_date
      unless @start_date.is_a?(Date) || @start_date.nil?
        raise ArgumentError.new('Arrests::Summary.initialize: Second argument must be a valid Date object or Nil.')
      end
      @stop_date = stop_date
      unless @stop_date.is_a?(Date) || @stop_date.nil?
        raise ArgumentError.new('Arrests::Summary.initialize: Third argument must be a valid Date object or Nil.')
      end
      @agency = agency
      unless @agency.is_a?(String) || @agency.nil?
        raise ArgumentError.new('Arrests::Summary.initialize: Fourth argument must be a valid String object or Nil.')
      end
    end
    
    def to_pdf
      standard_report do
        text("Arrest Summary by Officer", align: :center, size: 18, style: :bold)
        unless @agency.blank?
          text("Only #{@agency} Officers", align: :center, size: 14, style: :bold)
        end
        period = ""
        if @start_date.nil? && @stop_date.nil?
          period = "All Records"
        elsif @start_date.nil?
          period = "On or Before #{AppUtils.format_date(@stop_date)}"
        elsif @stop_date.nil?
          period = "On or After #{AppUtils.format_date(@start_date)}"
        else
          period = "#{AppUtils.format_date(@start_date)} -- #{AppUtils.format_date(@stop_date)}"
        end
        text(period, align: :center, size: 12, style: :bold)
        move_down(5)
        data = []
        officer = {}
        @arrests.each do |a|
          if officer[a.officer].nil?
            officer[a.officer] = {dates: {}, total: 0, unit: a.unit, city: 0, other: 0, district: 0, warrants: 0}
          end
          if officer[a.officer][:dates][a.arrest_datetime.to_date].nil?
            officer[a.officer][:dates][a.arrest_datetime.to_date] = {district: 0, city: 0, other: 0, total: 0, warrants: 0}
          end
          if a.charges.district.count > 0
            officer[a.officer][:dates][a.arrest_datetime.to_date][:district] += 1
            officer[a.officer][:district] += 1
          end
          if a.charges.city.count > 0
            officer[a.officer][:dates][a.arrest_datetime.to_date][:city] += 1
            officer[a.officer][:city] += 1
          end
          if a.charges.other.count > 0
            officer[a.officer][:dates][a.arrest_datetime.to_date][:other] += 1
            officer[a.officer][:other] += 1
          end
          officer[a.officer][:total] += 1
          officer[a.officer][:dates][a.arrest_datetime.to_date][:total] += 1
          if a.warrants.count > 0
            officer[a.officer][:dates][a.arrest_datetime.to_date][:warrants] += 1
            officer[a.officer][:warrants] += 1
          end
        end
        officer.keys.sort.each do |k|
          data = []
          officer[k][:dates].keys.sort.each do |d|
            data.push(["#{AppUtils.format_date(d)}",officer[k][:dates][d][:total],officer[k][:dates][d][:district],officer[k][:dates][d][:city],officer[k][:dates][d][:other],officer[k][:dates][d][:warrants]])
          end
          move_down(10)
          text("#{k} (#{officer[k][:unit]})", size: 14, style: :bold)
          data.unshift(["Date", "All Arrests", "For District", "For City", "For Other", "Warrants"])
          data.push(["Totals:", officer[k][:total], officer[k][:district], officer[k][:city], officer[k][:other], officer[k][:warrants]])
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], position: :center) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
            columns(0..-1).style(align: :center)
            column(0).style(width: 75)
            column(1).style(width: 65)
            column(2).style(width: 65)
            column(3).style(width: 65)
            column(4).style(width: 65)
            column(5).style(width: 65)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
          if y < 150
            start_new_page
          end
        end
        move_down(5)
        text("END OF REPORT", style: :bold)
      end
    end
  end
end