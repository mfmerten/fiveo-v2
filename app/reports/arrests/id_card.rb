module Arrests
  class IdCard < ReportBase
    def initialize(arrest)
      super
      @arrest = arrest
      unless @arrest.is_a?(Arrest)
        raise ArgumentError.new('Arrests::IdCard.initialize: First argument must be a valid Arrest object.')
      end
    end
    
    def to_pdf
      # note: custom header and footer sizes and text - cannot use standard_letter
      repeat(:all) do
        bounding_box([margin_box.left, margin_box.top - 10], width: margin_box.width, height: 95) do
          font("Times-Roman")
          if @arrest.person.nil? || @arrest.person.bookings.empty? || @arrest.person.bookings.last.release_datetime? || @arrest.person.bookings.last.jail.nil?
            text(SiteConfig.agency, align: :center, size: 18, style: :bold)
          else
            text(@arrest.person.bookings.last.jail.name, align: :center, size: 18, style: :bold)
          end
          move_down(5)
          text("IDENTIFICATION CARD", align: :center, size: 20, style: :bold)
          move_down(5)
          stroke_horizontal_rule
          move_down(5)
          font("Helvetica")
          if @arrest.person.nil?
            text("Unknown", size: 18, style: :bold)
          else
            text(AppUtils.show_person(@arrest.person), size: 18, style: :bold)
            text("#{@arrest.person.aka? ? 'Known Aliases: ' + @arrest.person.aka : 'No Known Aliases'}", size: 14)
          end
          stroke_horizontal_rule
          font("Times-Roman")
        end
        bounding_box([margin_box.left, margin_box.bottom + 20], width: margin_box.width, height: 20) do
          font("Helvetica")
          stroke_horizontal_rule
          move_down(5)
          text("No: #{@arrest.id}", align: :left, size: 9, style: :bold)
          move_up(10)
          text(SiteConfig.agency, align: :center, size: 12)
          font("Times-Roman")
        end
      end
  
      bounding_box([margin_box.left, margin_box.top - 110], width: margin_box.width, height: margin_box.height - 135) do
        if @arrest.person.nil?
          text("The Person for this Arrest is not valid!", size: 18, style: :bold, align: :center)
          text("Please fix it then print this ID Card again.", size: 14, style: :bold, align: :center)
        else
          image(get_img_path(@arrest.person.front_mugshot, :medium), at: [306, bounds.top - 5], fit: [100, 150])
          image(get_img_path(@arrest.person.side_mugshot, :medium), at: [456, bounds.top - 5], fit: [100, 150])
          # start position
          row_position = bounds.top - 5
          row_position -= text_field([col(2,1), row_position], col(2), "Date of Birth:", AppUtils.format_date(@arrest.person.date_of_birth))
          row_position -= text_field([col(2,1), row_position], col(2), "Place of Birth:", @arrest.person.place_of_birth)
          row_position -= text_field([col(2,1), row_position], col(2), "SSN:", @arrest.person.ssn)
          row_position -= text_field([col(2,1), row_position], col(2), "OLN:", "#{@arrest.person.oln} (#{@arrest.person.oln_state})")
          row_position -= text_field([col(2,1), row_position], col(2), "SID:", @arrest.person.sid)
          row_position -= text_field([col(2,1), row_position], col(2), "FBI ID:", @arrest.person.fbi)
          row_position -= text_field([col(2,1), row_position], col(2), "DOC No:", @arrest.person.doc_number)
          text_field([col(2,1), row_position], col(2), "Physical Address:", "#{@arrest.person.physical_line1}\n#{@arrest.person.physical_line2}", rows: 2)
          row_position -= text_field([col(2,2), row_position], col(2), "Mailing Address:", "#{@arrest.person.mailing_line1}\n#{@arrest.person.mailing_line2}", rows: 2)
          text_field([col(2,2), row_position], col(2), "Emergency Contact:", "#{@arrest.person.emergency_contact}\n#{@arrest.person.em_relationship}\n#{@arrest.person.emergency_phone}", pdf_color_scheme: 'em', rows: 3)
          text_field([col(4,1), row_position], col(4), "Home Phone:", @arrest.person.home_phone)
          row_position -= text_field([col(4,2), row_position], col(4), "Cell Phone:", @arrest.person.cell_phone)
          row_position -= text_field([col(2,1), row_position], col(2), "Occupation:", "#{@arrest.person.occupation? ? @arrest.person.occupation.titleize : ' '}")
          row_position -= text_field([col(2,1), row_position], col(2), "Employer:", "#{@arrest.person.employer? ? @arrest.person.employer.titleize : ' '}")
          text_field([col(2,1), row_position], col(2), "Description:", @arrest.person.person_description, rows: 3)
          row_position -= text_field([col(2,2), row_position], col(2), "Distinguishing Marks:", @arrest.person.marks_description, rows: 3)
          move_down(5)
          heading('ARREST DATA')
          move_down(5)
          row_position -= 29
          text_field([col(4,1), row_position], col(4), "Arrest Date:", AppUtils.format_date(@arrest.arrest_datetime))
          text_field([col(4,2), row_position], col(4), "Case Number:", @arrest.case_no)
          row_position -= text_field([col(2,2), row_position], col(2), "Agency:", @arrest.agency)
          text_field([col(4,1), row_position], col(4), "ATN:", @arrest.atn)
          text_field([col(4,2), row_position], col(4), "Leave This Blank:", "")
          row_position -= text_field([col(2,2), row_position], col(2), "Leave This Blank:", "")
          row_position -= text_field([col(1,1), row_position], col(1), "Officer(s):", @arrest.officers.join(' - '))
          move_down(5)
          data = []
          @arrest.district_charges.each do |c|
            data.push(['District', "#{c.warrant.nil? ? '' : c.warrant.warrant_no} ", c.count, c.charge])
          end
          @arrest.city_charges.each do |c|
            data.push(['City', "#{c.warrant.nil? ? '' : c.warrant.warrant_no} ", c.count, c.charge])
          end
          @arrest.other_charges.each do |c|
            data.push(['Other', "#{c.warrant.nil? ? '' : c.warrant.warrant_no} ", c.count, c.charge])
          end
          if data.empty?
            data = [[' ',' ',' ',' ']]
          end
          data.unshift(["Type","Warrant","Cnt","Charge"])
          charge_width = bounds.right - 178
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]) do 
            rows(1..-2).style(borders: [:left, :right])
            row(-1).style(borders: [:left, :right, :bottom]) 
            rows(1..-1).style(padding_top: 3, padding_bottom: 3)
            columns(0..3).style(size: 12, align: :left)
            column(0).style(width: 55)
            column(1).style(width: 85)
            column(2).style(width: 35)
            column(3).style(width: charge_width)
            row(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
        end
      end
      number_pages("Page: <page> of <total>", at: [bounds.right - 65, 15])
      render
    end
  end
end