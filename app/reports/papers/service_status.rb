module Papers
  class ServiceStatus < ReportBase
    def initialize(court_date, papers)
      super
      @court_date = court_date
      unless @court_date.is_a?(Date)
        raise ArgumentError.new("Papers::ServiceStatus.initialize: First argument must be a Date object.")
      end
      @papers = papers
      unless @papers.is_a?(Array)
        raise ArgumentError.new("Papers::ServiceStatus.initialize: Second argument must be an Array of Paper objects.")
      end
      @papers.each do |p|
        unless p.is_a?(Paper)
          raise ArgumentError.new("Papers::ServiceStatus.initialize: Second argument must be an Array of Paper objects: supplied Array contains invalid object type(s).")
        end
      end
    end
  
    def to_pdf
      standard_report do
        text("Service Status Report", align: :center, size: 18, style: :bold)
        text("Court Date: #{AppUtils.format_date(@court_date)}", align: :center, size: 14, style: :bold)
        move_down(8)
        data = []
        @papers.each do |p|
          data.push([p.id, "#{p.suit_no}", "#{p.paper_type.nil? ? '' : p.paper_type.name}", p.serve_to, AppUtils.format_date(p.served_datetime.to_date), AppUtils.format_date(p.returned_date)])
        end
        data.unshift(["Paper ID", "Suit No.", "Paper Type", "Serve To", "Served", "Returned"])
        table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom] 
          rows(1..-1).padding = 3
          columns(0..-1).style(align: :left, size: 12, padding: 2)
          column(0).style(width: 65)
          column(1).style(width: 65)
          # column(2) gets remaining
          column(3).style(width: 125)
          column(4).style(width: 65)
          column(5).style(width: 65)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
        end
        move_down(5)
        text("Paper Count: #{@papers.size}", style: :bold)
        move_down(5)
        text("END OF REPORT", size: 12)
      end
    end
  end
end