module Papers
  class PostingReport < ReportBase
    def initialize(status, report_time, payments)
      super
      @status = status
      unless @status.is_a?(String)
        raise ArgumentError.new("Papers::PostingReport.initialize: First argument must be a String object.")
      end
      unless ['post','preview','reprint'].include?(@status)
        raise ArgumentError.new("Papers::PostingReport.initialize: First argument must be a String object: valid values are 'post', 'preview', 'reprint'.")
      end
      @report_time = report_time
      unless @report_time.is_a?(Time)
        raise ArgumentError.new("Papers::PostingReport.initialize: Second argument must be a Time object.")
      end
      @payments = payments
      unless @payments.is_a?(Array)
        raise ArgumentError.new("Papers::PostingReport.initialize: Third argument must be an Array of PaperPayment objects.")
      end
      @payments.each do |p|
        unless p.is_a?(PaperPayment)
          raise ArgumentError.new("Papers::PostingReport.initialize: Third argument must be an Array of PaperPayment objects: supplied Array contains invalid object type(s).")
        end
      end
    end
  
    def to_pdf
      standard_report do
        charge_count = payment_count = 0
        transaction_count = @payments.size
        charge_total = payment_total = net = BigDecimal.new('0',2)
        accounts = {}
        account_charges = {}
        account_payments = {}
  
        title = "Paper Service Posting#{@status == 'preview' ? ' Preview' : ''} Report"
  
        charge_lines = []
        payment_lines = []
        @payments.each do |p|
          if p.charge? && p.charge != 0
            if accounts[p.paper_customer_id.to_s].nil?
              accounts[p.paper_customer_id.to_s] = BigDecimal.new("0.0",2)
              account_charges[p.paper_customer_id.to_s] = BigDecimal.new('0.0',2)
              account_payments[p.paper_customer_id.to_s] = BigDecimal.new('0.0',2)
            end
            charge_lines.push(["#{p.payment? && p.payment != 0 ? '*' : ' '}",AppUtils.format_date(p.transaction_date),p.id,p.paper_customer_id,p.memo," "," ",money(p.charge)])
            charge_count += 1
            charge_total += p.charge
            net += p.charge
            accounts[p.paper_customer_id.to_s] += p.charge
            account_charges[p.paper_customer_id.to_s] += p.charge
          end
          if p.payment? && p.payment != 0
            payment_lines.push(["#{p.charge? && p.charge != 0 ? '*' : ' '}",AppUtils.format_date(p.transaction_date),p.id,p.paper_customer_id,p.memo,p.receipt_no,p.payment_method,money(p.payment)])
            payment_count += 1
            payment_total -= p.payment
            net -= p.payment
            accounts[p.paper_customer_id.to_s] -= p.payment
            account_payments[p.paper_customer_id.to_s] -= p.payment
          end
          if @status == 'post'
            p.update_posted(@report_time)
          end
        end
  
        # print the charges
        text(title, size: 18, style: :bold, align: :center)
        text(AppUtils.format_date(@report_time), size: 14, style: :bold, align: :center)
        move_down(5)
        text("Charges", size: 16, style: :bold, align: :center)
        if charge_lines.empty?
          move_down(40)
          text("No Charges To Report", size: 14, style: :bold, align: :center)
        else
          charge_lines.unshift([" ", "Date", "Trans ID", "Acct ID", "Memo", "Receipt", "Type", "Amount"])
          table(charge_lines, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            columns(0..-1).style(align: :left, size: 12, padding: 2)
            column(0).style(width: 10)
            column(1).style(width: 65)
            column(2).style(width: 50)
            column(3).style(width: 50)
            # columns 4 & 5 split the differnece
            column(6).style(width: 65)
            column(7).style(width: 70, align: :right)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
          move_down(5)
          text("* These transactions also appear in Payments section.", size: 10)
          move_down(5)
          text("Charge Count: #{charge_count}  Charge Total: #{money(charge_total)}", size: 14, style: :bold)
        end
  
        # print the payments
        start_new_page
        text(title, size: 18, style: :bold, align: :center)
        text(AppUtils.format_date(@report_time), size: 14, style: :bold, align: :center)
        move_down(5)
        text("Payments", size: 16, style: :bold, align: :center)
        if payment_lines.empty?
          move_down(40)
          text("No Charges To Report", size: 12, style: :bold, align: :center)
        else
          payment_lines.unshift([" ", "Date", "Trans ID", "Acct ID", "Memo", "Receipt", "Type", "Amount"])
          table(payment_lines, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            columns(0..-1).style(align: :left, size: 12, padding: 2)
            column(0).style(width: 10)
            column(1).style(width: 65)
            column(2).style(width: 50)
            column(3).style(width: 50)
            # columns 4 and 5 split the difference
            column(6).style(width: 65)
            column(7).style(width: 70, align: :right)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
          move_down(5)
          text("* These transactions also appear in Charges section.", size: 10)
          move_down(5)
          text("Payment Count: #{payment_count}  Payment Total: #{money(-payment_total)}", size: 14, style: :bold)
        end
  
        # print the summary page
        start_new_page
        text(title, size: 18, style: :bold, align: :center)
        text(AppUtils.format_date(@report_time), size: 14, style: :bold, align: :center)
        move_down(5)
        text("Summary", size: 16, style: :bold, align: :center)
        data = [
          ["Charge Count:",charge_count],
          ["Payment Count:",payment_count],
          ["Total Count:",charge_count + payment_count],
          ["Transactions:",transaction_count],
          [" ",""],
          ["Total Charged:",money(charge_total)],
          ["Total Paid:",money(payment_total)],
          ["Net Change:",money(payment_total + charge_total)]
        ]
        table(data, header: false, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], position: :center) do
          columns(0..-1).style(align: :right, size: 12, padding: 5)
        end
        
        # summary by customer
        start_new_page
        text(title, size: 18, style: :bold, align: :center)
        text(AppUtils.format_date(@report_time), size: 14, style: :bold, align: :center)
        move_down(5)
        text("Summary by Account", size: 16, style: :bold, align: :center)
        data = []
        if accounts.empty?
          move_down(40)
          text("No Accounts To Report", size: 12, style: :bold, align: :center)
        else
          accounts.keys.sort{|a,b| a.rjust(10,'0') <=> b.rjust(10,'0')}.each do |a|
            customer = PaperCustomer.find_by_id(a.to_i)
            data.push(["#{a}", "#{customer.nil? ? 'Unknown' : customer.name}", money(account_charges[a]), money(account_payments[a]),money(accounts[a])])
          end
          data.unshift(["Account", "Name","Charges","Payments","Net Change"])
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            columns(0..-1).style(align: :left, size: 12, padding: 2)
            column(0).style(width: 65)
            # column 1 gets the difference
            column(2).style(width: 70, align: :right)
            column(3).style(width: 70, align: :right)
            column(4).style(width: 70, align: :right)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
        end
        move_down(5)
        text("END OF REPORT", size: 12)
      end
    end
  end
end
