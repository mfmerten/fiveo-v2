module Papers
  class Invoice < ReportBase
    def initialize(papers, receipt=false)
      super
      @papers = papers
      unless @papers.is_a?(Array)
        raise ArgumentError.new("Papers::Invoice.initialize: First argument must be an Array of Paper objects.")
      end
      @papers.each do |p|
        unless p.is_a?(Paper)
          raise ArgumentError.new("Papers::Invoice.initialize: First argument must be an Array of Paper objects: supplied Array contains invalid object type(s).")
        end
      end
      @receipt = (receipt == true)
    end
  
    def to_pdf
      standard_report(footer_no: false) do
        @papers.each do |paper|
          unless paper == @papers.first
            start_new_page
          end
          text("#{@receipt ? 'Receipt' : 'Invoice'}", align: :center, size: 24, style: :bold)
          move_up(18)
          text("No: #{paper.id}", size: 16, style: :bold)
          move_up(18)
          text("No: #{paper.id}", size: 16, style: :bold, align: :right)
          move_down(5)
          text(Date.today.to_s(:us), size: 16, style: :bold, align: :center)
          sep = "\n"
          heading("Billing Information")
          row_pos = cursor
          text_field([col(2,1), row_pos], col(2), 'Attention:', "#{paper.customer.nil? ? '' : paper.customer.attention} ")
          row_pos -= text_field([col(2,2), row_pos], col(2), 'Suit Or Docket:', "#{paper.suit_no} ")
          text_field([col(2,1), row_pos], col(2), 'Customer:', "#{paper.customer.nil? ? 'Unknown' : paper.customer.name} #{sep}#{paper.customer.nil? ? '' : paper.customer.address_line1} #{paper.customer.nil? ? '' : (paper.customer.address_line2.blank? ? '' : sep + paper.customer.address_line2)} #{sep}#{paper.customer.nil? ? '' : paper.customer.address_line3} ", rows: 3)
          row_pos -= text_field([col(2,2), row_pos], col(2), 'For:', "#{paper.plaintiff} #{sep}-VS-#{sep}#{paper.defendant} ", rows: 3)
          text_field([col(4,1), row_pos], col(4), 'Customer Number:', "#{paper.paper_customer_id} ")
          text_field([col(4,2), row_pos], col(4), 'Phone:',"#{paper.customer.nil? ? '' : paper.customer.phone} ")
          row_pos -= text_field([col(2,2), row_pos], col(2), "Office Use Only:"," ")
          move_down(10)
          heading("Invoice Details")
          row_pos = cursor
          text_field([col(4,3), row_pos], col(4), "Received:", AppUtils.format_date(paper.received_date))
          text_field([col(4,4), row_pos], col(4), "#{paper.served_datetime? ? 'Served:' : 'Returned:'}", "#{AppUtils.format_date((paper.served_datetime? ? paper.served_datetime.to_date : paper.returned_date))} ")
          row_pos -= text_field([col(2,1), row_pos], col(2), "Paper ID:", "#{paper.id} ")
          text_field([col(2,2), row_pos], col(2), "#{paper.served_datetime? ? 'Served By:' : 'Returned Because:'}", "#{paper.served_datetime? ? AppUtils.show_contact(paper, field: :officer) : paper.noservice_type} ")
          row_pos -= text_field([col(2,1), row_pos], col(2), "Type:", "#{paper.paper_type.nil? ? 'Unknown' : paper.paper_type.name} ")
          text_field([col(2,2), row_pos], col(2), "#{paper.served_datetime? ? 'Served To:' : 'Note:'}", "#{paper.served_datetime? ? paper.served_to + (paper.service_type.blank? ? '' : ' (' + paper.service_type + ')') : paper.noservice_extra} ")
          row_pos -= text_field([col(2,1), row_pos], col(2), "Serve To:", "#{paper.serve_to} ")
          move_down(20)
          mfee = (paper.mileage_fee * paper.mileage).round(2)
          data = [
            ["Service","Amount"],
            ["#{paper.served_datetime? ? 'Service Fee' : 'Non-Service Fee'} ","#{paper.mileage_only? ? '$0.00' : (paper.served_datetime? ? currency(paper.service_fee) : currency(paper.noservice_fee))} "],
            ["Mileage Fee (#{paper.mileage} miles @ #{currency(paper.mileage_fee)}/mile)","#{currency(mfee)} "],
            ["Total Due:","#{currency(paper.invoice_total)} "]
          ]
          if paper.invoice_due != paper.invoice_total && !paper.paid_in_full?
            data.push(["Paid to Date:", "#{currency(paper.invoice_total - paper.invoice_due)} "])
            data.push(["Remaining Due:", "#{currency(paper.invoice_due)} "])
          end
          if @receipt
            if paper.invoice_due > 0
              data.push(["Adjustment:", "#{currency(-paper.invoice_due)} "])
            end
            data.push(["Amount Paid:","#{currency(paper.invoice_total - paper.invoice_due)}#{paper.paid_in_full? ? ' (PAID IN FULL)' : ''} "])
          end
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: (bounds.width * 0.75).round, position: :center) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
            columns(0..-1).style(align: :right, size: 14, padding: 2)
            column(0).style(align: :left)
            row(-1).column(0).style(align: :right)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom], align: :left)
          end
          move_down(20)
          if @receipt
            text("Paid In Full - Thank You!", style: :bold, align: :center, size: 14)
          else
            data = [["Please pay this amount",{image: "#{Rails.root}/app/assets/images/icons/forward.png", image_height: 18},currency(paper.invoice_total)]]
            table(data, header: :false, position: :center) do
              row(0).style(borders: [], padding: 5)
              columns([0,2]).style(size: 14, font_style: :bold)
            end
            text("Include Invoice Number With Payment", size: 14, align: :center, style: :bold)
          end
        end
      end
    end
  end
end