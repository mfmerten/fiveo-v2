module Papers
  class PaymentHistory < ReportBase
    def initialize(customer, from_date, to_date, payments)
      super
      @customer = customer
      unless @customer.is_a?(PaperCustomer)
        raise ArgumentError.new("Papers::PaymentHistory.initialize: First argument must be a valid PaperCustomer object.")
      end
      @from_date = from_date
      unless @from_date.nil? || @from_date.is_a?(Date)
        raise ArgumentError.new("Papers::PaymentHistory.initialize: Second argument must be a valid Date object or Nil.")
      end
      @to_date = to_date
      unless @to_date.nil? || @to_date.is_a?(Date)
        raise ArgumentError.new("Papers::PaymentHistory.initialize: Third argument must be a valid Date object or Nil.")
      end
      @payments = payments
      unless @payments.is_a?(Array)
        raise ArgumentError.new("Papers::PaymentHistory.initialize: Fourth argument must be an Array of PaperPayment objects.")
      end
      @payments.each do |p|
        unless p.is_a?(PaperPayment)
          raise ArgumentError.new("Papers::PaymentHistory.initialize: Fourth argument must be an Array of PaperPayment objects: supplied Array contains invalid object type(s).")
        end
      end
    end
  
    def to_pdf
      standard_report do
        text("Paper Service Payment History", align: :center, size: 18, style: :bold)
        text("Customer #{@customer.id}: #{@customer.name}", align: :center, size: 16, style: :bold)
        period = ""
        if @from_date.nil? && @to_date.nil?
          period = "All Records"
        elsif @from_date.nil?
          period = "On or Before #{AppUtils.format_date(@to_date)}"
        elsif @to_date.nil?
          period = "On or After #{AppUtils.format_date(@from_date)}"
        else
          period = "#{AppUtils.format_date(@from_date)} -- #{AppUtils.format_date(@to_date)}"
        end
        text(period, align: :center, size: 12, style: :bold)
        move_down(8)
        data = [["Trans ID", "Date", "Invoice","Memo", "Receipt", "Type", "Paid", "Adjusted"]]
        total_paid = BigDecimal.new("0.0",2)
        total_adj = BigDecimal.new("0.0",2)
        @payments.each do |p|
          data.push([p.id, AppUtils.format_date(p.transaction_date), "#{p.paper.nil? ? 'N/A' : p.paper_id}", p.memo, p.receipt_no, p.payment_method, money(p.payment), money(p.charge)])
          total_paid += p.payment
          total_adj += p.charge
        end
        data.push([' ',' ',' ',' ', ' ', 'Total:', money(total_paid), money(total_adj)])
        table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom] 
          rows(1..-1).padding = 3
          rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
          row(-1).column(0).borders = [:left, :top, :bottom]
          row(-1).columns(1..4).borders = [:top, :bottom]
          row(-1).column(5).borders = [:top, :bottom, :right]
          columns(0..-1).style(align: :left, size: 12, padding: 2)
          column(0).style(width: 50)
          column(1).style(width: 65)
          column(2).style(width: 50)
          # column3 gets remaining width
          column(4).style(width: 75)
          column(5).style(width: 40)
          column(6).style(width: 65, align: :right)
          column(7).style(width: 65, align: :right)
          row(-1).column(5).style(align: :right)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
        end
        move_down(8)
        text("END OF REPORT", size: 12)
      end
    end
  end
end