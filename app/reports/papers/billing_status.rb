module Papers
  class BillingStatus < ReportBase
    def initialize(status, papers)
      super
      @status = status
      unless @status.is_a?(String)
        raise ArgumentError.new("Papers::BillingStatus.initialize: First argument must be a String object.")
      end
      unless ['payable','billable'].include?(@status)
        raise ArgumentError.new("Papers::BillingStatus.initialize: First argument must be a String object: allowable values are 'payable' and 'billable'.")
      end
      @papers = papers
      unless @papers.is_a?(Array)
        raise ArgumentError.new("Papers::BillingStatus.initialize: Second argument must be an Array of Paper objects.")
      end
      @papers.each do |p|
        unless p.is_a?(Paper)
          raise ArgumentError.new("Papers::BillingStatus.initialize: Second argument must be an Array of Paper objects: supplied Array contains invalid object type(s).")
        end
      end
    end
  
    def to_pdf
      standard_report do
        text("Paper Service #{@status.titleize} Report", size: 18, style: :bold, align: :center)
        text(Time.now.to_s(:us), align: :center, style: :bold)
        # initialize data
        default_mileage_fee = SiteConfig.mileage_fee
        default_noservice_fee = SiteConfig.noservice_fee
        totals = {count: 0, bad_count: 0, total: BigDecimal.new("0.0",2), pcost: BigDecimal.new("0.0",2), mcost: BigDecimal.new("0.0",2)}
        customers = {}
        @papers.each do |p|
          if p.customer.nil?
            totals[:bad] += 1
            next
          end
          if customers[p.paper_customer_id].nil?
            customers[p.paper_customer_id] = p.customer.name
          end
          if totals[p.paper_customer_id].nil?
            totals[p.paper_customer_id] = {count: 0, total: BigDecimal.new("0.0",2), pcost: BigDecimal.new("0.0",2), mcost: BigDecimal.new("0.0",2)}
          end
          totals[p.paper_customer_id][p.id] = {}
          totals[p.paper_customer_id][p.id][:suit_no] = p.suit_no
          totals[p.paper_customer_id][p.id][:paper_type] = (p.paper_type.nil? ? "Unknown" : p.paper_type.name)
          totals[p.paper_customer_id][p.id][:status] = (p.served_datetime? ? 'Served' : 'Returned')
          if p.invoice_datetime?
            # paper invoiced, use invoice amounts
            tcost = p.invoice_total
            mcost = (p.mileage * p.mileage_fee).round(2)
            scost = (p.mileage_only? ? BigDecimal.new('0.0',2) : (p.served_datetime? ? p.service_fee : p.noservice_fee))
          else
            # not invoiced, calculate totals as if invoicing
            if p.mileage_fee?
              mfee = p.mileage_fee
            elsif p.customer.mileage_fee?
              mfee = p.customer.mileage_fee
            else
              mfee = default_mileage_fee
            end
            mcost = (p.mileage * mfee).round(2)
            if p.noservice_fee?
              nsfee = p.noservice_fee
            elsif p.customer.noservice_fee?
              nsfee = p.customer.noservice_fee
            else
              nsfee = default_noservice_fee
            end
            if p.service_fee?
              sfee = p.service_fee
            else
              sfee = BigDecimal.new((p.paper_type_nil? ? "0.0" : p.paper_type.cost),2)
            end
            scost = (p.served_datetime? ? sfee : nsfee)
            tcost = mcost
            unless p.mileage_only?
              tcost += pcost
            end
          end
          totals[p.paper_customer_id][p.id][:invoice_total] = tcost
          totals[p.paper_customer_id][p.id][:mileage_charged] = mcost
          totals[p.paper_customer_id][p.id][:service_charged] = scost
          # customer totals
          totals[p.paper_customer_id][:total] += tcost
          totals[p.paper_customer_id][:pcost] += scost
          totals[p.paper_customer_id][:mcost] += mcost
          totals[p.paper_customer_id][:count] += 1
          # grand totals
          totals[:total] += tcost
          totals[:pcost] += scost
          totals[:mcost] += mcost
          totals[:count] += 1
        end
        # print the customer tables
        customers.to_a.sort{|a,b| a[1] <=> b[1]}.each do |customer_id,customer_name|
          if cursor <= 75
            start_new_page
          end
          text(customer_name, size: 14, style: :bold)
          move_up(5)
          data = [["Paper ID","Suit No.","Paper Type","Status","Service","Mileage","Total"]]
          totals[customer_id].keys.reject{|k| !k.is_a?(Integer)}.sort.each do |paper_id|
            data.push([paper_id,totals[customer_id][paper_id][:suit_no],totals[customer_id][paper_id][:paper_type],totals[customer_id][paper_id][:status],money(totals[customer_id][paper_id][:service_charged]),money(totals[customer_id][paper_id][:mileage_charged]),money(totals[customer_id][paper_id][:invoice_total])])
          end
          data.push([' ', ' ', ' ', 'Totals:', money(totals[customer_id][:pcost]), money(totals[customer_id][:mcost]),money(totals[customer_id][:total])])
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width - 10, position: 10) do
            rows(1..-2).borders = [:left, :right]
            row(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            row(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
            row(-1).column(0).borders = [:left, :top, :bottom]
            row(-1).columns(1..2).borders = [:top, :bottom]
            row(-1).column(3).borders = [:top, :bottom, :right]
            columns(0..-1).style(align: :left, padding: 2)
            column(0).style(width: 60)
            column(1).style(width: 85)
            # column2 gets whats left
            column(3).style(width: 50)
            column(4).style(width: 60, align: :right)
            column(5).style(width: 60, align: :right)
            column(6).style(width: 60, align: :right)
            row(-1).column(3).style(align: :right)
            row(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
          move_down(5)
        end
        # grand totals
        if cursor <= 75
          start_new_page
        end
        text("Grand Totals", size: 14, style: :bold)
        move_up(5)
        data = [
          ["Paper ID","Suit No.","Paper Type","Status","Service","Mileage","Total"],
          [' ', ' ', ' ', 'Totals:', money(totals[:pcost]),money(totals[:mcost]),money(totals[:total])]
        ]
        table(data, header: true, width: bounds.width - 10, position: 10) do
          rows(1..-2).borders = [:left, :right]
          row(-1).borders = [:left, :right, :bottom] 
          rows(1..-1).padding = 3
          row(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top])
          row(-1).column(0).borders = [:left, :top, :bottom]
          row(-1).columns(1..2).borders = [:top, :bottom]
          row(-1).column(3).borders = [:top, :bottom, :right]
          columns(0..-1).style(align: :left, padding: 2)
          column(0).style(width: 60)
          column(1).style(width: 85)
          # column2 gets whats left
          column(3).style(width: 50)
          column(4).style(width: 60, align: :right)
          column(5).style(width: 60, align: :right)
          column(6).style(width: 60, align: :right)
          row(-1).column(3).style(align: :right)
          row(0).style(size: 9, font_style: :bold, borders: [:bottom])
        end
        move_down(5)
        text("END OF REPORT", size: 12)
      end
    end
  end
end