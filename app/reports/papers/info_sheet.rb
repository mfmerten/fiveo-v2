module Papers
  class InfoSheet < ReportBase
    def initialize(paper)
      super
      @paper = paper
      unless @paper.is_a?(Paper)
        raise ArgumentError.new("Papers::InfoSheet.initialize: First argument must be a valid Paper object.")
      end
    end
  
    def to_pdf
      standard_report(footer_no: @paper.id.to_s) do
        text("Civil Process Information", align: :center, size: 18, style: :bold)
        move_down(5)
        text(Time.now.to_s(:us), size: 12, style: :bold, align: :center)
        move_down(10)
        data = [['Suit or Docket #:', "#{@paper.suit_no}"],['Plaintiff:', "#{@paper.plaintiff}"],['Defendant:', "#{@paper.defendant}"]]
        heading_column = 150
        data_column = bounds.width - heading_column - 50
        table(data, header: false, position: :left) do
          columns(0..-1).style(borders: [], align: :left, size: 14)
          column(0).style(width: heading_column, align: :right)
          column(1).style(width: data_column, font_style: :bold)
        end
  
        pad(5){stroke_horizontal_rule}
  
        data = [['Type of Document:',"#{@paper.paper_type.nil? ? '' : @paper.paper_type.name}"],['Date Received:', AppUtils.format_date(@paper.received_date)]]
        if @paper.court_date?
          data.push(['Court Date:', AppUtils.format_date(@paper.court_date)])
        end
        data.push(['Person to be Served:',@paper.serve_to])
        data.push(['Address:',@paper.address_line1])
        data.push([' ',@paper.address_line2])
        if @paper.phone?
          data.push(['Phone:', @paper.phone ])
        end
        table(data, header: false, position: :left) do
          columns(0..-1).style(borders: [], align: :left, size: 14)
          column(0).style(width: heading_column, align: :right)
          column(1).style(width: data_column, font_style: :bold)
        end
  
        pad(5){stroke_horizontal_rule}
  
        data = [
          ['Service Type:',"#{@paper.service_type.blank? ? '_______________________________' : @paper.service_type}"],
          ['Date Served:', "#{@paper.served_datetime? ? AppUtils.format_date(@paper.served_datetime) : '_________________________'}"],
          ['Person Served:',"#{@paper.served_to? ? @paper.served_to : '_____________________________________________'}"],
          ['Officer:', "#{@paper.officer? ? AppUtils.show_contact(@paper, field: :officer) : '_____________________________________________'}"],
          ['Returned Date:',"#{@paper.returned_date? ? AppUtils.format_date(@paper.returned_date) : '_________________________'}"]
        ]
        if @paper.noservice_type.blank?
          data.push(['Reason Returned:','___________________________________________________'])
        else
          data.push(['Reason Returned:',"#{@paper.noservice_type}#{@paper.noservice_type.slice(-1,1) != ':' && @paper.noservice_extra? ? ': ' : ' '}#{@paper.noservice_extra? ? @paper.noservice_extra : ''}"])
        end
        if @paper.comments?
          data.push(['Comments:',@paper.comments])
        else
          data.push(['Comments:', '___________________________________________________'])
          data.push([' ','___________________________________________________'])
          data.push([' ','___________________________________________________'])
        end
        table(data, header: false, position: :left) do
          columns(0..-1).style(borders: [], align: :left, size: 14)
          column(0).style(width: heading_column, align: :right)
          column(1).style(width: data_column, font_style: :bold)
        end
        if cursor > 25
          move_cursor_to(25)
        end
        font('Helvetica')
        text("Questions concerning service information should be directed to the Civil Process Clerk\nin our office at Phone: (318) 256-5651.  Thank You!", size: 10, align: :center, style: :bold)
      end
    end
  end
end