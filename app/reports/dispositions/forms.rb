module Dispositions
  class Forms < ReportBase
    def initialize(dispositions)
      super(margin: [0,0])
      @dispositions = dispositions
      unless @dispositions.is_a?(Array)
        raise ArgumentError.new('Dispositions::Forms.initialize: First argument must be an Array of Disposition objects.')
      end
      @dispositions.each do |r|
        unless r.is_a?(Disposition)
          raise ArgumentError.new('Dispositions::Forms.initialize: First argument must be an Array of Disposition objects: supplied Array contains invalid object type(s)!')
        end
      end
    end
  
    def to_pdf
      # no header or footer
      # formatted to print on the short green disposition forms for the FBI
      bounding_box([margin_box.left, margin_box.top], width: margin_box.width, height: margin_box.height) do
        @dispositions.each do |r|
          move_cursor_to(635)
          indent(100) do
            text(r.person.fbi.to_s, size: 12)
          end
          move_cursor_to(610)
          indent(410) do
            text(AppUtils.format_date(r.sentence_date), size: 12)
          end
          move_cursor_to(568)
          indent(320,20) do
            text(r.disposition.to_s, size: 9)
          end
          move_cursor_to(600)
          indent(60) do
            text(r.person.sort_name, size: 12)
          end
          move_cursor_to(556)
          indent(105) do
            text(AppUtils.format_date(r.person.date_of_birth), size: 12)
          end
          move_cursor_to(556)
          indent(264) do
            text(r.person.sex_name, size: 12)
          end
          move_cursor_to(485)
          indent(60) do
            text(r.person.sid.to_s, size: 12)
          end
          move_cursor_to(485)
          indent(190) do
            text(r.person.ssn.to_s, size: 12)
          end
          move_cursor_to(480)
          indent(410) do
            text(SiteConfig.afis_ori.to_s, size: 11)
          end
          move_cursor_to(440)
          indent(340) do
            text("#{r.updater.contact.sort_name}, #{r.updater.contact.title}", size: 11)
            text(SiteConfig.agency, size: 11)
            text("#{SiteConfig.agency_city}, #{SiteConfig.agency_state} #{SiteConfig.agency_zip}", size: 11)
          end
          move_cursor_to(378)
          indent(300,50) do
            text(r.updater.contact.title.to_s, size: 11, align: :center)
          end
          move_cursor_to(455)
          indent(60) do
            text(SiteConfig.afis_ori.to_s, size: 11)
          end
          move_cursor_to(420)
          indent(60) do
            text(SiteConfig.afis_agency, size: 11)
            text(SiteConfig.afis_street, size: 11)
            text("#{SiteConfig.afis_city}, #{SiteConfig.afis_state} #{SiteConfig.afis_zip}", size: 11)
          end
          move_cursor_to(335)
          indent(60) do
            text(r.atn.to_s, size: 12)
          end
          move_cursor_to(335)
          indent(180) do
            text(AppUtils.format_date(r.arrest_date), size: 12)
          end
          move_cursor_to(305)
          indent(60,40) do
            r.arrests.collect{|a| a.charges}.flatten.compact.uniq.each do |c|
              text("[#{c.id}]: #{c.count} Cnt #{c.charge}", size: 9)
            end
          end
          # mark the report and arrest(s) as reported
          r.report_date = Date.today
          r.status = 3
          r.save(validate: false)
        end
      end
      render
    end
  end
end