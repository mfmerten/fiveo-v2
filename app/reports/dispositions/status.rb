module Dispositions
  class Status < ReportBase
    def initialize(dispositions)
      super
      @dispositions = dispositions
      unless @dispositions.is_a?(Array)
        raise ArgumentError.new('Dispositions::Status.initialize: First argument must be an Array of Disposition objects.')
      end
      @dispositions.each do |a|
        unless a.is_a?(Disposition)
          raise ArgumentError.new('Dispositions::Status.initialize: First argument must be an Array of Disposition objects: supplied Array contains invalid object type(s)!')
        end
      end
    end
  
    def to_pdf
      standard_report do
        unknown = []
        pending = []
        final = []
        ready = []
        invalid = []
        @dispositions.each do |a|
          a.update_status
          case a.status
          when -1
            invalid.push(a)
          when 1
            pending.push(a)
          when 2
            if a.disposition?
              ready.push(a)
            else
              final.push(a)
            end
          else
            unknown.push(a)
          end
        end
  
        text("Disposition Status", size: 18, style: :bold, align: :center)
        move_up(15)
        y_position = cursor
        text(Date.today.to_s(:us), size: 14, style: :bold)
        move_cursor_to(y_position)
        text(Date.today.to_s(:us), size: 14, style: :bold, align: :right)
  
        # do a page for "Unknown"
        unless unknown.empty?
          heading("UNKNOWN")
          move_up(5)
          text("The status of these could not be updated!", size: 12, align: :center, style: :bold)
          move_up(5)
          data = [["ATN","Person", "Arrest(s)"]]
          unknown.each do |r|
            data.push([r.atn, AppUtils.show_person(r.person), r.arrests.collect{|a| "<b>Arrest:</b> #{a.id} (#{AppUtils.format_date(a.arrest_datetime)})"}.join(", ")])
          end
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            columns(0..-1).style(align: :left, size: 10)
            column(0).style(width: 65)
            column(1).style(width: 165)
            column(2).style(inline_format: true)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
          move_down(10)
        end
  
        # do a page for "Pending"
        unless pending.empty?
          if cursor <= 50
            start_new_page
          end
          heading("PENDING")
          move_up(5)
          text("One or more charges have not been finalized.", size: 12, align: :center, style: :bold)
          move_up(5)
          data = [["ATN","Person", "Arrest(s)"]]
          pending.each do |r|
            data.push([r.atn, AppUtils.show_person(r.person), r.arrests.collect{|a| "<b>Arrest:</b> #{a.id} (#{AppUtils.format_date(a.arrest_datetime)})"}.join(", ")])
          end
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            columns(0..-1).style(align: :left, size: 10)
            column(0).style(width: 65)
            column(1).style(width: 165)
            column(2).style(inline_format: true)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
          move_down(10)
        end
  
        # do a page for "Final"
        unless final.empty?
          if cursor <= 50
            start_new_page
          end
          heading("FINAL")
          move_up(5)
          text("Need disposition entry.", size: 12, align: :center, style: :bold)
          move_up(5)
          data = [["ATN","Person", "Arrest(s)"]]
          final.each do |r|
            data.push([r.atn, AppUtils.show_person(r.person), r.arrests.collect{|a| "<b>Arrest:</b> #{a.id} (#{AppUtils.format_date(a.arrest_datetime)})"}.join(", ")])
          end
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            columns(0..-1).style(align: :left, size: 10)
            column(0).style(width: 65)
            column(1).style(width: 165)
            column(2).style(inline_format: true)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
          move_down(10)
        end
  
        # do a page for "Ready"
        unless ready.empty?
          if cursor <= 50
            start_new_page
          end
          heading("FINAL W/Disposition")
          move_up(5)
          text("Ready for disposition reports.", size: 12, align: :center, style: :bold)
          move_up(5)
          data = [["ATN","Person", "Arrest(s)"]]
          ready.each do |r|
            data.push([r.atn, AppUtils.show_person(r.person), r.arrests.collect{|a| "<b>Arrest:</b> #{a.id} (#{AppUtils.format_date(a.arrest_datetime)})"}.join(", ")])
          end
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            columns(0..-1).style(align: :left, size: 10)
            column(0).style(width: 65)
            column(1).style(width: 165)
            column(2).style(inline_format: true)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
          move_down(10)
        end
  
        # last, list records with problems
        unless invalid.empty?
          if cursor <= 50
            start_new_page
          end
          heading("INVALID")
          move_up(5)
          text("No valid person linked, no arrests or no charges.", size: 12, align: :center, style: :bold)
          move_up(5)
          data = [["ATN","Person", "Reason"]]
          invalid.each do |r|
            reason = []
            if r.person.nil?
              reason.push("Invalid Person")
            end
            if r.arrests.empty?
              reason.push("No Arrests")
            end
            if r.original_charges.empty? && r.docket_charges.empty?
              reason.push("No Charges")
            end
            data.push([r.atn, AppUtils.show_person(r.person), reason.join(", ")])
          end
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            columns(0..-1).style(align: :left, size: 10)
            column(0).style(width: 65)
            column(1).style(width: 165)
            column(2).style(inline_format: true)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
          move_down(10)
        end
        text("END OF REPORT", size: 12)
      end
    end
  end
end