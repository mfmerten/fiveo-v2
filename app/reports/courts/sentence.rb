module Courts
  class Sentence < ReportBase
    def initialize(court)
      super(page_size: 'LEGAL')
      @court = court
      unless @court.is_a?(Court)
        raise ArgumentError.new("Courts::Sentence.initialize: First argument must be a Court object.")
      end
    end
  
    def to_pdf
      # custom footer, no header
      repeat(:all) do
        bounding_box([margin_box.left, margin_box.bottom + 20], width: margin_box.width, height: 20) do
          font("Helvetica")
          stroke_horizontal_rule
          move_down(5)
          text("No: #{@court.id}", align: :left, size: 9)
          move_up(10)
          text("#{SiteConfig.agency}", align: :center, size: 12)
          font("Times-Roman")
        end
      end
  
      bounding_box([margin_box.left, margin_box.top - 10], width: margin_box.width, height: margin_box.height - 35) do
        # generate a sentence sheet for each unsentenced charge
        charges = @court.docket.da_charges.reject{|c| !c.sentence.nil? || c.info_only? || c.dropped_by_da?}
        if charges.empty?
          move_down(40)
          text("No Unsatisfied Charges to Print", size: 14, align: :center, style: :bold)
        else
          @court.docket.da_charges.reject{|c| !c.sentence.nil? || c.info_only? || c.dropped_by_da? }.each do |chg|
            arrest = nil
            citation = nil
            bonds = []
            if !chg.arrest_charge.nil?
              arrest = chg.arrest_charge.arrest
              unless arrest.nil?
                bonds = arrest.bonds
              end
              citation = chg.arrest_charge.citation
            elsif !chg.arrest.nil?
              arrest = chg.arrest
            end
            data = [
              ["The #{SiteConfig.court_title} Court", "#{SiteConfig.chief_justice_title}:", "#{@court.judge.blank? ? '__________________' : @court.judge.upcase}"],
              ["#{SiteConfig.jurisdiction_title}", "Docket No:", "#{@court.docket.docket_no}"],
              ["State Of #{State.get(SiteConfig.agency_state)}", "Court Date:", "#{@court.court_date? ? AppUtils.format_date(@court.court_date) : '___________'}"]
            ]
            table(data, header: false, width: bounds.width) do
              columns(0..-1).style(size: 16, align: :left, borders: [], padding: 1)
              column(1).style(align: :right)
              column(2).style(width: 125, font_style: :bold, padding_left: 5)
            end
            move_down(10)
            text("#{AppUtils.show_person(@court.docket.person).upcase}", size: 18, style: :bold)
            move_up(18)
            check_box_span(85, position: 452) do
              text("Not In Court", size: 14, style: :bold)
            end
            
            data = [["Charge:", "(#{chg.count}) #{chg.charge}"]]
            if !arrest.nil?
              data.push(["Arrest Date:", "#{AppUtils.format_date(arrest.arrest_datetime)}    <b>Arrest Agency:</b> #{arrest.agency}"])
              if arrest.atn?
                data.push(["ATN:",arrest.atn])
              end
              unless chg.arrest_charge.nil?
                data.push(["DWI Test Officer:", "#{arrest.dwi_test_officer? ? AppUtils.show_contact(arrest, field: :dwi_test_officer) : 'N/A'}     <b>DWI Test Results:</b> #{arrest.dwi_test_results? ? arrest.dwi_test_results : 'N/A'}"])
              end
            elsif !citation.nil?
              data.push(["Citation Date:","#{AppUtils.format_date(citation.citation_datetime)}"])
            end
            data.push(["Convicted of:","______________________________________________________________"])
            table(data, header: :false, width: bounds.width) do
              columns(0..-1).style(size: 12, padding: 1, borders: [])
              column(0).style(font_style: :bold, align: :right, width: 110)
              column(1).style(inline_format: true, align: :left, padding_left: 5)
              row(-1).style(size: 14)
            end
            move_down(10)
            # Result (plea or conviction)
            data = [[]]
            [].concat(Docket::TrialResult).push("DA Diversion").each do |o|
              data[0].push(' ')
              data[0].push(o)
            end
            table(data, position: :center) do
              column(0).style(size: 14)
            end
            move_down(5)
            
            heading("Fines/Costs")
            text("Fine $_______________ and _______________ C/C $_______________", size: 14)
            move_down(10)
            text("For a total of $____________  Must Pay By: ____________ Or Be In Court On: ____________", size: 14)
            move_down(10)
            text("OR in default serve _____ Hours _____ Days _____ Months _____Years Parish Jail.", size: 14)
  
            heading("Sentence")
            y_position = cursor
            text("_____ Hours _____ Days _____ Months _____ Years", size: 14)
            move_cursor_to(y_position)
            check_box_span(200, position: 311) do
              text("D.O.C.", size: 14)
            end
            move_cursor_to(y_position)
            check_box_span(200, position: 383) do
              text("Parish Jail", size: 14)
            end
            move_down(10)
            check_box_span(bounds.width - 32) do
              text("Suspended  (Except: _____ Hours _____ Days _____ Months _____ Years)", size: 14)
            end
  
            heading("Probation")
            y_position = cursor
            text("_____ Hours _____ Days _____ Months _____ Years", size: 14)
            move_cursor_to(y_position)
            check_box_span(100, position: 311) do
              text("State", size: 14)
            end
            move_cursor_to(y_position)
            check_box_span(100, position: 367) do
              text("Parish", size: 14)
            end
            move_cursor_to(y_position)
            check_box_span(100, position: 430) do
              text("Unsupervised", size: 14)
            end
            move_down(10)
            check_box_span(bounds.width - 32) do
              text("Reverts to Unsupervised _______________________________________", size: 14)
            end
  
            heading("Conditions")
            section_start = cursor
  
            # column 1
            width = (bounds.right / 2).to_i - 17
            check_box_span(width) do
              text("Credit for Time Served", size: 14)
            end
            move_down(10)
            check_box_span(width) do
              text("W/O Benefit", size: 14)
            end
            move_down(10)
            text("_____ Days _____ Hours Community Service.", size: 14)
            move_down(10)
            check_box_span(width) do
              text("Substance Abuse Program.", size: 14)
            end
            move_down(10)
            check_box_span(width) do
              text("Driver Improvement Program", size: 14)
            end
            move_down(10)
            text("Monthly Probation Fees: $____________", size: 14)
            move_down(10)
            text("Restitution to Victim: $____________", size: 14)
            move_down(10)
            text("Restitution Paid By: _______________", size: 14)
            move_down(10)
            check_box_span(width) do
              text("Delay execution of sentence (see notes)", size: 14)
            end
            move_down(10)
            check_box_span(width) do
              text("Pre-Sentence Investigation.", size: 14)
            end
  
            section_end = cursor
            move_cursor_to(section_start)
  
            # column 2
            col2 = width + 17
            check_box_span(width, position: col2) do
              text("Anger Management", size: 14)
            end
            move_down(10)
            check_box_span(width, position: col2) do
              text("Substance Abuse Treatment", size: 14)
            end
            move_down(10)
            check_box_span(width, position: col2) do
              text("Domestic Abuse Program", size: 14)
            end
            move_down(10)
            check_box_span(width, position: col2) do
              text("Random Drug Screens", size: 14)
            end
            move_down(10)
            check_box_span(width, position: col2) do
              text("No Contact With Victim", size: 14)
            end
            move_down(10)
            check_box_span(width, position: col2) do
              text("No Firearms During Probation", size: 14)
            end
            move_down(10)
            y_position = cursor
            span(width, position: col2) do
              text("Article:", size: 14)
            end
            move_cursor_to(y_position)
            check_box_span(30, position: col2 + 51) do
              text("893", size: 14)
            end
            move_cursor_to(y_position)
            check_box_span(30, position: col2 + 96) do
              text("894", size: 14)
            end
            move_cursor_to(y_position)
            check_box_span(30, position: col2 + 142) do
              text("895", size: 14)
            end
            move_down(10)
            span(width, position: col2) do
              text("D.A.R.E. Program: $____________", size: 14, position: col2)
              move_down(10)
              text("Indigent Defender Fund: $____________", size: 14, position: col2)
            end
            move_down(10)
            check_box_span(width, position: col2) do
              text("Deny Goodtime", size: 14)
            end
            move_cursor_to(section_end)
  
            heading("Concurrency")
            y_position = cursor
            span(65, position: 15) do
              text("Sentence:", size: 14, align: :right)
              move_down(10)
              text("Fines:", size: 14, align: :right)
              move_down(10)
              text("Costs:", size: 14, align: :right)
            end
            move_cursor_to(y_position)
            3.times do
              check_box_span(100, position: 85) do
                text("Concurrent", size: 14)
              end
              move_down(10)
            end
            move_cursor_to(y_position)
            3.times do
              check_box_span(100, position: 173) do
                text("Consecutive", size: 14)
              end
              move_down(10)
            end
            move_cursor_to(y_position)
            span(bounds.width - 268, position: 268) do
              3.times do
                text("___________________________________", size: 14)
                move_down(10)
              end
            end
            move_up(10)
            heading("Notes")
            move_up(10)
            3.times do
              move_down(20)
              stroke_horizontal_rule
            end
            move_down(5)
            text("Bond No: #{bonds.empty? ? 'N/A' : bonds.collect{|b| b.bond_no + ' (' + AppUtils.format_date(b.issued_datetime) + ')'}.join(',')}", size: 11, style: :bold)
            unless chg == @court.docket.da_charges.reject{|c| !c.sentence.nil? || c.info_only? || c.dropped_by_da? }.last
              start_new_page
            end
          end
        end
      end
      render
    end
  end
end