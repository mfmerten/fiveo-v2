module Courts
  class Revocation < ReportBase
    def initialize(court)
      super(page_size: 'LEGAL')
      @court = court
      unless @court.is_a?(Court)
        raise ArgumentError.new("Courts::Revocation.initialize: First argument must be a Court object.")
      end
      @docket = @court.docket
    end
  
    def to_pdf
      # special footer, no header
      repeat(:all) do
        bounding_box([margin_box.left, margin_box.bottom + 20], width: margin_box.width, height: 20) do
          font("Helvetica")
          stroke_horizontal_rule
          move_down(5)
          text("No: #{@court.id}", align: :left, size: 9)
          move_up(10)
          text("#{SiteConfig.agency}", align: :center, size: 12)
          font("Times-Roman")
        end
      end
      bounding_box([margin_box.left, margin_box.top - 10], width: margin_box.width, height: margin_box.height - 35) do
        data = [
          ["The #{SiteConfig.court_title} Court", "#{SiteConfig.chief_justice_title}:", "#{@court.judge.blank? ? '__________________' : @court.judge.upcase}"],
          ["#{SiteConfig.jurisdiction_title}", "Docket No:", "#{@docket.docket_no}"],
          ["State Of #{State.get(SiteConfig.agency_state)}", "Court Date:", "#{@court.court_date? ? AppUtils.format_date(@court.court_date) : '___________'}"]
        ]
        table(data, header: false, width: bounds.width) do
          columns(0..-1).style(size: 16, align: :left, borders: [], padding: 1)
          column(1).style(align: :right)
          column(2).style(width: 125, font_style: :bold, padding_left: 5)
        end
        move_down(10)
        text("PROBATION REVOCATION", size: 20, style: :bold, align: :center)
        move_down(10)
        text("#{AppUtils.show_person(@docket.person).upcase}", size: 18, style: :bold)
  
        heading("Docket Information")
  
        unless @docket.da_charges.empty?
          @docket.da_charges.reject{|c| c.sentence.nil? }.each do |c|
            text("Charge: #{c.count} #{c.count == 1 ? 'cnt' : 'cnts'} #{c.charge}", size: 14, style: :bold)
            move_down(5)
            span(70) do
              text("Sentence:", size: 12, align: :right, style: :bold)
            end
            move_up(14)
            if c.sentence.nil?
              txt = "Not Sentenced"
            else
              txt = "On #{AppUtils.format_date(c.sentence.sentence_date)}, #{c.sentence.sentence_summary}"
            end
            span(bounds.right - 75, position: 75) do
              text(txt, size: 12)
            end
            move_down(5)
          end
        end
  
        heading("Arrest Information")
        arrests = @docket.arrests
        arrests.uniq.each do |a|
          text("Arrest Date: #{AppUtils.format_date(a.arrest_datetime)}    Arrest Agency: #{a.agency}", size: 14, style: :bold)
          data = []
          a.district_charges.each do |c|
            data.push(["#{c.count} #{c.count == 1 ? 'cnt' : 'cnts'}", c.charge])
          end
          unless data.empty?
            move_down(5)
            span(70) do
              text("Charges:", size: 12, align: :right, style: :bold)
            end
            move_up(17)
            table(data, header: false, width: bounds.right - 70, position: 70) do
              columns(0..-1).style(borders: [], align: :left, size: 12, padding: 1)
              column(0).style(width: 35, padding_left: 5)
            end
          end
          data = []
          a.bonds.each do |b|
            data.push([b.bond_no, AppUtils.format_date(b.issued_datetime), b.surety_name, currency(b.bond_amt), b.status_name])
          end
          unless data.empty?
            move_down(5)
            span(70) do
              text("Bonds:", size: 12, align: :right, style: :bold)
            end
            move_up(17)
            table(data, header: false, position: 70, width: bounds.right - 70) do
              columns(0..-1).style(borders: [], align: :left, size: 12, padding: 1)
              column(0).style(padding_left: 5)
              column(3).style(align: :right)
            end
          end
        end
        move_down(5)
        citations = @docket.citations
        citations.uniq.each do |m|
          text("Citation Date: #{AppUtils.format_date(m.citation_datetime)}    Citation No: #{m.ticket_no}", size: 14)
          data = []
          m.charges.each do |c|
            data.push(["#{c.count} #{c.count == 1 ? 'cnt' : 'cnts'}", c.charge])
          end
          unless data.empty?
            move_down(5)
            span(70) do
              text("Charges:", size: 12, align: :right, style: :bold)
            end
            move_up(17)
            table(data, header: false, position: 70, width: bounds.right - 70) do
              columns(0..-1).style(borders: [], align: :left, size: 12, padding: 1)
              column(0).style(width: 35, padding_left: 5)
            end
          end
        end
  
        move_down(5)
        heading("Revocation Information")
        
        box_width = bounds.width - 20
        y_position = cursor
        check_box_span(box_width) do
          text("D.O.C.", size: 14)
        end
        move_cursor_to y_position
        check_box_span(box_width, position: 75) do
          text("Parish", size: 14)
        end
        
        move_down(10)
        check_box_span(box_width) do
          text("Probation Revoked to serve original sentence with the following conditions:", size: 14)
        end
        
        box_width = bounds.width - 50
        move_down(10)
        check_box_span(box_width, position: 32) do
          text("Credit for time served", size: 14)
        end
        
        move_down(10)
        check_box_span(box_width, position: 32) do
          text("Deny Goodtime", size: 14)
        end
        
        move_down(10)
        check_box_span(box_width, position: 32) do
          text("Delay execution of sentence (see notes)", size: 14)
        end
        
        move_down(10)
        check_box_span(box_width, position: 32) do
          text("Concurrently with any time now serving", size: 14)
        end
        
        move_down(10)
        check_box_span(box_width, position: 32) do
          text("Consecutively with any time now serving", size: 14)
        end
        
        move_down(10)
        check_box_span(box_width, position: 32) do
          text("Concurrent with _______________________________________", size: 14)
        end
        
        move_down(10)
        check_box_span(box_width, position: 32) do
          text("Consecutive with _______________________________________", size: 14)
        end
        
        move_down(15)
        check_box_span(bounds.right - 20) do
          text("Special Conditions of Probation:", size: 14)
        end
        
        move_down(10)
        check_box_span(box_width, position: 32) do
          text("Serve ______ Hours, ______ Days, ______ Months, ______ Years in Parish Jail", size: 14)
        end
        
        move_down(10)
        check_box_span(box_width, position: 32) do
          text("Credit for time served", size: 14)
        end
        
        move_down(10)
        check_box_span(box_width, position: 32) do
          text("Deny Goodtime", size: 14)
        end
        
        move_down(10)
        check_box_span(box_width, position: 32) do
          text("Delay execution of sentence (see notes)", size: 14)
        end
        
        heading("Notes")
        move_up(10)
        (1..4).each do
          move_down(25)
          stroke_horizontal_rule
        end
      end
      number_pages("Page: <page> of <total>", at: [bounds.right - 65, 15])
      render
    end
  end
end