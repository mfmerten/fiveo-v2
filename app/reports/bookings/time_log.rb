module Bookings
  class TimeLog < ReportBase
    def initialize(booking)
      super
      @booking = booking
      unless @booking.is_a?(Booking)
        raise ArgumentError.new("Bookings::TimeLog.initialize: First argument must be a valid Booking object.")
      end
    end
  
    def to_pdf
      standard_report(footer_no: @booking.id) do
        text(@booking.jail.name, size: 16, style: :bold, align: :center)
        text("Time Log for Booking ID: #{@booking.id}", align: :center, size: 16, style: :bold)
        text(AppUtils.show_person(@booking.person), align: :center, size: 18, style: :bold)
        text("As Of: #{Time.now.to_s(:us)}", align: :center, size: 14, style: :bold)
        move_down(8)
        # Build an array and let prawn page the results
        data = []
        total_hours = 0
        @booking.booking_logs.each do |l|
          total_hours += l.hours_served
          data.push([AppUtils.format_date(l.start_datetime),AppUtils.show_contact(l, field: :start_officer),AppUtils.format_date(l.stop_datetime),"#{l.stop_datetime? ? AppUtils.show_contact(l, field: :stop_officer) : ' '}",l.hours_served])
        end
        data.push([" "," "," ","Total Hours:",total_hours])
        data.unshift(["Started", "By", "Stopped", "By", "Hours"])
        table(data, header: true, row_colors: [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even]) do
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom] 
          rows(1..-1).padding = 3
          rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
          columns(0..-1).style(align: :left, size: 11)
          column(0).style(width: 95)
          column(1).style(width: 165)
          column(2).style(width: 95)
          column(3).style(width: 165)
          column(4).style(width: 50)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
        end
        move_down(8)
        text("END OF REPORT", style: :bold)
      end
    end
  end
end