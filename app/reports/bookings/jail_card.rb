module Bookings
  class JailCard < ReportBase
    def initialize(booking, arrests=[], sentences=[], other=[])
      super(page_size: 'LEGAL')
      @booking = booking
      unless @booking.is_a?(Booking)
        raise ArgumentError.new("Bookings::JailCard.initialize: First argument must be a valid Booking object.")
      end
      @person = @booking.person
      unless @person.is_a?(Person)
        raise ArgumentError.new("Bookings::JailCard.initialize: First argument must be a valid Booking object: supplied Booking has invalid Person link!")
      end
      @arrests = arrests
      unless @arrests.is_a?(Array)
        raise ArgumentError.new("Bookings::JailCard.initialize: Second argument must be an Array of Arrest objects.")
      end
      @arrests.each do |a|
        unless a.is_a?(Hold)
          raise ArgumentError.new("Bookings::JailCard.initialize: Second argument must be an Array of Hold objects: supplied Array contains invalid object type(s)!")
        end
      end
      @sentences = sentences
      unless @sentences.is_a?(Array) or @sentences.is_a?(ActiveRecord::Relation)
        raise ArgumentError.new("Bookings::JailCard.initialize: Third argument must be an Array of Hold objects.")
      end
      @sentences.each do |s|
        unless s.is_a?(Hold)
          raise ArgumentError.new("Bookings::JailCard.initialize: Third argument must be an Array of Hold objects: supplied Array contains invalid object types(s)!")
        end
      end
      @other = other
      unless @other.is_a?(Array)
        raise ArgumentError.new("Bookings::JailCard.initialize: Fourth argument must be an Array of Hold objects.")
      end
      @other.each do |h|
        unless h.is_a?(Hold)
          raise ArgumentError.new("Bookings::JailCard.initialize: Fourth argument must be an Array of Hold objects: supplied Array contains invalid object type(s)!")
        end
      end
    end
    
    def to_pdf
      # note: custom header/footer and page layout
      repeat(:all) do
        bounding_box([margin_box.left, margin_box.bottom + 20], width: margin_box.width, height: 20) do
          font("Helvetica")
          stroke_horizontal_rule
          move_down(5)
          text("No: #{@booking.id}", align: :left, size: 9)
          move_up(10)
          text(SiteConfig.agency, align: :center, size: 12)
          font("Times-Roman")
        end
      end
      bounding_box([margin_box.left, margin_box.top - 10], width: margin_box.width, height: margin_box.height - 35) do
        text("#{@booking.jail.nil? ? "Invalid Jail!" : @booking.jail.name}", align: :center, size: 18, style: :bold)
        text("Jail Card", align: :center, size: 24, style: :bold)
        text("Printed: #{Time.now.to_s(:us)}", size: 12, style: :bold, align: :center)
        stroke_horizontal_rule
        move_down(5)
        text(AppUtils.show_person(@person), size: 18, style: :bold)
        text("#{@person.aka? ? 'Known Aliases: ' + @person.aka : 'No Known Aliases'}", size: 14)
        stroke_horizontal_rule
        move_down(7)
        row_pos = cursor
        text_field([col(2,2), row_pos], col(2), 'Booking Officer:', AppUtils.show_contact(@booking, field: :booking_officer))
        text_field([col(4,1), row_pos], col(4), 'Booking ID:', "#{@booking.id}")
        row_pos -= text_field([col(4,2), row_pos], col(4), 'Booking Date/Time:', AppUtils.format_date(@booking.booking_datetime))
        text_field([col(2,2), row_pos], col(2), "Emergency Contact:", "#{@person.emergency_contact}\n#{@person.em_relationship}\n#{@person.emergency_phone}", pdf_color_scheme: 'em', rows: 3)
        text_field([col(4,1), row_pos], col(4), "Date of Birth:",AppUtils.format_date(@person.date_of_birth))
        row_pos -= text_field([col(4,2), row_pos], col(4), 'SSN:', "#{@person.ssn} ")
        text_field([col(4,2), row_pos], col(4), 'Sex:', "#{@person.sex_name}")
        row_pos -= text_field([col(4,1), row_pos], col(4), 'Race:', @person.race)
        row_pos -= text_field([col(2,1), row_pos], col(2), 'Phone Called:', "#{@booking.phone_called}")
        row_pos -= text_field([col(1,1), row_pos], col(1), 'Medications:', "#{@person.medication_summary}", rows: 2)
        move_down(5)
        heading("Cash And Items Received")
        row_pos -= 25
        (-1..@booking.properties.size + 1).each do |i|
          next if i.even?
          if i == -1
            # cash
            text_field([col(2,1), row_pos], col(2), 'Cash:', currency(@booking.cash_at_booking))
          else
            # property
            text_field([col(2,1), row_pos], col(2), 'Property:', "#{@booking.properties[i].nil? ? '' : '(' + @booking.properties[i].quantity.to_s + ') ' + @booking.properties[i].description}")
          end
          # property
          row_pos -= text_field([col(2,2), row_pos], col(2), 'Property:', "#{@booking.properties[i+1].nil? ? '' : '(' + @booking.properties[i+1].quantity.to_s + ') ' + @booking.properties[i+1].description}")
        end
        unless @arrests.empty?
          move_down(5)
          heading('Current Arrests')
          move_up(10)
          arrs = @arrests.collect{|h| h.arrest}.compact.uniq
          data = []
          arrs.each do |a|
            data.push([a.id, AppUtils.format_date(a.arrest_datetime),a.agency, "#{a.charges.join(', ')}\nHolds: #{a.holds.active.join(', ')}"])
          end
          unless data.empty?
            data.unshift(['Arr. ID','Date/Time','Agency','Charges'])
            table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], position: :center) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom] 
              rows(1..-1).padding = 3
              columns(0..-1).style(align: :left, size: 12)
              column(0).style(width: 50)
              column(1).style(width: 100)
              column(2).style(width: 125)
              column(3).style(width: 290)
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
          end
        end
        unless @sentences.empty?
          move_down(5)
          heading("Current Sentences")
          data = []
          @sentences.each do |h|
            text = ""
            if !h.sentence.nil?
              text << "#{h.doc? ? '[DOC] ' : ''}#{h.sentence.conviction}(#{h.sentence.conviction_count})"
            elsif !h.doc_record.nil?
              text << "#{h.doc_record.conviction? ? '[DOC] ' + h.doc_record.conviction : '[DOC] Convictions Not On File'}"
            elsif !h.revocation.nil?
              probs = h.revocation.docket.probations.collect{|p| p.conviction + "(#{p.conviction_count})"}
              if probs.blank?
                text << "Revocation: #{h.doc? ? '[DOC] ' : ''}Convictions Not On File"
              else
                text << "Revocation: #{h.doc? ? '[DOC] ' : ''}#{probs.join(', ')}"
              end
            else
              text << "Convictions Not On File"
            end
            data.push([h.id, AppUtils.format_date(h.hold_datetime), text, h.hours_remaining])
          end
          unless data.empty?
            move_up(10)
            data.unshift(['Hold ID','Date/Time','Convicted Of','Hours Left'])
            table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], position: :center) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom] 
              rows(1..-1).padding = 3
              columns(0..-1).style(align: :left, size: 12)
              column(0).style(width: 50)
              column(1).style(width: 100)
              column(2).style(width: 350)
              column(3).style(width: 65)
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
          end
        end
        unless @other.empty?
          move_down(5)
          heading("Other Relevant Holds")
          data = []
          @other.each do |h|
            data.push([h.id, AppUtils.format_date(h.hold_datetime), "#{h.hold_type.blank? ? 'Invalid Type' : h.hold_type} ","#{h.agency? ? h.agency : ' '}","#{h.hold_by_unit} #{h.hold_by}"])
          end
          unless data.empty?
            move_up(10)
            data.unshift(['Hold ID','Date/Time','Type','For','By'])
            table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], position: :center) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom] 
              rows(1..-1).padding = 3
              columns(0..-1).style(align: :left, size: 12)
              column(0).style(width: 50)
              column(1).style(width: 100)
              column(2).style(width: 125)
              column(3).style(width: 125)
              column(4).style(width: 165)
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
          end
        end
        move_down(5)
        heading("Booking Statement")
        text("(1) I have been advised of the charges against me.", size: 12, style: :bold)
        text("(2) All property and/or money listed above is in the custody of #{SiteConfig.agency} and will be returned upon my leaving.", size: 12, style: :bold)
        text("(3) I have been allowed my right to make a telephone call.", size: 12, style: :bold)
        move_down(10)
        span(bounds.width - 40, position: 20) do
          text("This will serve as notice to the undersigned that any personal property and/or money not claimed within 90 days after the inmate is released from #{SiteConfig.agency} becomes the property of #{SiteConfig.agency}.  There will be no further notice as the undersigned acknowledges his/her understanding of this provision by their signature.", size: 14, style: :bold)
        end
        move_down(35)
        signature_line("Inmate Signature")
        heading("Release Statement")
        text("I have received all property and/or money from #{SiteConfig.agency}.", size: 14, style: :bold)
        move_down(35)
        y_position = cursor
        signature_line("Inmate Signature")
        move_cursor_to(y_position)
        stroke_horizontal_line(col(4,1), col(4,1) + col(4))
        move_down(2)
        span(col(4), position: col(4,1)) do
          text("Release Date", size: 9)
        end
        move_cursor_to(y_position)
        stroke_horizontal_line(col(4,2), col(4,2) + col(4))
        move_down(2)
        span(col(4), position: col(4,2)) do
          text("Release Time", size: 9)
        end
      end
      number_pages("Page: <page> of <total>", at: [bounds.right - 65, 15])
      render
    end
  end
end