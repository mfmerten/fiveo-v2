module Bookings
  class Headcount < ReportBase
    def initialize(report_date,jail_id)
      super
      @report_date = report_date
      unless @report_date.is_a?(Date)
        raise ArgumentError.new('Bookings::Headcount.initialize: First argument must be a Date object.')
      end
      @jail = Jail.find_by_id(jail_id)
      if @jail.nil?
        raise ArgumentError.new('Bookings::Headcount.initialize: Second argument must be a valid Jail ID.')
      end
      @report_datetime = AppUtils.get_datetime(@report_date, "23:59")
    end
  
    def to_pdf
      standard_report(footer_id: @report_datetime.to_s(:timestamp)) do
        # generate the report data
        btypes = ["Temporary Release","DOC","DOC Billable","City","Parish","Other","Not Billable"]
        report_data = {}
        btypes.each do |t|
          report_data[t] = []
        end
        report_data['Temporary Release'], report_data['DOC'], report_data['DOC Billable'], report_data['City'], report_data['Parish'], report_data['Other'], report_data['Not Billable'] = @jail.headcount(@report_date, "ALL")
        report_data['Transfers'] = @jail.transfers.between(@report_date.to_time, @report_date.to_time.end_of_day).includes(:booking).all
        totals = {'count' => 0, 'billable' => 0, 'physical' => 0, 'released' => 0, 'booked' => 0, 'transferred' => 0}
        btypes.each do |t|
          totals[t] = {'count' => 0, 'billable' => 0, 'physical' => 0, 'released' => 0, 'booked' => 0, 'transferred' => 0}
          total = 0
          physical = 0
          billable = 0
          released = 0
          booked = 0
          transferred = 0
          data = []
          report_data[t].each do |b|
            total += 1
            totals[t]['count'] += 1
            totals['count'] += 1
            unless t == "Temporary Release" || t == 'Not Billable' || (t == 'DOC Billable' && b.release_datetime? && b.release_datetime.to_date == @report_date)
              billable += 1
              totals[t]['billable'] += 1
              totals['billable'] += 1
            end
            if b.release_datetime? && b.release_datetime.to_date == @report_date
              released += 1
              totals[t]['released'] += 1
              totals['released'] += 1
            end
            if b.booking_datetime? && b.booking_datetime.to_date == @report_date
              booked += 1
              totals[t]['booked'] += 1
              totals['booked'] += 1
            end
            agency = b.is_transferred(@report_datetime)
            if agency.nil?
              agency = ' '
            else
              transferred += 1
              totals[t]['transferred'] += 1
              totals['transferred'] += 1
            end
            if agency == ' ' && (!b.release_datetime? || b.release_datetime.to_date != @report_date) && t != 'Temporary Release'
              physical += 1
              totals[t]['physical'] += 1
              totals['physical'] += 1
            end
            if t == 'Other' && agency == ' '
              agency = b.billing_status(@report_date)
            end
            if t == "Temporary Release"
              data.push([b.id, AppUtils.show_person(b.person), AppUtils.format_date(b.booking_datetime), "Temporary Release", b.temporary_release_reason])
            else
              data.push([b.id, AppUtils.show_person(b.person), AppUtils.format_date(b.booking_datetime), "#{b.release_datetime? && b.release_datetime.to_date == @report_date ? b.released_because + ': ' + AppUtils.format_date(b.release_datetime.to_date) : ' '}", agency])
            end
          end
          text("Daily Headcount for #{@jail.acronym}", align: :center, size: 20, style: :bold)
          move_down(5)
          text("#{t == 'Not Billable' ? 'Not Billable (Temporary Transfers)' : t}", align: :center, size: 18, style: :bold)
          move_down(5)
          text(AppUtils.format_date(@report_date), size: 12, style: :bold, align: :center)
          move_down(5)
          if data.empty?
            move_down(60)
            text("Nothing to report.", size: 12, style: :bold, align: :center)
            move_down(60)
          else
            data.unshift(['Booking#','Inmate','Booked','Released',"#{t == 'Other' ? 'Billable To' : 'Transferred To'}"])
            table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom] 
              rows(1..-1).padding = 3
              columns(0..-1).style(align: :left)
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
            move_down(5)
            data = [
              [' ','Count', 'Booked', 'Released', 'Transferred', 'Physical', 'Billable'],
              ["#{t}:",total,booked,released,transferred,physical,billable]
            ]
            table(data, header: true, width: bounds.width) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom] 
              rows(1..-1).padding = 3
              rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
              columns(0..-1).style(align: :right)
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
          end
          start_new_page
        end
        # do the summary page
        text("Daily Headcount for #{@jail.acronym}", align: :center, size: 20, style: :bold)
        move_down(5)
        text("REPORT SUMMARY", align: :center, size: 18, style: :bold)
        move_down(5)
        text(AppUtils.format_date(@report_date), size: 12, style: :bold, align: :center)
        move_down(5)
        data = []
        btypes.each do |t|
          data.push(["#{t == 'Not Billable' ? '*' : ''}#{t}:",totals[t]['count'],totals[t]['booked'],totals[t]['released'],totals[t]['transferred'],totals[t]['physical'],totals[t]['billable']])
        end
        data.push(["Grand Totals:",totals['count'],totals['booked'],totals['released'],totals['transferred'],totals['physical'],totals['billable']])
        data.unshift([' ','Count','Booked','Released','Transferred','Physical','Billable'])
        table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom] 
          rows(1..-1).padding = 3
          rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
          columns(0..-1).style(align: :right)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
        end
        move_down(5)
        text("*These are non-billable temporary transfers.", size: 10)
        # add a page for the transfer log for that day
        start_new_page
        text("Daily Headcount for #{@jail.acronym}", align: :center, size: 20, style: :bold)
        move_down(5)
        text("TRANSFER LOG", align: :center, size: 18, style: :bold)
        move_down(5)
        text(AppUtils.format_date(@report_date), size: 12, style: :bold, align: :center)
        move_down(5)
        if report_data['Transfers'].empty?
          move_down(50)
          text("Nothing To Report", size: 14, align: :center)
        else
          data = []
          report_data['Transfers'].each do |t|
            data.push(["#{t.booking.nil? ? '' : t.booking.id}", AppUtils.show_person(t.person), t.transfer_type, t.from_agency, t.to_agency])
          end
          data.unshift(['Booking','Name','Type','From','To'])
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
            columns(0..-1).style(align: :left)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
        end
      end
    end
  end
end