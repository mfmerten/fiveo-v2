module Bookings
  class FcsReport < ReportBase
    def initialize(start_date, stop_date, bookings, jail_id)
      super(page_layout: :landscape)
      @start_date = start_date
      unless @start_date.is_a?(Date)
        raise ArgumentError.new('Bookings::FcsReport.initialize: First argument must be a Date object.')
      end
      @stop_date = stop_date
      unless @stop_date.is_a?(Date)
        raise ArgumentError.new('Bookings::FcsReport.initialize: Second argument must be a Date object.')
      end
      @bookings = bookings
      unless @bookings.is_a?(Array)
        raise ArgumentError.new('Bookings::FcsReport.initialize: Third argument must be an Array of Booking objects.')
      end
      @bookings.each do |b|
        unless b.is_a?(Booking)
          raise ArgumentError.new('Bookings::FcsReport.initialize: Third argument must be an Array of Booking objects: supplied Array contains invalid object type(s)!')
        end
      end
      @jail = Jail.find(jail_id)
    end
    
    def to_pdf
      standard_report(header2: @jail.street, header3: "#{@jail.city}, #{@jail.state} #{@jail.zip}", header4: "Phone: #{@jail.phone}   FAX: #{@jail.fax}") do
        text(@start_date.strftime("%B %Y"), size: 12, style: :bold)
        move_up(14)
        text(@start_date.strftime("%B %Y"), size: 12, style: :bold, align: :right)
        move_up(14)
        text("Fraud and Child Support (FCS) Report", size: 16, style: :bold, align: :center)
        text("LA0072 RID 3357", size: 14, style: :bold)
        move_up(16)
        text("LA0072 RID 3357", size: 14, style: :bold, align: :right)
        text(@jail.name, size: 14, style: :bold, align: :center)
        data = []
        @bookings.each do |b|
          if p = b.person
            status = "#{b.released_because.blank? ? 'In Jail' + (b.hold_for_probation? ? ' - Prob Hold' : '') : b.released_because}"
            if status == "Transferred"
              unless b.transfers.empty?
                status << ": #{b.transfers.last.to_agency}"
              end
            end
            data.push([p.id, p.sort_name, "#{AppUtils.format_date(p.date_of_birth)} ", "#{p.ssn} ", "#{p.sex_name(true)} ", p.race_abbreviation, AppUtils.format_date(b.booking_datetime.to_date), status, AppUtils.format_date(b.release_datetime.to_date)])
          end
        end
        data.unshift(['ID No.', 'Name', "DOB", "SSN", "Sex", "Race", "Arrest Date","Status","Release Date"])
        table(data, header: true, row_colors: [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even]) do
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom] 
          rows(1..-1).padding = 3
          rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
          columns(0..-1).style(align: :left, size: 11)
          column(0).style(width: 55)
          column(1).style(width: 200)
          column(2).style(width: 75)
          column(3).style(width: 75)
          column(4).style(width: 35)
          column(5).style(width: 35)
          column(6).style(width: 75)
          column(7).style(width: 125)
          column(8).style(width: 75)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
        end
        move_down(5)
        text("END OF REPORT", style: :bold)
      end
    end
  end
end