module Bookings
  class List < ReportBase
    def initialize(bookings, jail_id, title=nil)
      super
      @bookings = bookings
      unless @bookings.is_a?(Array)
        raise ArgumentError.new("Bookings::List.initialize: First argument must be an Array of Booking objects.")
      end
      @bookings.each do |b|
        unless b.is_a?(Booking)
          raise ArgumentError.new("Bookings::List.initialize: First argument must be an Array of Booking objects: supplied Array contains invalid object type(s)!")
        end
      end
      @jail = Jail.find(jail_id)
      @title = title
      unless @title.nil? || @title.is_a?(String)
        raise ArgumentError.new("Bookings::List.initialize: Fourth argument must be a String object or Nil.")
      end
    end
    
    def to_pdf
      standard_report do
        head_count = @bookings.size
        phys_count = head_count
        text("#{@jail.acronym} Booking List", align: :center, size: 24, style: :bold)
        text(Date.today.to_s(:us), align: :center, size: 14, style: :bold)
        move_down(8)
        unless @title.blank?
          text(@title, align: :center, size: 14, style: :bold)
          move_down(5)
        end
        # Build an array and let prawn page the results
        data = []
        @bookings.each do |book|
          status = ""
          gone = false
          if book.doc?
            status << "[DOC]"
          end
          if book.release_datetime?
            unless status.empty?
              status << " "
            end
            status << "[Released]"
            gone = true
          end
          if !book.is_transferred.nil?
            unless status.empty?
              status << " "
            end
            status << "[Temp Transfer]"
            gone = true
          end
          if book.current_location =~ /^Absent: /
            gone = true
          end
          if book.temporary_release?
            gone = true
          end
          if gone
            phys_count -= 1
          end
          data.push([book.id, book.person.id, book.person.sort_name, book.current_location, status])
        end
        data.unshift(["Booking", "ID", "Inmate", "Current Location", "Status"])
        table(data, header: true, row_colors: [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even]) do
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom] 
          rows(1..-1).padding = 3
          columns(0..-1).style(align: :left, size: 12)
          column(0).style(width: 55)
          column(1).style(width: 55)
          column(2).style(width: 190)
          column(3).style(width: 175)
          column(4).style(width: 95)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
        end
        move_down(8)
        text("END OF REPORT -- Booking Records: #{head_count}, Physical Head Count: #{phys_count}", style: :bold)
      end
    end
  end
end