module Bookings
  class ArraignmentNotice < ReportBase
    def initialize(booking, arrests, charges)
      super(page_size: 'LEGAL')
      @booking = booking
      unless @booking.is_a?(Booking)
        raise ArgumentError.new('Bookings::ArraignmentNotice.initialize: First argument must be a valid Booking object.')
      end
      @arrests = arrests
      unless @arrests.is_a?(Array)
        raise ArgumentError.new('Bookings::ArraignmentNotice.initialize: Second argument must be an Array of Arrest objects.')
      end
      @arrests.each do |a|
        unless a.is_a?(Arrest)
          raise ArgumentError.new('Bookings::ArraignmentNotice.initialize: Second argument must be an Array of Arrest objects: supplied Array contains invalid object type(s)!')
        end
      end
      @charges = charges
      unless @charges.is_a?(String) || @charges.nil?
        raise ArgumentError.new('Bookings::ArraignmentNotice.initialize: Third argument must be a String object or Nil.')
      end
    end
    
    def to_pdf
      standard_report(footer_no: @booking.person_id.to_s) do
        font('Helvetica')
        text("Official Notice / Order to Appear for Arraignment", size: 24, style: :bold, align: :center)
        font('Times-Roman')
        move_down(5)
        text("State of #{State.get(SiteConfig.agency_state)}", size: 14, style: :bold)
        text("VS", size: 14, style: :bold)
        text(@booking.person.sort_name.upcase, size: 14, style: :bold)
        move_up(48)
        span(175, position: bounds.width - 175) do
          text("#{SiteConfig.court_title} Court", size: 14, style: :bold)
          text("#{SiteConfig.jurisdiction_title}", size: 14, style: :bold)
          text("State Of #{State.get(SiteConfig.agency_state)}", size: 14, style: :bold)
        end
        move_down(20)
        text("<b>YOU ARE ORDERED</b> to appear in the #{SiteConfig.jurisdiction_title} Courthouse in #{SiteConfig.agency_city}, #{State.get(SiteConfig.agency_state)},", size: 14, inline_format: true)
        move_down(5)
        text("on the ______ day of ________________, _________, at <b>9:00 AM</b> for arraignment on the charge / charges of: <b>#{@charges}</b>.", size: 14, inline_format: true)
        move_down(20)
        font('Helvetica')
        span(bounds.width - 100, position: 50) do
          text("FAILURE TO APPEAR FOR THIS HEARING WILL RESULT IN A BENCH WARRANT BEING ISSUED FOR YOUR ARREST AND YOUR BOND BEING FORFEITED!", size: 16, style: :bold)
        end
        font('Times-Roman')
        move_down(20)
        text("<b>Thus Done and Signed</b> on this ______ day of ________________, _________.", size: 14, inline_format: true)
        move_down(50)
        y_position = cursor
        signature_line("Defendant", placement: :left)
        move_cursor_to(y_position)
        signature_line("Officer / Deputy")
        move_down(40)
        signature_line("Bondsman and/or Surety", placement: :left)
        move_down(5)
        heading("Bond Information")
        @arrests.each do |a|
          text("Arrest ID: <b>#{a.id}</b>    Date/Time: <b>#{AppUtils.format_date(a.arrest_datetime)}</b>    Bond Set By: <b>#{a.hour_72_judge}</b>", size: 14, inline_format: true)
          data = []
          total_amt = BigDecimal.new('0.00',2)
          a.bonds.reject{|b| b.status? || b.final?}.each do |b|
            data.push([b.bond_no, b.surety_name, b.bond_type, currency(b.bond_amt)])
            total_amt += b.bond_amt
          end
          if data.empty?
            span(bounds.width - 50, position: 50) do
              text("[No Active Bonds For This Arrest]", size: 12, style: :bold, position: 50)
            end
          else
            data.unshift(['Bond No', 'Bondsman/Surety', 'Type', 'Amount'])
            move_up(5)
            table(data, header: true, width: bounds.width - 50, position: 50, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]) do
              rows(1..-2).borders = [:left, :right]
              rows(-1).borders = [:left, :right, :bottom] 
              rows(1..-1).padding = 3
              rows(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
              columns(0..-1).style(align: :left)
              column(3).style(align: :right)
              rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
            end
          end
          move_down(5)
        end
        heading("Address Information")
        span(col(2), position: col(2,2)) do
          fill_color(SiteConfig.pdf_em_txt)
          text("This Section Must Be Completed", size: 14, style: :bold, align: :center)
          fill_color('000000')
        end
        move_down(5)
        sep = "\n"
        row_pos = cursor
        text_field([col(2,2), row_pos], col(2), 'Physical (911) Service Address:', " ", pdf_color_scheme: 'em', rows: 4)
        row_pos -= text_field([col(2,1), row_pos], col(2), 'Physical Address On File:', "#{@booking.person.physical_line1}#{sep}#{@booking.person.physical_line2}", rows: 2)
        row_pos -= text_field([col(2,1), row_pos], col(2), 'Mailing Address On File:', "#{@booking.person.mailing_line1}#{sep}#{@booking.person.mailing_line2}", rows: 2)
        text_field([col(2,1), row_pos], col(2), 'Phone(s) On File:', "#{@booking.person.home_phone? ? 'Home: ' + @booking.person.home_phone + ' ' : ''}#{@booking.person.cell_phone? ? 'Cell: ' + @booking.person.cell_phone : ''}")
        text_field([col(2,2), row_pos], col(2), 'Valid Phone Number:', " ", pdf_color_scheme: 'em')
      end
    end
  end
end