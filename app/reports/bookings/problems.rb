module Bookings
  class Problems < ReportBase
    def initialize(booking_problems)
      super
      @booking_problems = booking_problems
      unless @booking_problems.is_a?(Array)
        raise ArgumentError.new("Bookings::Problems.initialize: First argument must be an Array of Booking Problems ([Booking Object, Problem String]).")
      end
      @booking_problems.each do |b|
        valid = true
        if b.is_a?(Array)
          unless b[0].is_a?(Booking)
            valid = false
          end
          unless b[1].is_a?(String)
            valid = false
          end
        else
          valid = false
        end
        unless valid
          raise ArgumentError.new("Bookings::Problems.initialize: First argument must be an Array of Booking Problems ([Booking Object, Problem String]): supplied Array contains invalid object type(s)!")
        end
      end
    end
  
    def to_pdf
      standard_report do
        text("Booking Problem List", align: :center, size: 20, style: :bold)
        text(Date.today.to_s(:us), align: :center, size: 18, style: :bold)
        move_down(8)
        # Build an array and let prawn page the results
        data = []
        total_hours = 0
        @booking_problems.each do |b|
          data.push([b[0].id,AppUtils.show_person(b[0].person),"#{b[0].jail.nil? ? 'Invalid' : b[0].jail.acronym}", b[1]])
        end
        data.unshift(["Booking", "Inmate", "Facility", "Problem(s)"])
        table(data, header: true, row_colors: [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even]) do
          rows(1..-2).borders = [:left, :right]
          rows(-1).borders = [:left, :right, :bottom] 
          rows(1..-1).padding = 3
          columns(0..-1).style(align: :left, size: 11)
          column(0).style(width: 65)
          column(1).style(width: 180)
          column(2).style(width: 65)
          column(3).style(width: 260)
          rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
        end
        move_down(8)
        text("END OF REPORT", style: :bold)
      end
    end
  end
end