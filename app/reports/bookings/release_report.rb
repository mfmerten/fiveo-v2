module Bookings
  class ReleaseReport < ReportBase
    def initialize(start_date, stop_date, jail, bookings)
      super(page_layout: :landscape)
      @start_date = start_date
      unless @start_date.is_a?(Date)
        raise ArgumentError.new("Bookings::ReleaseReport.initialize: First argument must be a Date object.")
      end
      @stop_date = stop_date
      unless @stop_date.is_a?(Date)
        raise ArgumentError.new("Bookings::ReleaseReport.initialize: Second argument must be a Date object.")
      end
      if @start_date > @stop_date
        @start_date, @stop_date = [@stop_date, @start_date]
      end
      @jail = jail
      unless @jail.is_a?(Jail)
        raise ArgumentError.new("Bookings::ReleaseReport.initialize: Third argument must be a valid Jail object.")
      end
      @bookings = bookings
      unless @bookings.is_a?(Array)
        raise ArgumentError.new("Bookings::ReleaseReport.initialize: Fourth argument must be an Array of Booking objects.")
      end
      @bookings.each do |b|
        unless b.is_a?(Booking)
          raise ArgumentError.new("Bookings::ReleaseReport.initialize: Fourth argument must be an Array of Booking objects: supplied Array contains invalid object type(s)!")
        end
      end
    end
    
    def to_pdf
      standard_report(header: false, footer_txt: SiteConfig.agency) do
        text("#{@jail.acronym} Booking Release Report", align: :center, size: 18, style: :bold)
        text("#{@start_date.to_s(:us)}  --  #{@stop_date.to_s(:us)}", align: :center, size: 14, style: :bold)
        move_down(8)
        # Build an array and let prawn page the results
        data = []
        @bookings.each do |book|
          data.push([book.id, AppUtils.show_person(book.person), AppUtils.format_date(book.release_datetime), book.release_officer, book.released_because, book.current_location])
        end
        if data.empty?
          move_down(40)
          text("Nothing To Report!", size: 14, style: :bold, align: :center)
        else
          data.unshift(["Booking", "Inmate", "Released", "By", "Reason", "Current Location"])
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd,SiteConfig.pdf_row_even], width: bounds.width) do
            rows(1..-2).borders = [:left, :right]
            rows(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            columns(0..-1).style(align: :left, size: 11)
            column(0).style(width: 55)
            column(1).style(width: 190)
            column(2).style(width: 125)
            column(3).style(width: 125)
            column(4).style(width: 95)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
        end
        move_down(8)
        text("END OF REPORT", style: :bold)
      end
    end
  end
end