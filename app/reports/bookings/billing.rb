module Bookings
  class Billing < ReportBase
    def initialize(jail_id, start_date, stop_date)
      super(page_layout: :landscape, skip_page_creation: true)
      @jail = Jail.find_by_id(jail_id)
      if @jail.nil?
        raise ArgumentError.new("Bookings::Billing.initialize: First argument must be a valid Jail ID.")
      end
      @start_date = start_date
      unless @start_date.is_a?(Date)
        raise ArgumentError.new("Bookings::Billing.initialize: Second argument must be a Date object.")
      end
      @stop_date = stop_date
      unless @stop_date.is_a?(Date)
        raise ArgumentError.new('Bookings::Billing.initialize: Third argument must be a Date object.')
      end
    end
  
    def to_pdf
      # generate the report data
      report_data = BillingTableData.new(@jail.id,@start_date,@stop_date)
      btypes = ['Temporary Release', 'DOC', 'DOC Billable', 'City', 'Parish', 'Other', 'Not Billable']
      # define spreadsheet columns
      coln = 30
      cols = 12
      colt = 45
      cold = 60
      widths =  [ cold, cols, coln, coln, coln, coln, cols, coln, coln, coln, coln,  coln, cols,  coln,  cols, coln, coln, coln, coln,     colt, cols, coln, coln, coln, coln,     colt]
      headers = ['Date',  ' ',  'D',  'P',  'C',  'O',  ' ', '*D',  'P',  'C',  'O', '!TR',  ' ', '*TT',   ' ',  'D',  'P',  'C',  'O',  'Total',  ' ',  'D',  'P',  'C',  'O',  'Total']
      # positions for headers (and populate blank_line)
      blank_line = []
      positions = []
      widths.size.times do |x|
        blank_line.push(' ')
        if x == 0
          positions.push(0)
        else
          positions.push(positions[x-1] + widths[x-1])
        end
      end
      # header
      repeat(:all) do
        bounding_box([margin_box.left, margin_box.top - 10], width: margin_box.width, height: 50) do
          text("Monthly Billing Report: #{@jail.acronym}", align: :center, size: 18, style: :bold)
          move_up(18)
          text("#{@start_date.strftime("%B")} #{@start_date.strftime("%Y")}#{@stop_date == @stop_date.end_of_month ? '' : ' MTD'}", size: 16)
          move_up(18)
          text("#{@start_date.strftime("%B")} #{@start_date.strftime("%Y")}#{@stop_date == @stop_date.end_of_month ? '' : ' MTD'}", size: 16, align: :right)
          # leave space for the page title stamp
          move_down(5)
        end
      end
      # footer
      repeat(:all) do
        bounding_box([margin_box.left, margin_box.bottom + 20], width: margin_box.width, height: 20) do
          font("Helvetica")
          stroke_horizontal_rule
          move_down(5)
          text("NO: #{AppUtils.format_date(@stop_date, format: :timestamp)}", align: :left, size: 12, style: :bold)
          move_up(14)
          text("#{SiteConfig.header_title}", align: :center, size: 12)
          font("Times-Roman")
        end
      end
      # complain if we don't have data
      if report_data.nil?
        move_down(40)
        text("Unable to retrieve billing data (program error).", size: 14, style: :bold, align: :center)
      else
        # page header stamps
        sections = btypes.clone
        sections.push("Summary by Date")
        sections.each do |section|
          create_stamp(section) do
            canvas do
              # calculate size of text
              if section == "Not Billable"
                section_text = "Not Billable (Temporary Transfers)"
              else
                section_text = section
              end
              section_size = width_of(section_text, size: 16, style: :bold)
              section_position = ((bounds.width / 2) - (section_size / 2)).to_i
              section_y = bounds.top - 65
              draw_text(section_text, at: [section_position, section_y], size: 16, style: :bold)
              if section == "Summary by Date"
                # table group headers
                move_down(85)
                row_pos = cursor
                span(widths[2] + widths[3] + widths[4] + widths[5], position: positions[2]) do
                  text('Booked', style: :bold, align: :center, size: 12)
                end
                move_cursor_to(row_pos)
                span(widths[7] + widths[8] + widths[9] + widths[10] + widths[11], position: positions[7]) do
                  text('Released', style: :bold, align: :center, size: 12)
                end
                move_cursor_to(row_pos)
                span(widths[13], position: positions[13]) do
                  text('Trans', style: :bold, align: :center, size: 12)
                end
                move_cursor_to(row_pos)
                span(widths[15] + widths[16] + widths[17] + widths[18] + widths[19], position: positions[15]) do
                  text('Physical Count', style: :bold, align: :center, size: 12)
                end
                move_cursor_to(row_pos)
                span(widths[21] + widths[22] + widths[23] + widths[24] + widths[25], position: positions[21]) do
                  text('Billing Count', style: :bold, align: :center, size: 12)
                end
              end
            end
          end
        end
        bounding_box([margin_box.left, margin_box.top - 70], width: margin_box.width, height: margin_box.height - 95) do
          btypes.each do |t|
            on_page_create{ stamp(t) }
            start_new_page
            data = report_data.rows(t)
            if data.empty?
              move_down(40)
              text("Nothing To Report", align: :center, size: 14)
            else
              move_up(20)
              data.unshift(['Book#', 'ID', 'Name', 'Booked', 'Released', 'Start', 'Stop', 'Phys', 'Bill'])
              table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]) do
                rows(1..-2).borders = [:left, :right]
                row(-1).borders = [:left, :right, :bottom] 
                rows(1..-1).padding = 3
                row(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
                columns(0..-1).style(size: 12)
                columns(0..1).style(width: 70, align: :right)
                column(2).style(width: 225, align: :left)
                columns(3..6).style(width: 70, align: :left)
                columns(7..8).style(width: 40, align: :right)
                row(0).style(size: 9, font_style: :bold, borders: [:bottom])
              end
            end
          end
  
          on_page_create { stamp("Summary by Date") }
          start_new_page
  
          data = report_data.summary
          data.unshift(headers)
          table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even]) do
            rows(1..-2).borders = [:left, :right]
            row(-1).borders = [:left, :right, :bottom] 
            rows(1..-1).padding = 3
            row(-1).style(font_style: :bold, borders: [:left, :right, :bottom, :top], background_color: 'FFFFFF')
            columns(0..-1).style(size: 12)
            columns([1,6,12,14,20]).style(borders: [:left, :right], background_color: 'FFFFFF')
            widths.size.times do |x|
              column(x).style(align: :right, width: widths[x])
            end
            row(0).style(size: 9, font_style: :bold, borders: [:bottom])
            row(0).columns([1,6,12,14,20]).style(borders: [])
          end
          move_down(5)
          text("* Not Billable: Last Day DOC or Non-billable Temporary Transfer,  ! Temporary Release", size: 10)
        end
      end
      number_pages("Page: <page> of <total>", at: [bounds.right - 65, 15])
      render
    end
  
    class BillingTableData
      def initialize(jail_id, start_date, stop_date=nil)
        jail = Jail.find(jail_id)
        @start_date = start_date
        if stop_date.nil?
          @stop_date = start_date
        else
          @stop_date = stop_date
        end
        # order of btypes is fixed by return value of jail.headcount method
        @btypes = ['Temporary Release','DOC','DOC Billable','City','Parish','Other','Not Billable']
        @items = {}
        @start_date.upto(@stop_date) do |d|
          raw_data = jail.headcount(d,'ALL')
          @btypes.size.times do |x|
            if @items[@btypes[x]].nil?
              @items[@btypes[x]] = []
            end
            raw_data[x].each do |b|
              @items[@btypes[x]].push(BillingItem.new(d,@btypes[x],b))
            end
          end
        end
      end
  
      def count(status=nil,date=nil)
        if date.nil?
          if status.nil?
            @items.values.flatten.compact.size
          elsif @items[status].nil?
            0
          else               
            @items[status].size
          end
        else
          if status.nil?
            @items.values.collect{|v| v.reject{|i| i.date != date}}.flatten.compact.size
          elsif @items[status].nil?
            0
          else
            @items[status].reject{|i| i.date != date}.size
          end
        end
      end
  
      def billable(status=nil,date=nil)
        if date.nil?
          if status.nil?
            @items.values.collect{|v| v.reject{|i| !i.billable?}}.flatten.compact.size
          elsif @items[status].nil?
            0
          else
            @items[status].reject{|i| !i.billable?}.size
          end
        else
          if status.nil?
            @items.values.collect{|v| v.reject{|i| i.date != date || !i.billable?}}.flatten.compact.size
          elsif @items[status].nil?
            return 0
          else
            @items[status].reject{|i| i.date != date || !i.billable?}.compact.size
          end
        end
      end
  
      def physical(status=nil,date=nil)
        if date.nil?
          if status.nil?
            @items.values.collect{|v| v.reject{|i| !i.physical?}}.flatten.compact.size
          elsif @items[status].nil?
            0
          else
            @items[status].reject{|i| !i.physical?}.size
          end
        else
          if status.nil?
            @items.values.collect{|v| v.reject{|i| i.date != date || !i.physical?}}.flatten.compact.size
          elsif @items[status].nil?
            return 0
          else
            @items[status].reject{|i| i.date != date || !i.physical?}.compact.size
          end
        end
      end
  
      def booked(status=nil,date=nil)
        if date.nil?
          if status.nil?
            @items.values.collect{|v| v.reject{|i| !i.booked?}}.flatten.compact.size
          elsif @items[status].nil?
            0
          else
            @items[status].reject{|i| !i.booked?}.size
          end
        else
          if status.nil?
            @items.values.collect{|v| v.reject{|i| i.date != date || !i.booked?}}.flatten.compact.size
          elsif @items[status].nil?
            return 0
          else
            @items[status].reject{|i| i.date != date || !i.booked?}.compact.size
          end
        end
      end
  
      def released(status=nil,date=nil)
        if date.nil?
          if status.nil?
            @items.values.collect{|v| v.reject{|i| !i.released?}}.flatten.compact.size
          elsif @items[status].nil?
            0
          else
            @items[status].reject{|i| !i.released?}.size
          end
        else
          if status.nil?
            @items.values.collect{|v| v.reject{|i| i.date != date || !i.released?}}.flatten.compact.size
          elsif @items[status].nil?
            return 0
          else
            @items[status].reject{|i| i.date != date || !i.released?}.compact.size
          end
        end
      end
  
      def transferred(status=nil,date=nil)
        if date.nil?
          if status.nil?
            @items.values.collect{|v| v.reject{|i| !i.transferred?}}.flatten.compact.size
          elsif @items[status].nil?
            0
          else
            @items[status].reject{|i| !i.transferred?}.size
          end
        else
          if status.nil?
            @items.values.collect{|v| v.reject{|i| i.date != date || !i.transferred?}}.flatten.compact.size
          elsif @items[status].nil?
            return 0
          else
            @items[status].reject{|i| i.date != date || !i.transferred?}.compact.size
          end
        end
      end
  
      def items(status)
        @items[status].sort{|a,b| a.name + a.id.to_s.rjust(10,'0') + a.date.to_s(:db) <=> b.name + b.id.to_s.rjust(10,'0') + a.date.to_s(:db)}
      end
  
      def rows(status)
        return [] unless status.is_a?(String)
        return [] if @items[status].nil? || @items[status].empty?
        item_rows = []
        last_booking = nil
        start_date = nil
        billing_count = 0
        billing_total = 0
        physical_count = 0
        physical_total = 0
        lines = @items[status].sort{|a,b| a.name + a.id.to_s.rjust(10,'0') + a.date.to_s(:db) <=> b.name + b.id.to_s.rjust(10,'0') + b.date.to_s(:db)}
        lines.size.times do |x|
          if last_booking.nil?
            last_booking = lines[x].id
            start_date = lines[x].date
          end
          if last_booking != lines[x].id
            # new booking id, write the previous record
            item_rows.push([lines[x-1].id.to_s, lines[x-1].person_id.to_s, lines[x-1].name, Booking.AppUtils.format_date(lines[x-1].booked), Booking.AppUtils.format_date(lines[x-1].released), Booking.AppUtils.format_date(start_date), Booking.AppUtils.format_date(lines[x-1].date), physical_count.to_s, billing_count.to_s])
            if lines[x].billable?
              billing_count = 1
              billing_total += 1
            else
              billing_count = 0
            end
            if lines[x].physical?
              physical_count = 1
              physical_total += 1
            else
              physical_count = 0
            end
            last_booking = lines[x].id
            start_date = lines[x].date
          else
            if lines[x].billable?
              billing_count += 1
              billing_total += 1
            end
            if lines[x].physical?
              physical_count += 1
              physical_total += 1
            end
          end
        end
        # push the last line
        item_rows.push([lines.last.id.to_s, lines.last.person_id.to_s, lines.last.name, Booking.AppUtils.format_date(lines.last.booked), Booking.AppUtils.format_date(lines.last.released), Booking.AppUtils.format_date(start_date), Booking.AppUtils.format_date(lines.last.date), physical_count.to_s, billing_count.to_s])
        # add a blank line
        item_rows.push([' ',' ',' ',' ',' ',' ','Totals:',physical_total.to_s,billing_total.to_s])
        return item_rows
      end
  
      def summary
        day_rows = []
        @start_date.upto(@stop_date) do |d|
          temp = [
            Booking.AppUtils.format_date(d),
            ' ',
            booked('DOC Billable',d).to_s,
            (booked('DOC',d) + booked('Parish',d)).to_s,
            booked('City',d).to_s,
            booked('Other',d).to_s,
            ' ',
            released('DOC Billable',d).to_s,
            (released('DOC',d) + released('Parish',d)).to_s,
            released('City',d).to_s,
            released('Other',d).to_s,
            count('Temporary Release',d).to_s,
            ' ',
            count('Not Billable',d).to_s,
            ' ',
            physical('DOC Billable',d).to_s,
            (physical('DOC',d) + physical('Parish',d)).to_s,
            physical('City',d).to_s,
            physical('Other',d).to_s,
            physical(nil,d).to_s,
            ' ',
            billable('DOC Billable',d).to_s,
            (billable('DOC',d) + billable('Parish',d)).to_s,
            billable('City',d).to_s,
            billable('Other',d).to_s,
            billable(nil,d).to_s
          ]
          day_rows.push(temp)
        end
        blank_line = []
        26.times do
          blank_line.push(' ')
        end
        day_rows.push(blank_line)
        temp = [
          'Totals:',
          ' ',
          booked('DOC Billable').to_s,
          (booked('DOC') + booked('Parish')).to_s,
          booked('City').to_s,
          booked('Other').to_s,
          ' ',
          released('DOC Billable').to_s,
          (released('DOC') + released('Parish')).to_s,
          released('City').to_s,
          released('Other').to_s,
          count('Temporary Release').to_s,
          ' ',
          count('Not Billable').to_s,
          ' ',
          physical('DOC Billable').to_s,
          (physical('DOC') + physical('Parish')).to_s,
          physical('City').to_s,
          physical('Other').to_s,
          physical.to_s,
          ' ',
          billable('DOC Billable').to_s,
          (billable('DOC') + billable('Parish')).to_s,
          billable('City').to_s,
          billable('Other').to_s,
          billable.to_s
        ]
        day_rows.push(temp)
      end
    end
  
    class BillingItem
      def initialize(date, status, booking)
        @date = date
        @status = status
        @id = booking.id
        @person_id = booking.person.id
        @name = booking.person.sort_name
        @booked = booking.booking_datetime? ? booking.booking_datetime.to_date : booking.booking_datetime
        @released = booking.release_datetime? ? booking.release_datetime.to_date : booking.release_datetime
        @agency = booking.is_transferred(Booking.AppUtils.get_datetime(@date,"23:59"))
      end
  
      def date
        @date
      end
  
      def status
        @status
      end
  
      def id
        @id
      end
  
      def person_id
        @person_id
      end
  
      def name
        @name
      end
  
      def booked
        @booked
      end
  
      def released
        @released
      end
  
      def agency
        @agency
      end
  
      def transferred?
        !@agency.blank?
      end
  
      def billable?
        return false if @status == 'Temporary Release' || @status == 'Not Billable'
        return false if @status == 'DOC Billable' && released?
        true
      end
  
      def physical?
        return false if @status == 'Temporary Release' || @status == 'Not Billable'
        return false if released?
        return false if transferred?
        true
      end
  
      def booked?
        @booked == @date
      end
  
      def released?
        return false if @released.nil?
        @released == @date
      end
    end
  end
end