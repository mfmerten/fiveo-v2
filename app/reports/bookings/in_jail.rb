module Bookings
  class InJail < ReportBase
    def initialize(jail_id, bookings)
      super(page_size: 'LEGAL', page_layout: :landscape)
      @jail = Jail.find(jail_id)
      @bookings = bookings
      unless @bookings.is_a?(Array)
        raise ArgumentError.new("Bookings::InJail.initialize: Second argument must be an Array of Booking objects.")
      end
      @bookings.each do |b|
        unless b.is_a?(Booking)
          raise ArgumentError.new("Bookings::InJail.initialize: Second argument must be an Array of Booking objects: supplied Array contains invalid object type(s)!")
        end
      end
    end
    
    def to_pdf
      # note: special page orientation, layout and header/footer
      repeat(:all) do
        bounding_box([margin_box.left, margin_box.top - 10], width: margin_box.width, height: 30) do
          font("Helvetica")
          text("Monthly In-Jail Report", align: :center, size: 18, style: :bold)
          move_up(16)
          text(Date.today.to_s(:us), align: :right, size: 14, style: :bold)
          move_up(16)
          text(@jail.name, size: 14, style: :bold)
          stroke_horizontal_rule
          font("Times-Roman")
        end
        bounding_box([margin_box.left, margin_box.bottom + 20], width: margin_box.width, height: 20) do
          font("Helvetica")
          stroke_horizontal_rule
          move_down(5)
          text("No: #{Date.today.to_s(:timestamp)}", align: :left, size: 9)
          move_up(10)
          text(SiteConfig.agency, align: :center, size: 12)
          font("Times-Roman")
        end
      end
      bounding_box([margin_box.left, margin_box.top - 45], width: margin_box.width, height: margin_box.height - 75) do
        data = []
        # populate the table
        @bookings.each do |b|
          # skip this unless there are pending charges (or probation holds)
          valid_holds = ['Probation (Parish)', 'Probation/Parole (State)', '48 Hour', '72 Hour', 'Bondable', 'No Bond', 'Hold for City', 'Hold for Other Agency']
          next if b.holds.active.reject{|h| !valid_holds.include?(h.hold_type)}.empty?
          # collect a list of all active holds
          holds = b.holds.active
          # collect a unique list of arrests linked by all active holds
          arrests = holds.collect{|h| h.arrest}.compact.uniq
          # gather list of holds without arrest
          non_arrest_holds = holds.reject{|h| !h.arrest.nil?}
          # Type (0)
          classification = b.billing_status
          if classification == "DOC" || classification == "DOC Billable"
            b_class = "D"
          elsif classification == "City"
            b_class = "C"
          elsif classification == "Parish"
            b_class = "P"
          elsif classification == ""
            b_class = "P"
          else
            b_class = "O"
          end
          # Docket / Court (1)
          # leave blank
          # Name (2)
          b_name = AppUtils.show_person(b.person)
          h_text = ""
          # first, list each active arrest and the holds for them
          unless arrests.empty?
            arrests.each do |a|
              unless h_text.blank?
                h_text << "\n"
              end
              h_text << "<b>Arrested:</b> #{AppUtils.format_date(a.arrest_datetime)} by #{a.agency? ? a.agency : 'Unknown Agency'} on the charges of:\n"
              # list the charges
              a.district_charges.each do |c|
                h_text << "--- #{c.charge}(#{c.count})#{c.warrant.nil? ? '' : ' [Warrant: ' + c.warrant.warrant_no + ']'}\n"
              end
              a.city_charges.each do |c|
                case
                when !c.warrant.nil?
                  h_text << "--- City Warrant: #{c.warrant.jurisdiction} Warrant #{c.warrant.warrant_no}: "
                when !c.agency.nil?
                  h_text << "--- City Charge: #{c.agency}: "
                end
                h_text << "#{c.charge}(#{c.count})\n"
              end
              a.other_charges.each do |c|
                if c.warrant.nil?
                  h_text << "--- Unknown Warrant Information: "
                else
                  h_text << "--- Other Warrant: #{c.warrant.jurisdiction} Warrant #{c.warrant.warrant_no}: "
                end
                h_text << "#{c.charge}(#{c.count})\n"
              end
              # set up bond string
              if a.hour_72_datetime?
                if a.bondable?
                  bond_string = "Bond Set at #{currency(a.bond_amt)}"
                else
                  bond_string = "Bond Denied"
                end
                bond_string << " on #{AppUtils.format_date(a.hour_72_datetime)} by #{a.hour_72_judge.blank? ? 'Unknown' : 'Judge ' + a.hour_72_judge}"
              else
                bond_string = "Pending Bond Hearing"
              end
              h_text << "<b>Bond Status:</b> #{bond_string}\n"
              h_text << "<b>Current holds for this arrest:</b> #{a.holds.active.join(', ')}\n"
            end
          end
          # next, list each non-arrest hold
          unless non_arrest_holds.empty?
            h_text << "<b>Other Holds:</b>\n"
            non_arrest_holds.each do |h|
              if h.hold_type.blank?
                h_text << "--- Invalid Hold Type!\n"
              elsif h.hold_type == "Serving Sentence"
                h_text << "--- Serving Sentence#{h.doc? ? ' (DOC)' : ''} for #{h.hold_reason}: #{(h.hours_remaining / 24.0).to_i} Days Remaining.\n"
              elsif h.hold_type == "Fines/Costs"
                h_text << "--- #{h.hold_reason_name}#{h.hold_reason}\n"
              elsif h.hold_type == "Probation (Parish)"
                h_text << "--- Hold for Parish Probation\n"
              elsif h.hold_type == "Probation/Parole (State)"
                h_text << "--- Hold for State Probation/Parole\n"
              elsif h.hold_type == "Hold for City"
                h_text << "--- Hold for City (#{h.agency? ? h.agency : 'Unknown City'}): manual hold, unknown reason.\n"
              elsif h.hold_type == "Hold for Other Agency"
                h_text << "--- Hold for Other Agency: #{h.agency? ? h.agency : 'Unknown Agency'}: manual hold, unknown reason.\n"
              elsif h.hold_type == "Temporary Release"
                h_text << "--- Temporarily Released because: #{h.temp_release_reason.blank? ? 'Unknown' : h.temp_release_reason}.\n"
              elsif h.hold_type == "Temporary Housing"
                h_text << "--- Being housed temporarily for #{h.agency? ? h.agency : 'Unknown Agency'}\n"
              elsif h.hold_type == "Temporary Transfer"
                h_text << "--- Temporarily transferred to #{h.agency? ? h.agency : 'Unknown Agency'}\n"
              else
                h_text << "--- Manual hold: #{h.hold_type} (unknown reason)\n"
              end
            end
          end
          # finally, add the booking to the table data array
          unless h_text.blank?
            h_text << "\n\n\n\n" # to give a minimum of 4 blank lines per listing
            data.push([b_class," ",b_name,h_text])
          end
        end
        # print the table
        if data.empty?
          text("Nothing To Report!", style: :bold, align: :center)
        else
          data.unshift(["T", "Docket/Court","Person","Arrest Information"])
          table(data, header: true, position: :center) do
            rows(1..-1).borders = [:left, :right, :top, :bottom]
            rows(1..-1).padding = 3
            columns(0..-1).style(align: :left, size: 11)
            column(0).style(width: 20)
            column(1).style(width: 125)
            column(2).style(width: 175)
            column(3).style(width: 605, inline_format: true)
            rows(0).style(size: 9, font_style: :bold, borders: [:bottom])
          end
        end
        move_down(5)
        text("END OF REPORT", style: :bold)
      end
      number_pages("Page: <page> of <total>", at: [bounds.right - 65, 15])
      render
    end
  end
end