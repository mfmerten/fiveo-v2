module Dockets
  class List < ReportBase
    def initialize(court_date, docket_type, judge, courts)
      super(page_size: 'LEGAL', page_layout: :landscape)
      @court_date = court_date
      unless @court_date.is_a?(Date)
        raise ArgumentError.new("Dockets::List.initialize: First argument must be a Date object.")
      end
      @dt = docket_type
      unless @dt.is_a?(String)
        raise ArgumentError.new("Dockets::List.initialize: Second argument must be a valid String object.")
      end
      @j = judge
      unless @j.is_a?(String)
        raise ArgumentError.new("Dockets::List.initialize: Third argument must be a valid String object.")
      end
      @courts = courts
      unless @courts.is_a?(Array)
        raise ArgumentError.new("Dockets::List.initialize: Fourth argument must be an Array of Court objects.")
      end
      @courts.each do |c|
        unless c.is_a?(Court)
          raise ArgumentError.new("Dockets::List.initialize: Fourth argument must be an Array of Court objects: supplied Array contains invalid object type(s).")
        end
      end
    end
  
    def to_pdf
      # custom header/footer
      repeat(:all) do
        bounding_box([margin_box.left, margin_box.top - 10], width: margin_box.width, height: 22) do
          font("Helvetica")
          text("#{@dt} Docket", align: :center, size: 18, style: :bold)
          move_up(20)
          text("#{@j}", size: 16, style: :bold)
          move_up(18)
          text(AppUtils.format_date(@court_date), align: :right, size: 16, style: :bold)
        end
        bounding_box([margin_box.left, margin_box.bottom + 20], width: margin_box.width, height: 20) do
          font("Helvetica")
          stroke_horizontal_rule
          move_down(5)
          text("No: #{Date.today.to_s(:timestamp)}", align: :left, size: 9)
          move_up(10)
          text("#{SiteConfig.agency}", align: :center, size: 12)
          font("Times-Roman")
        end
      end
  
      bounding_box([margin_box.left, margin_box.top - 27], width: margin_box.width, height: margin_box.height - 66) do
        data = []
        record = 1
        nl = "\n"
        sep = "\n\n\n\n\n"
        @courts.each do |c|
          blist = c.docket.bonds.collect{|b| "##{b.bond_no} [#{b.bond_type.blank? ? '?' : b.bond_type}] #{currency(b.bond_amt)} <#{b.status_name}>"}
          adates = c.docket.arrests.collect{|a| "#{AppUtils.format_date(a.arrest_datetime)}"}.join(", ")
          data.push(["##{record}","#{AppUtils.show_person(c.docket.person, false).upcase}#{nl}[#{c.docket.person.id}]","#{c.docket.docket_no}","#{adates.blank? ? '' : 'Arrest Date(s): ' + adates + nl}#{c.docket.charge_summary(true)}#{sep}","#{blist.join(' -- ')}#{sep}",c.court_type])
          record += 1
        end
        data.unshift(['#','Person','Docket','Charge(s)','Bonds','Court'])
        table(data, header: true, row_colors: [SiteConfig.pdf_row_odd, SiteConfig.pdf_row_even], width: bounds.width) do
          columns(0..-1).style(size: 12, borders: [:left, :right, :top, :bottom], align: :left)
          column(0).style(width: 30)
          column(1).style(width: 200)
          column(2).style(width: 55)
          column(4).style(width: 180)
          column(5).style(width: 100)
          row(0).style(borders: [:bottom], size: 9, font_style: :bold)
        end
      end
      number_pages("Page: <page> of <total>", at: [bounds.right - 65, 15])
      render
    end
  end
end