module Dockets
  class Letters < ReportBase
    def initialize(court_date, docket_type, time, bonds)
      super
      @court_date = court_date
      unless @court_date.is_a?(Date)
        raise ArgumentError.new("Dockets::Letters.initialize: First argument must be a Date object.")
      end
      @dt = docket_type
      unless @dt.is_a?(String)
        raise ArgumentError.new("Dockets::Letters.initialize: Second argument must be a valid String object.")
      end
      @time = time
      unless @time =~ /^\d{2}:\d{2} ?(AM|PM)$/
        raise ArgumentError.new("Dockets::Letters.initialize: Third argument must be a string representing a valid 12-Hour time value (such as '12:00 PM' or '08:02 AM').")
      end
      @bonds = bonds
      unless @bonds.is_a?(Array)
        raise ArgumentError.new("Dockets::Letters.initialize: Fourth argument must be an Array of Bond objects.")
      end
      @bonds.each do |b|
        unless b.is_a?(Bond)
          raise ArgumentError.new("Dockets::Letters.initialize: Fourth argument must be an Array of Bond objects: Supplied array contains invalid object type(s).")
        end
      end
    end
  
    def to_pdf
      standard_report do
        @bonds.each do |b|
          bond = b[0]
          court = b[1]
          move_down(80)
          if !bond.bondsman.nil?
            text(bond.bondsman.display_name, size: 12)
            if bond.bonding_company.nil?
              company = bond.bondsman.notes
            else
              company = bond.bonding_company.display_name
            end
            text(company, size: 12)
            text(bond.bondsman.address_line1, size: 12)
            unless bond.bondsman.address_line2.blank?
              text(bond.bondsman.address_line2, size: 12)
            end
            text(bond.bondsman.address_line3, size: 12)
          elsif !bond.surety.nil?
            text(bond.surety.display_name, size: 12)
            text(bond.surety.mailing_line1, size: 12)
            text(bond.surety.mailing_line2, size: 12)
          else
            text("NO Bondsman OR Surety On File!", size: 16, style: :bold)
          end
          move_down(30)
          text("Reference: <b>#{AppUtils.show_person(bond.person, false).upcase}</b>", inline_format: true, size: 14)
          move_down(25)
          y_position = cursor
          text("Dear Sir/Madam:", size: 14)
          move_cursor_to(y_position)
          text("Court Date:   <b>#{AppUtils.format_date(@court_date)}</b>", inline_format: true, size: 14, align: :right)
          move_down(10)
          text("I have been instructed by the #{SiteConfig.prosecutor_title} to notify you, as Bondsman, to have above subject in court on above date at <b>#{@time}</b> for <b>#{court.nil? || court.court_type.blank? ? @dt.upcase : court.court_type.upcase}</b> on Charge(s) of: #{bond.arrest.district_charges.join(', ')}.", inline_format: true, size: 14, indent_paragraphs: 20)
          move_down(10)
          text("Failure of the defendent to appear in <b>#{SiteConfig.court_title} Court</b> in the <b>#{SiteConfig.jurisdiction_title} Courthouse</b> at the hour and on the date specified may result in an order to forfeit the bond posted in this case.", inline_format: true, size: 14, indent_paragraphs: 20)
          move_down(15)
          indent(300) do
            text("Yours very truly,", size: 14)
            move_down(15)
            text("#{SiteConfig.ceo}", size: 14)
            move_down(10)
            text("#{SiteConfig.ceo_title} of #{SiteConfig.jurisdiction_title}", size: 14)
          end
          text("By: #{SiteConfig.court_clerk_name}", size: 14)
          move_down(5)
          text("#{SiteConfig.court_clerk_title}", size: 14)
          unless b == @bonds.last
            start_new_page
          end
        end
      end
    end
  end
end