module TipsHelper
  # tips, tooltips and some pre-defined messages
  
  # called to display text with a tooltip
  # note that supplied string is treated as html safe, so make sure it is
  # when using this method!
  # OpenTip to produce the tooltips
  def with_tooltip(string, tooltip, *args)
    options = args.extract_options!
    tt_class = 'tooltip'
    if options[:class]
      tt_class << " #{options[:class]}"
    end
    return "" if string.blank? || !string.respond_to?('to_s')
    txt = string
    return txt if tooltip.blank? || !tooltip.respond_to?('to_s')
    # AppUtils.compress_spaces is necessary for tooltips defined multi-line-style
    tt = AppUtils.compress_spaces(tooltip.to_s)
    content_tag(:span, style: 'display:inline;margin:0;padding:0;', class: tt_class, 'data-ot' => tt){txt.html_safe}
  end
  
  def charge_tool_tip
    "To correctly enter a charge, enter the complete numeric portion of the R.S. code (i.e. '14:102') and press the Tab key. If you choose to enter the charge as a phrase, you will have trouble choosing the correct phrase to match the desired Statute."
  end
  
  # generate a highlighted message (tip)
  def tip(text, *opts)
    options = HashWithIndifferentAccess.new(force: false, tip_type: 'info')
    options.update(opts.extract_options!)
    # type defaults to info
    unless ['info','warning'].include?(options[:tip_type])
      options[:tip_type] = 'info'
    end
    text.is_a?(String) or return nil
    if options[:tip_type] == 'info'
      tip_image = icon("info", height: 14)
      tip_txt = ""
    else
      tip_image = icon('caution')
      tip_txt = "<b>Caution:</b> "
    end
    if session[:user_show_tips] == true || options[:force] == true
      td1 = content_tag(:td, style: 'vertical-align:top;'){tip_image}
      td2 = content_tag(:td, style: 'width:100%;'){"#{tip_txt}#{AppUtils.compress_spaces(text)}".html_safe}
      trow = content_tag(:tr){td1 + td2}
      tabl = content_tag(:table){trow}
      content_tag(:div, class: 'tip'){tabl}
    else
      nil
    end
  end

  def remark_note
    AppUtils.compress_spaces("These remarks will not appear on any printed documentation. They
      are for internal #{SiteConfig.agency_acronym} use only and are not to be
      considered part of official record.")
  end

  def search_help
    AppUtils.compress_spaces("Information entered within the past 30 minutes may not appear
      in these results. If you need absolutely current information, use the
      filter instead. Records modified most recently are shown first. There is
      currently no way to change the sort order.")
  end

  # A tip providing expandable help for textile enabled field
  # formatting rules.
  def textile_help
    more_link = link_to_function("Click here", nil, id: "more_textile_link"){|page| page.show 'textile_helper'}
    less_link = link_to_function("Hide Formatting Rules", nil, id: 'less_textile_link'){|page| page.hide 'textile_helper'}
    txt = <<-EOT
    The text areas with the pale green background have special formatting rules. #{more_link} for more information.
    <div id='textile_helper' style='display: none;'>
      <h4>Paragraphs</h4>
      <p>
        When entering text, don't hit [Enter] until you are ready to end a paragraph.
        As you type, the line will automatically wrap when it reaches the end of the
        input box. This way, when the display area size changes, the lines will grow 
        or shrink properly to fit. When you are finished with a paragraph, hit [Enter]
        twice (to insert a blank line).
      </p>
      
      <h4>Bold and Italic Text</h4>
      <p>For bold text, enclose the text in asterisks (*) like this:</p>
      <blockquote>
        <p>this is normal, but *this is bold*</p>
      </blockquote>
      <p>to produce:</p>
      <blockquote>
        <p>this is normal, but <b>this is bold</b></p>
      </blockquote>
      <p>Italic text is done the same way, except you use underscores (_) instead, like:</p>
      <blockquote>
        <p>this is normal, but _this is italic_</p>
      </blockquote>
      <p>to produce:</p>
      <blockquote>
        <p>this is normal, but <em>this is italic</em></p>
      </blockquote>
      
      <h4>Lists</h4>
      <p>For bulleted lists, type:</p>
      <blockquote>
        <p>
          * line item 1<br/>
          * line item 2<br/>
          * line item 3
        </p>
      </blockquote>
      <p>and you will see:</p>
      <blockquote>
        <ul>
          <li>line item 1</li>
          <li>line item 2</li>
          <li>line item 3</li>
        </ul>
      </blockquote>
      <p>For a numbered list, use # instead of *, like:</p>
      <blockquote>
        <p>
          # line item 1<br/>
          # line item 2<br/>
          # line item 3
        </p>
      </blockquote>
      <p>which will render as:</p>
      <blockquote>
        <ol>
          <li>line item 1</li>
          <li>line item 2</li>
          <li>line item 3</li>
        </ol>
      </blockquote>
      
      <h4>Links</h4>
      <p>
        To insert a link, put link name in double-quotes, then a colon (:) followed by
        the link URL (or address), like:
      </p>
      <blockquote>
        <p>&quot;Midlake Technical Services Website&quot;:http://midlaketech.com</p>
      </blockquote>
      <p>and it will display as:</p>
      <blockquote>
        <p><a href='http://midlaketech.com'>Midlake Technical Services Website</a></p>
      </blockquote>
      
      <h4>More Information</h4>
      <p>
        For a complete list of formatting rules, check out the
        <a href='http://www.textism.com/tools/textile'>Textile Website</a>.
      </p>
      <p>#{less_link}</p>
    </div>
    EOT
    return txt
  end

  # A tip that provides expandable help for formatting names so
  # that the software can properly parse them.
  def name_help
    more_link = link_to_function("Click here", nil, id: "more_name_link"){|page| page.show 'name_helper'}
    less_link = link_to_function("Hide Name Rules", nil, id: 'less_name_link'){|page| page.hide 'name_helper'}
    txt = <<-EOT
    Names must be entered according to certain rules. #{more_link} for more information.
    <div id='name_helper' style='display: none;'>
      <h4>Entering Names</h4>
      <p>
        When entering names, the program expects that they will be in one of two different
        formats:
      </p>
      <p>1. First name first:  First Middle Last Suffix</p>
      <p>2. Last name first:   Last, First Middle Suffix (the comma is required!)</p>
      <p>
        In both of these formats, the Middle name and Suffix are optional. If the last name
        is two words (von Neuman), the name *must* be entered using method 2 (last name first).
        There may be as many middle names as desired.
      </p>
      <p>For the suffix, only the following variations will be recognized:</p>
      <ul>
        <li>Sr</li>
        <li>Jr or II</li>
        <li>III</li>
        <li>IV</li>
        <li>V</li>
      </ul>
      <p>
        Suffixes should be in upper case, may have a period (.) at the end and may
        be separated from the name with a comma.  The only comma that is required is the
        one following the last name when the name is in last-name-first format.
      </p>
      <p>#{less_link}</p>
    </div>
    EOT
    return txt
  end

  # A tip that provides photograph upload help.
  def photo_help
    AppUtils.compress_spaces(%(Photos should be in JPG format. Other formats (GIF, TIFF, etc) may or
      may not work properly depending on the external image processing software installed
      on your server.)).html_safe
  end

  # A tip that provides case number help.
  def case_help
    %(Leave Case Number blank to assign the next available case.).html_safe
  end

  # A tip telling the user how to identifiy form fields that are
  # required entry.
  def std_help
    tt = "Extra information will pop up in a box like this when you hover over the underlined text."
    AppUtils.compress_spaces(%(Fields with #{required} in front of the label are
      required. Words with dotted underline #{with_tooltip('like this',tt)}
      will show more information if you point at them.)).html_safe
  end
  
  # a tip telling the user about using emoji icons in their text
  def emoji_help
    AppUtils.compress_spaces(%(You can include small pictures (called emoji icons) in your text.
      There are hundreds available to choose from. You can find out more by
      checking out the <a href='http://www.emoji-cheat-sheet.com/' target='_blank'>Emoji Cheatsheet</a>.)).html_safe
  end

  # a tip reminding the user to make sure the officer select is
  # set to nil if they want to manually enter a name, badge and unit.
  def officer_help
    AppUtils.compress_spaces(%(To manually enter an officer name, make sure the officer select
      is set to nil (---) or your entry will be overwritten when you save.)).html_safe
  end
end
