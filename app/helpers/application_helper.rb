module ApplicationHelper

  include CalendarHelper
  
  # returns FiveO version as a string
  def version_no
    "2.0.58"
  end
  
  module_function :version_no
  
  # Returns the current version of the software formatted for
  # application layout
  def software_version
    "Version: ".html_safe + content_tag(:span){version_no}
  end
  
  # Sets our form builder as the default
  ActionView::Base.default_form_builder = CustomFormBuilder

  # Returns a string containing the specified number of HTML non-breaking
  # space characters. -- space(2) returns "&nbsp;&nbsp;"
  def space(count)
    if count.nil? || count.to_i <= 0
      txt = "&nbsp;"
    else
      txt = "&nbsp;" * count.to_i
    end
    return txt.html_safe
  end
  
  # textilize helper
  def texthtml(string)
    if string.is_a?(String)
      textilize(string).html_safe
    else
      "".html_safe
    end
  end
  
  # standard space between fields
  def field_spacer
    space(3)
  end
  
  # arrow spacer
  def arrow_spacer
    space(2) + icon('forward', height: 16) + space(2)
  end
  
  # produces a red asterisk to indicate required fields
  def required
    content_tag(:span, class: 'red bold', style: 'margin:0;padding:0'){'*'}
  end
  
  # helper for displaying errors on a form
  def display_errors(object)
    return "" unless object.respond_to?('errors')
    return "" unless object.errors.any?
    err_messages = "".html_safe
    object.errors.full_messages.each do |msg|
      err_messages << content_tag(:li){msg.html_safe}
    end
    content_tag(:div, id: 'error_explanation'){
      content_tag(:h2){
        pluralize(object.errors.count, "error") + " prohibited this form from being saved:"
      } + content_tag(:ul){err_messages}
    }
  end
  
  # produces standard application pagination navigation
  def app_paginate(collection, *args)
    options = HashWithIndifferentAccess.new(ajax: false, wrapper: true)
    options.update(args.extract_options!)
    if options[:ajax]
      if options[:wrapper]
        content_tag(:div, id: 'list-nav'){paginate collection, remote: true}
      else
        paginate collection, remote: true
      end
    else
      if options[:wrapper]
        content_tag(:div, class: 'pages'){paginate collection}
      else
        paginate collection
      end
    end
  end

  # this is used to generate the paginated index table using kaminari
  def do_paged_index(*opts)
    options = HashWithIndifferentAccess.new(filter: true)
    options.update(opts.extract_options!)
    if options[:partial]
      ivar = instance_variable_get("@#{options[:partial].to_s.pluralize}")
      partial_name = options[:partial].to_s
    else
      ivar = instance_variable_get("@#{controller.controller_name}")
      partial_name = controller.controller_name.singularize
    end
    txt = ''.html_safe
    if options[:list_title]
      @list_title = options[:list_title]
    else
      @list_title = controller.controller_name.titleize + ' List'
    end
    if session.has_key?("show_all_#{controller.controller_name}") && session["show_all_#{controller.controller_name}".to_sym] == false
      @list_title << " (Active)"
    end
    unless @query.nil? || @query.empty?
      @list_title << " (Filtered)"
    end
    @page_title ||= controller.controller_name.titleize
    unless @jail.nil?
      @list_title = "#{@jail.acronym} #{@list_title}"
      @page_title = "#{@jail.acronym} #{@page_title}"
    end
    txt << app_paginate(ivar, ajax: true)
    txt << begin_list(@list_title)
    txt << content_tag(:div, id: 'list-body'){render(partial: partial_name, collection: ivar)}
    txt << end_list
    txt << link_bar
    txt
  end
  
  # renders textile, then strips line endings and squishes the result
  # returning the text with tags stripped.  This is to convert text areas
  # to single line strings (for partials)
  #
  # if this is used with the only span in a div on a list partial, give the
  # span a class of 'one-line' to make it fixed width with automatic ellipsis (...)
  # if it overflows.
  def one_line(text)
    return "" unless text.nil? || text.is_a?(String)
    strip_tags(String.new(texthtml(text)).squish)
  end
  
  # provides the beginning tags for a list
  # if no text is specified, just returns blank
  def begin_list(*args)
    options = args.extract_options!
    text = args.shift
    txt = "".html_safe
    reset_cycle
    if options[:class]
      div_class = "list-heading #{options[:class]}-with-bg"
    else
      div_class = 'list-heading'
    end
    unless text.blank?
      txt << content_tag(:div, class: div_class){text}
    end
    txt
  end
  
  # provides the closing line for a list
  def end_list
    content_tag(:div, class: 'list-footing'){}
  end
  
  # does a generic list IF values array contains anything
  def do_list(heading, partial, values, *args)
    options = args.extract_options!
    if options[:allow_empty]
      values = [] if values.nil?
    else
      return "" if values.nil? || values.empty?
    end
    # weed out private records not owned by current user
    if values[0].respond_to?('private') && values[0].respond_to?('owned_by')
      safe_values = values.reject{|v| v.private? && v.owned_by != session[:user]}
    else
      safe_values = values
    end
    if partial.is_a?(Symbol)
      partial = partial.to_s
    end
    txt = "".html_safe
    txt << begin_list(heading, class: options[:theme])
    txt << render(partial: partial, collection: safe_values, locals: options[:locals])
    txt << end_list
    txt
  end
  
  # Displays an icon (from images/icons directory) in the specified height
  # (default 18 pixels, width is variable). This is used to standardize icon usage.
  def icon(name, *opts)
    options = HashWithIndifferentAccess.new(alt: nil, size: nil, width: nil, height: nil, style: 'vertical-align:middle;')
    options.update(opts.extract_options!)
    return nil unless name.is_a?(String)
    # if size not specified, default to 20px
    if options[:size].nil? && options[:height].nil? && options[:width].nil?
      options[:height] = 18
    end
    # let size override height and width
    unless options[:size].nil?
      options[:height] = nil;
      options[:width] = nil;
    end
    if options[:alt].nil?
      options[:alt] = name
    end
    image_tag("icons/" + name + ".png", options.symbolize_keys)
  end
  
  # conditionally displays a notice (usually at top of page). defaults to
  # red text but color can be specified
  def special_notice_if(condition, title, text="", *args, &block)
    options = args.extract_options!
    if condition
      color = options[:color] || 'red'
      content_tag(:div, class: "notice"){
        content_tag(:div, class: "title #{color}-with-bg"){title} +
        content_tag(:div, class: "text #{color}"){block_given? ? yield : text}
      }
    else
      ""
    end
  end
  
  # Builds a button bar from the contents of the @links array. @links should
  # be an array of 2-4 element arrays with name & url entries - no
  # permissions are checked, do that before populating @links. All we do here
  # is string the links together wrapped in <div>s.
  #
  # automatically displays created_by line and filter form (these may not actually
  # produce anything, it depends on conditions.  see each individual method for details.)
  def link_bar(*args)
    opts = args.extract_options!
    return "" if @links.nil?
    return "" unless @links.is_a?(Array)
    link_list = []
    link_list_pdf = []
    @links.each do |l|
      next unless l.is_a?(Array)
      options = HashWithIndifferentAccess.new(method: :get, disabled: false, red: false, pdf: false)
      options.update(l.extract_options!)
      linkopts = options.dup
      linkopts.delete(:pdf)
      linkopts.delete(:red)
      linkopts.delete(:method)
      if options[:method] == :get
        if options[:red]
          linkopts[:class] = 'red'
        end
        button = button_to_function(l[0], "window.location.href='#{l[1]}'", linkopts)
        if options[:pdf]
          link_list_pdf.push(button)
        else
          link_list.push(button)
        end
      else
        html_options = options.dup
        html_options.delete(:red)
        html_options.delete(:pdf)
        if options[:red]
          html_options[:class] = 'red'
        end
        button = button_to(l[0], l[1], html_options)
        if options[:pdf]
          link_list_pdf.push(button)
        else
          link_list.push(button)
        end
      end
    end
    txt = "".html_safe
    link_list.each{|l| txt << content_tag(:div, class: 'links'){l}}
    unless link_list_pdf.empty?
      txt << tag(:hr, style: 'color:white;')
      pdf_links = "".html_safe
      link_list_pdf.each{|l| pdf_links << content_tag(:div, class: 'links'){l}}
      txt << content_tag(:div, style: 'position:relative;'){
        content_tag(:div, style: 'margin-left:127px;'){pdf_links} +
        content_tag(:div, style: 'position:absolute;top:0;left:0;'){
          content_tag(:span, style: 'color:white;font-weight:bold;'){"PDF Documents: "}
        }
      }
    end
    if txt.blank?
      txt = '&nbsp;'.html_safe
    end
    if opts[:simple]
      content_tag(:div, class: 'linkbar'){txt}
    else
      content_tag(:div, class: 'linkbar'){txt} + created_by + filter_form
    end
  end

  # Builds a button bar from the contents of the @linksx array. @linksx
  # should be an array of 2-4 element arrays with name & url entries - no
  # permissions are checked, do that before populating @links. All we do here
  # is string the links together wrapped in <div>s.
  #
  # This is basically the same as link_bar but will look slightly
  # different and operates on a separate array, so both can be used on the
  # same view.
  #
  # Can be used multiple times by passing an array in the call. Will default
  # to linksx if none supplied.
  #
  def link_bar_extra(links=@linksx)
    return "" if links.nil?
    return "" unless links.is_a?(Array)
    return "" if links.empty?
    # convert array of hashes to array of strings
    link_list = []
    links.each do |l|
      next unless l.is_a?(Array)
      options = HashWithIndifferentAccess.new(method: :get, disabled: false)
      options.update(l.extract_options!)
      if options[:method] == :get
        # use button_to_function
        link_list.push(button_to_function(l[0], "window.location.href='#{l[1]}'", disabled: options[:disabled]))
      else
        # use button_to so that method='post' and method='put' become available
        link_list.push(button_to(l[0], l[1], options))
      end
    end
    txt = "".html_safe
    link_list.each{|l| txt << content_tag(:div, class: 'links'){l}}
    content_tag(:div, class: 'linkbarx'){txt}
  end
  
  # Looks up the supplied case number.  If valid and user can view cases,
  # returns the a button for the case number linked to the case record.
  # Otherwise, returns the case number without a link.
  def case_number_or_link(caseno)
    return "" unless caseno.is_a?(String) || (caseno.is_a?(Object) && caseno.respond_to?('case_no'))
    if caseno.respond_to?('case_no')
      case_no = caseno.case_no
    else
      case_no = caseno
    end
    return "" if case_no.blank?
    if can?(:view_case)
      return link_to_function(case_no, "window.location.href='#{show_case_case_notes_path(case: case_no)}'")
    else
      return case_no
    end
  end
  
  # This method generates a line of text that shows user name and date/time
  # for the record creator, updater and if possible, current record owner.
  #
  # This is executed automatically by the link_bar method and should not be
  # included manually in any view.  It will only display if the instance
  # variable name matches the singular of the controller name (standard
  # practice with Rails).  If it fails to display on any show view, you may
  # need to rethink your instance variable name.
  def created_by
    record_variable = controller.controller_name.singularize
    record = instance_variable_get("@#{record_variable}")
    return "" if record.nil?
    cname = uname = cdate = udate = ""
    if record.respond_to?('creator') && !record.creator.nil?
      cname = h(record.creator.display_name + "(" + record.created_by.to_s + ")")
    else
      cname = "Unknown"
    end
    if record.respond_to?('updater') && !record.updater.nil?
      uname = h(record.updater.display_name + "(" + record.updated_by.to_s + ")")
    else
      uname = "Unknown"
    end
    if record.respond_to?('created_at') && !record.created_at.nil?
      cdate = AppUtils.format_date(record.created_at)
    else
      cdate = "Unknown"
    end
    if record.respond_to?('updated_at') && !record.updated_at.nil?
      udate = AppUtils.format_date(record.updated_at)
    else
      udate = "Unknown"
    end
    if record.respond_to?('owned_by') && !record.owner.nil?
      oname = h(record.owner.display_name + "(" + record.owned_by.to_s + ")")
    end
    txt = "Created #{cdate} by #{cname} --- last updated #{udate} by #{uname}"
    unless oname.nil?
      txt << " --- Owned By #{oname}"
    end
    content_tag(:div, class: 'created_by'){txt}
  end

  # called to create a string of options for a popup window with defaults set
  def popup_option_string(*opts)
    options = HashWithIndifferentAccess.new(status: 0, toolbar: 0, location: 0, menubar: 0, directories: 0, resizable: 1, scrollbars: 1, height: 480, width: 640)
    options.update(opts.extract_options!)
    "status=#{options[:status]},toolbar=#{options[:toolbar]},location=#{options[:location]},menubar=#{options[:menubar]},directories=#{options[:directories]},resizable=#{options[:resizable]},scrollbars=#{options[:scrollbars]},height=#{options[:height]},width=#{options[:width]}"
  end
  
  # set form focus to a specific element
  def set_focus_to_id(id)
    javascript_tag("$('#{id}').focus()");
  end
  
  # Generates a form authenticity token for use in manually coded forms.
  def form_auth
    hidden_field_tag(request_forgery_protection_token.to_s, form_authenticity_token, id: nil)
  end

  # Adds a standard "Submit" and "Cancel" button pair to edit forms. Can also
  # display a "Delete" button which is usually only added to photo upload
  # views so that existing photos can be removed and/or a "Preview" button.
  def form_buttons(*opts)
    options = HashWithIndifferentAccess.new(delete: false, preview: false)
    options.update(opts.extract_options!)
    options[:popup] ||= true
    content_tag(:div, class: 'linkbar'){
      # first button shoved off-screen consumes Enter key presses - this is used
      # to force movement between fields using Tab without re-wiring the browser
      content_tag(:div, style: 'height:0;width:0;overflow:hidden;'){
        submit_tag("Enter", onclick: 'return false;')
      } +
      submit_tag("Submit") +
      submit_tag("Cancel") +
      (options[:delete] ? submit_tag("Delete") : "".html_safe) +
      (options[:preview] ? (options[:popup] ? submit_tag("Preview", onclick: "displayPreviewPopup('#{root_path}',$(this));return false;") : submit_tag("Preview")) : "".html_safe)
    }
  end
  
  # wrapper for number_to_currency without the currency symbol
  def number_to_money(value)
    number_to_currency(value, unit: '')
  end
  
  # Returns select option array for the specified data set
  def get_opts(otype, *opts)
    return nil unless otype.is_a?(String)
    options = HashWithIndifferentAccess.new(short_opts: false, allow_nil: false, allow_disabled: false, allow_program_only: false, jail_id: nil)
    options.update(opts.extract_options!)
    retarray = []
    if options[:allow_nil] == true
      retarray << ['---',nil]
    end
    case otype
    when 'Admins'
      optarray = User.admins_for_select
    when 'AFIS Status'
      optarray = [['Pending',1],['Final',2],['Reported',3]]
    when 'Agencies'
      optarray = Contact.agencies_for_select(options[:allow_disabled])
    when 'All Contacts'
      optarray = Contact.contacts_and_companies_for_select(options[:allow_disabled])
    when 'All Investigation Contacts'
      optarray = InvestContact.contacts_and_companies_for_select(session[:user],options[:allow_disabled])
    when 'Anatomy'
      optarray = Person::Anatomy.zip(Person::Anatomy)
    when 'Associate Type'
      optarray = Person::Associate.zip(Person::Associate)
    when 'Beard'
      optarray = Person::Beard.zip(Person::Beard)
    when 'Bond Status'
      optarray = Bond.statuses.zip(Bond.statuses)
    when 'Bond Type'
      optarray = Bond.types.zip(Bond.types)
    when 'Bonding Companies'
      optarray = Contact.bond_companies_for_select
    when 'Bondsmen'
      optarray = Contact.bondsmen_for_select(options[:allow_disabled])
    when 'Build Type'
      optarray = Person::BuildType.zip(Person::BuildType)
    when 'Call Disposition'
      optarray = Call.dispositions.keys.sort.collect{|k| [k, k]}
    when 'Call Report'
      optarray = Call.reporting_dates
    when 'Call Shift Numbers'
      optarray = (1..3).collect{|i| [i.to_s,i]}
    when 'Call Shifts'
      optarray = Call.dispatch_shifts_for_select
    when 'Call Via'
      optarray = Call.vias.zip(Call.vias)
    when 'Charge Type'
      optarray = Charge::ChargeType.zip(Charge::ChargeType)
    when 'Cleared Because'
      optarray = Hold::ClearType.zip(Hold::ClearType)
    when 'Colorschemes'
      optarray = colorschemes_for_select
    when 'Commissary Posting'
      optarray = Commissary.posting_dates(options[:jail_id])
    when 'Commissary Type'
      optarray = Commissary::CommissaryType.zip(Commissary::CommissaryType)
    when 'Companies'
      optarray = Contact.companies_for_select(options[:allow_disabled])
    when 'Company Names'
      optarray = Contact.company_names_for_select(options[:allow_disabled])
    when 'Complexion'
      optarray = Person::Complexion.zip(Person::Complexion)
    when 'Consecutive Type'
      optarray = Docket::ConsecutiveType.zip(Docket::ConsecutiveType)
    when 'Contacts'
      optarray = Contact.contacts_for_select(options[:allow_disabled])
    when 'Court Cost'
      optarray = CourtCost.court_costs_for_select
    when 'Court Cost Category'
      optarray = Docket::CourtCostCategory.zip(Docket::CourtCostCategory)
    when 'Court Type'
      optarray = CourtType.types_for_select
    when 'Detective Users'
      optarray = Contact.detective_users_for_select
    when 'Detectives'
      optarray = Contact.detectives_for_select(options[:allow_disabled])
    when 'Districts'
      optarray = SiteConfig.districts.zip(SiteConfig.districts)
    when 'Docket Type'
      optarray = DocketType.types_for_select
    when 'Eye Color'
      optarray = Person::EyeColor.zip(Person::EyeColor)
    when 'Eyebrow'
      optarray = Person::Eyebrow.zip(Person::Eyebrow)
    when 'Facility', 'Jail'
      optarray = Jail.all.collect{|j| [j.acronym, j.id]}
    when 'Forum'
      optarray = Forum.forums_for_select
    when 'Forum Category'
      optarray = ForumCategory.categories_for_select
    when 'Groups'
      optarray = Group.names_for_select
    when 'Hair Color'
      optarray = Person::HairColor.zip(Person::HairColor)
    when 'Hair Type'
      optarray = Person::HairType.zip(Person::HairType)
    when 'History Type'
      optarray = History.types_for_select
    when 'Hold Type'
      optarray = Hold::HoldType.reject{|k,v| v == false}.keys.sort.collect{|t| [t,t]}
    when 'Investigation Status'
      optarray = Investigation::InvestigationStatus.zip(Investigation::InvestigationStatus)
    when 'Items Per Page'
      optarray = [[10,10],[15,15],[20,20],[30,30],[50,50],[100,100],[200,200],[500,500]]
    when 'Jail Permsets'
      optarray = (1..9).collect{|i| ["Permset #{i.to_s}",i]}
    when 'Judge'
      optarray = Contact.judges_for_select(options[:allow_disabled])
    when 'Local Agencies'
      optarray = Contact.local_agencies_for_select(options[:allow_disabled])
    when 'Months'
      optarray = [['January',1],['February',2],['March',3],['April',4],['May',5],['June',6],['July',7],['August',8],['September',9],['October',10],['November',11],['December',12]]
    when 'Mustache'
      optarray = Person::Mustache.zip(Person::Mustache)
    when 'Noservice Type'
      optarray = Paper::NoserviceType.zip(Paper::NoserviceType)
    when 'Notification Frequency'
      optarray = Jail::NotifyFrequency.zip(Jail::NotifyFrequency)
    when 'Officers'
      optarray = Contact.officers_for_select(options[:allow_disabled])
    when 'Paper Customers'
      optarray = PaperCustomer.names_for_select(options[:allow_disabled])
    when 'Paper Customers Common'
      optarray = PaperCustomer.common_names_for_select(options[:allow_disabled])
    when 'Paper Payment Type'
      optarray = PaperPayment.types_for_select
    when 'Paper Posting'
      optarray = PaperPayment.posting_dates
    when 'Paper Type'
      optarray = PaperType.names_for_select
    when 'Patrol Shift Numbers'
      optarray = (1..6).collect{|i| [i.to_s,i]}
    when 'Patrol Shifts'
      optarray = Patrol.valid_shifts_for_select
    when 'Pawn Company'
      optarray = Contact.pawn_companies_for_select(options[:allow_disabled])
    when 'Pawn Item Type'
      optarray = PawnItemType.item_types_for_select
    when 'Pawn Ticket Type'
      optarray = Pawn::PawnTicketType.zip(Pawn::PawnTicketType)
    when 'Pharmacies'
      optarray = Contact.pharmacies_for_select(options[:allow_disabled])
    when 'Phone Type'
      optarray = Contact::PhoneType.zip(Contact::PhoneType)
    when 'Physicians'
      optarray = Contact.physicians_for_select(options[:allow_disabled])
    when 'Plea Type'
      optarray = Docket::PleaType.zip(Docket::PleaType)
    when 'Printable Companies'
      optarray = Contact.companies_for_select(options[:allow_disabled],true)
    when 'Probation Posting'
      optarray = ProbationPayment.posting_dates
    when 'Probation Status'
      optarray = Probation::ProbationStatus.zip(Probation::ProbationStatus)
    when 'Probation Type'
      optarray = Probation::ProbationType.zip(Probation::ProbationType)
    when 'Race'
      optarray = Person::Race.keys.sort.collect{|k|[k,k]}
    when 'Release Reason'
      optarray = Booking.release_reasons.zip(Booking.release_reasons)
    when 'Report Years'
      optarray = ((Date.today.year - 10)..Date.today.year).collect{|y| ["#{y}",y]}
    when 'Service Type'
      optarray = Paper::ServiceType.zip(Paper::ServiceType)
    when 'Skin Tone'
      optarray = Person::SkinTone.zip(Person::SkinTone)
    when 'State'
      optarray = State.states_for_select
    when 'Subject Type'
      optarray = CallSubject.types.zip(CallSubject.types)
    when "Temporary Release Reason"
      optarray = Hold::TempReleaseReason.zip(Hold::TempReleaseReason)
    when 'Transfer Type'
      optarray = Transfer::TransferType.zip(Transfer::TransferType)
    when 'Trial Result'
      optarray = Docket::TrialResult.zip(Docket::TrialResult)
    when 'Users'
      optarray = User.users_for_select(options[:allow_disabled])
    when 'Vehicle Type'
      optarray = Investigation::VehicleType.zip(Investigation::VehicleType)
    when 'Wards'
      optarray = SiteConfig.wards.zip(SiteConfig.wards)
    when 'Warrant Disposition'
      optarray = Warrant::WarrantDisposition.zip(Warrant::WarrantDisposition)
    when 'Warrant Type'
      optarray = [['Warrant',0],['Bench Warrant',1],['Writ',2]]
    when 'Zones'
      optarray = SiteConfig.wards.zip(SiteConfig.wards)
    else
      optarray = []
    end
    return retarray.concat(optarray)
  end
  
  # grabs a list of available colorschemes and returns a select-compatible array
  def colorschemes_for_select
    schemes = []
    Dir.chdir(File.join(Rails.root, "app/assets/stylesheets")) do 
      schemes = Dir["*.scss"]
    end
    cschemes = []
    schemes.sort.each do |c|
      next unless c =~ /^colorscheme_/         
      file_name = c.sub(/^colorscheme_/,'').sub(/\.css\.scss$/,'')
      scheme_name = file_name.titleize.gsub(/ /,'/')
      cschemes.push([scheme_name,file_name])
    end
    cschemes
  end

  # parses content for "emojicons" and replaces them with the image specified
  def emoticonify(content)
    h(content).to_str.gsub(/:([\w+-]+):/) do |match|
      if emoji = Emoji.find_by_alias($1)
        '<img alt="' + $1 + '" height="20" src="' + image_path("emoji/" + emoji.image_filename) + '" style="vertical-align:middle" width="20" />'
      else
        match
      end
    end.html_safe if content.present?
  end
  
  # attempt to replace old prototype link_to_function helper
  def link_to_function(name, *args)
    html_options = args.extract_options!.symbolize_keys
    
    if html_options[:link_type] == 'button'
      link_type = 'button'
    else
      link_type = 'link'
    end
    
    html_options.delete(:link_type)
    
    function = args[0] || ''
    unless function.blank?
      html_options[:onclick] = function
    end
    href = html_options[:href] || "#"
    
    if link_type == 'link'
      content_tag(:a, name, html_options.merge(href: href))
    else
      button_tag(name, html_options.merge(type: 'button', style: 'font-weight:normal'))
    end
  end
  
  # button_to_function is an alias for link_to_function
  def button_to_function(name, *args)
    args = args.push(link_type: 'button')
    link_to_function(name, *args)
  end
end