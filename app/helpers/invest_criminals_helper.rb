module InvestCriminalsHelper
  def invest_criminal_links(invest_criminals)
    return '' unless invest_criminals.is_a?(Array) && can?(:view_investigations)
    txt = []
    invest_criminals.each do |c|
      next unless c.is_a?(InvestCriminal)
      if c.private? && c.owned_by != session[:user]
        txt.push(c.id.to_s.html_safe)
      else
        txt.push(link_to c.id.to_s, c)
      end
    end
    if txt.empty?
      'None'
    else
      safe_join(txt, ', ')
    end
  end
end