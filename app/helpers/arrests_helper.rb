module ArrestsHelper
  # provides an html formatted safe multi-line string listing all arrest officers
  def html_officers(arrest)
    return "" unless arrest.is_a?(Arrest)
    txt = []
    arrest.officers.each do |o|
      txt.push("#{o.officer_unit? ? o.officer_unit + ' ' : ''}#{o.officer}")
    end
    safe_join(txt, tag(:br))
  end
end