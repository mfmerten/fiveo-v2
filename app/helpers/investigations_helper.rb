module InvestigationsHelper
  # produces a standard privacy section on an edit form
  def privacy_section
    record_variable = controller.controller_name.singularize
    record = instance_variable_get("@#{record_variable}")
    return "" if record.nil?
    tt = 'Access and Ownership only be changed at the top-most (Investigation) level.'
    h4_tag = content_tag(:h4){with_tooltip("Privacy Controls", tt)}
    private_tag = "".html_safe
    if record.respond_to?('private?')
      private_tag = edit_row('Private?', record.private? ? "Yes" : 'No')
    end
    owner_tag = "".html_safe
    if record.respond_to?('owner')
      owner_tag = edit_row('Current Owner:', AppUtils.show_user(record.owner))
    end
    return "" if owner_tag.blank? && private_tag.blank?
    h4_tag + private_tag + owner_tag
  end
  
  def investigation_links(investigations)
    return '' unless investigations.is_a?(Array) && can?(:view_investigations)
    txt = []
    investigations.each do |i|
      next unless i.is_a?(Investigation)
      if i.private? && i.owned_by != session[:user]
        txt.push(i.id.to_s.html_safe)
      else
        txt.push(link_to i.id.to_s, i)
      end
    end
    if txt.empty?
      'None'
    else
      safe_join(txt, ', ')
    end
  end
  
  # generates a notes section with ajax updating, in-place editing and dynamic
  # new/delete buttons -- assumes association is 'notes'
  # supports investigation objects and child objects of investigation objects.
  def ajax_notes(object, *args)
    options = HashWithIndifferentAccess.new(association: :notes)
    options.update(args.extract_options!)
    txt = ''.html_safe
    records = []
    parent = object.class.name.tableize.singularize
    unless parent == 'investigation'
      parent_object = object.investigation
    end
    object.send(options[:association].to_s).each do |note|
      edit_links = ''.html_safe
      if can?(:update_investigation) && check_author(object) && check_privacy(object)
        if parent_object
          # child of investigation (crime, report, etc)
          edit_links << link_to(icon('edit'), self.send("edit_investigation_#{parent}_invest_note_path", parent_object, object, note), style: 'margin-right:10px;')
          edit_links << link_to(icon('delete'), self.send("investigation_#{parent}_invest_note_path", parent_object, object, note), method: :delete, data: {confirm: confirm_msg_for(:note)})
        else
          # investigation
          edit_links << link_to(icon('edit'), self.send("edit_#{parent}_invest_note_path", object, note), style: 'margin-right:10px;')
          edit_links << link_to(icon('delete'), self.send("#{parent}_invest_note_path", object, note), method: :delete, data: {confirm: confirm_msg_for(:note)})
        end
      end
      records.push(
        content_tag(:tr){
          content_tag(:td){
            content_tag(:div, style: 'float:right;margin-right:10px;'){edit_links} +
            content_tag(:b){note.updated_at == note.created_at ? 'Created: ' : 'Updated: '} +
            content_tag(:span){AppUtils.format_date(note.updated_at)} +
            field_spacer +
            content_tag(:b){"By: "} +
            content_tag(:span){object.creator.display_name} +
            content_tag(:div){simple_format(note.note)}
          }
        }
      )
    end
    add_link = ''.html_safe
    if can?(:update_investigation) && check_author(object) && check_privacy(object)
      if parent_object
        # child of investigation (crime, report, etc)
        add_link << link_to(icon('add', height: 24), self.send("new_investigation_#{parent}_invest_note_path", parent_object, object))
      else
        # investigation
        add_link << link_to(icon('add', height: 24), self.send("new_#{parent}_invest_note_path", object))
      end
    end
    content_tag(:div, id: 'note-div'){
      content_tag(:h4){"Notes ".html_safe + add_link} +
      content_tag(:table, class: 'special', id: 'note-table'){safe_join(records, '\n'.html_safe)} +
      link_bar_extra
    }
  end
  
  # creates a table listing options to choose from an association
  # this special form deals with renamed direct investigation links rather
  # than the joins expected by nested_chooser_for
  def invest_chooser_for(form, association, *args)
    options = args.extract_options!
    object = form.object
    investigation = object.investigation
    prefix = object.class.name.tableize.singularize
    assoc_reflection = object.class.reflect_on_association(association)
    field_name = "#{prefix}[linked_#{assoc_reflection.name}][]"
    partial_name = "#{assoc_reflection.options[:source].to_s.pluralize}/#{assoc_reflection.options[:source]}_info"
    join_association = assoc_reflection.options[:through].to_s
    txt = ''.html_safe
    title = assoc_reflection.name.to_s.titleize
    if options[:tooltip]
      title = with_tooltip(title, options[:tooltip])
    end
    for record in investigation.send(association) do
      partial_contents = render(partial: partial_name, object: record)
      txt << content_tag(:tr){
        content_tag(:td){
          check_box_tag(field_name, record.id, object.send(association).include?(record), id: nil)
        } +
        content_tag(:td, style: 'width:100%;'){partial_contents}
      }
    end
    content_tag(:h4){title} +
    content_tag(:table, class: 'special', style: 'width:93%;margin-left:50px;'){txt}
  end
  
  # creates a table listing photos to choose from investigation.photos association
  # association is expected to be simply 'photos' but can be specified
  def invest_photo_chooser_for(form, *args)
    options = args.extract_options!
    association = options[:association] || :photos
    object = form.object
    investigation = object.investigation
    prefix = object.class.name.tableize.singularize
    field_name = "#{prefix}[linked_photos][]"
    title = "Photos"
    if options[:tooltip]
      title = with_tooltip(title, options[:tooltip])
    end
    counter = 0
    rows = ''.html_safe
    until investigation.photos[counter].nil?
      columns = ''.html_safe
      3.times do
        photo = investigation.photos[counter]
        if photo.nil?
          columns << content_tag(:td, style: 'width:3%'){} + content_tag(:td, style: 'width:30%'){}
        else
          columns << content_tag(:td, style: 'width:3%'){
            check_box_tag(field_name, photo.id, object.send(association).include?(photo), id: nil)
          }
          columns << content_tag(:td, style: 'width:30%;'){image_tag(photo.invest_photo.url(:small))}
        end
        counter += 1
      end
      rows << columns
    end
    content_tag(:h4){title} +
    content_tag(:table, class: 'special', style: 'width:93%;margin-left:50px;'){rows}
  end
  
end