module MedicalsHelper
  def medical_problem_list(medical)
    return unless medical.is_a?(Medical)
    problems = []
    medical.problem_names.keys.each do |problem|
      has_problem = medical.send(problem)
      unless has_problem.blank?
        problems.push([medical.problem_names(problem), has_problem])
      end
    end
    if problems.empty?
      "None Reported"
    else
      content_tag(:dl){safe_join(problems.collect{|p| content_tag(:dt){p[0]} + content_tag(:dd){p[1]}},"\n".html_safe)}
    end
  end
  
  def medical_condition_list(medical)
    return '' unless medical.is_a?(Medical)
    conditions = []
    medical.condition_names.keys.each do |condition|
      if medical.send("#{condition}?")
        conditions.push(medical.condition_names[condition])
      end
    end
    if conditions.empty?
      "None Reported"
    else
      content_tag(:ul){safe_join(conditions.collect{|c| content_tag(:li){c}},"\n".html_safe)}
    end
  end
end