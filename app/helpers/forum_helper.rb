# adapted from "forem" engine (https://github.com/radar/forem)
#
# Copyright 2011 Ryan Bigg, Philip Arndt and Josh Adams
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
module ForumHelper
  ### From application_helper
  
  def forum_badge(user)
    unless SiteConfig.posts_per_star == 0
      rank = (user.forum_score / SiteConfig.posts_per_star)
      rank = 5 if rank > 5
      badge = ''.html_safe
      rank.times do
        badge << icon('star', height: 12)
      end
      content_tag(:div, class: 'forum-badge'){badge}
    end
  end

  def forum_format(text, *options)
    emoticonify(as_formatted_html(text))
  end

  def forum_quote(text)
    as_quoted_text(text)
  end

  def as_formatted_html(text)
    texthtml(text)
  end

  def as_quoted_text(text)
    %(<div class='quote'>\n#{text}\n</div>\n)
  end

  def topics_count(forum)
    forum.forum_topics.count
  end

  def posts_count(forum)
    forum.forum_posts.count
  end
  
  def forum_avatar(user)
    image_tag user.user_photo.url(:thumb), alt: "Avatar"
  end
  
  def link_to_latest_post(topic)
    post = relevant_posts(topic).last
    text = "#{time_ago_in_words(post.created_at)} Ago By #{post.user}"
    link_to text, forum_forum_topic_path(post.forum_topic.forum, post.forum_topic, anchor: "post-#{post.id}", page: post.forum_topic.last_page(current_user))
  end

  def new_since_last_view_text(topic)
    topic_view = topic.view_for(current_user)
    forum_view = topic.forum.view_for(current_user)
    if forum_view
      if topic_view.nil? && topic.created_at > forum_view.past_viewed_at
        content_tag :super, "New"
      end
    end
  end

  def relevant_posts(topic)
    posts = topic.forum_posts.by_created_at
    if can?(:admin_forum)
      posts
    else
      posts.visible
    end
  end
end