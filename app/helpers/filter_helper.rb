module FilterHelper
  # methods for generating and manipulating filters and search

  # show filter help
  def filter_help(*opts)
    options = opts.extract_options!
    content_tag(:div, class: 'filter-help-div'){
      content_tag(:div, class: 'filter-help-label'){"Instructions"} +
      content_tag(:ul, class: 'filter-help-list'){
        content_tag(:li){AppUtils.compress_spaces("Enter your search term and click the <b>Set
          Filter</b> button. It will remain in effect until you <b>Clear
          Filter</b> or log out.").html_safe} +
        content_tag(:li){AppUtils.compress_spaces("Only records matching <b>all</b> of the
          information you enter will be found so do not enter more than you
          really need.").html_safe} +
        (options[:name] ? content_tag(:li){AppUtils.compress_spaces("When searching by name,
          you must enter <b>at least</b> the last name.  Do not enter
          initials.").html_safe} : '') +
        (options[:date] ? content_tag(:li){AppUtils.compress_spaces("For date ranges, entering
          only <b>Date From:</b> will find for that date and after. Entering
          only <b>Date To:</b> will find for that date or before. Entering both
          will find between the two dates (inclusive). Enter both the same to
          find for only one day.").html_safe} : '')
      }
    }
  end
  
  # detects whether @query is set and partial exists
  # returns empty string if not
  #
  # To actually produce a Filter form, you need the @query instance variable defined and
  # you need a _filter.html.erb partial.
  #
  # This method is automatically included anywhere a link_bar is executed. It should not
  # be added manually to any views.
  def filter_form
    if File.exist?("#{Rails.root}/app/views/#{controller.controller_name}/_filter.html.erb") && !@query.nil?
      content_tag(:div, id: 'search-filter-form'){build_filter}
    else
      ""
    end
  end
  
  # this builds a filter form from _filter partial
  #
  # the filter partial should be a series of filter_xxxx_row calls but can include
  # custom fields using 'filter_row("Title:") do' syntax
  #
  # The _filter partial should look something like:
  # <%= filter_help... %>
  # <%= filter_person_row... %>
  # <%= filter_text_row... %>
  # <%= filter_select_row... %>
  #
  # if filter partial exists and @query is defined, builds an advanced filter form
  # using the filter partial.
  #
  def build_filter
    filter_defined = File.exist?("#{Rails.root}/app/views/#{controller.controller_name}/_filter.html.erb") && !@query.nil?
    cname = controller.controller_name
    if @jail.nil?
      list_url = controller.send("#{cname}_path")
    else
      list_url = controller.send("jail_#{cname}_path",@jail)
    end
    clear_url = "#{list_url}?filter=clear"
    if filter_defined
      filter_div = content_tag(:div, id: 'advanced-search'){
        form_tag(list_url, method: :get){
          field_set_tag("Filter", class: 'filter-form'){
            render(partial: 'filter') +
            content_tag(:div, class: 'submit'){
              submit_tag("Set Filter", id: nil) +
              space(2) +
              content_tag(:span, style: 'display:inline-block;'){button_to_function("Clear Filter", "window.location.href='#{clear_url}'")}
            }
          }
        }
      }
    else
      filter_div = ''.html_safe
    end
    filter_div
  end

  # Creates a date select field.
  def filter_date_field(param, *opts)
    options = opts.extract_options!
    return nil unless param.is_a?(String) || param.is_a?(Symbol)
    value = @query[param.to_sym]
    options[:id] = "query_#{param.to_s}"
    options[:size] = 10
    options[:class] = 'date_mask'
    text_field_tag("query[#{param.to_s}]", value, options)
  end

  # Wrapper for filter_date_field that generates an entire row
  def filter_date_row(param, *opts)
    options = opts.extract_options!
    row_options = {}
    if options[:tooltip]
      row_options[:tooltip] = options[:tooltip]
      options.delete(:tooltip)
    end
    if options[:header]
      header = options[:header]
      options.delete(:header)
    else
      header = "Date:"
    end
    filter_row(header, filter_date_field(param.to_sym, options), row_options)
  end

  # this creates a complete row with from and to date fields to create
  # a date-range-row for filters.
  def filter_date_range_row(param,*opts)
    options = opts.extract_options!
    row_options = {}
    if options[:tooltip]
      row_options[:tooltip] = options[:tooltip]
      options.delete(:tooltip)
    end
    param_from = "#{param.to_s}_from"
    param_to = "#{param.to_s}_to"
    if options[:header]
      header = options[:header]
      options.delete(:header)
    else
      header = "Date:"
    end
    filter_row(header, row_options) do
      content_tag(:span){"<b>From:</b> ".html_safe + filter_date_field(param_from.to_sym, options.dup)} +
      content_tag(:span){"<b>To:</b> ".html_safe + filter_date_field(param_to.to_sym, options.dup)}
    end
  end

  # Generates a text input field on manually created forms like those used
  # for the search filters on index views.
  def filter_text_field(param,*opts)
    options = opts.extract_options!
    value = @query[param.to_sym] || ""
    options[:size] ||= 30
    options[:id] = nil
    text_field_tag("query[#{param.to_s}]", value, options)
  end

  # Wrapper for filter_text_field to return entire row
  # if :header is unspecified, will use param (:user becomes 'User:')
  def filter_text_row(param,*opts)
    options = opts.extract_options!
    row_options = {}
    if options[:tooltip]
      row_options[:tooltip] = options[:tooltip]
      options.delete(:tooltip)
    end
    if options[:header]
      header = options[:header]
      options.delete(:header)
    else
      header = param.to_s.gsub(/_/,' ').titleize.gsub(/ Id$/, ' ID') + ':'
    end
    filter_row(header, filter_text_field(param.to_sym, options), row_options)
  end
  
  def filter_phone_row(params,*opts)
    options = opts.extract_options!
    if options[:class].nil?
      options[:class] = 'phone_mask'
    else
      options[:class] = options[:class] + ' phone_mask'
    end
    options[:size] = 12
    filter_text_row(param.to_sym, options)
  end
  
  def filter_ssn_field(params, *opts)
    options = opts.extract_options!
    if options[:class].nil?
      options[:class] = 'ssn_mask'
    else
      options[:class] = options[:class] + ' ssn_mask'
    end
    options[:size] = 11
    filter_text_field(params.to_sym, options)
  end

  def filter_ssn_row(params,*opts)
    options = opts.extract_options!
    filter_row("SSN:", filter_ssn_field(params, options))
  end
  
  def filter_contact_field(param, *opts)
    options = opts.extract_options!
    content_tag(:span){filter_text_field("#{param}_badge".to_sym, options.dup.update({size: 6, placeholder: 'Badge'}))} +
    content_tag(:span){filter_text_field("#{param}_unit".to_sym, options.dup.update({size: 6, placeholder: 'Unit'}))} +
    content_tag(:span){filter_text_field(param.to_sym, options.dup.update({size: 25, placeholder: "Full Name"}))}
  end

  # Wrapper for filter_text_field to return a complex row for contact
  def filter_contact_row(param, *opts)
    options = opts.extract_options!
    row_options = {}
    if options[:tooltip]
      row_options[:tooltip] = options[:tooltip]
      options.delete(:tooltip)
    end
    if options[:header]
      header = options[:header]
      options.delete(:header)
    else
      header = param.to_s.titleize + ':'
    end
    filter_row(header, filter_contact_field(param, options.dup), row_options)
  end

  # wrapper for filter_text_field and filter_select that builds an address
  # input block (returns a complete filter table row)
  def filter_address_row(street,street2,city,state,zip,*opts)
    options = opts.extract_options!
    row_options = {}
    if options[:tooltip]
      row_options[:tooltip] = options[:tooltip]
      options.delete(:tooltip)
    end
    if options[:header]
      header = options[:header]
      options.delete(:header)
    else
      header = "Address:"
    end
    filter_row(header, row_options) do
      (street.nil? ? "".html_safe : content_tag(:div){filter_text_field(street.to_sym, options.dup.update({size: 50, placeholder: 'Street'}))}) +
      (street2.nil? ? "".html_safe : content_tag(:div){filter_text_field(street2.to_sym, options.dup.update({size: 50, placeholder: 'Street Line 2'}))}) +
      (city.nil? ? "".html_safe : content_tag(:span){filter_text_field(city.to_sym, options.dup.update({size: 25, placeholder: 'City'}))}) +
      (state.nil? ? "".html_safe : content_tag(:span){filter_select(state.to_sym, 'State', options.dup)}) +
      (zip.nil? ? "".html_safe : content_tag(:span){filter_text_field(zip.to_sym, options.dup.update({size: 10, placeholder: 'ZIP'}))})
    end
  end

  # wrapper for filter_text_field that builds a row for a person (id and name)
  # block (returns a complete filter table row)
  def filter_person_row(id_param, name_param, *opts)
    options = opts.extract_options!
    row_options = {}
    if options[:tooltip]
      row_options[:tooltip] = options[:tooltip]
      options.delete(:tooltip)
    end
    header = options[:header] || "Person:"
    options.delete(:header)
    filter_row(header, row_options) do
      content_tag(:span){filter_text_field(id_param.to_sym, options.dup.update({size: 8, placeholder: 'ID'}))} +
      content_tag(:span){filter_text_field(name_param.to_sym, options.dup.update({size: 35, placeholder: 'Full Name'}))}
    end
  end

  # Generates a select field on manually created forms like those used for
  # the search filters on index views.
  def filter_select(param, option_type, *opts)
    options = HashWithIndifferentAccess.new(include_nil: true, allow_disabled: true, allow_program_only: true)
    options.update(opts.extract_options!)
    value = @query[param.to_sym]
    if option_type.is_a?(String)
      # get_otps option name supplied
      select_options = options_for_select(get_opts(option_type, allow_disabled: options[:allow_disabled], allow_nil: options[:include_nil], allow_program_only: options[:allow_program_only], jail_id: options[:jail_id]), value)
    else
      # array of options supplied
      select_options = options_for_select(option_type, value)
    end
    options.delete(:include_nil)
    options.delete(:allow_disabled)
    options.delete(:allow_program_only)
    options.delete(:jail_id)
    options[:id] = nil
    select_tag("query[#{param.to_s}]", select_options, options)
  end

  def filter_select_row(param, option_type, *opts)
    options = opts.extract_options!
    row_options = {}
    if options[:tooltip]
      row_options[:tooltip] = options[:tooltip]
      options.delete(:tooltip)
    end
    if options[:header]
      header = options[:header]
      options.delete(:header)
    else
      header = param.to_s.gsub(/_id$/,'').titleize + ':'
    end
    filter_row(header, filter_select(param, option_type, options), row_options)
  end
  
  def filter_oln_field(oln_param, state_param, *opts)
    options = opts.extract_options!
    content_tag(:span){filter_text_field(oln_param.to_sym, options.dup.update({size: 12, placeholder: 'OLN'}))} +
    content_tag(:span){filter_select(state_param.to_sym, 'State', options.dup.update({placeholder: 'State'}))}
  end

  def filter_oln_row(oln_param, state_param, *opts)
    options = opts.extract_options!
    row_options = {}
    if options[:tooltip]
      row_options[:tooltip] = options[:tooltip]
      options.delete(:tooltip)
    end
    if options[:header]
      header = options[:header]
      options.delete(:header)
    else
      header = "OLN:"
    end
    filter_row(header, filter_oln_field(oln_param, state_param, options), row_options)
  end

  # Returns a set of radio buttons with the indicated values and labels with
  # an additional button for the nil value labeled "Any". The button
  # corresponding to the current @query value will be set.
  # choices should look something like [['0','No'],['1','Yes']]
  def filter_radio_buttons(param, choices, *opts)
    options = opts.extract_options!
    value = @query[param.to_sym]
    text = "".html_safe
    options[:id] = nil
    choices.each do |val, label|
      text << radio_button_tag("query[#{param.to_s}]", val, (value == val), options) + content_tag(:span, label)
    end
    text + radio_button_tag("query[#{param.to_s}]", "", value.blank?, options) + content_tag(:span, "Any")
  end

  # wrapper for filter_radio_buttons to return a complete table row
  def filter_radio_row(param, choices, *opts)
    options = opts.extract_options!
    row_options = {}
    if options[:tooltip]
      row_options[:tooltip] = options[:tooltip]
      options.delete(:tooltip)
    end
    if options[:header]
      header = options[:header]
      options.delete(:header)
    else
      header = param.to_s.titleize + ':'
    end
    filter_row(header, filter_radio_buttons(param.to_sym, choices, options), row_options)
  end

  # returns a set of radia buttons for Yes/No
  def filter_boolean_buttons(param, *opts)
    options = opts.extract_options!
    choices = [['1','Yes'],['0','No']]
    filter_radio_buttons(param.to_sym, choices, options)
  end

  # returns a set of radio buttons for Yes/No/Don't Care
  # as a complete table row
  def filter_boolean_row(param, *opts)
    options = opts.extract_options!
    row_options = {}
    if options[:tooltip]
      row_options[:tooltip] = options[:tooltip]
      options.delete(:tooltip)
    end
    if options[:header]
      header = options[:header]
      options.delete(:header)
    else
      header = param.to_s.titleize + '?'
    end
    filter_row(header, filter_boolean_buttons(param, options), row_options)
  end
end