module ContactsHelper
  def html_flags_for(contact)
    return "" unless contact.is_a?(Contact)
    txt = "".html_safe
    if contact.respond_to?('legacy') && contact.legacy?
      txt << content_tag(:span){icon('legacy')}
    end
    if contact.business?
      txt << content_tag(:span){icon('business')}
    else
      txt << content_tag(:span){icon('person')}
    end
    if contact.officer?
      if contact.business?
        txt << content_tag(:span){"[Agency]"}
      else
        txt << content_tag(:span){"[Officer]"}
      end
    end
    if contact.detective?
      txt << content_tag(:span){"[Detective]"}
    end
    if contact.bondsman?
      txt << content_tag(:span){"[Bondsman]"}
    end
    if contact.bonding_company?
      txt << content_tag(:span){"[Bonding Co.]"}
    end
    if contact.judge?
      txt << content_tag(:span){"[Judge]"}
    end
    if contact.pawn_co?
      txt << content_tag(:span){"[Pawn Shop]"}
    end
    if contact.physician?
      txt << content_tag(:span){"[Doctor]"}
    end
    if contact.pharmacy?
      txt << content_tag(:span){"[Pharmacy]"}
    end
    if contact.respond_to?('printable') && contact.printable?
      txt << content_tag(:span, class: 'blue'){"[Print]"}
    end
    if contact.active?
      txt << content_tag(:span, class: 'green'){"[Active]"}
    else
      txt << content_tag(:span, class: 'red'){"[Inactive]"}
    end
    txt
  end
  
  # provides an html formatted string for the specified phone for
  # a contact record, an invest_contact record or a warrant record.
  def html_phone(contact, phone)
    return "" unless contact.is_a?(Contact) || contact.is_a?(Warrant) || contact.is_a?(InvestContact)
    return "" unless contact.respond_to?(phone.to_s)
    txt = ''.html_safe
    if contact.send("#{phone}?")
      txt << content_tag(:b){(contact.send("#{phone}_type").blank? ? 'Phone' : contact.send("#{phone}_type")) + ": "}
      if contact.send("#{phone}_unlisted?")
        txt << content_tag(:span, class: 'red', style: 'margin:0;'){contact.send("#{phone}")}
      else
        txt << contact.send("#{phone}")
      end
    end
    content_tag(:span, style: 'font-weight:normal;'){txt}
  end
  
  # provides an html formatted string of any of the 3 possible phone numbers for
  # a contact record, an invest_contact record or a warrant record.
  def html_phones(contact)
    return "" unless contact.is_a?(Contact) || contact.is_a?(Warrant) || contact.is_a?(InvestContact)
    txt = ''.html_safe
    if contact.phone1?
      txt << content_tag(:b){(contact.phone1_type.blank? ? 'Phone' : contact.phone1_type) + ": "}
      if contact.phone1_unlisted?
        txt << content_tag(:span, class: 'red', style: 'margin:0;'){contact.phone1}
      else
        txt << contact.phone1
      end
    end
    if contact.phone2?
      unless txt.blank?
        txt << "&nbsp;&nbsp;&nbsp;".html_safe
      end
      txt << content_tag(:b){(contact.phone2_type.blank? ? 'Phone' : contact.phone2_type) + ": "}
      if contact.is_a?(Contact) && contact.phone2_unlisted?
        txt << content_tag(:span, class: 'red', style: 'margin:0'){contact.phone2}
      else
        txt << contact.phone2
      end
    end
    if contact.phone3?
      unless txt.blank?
        txt << "&nbsp;&nbsp;&nbsp;".html_safe
      end
      txt << content_tag(:b){(contact.phone3_type.blank? ? 'Phone' : contact.phone3_type) + ": "}
      if contact.is_a?(Contact) && contact.phone3_unlisted?
        txt << content_tag(:span, class: 'red', style: 'margin:0'){contact.phone3}
      else
        txt << contact.phone3
      end
    end
    content_tag(:span, style: 'font-weight:normal;'){txt}
  end
end