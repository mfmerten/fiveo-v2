module BookingsHelper
  def medical_disposition
    txt = "".html_safe
    if @booking.medicals.empty?
      txt << content_tag(:span, class: 'red'){"Not Screened"}
    else
      color = @booking.medicals.last.has_problems? ? 'red' : 'green'
      txt << content_tag(:span, class: color){@booking.medicals.last.disposition}
    end
    txt
  end

  def good_time_status
    txt = "".html_safe
    if @booking.good_time?
      txt << content_tag(:span, class: 'green'){'ON'}
    else
      txt << content_tag(:span, class: 'red'){'OFF'}
    end
    txt << space(2)
    txt << content_tag(:span, class: 'blue', style: 'font-weight:normal;'){"(Ignored for DOC Sentences)"}
    txt
  end
  
  def time_log_status
    txt = "".html_safe
    if @booking.disable_timers?
      txt << icon('stopped')
    else
      txt << content_tag(:span, class: 'green'){"Enabled"}
    end
    txt << space(3)
    txt << "("
    txt << @booking.simple_time_served_by_log
    txt << content_tag(:span, style: 'font-weight:normal;'){" Total Time Logged)"}
    txt
  end
  
  def emergency_contact_info
    txt = "".html_safe
    txt << @booking.person.emergency_contact
    if @booking.person.em_relationship?
      txt << " ("
      txt << @booking.person.em_relationship
      txt << ")"
    end
    txt << space(2)
    txt << @booking.person.emergency_phone
    txt
  end
  
  def show_inmate
    txt = "".html_safe
    txt << AppUtils.show_person(@booking.person)
    if @booking.doc_billable? && can?(:update_billing)
      txt << space(2)
      txt << icon('doc-billable')
    elsif @booking.doc?
      txt << space(2)
      txt << icon('doc')
    end
    txt
  end
  
  def scheduled_release_status
    "".html_safe +
    (Time.now + @booking.total_remaining_hours.hours).to_date.to_s(:us) +
    space(3) +
    content_tag(:span, class: 'blue', style: 'font-weight:normal;'){"(Future Good Time Not Considered)"}
  end
  
  def show_holds_section
    holds_all = @booking.holds.includes(:booking, :doc_record, :transfer, :sentence, :revocation, :probation, :arrest, :bonds)
    holds_active = holds_all.active
    txt = "".html_safe
    active_tag = "".html_safe
    unless holds_active.empty?
      active_tag << do_list('Active Holds', 'holds/hold', holds_active)
    end
    active_tag << button_to_function("Show All"){|page| page['active-holds'].hide; page['all-holds'].show}
    txt << content_tag(:div, id: 'active-holds'){active_tag}
    all_tag = "".html_safe
    unless holds_all.empty?
      all_tag << do_list('All Holds', 'holds/hold', holds_all)
    end
    all_tag << button_to_function("Show Active"){|page| page['active-holds'].show; page['all-holds'].hide}
    txt << content_tag(:div, id: 'all-holds', style: 'display:none;'){all_tag}
    txt << tag(:br)
    txt
  end
  
  def html_status(booking)
    return "" unless booking.is_a?(Booking)
    txt = ''.html_safe
    booking.status(returns: :array).each do |s|
      if ["[Temporary Housing]", "[Temporary Transfer]"].include?(s)
        txt << content_tag(:span, s, class: 'green')
      elsif ['[48 Hour EXP]', '[72 Hour EXP]', '[No Holds]', '[Time Served]'].include?(s)
        txt << content_tag(:span, s, style: 'color:white;background-color:red;font-weight:bold;')
      elsif ['[48 Hour]', '[72 Hour]', '[Bondable]'].include?(s)
        txt << content_tag(:span, s, style: 'color:blue;background-color:yellow;')
      elsif s == "[Pending Sentence]" || s =~ /^\[[0-9]/
        txt << content_tag(:span, s, style: 'color:black;background-color:aquamarine;')
      elsif ["[Hold for City]", "[Hold for Other Agency]", "[No Bond]", "[Probation Hold]"].include?(s)
        txt << content_tag(:span, s, class: 'red')
      elsif s == "[Temporary Release]"
        txt << content_tag(:span, s, class: 'blue')
      else
        txt << content_tag(:span, s)
      end
    end
    txt
  end
end
