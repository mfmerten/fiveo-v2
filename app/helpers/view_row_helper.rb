module ViewRowHelper
  # standardized methods for generating views that all look alike
  
  # used to generate rows on show views (and other places)
  def view_row(header, *args, &block)
    options = args.extract_options!
    value, header2, value2 = args
    options[:div_class] ||= 'view-row'
    if options[:narrow]
      options[:div_class] += '-medium'
    end
    header_class = 'view-header'
    if options[:header_class]
      header_class << " #{options[:header_class]}"
    end
    data_class = 'view-data'
    if options[:class]
      data_class << " #{options[:class]}"
    end
    if options[:required]
      header = required + header
    end
    if options[:required2]
      header2 = required + header2
    end
    if options[:dangerous]
      header_class << ' red'
    end
    if options[:dangerous2]
      header_class2 << ' red'
    end
    hidejs = nil
    js1 = nil
    js2 = nil
    heading_class = nil
    heading2_class = nil
    html1_options = HashWithIndifferentAccess.new(class: header_class)
    html2_options = HashWithIndifferentAccess.new(class: header_class)
    if options[:tooltip]
      html1_options['data-ot'] = AppUtils.compress_spaces(options[:tooltip])
      heading_class = 'tooltip'
    end
    if options[:tooltip2]
      html2_options['data-ot'] = AppUtils.compress_spaces(options[:tooltip2])
      heading_class2 = 'tooltip'
    end
    scol_class = 'one-col-div'
    if header2.blank?
      # single column
      content_tag(:div, class: options[:div_class]){
        content_tag(:div, class: scol_class){
          content_tag(:div, class: data_class){
            block_given? ? yield : value.to_s
          } +
          content_tag(:div, html1_options){
            content_tag(:div, class: heading_class){header}
          }
        }
      }
    else
      content_tag(:div, class: options[:div_class]){
        content_tag(:div, class: 'two-col-div'){
          content_tag(:div, class: data_class){value} +
          content_tag(:div, html1_options){
            content_tag(:div, class: heading_class){header}
          }
        } +
        content_tag(:div, class: 'two-col-div'){
          content_tag(:div, class: data_class){value2} +
          content_tag(:div, html2_options){
            content_tag(:div, class: heading_class2){header2}
          }
        }
      }
    end
  end
  
  # used to generate rows on edit views (and other places)
  def edit_row(header, *args, &block)
    options = args.extract_options!
    options[:div_class] = 'view-row-color'
    value = block_given? ? capture(&block).html_safe : args.first
    view_row(header, value, options)
  end
  
  # used to generate rows on edit views (and other places)
  def filter_row(header, *args, &block)
    options = args.extract_options!
    options[:div_class] = 'view-row-gray'
    value = block_given? ? capture(&block).html_safe : args.first
    view_row(header, value, options)
  end
  
  # used in many places to generate a date entry field with formatting
  def date_entry_tag(label, value, *args)
    options = args.extract_options!
    options[:class] = 'date_mask'
    options[:size] = 10
    text_field_tag(label, value, options)
  end
  
  # used in may places to generate a time entry field with formatting
  def time_entry_tag(label, value, *args)
    options = args.extract_options!
    options[:class] = 'time_mask'
    options[:size] = 5
    text_field_tag(label, value, options)
  end
  
  # Adds a note to views
  def view_note(*items)
    options = items.extract_options!
    image = options[:icon] || 'info'
    options.delete(:icon)
    items = items.flatten
    options[:text_class] ||= 'view-note-text'
    if options[:class]
      options[:text_class] << " #{options[:class]}"
    end
    text = ''.html_safe
    tabcontents = ''.html_safe
    items.each do |i|
      unless i.blank? || !i.is_a?(String)
        tabcontents << content_tag(:div, class: 'view-note'){
          content_tag(:span, class: 'view-note-image'){icon(image, height: 14)} +
          content_tag(:span, class: options[:text_class]){AppUtils.compress_spaces(i).html_safe}
        }
      end
    end
    tabcontents
  end
  
  # view_note is also aliased as edit_note, filter_note, report_note
  alias :edit_note :view_note
  alias :filter_note :view_note
  
  def view_text(header, text, *args)
    return "".html_safe if text.blank? || !text.is_a?(String)
    options = args.extract_options!
    div_class = 'view-text'
    if options[:class]
      div_class << " #{options[:class]}"
    end
    options[:display] ||= 'textile'
    options[:note] ||= remark_note
    txt = "".html_safe
    if options[:display] == 'break'
      txt << newline_to_break(text)
    elsif options[:display] == 'simple'
      txt << simple_format(text)
    else
      txt << texthtml(text)
    end
    if header.blank?
      heading_tag = "".html_safe
    else
      heading_tag = content_tag(:h3, class: options[:header_class]){header}
    end
    if header == 'Remarks'
      note_tag = view_note(options[:note], class: options[:note_class])
    else
      note_tag = "".html_safe
    end
    heading_tag + content_tag(:div, class: div_class){note_tag + txt}
  end
  
  # creates a list from an array of items
  def view_list(items=[], *args)
    options = args.extract_options!
    options[:type] ||= 'ul'
    return "" if items.blank? || !items.is_a?(Array) || items.empty?
    lines = []
    items.each do |i|
      case
      when i.blank?
        lines.push(content_tag(:li){' '})
      when i.is_a?(String)
        lines.push(content_tag(:li){i})
      else
        lines.push(content_tag(:li){i.to_s})
      end
    end
    heading = ''.html_safe
    unless options[:header].blank?
      heading = content_tag(:h3, class: options[:header_class]){options[:header]}
    end
    heading + content_tag(:div, class: "view-list#{options[:class].blank? ? '' : ' ' + options[:class]}"){
      content_tag(options[:type], class: options[:list_class]){lines}
    }
  end
  
  # shortcut for standard PDF button for reports
  # yes it is amazing the lengths we go to to type a few less characters :)
  def pdf_button(*args)
    options = HashWithIndifferentAccess.new(id: nil)
    options.update(args.extract_options!)
    submit_tag('PDF', options)
  end
  
  # displays a photo on the show view
  def view_photo(object, photo, *args)
    options = args.extract_options!
    return '' if object.nil?
    photo_name = photo.to_s
    options[:size] ||= :small
    link_to(image_tag(object.send("#{photo_name}").url(options[:size]), class: 'view-photo'), object.send("#{photo_name}").url(:original))
  end
  
  # returns an string for thumbnails of the provided photos
  # assumed that the view has permission to see them (check this first!)
  def view_photos(header, photos, *args)
    options = args.extract_options!
    return "" if !(photos.is_a?(Array) || photos.is_a?(ActiveRecord::Relation)) || photos.empty?
    photo_tags = ''.html_safe
    photos.each do |p|
      if p.is_a?(InvestPhoto)
        p_url = url_for([p.investigation, p])
      elsif p.is_a?(OffensePhoto)
        p_url = url_for([p.offense, p])
      else
        p_url = url_for(p)
      end
      # handle various photo record types
      if p.is_a?(OffensePhoto)
        photo_tags << link_to(image_tag(p.crime_scene_photo.url(:thumb), style: 'margin-right:5px;'), p_url)
      elsif p.is_a?(InvestPhoto)
        photo_tags << link_to(image_tag(p.invest_photo.url(:thumb), style: 'margin-right:5px;'), p_url)
      elsif p.is_a?(Person)
        photo_tags << link_to(image_tag(p.side_mugshot.url(:thumb), style: 'margin-right:5px;'), p_url)
        photo_tags << link_to(image_tag(p.front_mugshot.url(:thumb), style: 'margin-right:5px;'), p_url)
      elsif p.is_a?(Evidence)
        photo_tags << link_to(image_tag(p.evidence_photo.url(:thumb), style: 'margin-right:5px;'), p_url)
      elsif p.is_a?(User)
        photo_tags << link_to(image_tag(p.user_photo.url(:thumb), style: 'margin-right:5px;'), p_url)
      end
    end
    if header.blank?
      heading_tag = "".html_safe
    else
      heading_tag = content_tag(:h3, class: options[:header_class]){header}
    end
    div_class = 'view-text'
    if options[:class]
      div_class << " #{options[:class]}"
    end
    heading_tag + content_tag(:div, class: div_class){photo_tags}
  end
  
  # breaks given text on \n (newline) then rejoins it using <br/>
  def newline_to_break(txt)
    return "" if !txt.is_a?(String) || txt.blank?
    safe_join(txt.split("\n"), tag(:br))
  end
  
  #### Report Sections
  
  def report_table(heading, *args, &block)
    options = args.extract_options!
    value = block_given? ? capture(&block) : args.first
    content_tag(:table, class: 'report'){
      content_tag(:tr){content_tag(:th, colspan: '2'){heading}} +
      value
    }
  end
  
  def report_row(heading, *args, &block)
    options = args.extract_options!
    value = block_given? ? capture(&block) : args.first
    if options[:tooltip]
      heading = with_tooltip(heading, options[:tooltip])
    end
    heading_class = 'heading'
    value_class = ''
    if options[:dangerous]
      heading_class << ' red'
      value_class << ' red'
    end
    content_tag(:tr, class: cycle('list-row-odd','list-row-even')){
      content_tag(:td, class: heading_class){heading} +
      content_tag(:td, class: value_class){value}
    }
  end
  
  #### Partials
  
  def list_partial(*args, &block)
    options = args.extract_options!
    # require a block
    return "" unless block_given?
    value = capture(&block)
    if options[:object]
      div_class = cycle('link-list-row-odd','link-list-row-even')
      on_click = "window.location='#{url_for(options[:object])}';return false;"
    elsif options[:function]
      div_class = cycle('link-list-row-odd','link-list-row-even')
      on_click = options[:function]
    else
      div_class = cycle('list-row-odd','list-row-even')
      on_click = nil
    end
    if options[:div_class]
      div_class = options[:div_class]
    end
    div_style = nil
    if options[:color]
      div_style = "color:#{options[:color]};"
    end
    content_tag(:div, class: div_class, style: div_style, onclick: on_click){value}
  end
  
  def list_partial_line(*args, &block)
    options = args.extract_options!
    classes = []
    div_class = nil
    if block_given?
      data = capture(&block)
    else
      if args.size >= 2
        header = args.first.to_s
        data = args.second.to_s
      else
        header = nil
        data = args.first.to_s
      end
      line_content = ''.html_safe
      if header
        line_content << content_tag(:b){header}
        line_content << ' '.html_safe
      end
      # only do the one_line thing if the string is not a safebuffer
      # that is prepared ahead of time. text fields will not be safebuffer
      # and will therefore get stripped of tags and converted to one-line
      if data.is_a?(ActiveSupport::SafeBuffer)
        line_content << data
      else
        line_content << one_line(data)
      end
      data = content_tag(:span, class: 'one-line'){line_content}
    end
    classes = []
    classes.push('first') if options[:first]
    classes.push(options[:color]) if options[:color]
    unless classes.empty?
      div_class = classes.join(' ')
    end
    content_tag(:div, class: div_class){data}
  end
  
  def list_partial_item(*args)
    options = args.extract_options!
    # if passed one string argument, its data
    # if passed two, first is header, second is data
    if args.size >= 2
      header = args.first.to_s
      data = args.second.to_s
    elsif args.size == 1
      header = nil
      data = args.first.to_s
    else
      # no strings supplied, invalid item
      return ''
    end
    item_content = ''.html_safe
    unless header.nil?
      item_content << content_tag(:b){header}
      item_content << ' '.html_safe
    end
    if options[:data_color]
      item_content << content_tag(:span, class: options[:data_color].to_s){data}
    else
      item_content << data
    end
    item_class = nil
    classes = []
    if options[:bold]
      classes.push('bold')
    end
    if options[:idtag]
      classes.push('id')
    end
    if options[:idtag_wide]
      classes.push('id-wide')
    end
    if options[:right]
      classes.push('right')
    end
    if options[:color]
      classes.push(options[:color].to_s)
    end
    if options[:class]
      classes.push(options[:class])
    end
    unless classes.empty?
      item_class = classes.join(' ')
    end
    item_style = nil
    if options[:money]
      item_style = 'text-align:right;width:100px;'
    end
    content_tag(:span, class: item_class, style: item_style){item_content}
  end
  
  def list_partial_id(name, id)
    if controller.action_name == 'global_search'
      list_partial_item(name, idtag: true) + list_partial_item('ID:', id)
    else
      list_partial_item(id, idtag: true)
    end
  end
end