module OffensesHelper
  # builds array of documents from an offense
  def html_document_list(offense)
    return "" unless offense.is_a?(Offense)
    # build a list of documents
    documents = []
    documents.push("Photographs".html_safe) if @offense.pictures?
    documents.push("Restitution Form".html_safe) if @offense.restitution_form?
    documents.push("Victim Notification Form".html_safe) if @offense.victim_notification?
    documents.push("Permission To Search Form".html_safe) if @offense.perm_to_search?
    documents.push("Misdemeanor Summons".html_safe) if @offense.misd_summons?
    documents.push("48 Hour Form".html_safe) if @offense.fortyeight_hour?
    documents.push("Medical Release Form".html_safe) if @offense.medical_release?
    offense.arrests.each do |a|
      documents.push("Arrest #{a.id}: #{AppUtils.show_person(a.person)}".html_safe)
    end
    offense.witnesses.each do |w|
      documents.push("Witness Statement: #{AppUtils.show_person(w)}".html_safe)
    end
    offense.citations.each do |c|
      documents.push("Citation: ##{sanitize c.ticket_no}: #{AppUtils.show_person(c.person)}, For: #{sanitize c.charge_summary}".html_safe)
    end
    offense.forbids.each do |f|
      txt = "Forbidden To Enter -- By: #{sanitize f.complainant} For: #{sanitize f.sort_name}".html_safe
      if f.recall_date?
        txt << "Recalled: #{AppUtils.format_date(f.recall_date)}".html_safe
      elsif f.signed_date?
        txt << "Served: #{AppUtils.format_date(f.signed_date)}".html_safe
      else
        txt << content_tag(:span, class: 'red'){"NOT YET SERVED"}
      end
    end
    return "" if documents.empty?
    list_tags = "".html_safe
    documents.each do |d|
      list_tags << content_tag(:li){d}
    end
    content_tag(:ul){list_tags}
  end

  def offense_links(object)
    safe_join(object.offenses.collect{|o| can?(:view_offense) ? link_to(o.id.to_s, o) : o.id.to_s}, ', '.html_safe)
  end
end