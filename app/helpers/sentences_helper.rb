module SentencesHelper
  # examines a sentence (or probation) and returns an array of all special
  # conditions of sentence
  def sentence_conditions(object)
    return [] unless object.is_a?(Sentence) || object.is_a?(Probation)
    conditions = []
    # build list of sentence conditions
    conditions = []
    if (object.is_a?(Probation) && object.credit_served?) || (object.is_a?(Sentence) && object.credit_time_served?)
      conditions.push("Credit for Time Served")
    end
    if (object.is_a?(Sentence) && (object.community_service_days? || object.community_service_hours?)) || (object.is_a?(Probation) && (object.service_days? || object.service_hours?))
      txt = "".html_safe
      if object.is_a?(Sentence)
        if object.community_service_days?
          txt << "Community Service: #{object.community_service_days} days"
        else
          txt << "Community Service: #{object.community_service_hours} hours"
        end
      else
        if object.service_days?
          txt << "Community Service: #{object.service_days} days "
        else
          txt << "Community Service: #{object.service_hours} hours "
        end
        if object.service_served?
          txt << "(Served: #{object.service_served}) "
        else
          txt << "(Served: 0) "
        end
        if object.community_service_complete?
          txt << content_tag(:span, class: 'green'){"[Completed]"}
        else
          txt << content_tag(:span, class: 'red'){"[Incomplete]"}
        end
      end
      conditions.push(txt)
    end
    if (object.is_a?(Sentence) && object.substance_abuse_program?) || (object.is_a?(Probation) && object.sap?)
      txt = "Substance Abuse Program ".html_safe
      if object.is_a?(Probation)
        if object.sap_complete?
          txt << content_tag(:span, class: 'green'){"[Completed]"}
        else
          txt << content_tag(:span, class: 'red'){"[Incomplete]"}
        end
      end
      conditions.push(txt)
    end
    if (object.is_a?(Sentence) && object.driver_improvement?) || (object.is_a?(Probation) && object.driver?)
      txt = "Driver Improvement Program ".html_safe
      if object.is_a?(Probation)
        if object.driver_complete?
          txt << content_tag(:span, class: 'green'){"[Completed]"}
        else
          txt << content_tag(:span, class: 'red'){"[Incomplete]"}
        end
      end
      conditions.push(txt)
    end
    if object.probation_fees?
      txt = "".html_safe
      txt << "Pay monthly probation fees of #{number_to_currency(object.probation_fees)}."
      conditions.push(txt)
    end
    if (object.is_a?(Sentence) && object.anger_management?) || (object.is_a?(Probation) && object.anger?)
      txt = "Anger Management Program ".html_safe
      if object.is_a?(Probation)
        if object.anger_complete?
          txt << content_tag(:span, class: 'green'){"[Completed]"}
        else
          txt << content_tag(:span, class: 'red'){"[Incomplete]"}
        end
      end
      conditions.push(txt)
    end
    if (object.is_a?(Sentence) && object.substance_abuse_treatment?) || (object.is_a?(Probation) && object.sat?)
      txt = "Substance Abuse Treatment ".html_safe
      if object.is_a?(Probation)
        if object.sat_complete?
          txt << content_tag(:span, class: 'green'){"[Completed]"}
        else
          txt << content_tag(:span, class: 'red'){"[Incomplete]"}
        end
      end
      conditions.push(txt)
    end
    if (object.is_a?(Sentence) && object.random_drug_screens?) || (object.is_a?(Probation) && object.random_screens?)
      conditions.push("Random Drug Screens".html_safe)
    end
    if object.no_victim_contact?
      conditions.push("No Contact With Victim".html_safe)
    end
    if object.restitution_amount?
      txt = "".html_safe
      txt << "Must Pay #{number_to_currency(object.restitution_amount)} Restitution "
      if object.restitution_date?
        txt << "By: #{AppUtils.format_date(object.restitution_date)}"
      end
      conditions.push(txt)
    end
    if object.dare_amount?
      txt = "".html_safe
      txt << "Must Pay #{number_to_currency(object.dare_amount)} to D.A.R.E. Fund"
      conditions.push(txt)
    end
    if object.idf_amount?
      txt = "".html_safe
      txt << "Must Pay #{number_to_currency(object.idf_amount)} to Indigent Defender Fund (IDF)"
      conditions.push(txt)
    end
    if (object.is_a?(Sentence) && (object.art_893? || object.art_894? || object.art_895?)) || (object.is_a?(Probation) && (object.art893? || object.art894? || object.art895?))
      txt = "Article(s): ".html_safe
      if (object.is_a?(Sentence) && object.art_893?) || (object.is_a?(Probation) && object.art893?)
        txt << "893 "
      end
      if (object.is_a?(Sentence) && object.art_894?) || (object.is_a?(Probation) && object.art894?)
        txt << "894 "
      end
      if (object.is_a?(Sentence) && object.art_895?) || (object.is_a?(Probation) && object.art895?)
        txt << "895"
      end
      conditions.push(txt)
    end
    conditions
  end
end