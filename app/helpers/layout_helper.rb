# helpers used in layouts
module LayoutHelper
  def application_menu
    menu_items = [menu_item("Home", root_path)]
    
    admin_items = []
    admin_items.push(menu_item("Activities", activities_path, :admin))
    config_items = []
    config_items.push(menu_item("Main", site_main_path, :admin))
    config_items.push(menu_item("Dispatch", site_dispatch_path, :admin))
    config_items.push(menu_item("Enforcement", site_enforcement_path, :admin))
    config_items.push(menu_item("Detective", site_detective_path, :admin))
    config_items.push(menu_item("Jail", site_jail_path, :admin))
    config_items.push(menu_item("Court", site_court_path, :admin))
    config_items.push(menu_item("Admin", site_admin_path, :admin))
    admin_items.push(menu_section('Configuration', config_items))
    admin_items.push(menu_item("Database", databases_path, :view_database))
    admin_items.push(menu_item("Groups", groups_path, :view_group))
    admin_items.push(menu_item("Jails", jails_path, :view_jail))
    admin_items.push(menu_item("Message Log", message_logs_path, :admin))
    admin_items.push(menu_item("Users", users_path, :view_user))
    
    menu_items.push(menu_section('Admin', admin_items))
    
    menu_items.push(menu_item("People", people_path, :view_person))
    
    menu_items.push(menu_item("Contacts", contacts_path, :view_contact))
    
    menu_items.push(menu_item("Forums", forums_path, :view_forum))
    
    court_items = []
    court_items.push(menu_item("Dispositions", dispositions_path, :view_court))
    court_items.push(menu_item("Dockets", dockets_path, :view_court))
    court_items.push(menu_item("Bonds", bonds_path, :view_bond))
    court_items.push(menu_item("Garnishments", garnishments_path, :view_civil))
    court_items.push(menu_item("Papers", papers_path, :view_civil))
    court_items.push(menu_item("Probation", probations_path, :view_probation))

    menu_items.push(menu_section('Court', court_items))
    
    dispatch_items = []
    dispatch_items.push(menu_item("Calls", calls_path, :view_call))
    dispatch_items.push(menu_item("Forbids", forbids_path, :view_forbid))
    if SiteConfig.show_history
      dispatch_items.push(menu_item("History", histories_path, :view_history))
    end
    dispatch_items.push(menu_item("Pawns", pawns_path, :view_pawn))
    dispatch_items.push(menu_item("Warrants", warrants_path, :view_warrant))

    menu_items.push(menu_section('Dispatch', dispatch_items))
    
    patrol_items = []
    patrol_items.push(menu_item("Case Notes", case_notes_path, :view_case))
    patrol_items.push(menu_item("Citations", citations_path, :view_citation))
    patrol_items.push(menu_item("Damages", damages_path, :view_damage))
    patrol_items.push(menu_item("Evidence", evidences_path, :view_evidence))
    patrol_items.push(menu_item("Offense", offenses_path, :view_offense))
    patrol_items.push(menu_item('Patrols', patrols_path, :view_offense))
    patrol_items.push(menu_item("Stolen", stolens_path, :view_stolen))
    patrol_items.push(menu_item("Transports", transports_path, :view_transport))

    menu_items.push(menu_section('Patrol', patrol_items))

    detective_items = []
    detective_items.push(menu_item("Investigation", investigations_path, :view_investigation))
    detective_items.push(menu_item("Crimes", invest_crimes_path, :view_investigation))
    detective_items.push(menu_item("Criminals", invest_criminals_path, :view_investigation))
    detective_items.push(menu_item("Inv. Photos", invest_photos_path, :view_investigation))
    detective_items.push(menu_item("Inv. Reports", invest_reports_path, :view_investigation))
    detective_items.push(menu_item("Vehicles", invest_vehicles_path, :view_investigation))
    
    menu_items.push(menu_section('Detective', detective_items))
    
    jail_items = []
    Jail.all.each do |j|
      jail_items.push(menu_item(j.acronym, jail_bookings_path(j), ["view_jail_#{j.permset}".to_sym, :view_booking]))
    end
    jail_items.push(menu_item("Arrests", arrests_path, :view_arrest))
    jail_items.push(menu_item("DOC Sentences", docs_path, :view_booking))
    
    menu_items.push(menu_section('Jail', jail_items))
    
    util_items = []
    util_items.push(menu_item("Announcements", announcements_path, :view_announcement))
    util_items.push(menu_item("Events", events_path, :view_event))
    util_items.push(menu_item("FAX Covers", fax_covers_path))
    util_items.push(menu_item("Links", external_links_path, :view_link))
    util_items.push(menu_item("Statutes", statutes_path, :view_statute))
    util_items.push(menu_item("Tools", tools_path))
    
    menu_items.push(menu_section('Utils', util_items))
    
    user_items = []
    user_items.push(menu_item("Preferences", my_preferences_path))
    unless current_user.contact.nil?
      user_items.push(menu_item("Profile", current_user.contact, :view_contact))
    end
    user_items.push(menu_item("New Password", change_password_path))
    user_items.push(menu_item("New Message", new_user_message_path(user_id: session[:user])))
    if SiteConfig.allow_user_photos
      user_items.push(menu_item("User Photo", my_photo_path))
    end
    user_items.push(menu_item("Inbox", inbox_user_messages_path(user_id: session[:user])))
    user_items.push(menu_item("Outbox", outbox_user_messages_path(user_id: session[:user])))
    user_items.push(menu_item("Trashbin", trashbin_user_messages_path(user_id: session[:user])))
    
    menu_items.push(menu_section('User', user_items))
    
    link_items = []
    link_items.push(menu_item("View All", all_external_links_path))
    elinks = ExternalLink.for_front_page.by_short_name.all
    unless elinks.empty?
      elinks.sort{|a,b| a.short_name.downcase <=> b.short_name.downcase}.each do |l|
        link_items.push(menu_item(l.short_name, l.url))
      end
    end
    
    menu_items.push(menu_section('Links', link_items))
    
    # context sensitive help
    if File.exists?("#{Rails.root}/public/manual/#{@help_page}.html")
      help_url = url_for("#{ENV['RAILS_RELATIVE_URL_ROOT']}/manual/" + @help_page + '.html')
    else
      help_url = url_for("#{ENV['RAILS_RELATIVE_URL_ROOT']}/manual/index.html")
    end
    menu_items.push(menu_item('Help', help_url, target: '_blank'))
    
    # logout button, if logged in
    if session[:user]
      menu_items.push(menu_item('Logout', logout_url))
    end
    
    content_tag(:ul, id: 'navmenu'){safe_join(menu_items.compact, "\n".html_safe)}
  end
  
  def menu_section(name, items=[])
    if items.compact.empty?
      nil
    else
      content_tag(:li){
        link_to(name, '#') + "\n".html_safe +
        content_tag(:ul){safe_join(items.compact, "\n".html_safe)}
      }
    end
  end
  
  def menu_item(name, path, *args)
    options = args.extract_options!
    permission = args.shift
    if can?(permission) || permission.nil?
      content_tag(:li, link_to(name, path, options))
    else
      nil
    end
  end
  
  def page_links
    return nil unless session[:user]
    return nil if @page_links.nil? || !@page_links.is_a?(Array) || @page_links.empty?
    link_list = []
    @page_links.each do |l|
      if l.is_a?(Array)
        if l[0].is_a?(Array)
          obj = l[0].last
          obj_url = url_for(l[0])
        elsif l.last.respond_to?('descends_from_active_record') && l.last.descends_from_active_record?
          obj = l.last
          obj_url = url_for(l)
        else
          case l[0].class.name
          when 'Fixnum'
            obj = Contact.find_by_id(l[0])
          when 'User'
            obj = l[0].contact
          else
            obj = l[0]
          end
          obj_url = url_for(l[0])
        end
      else
        case l.class.name
        when 'Fixnum'
          obj = Contact.find_by_id(l)
        when 'User'
          obj = l.contact
        else
          obj = l
        end
        obj_url = url_for(l)
      end
      next if obj.nil?
      if obj.is_a?(Jail)
        txt = obj.acronym
      else
        if !l[1].blank? && l[1].is_a?(String)
          txt = l[1].titleize.gsub(/\s+/,'')
        else
          txt = obj.class.name
        end
        txt = h(txt)
      end
      if obj.class.name.constantize.respond_to?("base_perms")
        permission = can?(obj.class.name.constantize.base_perms)
      else
        permission = true
      end
      unless check_privacy(obj)
        permission = false
      end
      if permission
        link_list.push(link_to(txt, obj_url))
      end
    end
    return "" if link_list.empty?
    button_list = safe_join(link_list, space(2) + '&#8226;'.html_safe + space(2))
    content_tag(:div, id: 'page-links'){
      content_tag(:div, id: 'page-links-links'){button_list} +
      content_tag(:div, id: 'page-links-text'){
        content_tag(:span){"Related Pages:"}
      }
    }
  end

  # Used by application layout to present a page title if @page_title 
  # instance variable is set.
  def page_title(id=nil)
    if !@page_title.nil?
      # added this mess to show a legacy icon in the page title on show views
      legacy_icon = "".html_safe
      if controller.action_name == 'show'
        record_variable = controller.controller_name.classify.downcase
        if record = instance_variable_get("@#{record_variable}")
          if record.respond_to?("legacy?") && record.legacy?
            legacy_icon = icon("legacy")
          end
        end
      end
      # added this to add legacy tooltip to page title on index views
      if controller.action_name == 'index'
        begin
          current_model = controller.controller_name.singularize.classify.constantize
        rescue NameError
          current_model = nil
        end
        if !current_model.nil? && current_model.column_names.include?('legacy')
          tt = %(#{controller.controller_name.titleize} database contains
            information imported from older computer systems. These imported
            records are marked with #{icon('legacy', height: 14)} or 
            <i>[Legacy]</i>.  Records marked like this may not fully meet data
            requirements for this system. If you edit one of these records, you
            may be required to fix incorrect information or supply missing
            information before you can submit your changes.)
          @page_title = with_tooltip(@page_title, tt)
        end
      end
      content_tag(:h1, class: 'page-title'){emoticonify(h(@page_title)) + " " + legacy_icon}
    end
  end
  
  def flash_message
    notices = []
    flash.keys.each do |key|
      case key
      when 'notice'
        notices.push(content_tag(:div, id: 'flash-notice'){flash.notice.html_safe})
      when 'alert'
        notices.push(content_tag(:div, id: 'flash-alert'){flash.alert.html_safe})
      end
    end
    if notices.empty?
      ''
    else
      content_tag(:div){safe_join(notices,"\n".html_safe)}
    end
  end
  
  # this displays zero or more notices automatically depending on whether
  # the current record suggests it. Currently notifies if record is disabled
  # or private or inactive.
  def view_notices
    # skip this for "edit" and "merge" actions
    return '' if controller.action_name == 'edit' || controller.action_name == 'merge'
    record_variable = controller.controller_name.singularize
    record = instance_variable_get("@#{record_variable}")
    notices = []
    if record.respond_to?('private?') && record.private?
      notices.push(['<b>Private</b>: Only you can see this information.','blue'])
    end
    if record.respond_to?('disabled?') && record.disabled?
      notices.push(['This record is <b>Disabled</b>.','red'])
    end
    if record.respond_to?('active?') && !record.active?
      notices.push(['This record is <b>Inactive</b>.','red'])
    end
    if notices.empty?
      ''
    else
      txt = ''.html_safe
      notices.each do |n|
        txt << content_tag(:div, class: n[1]){
          content_tag(:div, class: 'text'){n[0].html_safe}
        }
      end
      content_tag(:div, id: 'page-notices'){txt}
    end
  end
end