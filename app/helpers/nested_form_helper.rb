module NestedFormHelper
  # Adds link to allow removing form lines for new records.
  # To prevent delete of existing records, add keep_existing: true
  def link_to_remove_fields(f, *args)
    options = args.extract_options!
    unless !f.form_object.new_record? && options[:keep_existing]
      f.hidden_field(:_destroy) + link_to_function(icon('delete'), "remove_fields($(this))", class: 'remove-link')
    end
  end
  
  def link_to_add_fields(f, association, partial_path='')
    new_object = f.object.class.reflect_on_association(association).klass.new
    if new_object.respond_to?('created_by')
      new_object.created_by = session[:user]
      new_object.updated_by = session[:user]
    end
    partial = association.to_s.singularize + "_fields"
    unless partial_path.blank?
      partial = partial_path + '/' + partial
    end
    fields = f.fields_for(association, new_object, child_index: "new_#{association}") do |builder|
      render(partial, f: builder)
    end
    link_to_function(icon('add'), "add_fields(\"#{association}\", \"#{escape_javascript(fields)}\")", class: 'add-link')
  end
  
  # creates a fields_for section for a nested association
  # if partial is in another directory, specify with partial_path: 'other directory'
  # To only get new records from the association, use only_new: true
  def nested_form_for(form, association, *args)
    options = args.extract_options!
    if options[:partial_path]
      partial_path = options[:partial_path]
    end
    fields = form.fields_for(association) do |attributes|
      next if options[:new_only] && !attributes.object.new_record?
      render "#{partial_path.blank? ? '' : partial_path + '/'}#{association.to_s.singularize}_fields", f: attributes
    end
    content_tag(:div, id: "#{association.to_s}"){fields}
  end
  
  # creates a table listing options to choose from an association
  def nested_chooser_for(form, association, *args)
    options = args.extract_options!
    if options[:parent]
      parent_object = options[:parent]
    else
      parent_object = form.object
    end
    prefix = form.object.class.name.tableize.singularize
    assoc_reflection = form.object.class.reflect_on_association(association)
    assoc_name = (assoc_reflection.options[:source] || assoc_reflection.name).to_s.singularize
    field_name = "#{form.object.class.name.tableize.singularize}[linked_#{assoc_reflection.name}][]"
    partial_name = "#{assoc_name.pluralize}/#{assoc_name}_info"
    join_association = assoc_reflection.options[:through].to_s
    txt = ''.html_safe
    title = assoc_reflection.name.to_s.titleize
    if options[:tooltip]
      title = with_tooltip(title, options[:tooltip])
    end
    for record in parent_object.send(join_association).includes(assoc_name.to_sym) do
      partial_contents = render(partial: partial_name, object: record.send(assoc_name))
      txt << content_tag(:tr){
        content_tag(:td){
          check_box_tag(field_name, record.id, form.object.send(join_association).include?(record), id: nil)
        } +
        content_tag(:td, style: 'width:100%;'){partial_contents}
      }
    end
    # now do the header and table
    content_tag(:h4){title} +
    content_tag(:table, class: 'special', style: 'width:93%;margin-left:50px;'){txt}
  end
end