# Messages Helper
module MessagesHelper
  # Provides notice of any user messages.  Used in the application layout.
  def mail_notice
    if session[:user]
      msgcnt = current_user.messages.inbox.count
      if msgcnt > 0
        msg = msgcnt == 1 ? " message" : " messages"
        path = inbox_user_messages_path(user_id: session[:user])
        link_to(icon("inbox", height: 16), path)
      else
        nil
      end
    else
      nil
    end
  end

  # Dynamic label for the sender/receiver column in the messages.rhtml view
  def sender_or_receiver_label
    if params[:action] == "outbox"
      "To"
      # Used for both inbox and trashbin
    else
      "From"
    end
  end

  # Checkbox for marking a message for deletion
  def delete_check_box(message)
    check_box_tag('selected[]', message.id, false, id: nil)
  end

  # Link to view the message
  def link_to_message(message)
    link_to "#{h(subject_and_status(message))}", user_message_path(current_user, message)
  end

  # url to view the message
  def url_to_message(message)
    url_for user_message_path(current_user, message)
  end

  # Dynamic data for the sender/receiver column in the messages.rhtml view
  def sender_or_receiver(message)
    if params[:action] == "outbox"
      message.receiver.display_name
      # Used for both inbox and trashbin
    else
      message.sender.display_name
    end
  end

  # Pretty format for message.subject which appeads the status
  # (Deleted/Unread)
  def subject_and_status(message)
    if message.receiver_deleted?
      message.subject + " (Deleted)" 
    elsif message.read_at.nil?
      message.subject + " (Unread)"  
    else 
      message.subject
    end
  end
end
