module InvestReportsHelper
  def invest_report_links(invest_reports)
    return '' unless invest_reports.is_a?(Array) && can?(:view_investigations)
    txt = []
    invest_reports.each do |r|
      next unless r.is_a?(InvestReport)
      if r.private? && r.owned_by != session[:user]
        txt.push(r.id.to_s.html_safe)
      else
        txt.push(link_to r.id.to_s, r)
      end
    end
    if txt.empty?
      'None'
    else
      safe_join(txt, ', ')
    end
  end
end