module RootHelper
  # Generates an HTML calendar for the month containing the indicated date
  # with optional text displayed to the left (previous) and right (next) of
  # the date name.  The current date will be highlighted, and all calendar
  # events for the month will be shown in the appropriate days.
  def build_calendar
    return nil unless session[:calendar].is_a?(Date)
    prev_month = content_tag(:div, class: 'calNav', onclick: "getCalendar('#{get_calendar_path}', 'previous_month')"){'&nbsp;&lt;&nbsp;'.html_safe}
    next_month = content_tag(:div, class: 'calNav', onclick: "getCalendar('#{get_calendar_path}', 'next_month')"){"&nbsp;&gt;&nbsp;".html_safe}
    prev_year = content_tag(:div, class: 'calNav', onclick: "getCalendar('#{get_calendar_path}', 'previous_year')"){"&nbsp;&lt;&lt;&nbsp;".html_safe}
    next_year = content_tag(:div, class: 'calNav', onclick: "getCalendar('#{get_calendar_path}', 'next_year')"){"&nbsp;&gt;&gt;&nbsp;".html_safe}
    this_month = content_tag(:div, class: 'calNav', onclick: "getCalendar('#{get_calendar_path}', 'this_month')"){"Today"}
    navigation = content_tag(:span){
      prev_year + space(1) + prev_month + space(1) + this_month + space(1) + next_month + space(1) + next_year
    }
    events = Event.get_current_events(session[:calendar], session[:user])
    calendar({year: session[:calendar].year, month: session[:calendar].month, previous_month_text: "", next_month_text: navigation}) do |d|
      cell_text = content_tag(:div, class: 'dayNumber', onclick: "window.location='#{new_event_path(start_date: d.to_s(:us))}';return false;"){d.mday.to_s}
      cell_attrs = {class: 'day'}
      item_text = ''.html_safe
      events.each do |e|
        if e[0] == d
          if e[1].details?
            item_text << content_tag(:li, class: 'eventItem'){
              content_tag(:span, class: (e[1].private? ? 'eventPrivateItem' : nil)){with_tooltip(e[1].name, e[1].details)}
            }
          else
            item_text << content_tag(:li, class: 'eventItem'){
              content_tag(:span, class: (e[1].private? ? 'eventPrivateItem' : nil)){e[1].name}
            }
          end
          cell_attrs[:class] = 'specialDay'
        end
      end
      cell_text << content_tag(:ul){item_text}
      [cell_text, cell_attrs]
    end
  end
end