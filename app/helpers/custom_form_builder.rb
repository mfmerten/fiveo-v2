# thanks to Zachary Wright for the basis of this
# http://www.likeawritingdesk.com/posts/very-custom-form-builders-in-rails

# Note that convention here is that any method prefixed with edit_row_ 
# produces a complete edit form row (header and data) in div wrappers
class CustomFormBuilder < ActionView::Helpers::FormBuilder
  
  # create wrapped copies of the standard form helpers with automatic labeling
  def self.create_tagged_field(method_name)
    define_method(("edit_row_" + method_name.sub(/_field$/,'')).to_sym) do |object_label, *args|
      options = args.extract_options!
      field = @template.content_tag(:span){self.send(method_name, object_label, *(args << options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)}))}
      wrap_edit_row(field, object_label, options)
    end
  end

  # create new methods for all field helpers *EXCEPT* phone_field (see below)
  ['text_field','password_field','file_field'].each do |name|
    create_tagged_field(name)
  end
  
  # expose object as a method
  def form_object
    @object
  end
  
  # create custom builders for often used field combinations
  # add tag text to checkbox
  def check_box(object_label,options={},checked_value=1,unchecked_value=0)
    tag_text = options[:tag] || "Yes"
    options.delete(:tag)
    tooltip = ""
    if options[:tooltip]
      tag_text = @template.with_tooltip(tag_text, options[:tooltip])
      options.delete(:tooltip)
    end
    field_tag = super(object_label,options,checked_value,unchecked_value)
    field_tag + tag_text.html_safe
  end
  
  # wrapped check_box field
  def edit_row_check_box(object_label,options={},checked_value=1,unchecked_value=0)
    cb_field = @template.content_tag(:span){check_box(object_label,options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)},checked_value,unchecked_value)}
    unless options[:header]
      options[:header] = object_label.to_s.titleize + '?'
    end
    wrap_edit_row(cb_field, object_label, options)
  end
  
  # wrapped collection of check_box fields
  # use like f.edit_row_check_boxes([label,tag],[label,tag],[label,tag],header: 'Options:')
  def edit_row_check_boxes(*args)
    options = args.extract_options!
    first_object_label = args.first[0]
    field = ''.html_safe
    args.each do |object_label, tag_name, *args|
      opts = args.extract_options!
      field << @template.content_tag(:span){check_box(object_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)}.update({tag: tag_name}.merge(opts)))}
    end
    wrap_edit_row(field, first_object_label, options)
  end
  
  # handled separately because of select options
  # requires get_opts option type as second argument
  def edit_row_select(object_label, getopts, *args)
    options = args.extract_options!
    jail = nil
    nillable = true
    unless options[:allow_nil].nil?
      nillable = options[:allow_nil]
      options.delete('allow_nil')
    end
    unless options[:jail_id].nil?
      jail = options[:jail_id]
      options.delete('jail_id')
    end
    field = @template.content_tag(:span){select(object_label, @template.get_opts(getopts, allow_nil: nillable, jail_id: jail))}
    wrap_edit_row(field, object_label, options)
  end
  
  def phone_field(object_label, *args)
    # note, this one in particular overrides the built-in phone_field 
    # helper witout calling super - does not add html5 attributes!
    options = args.extract_options!
    if options[:class].nil?
      options[:class] = 'phone_mask'
    else
      options[:class] = options[:class] + ' phone_mask'
    end
    options[:size] = 12
    text_field(object_label, options)
  end

  # text field with input mask for phones
  # includes, type, phone, notes and unlisted flags if object responds to them
  def edit_row_phone(object_label, *args)
    options = args.extract_options!
    phone_label = object_label
    type_label = "#{object_label}_type"
    unlisted_label = "#{object_label}_unlisted"
    combined_fields = ''.html_safe
    if @object.respond_to?(type_label)
      combined_fields << @template.content_tag(:span){select(type_label, @template.get_opts('Phone Type', allow_nil: true), options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)})}
    end
    if @object.respond_to?(phone_label)
      combined_fields << @template.content_tag(:span){phone_field(object_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)})}
    end
    if @object.respond_to?(unlisted_label)
      combined_fields << @template.content_tag(:span){check_box(unlisted_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)}.update({tag: 'Unlisted'}))}
    end
    wrap_edit_row(combined_fields, object_label, options)
  end

  def ssn_field(object_label, *args)
    options = args.extract_options!
    if options[:class].nil?
      options[:class] = 'ssn_mask'
    else
      options[:class] = options[:class] + ' ssn_mask'
    end
    options[:size] = 11
    text_field(object_label, options)
  end
  
  # text field with input mask for soc sec no
  def edit_row_ssn(object_label, *args)
    options = args.extract_options!
    field = @template.content_tag(:span){ssn_field(object_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)})}
    wrap_edit_row(field, object_label, options)
  end
  
  # text field right aligned and formatted for 2 decimal places
  def money_field(object_label, *args)
    options = args.extract_options!
    options[:size] = 12
    options[:placeholder] = '9999.99'
    options[:style] = 'text-align:right;'
    options[:value] = @template.number_with_precision(@object.send(object_label), precision: 2)
    "$".html_safe + text_field(object_label, options)
  end
  
  # money field wrapped
  def edit_row_money(object_label, *args)
    options = args.extract_options!
    field = money_field(object_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)})
    wrap_edit_row(field, object_label, options)
  end
  
  # generates a date field for edit forms
  def date_field(object_label,*args)
    options = args.extract_options!
    date_field_value = @object.send(object_label)
    if date_field_value.nil?
      options[:value] = ""
    elsif date_field_value.is_a?(String)
      options[:value] = date_field_value
    elsif date_field_value.is_a?(Date)
      options[:value] = date_field_value.to_s(:us)
    else
      options[:value] = date_field_value.to_s
    end
    options[:size] = 10
    options[:class] = 'date_mask'
    text_field(object_label, options)
  end
  
  # generates a datetime field for edit forms
  def datetime_field(object_label, *args)
    options = args.extract_options!
    datetime_field_value = @object.send(object_label)
    if datetime_field_value.nil?
      options[:value] = ''
    elsif datetime_field_value.is_a?(String)
      options[:value] = datetime_field_value
    elsif datetime_field_value.is_a?(Time) || datetime_field_value.is_a?(DateTime)
      options[:value] = datetime_field_value.to_s(:us_nozone)
    else
      options[:value] = datetime_field_value.to_s
    end
    options[:size] = 16
    options[:class] = 'date_time_mask'
    text_field(object_label, options)
  end
  
  # generates a wrapped date entry on edit forms
  def edit_row_date(object_label,*args)
    options = args.extract_options!
    field = @template.content_tag(:span){date_field(object_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)})}
    wrap_edit_row(field, object_label, options)
  end
  
  # generates a wrapped datetime entry on edit forms
  def edit_row_datetime(object_label,*args)
    options = args.extract_options!
    field = @template.content_tag(:span){datetime_field(object_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)})}
    wrap_edit_row(field, object_label, options)
  end
  
  # generates a time field for edit forms
  def time_field(object_label,*args)
    options = args.extract_options!
    if options[:class].nil?
      options[:class] = 'time_mask'
    else
      options[:class] = options[:class] + ' time_mask'
    end
    options[:size] = 5
    text_field(object_label, options)
  end
  
  # generates a wrapped time entry on edit forms
  def edit_row_time(object_label,*args)
    options = args.extract_options!
    field = @template.content_tag(:span){time_field(object_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)})}
    wrap_edit_row(field, object_label, options)
  end
  
  # generates a date and time entry on edit forms
  def edit_row_date_time(date_label,time_label,*args)
    options = args.extract_options!
    date_div = @template.content_tag(:span){date_field(date_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)})}
    time_div = @template.content_tag(:span){time_field(time_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)})}
    wrap_edit_row(date_div + time_div, date_label, options)
  end
  
  # generates a set of fields used for selecting or entering a person contact
  def officer_field(object_label, *args)
    options = args.extract_options!
    id_label = (object_label.to_s + "_id").to_sym
    unit_label = (object_label.to_s + "_unit").to_sym
    badge_label = (object_label.to_s + "_badge").to_sym
    # possibly includes get_opts option_type as first argument after label
    if args.first.is_a?(String)
      getopts = args.shift
    else
      getopts = "Officers"
    end
    # set up select control
    label_id = "#{@object_name}_#{object_label}"
    id_label_id = "#{@object_name}_#{id_label}"
    unit_label_id = "#{@object_name}_#{unit_label}"
    badge_label_id = "#{@object_name}_#{badge_label}"
    onchange = "contactRowToggle('#{id_label_id}','#{label_id}','#{unit_label_id}','#{badge_label_id}')"
    select_tag = @template.content_tag(:span){select(id_label, @template.get_opts(getopts, allow_nil: true), {}, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)}.update({onchange: onchange}))}
    icon_tag = @template.content_tag(:span){@template.icon('forward', height: 16)}
    unit_tag = @template.content_tag(:span){text_field(unit_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)}.update({size: 6, placeholder: 'Unit'}))}
    badge_tag = @template.content_tag(:span){text_field(badge_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)}.update({size: 6, placeholder: 'Badge'}))}
    name_tag = @template.content_tag(:span){text_field(object_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)}.update({size: 25, placeholder: 'Full Name'}))}
    select_tag + icon_tag + badge_tag + unit_tag + name_tag
  end
  
  # generates a set of fields used for selecting or entering a person contact
  def edit_row_officer(object_label, *args)
    options = args.extract_options!
    field_tag = officer_field(object_label, options)
    wrap_edit_row(field_tag, object_label, options)
  end
  
  # generates a set of fields for agency/business input
  # basically same as app_contact_field w/o unit and badge
  def agency_field(object_label, *args)
    options = args.extract_options!
    id_label = (object_label.to_s + "_id").to_sym
    # possibly includes get_opts option_type as first argument after label
    if args.first.is_a?(String)
      getopts = args.shift
    else
      getopts = "Agencies"
    end
    select_tag = @template.content_tag(:span){select(id_label, @template.get_opts(getopts, allow_nil: true), {}, options.update({class: 'agency-select', id: nil}))}
    icon_tag = @template.content_tag(:span){@template.icon('forward', height: 16)}
    name_tag = @template.content_tag(:span){text_field(object_label, options.update({size: 25, placeholder: 'Full Name', class: 'agency-name'}))}
    select_tag + icon_tag + name_tag
  end
  
  # wraps agency_field in divs for edit form row
  def edit_row_agency(object_label, *args)
    options = args.extract_options!
    field_tag = agency_field(object_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)})
    wrap_edit_row(field_tag, object_label, options)
  end
  
  def person_field(object_label, *args)
    options = args.extract_options!
    if options[:alias_ok]
      validate_js = "validateInfo('#{@template.people_url}', $(this), true); return false;"
    else
      validate_js = "validateInfo('#{@template.people_url}', $(this)); return false;"
    end
    options.delete(:alias_ok)
    id_field = @template.content_tag(:span){text_field(object_label, options.dup.update({size: 8, placeholder: 'ID', onblur: validate_js, class: 'person-id'}))}
    name_value = ""
    id_value = @object.send(object_label)
    unless id_value.nil?
      person = @object.send(object_label.to_s.sub(/_id$/,''))
      unless person.nil?
        name_value = person.sort_name
      end
    end
    name_field = @template.content_tag(:span){@template.text_field_tag('name', name_value, options.dup.update({size: 25, placeholder: 'Full Name', class: 'person-name', id: nil}))}
    find_button = @template.content_tag(:span){@template.button_to_function('Find By Name', "personChoose('#{@template.choose_people_url}',$(this))")}
    id_field + name_field + find_button
  end
  
  # provides a person id/name field combo with lookup and validate functionality
  def edit_row_person(object_label, *args)
    options = args.extract_options!
    fields = person_field(object_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)})
    wrap_edit_row(fields, object_label, options.reject{|k,v| [:alias_ok].include?(k)})
  end
  
  # provides an address field combo
  def address_field(street1_label, street2_label, city_label, state_label, zip_label, *args)
    options = args.extract_options!
    # provide only the fields for non-nil labels
    combined_field = ''.html_safe
    unless street1_label.nil?
      street_text = street2_label.nil? ? "Street" : "Street Line 1"
      combined_field << @template.content_tag(:span){text_field(street1_label, options.dup.update({size: 40, placeholder: street_text}))} + @template.tag('br')
    end
    unless street2_label.nil?
      street_text = street1_label.nil? ? "Street" : "Street Line 2"
      combined_field << @template.content_tag(:span){text_field(street2_label, options.dup.update({size: 40, placeholder: street_text}))} + @template.tag('br')
    end
    unless city_label.nil?
      combined_field << @template.content_tag(:span){text_field(city_label, options.dup.update({size: 20, placeholder: 'City'}))}
    end
    unless state_label.nil?
      combined_field << @template.content_tag(:span){select(state_label, @template.get_opts('State', allow_nil: true))}
    end
    unless zip_label.nil?
      combined_field << @template.content_tag(:span){text_field(zip_label, options.dup.update({size: 10, placeholder: 'ZIP'}))}
    end
    combined_field
  end
  
  # wraps address field
  def edit_row_address(street1_label, street2_label, city_label, state_label, zip_label, *args)
    options = args.extract_options!
    # provide only the fields for non-nil labels
    field = address_field(street1_label, street2_label, city_label, state_label, zip_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)})
    unless options[:header]
      options[:header] = 'Address:'
    end
    wrap_edit_row(field, street1_label, options)
  end
  
  # allows charge entry (type, cnt, charge, agency)
  # if type is nil, agency always == 'District' unless :charge_type is specified
  # if any other label is nil, skips that field
  # adding nolookup: true to options disables charge name lookup and substitution
  def charge_field(type_label, cnt_label, charge_label, agency_label, *args)
    options = args.extract_options!
    type_js = "updateChargeAgency($(this).uniqueId().attr('id'))"
    lookup_url = @template.validate_charge_statutes_path
    lookup_js = "chargeLookup('#{lookup_url}',$(this))"
    combined_field = ''.html_safe
    if options[:nolookup]
      chg_lookup = nil
    else
      chg_lookup = lookup_js
    end
    options.delete(:nolookup)
    ctypenames = Charge::ChargeType.dup
    if options[:nocity]
      ctypenames.delete('City')
    end
    if options[:noother]
      ctypenames.delete('Other')
    end
    options.delete(:nocity)
    options.delete(:noother)
    ctypes = ctypenames.zip(ctypenames)
    ctype = 'District'
    # allow object to override
    if @object.respond_to?('charge_type') && !@object.send('charge_type').blank?
      ctype = @object.send('charge_type')
    end
    # force type if specified
    unless options[:charge_type].blank?
      ctype = options[:charge_type]
    end
    options.delete(:charge_type)
    unless type_label.nil?
      combined_field << @template.content_tag(:span){select(type_label, ctypes, options.dup.update({selected: ctype}), {id: nil, onchange: type_js})}
    end
    unless cnt_label.nil?
      combined_field << @template.content_tag(:span){text_field(cnt_label, options.dup.update({size: 3, placeholder: 'Cnt'}))}
    end
    unless charge_label.nil?
      combined_field << @template.content_tag(:span){text_field(charge_label, options.dup.update({size: 60, placeholder: 'Charge', id: nil, onchange: chg_lookup}))}
    end
    unless agency_label.nil?
      if type_label.nil?
        # again, district charge only
        combined_field << hidden_field(agency_label, options.dup, {value: ''})
      else
        if ctype.nil? || ctype == 'District'
          cstyle = ostyle = 'display:none;'
        elsif ctype == 'City'
          cstyle = nil
          ostyle = 'display:none;'
        else
          ostyle = nil
          cstyle = 'display:none;'
        end
        combined_field << @template.content_tag(:span, class: 'district', style: 'display:none;'){text_field(agency_label, options.dup.update({class: 'agency-select', value: '', disabled: !ctype.nil? && ctype != 'District'}))}
        combined_field << @template.content_tag(:span, class: 'city', style: cstyle){"Local City: ".html_safe + agency_field(agency_label, "Local Agencies", options.dup.update({disabled: ctype != 'City'}))}
        combined_field << @template.content_tag(:span, class: 'other', style: ostyle){"Other Agency: ".html_safe + agency_field(agency_label, options.dup.update({disabled: ctype != 'Other'}))}
      end
    end
    combined_field
  end
  
  # allows charge entry (cnt, charge)
  def edit_row_charge(type_label, cnt_label, charge_label, agency_label, *args)
    options = args.extract_options!
    field_tag = charge_field(type_label, cnt_label, charge_label, agency_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)})
    wrap_edit_row(field_tag, charge_label, options.reject{|k,v| [:nolookup].include?(k)}.merge({tooltip: @template.charge_tool_tip}))
  end
  
  # allows sentence entry (hours, days, months, years, life, death)
  def sentence_field(base_label, *args)
    options = args.extract_options!
    combined_field = ''.html_safe
    hours_label = "#{base_label}_hours".to_sym
    days_label = "#{base_label}_days".to_sym
    months_label = "#{base_label}_months".to_sym
    years_label = "#{base_label}_years".to_sym
    life_label = "#{base_label}_life".to_sym
    death_label = "#{base_label}_death".to_sym
    options[:size] = 4
    if @object.respond_to?(hours_label)
      combined_field << @template.content_tag(:span){"Hrs: ".html_safe + text_field(hours_label, options)}
    end
    if @object.respond_to?(days_label)
      combined_field << @template.content_tag(:span){"Dys: ".html_safe + text_field(days_label, options)}
    end
    if @object.respond_to?(months_label)
      combined_field << @template.content_tag(:span){"Mths: ".html_safe + text_field(months_label, options)}
    end
    if @object.respond_to?(years_label)
      combined_field << @template.content_tag(:span){"Yrs: ".html_safe + text_field(years_label, options)}
    end
    if @object.respond_to?(life_label)
      combined_field << @template.content_tag(:span){"Life: ".html_safe + text_field(life_label, options)}
    end
    if @object.respond_to?(death_label)
      combined_field << @template.content_tag(:span){check_box(death_label, options.dup.reject{|k,v| k == :size}.update({tag: "Death"}))}
    end
    combined_field
  end
  
  # wraps sentence field
  def edit_row_sentence(base_label, *args)
    options = args.extract_options!
    field = sentence_field(base_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)})
    wrap_edit_row(field, base_label, options)
  end
  
  # allows OLN (operator license number) fields with state select
  def oln_field(oln_label, oln_state_label, *args)
    options = args.extract_options!
    combined_field = ''.html_safe
    combined_field << @template.content_tag(:span){text_field(oln_label, options.dup.update({size: 15}))}
    combined_field << @template.content_tag(:span){select(oln_state_label, @template.get_opts('State', allow_nil: true), options)}
    combined_field
  end
  
  # wraps oln_field
  def edit_row_oln(oln_label, oln_state_label, *args)
    options = args.extract_options!
    field = oln_field(oln_label, oln_state_label, options.dup.reject{|k,v| [:header, :required, :tooltip].include?(k)})
    wrap_edit_row(field, oln_label, options)
  end
  
  # taken from legacy_prototype_helper
  #
  def observe_field(field_id, options = {})
    if options[:frequency] && options[:frequency] > 0
      build_observer('Form.Element.Observer', field_id, options)
    else
      build_observer('Form.Element.EventObserver', field_id, options)
    end
  end
  
  # does a standard remarks field with h4 header and tooltip
  def textile_field(object_label, *args)
    options = args.extract_options!
    h4_text = options[:header] || object_label.to_s.titleize
    if options[:required]
      h4_text = @template.required + h4_text
      options.delete(:required)
    end
    object_header = @template.content_tag(:h4){
      if options[:tooltip]
        @template.with_tooltip(h4_text, options[:tooltip])
      else
        h4_text
      end
    }
    options.delete(:tooltip)
    options.delete(:header)
    if options[:class]
      options[:class] << ' textile'
    else
      options[:class] = 'textile'
    end
    object_header + text_area(object_label, options)
  end
  
  private
  
  # taken from prototype_legacy_helper
  def build_observer(klass, name, options = {})
    if options[:with] && (options[:with] !~ /[\{=(.]/)
      options[:with] = "'#{options[:with]}=' + encodeURIComponent(value)"
    else
      options[:with] ||= 'value' unless options[:function]
    end

    callback = options[:function] || @template.remote_function(options)
    javascript = "new #{klass}('#{name}', "
    javascript << "#{options[:frequency]}, " if options[:frequency]
    javascript << "function(element, value) {"
    javascript << "#{callback}}"
    javascript << ")"
    @template.javascript_tag(javascript)
  end
  
  def create_edit_label(field_label, *args)
    options = args.extract_options!
    custom_label = options[:header] || field_label.to_s.titleize + ':'
    label_class = options[:label_class]
    @template.content_tag("label", custom_label, class: label_class)
  end
  
  # uses edit_row helper (view_row_helper.rb) 
  # to create the edit form row using supplied field/label contents.
  def wrap_edit_row(field, field_label, *args)
    options = args.extract_options!
    field_label_tag = create_edit_label(field_label, options.reject{|k,v| k == :tooltip}.dup)
    @template.edit_row(field_label_tag, field, options)
  end
end