module InvestCrimesHelper
  def invest_crime_links(invest_crimes)
    return '' unless invest_crimes.is_a?(Array) && can?(:view_investigations)
    txt = []
    invest_crimes.each do |c|
      next unless c.is_a?(InvestCrime)
      if c.private? && c.owned_by != session[:user]
        txt.push(c.id.to_s.html_safe)
      else
        txt.push(link_to c.id.to_s, c)
      end
    end
    if txt.empty?
      'None'
    else
      safe_join(txt, ', ')
    end
  end
end