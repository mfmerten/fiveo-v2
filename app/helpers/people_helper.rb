module PeopleHelper
  # accepts a da charge and optional arrest charge and formats a string
  # showing processing status -- used for generating the rap sheet
  def charge_status_string(da_charge=nil, arr_charge=nil)
    return nil unless da_charge.is_a?(DaCharge)
    return nil unless arr_charge.nil? || arr_charge.is_a?(Charge)
    txt = ""
    if !arr_charge.nil? && !da_charge.nil? && da_charge.charge != arr_charge.charge
      txt << "Amended to (#{da_charge.count}) #{da_charge.charge}"
    end
    if da_charge.info_only?
      unless txt.blank?
        txt << " -- "
      end
      txt << "[Info Only]"
    elsif da_charge.sentence.nil? && !da_charge.dropped_by_da?
      unless txt.blank?
        txt << " -- "
      end
      txt << "Pending Court"
    elsif !da_charge.sentence.nil?
      unless txt.blank?
        txt << " -- "
      end
      txt << da_charge.sentence.sentence_summary
    end
    if da_charge.dropped_by_da?
      unless txt.blank?
        txt << " -- "
      end
      txt << "[Charge Dropped]"
    elsif da_charge.da_diversion?
      unless txt.blank?
        txt << " -- "
      end
      txt << "[DA Diversion]"
    end
    return txt
  end
  
  # used in rap sheet report class
  module_function :charge_status_string
  
  def html_status_flags(person, buttons=true)
    flags = []
    if person.deceased?
      flags.push(content_tag(:span, class: 'white black-bg'){'DECEASED'})
    end
    if person.is_wanted?
      if buttons
        flags.push(button_to_function("WANTED","window.location.href='#active'", class: 'red'))
      else
        flags.push(content_tag(:span, class: 'red-with-bg'){'WANTED'})
      end
    end
    if person.is_forbidden?
      if buttons
        flags.push(button_to_function("FORBIDDEN","window.location.href='#active'", class: 'blue'))
      else
        flags.push(content_tag(:span, class: 'blue-with-bg'){'FORBIDDEN'})
      end
    end
    if person.hepatitis?
      flags.push(content_tag(:span, class: 'red-with-bg'){'HEP'})
    end
    if person.hiv?
      flags.push(content_tag(:span, class: 'red-with-bg'){'HIV'})
    end
    if person.tb?
      flags.push(content_tag(:span, class: 'red-with-bg'){'TB'})
    end
    flags.empty? ? '' : safe_join(flags, space(2))
  end
  
  # takes a criminal history section and reproduces it in HTML
  def rap_sheet_section(hist)
    history = hist.clone
    unless history.rows.empty?
      txt = "".html_safe
      item_txt = "".html_safe
      section_txt = "".html_safe
      original_id = nil
      until history.rows.empty? do
        item = history.rows.shift
        if item.original_id != original_id
          # first part of a new item
          # clear the item and table text values
          item_txt = "".html_safe
          original_id = item.original_id
          # add ident row to item_txt
          # note, this needs to be clickable
          case item.row_name
          when 'Arrest'
            link_url = arrest_path(id: item.original_id)
          when 'Citation'
            link_url = citation_path(id: item.original_id)
          when 'Probation'
            link_url = probation_path(id: item.original_id)
          when 'Transfer'
            t = Transfer.find(item.original_id)
            link_url = jail_booking_transfer_path(t.jail, t.booking, t)
          else
            link_url = ''
          end
          item_txt << content_tag(:div, class: 'rap-name'){
            link_to(AppUtils.format_date(item.row_date).html_safe + ': '.html_safe +
            content_tag(:b){item.row_name}, link_url)
          }
        end
        # build the row
        item_txt << rap_sheet_item(item)
        # if last row for this item
        if history.rows.empty? || history.rows.first.original_id != original_id
          # add item_txt div to txt
          txt << content_tag(:div, class: 'rap-item'){item_txt}
        end
      end
      # no more items, return history section div
      content_tag(:div, class: 'rap-section'){txt}
    end
  end
  
  def rap_sheet_item(item)
    case item.row_name
    when 'Arrest','Citation'
      chg = content_tag(:div, class: 'rap-charge'){
        safe_join([item.column_1_text.to_s, " cnt ", item.column_2_text.to_s])
      }
      dispo = content_tag(:div, class: 'rap-dispo'){item.column_3_text.to_s}
    when 'Probation'
      chg = content_tag(:div, class: 'rap-charge'){
        safe_join([item.column_1_text.to_s, " cnt ", item.column_2_text.to_s])
      }
      dispo = content_tag(:div, class: 'rap-dispo'){
        safe_join([item.row_subname.to_s, ' &mdash; '.html_safe, item.column_3_text.to_s])
      }
    else
      chg = content_tag(:div, class: 'rap-charge'){
        content_tag(:b){"From:"} +
        safe_join([space(1), item.column_2_text, space(3)]) +
        content_tag(:b){"To:"} +
        safe_join([space(1), item.column_3_text])
      }
      dispo = ''.html_safe
    end
    chg + dispo
  end
end