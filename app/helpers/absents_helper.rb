module AbsentsHelper
  
  # generates a string of prisoner names and booking ids for absent views
  def absent_prisoner_string(absent)
    return "" unless absent.is_a?(Absent)
    result_array = []
    absent.absent_prisoners.each do |p|
      unless p.booking.nil? || p.booking.person.nil?
        result_array.push(p.booking_id.to_s + ": " + p.booking.person.sort_name)
      end
    end
    result_array.compact.uniq.join(' -- ')
  end

end
    