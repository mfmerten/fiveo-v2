module GroupsHelper
  # helper for writing perm checkboxes on the group edit view
  def perm_checkbox(perm, *args)
    options = args.extract_options!
    if options[:tag]
      tag = options[:tag]
    else
      tag = perm.to_s.titleize
    end
    # fix tags
    if tag =~ /^(View|Update|Admin) Jail [1-9]$/
      # just want the digit
      tag = tag.sub(/(View|Update|Admin) Jail /,'')
    elsif tag =~ /^Destroy/
      tag = 'Delete'
    elsif tag =~ /^View/
      tag = 'View'
    elsif tag =~ /^Update/
      tag = 'Update'
    elsif tag =~ /^Edit/
      tag = 'Edit'
    elsif tag =~ /^Reopen/
      tag = 'Reopen'
    elsif tag =~ /^Reset/
      tag = 'Reset'
    elsif tag =~ /^Merge/
      tag = 'Merge'
    elsif tag =~ /^Admin/
      tag = 'Admin'
    end
    field = check_box_tag('group[permissions][]', "#{Perm.get(perm)}", @group.permissions.include?(Perm.get(perm)), id: nil)
    if options[:tooltip]
      field_label =  with_tooltip(tag, options[:tooltip])
    else
      field_label = tag
    end
    content_tag(:span, class: options[:class]){field + ' '.html_safe + field_label}
  end
  
  # takes info for a whole row and builds checkboxes for them
  def perm_checkboxes(perm_base, *args)
    # any options outside array wrappers are for entire line
    options = args.extract_options!
    if options[:header]
      header = options[:header]
    else
      header = perm_base.to_s.titleize + ':'
    end
    txt = ''.html_safe
    # args should hold one or more arrays, each one makes up a checkbox
    # process them first
    cb_txt = ''.html_safe
    args.each do |a|
      # allow for symbols passed plain (without options)
      a = [a] unless a.is_a?(Array)
      a_opts = a.extract_options!
      unless a.empty?
        if a_opts[:perm]
          cb_symbol = a_opts[:perm]
          a_opts[:tag] = a[0].to_s.gsub(/_/,' ').titleize.gsub(/\bid\b/i, 'ID')
        else
          # special case if a is_a numeric character (jail permset, symbol is reversed)
          if a[0] =~ /^[0-9]$/
            cb_symbol = (perm_base.to_s + '_' + a[0]).to_sym
          else
            cb_symbol = (a[0].to_s + '_' + perm_base.to_s).to_sym
          end
        end
        # destroy is always red, so we can add that here to keep from
        # having to specify it on the edit form
        if a[0] == :destroy
          a_opts[:class] = 'red'
        end
        cb_txt << perm_checkbox(cb_symbol, a_opts)
      end
    end
    edit_row(header, cb_txt, options)
  end
    
end
    