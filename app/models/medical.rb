class Medical < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :booking, inverse_of: :medicals                            # constraint
  
  delegate :jail, to: :booking

  attr_accessor :officer_id
  
  before_validation :lookup_officers
  before_save :check_creator

  validates_presence_of :officer, :disposition
  validates_datetime :screen_datetime, on_or_before: :now
  validates_booking :booking_id

  ## Class Methods #############################################
  
  def self.base_perms
    :view_booking
  end
  
  def self.desc
    'Medical Screening Records'
  end

  ## Instance Methods #############################################
  
  def condition_names(key=nil)
    names = {
      'heart_disease' => 'Heart Disease',
      'blood_pressure' => 'High Blood Pressure',
      'diabetes' => 'Diabetes',
      'epilepsy' => 'Epilipsy',
      'hepatitis' => 'Hepatitis',
      'hiv' => 'HIV/AIDS',
      'tb' => 'Tuberculosis',
      'ulcers' => 'Ulcers',
      'venereal' => 'Venereal Disease'
    }
    if key.blank?
      names
    else
      names[key.to_s]
    end
  end
  
  def conditions_string
    condition_names.keys.collect{|c| self.send("#{c}?") ? condition_names(c) : nil}.compact.join(', ')
  end
  
  def has_problems?
    problem_names.keys.each do |k|
      return true unless self.send(k).blank?
    end
    false
  end
  
  def problem_names(key=nil)
    names = {
      'arrest_injuries' => 'Injured During Arrest:',
      'rec_med_tmnt' => 'Receiving Medical Treatment:',
      'rec_dental_tmnt' => 'Receiving Dental Treatment:',
      'rec_mental_tmnt' => 'Receiving Mental Treatment:',
      'current_meds' => 'Taking Medications:',
      'allergies' => 'Allergic to Medications:',
      'injuries' => 'Injuries, Illness, Pain:',
      'infestation' => 'Infestation, Lesions:',
      'alcohol' => 'Influence of Alcohol:',
      'drug' => 'Influence of Drugs:',
      'withdrawal' => 'Signs of Withdrawal:',
      'attempt_suicide' => 'Suicide Thoughts/Attempts:',
      'suicide_risk' => 'Suicide Risk:',
      'danger' => 'Danger to Self/Others:',
      'pregnant' => 'Pregnant:',
      'deformities' => 'Deformaties/Disabilities/Medical Devices:'
    }
    if key.blank?
      # no key specified, return name has
      names
    else
      # key specified, return value for key
      names[key.to_s]
    end
  end
  
  private

  def lookup_officers
    unless officer_id.blank?
      if ofc = Contact.find_by_id(officer_id)
        self.officer = ofc.sort_name
        self.officer_badge = ofc.badge_no
        self.officer_unit = ofc.unit
      end
      self.officer_id = nil
    end
    true
  end
end

# == Schema Information
#
# Table name: medicals
#
#  id              :integer          not null, primary key
#  booking_id      :integer          not null
#  arrest_injuries :string(255)      default(""), not null
#  officer         :string(255)      default(""), not null
#  officer_unit    :string(255)      default(""), not null
#  disposition     :string(255)      default(""), not null
#  rec_med_tmnt    :string(255)      default(""), not null
#  rec_dental_tmnt :string(255)      default(""), not null
#  rec_mental_tmnt :string(255)      default(""), not null
#  current_meds    :string(255)      default(""), not null
#  allergies       :string(255)      default(""), not null
#  injuries        :string(255)      default(""), not null
#  infestation     :string(255)      default(""), not null
#  alcohol         :string(255)      default(""), not null
#  drug            :string(255)      default(""), not null
#  withdrawal      :string(255)      default(""), not null
#  attempt_suicide :string(255)      default(""), not null
#  suicide_risk    :string(255)      default(""), not null
#  danger          :string(255)      default(""), not null
#  pregnant        :string(255)      default(""), not null
#  deformities     :string(255)      default(""), not null
#  heart_disease   :boolean          default(FALSE), not null
#  blood_pressure  :boolean          default(FALSE), not null
#  diabetes        :boolean          default(FALSE), not null
#  epilepsy        :boolean          default(FALSE), not null
#  hepatitis       :boolean          default(FALSE), not null
#  hiv             :boolean          default(FALSE), not null
#  tb              :boolean          default(FALSE), not null
#  ulcers          :boolean          default(FALSE), not null
#  venereal        :boolean          default(FALSE), not null
#  created_by      :integer          default(1)
#  updated_by      :integer          default(1)
#  created_at      :datetime
#  updated_at      :datetime
#  officer_badge   :string(255)      default(""), not null
#  screen_datetime :datetime
#
# Indexes
#
#  index_medicals_on_booking_id  (booking_id)
#  index_medicals_on_created_by  (created_by)
#  index_medicals_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  medicals_booking_id_fk  (booking_id => bookings.id)
#  medicals_created_by_fk  (created_by => users.id)
#  medicals_updated_by_fk  (updated_by => users.id)
#
