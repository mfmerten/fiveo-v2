# adapted from "forem" engine (https://github.com/radar/forem)
#
# Copyright 2011 Ryan Bigg, Philip Arndt and Josh Adams
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
class ForumView < ActiveRecord::Base
  belongs_to :forum, inverse_of: :forum_views                              # constraint
  belongs_to :forum_topic, inverse_of: :forum_views                        # constraint
  belongs_to :user, inverse_of: :forum_views                               # constraint

  before_create :set_viewed_at_to_now

  ## Class Methods #############################################
  
  def self.desc
    'Discussion Forums'
  end
  
  def self.base_perms
    :view_forum
  end
  
  ## Instance Methods #############################################
  
  def viewed_at
    updated_at
  end

  private

  ## Private Instance Methods #############################################
  
  def set_viewed_at_to_now
    self.current_viewed_at = Time.now
    self.past_viewed_at = current_viewed_at
  end
end

# == Schema Information
#
# Table name: forum_views
#
#  id                :integer          not null, primary key
#  user_id           :integer          not null
#  count             :integer          default(0), not null
#  current_viewed_at :datetime
#  past_viewed_at    :datetime
#  created_at        :datetime
#  updated_at        :datetime
#  forum_id          :integer
#  forum_topic_id    :integer
#
# Indexes
#
#  index_forum_views_on_forum_id        (forum_id)
#  index_forum_views_on_forum_topic_id  (forum_topic_id)
#  index_forum_views_on_updated_at      (updated_at)
#  index_forum_views_on_user_id         (user_id)
#
# Foreign Keys
#
#  forum_views_forum_id_fk        (forum_id => forums.id)
#  forum_views_forum_topic_id_fk  (forum_topic_id => forum_topics.id)
#  forum_views_user_id_fk         (user_id => users.id)
#
