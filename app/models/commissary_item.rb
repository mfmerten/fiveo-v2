class CommissaryItem < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'
  belongs_to :jail, inverse_of: :commissary_items
  
  ## Scopes #############################################
  
  scope :by_description, -> { order :description }
  scope :belonging_to_jail, ->(j) { where jail_id: j }
  
  # filter scopes
  scope :with_commissary_item_id, ->(c_id) { where id: c_id }
  scope :with_description, ->(des) { AppUtils.like self, :description, des }
  
  ## Callbacks #############################################
  
  before_save :check_creator
  before_validation :fix_description
  
  ## Validations #############################################
  
  validates_presence_of :description
  validates_numericality_of :price, greater_than: 0, less_than: 10 ** 10
  
  ## Class Methods #############################################
  
  def self.base_perms
    :update_commissary
  end
  
  def self.desc
    'Commissary Sales Items'
  end
  
  # process query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_commissary_item_id(query[:id]) unless query[:id].blank?
    result = result.with_description(query[:description]) unless query[:description].blank?
    result
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def fix_description
    if description?
      self.description = description.titleize
    end
    true
  end
end

# == Schema Information
#
# Table name: commissary_items
#
#  id          :integer          not null, primary key
#  description :string(255)      default(""), not null
#  price       :decimal(12, 2)   default(0.0), not null
#  active      :boolean          default(TRUE), not null
#  created_by  :integer          default(1)
#  updated_by  :integer          default(1)
#  created_at  :datetime
#  updated_at  :datetime
#  jail_id     :integer          not null
#
# Indexes
#
#  index_commissary_items_on_created_by  (created_by)
#  index_commissary_items_on_jail_id     (jail_id)
#  index_commissary_items_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  commissary_items_created_by_fk  (created_by => users.id)
#  commissary_items_jail_id_fk     (jail_id => jails.id)
#  commissary_items_updated_by_fk  (updated_by => users.id)
#
