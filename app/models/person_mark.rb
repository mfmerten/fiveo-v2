class PersonMark < ActiveRecord::Base
  belongs_to :person, inverse_of: :marks                                   # constraint

  validates_presence_of :description

  ## Class Methods #############################################
  
  def self.desc
    "Distinguishing Marks for People"
  end

  ## Instance Methods #############################################
  
  def to_s
    "#{description}#{location.blank? ? '' : ' on ' + location}"
  end
end

# == Schema Information
#
# Table name: person_marks
#
#  id          :integer          not null, primary key
#  person_id   :integer          not null
#  description :string(255)      default(""), not null
#  created_at  :datetime
#  updated_at  :datetime
#  location    :string(255)      default(""), not null
#
# Indexes
#
#  index_marks_on_person_id  (person_id)
#
# Foreign Keys
#
#  person_marks_person_id_fk  (person_id => people.id)
#
