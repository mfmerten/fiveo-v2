class InvestReportPhoto < ActiveRecord::Base
  belongs_to :invest_report, inverse_of: :invest_report_photos             # constraint
  belongs_to :invest_photo, inverse_of: :invest_report_photos              # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestReport Photos'
  end
end

# == Schema Information
#
# Table name: invest_report_photos
#
#  id               :integer          not null, primary key
#  invest_report_id :integer          not null
#  invest_photo_id  :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_invest_report_photos_on_invest_photo_id   (invest_photo_id)
#  index_invest_report_photos_on_invest_report_id  (invest_report_id)
#
# Foreign Keys
#
#  invest_report_photos_invest_photo_id_fk   (invest_photo_id => invest_photos.id)
#  invest_report_photos_invest_report_id_fk  (invest_report_id => invest_reports.id)
#
