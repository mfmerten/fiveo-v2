class InvestNote < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :investigation, inverse_of: :notes                         # constraint
  belongs_to :invest_crime, inverse_of: :notes                          # constraint
  belongs_to :invest_criminal, inverse_of: :notes                       # constraint
  belongs_to :invest_photo, inverse_of: :notes                          # constraint
  belongs_to :invest_report, inverse_of: :notes                         # constraint
  belongs_to :invest_vehicle, inverse_of: :notes                        # constraint
  
  before_save :check_creator
  
  validates_presence_of :note
  
  ## Class Methods #############################################
  
  def self.base_perms
    :update_investigaton
  end
  
  def self.desc
    "Investigation Notes"
  end
  
  ## Instance Methods ##########################################
  
  def invest_record
    return invest_crime || invest_criminal || invest_photo || invest_report || invest_vehicle || investigation
  end
end

# == Schema Information
#
# Table name: invest_notes
#
#  id                 :integer          not null, primary key
#  note               :text             default(""), not null
#  created_by         :integer          default(1)
#  updated_by         :integer          default(1)
#  created_at         :datetime
#  updated_at         :datetime
#  investigation_id   :integer
#  invest_crime_id    :integer
#  invest_criminal_id :integer
#  invest_photo_id    :integer
#  invest_report_id   :integer
#  invest_vehicle_id  :integer
#
# Indexes
#
#  index_invest_notes_on_created_by          (created_by)
#  index_invest_notes_on_invest_crime_id     (invest_crime_id)
#  index_invest_notes_on_invest_criminal_id  (invest_criminal_id)
#  index_invest_notes_on_invest_photo_id     (invest_photo_id)
#  index_invest_notes_on_invest_report_id    (invest_report_id)
#  index_invest_notes_on_invest_vehicle_id   (invest_vehicle_id)
#  index_invest_notes_on_investigation_id    (investigation_id)
#  index_invest_notes_on_updated_by          (updated_by)
#
# Foreign Keys
#
#  invest_notes_created_by_fk          (created_by => users.id)
#  invest_notes_invest_crime_id_fk     (invest_crime_id => invest_crimes.id)
#  invest_notes_invest_criminal_id_fk  (invest_criminal_id => invest_criminals.id)
#  invest_notes_invest_photo_id_fk     (invest_photo_id => invest_photos.id)
#  invest_notes_invest_report_id_fk    (invest_report_id => invest_reports.id)
#  invest_notes_invest_vehicle_id_fk   (invest_vehicle_id => invest_vehicles.id)
#  invest_notes_investigation_id_fk    (investigation_id => investigations.id)
#  invest_notes_updated_by_fk          (updated_by => users.id)
#
