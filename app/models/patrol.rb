class Patrol < ActiveRecord::Base
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'    # constraint with nullify
  
  scope :by_date, -> { order :patrol_started_datetime }
  scope :by_reverse_date, -> { order patrol_started_datetime: :desc }
  scope :on_or_after, ->(start) { AppUtils.for_daterange self, :patrol_started_datetime, start: start, stop: nil }
  scope :on_or_before, ->(stop) { AppUtils.for_daterange self, :patrol_started_datetime, start: nil, stop: stop }
  scope :between, ->(start,stop) { AppUtils.for_daterange self, :patrol_started_datetime, start: start, stop: stop }
  
  # filter scopes
  scope :with_patrol_id, ->(p_id) { where id: p_id }
  scope :with_officer, ->(name,unit,badge) { AppUtils.for_officer self, :officer, name, unit, badge }
  scope :with_patrol_started_date, ->(start,stop) { AppUtils.for_daterange self, :patrol_started_datetime, start: start, stop: stop }
  scope :with_shift, ->(s) { where shift: s }
  scope :with_supervisor, ->(s) { AppUtils.like self, :supervisor, s }
  scope :with_remarks, ->(r) { AppUtils.like self, :remarks, r }
  
  attr_accessor :officer_id
  
  before_save :check_creator
  before_validation :lookup_officers

  validates_presence_of :officer, :patrol_started_datetime, :shift
  validates_datetime :patrol_started_datetime, :patrol_ended_datetime, on_or_before: :now, allow_blank: true
  validates_numericality_of :shift, integer_only: true, greater_than: 0, less_than: 7
  validates_numericality_of :beginning_odometer, :ending_odometer, :roads_patrolled,
    :complaints, :traffic_stops, :citations_issued, :papers_served, :narcotics_arrests,
    :other_arrests, :public_assists, :warrants_served, :funeral_escorts, only_integer: true, allow_nil: true

  ## Class Methods #############################################
  
  def self.base_perms
    :view_offense
  end
  
  def self.between(start=nil,stop=nil)
    recs = by_date
    if start.is_a?(Date) && stop.is_a?(Date)
      recs.between(start.to_time, stop.to_time.end_of_day)
    elsif start.is_a?(Date)
      recs.on_or_after(start.to_time)
    elsif stop.is_a?(Date)
      recs.on_or_before(stop.to_time.end_of_day)
    else
      recs
    end
  end
  
  def self.desc
    'Patrol Logs'
  end
  
  def self.valid_shifts_for_select
    shifts = SiteConfig.patrol_shifts
    if shifts.empty?
      ['None Defined',nil]
    else
      temp = []
      shifts.each do |shift|
        supervisor = ""
        unless SiteConfig.send("patrol_shift#{shift}_supervisor").blank?
          supervisor = SiteConfig.send("patrol_shift#{shift}_supervisor") + ": "
        end
        temp.push(["#{shift} -- #{supervisor}#{SiteConfig.send("patrol_shift#{shift}_start")}-#{SiteConfig.send("patrol_shift#{shift}_stop")}", shift])
      end
      temp
    end
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_patrol_id(query[:id]) unless query[:id].blank?
    result = result.with_officer(query[:officer], query[:officer_unit], query[:officer_badge]) unless query[:officer].blank? && query[:officer_unit].blank? && query[:officer_badge].blank?
    result = result.with_patrol_started_date(query[:patrol_started_date_for],query[:patrol_started_date_to]) unless query[:patrol_started_date_for].blank? && query[:patrol_started_date_to].blank?
    result = result.with_shift(query[:shift]) unless query[:shift].blank?
    result = result.with_supervisor(query[:supervisor]) unless query[:supervisor].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end

  ## Instance Methods #############################################
  
  def activity_summary
    txt = []
    %w(roads_patrolled  complaints  traffic_stops  citations_issued  papers_served  narcotics_arrests  other_arrests  public_assists  warrants_served  funeral_escorts).each do |a|
      if send("#{a}?")
        txt.push("#{a.titleize}: #{send(a)}")
      end
    end
    txt.join(", ")
  end
  
  def duration
    return "0" unless patrol_started_datetime? && patrol_ended_datetime?
    AppUtils.duration_in_words(patrol_ended_datetime, patrol_started_datetime)
  end
  
  def mileage
    AppUtils.total_miles(beginning_odometer, ending_odometer)
  end
  
  def shift_string
    "#{shift}: #{supervisor.blank? ? '' : supervisor + ': '}#{shift_start} to #{shift_stop}"
  end
  
  def total_activities
    [roads_patrolled, complaints, traffic_stops, citations_issued, papers_served, narcotics_arrests, other_arrests, public_assists, warrants_served, funeral_escorts].compact.sum
  end
  
  private
  
  ## Instance Methods #############################################
  
  def lookup_officers
    unless officer_id.blank?
      if ofc = Contact.find_by_id(officer_id)
        self.officer = ofc.sort_name
        self.officer_badge = ofc.badge_no
        self.officer_unit = ofc.unit
      end
      self.officer_id = nil
    end
    true
  end
end

# == Schema Information
#
# Table name: patrols
#
#  id                      :integer          not null, primary key
#  officer                 :string(255)      default(""), not null
#  officer_unit            :string(255)      default(""), not null
#  officer_badge           :string(255)      default(""), not null
#  beginning_odometer      :integer          default(0), not null
#  ending_odometer         :integer          default(0), not null
#  shift                   :integer          default(0), not null
#  supervisor              :string(255)      default(""), not null
#  shift_start             :string(255)      default(""), not null
#  shift_stop              :string(255)      default(""), not null
#  roads_patrolled         :integer          default(0), not null
#  complaints              :integer          default(0), not null
#  traffic_stops           :integer          default(0), not null
#  citations_issued        :integer          default(0), not null
#  papers_served           :integer          default(0), not null
#  narcotics_arrests       :integer          default(0), not null
#  other_arrests           :integer          default(0), not null
#  public_assists          :integer          default(0), not null
#  warrants_served         :integer          default(0), not null
#  funeral_escorts         :integer          default(0), not null
#  remarks                 :text             default(""), not null
#  created_by              :integer          default(1)
#  updated_by              :integer          default(1)
#  created_at              :datetime
#  updated_at              :datetime
#  patrol_started_datetime :datetime
#  patrol_ended_datetime   :datetime
#
# Indexes
#
#  index_patrols_on_created_by  (created_by)
#  index_patrols_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  patrols_created_by_fk  (created_by => users.id)
#  patrols_updated_by_fk  (updated_by => users.id)
#
