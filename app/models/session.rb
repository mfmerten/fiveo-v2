class Session < ActiveRecord::Base
  # returns a short description for the DatabaseController index view.
  def self.desc
    "User Sessions"
  end

  # method called by a DatabaseController action to clear the session table.
  def self.reset_table
    sql = Session.connection
    sql.execute("truncate table sessions")
  end

  # method called by a rake task to reap old sessions
  def self.reap_expired_sessions
    if SiteConfig.session_max > SiteConfig.session_life
      destroy_all("created_at < '#{(SiteConfig.session_max + 720).minutes.ago.to_s(:db)}'")
    else
      destroy_all("created_at < '#{(SiteConfig.session_life + 720).minutes.ago.to_s(:db)}'")
    end
  end
end

# == Schema Information
#
# Table name: sessions
#
#  id         :integer          not null, primary key
#  session_id :string(255)      not null
#  data       :text
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_sessions_on_session_id  (session_id)
#  index_sessions_on_updated_at  (updated_at)
#
