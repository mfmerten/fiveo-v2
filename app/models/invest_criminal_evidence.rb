class InvestCriminalEvidence < ActiveRecord::Base
  belongs_to :invest_criminal, inverse_of: :invest_criminal_evidences # constraint
  belongs_to :invest_evidence, inverse_of: :invest_criminal_evidences # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestCriminal Evidence'
  end
end

# == Schema Information
#
# Table name: invest_criminal_evidences
#
#  id                 :integer          not null, primary key
#  invest_criminal_id :integer
#  invest_evidence_id :integer
#  created_at         :datetime
#  updated_at         :datetime
#
# Indexes
#
#  index_invest_criminal_evidences_on_invest_criminal_id  (invest_criminal_id)
#  index_invest_criminal_evidences_on_invest_evidence_id  (invest_evidence_id)
#
# Foreign Keys
#
#  invest_criminal_evidences_invest_criminal_id_fk  (invest_criminal_id => invest_criminals.id)
#  invest_criminal_evidences_invest_evidence_id_fk  (invest_evidence_id => invest_evidences.id)
#
