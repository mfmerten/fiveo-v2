class SignalCode < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  
  scope :by_code, -> { order :code }
  scope :by_name, -> { order :name }
  
  before_save :check_creator
  before_validation :fix_signal_code
  
  validates_uniqueness_of :code
  validates_presence_of :name
  
  ## Class Methods #############################################
  
  def self.base_perms
    :update_call
  end
  
  def self.desc
    'Signal Codes'
  end
  
  def self.get_signal(signal_code)
    sc = find_by_code(signal_code)
    return nil if sc.nil?
    sc.name
  end
  
  private
  
  ## Private Instance Methods #############################################

  def fix_signal_code
    self.code = code.upcase
    true
  end
end

# == Schema Information
#
# Table name: signal_codes
#
#  id         :integer          not null, primary key
#  code       :string(255)      default(""), not null
#  name       :string(255)      default(""), not null
#  created_by :integer          default(1)
#  updated_by :integer          default(1)
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_signal_codes_on_code        (code)
#  index_signal_codes_on_created_by  (created_by)
#  index_signal_codes_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  signal_codes_created_by_fk  (created_by => users.id)
#  signal_codes_updated_by_fk  (updated_by => users.id)
#
