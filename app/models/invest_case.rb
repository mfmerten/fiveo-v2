class InvestCase < ActiveRecord::Base
  belongs_to :investigation, inverse_of: :invest_cases                  # constraint
  belongs_to :call, inverse_of: :invest_cases                           # constraint
  has_many :invest_crime_cases, dependent: :destroy                     # constraint
  has_many :crimes, through: :invest_crime_cases, source: :invest_crime # constrained through invest_crime_cases

  ## Class Methods #############################################
  
  def self.desc
    "Investigation Cases"
  end
end

# == Schema Information
#
# Table name: invest_cases
#
#  id               :integer          not null, primary key
#  investigation_id :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#  call_id          :integer          not null
#
# Indexes
#
#  index_invest_cases_on_call_id           (call_id)
#  index_invest_cases_on_investigation_id  (investigation_id)
#
# Foreign Keys
#
#  invest_cases_call_id_fk           (call_id => calls.id)
#  invest_cases_investigation_id_fk  (investigation_id => investigations.id)
#
