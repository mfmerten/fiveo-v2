class InvestWitness < ActiveRecord::Base
  belongs_to :person, inverse_of: :invest_witnesses                        # constraint
  belongs_to :investigation, inverse_of: :invest_witnesses                 # constraint
  has_many :invest_crime_witnesses, dependent: :destroy                    # constraint
  has_many :crimes, through: :invest_crime_witnesses, source: :invest_crime  # constrained through invest_crime_witnesses
  has_many :invest_report_witnesses, dependent: :destroy                   # constraint
  has_many :reports, through: :invest_report_witnesses, source: :invest_report  # constrained through invest_report_witnesses
  
  ## Class Methods #############################################
  
  def self.desc
    'Investigation Witnesses'
  end
end

# == Schema Information
#
# Table name: invest_witnesses
#
#  id               :integer          not null, primary key
#  person_id        :integer          not null
#  investigation_id :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_invest_witnesses_on_investigation_id  (investigation_id)
#  index_invest_witnesses_on_person_id         (person_id)
#
# Foreign Keys
#
#  invest_witnesses_investigation_id_fk  (investigation_id => investigations.id)
#  invest_witnesses_person_id_fk         (person_id => people.id)
#
