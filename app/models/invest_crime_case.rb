class InvestCrimeCase < ActiveRecord::Base
  belongs_to :invest_crime, inverse_of: :invest_crime_cases                # constraint
  belongs_to :invest_case, inverse_of: :invest_crime_cases                 # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestCrime Cases'
  end
end

# == Schema Information
#
# Table name: invest_crime_cases
#
#  id              :integer          not null, primary key
#  invest_crime_id :integer          not null
#  invest_case_id  :integer          not null
#  created_at      :datetime
#  updated_at      :datetime
#
# Indexes
#
#  index_invest_crime_cases_on_invest_case_id   (invest_case_id)
#  index_invest_crime_cases_on_invest_crime_id  (invest_crime_id)
#
# Foreign Keys
#
#  invest_crime_cases_invest_case_id_fk   (invest_case_id => invest_cases.id)
#  invest_crime_cases_invest_crime_id_fk  (invest_crime_id => invest_crimes.id)
#
