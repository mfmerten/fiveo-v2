class InvestCriminalPhoto < ActiveRecord::Base
  belongs_to :invest_criminal, inverse_of: :invest_criminal_photos         # constraint
  belongs_to :invest_photo, inverse_of: :invest_criminal_photos            # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestCriminal Photos'
  end
end

# == Schema Information
#
# Table name: invest_criminal_photos
#
#  id                 :integer          not null, primary key
#  invest_criminal_id :integer          not null
#  invest_photo_id    :integer          not null
#  created_at         :datetime
#  updated_at         :datetime
#
# Indexes
#
#  index_invest_criminal_photos_on_invest_criminal_id  (invest_criminal_id)
#  index_invest_criminal_photos_on_invest_photo_id     (invest_photo_id)
#
# Foreign Keys
#
#  invest_criminal_photos_invest_criminal_id_fk  (invest_criminal_id => invest_criminals.id)
#  invest_criminal_photos_invest_photo_id_fk     (invest_photo_id => invest_photos.id)
#
