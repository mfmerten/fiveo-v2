class InvestCrimeWitness < ActiveRecord::Base
  belongs_to :invest_crime, inverse_of: :invest_crime_witnesses            # constraint
  belongs_to :invest_witness, inverse_of: :invest_crime_witnesses          # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'Investigation Crime Witnesses'
  end
end

# == Schema Information
#
# Table name: invest_crime_witnesses
#
#  id                :integer          not null, primary key
#  invest_crime_id   :integer          not null
#  invest_witness_id :integer          not null
#  created_at        :datetime
#  updated_at        :datetime
#
# Indexes
#
#  index_invest_crime_witnesses_on_invest_crime_id    (invest_crime_id)
#  index_invest_crime_witnesses_on_invest_witness_id  (invest_witness_id)
#
# Foreign Keys
#
#  invest_crime_witnesses_invest_crime_id_fk    (invest_crime_id => invest_crimes.id)
#  invest_crime_witnesses_invest_witness_id_fk  (invest_witness_id => invest_witnesses.id)
#
