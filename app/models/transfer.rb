class Transfer < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :booking, inverse_of: :transfers                              # constraint
  belongs_to :person, inverse_of: :transfers                               # constraint
  has_one :doc, dependent: :destroy, inverse_of: :transfer              # constraint
  has_one :hold, dependent: :destroy, inverse_of: :transfer             # constraint
    
  delegate :jail, to: :booking
  
  scope :between, ->(start,stop) { where('transfers.transfer_datetime >= ? AND transfers.transfer_datetime <= ?',start,stop) }
  scope :by_reverse_id, -> { order id: :desc }
  scope :belongs_to_jail, ->(j) { where('bookings.jail_id = ?',j).references(:booking) }
  
  # filter scopes
  scope :with_transfer_id, ->(t_id) { where id: t_id }
  scope :with_booking_id, ->(b_id) { where booking_id: b_id }
  scope :with_person_id, ->(p_id) { where person_id: p_id }
  scope :with_person_name, ->(name) { AppUtils.for_person self, name, association: :person }
  scope :with_transfer_type, ->(t) { AppUtils.like self, :transfer_type, t }
  scope :with_transfer_date, ->(start,stop) { AppUtils.for_daterange self, :transfer_datetime, start: start, stop: stop }
  scope :with_agency_name, ->(a) { AppUtils.like self, [:from_agency, :to_agency], a }
  scope :with_officer, ->(name,unit,badge) { AppUtils.for_officer self, [:from_officer,:to_officer], name, unit, badge }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  
  attr_accessor :from_agency_id, :to_agency_id, :from_officer_id, :to_officer_id
  
  before_save :check_creator
  before_validation :lookup_officers
  after_save :process_transfer

  validates_presence_of :to_agency, :from_agency, :to_officer, :from_officer
  validates_datetime :transfer_datetime

  TransferType = [
    "Temporary Transfer (OUT)", "Temporary Transfer Return (IN)",
    "Temporary Housing (IN)", "Temporary Housing Return (OUT)",
    "DOC Transfer (IN)", "Permanent Transfer (OUT)"
  ].freeze
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_booking
  end
  
  def self.desc
    'Jail Transfer Log'
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_transfer_id(query[:id]) unless query[:id].blank?
    result = result.with_booking_id(query[:booking_id]) unless query[:booking_id].blank?
    result = result.with_person_id(query[:person_id]) unless query[:person_id].blank?
    result = result.with_person_name(query[:person_name]) unless query[:person_name].blank?
    result = result.with_transfer_type(query[:transfer_type]) unless query[:transfer_type].blank?
    result = result.with_transfer_date(query[:transfer_from],query[:transfer_to]) unless query[:transfer_from].blank? && query[:transfer_to].blank?
    result = result.with_agency_name(query[:agency_name]) unless query[:agency_name].blank?
    result = result.with_officer(query[:officer], query[:officer_unit], query[:officer_badge]) unless query[:officer].blank? && query[:officer_unit].blank? && query[:officer_badge].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def process_transfer
    unless transfer_type.blank? || booking.nil? || booking.release_datetime?
      case transfer_type
      when "Temporary Transfer Return (IN)"
        h = booking.holds.active.of_type('Temporary Transfer').first
        # clear the temporary transfer hold
        unless h.nil?
          h.cleared_datetime = transfer_datetime
          h.cleared_by = updater.sort_name
          h.cleared_by_unit = updater.unit
          h.cleared_by_badge = updater.badge
          h.updated_by = updated_by
          h.cleared_because = "Returned from Transfer"
          h.explanation = "Automatic Clear by Transfer"
          h.save(validate: false)
        end
      when "Temporary Transfer (OUT)"
        # add a new temporary transfer hold
        h = Hold.new(
          transfer_id: id,
          booking_id: booking_id,
          hold_datetime: transfer_datetime,
          hold_type: "Temporary Transfer",
          hold_by: from_officer,
          hold_by_unit: from_officer_unit,
          hold_by_badge: from_officer_badge,
          remarks: remarks,
          created_by: updated_by,
          updated_by: updated_by,
          agency: to_agency,
          transfer_officer: to_officer,
          transfer_officer_unit: to_officer_unit,
          transfer_officer_badge: to_officer_badge
        )
        h.save(validate: false)
      when "Temporary Housing (IN)"
        # add a new temporary housing hold
        h = Hold.new(
          transfer_id: id,
          booking_id: booking_id,
          hold_datetime: transfer_datetime,
          hold_type: "Temporary Housing",
          hold_by: to_officer,
          hold_by_unit: to_officer_unit,
          hold_by_badge: to_officer_badge,
          remarks: remarks,
          created_by: updated_by,
          updated_by: updated_by,
          agency: from_agency,
          transfer_officer: from_officer,
          transfer_officer_unit: from_officer_unit,
          transfer_officer_badge: from_officer_badge
        )
        h.save(validate: false)
      when "Permanent Transfer (OUT)","Temporary Housing Return (OUT)"
        # clear all holds and release booking.
        booking.holds.active.each do |h|
          h.cleared_datetime = transfer_datetime
          h.cleared_by = from_officer
          h.cleared_by_unit = from_officer_unit
          h.cleared_by_badge = from_officer_badge
          h.updated_by = updated_by
          h.cleared_because = "Transferred"
          h.explanation = "Automatic Clear by Transfer to #{to_agency} by #{to_officer_unit} #{to_officer}."
          h.save(validate: false)
        end
        booking.release_officer = updater.sort_name
        booking.release_officer_unit = updater.unit
        booking.release_officer_badge = updater.badge
        booking.release_datetime = transfer_datetime
        booking.released_because = "Transferred"
        booking.updated_by = updated_by
        booking.save(validate: false)
        booking.send_release_messages
      when "DOC Transfer (IN)"
        # add a DOC record.
        d = Doc.new(
          transfer_id: id,
          billable: false,
          remarks: remarks,
          created_by: updated_by,
          updated_by: updated_by,
          sentence_id: nil,
          revocation_id: nil,
          doc_number: (person.nil? ? '' : person.doc_number),
          booking_id: booking_id,
          person_id: person_id
        )
        d.transfer_datetime = transfer_datetime
        d.save(validate: false)
      end
    end
  end
  
  def lookup_officers
    unless to_officer_id.blank?
      if ofc = Contact.find_by_id(to_officer_id)
        self.to_officer = ofc.sort_name
        self.to_officer_badge = ofc.badge_no
        self.to_officer_unit = ofc.unit
      end
      self.to_officer_id = nil
    end
    unless from_officer_id.blank?
      if ofc = Contact.find_by_id(from_officer_id)
        self.from_officer = ofc.sort_name
        self.from_officer_badge = ofc.badge_no
        self.from_officer_unit = ofc.unit
      end
      self.from_officer_id = nil
    end
    unless to_agency_id.blank?
      if ag = Contact.find_by_id(to_agency_id)
        self.to_agency = ag.sort_name
      end
      self.to_agency_id = nil
    end
    unless from_agency_id.blank?
      if ag = Contact.find_by_id(from_agency_id)
        self.from_agency = ag.sort_name
      end
      self.from_agency_id = nil
    end
    true
  end
end

# == Schema Information
#
# Table name: transfers
#
#  id                 :integer          not null, primary key
#  booking_id         :integer          not null
#  person_id          :integer          not null
#  from_agency        :string(255)      default(""), not null
#  from_officer       :string(255)      default(""), not null
#  from_officer_unit  :string(255)      default(""), not null
#  from_officer_badge :string(255)      default(""), not null
#  to_agency          :string(255)      default(""), not null
#  to_officer         :string(255)      default(""), not null
#  to_officer_unit    :string(255)      default(""), not null
#  to_officer_badge   :string(255)      default(""), not null
#  remarks            :text             default(""), not null
#  reported_datetime  :datetime
#  created_by         :integer          default(1)
#  updated_by         :integer          default(1)
#  created_at         :datetime
#  updated_at         :datetime
#  transfer_datetime  :datetime
#  transfer_type      :string(255)      default(""), not null
#
# Indexes
#
#  index_transfers_on_booking_id  (booking_id)
#  index_transfers_on_created_by  (created_by)
#  index_transfers_on_person_id   (person_id)
#  index_transfers_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  transfers_booking_id_fk  (booking_id => bookings.id)
#  transfers_created_by_fk  (created_by => users.id)
#  transfers_person_id_fk   (person_id => people.id)
#  transfers_updated_by_fk  (updated_by => users.id)
#
