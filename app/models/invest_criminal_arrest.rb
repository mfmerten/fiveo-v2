class InvestCriminalArrest < ActiveRecord::Base
  belongs_to :invest_criminal, inverse_of: :invest_criminal_arrests        # constraint
  belongs_to :invest_arrest, inverse_of: :invest_criminal_arrests          # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestCriminal Arrests'
  end
end

# == Schema Information
#
# Table name: invest_criminal_arrests
#
#  id                 :integer          not null, primary key
#  invest_criminal_id :integer          not null
#  invest_arrest_id   :integer          not null
#  created_at         :datetime
#  updated_at         :datetime
#
# Indexes
#
#  index_invest_criminal_arrests_on_invest_arrest_id    (invest_arrest_id)
#  index_invest_criminal_arrests_on_invest_criminal_id  (invest_criminal_id)
#
# Foreign Keys
#
#  invest_criminal_arrests_invest_arrest_id_fk    (invest_arrest_id => invest_arrests.id)
#  invest_criminal_arrests_invest_criminal_id_fk  (invest_criminal_id => invest_criminals.id)
#
