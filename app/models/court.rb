class Court < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'
  belongs_to :docket, -> { includes :person }, inverse_of: :courts
  has_many :sentences, dependent: :destroy, autosave: false
  has_many :probations, dependent: :destroy
  
  ## Scopes #############################################
  
  scope :for_letters, ->(c_date,judge,d_type) { where('courts.court_date = ? AND courts.judge = ? and courts.docket_type = ?', c_date, judge, d_type) }
  
  ## Callbacks #############################################
  
  before_save :check_creator
  after_save :generate_next_court
  
  ## Validations #############################################
  
  validates_presence_of :judge
  validates_date :court_date
  validates_date :next_court_date, :overturned_date, allow_blank: true
  validates_docket :docket_id
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_court
  end
  
  def self.desc
    'Court Appearance Table'
  end
  
  ## Instance Methods #############################################
  
  def process_court
    return true if processed?
    # process any pending sentences
    sentences.unprocessed.all.each{|s| s.save(validate: false)}
    # check if anything is remaining
    if sentences.unprocessed.all.empty? && (docket.nil? || docket.da_charges.reject{|c| !c.sentence.nil? || c.dropped_by_da? || c.info_only?}.empty?)
      self.update_attribute(:processed, true)
    end
  end
  
  def status
    txt = ""
    if is_refixed?
      txt = "[Refixed] "
    elsif is_upset_wo_refix?
      txt = "[Upset W/O Refix] "
    elsif is_dismissed?
      txt = "[Dismissed] "
    elsif !plea_type.blank?
      txt = "[Pled #{plea_type}] "
    elsif is_overturned?
      txt = "[Overturned] "
    end
    if bench_warrant_issued?
      txt << "[BW] "
    end
    if bond_forfeiture_issued?
      txt << "[Bond Forfeiture]"
    end
    if psi?
      txt << "[Pre-Sent. Inv.]"
    end
    if txt.blank?
      next_court_date? ? "[Final]" : "[Pending]"
    else
      txt
    end
  end
  
  def to_s
    "[#{AppUtils.format_date(court_date)} -- #{judge} -- #{court_type}]"
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def generate_next_court
    if next_court_date? && next_judge? && next_court_type? && next_docket_type?
      unless Court.where(docket_id: docket_id, court_date: next_court_date, judge: next_judge, court_type: next_court_type, docket_type: next_docket_type).first
        c = Court.new(
          docket_id: docket_id,
          court_date: next_court_date,
          judge: next_judge,
          court_type: next_court_type,
          docket_type: next_docket_type,
          created_by: updated_by,
          updated_by: updated_by
        )
        c.save(validate: false)
      end
    end
  end
end

# == Schema Information
#
# Table name: courts
#
#  id                     :integer          not null, primary key
#  court_date             :date
#  next_court_date        :date
#  bench_warrant_issued   :boolean          default(FALSE), not null
#  bond_forfeiture_issued :boolean          default(FALSE), not null
#  is_upset_wo_refix      :boolean          default(FALSE), not null
#  is_refixed             :boolean          default(FALSE), not null
#  refix_reason           :string(255)      default(""), not null
#  is_dismissed           :boolean          default(FALSE), not null
#  is_overturned          :boolean          default(FALSE), not null
#  overturned_date        :date
#  created_by             :integer          default(1)
#  updated_by             :integer          default(1)
#  created_at             :datetime
#  updated_at             :datetime
#  psi                    :boolean          default(FALSE), not null
#  processed              :boolean          default(FALSE), not null
#  fines_paid             :boolean          default(FALSE), not null
#  notes                  :text             default(""), not null
#  remarks                :text             default(""), not null
#  court_type             :string(255)      default(""), not null
#  next_court_type        :string(255)      default(""), not null
#  docket_type            :string(255)      default(""), not null
#  next_docket_type       :string(255)      default(""), not null
#  plea_type              :string(255)      default(""), not null
#  judge                  :string(255)      default(""), not null
#  next_judge             :string(255)      default(""), not null
#  writ_of_attach         :boolean          default(FALSE), not null
#  not_present            :boolean          default(FALSE), not null
#  docket_id              :integer          not null
#
# Indexes
#
#  index_courts_on_created_by  (created_by)
#  index_courts_on_docket_id   (docket_id)
#  index_courts_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  courts_created_by_fk  (created_by => users.id)
#  courts_updated_by_fk  (updated_by => users.id)
#  fk_rails_ac203dfa9b   (docket_id => dockets.id)
#
