class BookingLog < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :booking, inverse_of: :booking_logs
  
  ## Scopes #############################################
  
  scope :on_or_after, ->(time) { where('booking_logs.start_datetime >= ? OR (booking_logs.start_datetime <= ? AND (booking_logs.stop_datetime >= ? OR booking_logs.stop_datetime IS NULL))',time,time,time) }
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_booking
  end
  
  def self.desc
    'Time Served Log for Booking'
  end
  
  ## Instance Methods #############################################
  
  def hours_served(start=nil,stop=nil)
    return 0 unless(start.nil? || start.is_a?(Time)) && (stop.nil? || stop.is_a?(Time))
    # first, if asking for a stop time on or before the log start time, return 0
    return 0 if !stop.nil? && stop <= start_datetime
    # use start only if it makes sense
    if start.nil? || start_datetime > start
      my_start = start_datetime
    else
      my_start = start
    end
    # use stop only if it makes sense
    if stop.nil? || stop > stop_datetime
      if stop_datetime?
        my_stop = stop_datetime
      else
        my_stop = Time.now
      end
    else
      my_stop = stop
    end
    # calculate the time difference
    AppUtils.hours_diff(my_stop, my_start)
  end
  
  def minutes_served(since_datetime=nil)
    # check since_datetime
    return 0 unless since_datetime.nil? || since_datetime.is_a?(Time)
    # return 0 if not started yet
    return 0 unless start_datetime?
    # if since > stop, return 0
    return 0 if !since_datetime.nil? && !stop_datetime.nil? && since_datetime > stop_datetime
    # figure out whether start_datetime or since_datetime makes sense
    if since_datetime.nil? || start_datetime >= since_datetime
      log_start = start_datetime
    else
      log_start = since_datetime
    end
    
    # figure out which stop time to use
    if stop_datetime.nil?
      log_stop = Time.now
    else
      log_stop = stop_datetime
    end
    # calculate diff in minutes
    AppUtils.minutes_diff(log_stop, log_start)
  end
end

# == Schema Information
#
# Table name: booking_logs
#
#  id                  :integer          not null, primary key
#  booking_id          :integer          not null
#  start_datetime      :datetime
#  stop_datetime       :datetime
#  start_officer       :string(255)      default(""), not null
#  start_officer_badge :string(255)      default(""), not null
#  start_officer_unit  :string(255)      default(""), not null
#  stop_officer        :string(255)      default(""), not null
#  stop_officer_badge  :string(255)      default(""), not null
#  stop_officer_unit   :string(255)      default(""), not null
#  created_at          :datetime
#  updated_at          :datetime
#
# Indexes
#
#  index_booking_logs_on_booking_id  (booking_id)
#
# Foreign Keys
#
#  booking_logs_booking_id_fk  (booking_id => bookings.id)
#
