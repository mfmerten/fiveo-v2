class Announcement < ActiveRecord::Base
  
  ## Associations ###################################################
  
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'
  has_many :announcement_ignores, dependent: :destroy
  has_many :ignorers, through: :announcement_ignores, source: :user

  ## Scopes ###################################################
  
  scope :by_reverse_updated, -> { order updated_at: :desc }
  
  # filter scopes
  scope :with_announcement_id, ->(a_id) { where id: a_id }
  scope :with_created_by, ->(c_by) { where created_by: c_by }
  scope :with_subject, ->(sub) { AppUtils.like self, :subject, sub }
  scope :with_text, ->(txt) { AppUtils.like self, [:body, :subject], txt }
  
  ## Callbacks ###################################################
  
  after_validation :check_creator

  ## Validations ###################################################
  
  validates_presence_of :subject, :body

  ## Class Methods ###################################################
  
  def self.base_perms
    :view_announcement
  end
  
  def self.desc
    "Front page announcements"
  end
  
  # process filter query
  def self.process_query(query)
    return none unless query.is_a?(Hash)
    result = all
    result = result.with_announcement_id(query[:id]) unless query[:id].blank?
    result = result.with_created_by(query[:created_by]) unless query[:created_by].blank?
    result = result.with_subject(query[:subject]) unless query[:subject].blank?
    result = result.with_text(query[:text]) unless query[:text].blank?
    result
  end
end

# == Schema Information
#
# Table name: announcements
#
#  id          :integer          not null, primary key
#  subject     :string(255)      default(""), not null
#  body        :text             default(""), not null
#  created_at  :datetime
#  updated_at  :datetime
#  created_by  :integer          default(1)
#  updated_by  :integer          default(1)
#  unignorable :boolean          default(FALSE), not null
#
# Indexes
#
#  index_announcements_on_created_by  (created_by)
#  index_announcements_on_updated_at  (updated_at)
#  index_announcements_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  announcements_created_by_fk  (created_by => users.id)
#  announcements_updated_by_fk  (updated_by => users.id)
#
