class PawnItemType < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  
  scope :by_type, -> { order :item_type }
  
  before_save :check_creator
  
  validates_uniqueness_of :item_type
  
  ## Class Methods #############################################
  
  def self.base_perms
    :edit_pawn
  end
  
  def self.desc
    'Pawn Item Types'
  end
  
  def self.item_types_for_select
    all.collect{|i| [i.item_type,i.item_type]}
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def fix_signal_code
    self.code = code.upcase
    true
  end
end

# == Schema Information
#
# Table name: pawn_item_types
#
#  id         :integer          not null, primary key
#  item_type  :string(255)      default(""), not null
#  created_by :integer          default(1)
#  updated_by :integer          default(1)
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_pawn_item_types_on_created_by  (created_by)
#  index_pawn_item_types_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  pawn_item_types_created_by_fk  (created_by => users.id)
#  pawn_item_types_updated_by_fk  (updated_by => users.id)
#
