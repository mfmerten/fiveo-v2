class ForbidExemption < ActiveRecord::Base
  belongs_to :forbid, inverse_of: :forbid_exemptions                       # constraint
  belongs_to :person, inverse_of: :forbid_exemptions                       # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    "Forbid Exemptions"
  end
end

# == Schema Information
#
# Table name: forbid_exemptions
#
#  id         :integer          not null, primary key
#  forbid_id  :integer          not null
#  person_id  :integer          not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_forbid_exemptions_on_forbid_id  (forbid_id)
#  index_forbid_exemptions_on_person_id  (person_id)
#
# Foreign Keys
#
#  forbid_exemptions_forbid_id_fk  (forbid_id => forbids.id)
#  forbid_exemptions_person_id_fk  (person_id => people.id)
#
