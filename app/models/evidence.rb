class Evidence < ActiveRecord::Base
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :evidence_owner, class_name: "Person", foreign_key: 'owner_id', inverse_of: :evidences  # constraint
  has_many :offense_evidences, dependent: :destroy                      # constraint
  has_many :offenses, through: :offense_evidences                       # constrained through offense_evidences
  has_many :invest_evidences, dependent: :destroy                       # constraint
  has_many :investigations, through: :invest_evidences                  # constrained through invest_evidences
  has_many :crimes, through: :invest_evidences, source: :crimes         # constrained through invest_evidences
  has_many :criminals, through: :invest_evidences, source: :criminals   # constrained through invest_evidences
  has_many :evidence_locations, dependent: :destroy                     # constraint

  accepts_nested_attributes_for :evidence_locations, reject_if: ->(e) { e['location_to'].blank? || (e['officer_id'].blank? && e['officer'].blank?) || e['remarks'].blank? }

  scope :active, -> { where disposed: nil }
  scope :by_reverse_id, -> { order id: :desc }
  scope :with_case_no, ->(c_no) { where case_no: c_no }
  
  # filter scopes (see Class Methods)
  scope :with_evidence_id, ->(e_id) { where id: e_id }
  scope :with_evidence_number, ->(e_no) { where evidence_no: e_no }
  scope :with_offense_id, ->(o_id) { where offenses: {id: o_id} }
  # with_case_no defined above
  scope :with_owner_id, ->(o_id) { where owner_id: o_id }
  scope :with_description, ->(des) { AppUtils.like self, :description, des }
  scope :with_fingerprint_class, ->(fpc) { AppUtils.like self, :fingerprint_class, fpc }
  scope :with_dna_profile, ->(dna) { AppUtils.like self, :dna_profile, dna }
  scope :with_disposition, ->(dis) { AppUtils.like self, :disposition, dis }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  scope :with_disposed, ->(disp) { AppUtils.for_null self, :disposed, disp }
  scope :with_disposed_date, ->(start,stop) { AppUtils.for_daterange self, :disposed, start: start, stop: stop }
  scope :with_owner_name, ->(name) { AppUtils.for_person self, name, association: :evidence_owner }
  
  attr_accessor :photo_form
  
  has_attached_file(
    :evidence_photo,
    url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/:attachment/:id/:style/:basename.:extension",
    path: ":rails_root/public/system/:attachment/:id/:style/:basename.:extension",
    styles: {thumb: ["80x80", :jpg], medium: ["300x300",:jpg], small: ["150x150",:jpg]},
    default_url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/default_images/no_image_:style.jpg",
    default_style: :original,
    storage: :filesystem
  )

  before_save :check_creator
  before_validation :adjust_case_no
  after_save :check_evidence_no

  validates_associated :evidence_locations
  validates_presence_of :description
  validates_person :owner_id, allow_nil: true
  validates_presence_of :disposed, if: :disposition?
  validates_presence_of :disposition, if: :disposed?
  validates_date :disposed, allow_blank: true
  validates_attachment_content_type :evidence_photo, content_type: ["image/jpg","image/jpeg","image/gif","image/png","image/tiff"]

  ## Class Methods #############################################
  
  def self.base_perms
    :view_evidence
  end
  
  def self.by_location
    evidences = active
    data = []
    evidences.each do |e|
      if e.evidence_locations.empty? || e.evidence_locations.last.location_to.blank?
        data.push(["", e])
      else
        data.push([e.evidence_locations.last.location_to.upcase, e])
      end
    end
    return data.sort
  end
  
  def self.desc
    "Evidence Records"
  end

  # process filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_evidence_id(query[:id]) unless query[:id].blank?
    result = result.with_evidence_number(query[:evidence_number]) unless query[:evidence_number].blank?
    result = result.with_offense_id(query[:offense_id]) unless query[:offense_id].blank?
    result = result.with_case_no(query[:case_no]) unless query[:case_no].blank?
    result = result.with_owner_id(query[:owner_id]) unless query[:owner_id].blank?
    result = result.with_owner_name(query[:owner_name]) unless query[:owner_name].blank?
    result = result.with_description(query[:description]) unless query[:description].blank?
    result = result.with_fingerprint_class(query[:fingerprint_class]) unless query[:fingerprint_class].blank?
    result = result.with_dna_profile(query[:dna_profile]) unless query[:dna_profile].blank?
    result = result.with_disposed(query[:disposed]) unless query[:disposed].blank?
    result = result.with_disposed_date(query[:disposed_from],query[:disposed_to]) unless query[:disposed_from].blank? && query[:disposed_to].blank?
    result = result.with_disposition(query[:disposition]) unless query[:disposition].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def location
    if disposed?
      "Disposed: #{AppUtils.format_date(disposed)} (#{disposition})"
    else
      evidence_locations.empty? ? "Unknown" : evidence_locations.last.location_to
    end
  end
  
  private

  ## Private Instance Methods #############################################
  
  def adjust_case_no
    self.case_no = AppUtils.fix_case(case_no)
    true
  end

  def check_evidence_no
    unless evidence_number?
      self.update_attribute(:evidence_number, id.to_s.rjust(6,'0'))
    end
  end
end

# == Schema Information
#
# Table name: evidences
#
#  id                          :integer          not null, primary key
#  evidence_number             :string(255)      default(""), not null
#  case_no                     :string(255)      default(""), not null
#  description                 :text             default(""), not null
#  owner_id                    :integer
#  remarks                     :text             default(""), not null
#  created_at                  :datetime
#  updated_at                  :datetime
#  created_by                  :integer          default(1)
#  updated_by                  :integer          default(1)
#  evidence_photo_file_name    :string(255)
#  evidence_photo_content_type :string(255)
#  evidence_photo_file_size    :integer
#  evidence_photo_updated_at   :datetime
#  legacy                      :boolean          default(FALSE), not null
#  dna_profile                 :string(255)      default(""), not null
#  fingerprint_class           :string(255)      default(""), not null
#  evidence_locations_count    :integer          default(0), not null
#  disposed                    :date
#  disposition                 :string(255)      default("")
#
# Indexes
#
#  index_evidences_on_case_no          (case_no)
#  index_evidences_on_created_by       (created_by)
#  index_evidences_on_disposed         (disposed)
#  index_evidences_on_evidence_number  (evidence_number)
#  index_evidences_on_owner_id         (owner_id)
#  index_evidences_on_updated_by       (updated_by)
#
# Foreign Keys
#
#  evidences_created_by_fk  (created_by => users.id)
#  evidences_owner_id_fk    (owner_id => people.id)
#  evidences_updated_by_fk  (updated_by => users.id)
#
