class InvestReportArrest < ActiveRecord::Base
  belongs_to :invest_report, inverse_of: :invest_report_arrests            # constraint
  belongs_to :invest_arrest, inverse_of: :invest_report_arrests            # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestReport Arrests'
  end
end

# == Schema Information
#
# Table name: invest_report_arrests
#
#  id               :integer          not null, primary key
#  invest_report_id :integer          not null
#  invest_arrest_id :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_invest_report_arrests_on_invest_arrest_id  (invest_arrest_id)
#  index_invest_report_arrests_on_invest_report_id  (invest_report_id)
#
# Foreign Keys
#
#  invest_report_arrests_invest_arrest_id_fk  (invest_arrest_id => invest_arrests.id)
#  invest_report_arrests_invest_report_id_fk  (invest_report_id => invest_reports.id)
#
