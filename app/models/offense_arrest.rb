class OffenseArrest < ActiveRecord::Base
  belongs_to :offense, inverse_of: :offense_arrests                        # constraint
  belongs_to :arrest, inverse_of: :offense_arrests                         # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'Offense Arrests'
  end
end

# == Schema Information
#
# Table name: offense_arrests
#
#  id         :integer          not null, primary key
#  offense_id :integer          not null
#  arrest_id  :integer          not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_offense_arrests_on_arrest_id   (arrest_id)
#  index_offense_arrests_on_offense_id  (offense_id)
#
# Foreign Keys
#
#  offense_arrests_arrest_id_fk   (arrest_id => arrests.id)
#  offense_arrests_offense_id_fk  (offense_id => offenses.id)
#
