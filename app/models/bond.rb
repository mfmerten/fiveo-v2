class Bond < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'
  belongs_to :surety, class_name: 'Person', foreign_key: 'surety_id', inverse_of: :bonds_as_surety
  belongs_to :arrest, inverse_of: :bonds
  belongs_to :person, inverse_of: :bonds
  has_many :hold_bonds, dependent: :destroy
  has_many :holds, through: :hold_bonds
  
  ## Attributes #############################################
  
  attr_accessor :bondsman_id, :bonding_company_id
  
  ## Scopes #############################################
  
  scope :active, -> { where status: '', final: false }
  scope :by_reverse_date, -> { order issued_datetime: :desc }
  scope :by_name, -> { includes(:person).order('people.family_name,people.given_name') }
  
  # filter scopes (see Class Methods below)
  scope :with_bond_id, ->(b_id) { where id: b_id }
  scope :with_bondsman_name, ->(b_name) { AppUtils.like self, :bondsman_name, b_name }
  scope :with_bond_company, ->(b_co) { AppUtils.like self, :bond_company, b_co }
  scope :with_surety_id, ->(s_id) { where surety_id: s_id }
  scope :with_person_id, ->(p_id) { where person_id: p_id }
  scope :with_arrest_id, ->(a_id) { where arrest_id: a_id }
  scope :with_bond_no, ->(b_no) { AppUtils.like self, :bond_no, b_no }
  scope :with_bond_type, ->(b_type) { AppUtils.like self, :bond_type, b_type }
  scope :with_active, -> { where status: '', final: false }
  scope :with_status, ->(stat) { AppUtils.like self, :status, stat }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  scope :with_surety_name, ->(name) { AppUtils.for_person self, name, association: :surety }
  scope :with_person_name, ->(name) { AppUtils.for_person self, name, association: :person }
  scope :with_issued_date, ->(start,stop) { AppUtils.for_daterange self, :issued_datetime, start: start, stop: stop }
  
  ## Callbacks #############################################
  
  after_validation :check_creator, :check_bondsman, :check_other_agency, :check_bond_no
  before_validation :lookup_bondsman, :check_surety, :check_arrest_id

  ## Validations #############################################
  
  validates_numericality_of :bond_amt, greater_than_or_equal_to: 0, less_than: 10 ** 10
  validates_datetime :issued_datetime
  validates_datetime :status_datetime, allow_blank: true
  validates_person :person_id
  validates_person :surety_id, unless: :bondsman_name?
  
  ## Class Methods #############################################
  
  def self.statuses
    ['Forfeited', 'Revoked', 'Surrendered', 'For Other Agency']
  end
  
  def self.types
    [
      'Cash Bond', 'City Charges', 'No Bond', 'O.R. Bond', 'Out of Parish',
      'Professional', 'Property', 'Ticket', 'Unsecured'
    ]
  end
  
  def self.base_perms
    :view_bond
  end
  
  def self.desc
    'Bond Database'
  end
  
  # collects bonds that are "satisfied", but not yet finalized
  # This data is used by several reports.
  def self.satisfied_but_unfinalized
    found = {district: [], probation: [], no_holds: []}
    # get all active bonds (no status and not final)
    active.each do |b|
      if b.holds.empty?
        # detached from holds
        found[:no_holds].push(b)
        next
      end
      if b.holds.last.hold_type =~ /Probation/
        found[:probation].push(b)
        next
      end
      # for the rest, we skip unless for district arrest
      arr = b.arrest
      if arr.nil?
        unless b.holds.empty?
          arr = b.holds.last.arrest
        end
      end
      # no arrest
      next if arr.nil?
      # not for district/bondable
      next if arr.district_charges.empty? || b.holds.last.hold_type != 'Bondable'
      # check to see if arrest is final (all charges dropped or sentenced)
      if arr.is_final?
        found[:district].push(b)
      end
    end
    found
  end
  
  # process the query
  def self.process_query(query)
    return none unless query.is_a?(Hash)
    result = all
    result = result.with_bond_id(query[:id]) unless query[:id].blank?
    result = result.with_bondsman_name(query[:bondsman_name]) unless query[:bondsman_name].blank?
    result = result.with_bond_company(query[:bond_company]) unless query[:bond_company].blank?
    result = result.with_surety_id(query[:surety_id]) unless query[:surety_id].blank?
    result = result.with_surety_name(query[:surety_name]) unless query[:surety_name].blank?
    result = result.with_person_id(query[:person_id]) unless query[:person_id].blank?
    result = result.with_person_name(query[:person_name]) unless query[:person_name].blank?
    result = result.with_arrest_id(query[:arrest_id]) unless query[:arrest_id].blank?
    result = result.with_bond_no(query[:bond_no]) unless query[:bond_no].blank?
    result = result.with_issued_date(query[:issued_date_from],query[:issued_date_to]) unless query[:issued_date_from].blank? && query[:issued_date_to].blank?
    result = result.with_bond_type(query[:bond_type]) unless query[:bond_type].blank?
    result = result.with_active unless query[:active].blank?
    result = result.with_status(query[:status]) unless query[:status].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def bondsman_address
    [bondsman_address1, bondsman_address2, bondsman_address3].reject{|a| a.blank?}.join(', ')
  end
  
  # attempts to return the most significant booking for this bond.
  def booking
    return holds.last.booking unless holds.empty? || holds.last.booking.nil?
    return arrest.booking unless arrest.nil?
    nil
  end
  
  def check_recallable(new_status)
    book = person.bookings.last
    # check argument
    return "Invalid Recall Reason: #{new_status}" unless ['revoke','surrender','forfeit'].include?(new_status)
    # make sure person is valid
    if new_status == 'surrender'
      return "No Open Booking for Person" if book.nil? || book.release_datetime?
    end
    # cannot modify final bonds
    return "Bond is Final" if final?
    # cannot recall inactive bonds
    return "Bond Is #{status}" if status?
    # check conditions for open bookings
    if !book.nil? && !book.release_datetime?
      # check that this bond has a valid hold linked to it
      return "Invalid Or Unknown Type Hold" if holds.empty? || holds.last.hold_type.blank?
      # if bondable hold, must be able to locate original arrest
      # all other hold types will be dealing with single hold<=>bond pair
      if holds.last.hold_type == 'Bondable'
        return "No Valid Arrest Can Be Found" if arrest.nil? && holds.last.arrest.nil?
      end
    end
    nil
  end
  
  def check_undo_recallable
    # check bond state
    return "Invalid Bond Status!" unless ['Revoked', 'Surrendered', 'Forfeited'].include?(status)
    # check for holds/arrests
    return "Bond not linked!" if holds.empty? && arrest.nil?
    # only things working right now are un-revoke, un-forfeit but only if not final
    return nil if ["Revoked", "Forfeited"].include?(status) && !final?
    # finish this
    return "Not Implemented!"
  end
  
  # called to forfeit, revoke or surrender a bond
  def recall(new_status, user_id=nil, *args)
    options = HashWithIndifferentAccess.new(booking_id: nil)
    options.update(args.extract_options!)
    # if booking_id is supplied, this is being called from booking - do not 
    # need to do checking first.
    if options[:booking_id].blank?
      # first, check everything
      if retval = check_recallable(new_status)
        # something not right, pass on the error
        return retval
      end
      # this requires user_id, check if valid
      if user_id.blank? || !(user_id.is_a?(String) || user_id.is_a?(Integer))
        return "Supplied User ID missing or wrong type!"
      end
      unless user = User.find_by_id(user_id)
        return "Supplied User ID not found!"
      end
      # user ok - permissions must be checked by whatever calls this method, not doing it here
      #
      book = person.bookings.last
    else
      book = Booking.find_by_id(options[:booking_id])
      return "Invalid Booking Specified" if book.nil?
      user = book.updated_by
    end
    retval = nil
    arr = [arrest, holds.last.arrest].compact.first
    if !book.nil? && !book.release_datetime?
      # revoke/forfeit/surrender and booking is open... process booking
      if holds.last.hold_type == 'bondable'
        # bondable hold
        # has the booking changed?
        if book == holds.last.booking
          # same booking - reprocess existing bondable hold
          if holds.last.cleared_datetime?
            # make sure arrest does not already have a 72 hour or No Bond hold on this booking
            h72 = arr.holds.of_type("72 Hour").with_booking_id(book.id).last
            nobond = arr.holds.of_type("No Bond").with_booking_id(book.id).last
            if (h72.nil? || h72.cleared_datetime?) && (nobond.nil? || nobond.cleared_datetime?)
              self.holds.last.clear_release(user.id)
              self.holds.last.update_attribute(:remarks, self.holds.last.remarks + "\nReopened by #{new_status.titleize} of Bond ID #{self.id}")
            end
          end
        else
          # want to add new 72, but need to check for existing one or no bond first
          h72 = arr.holds.of_type("72 Hour").with_booking_id(book.id).last
          nobond = arr.holds.of_type("No Bond").with_booking_id(book.id).last
          if (h72.nil? || h72.cleared_datetime?) && (nobond.nil? || nobond.cleared_datetime?)
            # clear the 72 hour information
            arr.hour_72_judge = nil
            arr.hour_72_datetime = nil
            arr.attorney = nil
            arr.condition_of_bond = nil
            arr.bond_amt = 0
            arr.bondable = false
            arr.updated_by = user.id
            arr.save(validate: false)
            # note that saving the arrest will cause a proper 72 hour hold to
            # be added to the booking unless there is already a cleared one
            newh72 = arr.holds.of_type("72 Hour").with_booking_id(book.id).last
            if newh72.nil? || newh72.cleared_datetime? 
              # need to force adding of a new one
              newh = Hold.add(book.id, arr.id, '72 Hour')
              newh.update_attribute(:remarks, "Added by #{new_status.titleize} of Bond ID #{self.id}")
            end
          end
        end
      else
        # anything other than bondable
        if book == holds.last.booking
          # same booking, reactivate current hold
          if holds.last.cleared_datetime?
            self.holds.last.clear_release(updated_by)
            self.holds.last.update_attribute(:remarks, self.holds.last.remarks + "\nReopened by #{new_status.titleize} of Bond ID #{self.id}")
          end
        else
          # add a new hold of the same type unless one already exists, such as
          # having recalled another bond for the same arrest
          if arr.nil?
            # no arrest linked
            newhold = Hold.add(book.id, arrest_id, holds.last.hold_type)
            newhold.update_attribute(:remarks, "Added by #{new_status.titleize} of Bond ID #{self.id}")
            self.holds << newhold
          else
            existing = arr.holds.of_type(holds.last.hold_type).with_booking_id(book.id).last
            if existing.nil? || existing.cleared_datetime?
              # arrest linked, but any existing hold has been cleared, make a new one
              newhold = Hold.add(book.id, arrest_id, holds.last.hold_type)
              newhold.update_attribute(:remarks, "Added by #{new_status.titleize} of Bond ID #{self.id}")
              self.holds << newhold
            end
          end
        end
      end
      self.update_attribute(:final, true)
    end
    # process status for this bond
    temp = {'surrender' => 'Surrendered', 'revoke' => 'Revoked', 'forfeit' => 'Forfeited'}
    self.update_attribute(:status, temp[new_status])
    self.update_attribute(:status_datetime, Time.now)
    self.update_attribute(:updated_by, user.id)
    nil
  end
  
  def status_name
    "#{status? ? status : 'Valid'}#{final? ? ' (Final)' : ''}"
  end
  
  def surety_name
    if bondsman_name?
      "Bondsman: #{bondsman_name} (#{bond_company? ? bond_company : 'Unknown'})"
    elsif !surety.nil?
      "#{surety.sort_name}#{bond_type.blank? ? '' : ' (' + bond_type + ')'}"
    else
      "Unknown"
    end
  end
  
  def undo_recall(user_id)
    # first, check everything
    if retval = check_undo_recallable
      # something not right, pass on the error
      return retval
    end
    # this requires user_id, check if valid
    if user_id.blank? || !(user_id.is_a?(String) || user_id.is_a?(Integer))
      return "Supplied User ID missing or wrong type!"
    end
    unless user = User.find_by_id(user_id)
      return "Supplied User ID not found!"
    end
    # user ok - permissions must be check by whatever calls this method, not doing it here
    #
    # handle simplest case... bond recalled but not final
    # this happens when revoke or forfeit done and not yet booked
    unless final?
      # just make the bond active again
      self.status = ''
      self.status_datetime = nil
      self.updated_by = user_id
      self.save(validate: false)
      return nil
    end
    # bond is recalled and final meaning that arrests/holds/bookings are involved.
    #
    
    # need to handle case where hold is bondable but new booking has no-bond hold
    # complete this
    return "Not Implemented Yet"
  end
  
  def check_arrest_id
    if arrest(true).nil?
      if !holds.empty? && !holds.last.arrest.nil?
        self.arrest_id = holds.last.arrest_id
      end
    end
    true
  end
  
  def check_bond_no
    if bond_no.blank?
      self.bond_no = id.to_s.rjust(6,'0')
    end
    true
  end

  def check_bondsman
    self.bond_type = 'Professional' unless bondsman_name.blank?
    true
  end
  
  def lookup_bondsman
    unless bondsman_id.blank?
      if bman = Contact.find_by_id(bondsman_id)
        self.bondsman_name = bman.display_name
        self.bondsman_address1 = bman.address_line1
        self.bondsman_address2 = bman.address_line2
        self.bondsman_address3 = bman.address_line3
      end
      self.bondsman_id = nil
    end
    unless bonding_company_id.blank?
      if bco = Contact.find_by_id(bonding_company_id)
        self.bond_company = bco.display_name
      end
      self.bonding_company_id = nil
    end
    true
  end
  
  def check_other_agency
    if status == 'For Other Agency'
      self.final = true
    end
    true
  end
  
  def check_surety
    retval = true
    if !bondsman_id.blank?
      unless Contact.find_by(id: bondsman_id)
        self.errors.add(:bondsman_id, "must be a current valid Bondsman ID or blank!")
        retval = false
      end
    else
      if surety_id? && bondsman_name?
        self.errors.add(:bondsman_name, "Only one of the Bondsman or the Surety must be specifed, not both!")
        retval = false
      elsif !surety_id? && !bondsman_name?
        self.errors.add(:bondsman_name, "Either the Bondsman or the Surety must be specified!")
        retval = false
      end
    end
    retval
  end
end

# == Schema Information
#
# Table name: bonds
#
#  id                :integer          not null, primary key
#  surety_id         :integer
#  person_id         :integer          not null
#  arrest_id         :integer
#  bond_amt          :decimal(12, 2)   default(0.0), not null
#  remarks           :text             default(""), not null
#  power             :string(255)      default(""), not null
#  bond_no           :string(255)      default(""), not null
#  created_by        :integer          default(1)
#  updated_by        :integer          default(1)
#  created_at        :datetime
#  updated_at        :datetime
#  issued_datetime   :datetime
#  status_datetime   :datetime
#  final             :boolean          default(FALSE), not null
#  status            :string(255)      default(""), not null
#  bond_type         :string(255)      default(""), not null
#  bondsman_name     :string(255)      default(""), not null
#  bond_company      :string(255)      default(""), not null
#  bondsman_address1 :string(255)      default(""), not null
#  bondsman_address2 :string(255)      default(""), not null
#  bondsman_address3 :string(255)      default(""), not null
#
# Indexes
#
#  index_bonds_on_arrest_id   (arrest_id)
#  index_bonds_on_created_by  (created_by)
#  index_bonds_on_person_id   (person_id)
#  index_bonds_on_surety_id   (surety_id)
#  index_bonds_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  bonds_arrest_id_fk   (arrest_id => arrests.id)
#  bonds_created_by_fk  (created_by => users.id)
#  bonds_person_id_fk   (person_id => people.id)
#  bonds_surety_id_fk   (surety_id => people.id)
#  bonds_updated_by_fk  (updated_by => users.id)
#
