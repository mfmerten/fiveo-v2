class Forbid < ActiveRecord::Base
  belongs_to :creator, class_name: "User", foreign_key: "created_by"    # constraint with nullify
  belongs_to :updater, class_name: "User", foreign_key: "updated_by"    # constraint with nullify
  belongs_to :call, inverse_of: :forbids                                # constraint
  belongs_to :person, class_name: "Person", foreign_key: "forbidden_id", inverse_of: :forbids  # constraint
  has_many :forbid_exemptions, dependent: :destroy                      # constraint
  has_many :exempted_people, through: :forbid_exemptions, source: :person  # constrained through forbid_exemption
  has_many :forbid_possibles, dependent: :destroy
  has_many :ppeople, through: :forbid_possibles, source: :person
  has_many :offense_forbids, dependent: :destroy                        # constraint
  has_many :offenses, through: :offense_forbids                         # constrained through offense_forbids
  
  scope :active, -> { where recall_date: nil }
  scope :by_name, -> { order :family_name, :given_name }
  scope :with_case_no, ->(c_no) { where case_no: c_no }
  
  # filter scopes (see Class Methods)
  scope :with_forbid_id, ->(f_id) { where id: f_id }
  scope :with_forbidden_id, ->(f_id) { where forbidden_id: f_id }
  scope :with_details, ->(det) { AppUtils.like self, :details, det }
  scope :with_complainant, ->(com) { AppUtils.for_person self, com, simple: true, field: :complainant }
  scope :with_address, ->(addr) { AppUtils.like self, [:comp_street, :comp_city_state_zip, :forbid_street, :forbid_city_state_zip], addr }
  # with_case_no defined above
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  scope :with_request_date, ->(start,stop) { AppUtils.for_daterange self, :request_date, start: start, stop: stop, date: true }
  scope :with_signed_date, ->(start,stop) { AppUtils.for_daterange self, :signed_date, start: start, stop: stop, date: true }
  scope :with_recall_date, ->(start,stop) { AppUtils.for_daterange self, :recall_date, start: start, stop: stop, date: true }
  scope :with_receiving_officer, ->(name,unit,badge) { AppUtils.for_officer self, :receiving_officer, name, unit, badge }
  scope :with_serving_officer, ->(name,unit,badge) { AppUtils.for_officer self, :serving_officer, name, unit, badge }
  scope :with_forbidden_name, ->(n) { AppUtils.for_person self, n }
  
  attr_accessor :full_name, :receiving_officer_id, :serving_officer_id

  before_save :check_creator
  after_save :update_person_matches
  before_validation :split_name, :adjust_case_no, :lookup_officers
  after_save :send_new_messages
  after_update :send_recall_messages

  validates_presence_of :receiving_officer, :details, :complainant, :family_name, :given_name, :comp_street, :comp_city_state_zip
  validates_date :request_date
  validates_date :signed_date, :recall_date, allow_blank: true
  validates_presence_of :serving_officer, if: :signed_date?
  validates_presence_of :forbidden_id, if: :signed_date?, message: 'is required for signed forbids.'
  validates_person :forbidden_id, allow_nil: true
  validates_phone :comp_phone
  validates_phone :forbid_phone, allow_blank: true
  validates_call :call_id, allow_nil: true

  ## Class Methods #############################################
  
  def self.base_perms
    :view_forbid
  end
  
  def self.desc
    "Forbidden To Enter forms"
  end
  
  def self.forbids_for(person_id)
    # make sure person_id is valid
    return none unless p = Person.find_by(id: person_id)
    
    active.where(forbidden_id: nil).with_forbidden_name(p.sort_name)
  end
  
  # process filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_forbid_id(query[:id]) unless query[:id].blank?
    result = result.with_forbidden_id(query[:forbidden_id]) unless query[:forbidden_id].blank?
    result = result.with_details(query[:details]) unless query[:details].blank?
    result = result.with_complainant(query[:complainant]) unless query[:complainant].blank?
    result = result.with_address(query[:address]) unless query[:address].blank?
    result = result.with_request_date(query[:request_date_from],query[:request_date_to]) unless query[:request_date_from].blank? && query[:request_date_to].blank?
    result = result.with_receiving_officer(query[:receiving_officer], query[:receiving_officer_unit], query[:receiving_officer_badge]) unless query[:receiving_officer].blank? && query[:receiving_officer_unit].blank? && query[:receiving_officer_badge].blank?
    result = result.with_case_no(query[:case_no]) unless query[:case_no].blank?
    result = result.with_signed_date(query[:signed_date_from], query[:signed_date_to]) unless query[:signed_date_from].blank? && query[:signed_date_to].blank?
    result = result.with_serving_officer(query[:serving_officer], query[:serving_officer_unit], query[:serving_officer_badge]) unless query[:serving_officer].blank? && query[:serving_officer_unit].blank? && query[:serving_officer_badge].blank?
    result = result.with_recall_date(query[:recall_date_from], query[:recall_date_to]) unless query[:recall_date_from].blank? && query[:recall_date_to].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result = result.with_forbidden_name(query[:name]) unless query[:name].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def comp_address
    [comp_street, comp_city_state_zip].reject{|a| a.blank?}.join(', ')
  end
  
  def forbid_address
    [forbid_street, forbid_city_state_zip].reject{|a| a.blank?}.join(', ')
  end
  
  def display_name
    n = Namae::Name.new(family: family_name, given: given_name, suffix: suffix)
    n.display_order
  end
  
  def sort_name
    n = Namae::Name.new(family: family_name, given: given_name, suffix: suffix)
    n.sort_order
  end
  
  def update_person_matches
    self.ppeople.clear
    matches = Person.for_forbid(id)
    matches.each do |p|
      self.ppeople << p unless p.exempted_forbids.include?(self)
    end
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def adjust_case_no
    self.case_no = AppUtils.fix_case(case_no)
    return true
  end
  
  def lookup_officers
    unless receiving_officer_id.blank?
      if ofc = Contact.find_by_id(receiving_officer_id)
        self.receiving_officer = ofc.sort_name
        self.receiving_officer_badge = ofc.badge_no
        self.receiving_officer_unit = ofc.unit
      end
      self.receiving_officer_id = nil
    end
    unless serving_officer_id.blank?
      if ofc = Contact.find_by_id(serving_officer_id)
        self.serving_officer = ofc.sort_name
        self.serving_officer_badge = ofc.badge_no
        self.serving_officer_unit = ofc.unit
      end
      self.serving_officer_id = nil
    end
    true
  end
  
  def send_new_messages
    receivers = User.where('notify_forbid_new IS TRUE').all
    receivers.each do |r|
      next unless r.can?(self.class.base_perms)
      txt = AutomatedMessagesController.new.notify(self, 'new')
      Message.create(receiver_id: r.id, sender_id: updated_by, subject: "New Forbid (ID: #{id})", body: txt)
    end
  end
  
  def send_recall_messages
    if recall_date? && previous_changes.include?('recall_date')
      receivers = User.where('notify_forbid_recalled IS TRUE').all
      receivers.each do |r|
        next unless r.can?(self.class.base_perms)
        txt = AutomatedMessagesController.new.notify(self, 'recalled')
        Message.create(receiver_id: r.id, sender_id: updated_by, subject: "Recalled Forbid (ID: #{id})", body: txt)
      end
    end
  end
  
  def split_name
    unless full_name.nil? || full_name.blank?
      n = Namae::Name.parse(full_name)
      self.family_name = n.family || ''
      self.given_name = n.given || ''
      self.suffix = n.suffix || ''
    end
    unless complainant.nil? || complainant.blank?
      self.complainant = AppUtils.parse_name(complainant,true)
    end
    return true
  end
end

# == Schema Information
#
# Table name: forbids
#
#  id                      :integer          not null, primary key
#  call_id                 :integer
#  case_no                 :string(255)      default(""), not null
#  forbidden_id            :integer
#  request_date            :date
#  receiving_officer       :string(255)      default(""), not null
#  receiving_officer_unit  :string(255)      default(""), not null
#  signed_date             :date
#  serving_officer         :string(255)      default(""), not null
#  serving_officer_unit    :string(255)      default(""), not null
#  recall_date             :date
#  details                 :text             default(""), not null
#  remarks                 :text             default(""), not null
#  created_by              :integer          default(1)
#  updated_by              :integer          default(1)
#  created_at              :datetime
#  updated_at              :datetime
#  receiving_officer_badge :string(255)      default(""), not null
#  serving_officer_badge   :string(255)      default(""), not null
#  complainant             :string(255)      default(""), not null
#  comp_street             :string(255)      default(""), not null
#  comp_city_state_zip     :string(255)      default(""), not null
#  comp_phone              :string(255)      default(""), not null
#  family_name             :string(255)      default(""), not null
#  given_name              :string(255)      default(""), not null
#  suffix                  :string(255)      default(""), not null
#  forbid_street           :string(255)      default(""), not null
#  forbid_city_state_zip   :string(255)      default(""), not null
#  forbid_phone            :string(255)      default(""), not null
#  legacy                  :boolean          default(FALSE), not null
#
# Indexes
#
#  index_forbids_on_call_id                     (call_id)
#  index_forbids_on_case_no                     (case_no)
#  index_forbids_on_created_by                  (created_by)
#  index_forbids_on_family_name_and_given_name  (family_name,given_name)
#  index_forbids_on_forbidden_id                (forbidden_id)
#  index_forbids_on_updated_by                  (updated_by)
#
# Foreign Keys
#
#  forbids_call_id_fk       (call_id => calls.id)
#  forbids_created_by_fk    (created_by => users.id)
#  forbids_forbidden_id_fk  (forbidden_id => people.id)
#  forbids_updated_by_fk    (updated_by => users.id)
#
