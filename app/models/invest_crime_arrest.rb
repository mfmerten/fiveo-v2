class InvestCrimeArrest < ActiveRecord::Base
  belongs_to :invest_crime, inverse_of: :invest_crime_arrests              # constraint
  belongs_to :invest_arrest, inverse_of: :invest_crime_arrests             # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestCrime Arrests'
  end
end

# == Schema Information
#
# Table name: invest_crime_arrests
#
#  id               :integer          not null, primary key
#  invest_crime_id  :integer          not null
#  invest_arrest_id :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_invest_crime_arrests_on_invest_arrest_id  (invest_arrest_id)
#  index_invest_crime_arrests_on_invest_crime_id   (invest_crime_id)
#
# Foreign Keys
#
#  invest_crime_arrests_invest_arrest_id_fk  (invest_arrest_id => invest_arrests.id)
#  invest_crime_arrests_invest_crime_id_fk   (invest_crime_id => invest_crimes.id)
#
