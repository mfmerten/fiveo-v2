class OffenseWitness < ActiveRecord::Base
  belongs_to :offense, inverse_of: :offense_witnesses                      # constraint
  belongs_to :person, inverse_of: :offense_witnesses                       # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'Offense Witnesses'
  end
end

# == Schema Information
#
# Table name: offense_witnesses
#
#  id         :integer          not null, primary key
#  offense_id :integer          not null
#  person_id  :integer          not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_offense_witnesses_on_offense_id  (offense_id)
#  index_offense_witnesses_on_person_id   (person_id)
#
# Foreign Keys
#
#  offense_witnesses_offense_id_fk  (offense_id => offenses.id)
#  offense_witnesses_person_id_fk   (person_id => people.id)
#
