class Message < ActiveRecord::Base
  belongs_to :sender, class_name: "User", foreign_key: "sender_id"      # constraint
  belongs_to :receiver, class_name: "User", foreign_key: "receiver_id"  # constraint
  
  scope :inbox, -> { where(receiver_deleted: false).order(created_at: :desc) }
  scope :outbox, -> { where(sender_deleted: false).order(created_at: :desc) }
  scope :trashbin, -> { where(receiver_deleted: true, receiver_purged: false).order(created_at: :desc) }
  
  before_create :check_for_support_ticket_replies
  after_create :write_to_log, :send_any_emails

  validates_presence_of :subject, :body
  validates_user :receiver_id, :sender_id
  
  ## Class Methods #############################################
  
  def self.desc
    "User to User messages"
  end
  
  def self.purge
    if SiteConfig.message_retention.blank?
      ret_policy = 0
    else
      ret_policy = SiteConfig.message_retention
    end
    # allow 0 or blank to indicate that user preferences are allowed
    if ret_policy == 0
      # user preferences allowed, do it the hard way
      all.each do |m|
        unless m.receiver_purged?
          if m.receiver.message_retention? && m.receiver.message_retention > 0 && m.receiver.message_retention.days.ago > m.created_at
            m.receiver_deleted = true
            m.receiver_purged = true
          end
        end
        unless m.sender_purged?
          if m.sender.message_retention? && m.sender.message_retention > 0 && m.receiver.message_retention.days.ago > m.created_at
            m.sender_deleted = true
            m.sender_purged = true
          end
        end
        if m.changed?
          m.save(validate: false)
        end
        # delete message if both sender and receiver has purged message
        m.purge
      end
    else
      # site config is set, can do a much faster delete_all
      delete_all(["created_at < ?",ret_policy.days.ago])
    end
  end

  ## Instance Methods #############################################
  
  def mark_message_read(user)
    if user.id == receiver_id
      self.read_at = Time.now
      self.save(validate: false)
    end
  end

  def purge
    if sender_purged && receiver_purged
      self.destroy
    end
  end

  private

  ## Private Instance Methods #############################################
  
  def check_for_support_ticket_replies
    return false if subject =~ /^Re:.*Ticket #\d+/
    return true
  end

  def write_to_log
    m = MessageLog.new
    attr_hash = attributes
    attr_hash.delete('id')
    m.update_attributes(attr_hash)
    m.save(validate: false)
  end

  def send_any_emails
    if !receiver.nil? && receiver.send_email? && !receiver.contact.nil? && receiver.contact.pri_email?
      UserMailer.message_email(self).deliver_now
    end
  end
end

# == Schema Information
#
# Table name: messages
#
#  id               :integer          not null, primary key
#  receiver_deleted :boolean          default(FALSE), not null
#  receiver_purged  :boolean          default(FALSE), not null
#  sender_deleted   :boolean          default(FALSE), not null
#  sender_purged    :boolean          default(FALSE), not null
#  read_at          :datetime
#  receiver_id      :integer          not null
#  sender_id        :integer          not null
#  subject          :string(255)      default(""), not null
#  body             :text             default(""), not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_messages_on_receiver_id  (receiver_id)
#  index_messages_on_sender_id    (sender_id)
#
# Foreign Keys
#
#  messages_receiver_id_fk  (receiver_id => users.id)
#  messages_sender_id_fk    (sender_id => users.id)
#
