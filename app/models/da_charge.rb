class DaCharge < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :docket, inverse_of: :da_charges
  belongs_to :arrest_charge, class_name: 'Charge', foreign_key: 'charge_id', inverse_of: :da_charges
  has_one :sentence, dependent: :destroy, inverse_of: :da_charge, autosave: false
  belongs_to :arrest, inverse_of: :da_charges
  has_one :probation, inverse_of: :da_charge, dependent: :destroy

  ## Callbacks #############################################
  
  after_save :check_dropped, :check_info_only

  ## Validations #############################################
  
  validates_presence_of :count, :charge

  ## Class Methods #############################################
  
  def self.desc
    'Charges for Dockets'
  end
  
  ## Instance Methods #############################################
  
  def is_final?
    # ignore charges if for invalid or miscellaneous dockets
    return true if docket.nil? || docket.misc?
    # if charge is dropped, its final
    return true if dropped_by_da?
    # if charge is marked info_only, ignore it
    return true if info_only?
    # now depends on state of sentence processing
    # a processed sentence means final resolution in place
    !sentence.nil? && sentence.processed?
  end
  
  def time_served_in_hours
    # if arrest linked, return arrest hours served
    unless arrest.nil?
      return arrest.hours_served
    end
    # if arrest_charge linked, go through that to get arrest hours served
    unless arrest_charge.nil? || arrest_charge.arrest.nil?
      return arrest_charge.arrest.hours_served
    end
    # everything else (including citations) have 0 time served
    return 0
  end
  
  def to_s
    "#{charge}(#{count})"
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def check_dropped
    if dropped_by_da? && !processed?
      self.update_attribute(:processed, true)
    end
  end
  
  def check_info_only
    if info_only? && !processed?
      self.update_attribute(:processed, true)
    end
  end
end

# == Schema Information
#
# Table name: da_charges
#
#  id            :integer          not null, primary key
#  docket_id     :integer          not null
#  charge_id     :integer
#  count         :integer          default(1), not null
#  charge        :string(255)      default(""), not null
#  dropped_by_da :boolean          default(FALSE), not null
#  processed     :boolean          default(FALSE), not null
#  created_at    :datetime
#  updated_at    :datetime
#  arrest_id     :integer
#  info_only     :boolean          default(FALSE), not null
#  da_diversion  :boolean          default(FALSE), not null
#
# Indexes
#
#  index_da_charges_on_arrest_id  (arrest_id)
#  index_da_charges_on_charge_id  (charge_id)
#  index_da_charges_on_docket_id  (docket_id)
#
# Foreign Keys
#
#  da_charges_arrest_id_fk  (arrest_id => arrests.id)
#  da_charges_charge_id_fk  (charge_id => charges.id)
#  da_charges_docket_id_fk  (docket_id => dockets.id)
#
