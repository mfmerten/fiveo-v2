class OffenseSuspect < ActiveRecord::Base
  belongs_to :offense, inverse_of: :offense_suspects                       # constraint
  belongs_to :person, inverse_of: :offense_suspects                        # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'Offense Suspects'
  end
end

# == Schema Information
#
# Table name: offense_suspects
#
#  id         :integer          not null, primary key
#  offense_id :integer          not null
#  person_id  :integer          not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_offense_suspects_on_offense_id  (offense_id)
#  index_offense_suspects_on_person_id   (person_id)
#
# Foreign Keys
#
#  offense_suspects_offense_id_fk  (offense_id => offenses.id)
#  offense_suspects_person_id_fk   (person_id => people.id)
#
