class Arrest < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: "User", foreign_key: "created_by"
  belongs_to :updater, class_name: "User", foreign_key: "updated_by"
  belongs_to :person, inverse_of: :arrests
  belongs_to :disposition, inverse_of: :arrests
  has_many :offense_arrests, dependent: :destroy
  has_many :offenses, through: :offense_arrests
  has_many :warrants, dependent: :nullify
  has_many :holds, dependent: :destroy, autosave: true
  has_many :bookings, -> { distinct.order(:id) }, through: :holds
  has_many :bonds, dependent: :nullify
  has_many :revocations, dependent: :nullify
  has_many :da_charges, dependent: :nullify
  has_many :da_charge_dockets, -> { distinct }, through: :da_charges, class_name: 'Docket', source: :docket
  has_many :charges
  has_many :charge_da_charges, -> { distinct }, through: :charges, source: :da_charges, class_name: 'DaCharge'
  has_many :charge_dockets, -> { distinct }, through: :charge_da_charges, class_name: 'Docket', source: :docket
  has_many :city_charges, -> { where charge_type: 'City' }, class_name: 'Charge'
  has_many :district_charges, -> { where charge_type: 'District' }, class_name: 'Charge'
  has_many :other_charges, -> { where charge_type: 'Other' }, class_name: 'Charge'
  has_many :untyped_charges, -> { where charge_type: '' }, class_name: 'Charge'
  has_many :officers, class_name: 'ArrestOfficer', dependent: :destroy
  has_many :arrest_charges, -> { where warrant_id: nil }, class_name: 'Charge'
  has_many :invest_arrests, dependent: :destroy
  has_many :investigations, through: :invest_arrests
  has_many :crimes, through: :invest_arrests, source: :crimes
  has_many :criminals, through: :invest_arrests, source: :criminals
  has_many :reports, through: :invest_arrests, source: :reports
    
  ## Attributes #############################################
  
  accepts_nested_attributes_for :officers, allow_destroy: true, reject_if: ->(o) { o['officer'].blank? && o['officer_id'].blank? }
  accepts_nested_attributes_for :arrest_charges, allow_destroy: true, reject_if: ->(c) { c['charge'].blank? }
  attr_accessor :parish_probation, :state_probation, :dwi_test_officer_id
  attr_accessor :agency_id, :assigned_warrants
  
  ## Scopes #############################################
  
  scope :by_reverse_id, -> { order id: :desc }
  scope :on_or_before, ->(time) { where('arrests.arrest_datetime <= ?',time) }
  scope :on_or_after, ->(time) { where('arrests.arrest_datetime >= ?',time) }
  scope :between, ->(start,stop) { where('arrests.arrest_datetime >= ? AND arrests.arrest_datetime <= ?',start,stop) }

  # filter scopes
  scope :with_arrest_id, ->(a_id) { where id: a_id }
  scope :with_person_id, ->(p_id) { where person_id: p_id }
  scope :with_offense_id, ->(o_id) { includes(:offense_arrests).where(offense_arrests: {offense_id: o_id}) }
  scope :with_case_no, ->(c_no) { where case_no: c_no }
  scope :with_agency, ->(ag) { AppUtils.like self, :agency, ag }
  scope :with_dwi_test_results, ->(dwi) { AppUtils.like self, :dwi_test_results, dwi }
  scope :with_warrant_no, ->(w_no) { AppUtils.like self, :warrant_no, w_no, association: :warrants }
  scope :with_charge, ->(chg) { AppUtils.like self, :charge, chg, association: :charges }
  scope :with_ward, ->(wd) { where ward: wd }
  scope :with_district, ->(dis) { where district: dis }
  scope :with_zone, ->(zn) { where zone: zn }
  scope :with_victim_notify, ->(notify) { AppUtils.for_bool self, :victim_notify, notify }
  scope :with_attorney, ->(attn) { AppUtils.like self, :attorney, attn }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  scope :with_arrest_date, ->(start,stop) { AppUtils.for_daterange self, :arrest_datetime, start: start, stop: stop }
  scope :with_officer, ->(name,unit,badge) { AppUtils.for_officer self, :officer, name, unit, badge, association: :officers }
  scope :with_dwi_test_officer, ->(name,unit,badge) { AppUtils.for_officer self, :dwi_test_officer, name, unit, badge }
  scope :with_person_name, ->(name) { AppUtils.for_person self, name, association: :person }

  ## Callbacks #############################################

  after_validation :check_creator
  before_validation :adjust_case_no, :adjust_atn, :check_72_hour, :lookup_contacts
  after_save :process_warrants, :check_and_generate_case, :process_72_hour, :process_holds, :process_afis
  after_create :check_probation, :send_messages
  before_destroy :check_destroyable, :process_dependents
  
  ## Validations #############################################
  
  validates_associated :officers, :charges
  validates_presence_of :agency
  validates_datetime :arrest_datetime
  validates_datetime :hour_72_datetime, allow_blank: true
  validates_numericality_of :bond_amt, greater_than_or_equal_to: 0, less_than: 10 ** 10
  validates_person :person_id
  
  ## Class Methods #############################################
  
  def self.arrest_summary(start=nil, stop=nil, agency_name=nil)
    # get a list of arrests within the given time period
    results = with_arrest_date(start, stop)
    logger.info("Arrest Results: #{results.inspect}")
    # if agency supplied, limit list to officers for that agency
    officer_names = nil
    if !agency_name.blank? && ag = Contact.find_by(sort_name: agency_name)
      officer_names = Contact.where(agency_id: ag.id, officer: true).pluck(:sort_name, :unit)
      logger.info("Officers Selected: #{officer_names.inspect}")
      arrests_with_officers = []
      officer_names.each do |name,unit|
        logger.info("Officer: #{unit} #{name}")
        temp = results.with_officer(name,unit,'').pluck(:id)
        arrests_with_officers.concat(temp)
      end
      results = results.where(id: arrests_with_officers.flatten.compact.uniq)
    end
    results
  end
  
  def self.arrests_by_officer(name, unit, *args)
    options = HashWithIndifferentAccess.new(start: nil, stop: nil)
    options.update(args.extract_options!)
    
    # check for optional badge
    badge = args.shift || ''
    
    result = with_officer(name, unit, badge)
    
    if options[:start] || options[:stop]
      result = result.with_arrest_date(options[:start],options[:stop])
    end
    result
  end
  
  def self.base_perms
    :view_arrest
  end
  
  def self.desc
    'Arrests database'
  end

  def self.hours_served(*arr_ids)
    arrs = where(id: arr_ids.flatten.compact.uniq)
    holds = arrs.collect{|a| a.holds}.flatten.compact
    logger.info("Holds: #{holds.inspect}")
    ranges = holds.collect{|h| h.sentence_time_range}
    logger.info("Ranges: #{ranges.inspect}")
    return 0 if ranges.empty?

    # get a list of all bookings involved and calculate ranges for each
    # temp release hold.  this will be specified with :excluding option
    # of AppUtils.calculate_time_served
    bholds = holds.collect{|h| h.booking}.compact.uniq.collect{|b| b.holds}.flatten.reject{|h| h.hold_type != 'Temporary Release'}.compact.uniq
    exclude_ranges = bholds.collect{|h| h.sentence_time_range}
    logger.info("Excludes: #{exclude_ranges.inspect}")
    ts = TimeServed.new(ranges, nil_value: Time.now)
    logger.info("Time before excludes: #{ts.hours}")
    ts.add_excludes(exclude_ranges)
    logger.info("Time after excludes: #{ts.hours}")
    ts.hours
  end
  
  # process the query
  def self.process_query(query)
    return none unless query.is_a?(Hash)
    result = all
    result = result.with_arrest_id(query[:id]) unless query[:id].blank?
    result = result.with_person_id(query[:person_id]) unless query[:person_id].blank?
    result = result.with_person_name(query[:name]) unless query[:name].blank?
    result = result.with_offense_id(query[:offense_id]) unless query[:offense_id].blank?
    result = result.with_case_no(query[:case_no]) unless query[:case_no].blank?
    result = result.with_arrest_date(query[:arrest_date_from],query[:arrest_date_to]) unless query[:arrest_date_from].blank? && query[:arrest_date_to].blank?
    result = result.with_agency(query[:agency]) unless query[:agency].blank?
    result = result.with_officer(query[:officer], query[:officer_unit], query[:officer_badge]) unless query[:officer].blank? && query[:officer_unit].blank? && query[:officer_badge].blank?
    result = result.with_dwi_test_officer(query[:dwi_test_officer], query[:dwi_test_officer_unit], query[:dwi_test_officer_badge]) unless query[:dwi_test_officer].blank? && query[:dwi_test_officer_unit].blank? && query[:dwi_test_officer_badge].blank?
    result = result.with_dwi_test_results(query[:dwi_test_results]) unless query[:dwi_test_results].blank?
    result = result.with_warrant_no(query[:warrant_no]) unless query[:warrant_no].blank?
    result = result.with_charge(query[:charge]) unless query[:charge].blank?
    result = result.with_ward(query[:ward]) unless query[:ward].blank?
    result = result.with_district(query[:district]) unless query[:district].blank?
    result = result.with_zone(query[:zone]) unless query[:zone].blank?
    result = result.with_victim_notify(query[:victim_notify]) unless query[:victim_notify].blank?
    result = result.with_attorney(query[:attorney]) unless query[:attorney].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  # get the latest booking linked to this arrest  
  def booking
    bookings.by_id.last
  end
  
  # get a list of docket charges either linked to the arrest or one of the
  # arrest charges.
  def docket_charges
    temp = da_charges(true).pluck(:id)
    temp.concat(charge_da_charges(true).pluck(:id))
    DaCharge.where(id: temp.compact.uniq)
  end
  
  # get a list of dockets either linked to the arrest (docket -> da_charge -> arrest)
  # or linked through charge (docket -> da_charge -> charge -> arrest)
  def dockets
    temp = da_charge_dockets(true).pluck(:id)
    temp.concat(charge_dockets(true).pluck(:id))
    Docket.where(id: temp.compact.uniq)
  end
  
  def hours_served
    self.class.hours_served(id)
  end

  def is_final?
    result = true
    
    # false if any district charge is not final
    district_charges(true).each do |c|
      unless c.is_final?
        result = false
        logger.info("District Charge NOT final!")
      end
    end
    
    # false if any linked docket_charges are not final
    if result
      docket_charges.each do |c|
        unless c.is_final?
          result = false
          logger.info("Docket Charge NOT final!")
        end
      end
    end
    
    result
  end
  
  def not_rebookable
    reasons = []
    if holds.empty?
      reasons.push("No Holds - edit and submit the arrest to add initial holds.")
    end
    unless holds.active.empty?
      reasons.push("Active Holds - Already has open holds!")
    end
    if person.bookings.empty? || person.bookings.last.release_datetime?
      reasons.push("No Booking - Person must have open booking!")
    end
    unless person.nil?
      if booking = person.bookings.last
        unless holds.empty?
          if original_booking = holds.last.booking
            if original_booking == booking
              reasons.push("Same Booking - reopen holds instead.")
            end
          end
        end
      end
    end
    reasons.empty? ? nil : reasons.join(', ')
  end
  
  def reasons_not_destroyable
    reasons = []
    unless bonds(true).empty?
      reasons.push("Has Bonds")
    end
    unless revocations(true).empty?
      reasons.push("Has Revocations")
    end
    unless docket_charges.empty?
      reasons.push("Has Docket Charges")
    end
    unless investigations(true).empty?
      reasons.push("Linked to Investigation")
    end
    unless offenses(true).empty?
      reasons.push("Linked to Offense")
    end
    reasons.empty? ? nil : reasons.join(', ')
  end
  
  # this causes a reprocessing of holds for this arrest.  If the person has
  # a new booking, it will place initial holds on it.  If the booking has
  # already been processed, it will only add the ones that are missing for
  # the current status.
  #
  # Note: this can also be done manually by editing the arrest and submitting
  # it again w/o changes. This is simply a convenience method giving the
  # jailers a one-click option.
  #
  # returns nil if success, error message if not
  def rebook
    # just to make sure, check rebookable status
    if errors = not_rebookable
      return errors
    end
    # everything looks ok, rebook by processing holds on new booking
    unless process_holds
      return "Hold Processing Failed!"
    end
    # at this point, new booking should have initial set of holds for this arrest.
    # I am intentionally deciding not to process any existing 72 hour information.
    # The jailers should do that themselves in the normal way if they want to
    # reinstate a bond.
    # If it is desired in the future to automatically process 72 hour information
    # at this time, replace the process_holds call above with a simple self.save
    # to trigger all of the automatic processing for this arrest.
    nil
  end
  
  def process_warrants
    # skip this if assigned_warrants is uninitialized
    return true if self.assigned_warrants.nil?
    existing_warrants = warrants(true).pluck(:id)
    new_warrants = assigned_warrants.collect{|w| w.to_i}
    existing_warrants.each do |w|
      unless new_warrants.include?(w)
        war = Warrant.find_by(id: w)
        war.arrest_id = nil
        war.charges.each{|c| c.update_attribute(:arrest_id,nil)}
        war.dispo_date = nil
        war.person_id = nil
        war.disposition = ''
        war.save(validate: false)
      end
    end
    new_warrants.each do |w|
      if war = Warrant.find_by(id: w)
        warrants << war
        war.charges.each{|c| c.update_attribute(:arrest_id, id)}
        war.person_id = person_id
        war.dispo_date = arrest_datetime.to_date
        war.disposition = 'Served'
        war.save(validate: false)
      end
    end
    self.assigned_warrants = nil
    true
  end
  
  def check_and_generate_case
    return true if legacy?
    unless case_no?
      offenses.each do |o|
        if o.case_no?
          self.update_attribute(:case_no, o.case_no) and break
        end
      end
    end
    unless case_no?
      call = nil
      # look up or create options
      via = "Arrest"
      dispo = "Report Only"
      det = "*** Auto-generated Call Sheet for Arrest of: #{person.sort_name} ***\n"
      unless district_charges.empty?
        det << "District Charges: #{district_charges.join(', ')}\n"
      end
      unless city_charges.empty?
        det << "City Charges: #{city_charges.join(', ')}\n"
      end
      unless other_charges.empty?
        det << "Out of Parish/State Charges: #{other_charges.join(', ')}\n"
      end
      det << "Arresting Agency: #{agency}\nArresting Officers: #{officers.join(' - ')}\n"
      # generate a call sheet
      unless person.nil? # this would be a validation violation and should not occur
        unless updater.nil? # this should also never happen
          if call = Call.new(
              case_no: Case.get_next_case_number,
              followup_no: '',
              received_via: via,
              received_by: updater.sort_name,
              received_by_unit: updater.unit,
              received_by_badge: updater.badge,
              disposition: dispo,
              dispatcher: updater.sort_name,
              dispatcher_unit: updater.unit,
              dispatcher_badge: updater.badge,
              ward: ward,
              district: district,
              zone: zone,
              remarks: remarks,
              created_by: updated_by,
              updated_by: updated_by,
              signal_code: '999',
              signal_name: 'Miscellaneous',
              details: det
            )
            call.call_datetime = arrest_datetime
            call.save(validate: false)
            call.call_subjects.create(
              subject_type: "Arrested",
              sort_name: person.sort_name,
              address1: person.physical_line1,
              address2: person.physical_line2,
              home_phone: person.home_phone,
              cell_phone: person.cell_phone
            )
          end
        end
      end
      if call.nil?
        # something went wrong with call, fall back on self-generated no
        self.update_attribute(:case_no, Case.get_next_case_number)
      else
        self.update_attribute(:case_no, call.case_no)
      end
    end
    true
  end
  
  def adjust_atn
    if atn?
      self.atn = atn.strip
    end
    true
  end

  def adjust_case_no
    self.case_no = AppUtils.fix_case(case_no)
    true
  end
  
  def check_72_hour
    retval = true
    if hour_72_datetime || hour_72_judge? || condition_of_bond? || bond_amt?
      if hour_72_datetime.blank?
        self.errors.add(:hour_72_datetime, "cannot be blank!")
        retval = false
      end
      if hour_72_judge.blank?
        self.errors.add(:hour_72_judge, "cannot be blank!")
        retval = false
      end
    end
    retval
  end
  
  def check_destroyable
    reasons_not_destroyable.blank?
  end

  def check_probation
    unless person.nil?
      if person.on_probation?(hold_if_arrested: true)
        hdatetime = Time.now
        probation_hold = self.holds.build(
          created_by: updated_by,
          updated_by: updated_by,
          hold_datetime: hdatetime,
          hold_type: "Probation (Parish)",
          hold_by: updater.sort_name,
          hold_by_unit: updater.unit,
          hold_by_badge: updater.badge,
          booking_id: person.bookings.last.id
        )
        probation_hold.save(validate: false)
      end
    end
    true
  end
  
  def lookup_contacts
    unless dwi_test_officer_id.blank?
      if ofc = Contact.find_by_id(dwi_test_officer_id)
        self.dwi_test_officer = ofc.sort_name
        self.dwi_test_officer_badge = ofc.badge_no
        self.dwi_test_officer_unit = ofc.unit
      end
      self.dwi_test_officer_id = nil
    end
    unless agency_id.blank?
      if ag = Contact.find_by_id(agency_id)
        self.agency = ag.sort_name
      end
      self.agency_id = nil
    end
    true
  end

  def process_72_hour
    if hour_72_datetime?
      if h72 = holds.of_type("72 Hour").last
        unless h72.cleared_datetime? || h72.booking.release_datetime?
          # update attorney field
          unless h72.booking.attorney?
            h72.booking.update_attribute(:attorney, attorney)
          end
          h72.cleared_datetime = hour_72_datetime
          h72.cleared_by_unit = updater.unit
          h72.cleared_by_badge = updater.badge
          h72.cleared_by = updater.sort_name
          h72.explanation = "Automatic Clear by 72 Hour"
          h72.cleared_because = "72 Hour Hearing Completed"
          h72.remarks = "#{h72.remarks? ? h72.remarks : ''}"
          unless h72.remarks.blank?
            h72.remarks << "\n"
          end
          h72.remarks << "Per Judge #{hour_72_judge}#{condition_of_bond? ? ': ' + condition_of_bond : ''}"
          h72.save(validate: false)
          if bondable?
            newhold = Hold.add(h72.booking.id, id, "Bondable")
          else
            newhold = Hold.add(h72.booking.id, id, "No Bond")
          end
          if who = User.find_by_id(newhold.updated_by)
            newhold.hold_by = who.sort_name
            newhold.hold_by_unit = who.unit
            newhold.hold_by_badge = who.badge
          end
          if condition_of_bond?
            newhold.remarks = "Per Judge #{hour_72_judge}: #{condition_of_bond}"
          end
          newhold.save(validate: false)
        end
      end
    end
    true
  end
  
  # the following conditions could exist
  #
  # 1: arrest atn is blank and no disposition is linked...
  #      Nothing to do!
  # 2: arrest atn is blank and disposition is linked...
  #      if disposition is only linked to this arrest, destroy it.
  #      if disposition is linked to other arrests, unlink it from this arrest
  # 3: arrest atn is not blank and no disposition is linked...
  #      if disposition can be found with this atn, link to it.
  #      if disposition cannot be found with this atn, create one
  # 4: arrest atn is not blank and disposition is linked...
  #      if disposition atn == this atn, nothing to do!
  #      if disposition atn != this atn...
  #         if disposition is only linked to this arrest, update disposition atn
  #         if disposition is linked to other arrests

  def process_afis
    # if atn changed to blank, remove or unlink disposition
    if atn.blank?
      unless disposition.nil?
        if disposition.arrests.count <= 1
          disposition.destroy
        else
          self.update_attribute(:disposition_id, nil)
        end
      end
    elsif disposition.nil?
      link_to_or_create_disposition
    else
      # check to see if atns match
      if atn != disposition.atn
        # update disposition if this arrest is the only one linked,
        if disposition.arrests.count <= 1
          # we are the only one linked, update disposition
          disposition.update_attribute(:atn, atn)
        else
          link_to_or_create_disposition
        end
      end
    end
    true
  end
  
  # find a disposition with this atn and link to it, or
  # create a new one for this atn
  def link_to_or_create_disposition
    unless atn.blank?
      if dispo = Disposition.find_by(atn: atn)
        # found, link to it
        self.update_attribute(:disposition_id, dispo.id)
      else
        # create a new one
        if dispo = Disposition.new(atn: atn, person_id: person_id, created_by: updated_by, updated_by: updated_by)
          dispo.save(validate: false)
          self.update_attribute(:disposition_id, dispo.id)
        end
      end
    end
  end
  
  def process_dependents
    unless disposition.nil? || disposition.arrests.count > 1
      # remove the disposition
      disposition.destroy
    end
    warrants.each do |w|
      w.disposition = ''
      w.dispo_date = nil
      w.updated_by = updated_by
      w.save(validate: false)
    end
    charges.each do |c|
      if c.warrant.nil?
        c.destroy
      else
        c.update_attribute(:arrest_id, nil)
      end
    end
    warrants.clear
    return true
  end

  def process_holds
    booking = person.bookings.last
    return true if booking.nil? || booking.release_datetime?
    current = holds(true).with_booking_id(booking.id).collect{|h| h.hold_type}
    unless district_charges(true).empty?
      unless current.include?('72 Hour')
        Hold.add(booking.id, id, '72 Hour')
      end
    end
    unless city_charges(true).empty?
      agencies = []
      city_charges.each do |c|
        if !c.agency?
          if !(c.warrant.nil? || !c.warrant.jurisdiction?)
            agencies.push(c.warrant.jurisdiction.titleize)
          end
        else
          agencies.push(c.agency.titleize)
        end
      end
      city_holds = holds.with_booking_id(booking.id).of_type('Hold for City').reject{|h| !h.agency?}
      city_agencies = city_holds.collect{|h| h.agency.titleize}.compact.uniq
      city_charge_agencies = agencies.compact.uniq
      city_charge_agencies.each do |city|
        unless city_agencies.include?(city)
          Hold.add(booking.id, id, 'Hold for City', city)
        end
      end
      # remove any "Hold for City" hold that is not for a city currently listed
      # in charges unless it already has bonds
      city_holds.each do |h|
        next unless h.bonds.empty?
        unless city_charge_agencies.include?(h.agency)
          h.destroy
        end
      end
    end
    unless other_charges(true).empty?
      agencies = []
      other_charges.each do |c|
        if !(c.warrant.nil? || !c.warrant.jurisdiction?)
          agencies.push(c.warrant.jurisdiction.titleize)
        end
      end
      other_holds = holds.with_booking_id(booking.id).of_type('Hold for Other Agency').reject{|h| !h.agency?}
      other_agencies = other_holds.collect{|h| h.agency.titleize}.compact.uniq
      other_charge_agencies = agencies.compact.uniq
      other_charge_agencies.compact.uniq.each do |other|
        unless other_agencies.include?(other)
          Hold.add(booking.id, id, 'Hold for Other Agency', other)
        end
      end
      # remove any "Hold of Other Agency" hold that is not for an other
      # agency currently listed in charges unless it has bonds
      other_holds.each do |h|
        next unless h.bonds.empty?
        unless other_charge_agencies.include?(h.agency)
          h.destroy
        end
      end
    end
    if true & parish_probation
      unless current.include?('Probation (Parish)')
        Hold.add(booking.id, id, 'Probation (Parish)')
      end
    end
    if true & state_probation
      unless current.include?('Probation/Parole (State)')
        Hold.add(booking.id, id, 'Probation/Parole (State)')
      end
    end
    true
  end

  def send_messages
    receivers = User.where(notify_arrest_new: true).all
    receivers.each do |r|
      next unless r.can?(self.class.base_perms)
      txt = AutomatedMessagesController.new.notify(self, 'new')
      Message.create(receiver_id: r.id, sender_id: updated_by, subject: "Arrested: #{person.nil? ? 'Invalid Person' : person.sort_name}", body: txt)
    end
  end
end

# == Schema Information
#
# Table name: arrests
#
#  id                     :integer          not null, primary key
#  person_id              :integer          not null
#  case_no                :string(255)      default(""), not null
#  agency                 :string(255)      default(""), not null
#  ward                   :string(255)      default(""), not null
#  district               :string(255)      default(""), not null
#  victim_notify          :boolean          default(FALSE), not null
#  dwi_test_officer       :string(255)      default(""), not null
#  dwi_test_officer_unit  :string(255)      default(""), not null
#  dwi_test_results       :string(255)      default(""), not null
#  attorney               :string(255)      default(""), not null
#  condition_of_bond      :string(255)      default(""), not null
#  remarks                :text             default(""), not null
#  created_by             :integer          default(1)
#  updated_by             :integer          default(1)
#  created_at             :datetime
#  updated_at             :datetime
#  bond_amt               :decimal(12, 2)   default(0.0), not null
#  bondable               :boolean          default(FALSE), not null
#  dwi_test_officer_badge :string(255)      default(""), not null
#  legacy                 :boolean          default(FALSE), not null
#  atn                    :string(255)      default(""), not null
#  arrest_datetime        :datetime
#  hour_72_datetime       :datetime
#  hour_72_judge          :string(255)      default(""), not null
#  zone                   :string(255)      default(""), not null
#  warrants_count         :integer          default(0), not null
#  arrest_officers_count  :integer          default(0), not null
#  disposition_id         :integer
#
# Indexes
#
#  index_arrests_on_atn             (atn)
#  index_arrests_on_case_no         (case_no)
#  index_arrests_on_created_by      (created_by)
#  index_arrests_on_disposition_id  (disposition_id)
#  index_arrests_on_person_id       (person_id)
#  index_arrests_on_updated_by      (updated_by)
#
# Foreign Keys
#
#  arrests_created_by_fk  (created_by => users.id)
#  arrests_person_id_fk   (person_id => people.id)
#  arrests_updated_by_fk  (updated_by => users.id)
#  fk_rails_1f65d0c1f6    (disposition_id => dispositions.id)
#
