require 'digest/sha1'
require 'bcrypt'

class User < ActiveRecord::Base
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :contact, inverse_of: :user                                # constraint
  has_many :user_groups, dependent: :destroy                            # constraint
  has_many :groups, through: :user_groups                               # constraint
  has_many :announcement_ignores, dependent: :destroy                   # constraint
  has_many :ignored_announcements, through: :announcement_ignores, source: :announcement  # constrained through announcement_ignores
  has_many :activities, dependent: :nullify                             # constraint
  has_many :messages, foreign_key: "receiver_id", dependent: :destroy   # constraint
  has_many :sent_messages, class_name: "Message", foreign_key: "sender_id", dependent: :destroy  # constraint
  has_many :message_logs_as_sender, class_name: "MessageLog", foreign_key: 'sender_id', dependent: :destroy  # constraint
  has_many :message_logs_as_receiver, class_name: "MessageLog", foreign_key: 'receiver_id', dependent: :destroy  # constraint
  has_many :forum_topics, dependent: :destroy                           # constraint
  has_many :forum_posts, dependent: :destroy                            # constraint
  has_many :forum_subscriptions, foreign_key: :subscriber_id, dependent: :destroy  # constraint
  has_many :forum_views, dependent: :destroy                            # constraint
  
  accepts_nested_attributes_for :user_groups, allow_destroy: true, reject_if: ->(u) { u['group_id'].blank? || !Group.exists?(u['group_id'])}
  
  scope :by_login, -> { order :login }
  scope :active, -> { where disabled_text: '' }
  scope :with_photo, -> { where.not(user_photo_file_name: '').where.not(user_photo_file_name: nil) }
  
  # filter scopes
  scope :with_login, ->(l) { AppUtils.like self, :login, l }
  scope :with_full_name, ->(name) { AppUtils.like self, :sort_name, name, association: :contact }
  scope :with_group, ->(g) { where('groups.id = ?',g).references(:groups) }
  scope :with_disabled, ->(d) { where.not(disabled_text: '') }
  scope :with_disabled_text, ->(t) { AppUtils.like self, :disabled_text, t }
  scope :with_locked, ->(l) { AppUtils.for_bool self, :locked, l }
  scope :with_expires_date, ->(start,stop) { AppUtils.for_daterange self, :expires, start: start, stop: stop, date: true }
  
  attr_accessor :password, :password_confirmation, :keep_pass, :old_password, :photo_form

  has_attached_file(
    :user_photo,
    default_url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/default_images/missing_:style.jpg",
    url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/:attachment/:id/:style/:basename.:extension",
    path: ":rails_root/public/system/:attachment/:id/:style/:basename.:extension",
    styles: {thumb: ["80x80", :jpg], medium: ["300x300",:jpg], small: ["150x150",:jpg]},
    default_style: :original,
    storage: :filesystem
  )

  before_save :check_creator

  validates_length_of :login, within: 3..40
  validates_length_of :password, within: 5..40, unless: :keep_pass
  validates_presence_of :login
  validates_presence_of :password, :password_confirmation, unless: :keep_pass
  validates_uniqueness_of :login
  validates_confirmation_of :password, unless: :keep_pass
  validates_numericality_of :message_retention, only_integer: true, greater_than_or_equal_to: 0
  validates_attachment_content_type :user_photo, content_type: ["image/jpg","image/jpeg","image/gif","image/png","image/tiff"]

  ## Class Methods #############################################
  
  def self.admins_for_select
    includes([:contact]).where("users.login != 'mfmerten' AND users.login != 'admin'").all.reject{|u| !u.is_admin?}.collect{|u| ["#{u.sort_name} (#{u.login})", u.id]}
  end
  
  def self.authenticate(login, pass)
    return nil unless u = find_by_login(login)
    if u.hashed_password.blank?
      return u if User.encrypt(pass, u.salt) == u.old_hashed_password
    else
      return u if BCrypt::Password.new(u.hashed_password).is_password? pass
    end
    nil
  end
  
  def self.base_perms
    :view_user
  end
  
  def self.desc
    "User login accounts"
  end
  
  def self.users_for_select(allow_disabled=false)
    users = includes(:contact)
    unless allow_disabled
      users = users.where("disabled_text = ''")
    end
    users.all.sort{|a,b| a.sort_name <=> b.sort_name }.collect{|u| ["#{u.sort_name} (#{u.login})", u.id]}
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_login(query[:login]) unless query[:login].blank?
    result = result.with_full_name(query[:full_name]) unless query[:full_name].blank?
    result = result.with_group(query[:group]) unless query[:group].blank?
    result = result.with_disabled(query[:disabled]) unless query[:disabled].blank?
    result = result.with_disabled_text(query[:disabled_text]) unless query[:disabled_text].blank?
    result = result.with_locked(query[:locked]) unless query[:locked].blank?
    result = result.with_expires_date(query[:expires_from],query[:expires_to]) unless query[:expires_from].blank? && query[:expires_to].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def badge
    contact.nil? ? '' : contact.badge_no
  end
  
  # if multiple permissions supplied, user must have all
  def can?(*args)
    options = args.extract_options!
    permissions = args.flatten.compact.collect{|a| a.is_a?(Symbol) ? Perm.get(a) : a.to_i}.uniq
    unless options[:noadmin]
      # shortcut if admin
      return true if pids.include?(Perm.get(:admin))
    end
    (pids & permissions) == permissions
  end
  
  def forum_name
    contact.nil? ? login : contact.display_name
  end
  
  def forum_score
    # count the number of posts + number of topics
    # effectively gives extra credit for creating new topics
    forum_posts.size + forum_topics.size
  end
  
  def fullname
    "#{unit.blank? ? '' : unit + ' '}#{display_name}#{badge.blank? ? '' : ' (' + badge + ')'}"
  end
  
  def is_admin?
    can?(:admin)
  end
  
  def is_forum_admin?
    can?(:admin) || can?(:admin_forum)
  end
  
  def display_name
    contact.nil? ? login : contact.display_name
  end
  
  def sort_name
    contact.nil? ? login : contact.sort_name
  end
  
  # all new passwords now get encrypted with bcrypt
  def password=(pass)
    @password=pass
    self.hashed_password = BCrypt::Password.create(@password)
    if @password == "pa$$word"
      self.expires = Date.today
    else
      self.expires = Date.today + 180.days
    end
  end
  
  def pids
    groups.collect{|g| g.permissions}.flatten.compact.collect{|p| p.to_i}.uniq 
  end
  
  def to_s
    forum_name
  end
  
  def unit
    contact.nil? ? '' : contact.unit
  end
  
  private
  
  ## Private Class Methods #############################################
  
  def self.encrypt(pass, salt)
    Digest::SHA1.hexdigest(pass+salt)
  end
  
  def self.random_string(len)
    #generat a random password consisting of strings and digits
    chars = ("a".."z").to_a + ("A".."Z").to_a + ("0".."9").to_a
    newpass = ""
    1.upto(len) { |i| newpass << chars[rand(chars.size-1)] }
    return newpass
  end
end

# == Schema Information
#
# Table name: users
#
#  id                      :integer          not null, primary key
#  login                   :string(255)      default(""), not null
#  locked                  :boolean          default(FALSE), not null
#  old_hashed_password     :string(255)      default(""), not null
#  salt                    :string(255)      default(""), not null
#  items_per_page          :integer          default(15), not null
#  expires                 :date
#  created_at              :datetime
#  contact_id              :integer
#  created_by              :integer          default(1)
#  updated_by              :integer          default(1)
#  updated_at              :datetime
#  disabled_text           :string(255)      default(""), not null
#  user_photo_file_name    :string(255)
#  user_photo_content_type :string(255)
#  user_photo_file_size    :integer
#  user_photo_updated_at   :datetime
#  send_email              :boolean          default(FALSE), not null
#  show_tips               :boolean          default(TRUE), not null
#  notify_booking_release  :boolean          default(FALSE), not null
#  notify_booking_new      :boolean          default(FALSE), not null
#  notify_arrest_new       :boolean          default(FALSE), not null
#  notify_call_new         :boolean          default(FALSE), not null
#  notify_offense_new      :boolean          default(FALSE), not null
#  notify_forbid_new       :boolean          default(FALSE), not null
#  notify_forbid_recalled  :boolean          default(FALSE), not null
#  notify_pawn_new         :boolean          default(FALSE), not null
#  notify_warrant_new      :boolean          default(FALSE), not null
#  notify_warrant_resolved :boolean          default(FALSE), not null
#  message_retention       :integer          default(0), not null
#  notify_booking_problems :boolean          default(FALSE), not null
#  forum_auto_subscribe    :boolean          default(FALSE), not null
#  hashed_password         :string(255)      default(""), not null
#
# Indexes
#
#  index_users_on_contact_id  (contact_id)
#  index_users_on_created_by  (created_by)
#  index_users_on_login       (login)
#  index_users_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  users_contact_id_fk  (contact_id => contacts.id)
#  users_created_by_fk  (created_by => users.id)
#  users_updated_by_fk  (updated_by => users.id)
#
