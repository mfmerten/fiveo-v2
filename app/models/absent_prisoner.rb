class AbsentPrisoner < ActiveRecord::Base
  
  ## Associations ###################################################
  
  belongs_to :absent, inverse_of: :absent_prisoners
  belongs_to :booking, inverse_of: :absent_prisoners
  
  ## Class Methods ###################################################
  
  def self.desc
    "Prisoners for Absents"
  end
end

# == Schema Information
#
# Table name: absent_prisoners
#
#  id         :integer          not null, primary key
#  absent_id  :integer          not null
#  booking_id :integer          not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_absent_prisoners_on_absent_id   (absent_id)
#  index_absent_prisoners_on_booking_id  (booking_id)
#
# Foreign Keys
#
#  absent_prisoners_absent_id_fk   (absent_id => absents.id)
#  absent_prisoners_booking_id_fk  (booking_id => bookings.id)
#
