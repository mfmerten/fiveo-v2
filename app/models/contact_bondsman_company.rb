class ContactBondsmanCompany < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :contact, inverse_of: :contact_bondsman_companies             # constraint
  belongs_to :bondsman, class_name: 'Contact', foreign_key: 'bondsman_id'  # constraint
  belongs_to :company, class_name: 'Contact', foreign_key: 'company_id' # constraint

  ## Class Methods #############################################
  
  def self.desc
    "Bondsman Companies for Contacts"
  end
end

# == Schema Information
#
# Table name: contact_bondsman_companies
#
#  id          :integer          not null, primary key
#  contact_id  :integer          not null
#  bondsman_id :integer
#  company_id  :integer
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_contact_bondsman_companies_on_bondsman_id  (bondsman_id)
#  index_contact_bondsman_companies_on_company_id   (company_id)
#  index_contact_bondsman_companies_on_contact_id   (contact_id)
#
# Foreign Keys
#
#  contact_bondsman_companies_bondsman_id_fk  (bondsman_id => contacts.id)
#  contact_bondsman_companies_company_id_fk   (company_id => contacts.id)
#  contact_bondsman_companies_contact_id_fk   (contact_id => contacts.id)
#
