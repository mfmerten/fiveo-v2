class Garnishment < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  
  scope :by_name, -> { order :name }
  scope :active, -> { where satisfied_date: nil }
  
  # filter scopes
  scope :with_garnishment_id, ->(g_id) { where id: g_id }
  scope :with_name, ->(n) { AppUtils.like self, :sort_name, n }
  scope :with_suit_no, ->(s_no) { AppUtils.like self, :suit_no, s_no }
  scope :with_received_date, ->(start,stop) { AppUtils.for_daterange self, :received_date, start: start, stop: stop, date: true }
  scope :with_satisfied_date, ->(start,stop) { AppUtils.for_daterange self, :satisfied_date, start: start, stop: stop, date: true }
  scope :with_description, ->(des) { AppUtils.like self, :description, des }
  scope :with_attorney, ->(att) { AppUtils.like self, :attorney, att }
  scope :with_details, ->(det) { AppUtils.like self, :details, det }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  
  before_validation :normalize_name
  before_save :check_creator
  
  validates_presence_of :name, :suit_no, :description
  validates_date :received_date
  validates_date :satisfied_date, allow_blank: true
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_civil
  end
  
  def self.desc
     "Garnishments"
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_garnishment_id(query[:id]) unless query[:id].blank?
    result = result.with_name(query[:name]) unless query[:name].blank?
    result = result.with_received_date(query[:received_date_from], query[:received_date_to]) unless query[:received_date_from].blank? && query[:received_date_to].blank?
    result = result.with_satisfied_date(query[:satisfied_date_from], query[:satisfied_date_to]) unless query[:satisfied_date_from].blank? && query[:received_date_to].blank?
    result = result.with_description(query[:description]) unless query[:description].blank?
    result = result.with_attorney(query[:attorney]) unless query[:attorney].blank?
    result = result.with_details(query[:details]) unless query[:details].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  # returns name in (given family suffix) order
  def display_name
    AppUtils.parse_name(sort_name,true)
  end
  
  private
  
  ## Instance Methods #############################################
  
  def normalize_name
    self.sort_name = AppUtils.parse_name(sort_name)
  end
end

# == Schema Information
#
# Table name: garnishments
#
#  id             :integer          not null, primary key
#  sort_name      :string(255)      default(""), not null
#  suit_no        :string(255)      default(""), not null
#  description    :string(255)      default(""), not null
#  attorney       :string(255)      default(""), not null
#  details        :text             default(""), not null
#  received_date  :date
#  satisfied_date :date
#  remarks        :text             default(""), not null
#  created_by     :integer          default(1)
#  updated_by     :integer          default(1)
#  created_at     :datetime
#  updated_at     :datetime
#
# Indexes
#
#  index_garnishments_on_created_by  (created_by)
#  index_garnishments_on_sort_name   (sort_name)
#  index_garnishments_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  garnishments_created_by_fk  (created_by => users.id)
#  garnishments_updated_by_fk  (updated_by => users.id)
#
