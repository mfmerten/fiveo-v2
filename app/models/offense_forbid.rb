class OffenseForbid < ActiveRecord::Base
  belongs_to :forbid, inverse_of: :offense_forbids                         # constraint
  belongs_to :offense, inverse_of: :offense_forbids                        # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    "Offense Forbids"
  end
end

# == Schema Information
#
# Table name: offense_forbids
#
#  id         :integer          not null, primary key
#  forbid_id  :integer          not null
#  offense_id :integer          not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_offense_forbids_on_forbid_id   (forbid_id)
#  index_offense_forbids_on_offense_id  (offense_id)
#
# Foreign Keys
#
#  offense_forbids_forbid_id_fk   (forbid_id => forbids.id)
#  offense_forbids_offense_id_fk  (offense_id => offenses.id)
#
