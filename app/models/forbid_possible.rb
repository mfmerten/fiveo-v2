class ForbidPossible < ActiveRecord::Base
  belongs_to :forbid, inverse_of: :forbid_possibles
  belongs_to :person, inverse_of: :forbid_possibles
  
  ## Class Methods #############################################
  
  def self.desc
    "Forbid Possible People"
  end
end

# == Schema Information
#
# Table name: forbid_possibles
#
#  id         :integer          not null, primary key
#  created_at :datetime
#  updated_at :datetime
#  forbid_id  :integer
#  person_id  :integer
#
# Indexes
#
#  index_forbid_possibles_on_forbid_id  (forbid_id)
#  index_forbid_possibles_on_person_id  (person_id)
#
# Foreign Keys
#
#  fk_rails_40f59c4aea  (forbid_id => forbids.id)
#  fk_rails_9cabebf21a  (person_id => people.id)
#
