class Charge < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :arrest, inverse_of: :charges
  has_many :da_charges, dependent: :nullify
  belongs_to :citation, inverse_of: :charges
  belongs_to :warrant, inverse_of: :charges
  belongs_to :agency_contact, class_name: 'Contact', foreign_key: 'agency_id'
  
  ## Attributes #############################################
  
  attr_accessor :agency_id
  
  ## Scopes #############################################
  
  scope :city, -> { where charge_type: 'City' }
  scope :district, -> { where charge_type: 'District' }
  scope :other, -> { where charge_type: 'Other' }
  scope :untyped, -> { where charge_type: '' }
  
  ## Callbacks #############################################
  
  before_validation :lookup_agency

  ## Validations #############################################
  
  validates_presence_of :charge
  validates_numericality_of :count, only_integer: true, greater_than: 0

  ## Constants #############################################
  
  ChargeType = ['District','City','Other'].freeze

  ## Class Methods #############################################
  
  def self.desc
    'Charges Table'
  end
  
  ## Instance Methods #############################################
  
  def is_final?
    # not final if not linked to a docket or only linked to misc dockets
    return false if da_charges.empty? || da_charges.reject{|c| c.docket.nil? || c.docket.misc?}.empty?
    # otherwise depends on state of da_charges
    da_charges.reject{|c| c.is_final?}.empty?
  end
  
  def to_s
    txt = ''
    if agency.blank?
      unless warrant.nil?
        txt << "[#{warrant.jurisdiction}] "
      end
    else
      txt << "[#{agency}] "
    end
    txt << charge
    txt << "(#{count})"
    txt
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def lookup_agency
    unless agency_id.blank?
      if ag = Contact.find_by_id(agency_id)
        self.agency = ag.sort_name
      end
      self.agency_id = nil
    end
    true
  end
end

# == Schema Information
#
# Table name: charges
#
#  id          :integer          not null, primary key
#  citation_id :integer
#  charge      :string(255)      default(""), not null
#  count       :integer          default(1), not null
#  created_at  :datetime
#  updated_at  :datetime
#  warrant_id  :integer
#  agency      :string(255)      default(""), not null
#  arrest_id   :integer
#  charge_type :string(255)      default(""), not null
#
# Indexes
#
#  index_charges_on_arrest_id    (arrest_id)
#  index_charges_on_citation_id  (citation_id)
#  index_charges_on_warrant_id   (warrant_id)
#
# Foreign Keys
#
#  charges_arrest_id_fk    (arrest_id => arrests.id)
#  charges_citation_id_fk  (citation_id => citations.id)
#  charges_warrant_id_fk   (warrant_id => warrants.id)
#
