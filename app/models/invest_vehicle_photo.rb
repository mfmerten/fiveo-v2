class InvestVehiclePhoto < ActiveRecord::Base
  belongs_to :invest_vehicle, inverse_of: :invest_vehicle_photos           # constraint
  belongs_to :invest_photo, inverse_of: :invest_vehicle_photos             # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestVehicle Photos'
  end
end

# == Schema Information
#
# Table name: invest_vehicle_photos
#
#  id                :integer          not null, primary key
#  invest_vehicle_id :integer          not null
#  invest_photo_id   :integer          not null
#  created_at        :datetime
#  updated_at        :datetime
#
# Indexes
#
#  index_invest_vehicle_photos_on_invest_photo_id    (invest_photo_id)
#  index_invest_vehicle_photos_on_invest_vehicle_id  (invest_vehicle_id)
#
# Foreign Keys
#
#  invest_vehicle_photos_invest_photo_id_fk    (invest_photo_id => invest_photos.id)
#  invest_vehicle_photos_invest_vehicle_id_fk  (invest_vehicle_id => invest_vehicles.id)
#
