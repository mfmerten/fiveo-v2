class Damage < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'
  belongs_to :person
  has_many :damage_items, dependent: :destroy

  ## Attributes #############################################
  
  accepts_nested_attributes_for :damage_items, reject_if: ->(d) { d['description'].blank? || d['estimate'].blank? }

  ## Scopes #############################################
  
  # filter scopes (see Class Methods)
  scope :with_damage_id, ->(d_id) { where id: d_id }
  scope :with_case_no, ->(c_no) { where case_no: c_no }
  scope :with_person_id, ->(p_id) { where person_id: p_id }
  scope :with_name, ->(nm) { AppUtils.for_person self, nm, simple: true }
  scope :with_address, ->(addr) { AppUtils.like self, :victim_address, addr }
  scope :with_description, ->(des) { AppUtils.like self, :description, des, association: :damage_items }
  scope :with_damage_date, ->(start,stop) { AppUtils.for_daterange self, :damage_datetime, start: start, stop: stop }
  
  ## Callbacks #############################################
  
  before_save :check_creator
  before_validation :lookup_victim, :adjust_case_no
  
  ## Validations #############################################
  
  validates_associated :damage_items
  validates_presence_of :case_no, :sort_name, :victim_address, :victim_phone
  validates_datetime :damage_datetime

  ## Class Methods #############################################
  
  def self.base_perms
    :view_damage
  end
  
  def self.desc
    "Criminal Damages"
  end

  # process filter
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_damage_id(query[:id]) unless query[:id].blank?
    result = result.with_case_no(query[:case_no]) unless query[:case_no].blank?
    result = result.with_person_id(query[:person_id]) unless query[:person_id].blank?
    result = result.with_name(query[:victim_name]) unless query[:victim_name].blank?
    result = result.with_address(query[:victim_address]) unless query[:victim_address].blank?
    result = result.with_damage_date(query[:damage_from], query[:damage_to]) unless query[:damage_from].blank? && query[:damage_to].blank?
    result = result.with_description(query[:description]) unless query[:description].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def estimate
    damage_items.sum('estimate') || 0
  end
  
  # return name in (given family suffix) order
  def display_name
    AppUtils.parse_name(sort_name,true)
  end
  
  private

  ## Private Instance Methods #############################################
  
  def adjust_case_no
    self.case_no = AppUtils.fix_case(case_no)
    true
  end
  
  def lookup_victim
    if person.nil?
      self.sort_name = AppUtils.parse_name(sort_name)
    else
      self.sort_name = person.sort_name
      self.victim_address = person.physical_address
      self.victim_phone = person.phones
    end
    true
  end
end

# == Schema Information
#
# Table name: damages
#
#  id              :integer          not null, primary key
#  person_id       :integer
#  case_no         :string(255)      default(""), not null
#  sort_name       :string(255)      default(""), not null
#  victim_address  :string(255)      default(""), not null
#  victim_phone    :string(255)      default(""), not null
#  crimes          :string(255)      default(""), not null
#  damage_datetime :datetime
#  created_by      :integer          default(1)
#  updated_by      :integer          default(1)
#  created_at      :datetime
#  updated_at      :datetime
#
# Indexes
#
#  index_damages_on_created_by  (created_by)
#  index_damages_on_person_id   (person_id)
#  index_damages_on_sort_name   (sort_name)
#  index_damages_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  damages_created_by_fk  (created_by => users.id)
#  damages_person_id_fk   (person_id => people.id)
#  damages_updated_by_fk  (updated_by => users.id)
#
