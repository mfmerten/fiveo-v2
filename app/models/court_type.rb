class CourtType < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'
  
  ## Scopes #############################################
  
  scope :by_name, -> { order :name }
  
  ## Callbacks #############################################
  
  before_save :check_creator
  
  ## Validations #############################################
  
  validates_presence_of :name
  validates_uniqueness_of :name
  
  ## Class Methods #############################################
  
  def self.base_perms
    :update_court
  end
  
  def self.desc
    "Court Types for Courts"
  end
  
  def self.types_for_select
    all.collect{|t|[t.name,t.name]}
  end
end

# == Schema Information
#
# Table name: court_types
#
#  id         :integer          not null, primary key
#  name       :string(255)      default(""), not null
#  created_by :integer          default(1)
#  updated_by :integer          default(1)
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_court_types_on_created_by  (created_by)
#  index_court_types_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  court_types_created_by_fk  (created_by => users.id)
#  court_types_updated_by_fk  (updated_by => users.id)
#
