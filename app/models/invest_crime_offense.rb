class InvestCrimeOffense < ActiveRecord::Base
  belongs_to :invest_offense, inverse_of: :invest_crime_offenses           # constraint
  belongs_to :invest_crime, inverse_of: :invest_crime_offenses             # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestCrime Offenses'
  end
end

# == Schema Information
#
# Table name: invest_crime_offenses
#
#  id                :integer          not null, primary key
#  invest_crime_id   :integer          not null
#  invest_offense_id :integer          not null
#  created_at        :datetime
#  updated_at        :datetime
#
# Indexes
#
#  index_invest_crime_offenses_on_invest_crime_id    (invest_crime_id)
#  index_invest_crime_offenses_on_invest_offense_id  (invest_offense_id)
#
# Foreign Keys
#
#  invest_crime_offenses_invest_crime_id_fk    (invest_crime_id => invest_crimes.id)
#  invest_crime_offenses_invest_offense_id_fk  (invest_offense_id => invest_offenses.id)
#
