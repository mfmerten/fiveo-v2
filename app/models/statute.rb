class Statute < ActiveRecord::Base
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'    # constraint with nullify

  scope :by_code, -> { order :code }
  scope :by_name, -> { order :name }
  scope :by_common_name, -> { order :common_name }
  
  # filter scopes
  scope :with_statute_id, ->(s_id) { where id: s_id }
  scope :with_name, ->(n) { AppUtils.like self, [:name, :common_name], n }
  
  before_save :check_creator

  validates_presence_of :name

  ## Class Methods #############################################
  
  def self.base_perms
    :view_statute
  end
  
  def self.desc
    "Statutes (laws)"
  end
  
  def self.find_first_by_code(code=nil)
    return nil if code.blank?
    by_code.where(code: code).first
  end
  
  def self.find_first_by_common_name_partial(name=nil)
    return nil if name.blank?
    by_common_name.where("LOWER(common_name) LIKE LOWER(?)", "%#{name}%").first
  end
  
  def self.find_first_by_name_partial(name=nil)
    return nil if name.blank?
    by_name.where("LOWER(name) LIKE LOWER(?)", "%#{name}%").first
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_statute_id(query[:id]) unless query[:id].blank?
    result = result.with_name(query[:name]) unless query[:name].blank?
    result
  end
end

# == Schema Information
#
# Table name: statutes
#
#  id          :integer          not null, primary key
#  name        :string(255)      default(""), not null
#  created_at  :datetime
#  updated_at  :datetime
#  created_by  :integer          default(1)
#  updated_by  :integer          default(1)
#  common_name :string(255)      default(""), not null
#  code        :string(255)      default(""), not null
#
# Indexes
#
#  index_statutes_on_code         (code)
#  index_statutes_on_common_name  (common_name)
#  index_statutes_on_created_by   (created_by)
#  index_statutes_on_name         (name)
#  index_statutes_on_updated_by   (updated_by)
#
# Foreign Keys
#
#  statutes_created_by_fk  (created_by => users.id)
#  statutes_updated_by_fk  (updated_by => users.id)
#
