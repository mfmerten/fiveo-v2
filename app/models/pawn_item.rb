class PawnItem < ActiveRecord::Base
  belongs_to :pawn, inverse_of: :pawn_items                                # constraint

  validates_presence_of :description, :item_type

  ## Class Methods #############################################
  
  def self.desc
    "Pawn Ticket Items"
  end

  ## Instance Methods #############################################
  
  def to_s
    "#{description}#{model_no? ? ' (Model: ' + model_no + ')' : ''}#{serial_no? ? ' (Serial: ' + serial_no + ')' : ''}"
  end
end

# == Schema Information
#
# Table name: pawn_items
#
#  id          :integer          not null, primary key
#  pawn_id     :integer          not null
#  model_no    :string(255)      default(""), not null
#  serial_no   :string(255)      default(""), not null
#  description :string(255)      default(""), not null
#  created_at  :datetime
#  updated_at  :datetime
#  item_type   :string(255)      default(""), not null
#
# Indexes
#
#  index_pawn_items_on_pawn_id  (pawn_id)
#
# Foreign Keys
#
#  pawn_items_pawn_id_fk  (pawn_id => pawns.id)
#
