class InvestCriminal < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :owner, class_name: 'User', foreign_key: 'owned_by'        # constraint with nullify
  belongs_to :investigation, inverse_of: :criminals                     # constraint
  belongs_to :person, inverse_of: :invest_criminals                     # constraint
  has_many :notes, class_name: 'InvestNote', dependent: :destroy        # constraint
  has_many :invest_criminal_arrests, dependent: :destroy                # constraint
  has_many :invest_arrests, through: :invest_criminal_arrests           # constrained through invest_criminal_arrests
  has_many :arrests, -> { distinct }, through: :invest_arrests          # constrained through invest_criminal_arrests
  has_many :invest_crime_criminals, dependent: :destroy                 # constraint
  has_many :crimes, through: :invest_crime_criminals, source: :invest_crime  # constrained through invest_crime_criminals
  has_many :invest_criminal_evidences, dependent: :destroy              # constraint
  has_many :invest_evidences, through: :invest_criminal_evidences       # constrained through invest_criminal_evidences
  has_many :evidences, -> { distinct }, through: :invest_evidences      # constrained through invest_criminal_evidences
  has_many :invest_criminal_photos, dependent: :destroy                 # constraint
  has_many :photos, through: :invest_criminal_photos, source: :invest_photo  # constrained through invest_criminal_photos
  has_many :invest_report_criminals, dependent: :destroy                # constraint
  has_many :reports, through: :invest_report_criminals, source: :invest_report  # constrained through invest_report_criminals
  has_many :invest_criminal_stolens, dependent: :destroy                # constraint
  has_many :invest_stolens, through: :invest_criminal_stolens           # constrained through invest_criminal_stolens
  has_many :stolens, -> { distinct }, through: :invest_stolens          # constrained through invest_criminal_stolens
  has_many :invest_criminal_vehicles, dependent: :destroy               # constraint
  has_many :vehicles, through: :invest_criminal_vehicles, source: :invest_vehicle # constrained through invest_criminal_vehicles
  has_many :invest_criminal_warrants, dependent: :destroy               # constraint
  has_many :invest_warrants, through: :invest_criminal_warrants         # constrained through invest_criminal_warrants
  has_many :warrants, -> { distinct }, through: :invest_warrants        # constrained through invest_criminal_warrants
  
  scope :by_reverse_id, -> { order id: :desc }
  
  # filter scopes
  scope :with_invest_criminal_id, ->(c_id) { where id: c_id }
  scope :with_investigation_id, ->(i_id) { where investigation_id: i_id }
  scope :with_invest_crime_id, ->(c_id) { AppUtils.for_lower self, :invest_crime_id, c_id, association: :invest_crime_criminals }
  scope :with_person_id, ->(p_id) { where person_id: p_id }
  scope :with_description, ->(des) { AppUtils.like self, :description, des }
  scope :with_height, ->(ht) { AppUtils.like self, :height, ht }
  scope :with_weight, ->(wt) { AppUtils.like self, :weight, wt }
  scope :with_shoes, ->(sh) { AppUtils.like self, :shoes, sh }
  scope :with_fingerprint_class, ->(fpc) { AppUtils.like self, :fingerprint_class, fpc }
  scope :with_dna_profile, ->(dna) { AppUtils.like self, :dna_profile, dna }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  scope :with_name, ->(n) { AppUtils.for_lower self, :sort_name, n }
  
  before_save :check_creator
  before_validation :normalize_name

  validates_presence_of :description
  validates_person :person_id, allow_nil: true

  ## Class Methods #############################################
  
  def self.base_perms
    :view_investigation
  end
  
  def self.desc
    "Suspects for Detective Investigations"
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_invest_criminal_id(query[:id]) unless query[:id].blank?
    result = result.wtih_invesgitation_id(query[:investigation_id]) unless query[:investigation_id].blank?
    result = result.with_invest_crime_id(query[:invest_crime_id]) unless query[:invest_crime_id].blank?
    result = result.wtih_person_id(query[:person_id]) unless query[:person_id].blank?
    result = result.with_description(query[:description]) unless query[:description].blank?
    result = result.with_height(query[:height]) unless query[:height].blank?
    result = result.with_weight(query[:weight]) unless query[:weight].blank?
    result = result.with_shoes(query[:shoes]) unless query[:shoes].blank?
    result = result.with_fingerprint_class(query[:fingerprint_class]) unless query[:fingerprint_class].blank?
    result = result.with_dna_profile(query[:dna_profile]) unless query[:dna_profile].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result = result.with_name(query[:name]) unless query[:name].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def linked_arrests=(selected_arrests)
    self.invest_arrest_ids = selected_arrests.reject{|a| a.blank?}.collect{|a| a.to_i}.uniq
  end
  
  def linked_crimes=(selected_crimes)
    self.crime_ids = selected_crimes.reject{|c| c.blank?}.collect{|c| c.to_i}.uniq
  end
  
  def linked_evidences=(selected_evidences)
    self.invest_evidence_ids = selected_evidences.reject{|e| e.blank?}.collect{|e| e.to_i}.uniq
  end
  
  def linked_photos=(selected_photos)
    self.photo_ids = selected_photos.reject{|p| p.blank?}.collect{|p| p.to_i}.uniq
  end
  
  def linked_stolens=(selected_stolens)
    self.invest_stolen_ids = selected_stolens.reject{|s| s.blank?}.collect{|s| s.to_i}.uniq
  end
  
  def linked_vehicles=(selected_vehicles)
    self.vehicle_ids = selected_vehicles.reject{|v| v.blank?}.collect{|v| v.to_i}.uniq
  end
  
  def linked_warrants=(selected_warrants)
    self.invest_warrant_ids = selected_warrants.reject{|w| w.blank?}.collect{|w| w.to_i}.uniq
  end
  
  # return name in (given family suffix) order
  def display_name
    AppUtils.parse_name(sort_name,true)
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def normalize_name
    if person.nil?
      self.sort_name = AppUtils.parse_name(sort_name)
    else
      self.sort_name = person.sort_name
    end
    true
  end
end

# == Schema Information
#
# Table name: invest_criminals
#
#  id                :integer          not null, primary key
#  investigation_id  :integer          not null
#  person_id         :integer
#  sort_name         :string(255)      default(""), not null
#  description       :string(255)      default(""), not null
#  height            :string(255)      default(""), not null
#  weight            :string(255)      default(""), not null
#  shoes             :string(255)      default(""), not null
#  fingerprint_class :string(255)      default(""), not null
#  dna_profile       :string(255)      default(""), not null
#  remarks           :text             default(""), not null
#  created_by        :integer          default(1)
#  updated_by        :integer          default(1)
#  created_at        :datetime
#  updated_at        :datetime
#  private           :boolean          default(TRUE), not null
#  owned_by          :integer          default(1)
#
# Indexes
#
#  index_invest_criminals_on_created_by        (created_by)
#  index_invest_criminals_on_investigation_id  (investigation_id)
#  index_invest_criminals_on_owned_by          (owned_by)
#  index_invest_criminals_on_person_id         (person_id)
#  index_invest_criminals_on_sort_name         (sort_name)
#  index_invest_criminals_on_updated_by        (updated_by)
#
# Foreign Keys
#
#  invest_criminals_created_by_fk        (created_by => users.id)
#  invest_criminals_investigation_id_fk  (investigation_id => investigations.id)
#  invest_criminals_owned_by_fk          (owned_by => users.id)
#  invest_criminals_person_id_fk         (person_id => people.id)
#  invest_criminals_updated_by_fk        (updated_by => users.id)
#
