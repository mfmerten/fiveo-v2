class CallSubject < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :call, counter_cache: true, inverse_of: :call_subjects

  ## Callbacks #############################################
  
  before_validation :fix_name

  ## Validations #############################################
  
  validates_presence_of :sort_name, :subject_type
  validates_phone :home_phone, :cell_phone, :work_phone, allow_blank: true

  ## Class Methods #############################################
  
  def self.desc
    'Subjects for Call Sheets'
  end
  
  def self.types
    [
      "Complainant", "Reported By", "Subject", "Victim",
      "Witness", "Arrested"
    ]
  end
  
  ## Instance Methods #############################################
  
  def address
    [address1,address2].reject{|a| a.blank?}.join(', ')
  end
  
  def display_name
    AppUtils.parse_name(sort_name,true)
  end
  
  def to_s
    "#{subject_type? ? subject_type : 'Subject'}: #{sort_name}"
  end
  
  def fix_name
    self.sort_name = AppUtils.parse_name(sort_name)
    true
  end
end

# == Schema Information
#
# Table name: call_subjects
#
#  id           :integer          not null, primary key
#  call_id      :integer          not null
#  sort_name    :string(255)      default(""), not null
#  address1     :string(255)      default(""), not null
#  address2     :string(255)      default(""), not null
#  home_phone   :string(255)      default(""), not null
#  cell_phone   :string(255)      default(""), not null
#  created_at   :datetime
#  updated_at   :datetime
#  work_phone   :string(255)      default(""), not null
#  subject_type :string(255)      default(""), not null
#
# Indexes
#
#  index_call_subjects_on_call_id    (call_id)
#  index_call_subjects_on_sort_name  (sort_name)
#
# Foreign Keys
#
#  call_subjects_call_id_fk  (call_id => calls.id)
#
