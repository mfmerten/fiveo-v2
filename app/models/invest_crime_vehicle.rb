class InvestCrimeVehicle < ActiveRecord::Base
  belongs_to :invest_crime, inverse_of: :invest_crime_vehicles             # constraint
  belongs_to :invest_vehicle, inverse_of: :invest_crime_vehicles           # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestCrime Vehicles'
  end
end

# == Schema Information
#
# Table name: invest_crime_vehicles
#
#  id                :integer          not null, primary key
#  invest_crime_id   :integer          not null
#  invest_vehicle_id :integer          not null
#  created_at        :datetime
#  updated_at        :datetime
#
# Indexes
#
#  index_invest_crime_vehicles_on_invest_crime_id    (invest_crime_id)
#  index_invest_crime_vehicles_on_invest_vehicle_id  (invest_vehicle_id)
#
# Foreign Keys
#
#  invest_crime_vehicles_invest_crime_id_fk    (invest_crime_id => invest_crimes.id)
#  invest_crime_vehicles_invest_vehicle_id_fk  (invest_vehicle_id => invest_vehicles.id)
#
