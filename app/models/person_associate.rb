class PersonAssociate < ActiveRecord::Base
  belongs_to :person, inverse_of: :associates                              # constraint

  validates_presence_of :name

  ## Class Methods #############################################
  
  def self.desc
    "Known associates for People"
  end
  
  ## Instance Methods #############################################
  
  def to_s
    name
  end
end

# == Schema Information
#
# Table name: person_associates
#
#  id           :integer          not null, primary key
#  person_id    :integer          not null
#  name         :string(255)      default(""), not null
#  remarks      :text             default(""), not null
#  created_at   :datetime
#  updated_at   :datetime
#  relationship :string(255)      default(""), not null
#
# Indexes
#
#  index_associates_on_person_id  (person_id)
#
# Foreign Keys
#
#  person_associates_person_id_fk  (person_id => people.id)
#
