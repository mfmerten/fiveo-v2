class Doc < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'
  belongs_to :transfer, inverse_of: :doc
  belongs_to :hold, dependent: :destroy, inverse_of: :doc_record
  belongs_to :booking, inverse_of: :docs
  belongs_to :person, inverse_of: :docs
  belongs_to :sentence, inverse_of: :doc_record
  belongs_to :revocation, inverse_of: :doc_record
  
  ## Attributes #############################################
  
  attr_accessor :force
  
  ## Scopes #############################################
  
  scope :with_problems, -> { where "docs.conviction IS NULL OR (docs.to_serve_hours + docs.to_serve_days + docs.to_serve_months + docs.to_serve_years <= 0) OR docs.doc_number IS NULL" }
  scope :active, -> { where(hold: {cleared_datetime: nil}) }
  scope :by_name, -> { includes(:person).order('people.family_name, people.given_name, people.suffix') }

  # filter scopes
  scope :with_doc_id, ->(d_id) { where id: d_id }
  scope :with_doc_number, ->(d_no) { where doc_number: d_no }
  scope :with_booking_id, ->(b_id) { where booking_id: b_id }
  scope :with_person_id, ->(p_id) { where person_id: p_id }
  scope :with_sex, ->(sx) { where(person: {sex: sx}).references(:person) }
  scope :with_conviction, ->(conv) { AppUtils.like self, :conviction, conv }
  scope :with_billable, ->(bill) { AppUtils.for_bool self, :billable, bill }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  scope :with_person_name, ->(name) { AppUtils.for_person self, name, association: :person}
  scope :with_transfer_date, ->(start,stop) { AppUtils.for_daterange self, :transfer_datetime, start: start, stop: stop }

  ## Callbacks #############################################
  
  before_validation :validate_booking
  before_save :fix_numbers, :process, :check_creator
  after_update :update_person

  ## Valdiations #############################################
  
  validates_numericality_of :to_serve_hours, :to_serve_days, :to_serve_months,
    :to_serve_years, :to_serve_life, :sentence_hours, :sentence_days, :sentence_months,
    :sentence_years, :sentence_life, only_integer: true, allow_nil: true
  # do not validate transfer_datetime - it is updated by transfer process and is
  # not available to the user.

  ## Class Methods #############################################
  
  def self.base_perms
    :view_booking
  end
  
  def self.desc
    'DOC inmate records'
  end
  
  def self.process_docs
    where({processed: false}).all.each{|d| d.process_doc}
  end
  
  # process filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_doc_id(query[:id]) unless query[:id].blank?
    result = result.with_doc_number(query[:doc_number]) unless query[:doc_number].blank?
    result = result.with_booking_id(query[:booking_id]) unless query[:booking_id].blank?
    result = result.with_person_id(query[:person_id]) unless query[:person_id].blank?
    result = result.with_sex(query[:sex]) unless query[:sex].blank?
    result = result.with_person_name(query[:person_name]) unless query[:person_name].blank?
    result = result.with_conviction(query[:conviction]) unless query[:conviction].blank?
    result = result.with_billable(query[:billable]) unless query[:billable].blank?
    result = result.with_transfer_date(query[:transfer_from],query[:transfer_to]) unless query[:transfer_from].blank? && query[:transfer_to].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end

  ## Instance Methods #############################################
  
  def missing_information
    info = []
    info.push('[Needs To-Serve Time]') if to_serve_string.blank?
    info.push('[Needs Billing Info]') unless billable? && bill_from_date?
    info.push('[Needs DOC Number]') unless doc_number?
    info.push('[Needs Conviction Info]') unless conviction?
    info.join(' ')
  end
  
  def needs_information
    return true if to_serve_string.blank?
    return true unless billable? && bill_from_date?
    return true unless doc_number?
    return true unless conviction?
    false
  end
  
  def sentence_string
    AppUtils.sentence_to_string(sentence_hours, sentence_days, sentence_months, sentence_years, sentence_life, sentence_death)
  end
  
  def to_serve_string
    AppUtils.sentence_to_string(to_serve_hours, to_serve_days, to_serve_months, to_serve_years, to_serve_life, to_serve_death)
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def fix_numbers
    ['to_serve','sentence'].each do |t|
      ['hours','days','months','years','life'].each do |s|
        if send("#{t}_#{s}").blank?
          self.send("#{t}_#{s}=",0)
        end
      end
    end
    return true
  end
  
  def process
    # new processing attempt, clear out any previous processing problems
    self.problems = ""
    
    # must have a valid booking
    if booking.nil?
      self.problems << "[Invalid Booking]"
    else
      # calculate hours to serve
      to_serve = 0
      if to_serve_hours? || to_serve_days? || to_serve_months? || to_serve_years? || to_serve_life? || to_serve_death?
        to_serve = AppUtils.sentence_to_hours(to_serve_hours, to_serve_days, to_serve_months, to_serve_years, to_serve_life, to_serve_death)
      else
        self.problems << "[No To-Serve Time]"
      end

      # if this generated from a sentence or revocation, check for consecutives - cannot
      # process this record if consecutives specified but not themselves processed
      consecs = []
      if !sentence.nil?
        c = sentence.consecutive_holds
        if c.nil?
          self.problems << "[Sentence Consecutive to Unprocessed Sentences]"
        else
          consecs.concat(c)
        end
      elsif !revocation.nil?
        c = revocation.consecutive_holds
        if c.nil?
          self.problems << "[Revocation Consecutive to Unprocessed Sentences]"
        else
          consecs.concat(c)
        end
      end

      # get appropriate information from source of doc record
      if !transfer.nil?
        hdatetime = transfer.transfer_datetime
        hby = transfer.creator.sort_name
        hby_unit = transfer.creator.unit
        hby_badge = transfer.creator.badge
      elsif !sentence.nil?
        hdatetime = AppUtils.get_datetime(sentence.court.court_date, "12:00")
        hby = sentence.creator.sort_name
        hby_unit = sentence.creator.unit
        hby_badge = sentence.creator.badge
      elsif !revocation.nil?
        hdatetime = AppUtils.get_datetime(revocation.revocation_date, "12:00")
        hby = revocation.creator.sort_name
        hby_unit = revocation.creator.unit
        hby_badge = revocation.creator.badge
      else
        hdatetime = booking.booking_datetime
        hby = booking.booking_officer
        hby_unit = booking.booking_officer_unit
        hby_badge = booking.booking_officer_badge
      end

      # add or update a sentence hold
      if hold.nil? && booking.release_datetime? && force != true
        self.problems << "[Booking Released: Cannot add hold]"
      else
        h = hold
        if h.nil?
          # no existing hold
          h = booking.holds.build
        end
        h.booking_id = booking.id
        h.hold_datetime = hdatetime
        h.hold_type = "Serving Sentence"
        h.hold_by = hby
        h.hold_by_unit = hby_unit
        h.hold_by_badge = hby_badge
        h.remarks = "Added Automatically By DOC Processing."
        h.created_by = updated_by unless h.created_by?
        h.updated_by = updated_by
        h.doc = true
        unless h.hours_served? && h.hours_served > 0
          h.hours_served = booking.hours_served_since(hdatetime)
        end
        h.hours_total = to_serve
        if to_serve_death?
          h.death_sentence = true
        end
        h.hours_goodtime = 0
        h.hours_time_served = 0
        unless h.datetime_counted?
          h.datetime_counted = Time.now
        end
        h.doc_billable = billable
        h.bill_from_date = bill_from_date
        h.transfer_id = transfer_id
        h.sentence_id = sentence_id
        h.revocation_id = revocation_id
        if force == true && booking.release_datetime?
          h.cleared_datetime = h.hold_datetime
          h.cleared_by = h.hold_by
          h.cleared_by_unit = h.hold_by_unit
          h.cleared_by_badge = h.hold_by_badge
          h.cleared_because = "Automatic Release"
          h.explanation = "Inmate Released Prior To Sentence Processing because of Time Served or Transfer."
          h.remarks = "Added and Cleared Automatically by DOC Processing."
        end
        if h.valid?
          h.save
          h.blockers << consecs
          # add hold id to doc record
          self.hold_id = h.id
        else
          self.problems << '[Unable to Create Sentence Hold]'
          self.problems << "(#{h.errors.full_messages.join(', ')})"
          success = false
        end

        # check for doc_number
        if doc_number?
          if person.doc_number != doc_number
            person.update_attribute(:doc_number, doc_number)
          end
        else
          if person.doc_number?
            doc_number = person.doc_number
          end
        end
      end
    end
    self.force = false
    return true
  end
  
  def update_person
    if doc_number?
      unless person.nil?
        unless doc_number == person.doc_number
          self.person.update_attribute(:doc_number, doc_number)
        end
      end
    else
      unless person.nil?
        if person.doc_number?
          self.update_attribute(:doc_number, person.doc_number)
        end
      end
    end
  end
  
  def validate_booking
    if booking_id?
      if booking.nil?
        self.errors.add(:booking_id, 'must be a valid booking id!')
        return false
      end
      # check that person is same
      if person_id? && booking.person_id? && person_id != booking.person_id
        self.errors.add(:booking_id, 'is not for this person!')
        return false
      end
      # if no hold is attached, verify that booking is open
      if hold.nil? && booking.release_datetime? && force != true
        self.errors.add(:booking_id, 'must be a currently open booking for this person!')
        return false
      end
    else
      self.errors.add(:booking_id, 'is required!')
      return false
    end
    return true
  end
end

# == Schema Information
#
# Table name: docs
#
#  id                :integer          not null, primary key
#  transfer_id       :integer
#  sentence_days     :integer          default(0), not null
#  sentence_months   :integer          default(0), not null
#  sentence_years    :integer          default(0), not null
#  to_serve_days     :integer          default(0), not null
#  to_serve_months   :integer          default(0), not null
#  to_serve_years    :integer          default(0), not null
#  conviction        :string(255)      default(""), not null
#  billable          :boolean          default(FALSE), not null
#  bill_from_date    :date
#  remarks           :text             default(""), not null
#  created_by        :integer          default(1)
#  updated_by        :integer          default(1)
#  created_at        :datetime
#  updated_at        :datetime
#  hold_id           :integer
#  sentence_id       :integer
#  doc_number        :string(255)      default(""), not null
#  booking_id        :integer          not null
#  person_id         :integer          not null
#  revocation_id     :integer
#  sentence_hours    :integer          default(0), not null
#  to_serve_hours    :integer          default(0), not null
#  problems          :string(255)      default(""), not null
#  sentence_life     :integer          default(0), not null
#  sentence_death    :boolean          default(FALSE), not null
#  to_serve_life     :integer          default(0), not null
#  to_serve_death    :boolean          default(FALSE), not null
#  transfer_datetime :datetime
#
# Indexes
#
#  index_docs_on_booking_id     (booking_id)
#  index_docs_on_created_by     (created_by)
#  index_docs_on_hold_id        (hold_id)
#  index_docs_on_person_id      (person_id)
#  index_docs_on_revocation_id  (revocation_id)
#  index_docs_on_sentence_id    (sentence_id)
#  index_docs_on_transfer_id    (transfer_id)
#  index_docs_on_updated_by     (updated_by)
#
# Foreign Keys
#
#  docs_booking_id_fk     (booking_id => bookings.id)
#  docs_created_by_fk     (created_by => users.id)
#  docs_hold_id_fk        (hold_id => holds.id)
#  docs_person_id_fk      (person_id => people.id)
#  docs_revocation_id_fk  (revocation_id => revocations.id)
#  docs_sentence_id_fk    (sentence_id => sentences.id)
#  docs_transfer_id_fk    (transfer_id => transfers.id)
#  docs_updated_by_fk     (updated_by => users.id)
#
