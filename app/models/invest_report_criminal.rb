class InvestReportCriminal < ActiveRecord::Base
  belongs_to :invest_report, inverse_of: :invest_report_criminals          # constraint
  belongs_to :invest_criminal, inverse_of: :invest_report_criminals        # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestReport Criminals'
  end
end

# == Schema Information
#
# Table name: invest_report_criminals
#
#  id                 :integer          not null, primary key
#  invest_report_id   :integer          not null
#  invest_criminal_id :integer          not null
#  created_at         :datetime
#  updated_at         :datetime
#
# Indexes
#
#  index_invest_report_criminals_on_invest_criminal_id  (invest_criminal_id)
#  index_invest_report_criminals_on_invest_report_id    (invest_report_id)
#
# Foreign Keys
#
#  invest_report_criminals_invest_criminal_id_fk  (invest_criminal_id => invest_criminals.id)
#  invest_report_criminals_invest_report_id_fk    (invest_report_id => invest_reports.id)
#
