# this is used to populate a form for adjusting time on a sentence hold
# it does not persist (no database table, regardless of the schema shown above)
# and is used to simplify initialization and validation of form data
class HoldAdjustment
  include ActiveAttr::Model
  
  attribute :plus_hours_total,         type: Integer,    default: 0
  attribute :plus_days_total,          type: Integer,    default: 0
  attribute :plus_months_total,        type: Integer,    default: 0
  attribute :plus_years_total,         type: Integer,    default: 0
  attribute :minus_hours_total,        type: Integer,    default: 0
  attribute :minus_days_total,         type: Integer,    default: 0
  attribute :minus_months_total,       type: Integer,    default: 0
  attribute :minus_years_total,        type: Integer,    default: 0
  attribute :plus_hours_served,        type: Integer,    default: 0
  attribute :plus_days_served,         type: Integer,    default: 0
  attribute :plus_months_served,       type: Integer,    default: 0
  attribute :plus_years_served,        type: Integer,    default: 0
  attribute :minus_hours_served,       type: Integer,    default: 0
  attribute :minus_days_served,        type: Integer,    default: 0
  attribute :minus_months_served,      type: Integer,    default: 0
  attribute :minus_years_served,       type: Integer,    default: 0
  attribute :plus_hours_goodtime,      type: Integer,    default: 0
  attribute :plus_days_goodtime,       type: Integer,    default: 0
  attribute :plus_months_goodtime,     type: Integer,    default: 0
  attribute :plus_years_goodtime,      type: Integer,    default: 0
  attribute :minus_hours_goodtime,     type: Integer,    default: 0
  attribute :minus_days_goodtime,      type: Integer,    default: 0
  attribute :minus_months_goodtime,    type: Integer,    default: 0
  attribute :minus_years_goodtime,     type: Integer,    default: 0
  attribute :plus_hours_time_served,   type: Integer,    default: 0
  attribute :plus_days_time_served,    type: Integer,    default: 0
  attribute :plus_months_time_served,  type: Integer,    default: 0
  attribute :plus_years_time_served,   type: Integer,    default: 0
  attribute :minus_hours_time_served,  type: Integer,    default: 0
  attribute :minus_days_time_served,   type: Integer,    default: 0
  attribute :minus_months_time_served, type: Integer,    default: 0
  attribute :minus_years_time_served,  type: Integer,    default: 0

  validates_numericality_of :plus_hours_total, :plus_days_total, :plus_months_total,
    :plus_years_total, :minus_hours_total, :minus_days_total, :minus_months_total,
    :minus_years_total, :plus_hours_served, :plus_days_served, :plus_months_served,
    :plus_years_served, :minus_hours_served, :minus_days_served, :minus_months_served,
    :minus_years_served, :plus_hours_goodtime, :plus_days_goodtime, :plus_months_goodtime,
    :plus_years_goodtime, :minus_hours_goodtime, :minus_days_goodtime, :minus_months_goodtime,
    :minus_years_goodtime, :plus_hours_time_served, :plus_days_time_served, :plus_months_time_served,
    :plus_years_time_served, :minus_hours_time_served, :minus_days_time_served,
    :minus_months_time_served, :minus_years_time_served, integer_only: true, greater_than_or_equal_to: 0

  def hours_total_adjustment
    (plus_hours_total - minus_hours_total) +
    ((plus_days_total - minus_days_total) * 24) +
    ((plus_months_total - minus_months_total) * 30 * 24) +
    ((plus_years_total - minus_years_total) * 365 * 24)
  end

  def hours_served_adjustment
    (plus_hours_served - minus_hours_served) +
    ((plus_days_served - minus_days_served) * 24) +
    ((plus_months_served - minus_months_served) * 30 * 24) +
    ((plus_years_served - minus_years_served) * 365 * 24)
  end

  def hours_goodtime_adjustment
    (plus_hours_goodtime - minus_hours_goodtime) +
    ((plus_days_goodtime - minus_days_goodtime) * 24) +
    ((plus_months_goodtime - minus_months_goodtime) * 30 * 24) +
    ((plus_years_goodtime - minus_years_goodtime) * 365 * 24)
  end

  def hours_time_served_adjustment
    (plus_hours_time_served - minus_hours_time_served) +
    ((plus_days_time_served - minus_days_time_served) * 24) +
    ((plus_months_time_served - minus_months_time_served) * 30 * 24) +
    ((plus_years_time_served - minus_years_time_served) * 365 * 24)
  end
end
