class WarrantExemption < ActiveRecord::Base
  belongs_to :warrant, inverse_of: :warrant_exemptions                     # constraint
  belongs_to :person, inverse_of: :warrant_exemptions                      # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'Warrant Exemptions'
  end
end

# == Schema Information
#
# Table name: warrant_exemptions
#
#  id         :integer          not null, primary key
#  warrant_id :integer          not null
#  person_id  :integer          not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_warrant_exemptions_on_person_id   (person_id)
#  index_warrant_exemptions_on_warrant_id  (warrant_id)
#
# Foreign Keys
#
#  warrant_exemptions_person_id_fk   (person_id => people.id)
#  warrant_exemptions_warrant_id_fk  (warrant_id => warrants.id)
#
