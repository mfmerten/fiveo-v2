class AnnouncementIgnore < ActiveRecord::Base
  
  ## Associations ###################################################
  
  belongs_to :announcement, inverse_of: :announcement_ignores
  belongs_to :user, inverse_of: :announcement_ignores
  
  ## Class Methods ###################################################
  
  def self.desc
    "Ignored Announcements"
  end
end

# == Schema Information
#
# Table name: announcement_ignores
#
#  id              :integer          not null, primary key
#  announcement_id :integer          not null
#  user_id         :integer          not null
#  created_at      :datetime
#  updated_at      :datetime
#
# Indexes
#
#  index_announcement_ignores_on_announcement_id  (announcement_id)
#  index_announcement_ignores_on_user_id          (user_id)
#
# Foreign Keys
#
#  announcement_ignores_announcement_id_fk  (announcement_id => announcements.id)
#  announcement_ignores_user_id_fk          (user_id => users.id)
#
