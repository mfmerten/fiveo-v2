class PaperType < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify

  scope :by_name, -> { order :name }
  
  before_save :check_creator

  validates_presence_of :name
  validates_uniqueness_of :name
  validates_numericality_of :cost, greater_than_or_equal_to: 0, less_than: 10 ** 10

  ## Class Methods #############################################
  
  def self.desc
    "Paper Types"
  end
  
  def self.base_perms
    :update_papers
  end
  
  def self.names_for_select
    order('name').all.collect{|c| [c.name,c.id]}
  end
end

# == Schema Information
#
# Table name: paper_types
#
#  id         :integer          not null, primary key
#  name       :string(255)      default(""), not null
#  cost       :decimal(12, 2)   default(0.0), not null
#  created_by :integer          default(1)
#  updated_by :integer          default(1)
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_paper_types_on_created_by  (created_by)
#  index_paper_types_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  paper_types_created_by_fk  (created_by => users.id)
#  paper_types_updated_by_fk  (updated_by => users.id)
#
