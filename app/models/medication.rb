class Medication < ActiveRecord::Base
   belongs_to :creator, class_name: 'User', foreign_key: 'created_by'   # constraint with nullify
   belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'   # constraint with nullify
   belongs_to :physician, class_name: 'Contact', foreign_key: 'physician_id', inverse_of: :meds_as_physician  # constraint
   belongs_to :pharmacy, class_name: 'Contact', foreign_key: 'pharmacy_id', inverse_of: :meds_as_pharmacy  # constraint
   belongs_to :person, inverse_of: :medications                         # constraint
   has_many :schedules, class_name: 'MedicationSchedule', dependent: :destroy  # constraint
   
   accepts_nested_attributes_for :schedules, allow_destroy: true, reject_if: ->(s) { s['dose'].blank?}
   
   scope :by_drug, -> { order :drug_name }
   scope :belonging_to_person, ->(p_id) { where person_id: p_id }
   
   before_save :check_creator
   
   validates_associated :schedules
   validates_presence_of :drug_name, :dosage
   validates_person :person_id
   validates_contact :physician_id, :pharmacy_id, allow_nil: true
   
   ## Class Methods #############################################
   
   def self.base_perms
      :view_person
   end
   
   def self.desc
      'Inmate Medications'
   end
   
   ## Instance Methods #############################################
   
   def dose_schedule
      schedules.join(", ")
   end
   
   def to_s
     "#{drug_name}#{dosage? ? '(' + dosage + ')' : ''}"
   end
end

# == Schema Information
#
# Table name: medications
#
#  id              :integer          not null, primary key
#  person_id       :integer          not null
#  prescription_no :string(255)      default(""), not null
#  physician_id    :integer
#  pharmacy_id     :integer
#  drug_name       :string(255)      default(""), not null
#  dosage          :string(255)      default(""), not null
#  warnings        :string(255)      default(""), not null
#  created_by      :integer          default(1)
#  updated_by      :integer          default(1)
#  created_at      :datetime
#  updated_at      :datetime
#
# Indexes
#
#  index_medications_on_created_by    (created_by)
#  index_medications_on_person_id     (person_id)
#  index_medications_on_pharmacy_id   (pharmacy_id)
#  index_medications_on_physician_id  (physician_id)
#  index_medications_on_updated_by    (updated_by)
#
# Foreign Keys
#
#  medications_created_by_fk    (created_by => users.id)
#  medications_person_id_fk     (person_id => people.id)
#  medications_pharmacy_id_fk   (pharmacy_id => contacts.id)
#  medications_physician_id_fk  (physician_id => contacts.id)
#  medications_updated_by_fk    (updated_by => users.id)
#
