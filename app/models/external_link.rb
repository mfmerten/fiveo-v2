class ExternalLink < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  
  scope :by_name, -> { order :name }
  scope :by_short_name, -> { order :short_name }
  scope :for_front_page, -> { where front_page: true }
  
  # filter scopes
  scope :with_external_link_id, ->(e_id) { where id: e_id }
  scope :with_name, ->(nam) { AppUtils.like self, :name, nam }
  scope :with_short_name, ->(sn) { AppUtils.like self, :short_name, sn }
  scope :with_front_page, ->(fp) { AppUtils.for_bool self, :front_page, fp }
  scope :with_description, ->(des) { AppUtils.like self, :description, des }
  
  before_validation :fix_url
  before_save :check_creator
  
  validates_presence_of :name
  validates_length_of :short_name, maximum: 10, allow_blank: true
  validates_presence_of :short_name, if: :front_page?, message: 'must be entered if Front Page is checked!'
  validates_url :url
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_link
  end
  
  def self.desc
     "External Links"
  end
  
  # process filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_external_link_id(query[:id]) unless query[:id].blank?
    result = result.with_name(query[:name]) unless query[:name].blank?
    result = result.with_short_name(query[:short_name]) unless query[:short_name].blank?
    result = result.with_front_page(query[:front_page]) unless query[:front_page].blank?
    result = result.with_description(query[:description]) unless query[:description].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def full_name
    "#{name}#{short_name? ? ' (' + short_name + ')' : ''}"
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def fix_url
    unless url =~ /^http/
      self.url = "http://" + url
    end
    true
  end
end

# == Schema Information
#
# Table name: external_links
#
#  id          :integer          not null, primary key
#  name        :string(255)      default(""), not null
#  short_name  :string(255)      default(""), not null
#  url         :string(255)      default(""), not null
#  description :text             default(""), not null
#  front_page  :boolean          default(FALSE), not null
#  created_by  :integer          default(1)
#  updated_by  :integer          default(1)
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_external_links_on_created_by  (created_by)
#  index_external_links_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  external_links_created_by_fk  (created_by => users.id)
#  external_links_updated_by_fk  (updated_by => users.id)
#
