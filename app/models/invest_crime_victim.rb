class InvestCrimeVictim < ActiveRecord::Base
  belongs_to :invest_crime, inverse_of: :invest_crime_victims              # constraint
  belongs_to :invest_victim, inverse_of: :invest_crime_victims             # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'Investigation Crime Victims'
  end
end

# == Schema Information
#
# Table name: invest_crime_victims
#
#  id               :integer          not null, primary key
#  invest_crime_id  :integer          not null
#  invest_victim_id :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_invest_crime_victims_on_invest_crime_id   (invest_crime_id)
#  index_invest_crime_victims_on_invest_victim_id  (invest_victim_id)
#
# Foreign Keys
#
#  invest_crime_victims_invest_crime_id_fk   (invest_crime_id => invest_crimes.id)
#  invest_crime_victims_invest_victim_id_fk  (invest_victim_id => invest_victims.id)
#
