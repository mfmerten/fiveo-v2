class InvestCriminalWarrant < ActiveRecord::Base
  belongs_to :invest_warrant, inverse_of: :invest_criminal_warrants        # constraint
  belongs_to :invest_criminal, inverse_of: :invest_criminal_warrants       # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestCriminal Warrants'
  end
end

# == Schema Information
#
# Table name: invest_criminal_warrants
#
#  id                 :integer          not null, primary key
#  invest_criminal_id :integer          not null
#  invest_warrant_id  :integer          not null
#  created_at         :datetime
#  updated_at         :datetime
#
# Indexes
#
#  index_invest_criminal_warrants_on_invest_criminal_id  (invest_criminal_id)
#  index_invest_criminal_warrants_on_invest_warrant_id   (invest_warrant_id)
#
# Foreign Keys
#
#  invest_criminal_warrants_invest_criminal_id_fk  (invest_criminal_id => invest_criminals.id)
#  invest_criminal_warrants_invest_warrant_id_fk   (invest_warrant_id => invest_warrants.id)
#
