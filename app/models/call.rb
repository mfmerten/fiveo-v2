class Call < ActiveRecord::Base
  
  ## Associations #############################################
  
  has_many :offenses, dependent: :nullify
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'
  has_many :forbids, dependent: :nullify
  has_many :invest_cases, dependent: :destroy
  has_many :investigations, through: :invest_cases
  has_many :call_units, dependent: :destroy
  has_many :call_subjects, dependent: :destroy
    
  ## Attributes #############################################
  
  accepts_nested_attributes_for :call_units, allow_destroy: true, reject_if: ->(u) { u['officer'].blank? && u['officer_id'].blank? }
  accepts_nested_attributes_for :call_subjects, allow_destroy: true, reject_if: ->(s) { s['name'].blank? }
  attr_accessor :followup_no, :received_by_id, :dispatcher_id, :detective_id

  ## Scopes #############################################
  
  scope :by_reverse_id, -> { order id: :desc }
  scope :by_reverse_date, -> { order call_datetime: :desc }
  scope :by_date, -> { order :call_datetime }
  
  # filter scopes (see Class Methods below)
  scope :with_signal_code, ->(s_code) { AppUtils.like self, :signal_code, s_code }
  scope :with_signal_name, ->(s_name) { AppUtils.like self, :signal_name, s_name }
  scope :with_case_no, ->(c_no) { where case_no: c_no }
  scope :with_received_via, ->(r_via) { AppUtils.like self, :received_via, r_via }
  scope :with_ward, ->(wd) { where ward: wd }
  scope :with_district, ->(dis) { where district: dis }
  scope :with_zone, ->(zn) { where zone: zn }
  scope :with_name, ->(nam) { AppUtils.like self, :name, nam, association: :call_subjects }
  scope :with_address, ->(add) { AppUtils.like self, [:address1, :address2], add, association: :call_subjects }
  scope :with_details, ->(det) { AppUtils.like self, :details, det }
  scope :with_units, ->(uni) { AppUtils.like self, [:officer, :officer_unit], uni, association: :call_units }
  scope :with_disposition, ->(dis) { AppUtils.like self, :disposition, dis }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  scope :with_call_date, ->(start,stop) { AppUtils.for_daterange self, :call_datetime, start: start, stop: stop }
  scope :with_received_by, ->(name,unit,badge) { AppUtils.for_officer self, :received_by, name, unit, badge }
  scope :with_detective, ->(name,unit,badge) { AppUtils.for_officer self, :detective, name, unit, badge }
  scope :with_dispatcher, ->(name,unit,badge) { AppUtils.for_officer self, :dispatcher, name, unit, badge }
  
  ## Callbacks #############################################
  
  after_validation :check_creator
  before_destroy :check_destroyable
  before_validation :check_or_generate_case, :check_signal, :lookup_officers
  after_create :send_messages

  ## Validations #############################################
  
  validates_associated :call_units, :call_subjects
  validates_presence_of :details, :dispatcher
  validates_datetime :call_datetime
  validates_datetime :dispatch_datetime, :arrival_datetime, :concluded_datetime, allow_blank: true

  ## Class Methods #############################################
  
  def self.dispositions(key=nil)
    dispo = {
      'Officer Report' => 'OFC',
      'Referred To Detective' => 'DET',
      'Report Only' => 'RPT'
    }
    if key
      dispo[key]
    else
      dispo
    end
  end
  
  def self.vias
    [
      "911", "Phone", "Radio", "Person", "Alarm",
      "E-Mail", "Text Message", "FAX", "Mail",
      "Tty", "Arrest", "Other"
    ]
  end
  
  def self.base_perms
    :view_call
  end
  
  def self.desc
    "Call Sheets"
  end
  
  def self.dispatch_shifts_for_select
    shifts = SiteConfig.dispatch_shifts
    if shifts.empty?
      ['None Defined',nil]
    else
      temp = []
      shifts.each do |shift|
        temp.push(["#{shift} -- #{SiteConfig.send("dispatch_shift#{shift}_start")}-#{SiteConfig.send("dispatch_shift#{shift}_stop")}", shift])
      end
      temp
    end
  end
  
  def self.for_shift(shift, date=Date.today)
    return [] unless SiteConfig.dispatch_shifts.include?(shift) && date.is_a?(Date)
    start = AppUtils.get_datetime(date, SiteConfig.send("dispatch_shift#{shift}_start"))
    stop = AppUtils.get_datetime(date, SiteConfig.send("dispatch_shift#{shift}_stop"))
    if stop < start
      # night shift
      stop = AppUtils.get_datetime(date + 1.day, SiteConfig.send("dispatch_shift#{shift}_stop"))
    end
    # give up if resulting times are invalid
    return [] if start.nil? || stop.nil?
    by_date.with_call_date(start,stop)
  end
  
  # process query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_call_date(query[:call_from], query[:call_to]) unless query[:call_form].blank? && query[:call_to].blank?
    result = result.with_signal_code(query[:signal_code]) unless query[:signal_code].blank?
    result = result.with_signal_name(query[:signal_name]) unless query[:signal_name].blank?
    result = result.with_case_no(query[:case_no]) unless query[:case_no].blank?
    result = result.with_received_via(query[:received_via]) unless query[:received_via].blank?
    result = result.with_received_by(query[:received_by],query[:received_by_unit],query[:received_by_badge]) unless query[:received_by].blank? && query[:received_by_unit].blank? && query[:received_by_badge]
    result = result.with_ward(query[:ward]) unless query[:ward].blank?
    result = result.with_district(query[:district]) unless query[:district].blank?
    result = result.with_zone(query[:zone]) unless query[:zone].blank?
    result = result.with_name(query[:name]) unless query[:name].blank?
    result = result.with_address(query[:address]) unless query[:address].blank?
    result = result.with_details(query[:details]) unless query[:details].blank?
    result = result.with_units(query[:units]) unless query[:units].blank?
    result = result.with_disposition(query[:disposition]) unless query[:disposition].blank?
    result = result.with_detective(query[:detective],query[:detective_unit],query[:detective_badge]) unless query[:detective].blank? && query[:detective_unit].blank? && query[:detective_badge].blank?
    result = result.with_dispatcher(query[:dispatcher],query[:dispatcher_unit],query[:dispatcher_badge]) unless query[:dispatcher].blank? && query[:dispatcher_unit].blank? && query[:dispatcher_badge].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def reasons_not_destroyable
    reasons = []
    reasons.push("Has Linked Offenses") unless offenses.empty?
    reasons.push("Has Linked Investigations") unless investigations.empty?
    reasons.push("Has Linked Forbids") unless forbids.empty?
    reasons.empty? ? nil : reasons.join(', ')
  end
  
  def subject_summary
    call_subjects.join(', ')
  end
  
  def units
    call_units.join(", ")
  end
  
  def check_destroyable
    reasons_not_destroyable.nil?
  end
  
  def check_or_generate_case
    self.followup_no = AppUtils.fix_case(followup_no)
    unless followup_no.blank?
      unless Case.valid_case(followup_no)
        self.errors.add(:followup_no, "must be a valid existing case!")
        return false
      end
      self.case_no = followup_no
    end
    self.case_no = AppUtils.fix_case(case_no)
    unless case_no?
      self.case_no = Case.get_next_case_number
    end
    return true
  end
  
  def check_signal
    retval = true
    if signal_code.blank?
      self.errors.add(:signal_code, "is required!")
      retval = false
    else
      self.signal_code = signal_code.upcase
      sname = SignalCode.get_signal(signal_code)
      if sname.nil?
        self.errors.add(:signal_code, "not recognized!  Please check spelling.")
        retval = false
      else
        self.signal_name = sname
      end
    end
    return retval
  end
  
  def lookup_officers
    unless received_by_id.blank?
      if ofc = Contact.find_by_id(received_by_id)
        self.received_by = ofc.sort_name
        self.received_by_badge = ofc.badge_no
        self.received_by_unit = ofc.unit
      end
      self.received_by_id = nil
    end
    unless dispatcher_id.blank?
      if ofc = Contact.find_by_id(dispatcher_id)
        self.dispatcher = ofc.sort_name
        self.dispatcher_badge = ofc.badge_no
        self.dispatcher_unit = ofc.unit
      end
      self.dispatcher_id = nil
    end
    unless detective_id.blank?
      if ofc = Contact.find_by_id(detective_id)
        self.detective = ofc.sort_name
        self.detective_badge = ofc.badge_no
        self.detective_unit = ofc.unit
      end
      self.detective_id = nil
    end
    return true
  end
  
  def send_messages
    receivers = User.where(notify_call_new: true).all
    receivers.each do |r|
      next unless r.can?(self.class.base_perms)
      txt = AutomatedMessagesController.new.notify(self, 'new')
      Message.create(receiver_id: r.id, sender_id: updated_by, subject: "New Call Sheet (Case: #{case_no})", body: txt)
    end
  end
end

# == Schema Information
#
# Table name: calls
#
#  id                  :integer          not null, primary key
#  case_no             :string(255)      default(""), not null
#  received_by         :string(255)      default(""), not null
#  details             :text             default(""), not null
#  dispatcher          :string(255)      default(""), not null
#  ward                :string(255)      default(""), not null
#  district            :string(255)      default(""), not null
#  remarks             :text             default(""), not null
#  created_at          :datetime
#  updated_at          :datetime
#  created_by          :integer          default(1)
#  updated_by          :integer          default(1)
#  dispatcher_unit     :string(255)      default(""), not null
#  received_by_unit    :string(255)      default(""), not null
#  received_by_badge   :string(255)      default(""), not null
#  dispatcher_badge    :string(255)      default(""), not null
#  detective           :string(255)      default(""), not null
#  detective_unit      :string(255)      default(""), not null
#  detective_badge     :string(255)      default(""), not null
#  legacy              :boolean          default(FALSE), not null
#  call_datetime       :datetime
#  dispatch_datetime   :datetime
#  arrival_datetime    :datetime
#  concluded_datetime  :datetime
#  received_via        :string(255)      default(""), not null
#  disposition         :string(255)      default(""), not null
#  signal_code         :string(255)      default(""), not null
#  signal_name         :string(255)      default(""), not null
#  zone                :string(255)      default(""), not null
#  call_subjects_count :integer          default(0), not null
#
# Indexes
#
#  index_calls_on_case_no      (case_no)
#  index_calls_on_created_by   (created_by)
#  index_calls_on_signal_code  (signal_code)
#  index_calls_on_updated_by   (updated_by)
#
# Foreign Keys
#
#  calls_created_by_fk  (created_by => users.id)
#  calls_updated_by_fk  (updated_by => users.id)
#
