class Transport < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :person, inverse_of: :transports                           # constraint

  scope :by_reverse_date, -> { order begin_datetime: :desc }
  
  # filter scopes
  scope :with_transport_id, ->(t_id) { where id: t_id }
  scope :with_person_id, ->(p_id) { where person_id: p_id }
  scope :with_full_name, ->(name) { AppUtils.for_person self, name, association: :person }
  scope :with_officer, ->(name,unit,badge) { AppUtils.for_officer self, [:officer1, :officer2], name, unit, badge }
  scope :with_begin_date, ->(start,stop) { AppUtils.for_daterange self, :begin_datetime, start: start, stop: stop }
  scope :with_agency, ->(a) { AppUtils.like self, [:from, :to], a }
  scope :with_city, ->(c) { AppUtils.like self, [:from_city, :to_city], c }
  scope :with_state, ->(s) { AppUtils.for_lower self, [:from_state, :to_state], s }
  scope :with_zip, ->(z) { AppUtils.like self, [:from_zip, :to_zip], z }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  
  attr_accessor :officer1_id, :officer2_id, :from_id, :to_id
  
  before_save :check_creator
  before_validation :lookup_contacts

  validates_presence_of :officer1, :from, :to
  validates_numericality_of :begin_miles, :end_miles, only_integer: true, allow_nil: true
  validates_datetime :begin_datetime, :end_datetime, on_or_before: :now, allow_blank: true
  validates_zip :from_zip, :to_zip, allow_nil: true, allow_blank: true
  validates_person :person_id

  ## Class Methods #############################################
  
  def self.base_perms
    :view_transport
  end
  
  def self.desc
    "Prisoner Transportation Database"
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_transport_id(query[:id]) unless query[:id].blank?
    result = result.with_person_id(query[:person_id]) unless query[:person_id].blank?
    result = result.with_full_name(query[:full_name]) unless query[:full_name].blank?
    result = result.with_officer(query[:officer], query[:officer_unit], query[:officer_badge]) unless query[:officer].blank? && query[:officer_unit].blank? && query[:officer_badge].blank?
    result = result.with_begin_date(query[:begin_date_from], query[:begin_date_to]) unless query[:begin_date_from].blank? && query[:begin_date_to].blank?
    result = result.with_agency(query[:agency]) unless query[:agency].blank?
    result = result.with_city(query[:city]) unless query[:city].blank?
    result = result.with_state(query[:state]) unless query[:state].blank?
    result = result.with_zip(query[:zip]) unless query[:zip].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def duration
    return "0" unless begin_datetime? && end_datetime?
    AppUtils.duration_in_words(end_datetime, begin_datetime)
  end
  
  def from_address_line1
    from_street.titleize
  end
  
  def from_address_line2
    name = ""
    if from_city
      name << from_city.titleize
    end
    if from_state?
      unless name.blank?
        name << ", "
      end
      name << from_state
    end
    if from_zip?
      unless name.blank?
        name << " "
      end
      name << from_zip
    end
    return name
  end
  
  def mileage
    AppUtils.total_miles(begin_miles, end_miles)
  end
  
  def to_address_line1
    to_street.titleize
  end
  
  def to_address_line2
    name = ""
    if to_city
      name << to_city.titleize
    end
    if to_state
      unless name.empty?
        name << ", "
      end
      name << to_state
    end
    if to_zip?
      unless name.empty?
        name << " "
      end
      name << to_zip
    end
    return name
  end

  private

  ## Private Instance Methods #############################################
  
  def lookup_contacts
    unless officer1_id.blank?
      if ofc = Contact.find_by_id(officer1_id)
        self.officer1 = ofc.sort_name
        self.officer1_badge = ofc.badge_no
        self.officer1_unit = ofc.unit
      end
      self.officer1_id = nil
    end
    unless officer2_id.blank?
      if ofc = Contact.find_by_id(officer2_id)
        self.officer2 = ofc.sort_name
        self.officer2_badge = ofc.badge_no
        self.officer2_unit = ofc.unit
      end
      self.officer2_id = nil
    end
    unless from_id.blank?
      if ag = Contact.find_by_id(from_id)
        self.from = ag.sort_name
        self.from_street = ag.street
        self.from_city = ag.city
        self.from_state = ag.state
        self.from_zip = ag.zip
      end
      self.from_id = nil
    end
    unless to_id.blank?
      if ag = Contact.find_by_id(to_id)
        self.to = ag.sort_name
        self.to_street = ag.street
        self.to_city = ag.city
        self.to_state = ag.state
        self.to_zip = ag.zip
      end
      self.to_id = nil
    end
    true
  end
end

# == Schema Information
#
# Table name: transports
#
#  id              :integer          not null, primary key
#  person_id       :integer          not null
#  officer1        :string(255)      default(""), not null
#  officer1_unit   :string(255)      default(""), not null
#  officer2        :string(255)      default(""), not null
#  officer2_unit   :string(255)      default(""), not null
#  begin_miles     :integer          default(0), not null
#  end_miles       :integer          default(0), not null
#  estimated_miles :integer          default(0), not null
#  from            :string(255)      default(""), not null
#  from_street     :string(255)      default(""), not null
#  from_city       :string(255)      default(""), not null
#  from_zip        :string(255)      default(""), not null
#  to              :string(255)      default(""), not null
#  to_street       :string(255)      default(""), not null
#  to_city         :string(255)      default(""), not null
#  to_zip          :string(255)      default(""), not null
#  remarks         :text             default(""), not null
#  created_by      :integer          default(1)
#  updated_by      :integer          default(1)
#  created_at      :datetime
#  updated_at      :datetime
#  officer1_badge  :string(255)      default(""), not null
#  officer2_badge  :string(255)      default(""), not null
#  from_state      :string(255)      default(""), not null
#  to_state        :string(255)      default(""), not null
#  begin_datetime  :datetime
#  end_datetime    :datetime
#
# Indexes
#
#  index_transports_on_created_by  (created_by)
#  index_transports_on_person_id   (person_id)
#  index_transports_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  transports_created_by_fk  (created_by => users.id)
#  transports_person_id_fk   (person_id => people.id)
#  transports_updated_by_fk  (updated_by => users.id)
#
