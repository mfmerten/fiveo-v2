class OffensePhoto < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :offense, inverse_of: :offense_photos                      # constraint
  
  scope :by_reverse_offense_and_date, -> { order offense_id: :desc, date_taken: :desc }
  scope :with_case_no, ->(c_no) { where case_no: c_no }
  
  attr_accessor :taken_by_id, :photo_form
  
  has_attached_file(
    :crime_scene_photo,
    url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/:attachment/:id/:style/:basename.:extension",
    path: ":rails_root/public/system/:attachment/:id/:style/:basename.:extension",
    styles: {thumb: ["80x80", :jpg], medium: ["300x300",:jpg], small: ["150x150",:jpg]},
    default_url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/default_images/no_image_:style.jpg",
    default_style: :original,
    storage: :filesystem
  )
  
  before_save :check_creator
  before_validation :adjust_case_no, :lookup_officers
  
  validates_presence_of :description, :location_taken, :case_no
  validates_date :date_taken
  validates_offense :offense_id
  validates_attachment_content_type :crime_scene_photo, content_type: ["image/jpg","image/jpeg","image/gif","image/png","image/tiff"]
    
  ## Class Methods #############################################
  
  def self.base_perms
    :view_offense
  end
  
  def self.desc
    "Offense Photo Database"
  end
  
  private
  
  ## Instance Methods #############################################
  
  def lookup_officers
    unless taken_by_id.blank?
      if ofc = Contact.find_by_id(taken_by_id)
        self.taken_by = ofc.sort_name
        self.taken_by_unit = ofc.unit
        self.taken_by_badge = ofc.badge_no
      end
      self.taken_by_id = nil
    end
    true
  end
  
  def adjust_case_no
    self.case_no = AppUtils.fix_case(case_no)
    true
  end
end

# == Schema Information
#
# Table name: offense_photos
#
#  id                             :integer          not null, primary key
#  offense_id                     :integer          not null
#  case_no                        :string(255)      default(""), not null
#  crime_scene_photo_file_name    :string(255)
#  crime_scene_photo_content_type :string(255)
#  crime_scene_photo_file_size    :integer
#  crime_scene_photo_updated_at   :datetime
#  description                    :string(255)      default(""), not null
#  date_taken                     :date
#  location_taken                 :string(255)      default(""), not null
#  taken_by                       :string(255)      default(""), not null
#  taken_by_unit                  :string(255)      default(""), not null
#  remarks                        :text             default(""), not null
#  created_by                     :integer          default(1)
#  updated_by                     :integer          default(1)
#  created_at                     :datetime
#  updated_at                     :datetime
#  taken_by_badge                 :string(255)      default(""), not null
#
# Indexes
#
#  index_offense_photos_on_case_no     (case_no)
#  index_offense_photos_on_created_by  (created_by)
#  index_offense_photos_on_offense_id  (offense_id)
#  index_offense_photos_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  offense_photos_created_by_fk  (created_by => users.id)
#  offense_photos_offense_id_fk  (offense_id => offenses.id)
#  offense_photos_updated_by_fk  (updated_by => users.id)
#
