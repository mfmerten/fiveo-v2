class InvestContact < ActiveRecord::Base
  belongs_to :investigation, inverse_of: :invest_contacts                  # constraint
  belongs_to :contact, inverse_of: :invest_contacts                        # constraint
  has_many :invest_crime_contacts, dependent: :destroy                     # constraint
  has_many :crimes, through: :invest_crime_contacts, source: :invest_crime # constrained through invest_crime_contacts

  ## Class Methods #############################################
  
  def self.desc
    "Investigation Contacts"
  end
end

# == Schema Information
#
# Table name: invest_contacts
#
#  id               :integer          not null, primary key
#  investigation_id :integer          not null
#  contact_id       :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_invest_contacts_on_contact_id        (contact_id)
#  index_invest_contacts_on_investigation_id  (investigation_id)
#
# Foreign Keys
#
#  invest_contacts_contact_id_fk        (contact_id => contacts.id)
#  invest_contacts_investigation_id_fk  (investigation_id => investigations.id)
#
