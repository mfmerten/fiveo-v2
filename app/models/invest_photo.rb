class InvestPhoto < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :owner, class_name: 'User', foreign_key: 'owned_by'        # constraint with nullify
  belongs_to :investigation, inverse_of: :photos                        # constraint
  has_many :notes, class_name: 'InvestNote', dependent: :destroy        # constraint
  has_many :invest_crime_photos, dependent: :destroy                    # constraint
  has_many :crimes, through: :invest_crime_photos, source: :invest_crime # constrained through invest_crime_photos
  has_many :invest_criminal_photos, dependent: :destroy                 # constraint
  has_many :criminals, through: :invest_criminal_photos, source: :invest_criminal  # constrained through invest_criminal_photos
  has_many :invest_vehicle_photos, dependent: :destroy                  # constraint
  has_many :vehicles, through: :invest_vehicle_photos, source: :invest_vehicle  # constrained through invest_vehicle_photos
  has_many :invest_report_photos, dependent: :destroy                   # constraint
  has_many :reports, through: :invest_report_photos, source: :invest_report  # constrained through invest_report_photos

  scope :by_reverse_id, -> { order id: :desc }
  
  # filter scopes
  scope :with_invest_photo_id, ->(p_id) { where id: p_id }
  scope :with_investigation_id, ->(i_id) { where investigation_id: i_id }
  scope :with_invest_crime_id, ->(c_id) { joins(:invest_crime_photos).where(invest_crime_photos: {invest_crime_id: c_id}) }
  scope :with_invest_criminal_id, ->(c_id) { joins(:invest_criminal_photos).where(invest_criminal_photos: {invest_criminal_id: c_id}) }
  scope :with_invest_vehicle_id, ->(v_id) { joins(:invest_vehicle_photos).where(invest_vehicle_photos: {invest_vehicle_id: v_id}) }
  scope :with_invest_report_id, ->(r_id) { joins(:invest_report_photos).where(invest_report_photos: {invest_report_id: r_id}) }
  scope :with_date_taken, ->(start,stop) { AppUtils.for_daterange self, :date_taken, start: start, stop: stop, date: true }
  scope :with_location_taken, ->(loc) { AppUtils.like self, :location_taken, loc }
  scope :with_description, ->(des) { AppUtils.like self, :description, des }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  scope :with_taken_by, ->(name,unit,badge) { AppUtils.for_officer self, :taken_by, name, unit, badge }
  
  attr_accessor :taken_by_id, :photo_form
  
  has_attached_file(
    :invest_photo,
    url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/:attachment/:id/:style/:basename.:extension",
    path: ":rails_root/public/system/:attachment/:id/:style/:basename.:extension",
    styles: {thumb: ["80x80", :jpg], medium: ["300x300",:jpg], small: ["150x150",:jpg]},
    default_url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/default_images/no_image_:style.jpg",
    default_style: :original,
    storage: :filesystem
  )

  before_save :check_creator
  before_validation :lookup_officers

  validates_date :date_taken
  validates_presence_of :location_taken, :description, :taken_by
  validates_attachment_content_type :invest_photo, content_type: ["image/jpg","image/jpeg","image/gif","image/png","image/tiff"]

  ## Class Methods #############################################
  
  def self.desc
    "Photos for Detective Investigations"
  end
  
  def self.base_perms
    :view_investigation
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_invest_photo_id(query[:id]) unless query[:id].blank?
    result = result.with_investigation_id(query[:investigation_id]) unless query[:investigation_id].blank?
    result = result.with_invest_crime_id(query[:invest_crime_id]) unless query[:invest_crime_id].blank?
    result = result.with_invest_criminal_id(query[:invest_criminal_id]) unless query[:invest_criminal_id].blank?
    result = result.with_invest_vehicle_id(query[:invest_vehicle_id]) unless query[:invest_vehicle_id].blank?
    result = result.with_invest_report_id(query[:invest_report_id]) unless query[:invest_report_id].blank?
    result = result.with_date_taken(query[:date_taken_from],query[:date_taken_to]) unless query[:date_taken_from].blank? && query[:date_taken_to].blank?
    result = result.with_location_taken(query[:location_taken]) unless query[:location_taken].blank?
    result = result.with_description(query[:description]) unless query[:description].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result = result.with_taken_by(query[:taken_by], query[:taken_by_unit], query[:taken_by_badge]) unless query[:taken_by].blank? && query[:taken_by_unit].blank? && query[:taken_by_badge].blank?
    result
  end
  
  ## Instance Methods ##########################################
  
  def linked_crimes=(selected_crimes)
    self.crime_ids = selected_crimes.reject{|c| c.blank?}.collect{|c| c.to_i}.uniq
  end
  
  def linked_criminals=(selected_criminals)
    self.criminal_ids = selected_criminals.reject{|c| c.blank?}.collect{|c| c.to_i}.uniq
  end
  
  def linked_vehicles=(selected_vehicles)
    self.vehicle_ids = selected_vehicles.reject{|v| v.blank?}.collect{|v| v.to_i}.uniq
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def lookup_officers
    unless taken_by_id.blank?
      if ofc = Contact.find_by_id(taken_by_id)
        self.taken_by = ofc.sort_name
        self.taken_by_badge = ofc.badge_no
        self.taken_by_unit = ofc.unit
      end
      self.taken_by_id = nil
    end
    true
  end
end

# == Schema Information
#
# Table name: invest_photos
#
#  id                        :integer          not null, primary key
#  investigation_id          :integer          not null
#  invest_photo_file_name    :string(255)
#  invest_photo_content_type :string(255)
#  invest_photo_file_size    :integer
#  invest_photo_updated_at   :datetime
#  private                   :boolean          default(TRUE), not null
#  description               :string(255)      default(""), not null
#  date_taken                :date
#  location_taken            :string(255)      default(""), not null
#  taken_by                  :string(255)      default(""), not null
#  taken_by_unit             :string(255)      default(""), not null
#  taken_by_badge            :string(255)      default(""), not null
#  remarks                   :text             default(""), not null
#  created_by                :integer          default(1)
#  updated_by                :integer          default(1)
#  created_at                :datetime
#  updated_at                :datetime
#  owned_by                  :integer          default(1)
#
# Indexes
#
#  index_invest_photos_on_created_by        (created_by)
#  index_invest_photos_on_investigation_id  (investigation_id)
#  index_invest_photos_on_owned_by          (owned_by)
#  index_invest_photos_on_updated_by        (updated_by)
#
# Foreign Keys
#
#  invest_photos_created_by_fk        (created_by => users.id)
#  invest_photos_investigation_id_fk  (investigation_id => investigations.id)
#  invest_photos_owned_by_fk          (owned_by => users.id)
#  invest_photos_updated_by_fk        (updated_by => users.id)
#
