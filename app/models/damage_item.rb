class DamageItem < ActiveRecord::Base
  
  ## Class Methods #############################################
  
  belongs_to :damage, inverse_of: :damage_items

  ## Class Methods #############################################
  
  validates_presence_of :description
  validates_numericality_of :estimate, greater_than_or_equal_to: 0, less_than: 10 ** 10

  ## Class Methods #############################################
  
  def self.desc
    "Criminal Damage Items"
  end
end

# == Schema Information
#
# Table name: damage_items
#
#  id          :integer          not null, primary key
#  damage_id   :integer          not null
#  description :string(255)      default(""), not null
#  estimate    :decimal(12, 2)   default(0.0), not null
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_damage_items_on_damage_id  (damage_id)
#
# Foreign Keys
#
#  damage_items_damage_id_fk  (damage_id => damages.id)
#
