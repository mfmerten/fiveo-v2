# Note the following:
#       owner_id is the Person ID for the registered owner of the vehicle and is
#            accessed through the :registered_owner association
#       owned_by is the User ID for the Detective that currently owns the record
#            and is accessed through the :owner association.
# Hopefully this will not cause too much confusion.
class InvestVehicle < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :owner, class_name: 'User', foreign_key: 'owned_by'        # constraint with nullify
  belongs_to :investigation, inverse_of: :vehicles                         # constraint
  belongs_to :registered_owner, class_name: 'Person', foreign_key: 'owner_id', inverse_of: :invest_vehicles  # constraint
  has_many :notes, class_name: 'InvestNote', dependent: :destroy        # constraint
  has_many :invest_vehicle_photos, dependent: :destroy                     # constraint
  has_many :photos, through: :invest_vehicle_photos, source: :invest_photo  # constrained through invest_vehicle_photos
  has_many :invest_crime_vehicles, dependent: :destroy                     # constraint
  has_many :crimes, through: :invest_crime_vehicles, source: :invest_crime  # constrained through invest_crime_vehicles
  has_many :invest_criminal_vehicles, dependent: :destroy                  # constraint 
  has_many :criminals, through: :invest_criminal_vehicles, source: :invest_criminal  # constrained through invest_criminal_vehicles
  has_many :invest_report_vehicles, dependent: :destroy                    # constraint
  has_many :reports, through: :invest_report_vehicles, source: :invest_report  # constrained through invest_report_vehicles
  
  scope :not_private, -> { where private: false }
  scope :by_reverse_id, -> { order id: :desc }
  
  # filter scopes
  scope :with_invest_vehicle_id, ->(v_id) { where id: v_id }
  scope :with_investigation_id, ->(i_id) { where investigation_id: i_id }
  scope :with_invest_criminal_id, ->(c_id) { joins(:invest_criminal_vehicles).where(invest_criminal_vehicles: {invest_criminal_id: c_id}) }
  scope :with_invest_crime_id, ->(c_id) { joins(:invest_crime_vehicles).where(invest_crime_vehicles: {invest_crime_id: c_id}) }
  scope :with_owner_id, ->(o_id) { where owner_id: o_id }
  scope :with_name, ->(n) { AppUtils.for_person self, n, association: :registered_owner }
  scope :with_make, ->(m) { AppUtils.like self, :make, m }
  scope :with_model, ->(m) { AppUtils.like self, :model, m }
  scope :with_vin, ->(v) { AppUtils.like self, :vin, v }
  scope :with_license, ->(lic) { AppUtils.like self, :license, lic }
  scope :with_license_state, ->(st) { AppUtils.for_lower self, :license_state, st }
  scope :with_color, ->(col) { AppUtils.like self, :color, col }
  scope :with_tires, ->(tir) { AppUtils.like self, :tires, tir }
  scope :with_model_year, ->(mody) { AppUtils.like self, :model_year, mody }
  scope :with_vehicle_type, ->(vt) { AppUtils.like self, :vehicle_type, vt }
  scope :with_description, ->(des) { AppUtils.like self, :description, des }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  
  before_save :check_creator

  validates_person :owner_id, allow_nil: true

  ## Class Methods #############################################
  
  def self.base_perms
    :view_investigation
  end
  
  def self.desc
    'Vehicles for Detective Investigations'
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_invest_vehicle_id(query[:id]) unless query[:id].blank?
    result = result.with_investigation_id(query[:investigation_id]) unless query[:investigation_id].blank?
    result = result.with_invest_criminal_id(query[:invest_criminal_id]) unless query[:invest_criminal_id].blank?
    result = result.with_invest_crime_id(query[:invest_crime_id]) unless query[:invest_crime_id].blank?
    result = result.with_owner_id(query[:owner_id]) unless query[:owner_id].blank?
    result = result.with_name(query[:name]) unless query[:name].blank?
    result = result.with_make(query[:make]) unless query[:make].blank?
    result = result.with_model(query[:model]) unless query[:model].blank?
    result = result.with_vin(query[:vin]) unless query[:vin].blank?
    result = result.with_license(query[:license]) unless query[:license].blank?
    result = result.with_license_state(query[:license_state]) unless query[:license_state].blank?
    result = result.with_color(query[:color]) unless query[:color].blank?
    result = result.with_tires(query[:tires]) unless query[:tires].blank?
    result = result.with_model_year(query[:model_year]) unless query[:model_year].blank?
    result = result.with_vehicle_type(query[:vehicle_type]) unless query[:vehicle_type].blank?
    result = result.with_description(query[:description]) unless query[:description].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def desc
    txt = [color, model_year, make, model, vehicle_type].reject{|a| a.blank?}.join(" ")
    opts = []
    unless vin.blank?
      opts.push("VIN #{vin}")
    end
    unless license.blank?
      opts.push("TAG #{license} #{license_state}")
    end
    unless tires.blank?
      opts.push("#{tires} tires")
    end
    txt2 = opts.join(', ')
    unless txt2.blank?
      if txt.blank?
        txt2 = "Has " + txt2
      else
        txt2 = ", with " + txt2
      end
    end
    txt = [txt, txt2].reject{|a| a.blank?}.join
    unless txt.blank?
      txt << "."
    end
    txt
  end
  
  def linked_crimes=(selected_crimes)
    self.crime_ids = selected_crimes.reject{|c| c.blank?}.collect{|c| c.to_i}.uniq
  end
  
  def linked_criminals=(selected_criminals)
    self.criminal_ids = selected_criminals.reject{|c| c.blank?}.collect{|c| c.to_i}.uniq
  end
  
  def linked_photos=(selected_photos)
    self.photo_ids = selected_photos.reject{|p| p.blank?}.collect{|p| p.to_i}.uniq
  end
end

# == Schema Information
#
# Table name: invest_vehicles
#
#  id               :integer          not null, primary key
#  investigation_id :integer          not null
#  private          :boolean          default(TRUE), not null
#  owner_id         :integer
#  make             :string(255)      default(""), not null
#  model            :string(255)      default(""), not null
#  vin              :string(255)      default(""), not null
#  license          :string(255)      default(""), not null
#  color            :string(255)      default(""), not null
#  tires            :string(255)      default(""), not null
#  model_year       :string(255)      default(""), not null
#  description      :text             default(""), not null
#  remarks          :text             default(""), not null
#  created_by       :integer          default(1)
#  updated_by       :integer          default(1)
#  created_at       :datetime
#  updated_at       :datetime
#  owned_by         :integer          default(1)
#  license_state    :string(255)      default(""), not null
#  vehicle_type     :string(255)      default(""), not null
#
# Indexes
#
#  index_invest_vehicles_on_created_by        (created_by)
#  index_invest_vehicles_on_investigation_id  (investigation_id)
#  index_invest_vehicles_on_owned_by          (owned_by)
#  index_invest_vehicles_on_owner_id          (owner_id)
#  index_invest_vehicles_on_updated_by        (updated_by)
#
# Foreign Keys
#
#  invest_vehicles_created_by_fk        (created_by => users.id)
#  invest_vehicles_investigation_id_fk  (investigation_id => investigations.id)
#  invest_vehicles_owned_by_fk          (owned_by => users.id)
#  invest_vehicles_owner_id_fk          (owner_id => people.id)
#  invest_vehicles_updated_by_fk        (updated_by => users.id)
#
