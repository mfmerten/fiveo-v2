class PaperPayment < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :customer, class_name: 'PaperCustomer', foreign_key: 'paper_customer_id', inverse_of: :payments  # constraint
  belongs_to :paper, inverse_of: :payments                                 # constraint
  
  scope :posted, -> { where.not(report_datetime: nil) }
  scope :posted_on, ->(time) { where report_datetime: time }
  scope :unposted, -> { where report_datetime: nil }
  scope :by_date, -> { order :transaction_date, :id }
  
  attr_accessor :officer_id

  before_save :check_creator
  after_save :process_transaction
  before_validation :check_links, :check_amounts, :lookup_officers
  before_destroy :check_destroyable
  after_destroy :process_destroy

  validates_presence_of :officer, :paper_customer_id
  validates_presence_of :memo, if: :adjustment?
  validates_presence_of :paper_id, unless: :adjustment?
  validates_date :transaction_date
  validates_numericality_of :payment, :charge, greater_than_or_equal_to: 0, less_than: 10 ** 10

  ## Class Methods #############################################
  
  def self.base_perms
    :view_civil
  end
  
  def self.desc
    "Paper Service Payments"
  end
  
  def self.posting_dates
    select('DISTINCT report_datetime').order(report_datetime: :desc).where("report_datetime is not null").limit(10).all.collect{|c| [AppUtils.format_date(c.report_datetime),c.report_datetime]}
  end
  
  def self.types_for_select
    type_names = []
    (1..5).each do |x|
      unless SiteConfig.send("paper_pay_type#{x}").blank?
        type_names.push([SiteConfig.send("paper_pay_type#{x}"),SiteConfig.send("paper_pay_type#{x}")])
      end
    end
    type_names
  end
  
  ## Instance Methods #############################################
  
  def is_invoice?
    return false if paper.nil?
    self == paper.payments.first
  end
  
  def reasons_not_destroyable
    reasons = []
    reasons.push('Already Posted') if report_datetime?
    reasons.push('Invoice Transaction') if is_invoice? && paper.payments.size > 1
    reasons.empty? ? nil : reasons.join(', ')
  end
  
  def update_posted(report_time)
    if report_time.is_a?(Time)
      self.update_attribute(:report_datetime, report_time)
    end
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def check_amounts
    if adjustment?
      unless (charge? && charge != 0) || (payment? && payment > 0)
        self.errors.add(:charge, "cannot be 0 if payment is 0!")
        self.errors.add(:payment, "cannot be 0 if charge is 0!")
        return false
      end
    else
      unless payment?
        self.errors.add(:payment, "cannot be blank!")
      end
      unless paid_in_full?
        self.charge = 0
      end
    end
    if payment? && payment < 0
      self.errors.add(:payment, "cannot be negative!")
      return false
    end
    true
  end
  
  def check_destroyable
    reasons_not_destroyable.nil?
  end
  
  def check_links
    retval = true
    if paper_customer_id?
      if customer.nil?
        self.errors.add(:paper_customer_id, 'is not a valid customer!')
        retval = false
      end
    end
    if paper_id?
      if paper.nil?
        self.errors.add(:paper_id, 'is not a valid paper!')
        retval = false
      elsif paper.paper_customer_id != paper_customer_id
        self.errors.add(:paper_id, 'is not for this customer!')
        retval = false
      end
    end
    return retval
  end
  
  def lookup_officers
    unless officer_id.blank?
      if ofc = Contact.find_by_id(officer_id)
        self.officer = ofc.sort_name
        self.officer_badge = ofc.badge_no
        self.officer_unit = ofc.unit
      end
      self.officer_id = nil
    end
    true
  end
  
  def process_destroy
    unless paper.nil?
      paper.update_invoice_status
    end
    true
  end
  
  def process_transaction
    return true if adjustment?
    unless paper.nil?
      paper.update_invoice_status
    end
    if paid_in_full?
      total_charges = paper.payments.sum(:charge)
      total_paid = paper.payments.sum(:payment)
      self.update_column(:charge, total_paid - total_charges)
    end
    true
  end
end

# == Schema Information
#
# Table name: paper_payments
#
#  id                :integer          not null, primary key
#  paper_id          :integer
#  paper_customer_id :integer          not null
#  transaction_date  :date
#  officer           :string(255)      default(""), not null
#  officer_unit      :string(255)      default(""), not null
#  officer_badge     :string(255)      default(""), not null
#  receipt_no        :string(255)      default(""), not null
#  payment           :decimal(12, 2)   default(0.0), not null
#  charge            :decimal(12, 2)   default(0.0), not null
#  report_datetime   :datetime
#  memo              :string(255)      default(""), not null
#  created_by        :integer          default(1)
#  updated_by        :integer          default(1)
#  created_at        :datetime
#  updated_at        :datetime
#  payment_method    :string(255)      default(""), not null
#  paid_in_full      :boolean          default(FALSE), not null
#  adjustment        :boolean          default(FALSE), not null
#
# Indexes
#
#  index_paper_payments_on_created_by         (created_by)
#  index_paper_payments_on_paper_customer_id  (paper_customer_id)
#  index_paper_payments_on_paper_id           (paper_id)
#  index_paper_payments_on_updated_by         (updated_by)
#
# Foreign Keys
#
#  paper_payments_created_by_fk         (created_by => users.id)
#  paper_payments_paper_customer_id_fk  (paper_customer_id => paper_customers.id)
#  paper_payments_paper_id_fk           (paper_id => papers.id)
#  paper_payments_updated_by_fk         (updated_by => users.id)
#
