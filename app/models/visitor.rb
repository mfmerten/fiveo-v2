class Visitor < ActiveRecord::Base
  belongs_to :booking, inverse_of: :visitors                               # constraint
  
  delegate :jail, to: :booking
  
  scope :active, -> { where sign_out_time: nil }
  scope :by_date, -> { order :visit_date, :sign_in_time }
  scope :belongs_to_jail, ->(j) { where booking: {jail_id: j} }
  
  validates_presence_of :name, :street, :city, :state
  validates_zip :zip
  validates_phone :phone
  validates_date :visit_date
  validates_time_string :sign_in_time
  validates_time_string :sign_out_time, allow_nil: true
  validates_booking :booking_id
  
  ## Class Methods #############################################
  
  def self.desc
    'Jail Visitor Records'
  end
end

# == Schema Information
#
# Table name: visitors
#
#  id            :integer          not null, primary key
#  booking_id    :integer          not null
#  name          :string(255)      default(""), not null
#  street        :string(255)      default(""), not null
#  city          :string(255)      default(""), not null
#  zip           :string(255)      default(""), not null
#  phone         :string(255)      default(""), not null
#  visit_date    :date
#  sign_in_time  :string(255)      default(""), not null
#  sign_out_time :string(255)      default(""), not null
#  remarks       :text             default(""), not null
#  created_at    :datetime
#  updated_at    :datetime
#  legacy        :boolean          default(FALSE), not null
#  state         :string(255)      default(""), not null
#  relationship  :string(255)      default(""), not null
#
# Indexes
#
#  index_visitors_on_booking_id  (booking_id)
#
# Foreign Keys
#
#  visitors_booking_id_fk  (booking_id => bookings.id)
#
