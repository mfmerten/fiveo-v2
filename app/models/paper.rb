class Paper < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :customer, class_name: 'PaperCustomer', foreign_key: 'paper_customer_id', inverse_of: :papers  # constraint
  has_many :payments, class_name: 'PaperPayment', dependent: :destroy   # constraint
  
  scope :by_reverse_id, -> { order id: :desc }
  scope :active, -> { where "(papers.billable IS TRUE AND papers.paid_date IS NULL) or (papers.billable IS FALSE AND papers.served_datetime IS NULL AND papers.returned_date IS NULL)" }
  scope :belongs_to_customer, ->(c_id) { where paper_customer_id: c_id }
  scope :billable, -> { where "papers.billable IS TRUE AND papers.invoice_datetime IS NULL AND (papers.served_datetime IS NOT NULL OR papers.returned_date IS NOT NULL)" }
  scope :payable, -> { where "papers.invoice_datetime IS NOT NULL AND papers.paid_date IS NULL AND papers.invoice_total > 0" }
  scope :is_invoice, -> { where "papers.invoice_datetime IS NOT NULL AND papers.paid_date IS NULL" }
  
  # filter scopes
  scope :with_paper_id, ->(p_id) { where id: p_id }
  scope :with_paper_type, ->(p_type) { AppUtils.like self, :paper_type, p_type }
  scope :with_court_date, ->(c_date) { AppUtils.for_date self, :court_date, c_date }
  scope :with_suit_no, ->(s_no) { AppUtils.like self, :suit_no, s_no }
  scope :with_name, ->(n) { AppUtils.like self, [:plaintiff, :defendant, :serve_to, :served_to], n }
  scope :with_billable, ->(b) { AppUtils.for_bool self, :billable, b }
  scope :with_paper_customer_id, ->(p_id) { where paper_customer_id: p_id }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  
  attr_accessor :officer_id, :paper_type_id
  
  before_validation :lookup_officers, :check_noservice, :check_customer, :check_type
  before_save :check_creator, :fix_fees
  before_destroy :check_destroyable

  validates_presence_of :suit_no, :plaintiff, :defendant, :serve_to, :paper_type
  validates_phone :phone, allow_blank: true
  validates_zip :zip, allow_blank: true
  validates_date :received_date
  validates_datetime :served_datetime, allow_blank: true
  validates_date :returned_date, :paid_date, allow_blank: true
  validates_date :court_date, allow_blank: true
  validates_presence_of :officer, :served_to, :service_type, if: :served_datetime?
  validates_presence_of :officer, :service_type, if: :returned_date?
  validates_numericality_of :mileage, greater_than_or_equal_to: 0, less_than: 10 ** 4
  validates_numericality_of :service_fee, :noservice_fee, :mileage_fee, greater_than_or_equal_to: 0, less_than: 10 ** 10

  NoserviceType = [
    "Moved, Address Unknown", "Does Not Live at This Address",
    "Please Provide Physical Address", "Address Not In Parish",
    "Unable to Locate", "Vacant House/Lot/Camp", "Works Out of State",
    "In Jail Out of Parish", "No Such Number or Address",
    "Tagged and Sent To Clerks ON", "Received Too Late for Service",
    "Paid Per DAs Office", "Deceased", "Moved To:", "Military Reasons"
  ].freeze

  ServiceType = ['Domiciliary','No Service','Personal'].freeze
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_civil
  end
  
  def self.desc
    "Paper Service"
  end
  
  def self.find_by_name(serve_to)
    return nil unless serve_to.is_a?(String) && !serve_to.blank?
    where(serve_to: serve_to).last
  end
  
  def self.find_by_suit(suit_no)
    return nil unless suit_no.is_a?(String) && !suit_no.blank?
    where(suit_no: suit_no).last
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_paper_id(query[:id]) unless query[:id].blank?
    result = result.with_paper_type(query[:paper_type]) unless query[:paper_type].blank?
    result = result.with_court_date(query[:court_date]) unless query[:cout_date].blank?
    result = result.with_suit_no(query[:suit_no]) unless query[:suit_no].blank?
    result = result.with_name(query[:name]) unless query[:name].blank?
    result = result.with_billable(query[:billable]) unless query[:billable].blank?
    result = result.with_paper_customer_id(query[:paper_customer_id]) unless query[:paper_customer_id].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def address
    [address_line1, address_line2].reject{|a| a.blank?}.join(', ')
  end
  
  def address_line1
    street
  end

  def address_line2
    name = ""
    if city
      name << city
    end
    if state?
      unless name.empty?
        name << ", "
      end
      name << state
    end
    if zip?
      unless name.empty?
        name << " "
      end
      name << zip
    end
    return name
  end
  
  def can_uninvoice?
    payments.posted.count == 0
  end
  
  def invoice_due(edate=nil)
    return 0 if !billable? || !invoice_datetime?
    invoice_total - total_payments(edate)
  end
  
  def map_address
    "#{street}, #{city}#{state? ? ', ' + state : ''}"
  end
  
  def status
    if paid_date?
      "Paid"
    elsif invoice_datetime?
      "Payable"
    elsif served_datetime? || returned_date?
      if billable?
        "Billable"
      else
        'No-Bill'
      end
    else
      "Servable"
    end
  end
  
  def total_charges(edate=nil)
    trans = payments
    if edate.is_a?(Date)
      trans = payments.where('transaction_date <= ?',edate)
    end
    trans.sum(:charge)
  end
  
  def total_payments(edate=nil)
    trans = payments
    if edate.is_a?(Date)
      trans = payments.where('transaction_date <= ?',edate)
    end
    trans.sum(:payment)
  end
  
  def uninvoice
    return false unless can_uninvoice?
    payments.reverse.each{|p| p.destroy}
    true
  end
  
  def update_invoice_status
    # NOTE: test everything here, do not want to save this record unless it
    #       really needs it.
    # check for payments
    if payments(true).empty?
      # not invoiced any more
      if invoice_datetime?
        self.invoice_datetime = nil
      end
      unless invoice_total == 0
        self.invoice_total = 0
      end
      if paid_in_full?
        self.paid_in_full = false
      end
      if paid_date?
        self.paid_date = nil
      end
    else
      if invoice_datetime != payments.first.transaction_date
        self.invoice_datetime = payments.first.transaction_date
        self.updated_by = payments.first.updated_by
      end
      if invoice_total != payments.first.charge
        self.invoice_total = payments.first.charge
        self.updated_by = payments.first.updated_by
      end
      # now check payment info if any
      if payments.size == 1
        # only invoice transaction exists, initialize payment info
        if paid_date?
          self.paid_date = nil
        end
        if paid_in_full?
          self.paid_in_full = false
        end
      elsif total_payments - total_charges == 0
        # all paid up normally
        unless paid_date == payments.last.transaction_date
          self.paid_date = payments.last.transaction_date
        end
        unless paid_in_full?
          self.paid_in_full = true
        end
      else
        # not enough payments to equal total due
        # check if last payment is paid_in_full?
        if payments.last.paid_in_full?
          unless paid_in_full?
            self.paid_in_full = true
          end
          unless paid_date == payments.last.transaction_date
            self.paid_date = payments.last.transaction_date
          end
        else
          if paid_in_full?
            self.paid_in_full = false
          end
          if paid_date?
            self.paid_date = nil
          end
        end
      end
    end
    if changed?
      self.save(validate: false)
    end
  end
  
  def reasons_not_destroyable
    payments.posted.count > 0 ? "Has Posted Payments" : nil
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def check_customer
    if paper_customer_id?
      if customer.nil?
        self.errors.add(:paper_customer_id, "is not a valid Customer ID!")
        return false
      end
    else
      if billable?
        self.errors.add(:paper_customer_id, "is required if this paper is billable!")
        return false
      end
    end
    true
  end
  
  def check_destroyable
    reasons_not_destroyable.nil?
  end
  
  # checks for a service type of no service and enforces entry of the 
  # noservice type.  Checks noservice type and if Moved To:, enforces entry
  # of noservice_extra (for new location)
  def check_noservice
    if service_type == "No Service"
      if noservice_type.blank?
        self.errors.add(:noservice_type, "must be set if Service type is 'No Service'!")
        return false
      end
      if noservice_type == "Moved To:"
        unless noservice_extra?
          self.errors.add(:noservice_extra, "cannot be blank if No Service Type is 'Moved To:'.  Enter new location here.")
          return false
        end
      end
    end
    true
  end
  
  def check_type
    unless paper_type_id.blank?
      if ptype = PaperType.find_by_id(paper_type_id)
        self.paper_type = ptype.name
        self.service_fee = ptype.cost
      end
      self.paper_type_id = nil
    end
    true
  end
  
  def fix_fees
    if noservice_fee == 0
      if customer.nil?
        self.noservice_fee = BigDecimal.new("0.0",2)
      elsif customer.noservice_fee?
        self.noservice_fee = customer.noservice_fee
      else
        self.noservice_fee = SiteConfig.noservice_fee
      end
    end
    if mileage_fee == 0
      if customer.nil?
        self.mileage_fee = BigDecimal.new("0.0",2)
      elsif customer.mileage_fee?
        self.mileage_fee = customer.mileage_fee
      else
        self.mileage_fee = SiteConfig.mileage_fee
      end
    end
    true
  end
  
  def lookup_officers
    unless officer_id.blank?
      if ofc = Contact.find_by_id(officer_id)
        self.officer = ofc.sort_name
        self.officer_badge = ofc.badge_no
        self.officer_unit = ofc.unit
      end
      self.officer_id = nil
    end
    true
  end
end

# == Schema Information
#
# Table name: papers
#
#  id                :integer          not null, primary key
#  suit_no           :string(255)      default(""), not null
#  plaintiff         :string(255)      default(""), not null
#  defendant         :string(255)      default(""), not null
#  paper_customer_id :integer
#  serve_to          :string(255)      default(""), not null
#  street            :string(255)      default(""), not null
#  city              :string(255)      default(""), not null
#  zip               :string(255)      default(""), not null
#  phone             :string(255)      default(""), not null
#  received_date     :date
#  returned_date     :date
#  noservice_extra   :string(255)      default(""), not null
#  served_to         :string(255)      default(""), not null
#  officer           :string(255)      default(""), not null
#  officer_badge     :string(255)      default(""), not null
#  officer_unit      :string(255)      default(""), not null
#  comments          :text             default(""), not null
#  billable          :boolean          default(TRUE), not null
#  invoice_datetime  :datetime
#  paid_date         :date
#  invoice_total     :decimal(12, 2)   default(0.0), not null
#  noservice_fee     :decimal(12, 2)   default(0.0), not null
#  mileage_fee       :decimal(12, 2)   default(0.0), not null
#  service_fee       :decimal(12, 2)   default(0.0), not null
#  memo              :string(255)      default(""), not null
#  remarks           :text             default(""), not null
#  created_by        :integer          default(1)
#  updated_by        :integer          default(1)
#  created_at        :datetime
#  updated_at        :datetime
#  court_date        :date
#  mileage           :decimal(5, 1)    default(0.0), not null
#  report_datetime   :datetime
#  mileage_only      :boolean          default(FALSE), not null
#  state             :string(255)      default(""), not null
#  served_datetime   :datetime
#  noservice_type    :string(255)      default(""), not null
#  service_type      :string(255)      default(""), not null
#  paid_in_full      :boolean          default(FALSE), not null
#  paper_type        :string(255)      default(""), not null
#
# Indexes
#
#  index_papers_on_created_by         (created_by)
#  index_papers_on_paper_customer_id  (paper_customer_id)
#  index_papers_on_updated_by         (updated_by)
#
# Foreign Keys
#
#  papers_created_by_fk         (created_by => users.id)
#  papers_paper_customer_id_fk  (paper_customer_id => paper_customers.id)
#  papers_updated_by_fk         (updated_by => users.id)
#
