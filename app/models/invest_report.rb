class InvestReport < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :owner, class_name: 'User', foreign_key: 'updated_by'      # constraint with nullify
  belongs_to :investigation, inverse_of: :reports                       # constraint
  has_many :notes, class_name: 'InvestNote', dependent: :destroy        # constraint
  has_many :invest_report_arrests, dependent: :destroy                  # constraint
  has_many :invest_arrests, through: :invest_report_arrests             # constrained through invest_report_arrests
  has_many :arrests, -> { distinct }, through: :invest_arrests          # constrained through invest_report_arrests
  has_many :invest_report_criminals, dependent: :destroy                # constraint
  has_many :criminals, through: :invest_report_criminals, source: :invest_criminal  # constrained through invest_report_criminals
  has_many :invest_report_offenses, dependent: :destroy                 # constraint
  has_many :invest_offenses, through: :invest_report_offenses           # constrained through invest_report_offenses
  has_many :offenses, -> { distinct }, through: :invest_offenses        # constrained through invest_report_offenses
  has_many :invest_report_photos, dependent: :destroy                   # constraint
  has_many :photos, through: :invest_report_photos, source: :invest_photo  # constrained through invest_report_photos
  has_many :invest_report_vehicles, dependent: :destroy                 # constraint
  has_many :vehicles, through: :invest_report_vehicles, source: :invest_vehicle  # constrained through invest_report_vehicles
  has_many :invest_report_victims, dependent: :destroy                  # constraint
  has_many :invest_victims, through: :invest_report_victims             # constrained through invest_report_victims
  has_many :victims, -> { distinct }, through: :invest_victims, source: :person  # constrained through invest_report_victims
  has_many :invest_report_witnesses, dependent: :destroy                # constraint
  has_many :invest_witnesses, through: :invest_report_witnesses         # constrained through invest_report_witnesses
  has_many :witnesses, -> { distinct }, through: :invest_witnesses, source: :person # constrained through invest_report_witnesses
  
  scope :by_reverse_id, -> { order id: :desc }
  
  # filter scopes
  scope :with_invest_report_id, ->(r_id) { where id: r_id }
  scope :with_investigation_id, ->(i_id) { where investigation_id: i_id }
  scope :with_report_date, ->(start,stop) { AppUtils.for_daterange self, :report_datetime, start: start, stop: stop }
  scope :with_details, ->(det) { AppUtils.like self, :details, det }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  
  before_save :check_creator

  validates_presence_of :details
  validates_datetime :report_datetime, on_or_before: :now
  
  ## Class Methods #############################################
  
  def self.desc
    "Reports for Detective Investigations"
  end
  
  def self.base_perms
    :view_investigation
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_invest_report_id(query[:id]) unless query[:id].blank?
    result = result.with_investigation_id(query[:investigation_id]) unless query[:investigation_id].blank?
    result = result.with_report_date(query[:report_date_from],query[:report_date_to]) unless query[:report_date_from].blank? && query[:report_date_to].blank?
    result = result.with_details(query[:details]) unless query[:details].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def linked_arrests=(selected_arrests)
    self.invest_arrest_ids = selected_arrests.reject{|a| a.blank?}.collect{|a| a.to_i}.uniq
  end
  
  def linked_criminals=(selected_criminals)
    self.criminal_ids = selected_criminals.reject{|c| c.blank?}.collect{|c| c.to_i}.uniq
  end
  
  def linked_offenses=(selected_offenses)
    self.invest_offense_ids = selected_offenses.reject{|o| o.blank?}.collect{|o| o.to_i}.uniq
  end
  
  def linked_photos=(selected_photos)
    self.photo_ids = selected_photos.reject{|p| p.blank?}.collect{|p| p.to_i}.uniq
  end
  
  def linked_vehicles=(selected_vehicles)
    self.vehicle_ids = selected_vehicles.reject{|v| v.blank?}.collect{|v| v.to_i}.uniq
  end
  
  def linked_victims=(selected_victims)
    self.invest_victim_ids = selected_victims.reject{|v| v.blank?}.collect{|v| v.to_i}.uniq
  end
  
  def linked_witnesses=(selected_witnesses)
    self.invest_witness_ids = selected_witnesses.reject{|w| w.blank?}.collect{|w| w.to_i}.uniq
  end
end

# == Schema Information
#
# Table name: invest_reports
#
#  id               :integer          not null, primary key
#  investigation_id :integer          not null
#  details          :text             default(""), not null
#  remarks          :text             default(""), not null
#  private          :boolean          default(TRUE), not null
#  created_by       :integer          default(1)
#  updated_by       :integer          default(1)
#  created_at       :datetime
#  updated_at       :datetime
#  owned_by         :integer          default(1)
#  report_datetime  :datetime
#  supplemental     :boolean          default(FALSE), not null
#
# Indexes
#
#  index_invest_reports_on_created_by        (created_by)
#  index_invest_reports_on_investigation_id  (investigation_id)
#  index_invest_reports_on_owned_by          (owned_by)
#  index_invest_reports_on_updated_by        (updated_by)
#
# Foreign Keys
#
#  invest_reports_created_by_fk        (created_by => users.id)
#  invest_reports_investigation_id_fk  (investigation_id => investigations.id)
#  invest_reports_owned_by_fk          (owned_by => users.id)
#  invest_reports_updated_by_fk        (updated_by => users.id)
#
