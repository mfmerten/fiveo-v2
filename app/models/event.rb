class Event < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'
  belongs_to :owner, class_name: 'User', foreign_key: 'owned_by'
  belongs_to :investigation, inverse_of: :events
  
  ## Scopes #############################################
  
  scope :by_date, -> { order :start_date }
  scope :active, -> { AppUtils.for_daterange self, :end_date, start: Date.today, date: true }
  scope :valid_on, ->(event_date) { where("events.start_date <= ? AND events.end_date >= ?", event_date, event_date) }
  scope :valid_within, ->(start,stop) { where("events.start_date <= ? AND events.end_date >= ?", stop, start) }
  
  # filter scopes (see Class Methods)
  scope :with_text, ->(txt) { AppUtils.like self, [:name, :details], txt }
  scope :with_created_by, ->(uid) { where created_by: uid }
  scope :with_event_id, ->(e_id) { where id: e_id }
  scope :with_start_date, ->(start,stop) { AppUtils.for_daterange self, :start_date, start: start, stop: stop, date: true }
  
  ## Callbacks #############################################
  
  before_save :check_creator
  before_validation :fix_end_date

  ## Validations #############################################
  
  validates_presence_of :name
  validates_date :start_date, :end_date

  ## Class Methods #############################################
  
  def self.visible_to(uid)
    table = Event.arel_table
    is_public = table[:private].eq(false)
    owned = table[:owned_by].eq(uid)
    where(is_public.or(owned))
  end
  
  def self.base_perms
    :view_event
  end
  
  def self.desc
    "Calendar events for the front page."
  end

  def self.get_current_events(date,userid)
    return [] unless date.is_a?(Date) && User.exists?(userid)
    events = []
    dates_in_this_month = date.beginning_of_month.upto(date.end_of_month)
    visible_to(userid).valid_within(date.beginning_of_month, date.end_of_month).each do |e|
      dates_event_valid = e.start_date.upto(e.end_date)
      # get overlap between date arrays and add the event on each day in the overlap
      (dates_event_valid.to_a & dates_in_this_month.to_a).each do |d|
        events.push([d, e])
      end
    end
    return events
  end

  # process filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_event_id(query[:id]) unless query[:id].blank?
    result = result.with_start_date(query[:start_from],query[:start_to]) unless query[:start_from].blank? && query[:start_to].blank?
    result = result.with_created_by(query[:created_by]) unless query[:created_by].blank?
    result = result.with_text(query[:text]) unless query[:text].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def expired?
    end_date < Date.today
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def fix_end_date
    unless end_date?
      self.end_date = start_date
    end
    if start_date? && start_date > end_date
      self.start_date, self.end_date = [end_date, start_date]
    end
    true
  end
end

# == Schema Information
#
# Table name: events
#
#  id               :integer          not null, primary key
#  start_date       :date
#  end_date         :date
#  name             :string(255)      default(""), not null
#  details          :text             default(""), not null
#  created_at       :datetime
#  updated_at       :datetime
#  created_by       :integer          default(1)
#  updated_by       :integer          default(1)
#  private          :boolean          default(FALSE), not null
#  investigation_id :integer
#  owned_by         :integer          default(1)
#
# Indexes
#
#  index_events_on_created_by        (created_by)
#  index_events_on_investigation_id  (investigation_id)
#  index_events_on_owned_by          (owned_by)
#  index_events_on_updated_by        (updated_by)
#
# Foreign Keys
#
#  events_created_by_fk        (created_by => users.id)
#  events_investigation_id_fk  (investigation_id => investigations.id)
#  events_owned_by_fk          (owned_by => users.id)
#  events_updated_by_fk        (updated_by => users.id)
#
