class BookingProperty < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :booking, inverse_of: :properties

  ## Validations #############################################
  
  validates_presence_of :description
  validates_numericality_of :quantity, only_integer: true

  ## Class Methods #############################################
  
  def self.desc
    'Inmate Property Records'
  end
  
  ## Instance Methods #############################################
  
  def to_s
    "#{description} (#{quantity})"
  end
end

# == Schema Information
#
# Table name: booking_properties
#
#  id          :integer          not null, primary key
#  booking_id  :integer          not null
#  quantity    :integer          default(1), not null
#  description :string(255)      default(""), not null
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_properties_on_booking_id  (booking_id)
#
# Foreign Keys
#
#  booking_properties_booking_id_fk  (booking_id => bookings.id)
#
