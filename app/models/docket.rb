class Docket < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'
  belongs_to :person, inverse_of: :dockets
  has_many :probations, dependent: :destroy
  has_many :courts, dependent: :destroy
  has_many :sentences, -> { distinct }, through: :courts
  has_many :revocations, dependent: :destroy
  has_many :da_charges, dependent: :destroy
  has_many :charges, -> { distinct }, through: :da_charges, source: :arrest_charge
  has_many :arrests, -> { distinct }, through: :charges
  has_many :citations, -> { distinct }, through: :charges
  has_many :warrants, -> { distinct }, through: :charges
  has_many :da_charge_arrests, -> { distinct }, through: :da_charges, source: :arrest
  has_many :revocation_consecutives, dependent: :destroy
  has_many :sentence_consecutives, dependent: :destroy
  
  ## Attributes #############################################
  
  accepts_nested_attributes_for :da_charges, allow_destroy: true, reject_if: ->(c) { c['charge'].blank? }

  ## Scopes #############################################
  
  scope :not_misc, -> { where misc: false }
  scope :by_docket_no, -> { order :docket_no }
  scope :with_arrests_case_no, ->(cno) { AppUtils.for_lower self, :case_no, cno, association: :arrest }
  scope :with_da_charge_arrests_case_no, ->(cno) { AppUtils.for_lower self, :case_no, cno, association: :da_charge_arrests }
  
  # filter scopes
  scope :with_docket_no, ->(dno) { AppUtils.like self, :docket_no, dno }
  scope :with_person_id, ->(pid) { where person_id: pid }
  scope :with_judge, ->(jdg) { AppUtils.like self, :judge, jdg, association: :courts }
  scope :with_docket_type, ->(dtp) { AppUtils.like self, :docket_type, dtp, association: :courts }
  scope :with_case_no, ->(caseno) { find_all_by_case_no caseno }
  scope :with_person_name, ->(name) { AppUtils.for_person self, name, association: :person }  
  scope :with_court_date, ->(cdate) { AppUtils.for_date self, :court_date, c_date, association: :courts, date: true }
  
  ## Callbacks #############################################
  
  before_save :check_creator
  before_validation :fix_docket_no
  after_save :check_misc

  ## Validations #############################################
  
  validates_associated :da_charges
  validates_presence_of :docket_no
  validates_uniqueness_of :docket_no, scope: :person_id, message: 'already exists for this person'
  validates_person :person_id

  ## Constants #############################################
  
  ConsecutiveType = ['Any Current', 'Other Dockets', 'This Docket'].freeze

  CourtCostCategory = ['Felony', 'Misdemeanor', 'Traffic', 'DWI', 'Wildlife', 'Other'].freeze

  PleaType = ['Guilty', 'Not Guilty', 'No Contest'].freeze

  TrialResult = ["Pled Guilty", "Found Guilty", "Not Guilty", "Dismissed", "No Contest"].freeze
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_court
  end
  
  def self.desc
    'Court Docket Table'
  end
  
  def self.dockets_for_select(person_id)
    return [] unless person_id.is_a?(Integer)
    where(person_id: person_id).all.collect{|d| [d.docket_no, d.id]}.unshift(["---",nil])
  end
  
  def self.find_all_by_case_no(caseno)
    # gather docket ids from :arrests association
    ids = with_arrests_case_no(caseno).pluck(:id)
    # combine with docket ids from :da_charge_arrests association
    ids = ids.concat(with_da_charge_arrests_case_no(caseno).pluck(:id)).uniq
    # return relation with all dockets from ids array
    where(id: ids)
  end
  
  def self.process_dockets
    where('processed IS FALSE').all.each{|d| d.process_docket}
  end
  
  # process filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_docket_no(query[:docket_no]) unless query[:docket_no].blank?
    result = result.with_person_id(query[:person_id]) unless query[:person_id].blank?
    result = result.with_person_name(query[:name]) unless query[:name].blank?
    result = result.with_court_date(query[:court_date]) unless query[:court_date].blank?
    result = result.with_judge(query[:judge]) unless query[:judge].blank?
    result = result.with_docket_type(query[:docket_type]) unless query[:docket_type].blank?
    result = result.with_case_no(query[:case_no]) unless query[:case_no].blank?
    result
  end
      
  ## Instance Methods #############################################
  
  def bonds
    arrests.collect{|a| a.bonds}.flatten.compact.uniq
  end
  
  def case_numbers
    arrests.collect{|a| a.case_no}.compact.uniq
  end
  
  def charge_summary(skip_dropped = false)
    if skip_dropped == true
      text = da_charges.reject{|c| c.dropped_by_da?}.join(", ")
    else
      text = da_charges.join(", ")
    end
    return text
  end
  
  def linked_charges=(selected_charges)
    selected_charges.each do |c|
      if chg = Charge.find_by_id(c)
        self.da_charges.create(charge_id: chg.id, docket_id: id, count: chg.count, charge: chg.charge, processed: false, dropped_by_da: false)
      end
    end
  end
  
  def next_courts
    return [] if courts.empty?
    cs = Court.order('court_date').where(['docket_id = ? AND court_date >= ?', id, Date.today]).all
    return [] if cs.empty?
    next_date = cs.first
    cs.reject{|c| c.court_date != next_date}
  end
  
  def original_sentence_date
    # return earliest date from all sentences
    return nil if sentences.empty?
    sent_date = Date.today
    sentences.each do |s|
      if !s.sentence_date.nil? && s.sentence_date < sent_date
        sent_date = s.sentence_date
      end
    end
    return sent_date
  end
  
  def process_docket
    return true if processed?
    # if the docket is miscellaneous, none of the charges will ever be
    # processed, they are merely for informational purposes.  Same goes
    # for all of the courts.
    if misc?
      da_charges.each do |c|
        c.update_attribute(:processed, true)
      end
      courts.each do |c|
        c.update_attribute(:processed, true)
      end
      self.update_attribute(:processed, true)
    end
    revocations.reject(&:processed?).each{|r| r.save(validate: false)}
    courts.reject(&:processed?).each{|c| c.process_court}
    if courts.reject(&:processed?).empty? && revocations.reject(&:processed?).empty?
      self.update_attribute(:processed, :true)
    end
  end
  
  def sentence_summary
    return nil if sentences.empty?
    txt = []
    sentences.each do |s|
      unless ['Not Guilty','Dismissed'].include?(s.result)
        txt.push("#{s.conviction}(#{s.conviction_count})")
      end
    end
    return txt.join(', ')
  end
  
  def time_served_in_hours
    return 0 if arrests.empty?
    Arrest.hours_served(arrests.collect{|a| a.id})
  end
  
  def total_sentence_length
    total_hours = 0
    return 0 if sentences.empty?
    consecutives = []
    concurrents = []
    sentences.each do |s|
      temp = s.total_hours
      return -1 if temp < 0
      if s.consecutive_type.blank?
        concurrents.push(temp)
      else
        consecutives.push(temp)
      end
    end
    total_hours += concurrents.sort{|a,b| b <=> a}.first
    consecutives.each{|c| total_hours += c}
    return total_hours
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def fix_docket_no
    if docket_no?
      self.docket_no = docket_no.clone.strip.sub(/^0*/,'')
    else
      self.docket_no = Time.now.strftime("%Y%m%d%H%M%S")
    end
    true
  end
  
  def check_misc
    if misc?
      da_charges.each do |c|
        unless c.info_only?
          c.update_attribute(:info_only, true)
          c.update_attribute(:processed, true)
        end
      end
      unless processed?
        self.update_attribute(:processed, true)
      end
    end
  end
end

# == Schema Information
#
# Table name: dockets
#
#  id         :integer          not null, primary key
#  docket_no  :string(255)      default(""), not null
#  person_id  :integer          not null
#  created_by :integer          default(1)
#  updated_by :integer          default(1)
#  created_at :datetime
#  updated_at :datetime
#  processed  :boolean          default(FALSE), not null
#  notes      :text             default(""), not null
#  remarks    :text             default(""), not null
#  misc       :boolean          default(FALSE), not null
#
# Indexes
#
#  index_dockets_on_created_by  (created_by)
#  index_dockets_on_docket_no   (docket_no)
#  index_dockets_on_person_id   (person_id)
#  index_dockets_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  dockets_created_by_fk  (created_by => users.id)
#  dockets_person_id_fk   (person_id => people.id)
#  dockets_updated_by_fk  (updated_by => users.id)
#
