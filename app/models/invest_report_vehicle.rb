class InvestReportVehicle < ActiveRecord::Base
  belongs_to :invest_report, inverse_of: :invest_report_vehicles           # constraint
  belongs_to :invest_vehicle, inverse_of: :invest_report_vehicles          # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestReport Vehicles'
  end
end

# == Schema Information
#
# Table name: invest_report_vehicles
#
#  id                :integer          not null, primary key
#  invest_report_id  :integer          not null
#  invest_vehicle_id :integer          not null
#  created_at        :datetime
#  updated_at        :datetime
#
# Indexes
#
#  index_invest_report_vehicles_on_invest_report_id   (invest_report_id)
#  index_invest_report_vehicles_on_invest_vehicle_id  (invest_vehicle_id)
#
# Foreign Keys
#
#  invest_report_vehicles_invest_report_id_fk   (invest_report_id => invest_reports.id)
#  invest_report_vehicles_invest_vehicle_id_fk  (invest_vehicle_id => invest_vehicles.id)
#
