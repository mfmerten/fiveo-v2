class InvestReportWitness < ActiveRecord::Base
  belongs_to :invest_report, inverse_of: :invest_report_witnesses          # constraint
  belongs_to :invest_witness, inverse_of: :invest_report_witnesses         # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'Investigation Report Witnesses'
  end
end

# == Schema Information
#
# Table name: invest_report_witnesses
#
#  id                :integer          not null, primary key
#  invest_report_id  :integer          not null
#  invest_witness_id :integer          not null
#  created_at        :datetime
#  updated_at        :datetime
#
# Indexes
#
#  index_invest_report_witnesses_on_invest_report_id   (invest_report_id)
#  index_invest_report_witnesses_on_invest_witness_id  (invest_witness_id)
#
# Foreign Keys
#
#  invest_report_witnesses_invest_report_id_fk   (invest_report_id => invest_reports.id)
#  invest_report_witnesses_invest_witness_id_fk  (invest_witness_id => invest_witnesses.id)
#
