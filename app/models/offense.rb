class Offense < ActiveRecord::Base
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :call, inverse_of: :offenses                               # constraint
  has_many :offense_forbids, dependent: :destroy                        # constraint
  has_many :forbids, through: :offense_forbids                          # constrained through offense_forbids
  has_many :offense_photos, dependent: :destroy                         # constraint
  has_many :invest_offenses, dependent: :destroy                        # constraint
  has_many :investigation, through: :invest_offenses                    # constrained through invest_offenses
  has_many :offense_evidences, dependent: :destroy                      # constraint
  has_many :evidences, through: :offense_evidences                      # constrained through offense_evidences
  has_many :offense_arrests, dependent: :destroy                        # constraint
  has_many :arrests, through: :offense_arrests                          # constrained through offense_arrests
  has_many :offense_victims, dependent: :destroy                        # constraint
  has_many :victims, through: :offense_victims, source: :person         # constrained through offense_victims
  has_many :offense_witnesses, dependent: :destroy                      # constraint
  has_many :witnesses, through: :offense_witnesses, source: :person     # constrained through offense_witnesses
  has_many :offense_suspects, dependent: :destroy                       # constraint
  has_many :suspects, through: :offense_suspects, source: :person       # constrained through offense_suspects
  has_many :offense_citations, dependent: :destroy                      # constraint
  has_many :citations, through: :offense_citations                      # constrained through offense_citations
  has_many :wreckers, class_name: 'OffenseWrecker', dependent: :destroy # constraint
  
  accepts_nested_attributes_for :offense_evidences, allow_destroy: true, reject_if: ->(e) { e['evidence_id'].blank? || !Evidence.exists?(e['evidence_id']) }
  accepts_nested_attributes_for :offense_arrests, allow_destroy: true, reject_if: ->(e) { e['arrest_id'].blank? || !Arrest.exists?(e['arrest_id']) }
  accepts_nested_attributes_for :offense_victims, allow_destroy: true, reject_if: ->(p) { p['person_id'].blank? || !Person.exists?(p['person_id']) }
  accepts_nested_attributes_for :offense_witnesses, allow_destroy: true, reject_if: ->(p) { p['person_id'].blank? || !Person.exists?(p['person_id']) }
  accepts_nested_attributes_for :offense_suspects, allow_destroy: true, reject_if: ->(p) { p['person_id'].blank? || !Person.exists?(p['person_id']) }
  accepts_nested_attributes_for :offense_citations, allow_destroy: true, reject_if: ->(c) { c['citation_id'].blank? || !Citation.exists?(c['citation_id']) }
  accepts_nested_attributes_for :wreckers, allow_destroy: true, reject_if: ->(w) { w['name'].blank? }
  
  scope :by_date, -> { order offense_datetime: :desc }
  
  # filter scopes
  scope :with_offense_id, ->(o_id) { where id: o_id }
  scope :with_call_id, ->(c_id) { where call_id: c_id }
  scope :with_case_no, ->(c_no) { AppUtils.for_lower self, :case_no, c_no, association: :call }
  scope :with_offense_date, ->(start,stop) { AppUtils.for_daterange self, :offense_datetime, start: start, stop: stop }
  scope :with_location, ->(loc) { AppUtils.like self, :location, loc }
  scope :with_details, ->(det) { AppUtils.like self, :details, det }
  scope :with_wrecker_used, ->(wrk) { AppUtils.like self, :name, wrk, association: :wreckers }
  scope :with_wrecker_by_request, ->(req) { AppUtils.for_bool self, :by_request, req, association: :wreckers }
  scope :with_vehicle_vin, ->(v) { AppUtils.like self, :vin, v, association: :wreckers }
  scope :with_vehicle_lic, ->(t) { AppUtils.like self, :tag, t, association: :wreckers }
  scope :with_pictures, ->(p) { AppUtils.for_bool self, :pictures, p }
  scope :with_restitution_form, ->(r) { AppUtils.for_bool self, :restitution_form, r }
  scope :with_victim_notification, ->(v) { AppUtils.for_bool self, :victim_notification, v }
  scope :with_perm_to_search, ->(p) { AppUtils.for_bool self, :perm_to_search, p }
  scope :with_misd_summons, ->(m) { AppUtils.for_bool self, :misd_summons, m }
  scope :with_fortyeight_hour, ->(f) { AppUtils.for_bool self, :fortyeight_hour, f }
  scope :with_medical_release, ->(m) { AppUtils.for_bool self, :medical_release, m }
  scope :with_other_documents, ->(o) { AppUtils.like self, :other_documents, o }
  scope :with_officer, ->(name,unit,badge) { AppUtils.for_officer self, :officer, name, unit, badge }
  scope :with_photo_desc, ->(des) { AppUtils.like self, :description, des, association: :offense_photos }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  
  attr_accessor :officer_id
  # the following attributes are for batch photo uploads
  attr_accessor :photo_date, :photo_desc, :photo_taken_by, :photo_taken_by_unit
  attr_accessor :photo_taken_by_badge, :photo_taken_by_id, :photo_loc, :photo_batch
  
  before_save :check_creator
  before_validation :lookup_officers, :check_photo_date
  after_update :save_photos
  after_create :send_new_messages

  validates_associated :wreckers
  validates_presence_of :officer, :location, :details, unless: :photo_batch
  validates_presence_of :photo_desc, :photo_taken_by, :photo_loc, :photo_date, if: :photo_batch
  validates_datetime :offense_datetime, unless: :photo_batch
  validates_call :call_id, unless: :photo_batch

  ## Class Methods #############################################
  
  def self.desc
    "Offense Reports"
  end
  
  def self.base_perms
    :view_offense
  end
  
  def self.find_all_by_case_no(case_no)
    joins(:call).where(calls: {case_no: case_no}).all
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_offense_id(query[:id]) unless query[:id].blank?
    result = result.with_call_id(query[:call_id]) unless query[:call_id].blank?
    result = result.with_case_no(query[:case_no]) unless query[:case_no].blank?
    result = result.with_offense_date(query[:offense_from],query[:offense_to]) unless query[:office_from].blank? && query[:offense_to].blank?
    result = result.with_location(query[:location]) unless query[:location].blank?
    result = result.wtih_details(query[:details]) unless query[:details].blank?
    result = result.with_wrecker_used(query[:wrecker_used]) unless query[:wrecker_used].blank?
    result = result.with_wrecker_by_request(query[:wrecker_by_request]) unless query[:wrecker_by_request].blank?
    result = result.with_vehicle_vin(query[:vehicle_vin]) unless query[:vehicle_vin].blank?
    result = result.with_vehicle_lic(query[:vehicle_lic]) unless query[:vehicle_lic].blank?
    result = result.with_pictures(query[:pictures]) unless query[:pictures].blank?
    result = result.with_restitution_form(query[:restitution_form]) unless query[:restitution_form].blank?
    result = result.with_victim_notification(query[:victim_notification]) unless query[:victim_notification].blank?
    result = result.with_perm_to_search(query[:perm_to_search]) unless query[:perm_to_search].blank?
    result = result.with_misd_summons(query[:misd_summons]) unless query[:misd_summons].blank?
    result = result.wtih_fortyeight_hour(query[:fortyeight_hour]) unless query[:fortyeight_hour].blank?
    result = result.with_medical_release(query[:medical_release]) unless query[:medical_release].blank?
    result = result.with_other_documents(query[:other_documents]) unless query[:other_documents].blank?
    result = result.with_officer(query[:officer], query[:officer_unit], query[:officer_badge]) unless query[:officer].blank? && query[:officer_unit].blank? && query[:officer_badge].blank?
    result = result.with_photo_desc(query[:photo_desc]) unless query[:photo_desc].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def case_no
    call.nil? ? '' : call.case_no
  end
  
  def case_no?
    return false if call.nil?
    call.case_no?
  end
  
  def new_photos=(photo_attributes)
    photo_attributes.each do |attributes|
      unless attributes[:crime_scene_photo].blank?
        attributes.update({
          offense_id: id,
          case_no: case_no,
          description: photo_desc,
          date_taken: AppUtils.valid_date(photo_date),
          location_taken: photo_loc,
          taken_by: photo_taken_by,
          taken_by_unit: photo_taken_by_unit,
          taken_by_badge: photo_taken_by_badge,
          taken_by_id: photo_taken_by_id,
          created_by: updated_by,
          updated_by: updated_by
        })
        offense_photos.build(attributes)
      end
    end
  end
  
  def involved_parties(search=false)
    txt = []
    victims.each do |v|
      txt.push(["Victim:", v.sort_name])
    end
    witnesses.each do |w|
      txt.push(["Witness:", w.sort_name])
    end
    suspects.each do |s|
      txt.push(["Suspect:", s.sort_name])
    end
    if search == true
      if txt.empty?
        return nil
      else
        return txt
      end
    else
      if txt.empty?
        return ""
      else
        return txt.collect{|a| a[0] + ' ' + a[1]}.join(",  ")
      end
    end
  end

  private
  
  ## Private Instance Methods #############################################
  
  def check_photo_date
    if photo_batch
      if photo_date.blank?
        errors.add(:photo_date, 'is required!')
        return false
      end
      tmp = AppUtils.valid_date(photo_date)
      if tmp.nil?
        errors.add(:photo_date, 'is not a valid date!')
        return false
      end
    end
    true
  end
  
  def save_photos
    offense_photos.each{|p| p.save(validate: false)}
  end
  
  def send_new_messages
    receivers = User.where(notify_offense_new: true).all
    receivers.each do |r|
      next unless r.can?(self.class.base_perms)
      txt = AutomatedMessagesController.new.notify(self, 'new')
      Message.create(receiver_id: r.id, sender_id: updated_by, subject: "New Offense Report (Case: #{case_no})", body: txt)
    end
  end

  def lookup_officers
    if photo_batch
      unless photo_taken_by_id.blank?
        if ofc = Contact.find_by_id(photo_taken_by_id)
          self.photo_taken_by = ofc.sort_name
          self.photo_taken_by_badge = ofc.badge_no
          self.photo_taken_by_unit = ofc.unit
        end
        self.photo_taken_by_id = nil
      end
    else
      unless officer_id.blank?
        if ofc = Contact.find_by_id(officer_id)
          self.officer = ofc.sort_name
          self.officer_badge = ofc.badge_no
          self.officer_unit = ofc.unit
        end
        self.officer_id = nil
      end
    end
    true
  end
end

# == Schema Information
#
# Table name: offenses
#
#  id                  :integer          not null, primary key
#  call_id             :integer
#  location            :string(255)      default(""), not null
#  details             :text             default(""), not null
#  pictures            :boolean          default(FALSE), not null
#  restitution_form    :boolean          default(FALSE), not null
#  victim_notification :boolean          default(FALSE), not null
#  perm_to_search      :boolean          default(FALSE), not null
#  misd_summons        :boolean          default(FALSE), not null
#  fortyeight_hour     :boolean          default(FALSE), not null
#  medical_release     :boolean          default(FALSE), not null
#  other_documents     :text             default(""), not null
#  officer             :string(255)      default(""), not null
#  remarks             :text             default(""), not null
#  created_at          :datetime
#  updated_at          :datetime
#  created_by          :integer          default(1)
#  updated_by          :integer          default(1)
#  officer_unit        :string(255)      default(""), not null
#  officer_badge       :string(255)      default(""), not null
#  legacy              :boolean          default(FALSE), not null
#  offense_datetime    :datetime
#
# Indexes
#
#  index_offenses_on_call_id     (call_id)
#  index_offenses_on_created_by  (created_by)
#  index_offenses_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  offenses_call_id_fk     (call_id => calls.id)
#  offenses_created_by_fk  (created_by => users.id)
#  offenses_updated_by_fk  (updated_by => users.id)
#
