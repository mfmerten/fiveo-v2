class InvestArrest < ActiveRecord::Base
  belongs_to :arrest, inverse_of: :invest_arrests                          # constraint
  belongs_to :investigation, inverse_of: :invest_arrests                   # constraint
  has_many :invest_crime_arrests, dependent: :destroy                      # constraint
  has_many :crimes, through: :invest_crime_arrests, source: :invest_crime  # constrained through invest_crime_arrests
  has_many :invest_criminal_arrests, dependent: :destroy                   # constraint
  has_many :criminals, through: :invest_criminal_arrests, source: :invest_criminal  # constrained through invest_criminal_arrests
  has_many :invest_report_arrests, dependent: :destroy                     # constraint
  has_many :reports, through: :invest_report_arrests, source: :invest_report  # constrained through invest_report_arrests
  
  ## Class Methods #############################################
  
  def self.desc
    "Investigation -> Arrest links"
  end
end

# == Schema Information
#
# Table name: invest_arrests
#
#  id               :integer          not null, primary key
#  arrest_id        :integer          not null
#  investigation_id :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_invest_arrests_on_arrest_id         (arrest_id)
#  index_invest_arrests_on_investigation_id  (investigation_id)
#
# Foreign Keys
#
#  invest_arrests_arrest_id_fk         (arrest_id => arrests.id)
#  invest_arrests_investigation_id_fk  (investigation_id => investigations.id)
#
