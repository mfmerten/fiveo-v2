class Activity < ActiveRecord::Base
  
  ## Associations ###################################################
  
  belongs_to :user, inverse_of: :activities
  
  ## Scopes ###################################################
  
  scope :by_reverse_id, -> { order id: :desc }
  
  # filter scopes (see Class Methods below)
  scope :with_activity_id, ->(activity_id) { where id: activity_id }
  scope :with_username, ->(uname) { AppUtils.like self, :username, uname }
  scope :with_section, ->(sect) { AppUtils.for_lower self, :section, sect }
  scope :with_description, ->(desc) { AppUtils.like self, :description, desc }
  scope :with_created, ->(start,stop) { AppUtils.for_daterange self, :created_at, start: start, stop: stop }
  
  ## Class Methods ###################################################
  
  def self.add(section=nil,description=nil,uid=nil,login=nil)
    username = "#{login || 'Unknown'} (#{uid || 'Unknown'})"
    create(section: section, description: description, user_id: uid, username: username)
    # note: using logger.warn here to make them stand out from info messages
    logger.warn("User Activity: #{Time.now.to_s(:us)} #{section}: #{uid} => #{description}")
  end

  def self.base_perms
    :admin
  end

  def self.desc
    "User Activity Log"
  end
  
  def self.purge_old_records
    unless SiteConfig.activity_retention.blank? || SiteConfig.activity_retention == 0
      extent = SiteConfig.activity_retention.days.ago
      delete_all(["created_at <= ?", extent])
    end
  end
  
  # process the filter
  def self.process_query(query)
    return none unless query.is_a?(Hash)
    result = all
    result = result.with_activity_id(query[:id]) unless query[:id].blank?
    result = result.with_created(query[:created_from], query[:created_to]) unless query[:created_from].blank? && query[:created_to].blank?
    result = result.with_username(query[:username]) unless query[:username].blank?
    result = result.with_section(query[:section]) unless query[:section].blank?
    result = result.with_description(query[:description]) unless query[:description].blank?
    result
  end
end

# == Schema Information
#
# Table name: activities
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  section     :string(255)      default(""), not null
#  description :string(255)      default(""), not null
#  created_at  :datetime
#  updated_at  :datetime
#  username    :string(255)      default(""), not null
#
# Indexes
#
#  index_activities_on_user_id  (user_id)
#
# Foreign Keys
#
#  activities_user_id_fk  (user_id => users.id)
#
