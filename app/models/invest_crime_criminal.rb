class InvestCrimeCriminal < ActiveRecord::Base
  belongs_to :invest_crime, inverse_of: :invest_crime_criminals            # constraint
  belongs_to :invest_criminal, inverse_of: :invest_crime_criminals         # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestCrime Criminals'
  end
end

# == Schema Information
#
# Table name: invest_crime_criminals
#
#  id                 :integer          not null, primary key
#  invest_crime_id    :integer          not null
#  invest_criminal_id :integer          not null
#  created_at         :datetime
#  updated_at         :datetime
#
# Indexes
#
#  index_invest_crime_criminals_on_invest_crime_id     (invest_crime_id)
#  index_invest_crime_criminals_on_invest_criminal_id  (invest_criminal_id)
#
# Foreign Keys
#
#  invest_crime_criminals_invest_crime_id_fk     (invest_crime_id => invest_crimes.id)
#  invest_crime_criminals_invest_criminal_id_fk  (invest_criminal_id => invest_criminals.id)
#
