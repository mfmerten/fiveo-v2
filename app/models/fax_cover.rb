class FaxCover < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify

  scope :by_name, -> { order :name }
  
  before_save :check_creator

  validates_presence_of :name, :from_name
  validates_uniqueness_of :name
  validates_phone :from_fax, :from_phone

  ## Class Methods #############################################
  
  def self.desc
    "Custom Fax Cover Pages"
  end
end

# == Schema Information
#
# Table name: fax_covers
#
#  id             :integer          not null, primary key
#  name           :string(255)      default(""), not null
#  from_name      :string(255)      default(""), not null
#  from_fax       :string(255)      default(""), not null
#  from_phone     :string(255)      default(""), not null
#  include_notice :boolean          default(TRUE), not null
#  notice_title   :string(255)      default(""), not null
#  notice_body    :text             default(""), not null
#  created_by     :integer          default(1)
#  updated_by     :integer          default(1)
#  created_at     :datetime
#  updated_at     :datetime
#
# Indexes
#
#  index_fax_covers_on_created_by  (created_by)
#  index_fax_covers_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  fax_covers_created_by_fk  (created_by => users.id)
#  fax_covers_updated_by_fk  (updated_by => users.id)
#
