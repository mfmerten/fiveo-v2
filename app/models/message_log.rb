class MessageLog < ActiveRecord::Base
  belongs_to :sender, class_name: 'User', foreign_key: 'sender_id', inverse_of: :message_logs_as_sender  # constraint
  belongs_to :receiver, class_name: 'User', foreign_key: 'receiver_id', inverse_of: :message_logs_as_receiver  # constraint
  
  scope :by_reverse_id, -> { order id: :desc }
  
  # filter scopes
  scope :with_message_log_id, ->(m_id) { where id: m_id }
  scope :with_created, ->(start,stop) { AppUtils.for_daterange self, :created_at, start: start, stop: stop }
  scope :with_user_id, ->(u_id) { AppUtils.for_lower self, [:receiver_id, :sender_id], u_id }
  scope :with_subject, ->(sub) { AppUtils.like self, :subject, sub }
  scope :with_body, ->(bdy) { AppUtils.like self, :body, bdy }
  
  # no validations, these messages are recorded as-is

  ## Class Methods #############################################
  
  def self.base_perms
    :admin
  end
  
  def self.desc
    'Message Logs'
  end

  def self.purge
    if SiteConfig.message_log_retention.blank?
      ret_policy = 0
    else
      ret_policy = SiteConfig.message_log_retention
    end
    unless ret_policy == 0
      exp_datetime = ret_policy.days.ago
      delete_all(["created_at < ?",exp_datetime])
    end
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_message_log_id(query[:id]) unless query[:id].blank?
    result = result.with_created(query[:created_from],query[:created_to]) unless query[:created_from].blank? && query[:created_to].blank?
    result = result.with_user_id(query[:user_id]) unless query[:user_id].blank?
    result = result.with_subject(query[:subject]) unless query[:subject].blank?
    result = result.with_body(query[:body]) unless query[:body].blank?
    result
  end
end

# == Schema Information
#
# Table name: message_logs
#
#  id               :integer          not null, primary key
#  receiver_deleted :boolean          default(FALSE), not null
#  receiver_purged  :boolean          default(FALSE), not null
#  sender_deleted   :boolean          default(FALSE), not null
#  sender_purged    :boolean          default(FALSE), not null
#  read_at          :datetime
#  receiver_id      :integer          not null
#  sender_id        :integer          not null
#  subject          :string(255)      default(""), not null
#  body             :text             default(""), not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_message_logs_on_receiver_id  (receiver_id)
#  index_message_logs_on_sender_id    (sender_id)
#
# Foreign Keys
#
#  message_logs_receiver_id_fk  (receiver_id => users.id)
#  message_logs_sender_id_fk    (sender_id => users.id)
#
