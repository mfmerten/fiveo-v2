class Booking < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'
  belongs_to :jail, inverse_of: :bookings
  belongs_to :person, inverse_of: :bookings
  has_many :holds, dependent: :destroy
  has_many :temp_release_holds, -> { where(hold_type: 'Temporary Release') }, class_name: 'Hold'
  has_many :doc_records, -> { where(holds: {cleared_datetime: nil}) }, through: :holds, source: :doc_record
  has_many :docs, dependent: :destroy
  has_many :arrests, through: :holds
  has_many :medicals, dependent: :destroy
  has_many :visitors, dependent: :destroy
  has_many :absent_prisoners, dependent: :destroy
  has_many :absents, -> { distinct }, through: :absent_prisoners
  has_many :transfers, dependent: :destroy
  has_many :booking_logs, dependent: :destroy
  has_many :properties, class_name: 'BookingProperty', dependent: :destroy

  ## Attributes #############################################

  accepts_nested_attributes_for :properties, allow_destroy: true, reject_if: ->(p) { p['description'].blank? }
  attr_accessor :booking_officer_id, :release_officer_id

  ## Scopes #############################################
  
  scope :by_id, -> { order :id }
  scope :active, -> { where release_datetime: nil }
  scope :active_on, ->(date) { where('bookings.booking_datetime <= ? and (bookings.release_datetime > ? OR bookings.release_datetime IS NULL)',date.to_time.end_of_day,date.to_time)}
  scope :in_jail_between, ->(start,stop) { where('bookings.booking_datetime <= ? AND (bookings.release_datetime > ? OR bookings.release_datetime IS NULL)',stop,start) }
  scope :by_name, -> { includes(:person).order('people.family_name, people.given_name, bookings.booking_datetime') }
  scope :belonging_to_jail, ->(j) { where jail_id: j }
  scope :with_mugshot, -> { where("people.front_mugshot_file_name != '' AND people.front_mugshot_file_name IS NOT NULL").references(:person) }
  
  # filter scopes (See Class Methods below)
  scope :with_booking_id, ->(b_id) { where id: b_id }
  scope :with_person_id, ->(p_id) { where person_id: p_id }
  scope :with_cell, ->(c) { AppUtils.like self, :cell, c }
  scope :with_afis, ->(af) { AppUtils.for_bool self, :afis, af }
  scope :with_released_because, ->(because) { AppUtils.like self, :released_because, because }
  scope :with_person_name, ->(name) { AppUtils.for_person self, name, association: :person }
  scope :with_booking_officer, ->(name,unit,badge) { AppUtils.for_officer self, :booking_officer, name, unit, badge }
  scope :with_release_officer, ->(name,unit,badge) { AppUtils.for_officer self, :release_officer, name, unit, badge }
  scope :with_sched_release_date, ->(start,stop) { AppUtils.for_daterange self, :sched_release_date, start: start, stop: stop, date: true }
  scope :with_release_date, ->(start,stop) { AppUtils.for_daterange self, :release_datetime, start: start, stop: stop }
  
  ## Callbacks #############################################
  
  before_save :check_creator
  before_validation :lookup_officers, :check_person
  after_save :check_timers, :process_bond_recalls
  after_update :send_release_messages
  after_create :send_new_messages
  before_destroy :check_destroyable

  ## Validations #############################################
  
  validates_associated :properties
  validates_presence_of :booking_officer, :cell
  validates_datetime :booking_datetime
  validates_date :sched_release_date, allow_blank: true
  validates_datetime :release_datetime, allow_blank: true
  with_options if: :release_datetime? do |released|
    released.validates :released_because, presence: true
    released.validates :release_officer, presence: true
  end
  validates_phone :phone_called, allow_blank: true
  validates_numericality_of :cash_at_booking, greater_than_or_equal_to: 0, less_than: 10 ** 10
  validates_person :person_id
  validates_jail :jail_id

  ## Class Methods #############################################
  
  def self.base_perms
    :view_booking
  end
  
  def self.desc
    'Booking Records'
  end
  
  def self.release_reasons
    [
      'Fines Paid', "Bonded", "Time Served", "Transferred", "Escaped",
      "Deceased", "Released W/O Charges", "48 Hour Expired",
      "Released By Judge", "Paroled", "Pardoned", "Executed"
    ]
  end
    
  def self.update_sentences_by_hour
    Activity.create(section: "Admin", description: "Executing jail time served update scan.", user_id: 1)
    # loop through all current booking records
    active.all.each do |book|
      book.holds.active.sentenced.reject{|h| h.active_blockers?}.each do |hold|
        hold.update_hour_counters
      end
    end
  end
  
  def self.update_cashless_systems(test = false)
    # skip if csi is not enabled
    return nil unless SiteConfig.enable_cashless_systems
    require 'csv'
    inmate_list = []
    active.all.each do |b|
      inmate_resident_id = b.id
      inmate_personal_id = b.person.id
      inmate_lastname = b.person.family_name
      parts = b.person.given_name.split(/\s+/,2)
      inmate_firstname = parts[0]
      inmate_middlename = parts[1]
      classification = b.billing_status
      case classification
      when "Parish"
        inmate_class_id = "PAR"
      when "DOC Billable" , "DOC"
        inmate_class_id = "DOC"
      when "City"
        inmate_class_id = "CTY"
      else
        inmate_class_id = "UNK"
      end
      inmate_unit_id = b.jail.acronym
      inmate_bldg_id = nil
      inmate_pod_id = nil
      inmate_section_id = nil
      inmate_bay_id = nil
      inmate_bed = b.cell
      # optional data
      inmate_ssn = nil
      if SiteConfig.csi_provide_ssn
        inmate_ssn = b.person.ssn
      end
      inmate_dob = nil
      if SiteConfig.csi_provide_dob && b.person.date_of_birth?
        inmate_dob = b.person.date_of_birth.to_s(:us)
      end
      inmate_race_id = nil
      if SiteConfig.csi_provide_race && b.person.race?
        inmate_race_id = b.person.race_abbreviation
      end
      inmate_gender = nil
      if SiteConfig.csi_provide_sex
        inmate_gender = b.person.sex_name(true)
      end
      inmate_address = nil
      if SiteConfig.csi_provide_addr
        inmate_address = b.person.mailing_address
      end
      inmate_list.push([
        inmate_resident_id, inmate_personal_id, inmate_lastname, inmate_firstname,
        inmate_middlename, inmate_class_id, inmate_ssn, inmate_dob, inmate_race_id,
        inmate_gender, inmate_unit_id, inmate_bldg_id, inmate_pod_id, inmate_section_id,
        inmate_bay_id, inmate_bed, inmate_address
      ])
    end
    # add the headers
    inmate_list.unshift([
      "ResidentID","PersonalId","LastName","FirstName","MiddleName","ClassId",
      "Ssn","Dob","RaceId","Gender","UnitId","BldgId","PodId","SectionId",
      "BayId","Bed","Address"
    ])
    # Write diagnostic inmate list if 'test' is true
    if test
      if SiteConfig.csi_ftp_filename.blank?
        fname = "/tmp" + SiteConfig.agency_acronym.downcase + ".list"
      else
        fname = "/tmp" + SiteConfig.csi_ftp_filename + ".list"
      end
      outfile = File.open(fname,'w')
      outfile.puts(inmate_list.inspect)
      outfile.close
      csvstring = ''
      CSV::Writer.generate(csvstring) do |csv|
        inmate_list.each do |l|
          csv << l
        end
      end
    else
      # Write actual ftp file
      if SiteConfig.csi_ftp_filename.blank?
        fname = "/tmp/" + SiteConfig.agency_acronym.downcase + ".txt"
      else
        fname = "/tmp/" + SiteConfig.csi_ftp_filename + ".txt"
      end
      outfile = File.open(fname,'w')
      outfile.puts(csvstring)
      outfile.close
      # FTP the resulting file to CSI
      # only do this if ftp address, username and password are specified in
      # site configuration
      unless SiteConfig.csi_ftp_url.blank? || SiteConfig.csi_ftp_user.blank? || SiteConfig.csi_ftp_password.blank?
        require 'net/ftp'
        ftp = Net::FTP.new
        ftp.connect(SiteConfig.csi_ftp_url,21)
        ftp.login(SiteConfig.csi_ftp_user, SiteConfig.csi_ftp_password)
        ftp.putbinaryfile(fname)
        ftp.close
      end
    end
  end
  
  def self.send_problem_notifications(frequency=nil)
    return true if frequency.nil? || !Jail::NotifyFrequency.include?(frequency)
    receivers = User.where(notify_booking_problems: true)
    facilities = Jail.where(notify_frequency: frequency).all
    facilities.each do |facility|
      problems = facility.problems
      next if problems.empty?
      receivers.each do |r|
        next unless r.can?(facility.view_permission, self.base_perms)
        txt = AutomatedMessagesController.new.notify(facility,'problems')
        Message.create(receiver_id: r.id, sender_id: r.id, subject: "#{facility.acronym} Booking Problems", body: txt)
      end
    end
  end
  
  # process the query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_booking_id(query[:id]) unless query[:id].blank?
    result = result.with_person_id(query[:person_id]) unless query[:person_id].blank?
    result = result.with_person_name(query[:name]) unless query[:name].blank?
    result = result.with_booking_officer(query[:booking_officer],query[:booking_officer_unit],query[:booking_officer_badge]) unless query[:booking_officer].blank? && query[:booking_officer_unit].blank? && query[:booking_officer_badge].blank?
    result = result.with_cell(query[:cell]) unless query[:cell].blank?
    result = result.with_afis(query[:afis]) unless query[:afis].blank?
    result = result.with_sched_release_date(query[:sched_release_date_from],query[:sched_release_date_to]) unless query[:sched_release_date_from].blank? && query[:sched_release_date_to].blank?
    result = result.with_release_date(query[:release_date_from],query[:release_date_to]) unless query[:release_date_from].blank? && query[:release_date_to].blank?
    result = result.with_release_officer(query[:release_officer],query[:release_officer_unit],query[:release_officer_badge]) unless query[:release_officer].blank? && query[:release_officer_unit].blank? && query[:release_officer_badge].blank?
    result = result.with_released_because(query[:released_because]) unless query[:released_because].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def allow_arraignment_notice?
    holds.active.of_type('Bondable').count > 0
  end
  
  def billing_status(billing_date = Date.yesterday)
    return 'Error' unless billing_date.is_a?(Date) && billing_date < Date.today
    return 'Error' if billing_date < booking_datetime.to_date
    return 'Error' if release_datetime? && billing_date > release_datetime.to_date
    released_today = !release_datetime.nil? && (billing_date == release_datetime.to_date)
    status = []
    holds.each do |h|
      # some holds get start dates before actual booking.  For these, need
      # to consider only actual booking time so get the later date.
      hold_start_date = [h.hold_datetime.to_date, booking_datetime.to_date].sort.pop
      # check hold dates for billing_date
      hold_here_today = (billing_date >= hold_start_date && (!h.cleared_datetime? || billing_date <= h.cleared_datetime.to_date))
      hold_here_yesterday = (billing_date > hold_start_date && hold_here_today)
      hold_here_midnight = (billing_date >= hold_start_date && (!h.cleared_datetime? || billing_date < h.cleared_datetime.to_date))
      hold_cleared_today = (hold_here_today && !hold_here_midnight)
      # determine return value for this hold
      if hold_here_today
        if h.doc_billable? && h.bill_from_date? && (h.bill_from_date <= billing_date)
          # only push DOC billable if here all day or released today
          unless hold_cleared_today && !released_today
            status.push('DOC Billable')
          end
        elsif h.hold_type == 'Temporary Release'
          # override all other status if temporary release for entire day only
          if hold_here_yesterday && hold_here_midnight
            return 'Temporary Release'
          end
        elsif h.doc?
          status.push('DOC')
        elsif h.hold_type == 'Hold for City'
          status.push("City")
        elsif h.hold_type == 'Temporary Transfer'
          if h.other_billable?
            # these get billed to whomever is getting billed normally
            # therefore do not add a status for this hold
          else
            if hold_here_yesterday && hold_here_midnight
              # these should not be billed regardless of their other status
              # however, only if transferred the entire day
              return "Not Billable"
            else
              # not transferred the whole day, don't add a status
            end
          end
        elsif h.hold_type == 'Hold for Other Agency' || h.hold_type == 'Temporary Housing'
          status.push("#{h.agency.blank? ? 'Unknown' : h.agency}")
        else
          status.push('Parish')
        end
      end
    end
    status = status.uniq
    if status.nil? || status.empty?
      return "Parish"
    elsif status.include?("DOC Billable")
      return "DOC Billable"
    elsif status.include?("DOC")
      return "DOC"
    elsif status.include?("Parish")
      return "Parish"
    elsif status.include?("City")
      return "City"
    elsif !status.include?("Not Billable")
      return status.join(',')
    else
      return "Not Billable"
    end
  end
  
  def reasons_not_destroyable
    reasons = []
    unless holds.empty?
      reasons.push("Has attached Holds")
    end
    unless transfers.empty?
      reasons.push("Has attached Transfers")
    end
    unless docs.empty?
      reasons.push("Has attached DOCs")
    end
    reasons.empty? ? nil : reasons.join(', ')
  end
  
  def current_location(*opts)
    options = opts.extract_options!
    if release_datetime?
      if !transfers.empty? && transfers.last.transfer_datetime == release_datetime
        "Transferred: #{transfers.last.to_agency}"
      else
        "Released"
      end
    elsif temporary_release?
      txt = temporary_release_reason
      "Temporary Release#{txt.blank? ? '' : ': ' + txt}"
    elsif tagency = is_transferred
      "Other: #{tagency}"
    elsif absents.empty? || absents.last.return_datetime?
      "#{jail.acronym}: #{cell}"
    else
      labsent = absents.last
      txt = "Absent: ".html_safe
      txt << (labsent.absent_type.blank? ? 'Unknown Absent Type' : labsent.absent_type)
      if labsent.leave_datetime?
        txt
      else
        ["Pending:", txt].join(' ')
      end
    end
  end
  
  def doc?
    holds.active.doc.count > 0
  end
  
  def doc_billable?
    holds.active.doc_billable.count > 0
  end
  
  def doc_incomplete?
    doc_records.with_problems.count > 0
  end
  
  def hold_for_probation?
    holds.active.parish.count > 0
  end
  
  def hold_summary
    holds.active.collect{|h| (h.hold_type.blank? ? '[Invalid]' : '[' + h.hold_type + ']')}.uniq.to_sentence(last_word_connector: ' and ')
  end
  
  def hours_served_since(time)
    return 0 if !time.is_a?(Time) || booking_logs.empty?
    hrs = 0
    booking_logs.on_or_after(time).all.each do |l|
      hrs += l.hours_served(time)
    end
    return 0 if hrs < 0
    hrs
  end
  
  def is_expired
    holds.active.expired.count > 0
  end
  
  def is_housing
    holds.active.of_type('Temporary Housing').count > 0
  end
  
  def is_serving_time
    holds.active.sentenced.count > 0
  end
  
  def is_time_served
    holds.active.sentenced.served.count > 0
  end
  
  def is_transferred(datetime = Time.now)
    return nil unless datetime.is_a?(Time)
    h = holds.active_at(datetime).of_type('Temporary Transfer').first
    h.nil? ? nil : (h.agency.nil? ? "Unknown" : h.agency)
  end
  
  def last_booking_logs(lines=5)
    booking_logs.last(lines).reverse
  end
  
  def minutes_served_since(time)
    # calculate the total time logged since the time specified
    return 0 if !time.is_a?(Time) || booking_logs.empty?
    mins = 0
    booking_logs.on_or_after(time).all.each do |l|
      mins += l.minutes_served(time)
    end
    mins < 0 ? 0 : mins
  end
  
  def problems
    return nil if release_datetime?
    probs = []
    probs.push("No Holds") if holds.active.count == 0
    probs.push("Sentence Time Served") if holds.active.served.count > 0
    probs.push("Expired Holds") if holds.active.expired.count > 0
    probs.push("DOC Needs Info") if doc_records.with_problems.count > 0
    probs.join(', ')
  end
  
  def property_summary
    properties.join(', ')
  end
  
  def releasable?
    holds.active.empty?
  end
  
  def simple_time_served_by_log
    total = 0
    booking_logs.each{|l| total += l.hours_served}
    total = 0 if total < 0
    AppUtils.hours_to_string(total)
  end
  
  def start_time(userid)
    return false unless u = User.find_by_id(userid)
    l = booking_logs.last
    if l.nil? || l.stop_datetime?
      # there appears to be no active time log entry... add one
      self.booking_logs.create(start_datetime: Time.now, start_officer: u.sort_name, start_officer_unit: u.unit, start_officer_badge: u.badge)
      self.update_attribute(:disable_timers, false)
    end
  end
  
  def status(*args)
    options = args.extract_options!
    options[:returns] ||= :string
    # released takes precedence
    if release_datetime?
      if released_because.blank?
        return options[:returns] == :array ? ["[Released]"] : "[Released]" 
      else
        return options[:returns] == :array ? ["[Released: #{released_because}]"] : "[Released: #{released_because}]"
      end
    end
    if holds.active.empty?
      return options[:returns] == :array ? ["[No Holds]"] : "[No Holds]"
    end
    statuses = []
    holds.active.each do |h|
      unless h.hold_type.blank?
        if ["Temporary Transfer","Temporary Housing"].include?(h.hold_type)
          statuses.push("[#{h.hold_type}]")
        elsif h.hold_type == "Serving Sentence"
          hrs = h.hours_remaining
          if hrs > 0
            statuses.push("[#{AppUtils.hours_to_string(hrs)}]")
          else
            if !h.hours_total || h.hours_total <= 0
              statuses.push("[Pending Sentence]")
            else
              statuses.push("[Time Served]")
            end
          end
        elsif ["Hold for City","Hold for Other Agency", "No Bond"].include?(h.hold_type)
          statuses.push("[#{h.hold_type}]")
        elsif ["Probation (Parish)","Probation/Parole (State)"].include?(h.hold_type)
          statuses.push("[Probation Hold]")
        elsif ["48 Hour", "72 Hour", "Bondable"].include?(h.hold_type)
          statuses.push("[#{h.hold_type}#{h.expired? ? ' EXP' : ''}]")
        elsif h.hold_type == "Temporary Release"
          statuses.push("[Temp Release]")
        else
          statuses.push("[#{h.hold_type}]")
        end
      end
    end
    if options[:returns] == :array
      statuses
    else
      statuses.join(' ')
    end
  end
  
  def stop_time(userid)
    return false unless u = User.find_by_id(userid)
    l = booking_logs.last
    unless l.nil? || l.stop_datetime?
      # there appears to be an active time log entry... stop it
      l.update_attributes(stop_datetime: Time.now, stop_officer: u.sort_name, stop_officer_unit: u.unit, stop_officer_badge: u.badge)
      self.update_attribute(:disable_timers, true)
    end
  end
  
  def temporary_release?(tdate=nil)
    tdate ||= Date.today
    return false unless tdate.is_a?(Date)
    holds.of_type('Temporary Release').active_on(tdate).count > 0
  end

  def temporary_release_reason(tdate=nil)
    tdate ||= Date.today
    return nil unless tdate.is_a?(Date)
    if trelease = holds.of_type('Temporary Release').active_on(tdate).first
      return trelease.temp_release_reason
    end
    nil
  end
  
  def time_served_by_log
    AppUtils.hours_to_sentence(booking_logs.collect{|l| l.hours_served}.sum)
  end
  
  def total_remaining_hours
    hour_array = []
    holds.active.sentenced.reject{|h| h.active_blockers?}.each do |h|
      temp = h.find_hours_total(0)
      return -1 unless temp.reject{|v| v >= 0}.empty?
      hour_array.concat(temp)
    end
    return 0 if hour_array.empty?
    return hour_array.sort.last
  end
  
  def total_remaining_days
    hrs = total_remaining_hours
    return -1 if hrs < 0
    (hrs / 24.0).round
  end
  
  def total_remaining_text
    temp = total_remaining_hours
    if temp != 0
      AppUtils.hours_to_sentence(total_remaining_hours, returns: 'string')
    else
      "0 Hours (All Time Served)"
    end
  end
  
  def check_destroyable
    reasons_not_destroyable.nil?
  end
  
  def check_person
    if !person.nil? && person.bookings.active.count > 1
      self.errors.add(:person_id, "cannot be used. Already has an open Booking!")
      return false
    end
    true
  end
  
  def check_timers
    if release_datetime?
      l = booking_logs.last
      unless l.nil? || l.stop_datetime?
        # there appears to be an active time log entry... stop it
        l.update_attributes(stop_datetime: release_datetime, stop_officer: updater.sort_name, stop_officer_unit: updater.unit, stop_officer_badge: updater.badge)
        l.save
      end
    else
      # don't create time record if disable timers is set when booking created.
      unless disable_timers?
        l = booking_logs.last
        if l.nil? || l.stop_datetime?
          # last entry doesn't exist or is stopped, create a new one
          self.booking_logs.create(start_datetime: Time.now, start_officer: updater.sort_name, start_officer_unit: updater.unit, start_officer_badge: updater.badge)
        end
      end
    end
    true
  end
  
  def lookup_officers
    unless booking_officer_id.blank?
      if ofc = Contact.find_by_id(booking_officer_id)
        self.booking_officer = ofc.sort_name
        self.booking_officer_badge = ofc.badge_no
        self.booking_officer_unit = ofc.unit
      end
      self.booking_officer_id = nil
    end
    unless release_officer_id.blank?
      if ofc = Contact.find_by_id(release_officer_id)
        self.release_officer = ofc.sort_name
        self.release_officer_badge = ofc.badge_no || ''
        self.release_officer_unit = ofc.unit || ''
      end
      self.release_officer_id = nil
    end
    true
  end
  
  def process_bond_recalls
    return true if release_datetime?
    return true if person.nil?
    blist = person.bonds.reject{|b| b.final? || !['Revoked','Forfeited'].include?(b.status)}
    blist.each do |b|
      # use bond.recall method to add the holds
      stat = (b.status == 'Revoked' ? 'revoke' : 'forfeit')
      b.recall(stat, booking_id: self.id)
    end
    true
  end
  
  def send_new_messages
    receivers = User.where(notify_booking_new: true).all
    receivers.each do |r|
      next unless r.can?(self.class.base_perms)
      txt = AutomatedMessagesController.new.notify(self, 'new')
      Message.create(receiver_id: r.id, sender_id: updated_by, subject: "Booked #{jail.name}: #{person.nil? ? 'Invalid Person' : person.sort_name}", body: txt)
    end
  end
  
  def send_release_messages
    if previous_changes.include?('release_datetime')
      if release_datetime?
        # released booking
        receivers = User.where(notify_booking_release: true).all
        receivers.each do |r|
          next unless r.can?(self.class.base_perms)
          txt = AutomatedMessagesController.new.notify(self, 'released')
          Message.create(receiver_id: r.id, sender_id: updated_by, subject: "Released #{jail.name}: #{person.nil? ? 'Invalid Person' : person.sort_name}", body: txt)
        end
      else
        # reopened booking
        receivers = User.where(notify_booking_new: true).all
        receivers.each do |r|
          next unless r.can?(self.class.base_perms)
          txt = AutomatedMessagesController.new.notify(self, 'reopened')
          Message.create(receiver_id: r.id, sender_id: updated_by, subject: "Rebooked #{jail.name}: #{person.nil? ? 'Invalid Person' : person.sort_name}", body: txt)
        end
      end
    end
  end
end

# == Schema Information
#
# Table name: bookings
#
#  id                    :integer          not null, primary key
#  person_id             :integer          not null
#  cash_at_booking       :decimal(12, 2)   default(0.0), not null
#  cash_receipt          :string(255)      default(""), not null
#  booking_officer       :string(255)      default(""), not null
#  booking_officer_unit  :string(255)      default(""), not null
#  remarks               :text             default(""), not null
#  phone_called          :string(255)      default(""), not null
#  intoxicated           :boolean          default(FALSE), not null
#  sched_release_date    :date
#  afis                  :boolean          default(FALSE), not null
#  release_officer       :string(255)      default(""), not null
#  release_officer_unit  :string(255)      default(""), not null
#  created_by            :integer          default(1)
#  updated_by            :integer          default(1)
#  created_at            :datetime
#  updated_at            :datetime
#  booking_officer_badge :string(255)      default(""), not null
#  release_officer_badge :string(255)      default(""), not null
#  good_time             :boolean          default(TRUE), not null
#  disable_timers        :boolean          default(FALSE), not null
#  attorney              :string(255)      default(""), not null
#  legacy                :boolean          default(FALSE), not null
#  jail_id               :integer          not null
#  cell                  :string(255)      default(""), not null
#  booking_datetime      :datetime
#  release_datetime      :datetime
#  released_because      :string(255)      default(""), not null
#
# Indexes
#
#  index_bookings_on_created_by  (created_by)
#  index_bookings_on_jail_id     (jail_id)
#  index_bookings_on_person_id   (person_id)
#  index_bookings_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  bookings_created_by_fk  (created_by => users.id)
#  bookings_jail_id_fk     (jail_id => jails.id)
#  bookings_person_id_fk   (person_id => people.id)
#  bookings_updated_by_fk  (updated_by => users.id)
#
