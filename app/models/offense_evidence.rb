class OffenseEvidence < ActiveRecord::Base
  belongs_to :offense, inverse_of: :offense_evidences                      # constraint
  belongs_to :evidence, inverse_of: :offense_evidences                     # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'Offense Evidences'
  end
end

# == Schema Information
#
# Table name: offense_evidences
#
#  id          :integer          not null, primary key
#  offense_id  :integer          not null
#  evidence_id :integer          not null
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_offense_evidences_on_evidence_id  (evidence_id)
#  index_offense_evidences_on_offense_id   (offense_id)
#
# Foreign Keys
#
#  offense_evidences_evidence_id_fk  (evidence_id => evidences.id)
#  offense_evidences_offense_id_fk   (offense_id => offenses.id)
#
