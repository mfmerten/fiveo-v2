class Commissary < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'
  belongs_to :person, inverse_of: :commissaries
  belongs_to :jail, inverse_of: :commissaries
  
  ## Attributes #############################################
  
  attr_accessor :officer_id
  
  ## Scopes #############################################
  
  scope :unposted, -> { where report_datetime: nil }
  scope :posted_on, ->(posting_time) { where(report_datetime: posting_time) }
  scope :deposits, -> { where 'commissaries.deposit > 0' }
  scope :withdrawals, -> { where 'withdraw > 0' }
  scope :by_name, -> { includes(:person).order('people.family_name, people.given_name, commissaries.id') }
  scope :by_date, -> { order :transaction_date }
  scope :between, ->(start, stop) { AppUtils.for_daterange self, :transaction_date, start: start, stop: stop, date: true }
  scope :on_or_before, ->(date) { AppUtils.for_daterange self, :transaction_date, stop: date, date: true }
  scope :for_balance, -> { joins(:person).select('commissaries.person_id, SUM(commissaries.deposit - commissaries.withdraw) AS balance, commissaries.jail_id').group('commissaries.person_id').having("commissaries.balance != 0") }
  scope :belonging_to_person, ->(p_id) { includes(:person).where(person_id: p_id) }
  scope :belonging_to_jail, ->(j) { where jail_id: j }
  
  ## Callbacks #############################################
  
  before_save :check_creator
  before_validation :check_amounts, :lookup_officers
  before_destroy :check_destroyable

  ## Validations #############################################
  
  validates_presence_of :officer
  validates_date :transaction_date
  validates_numericality_of :deposit, :withdraw, greater_than_or_equal_to: 0, less_than: 10 ** 10
  validates_person :person_id
  validates_jail :jail_id

  ## Constants #############################################
  
  CommissaryType = ['Adjustment', 'Sales', 'Deposit', 'Medical', 'Refund'].freeze
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_commissary
  end
  
  def self.desc
    'Jail Commissary Records'
  end
  
  def self.posting_dates(jail)
    select('DISTINCT report_datetime').order(report_datetime: :desc).where(["report_datetime IS NOT NULL AND jail_id = ?",jail]).limit(10).all.collect{|c| [AppUtils.format_date(c.report_datetime),c.report_datetime]}
  end
  
  ## Instance Methods #############################################
  
  def reasons_not_destroyable
    report_datetime? ? "Transaction Posted" : nil
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def check_amounts
    retval = true
    unless (withdraw? && withdraw > 0) || (deposit? && deposit > 0)
      self.errors.add(:base, "Must enter either a withdraw amount or a deposit amount!")
      retval = false
    end
    if (withdraw? && withdraw > 0) && (deposit? && deposit > 0)
      self.errors.add(:base, "Must enter a withdraw amount or a deposit amount, not both!")
      retval = false
    end
    if withdraw? && withdraw < 0
      self.errors.add(:withdraw, "cannot be negative!")
      retval = false
    end
    if deposit? && deposit < 0
      self.errors.add(:deposit, "cannot be negative!")
      retval = false
    end
    retval
  end

  def check_destroyable
    reasons_not_destroyable.nil?
  end
  
  def lookup_officers
    unless officer_id.blank?
      if ofc = Contact.find_by_id(officer_id)
        self.officer = ofc.sort_name
        self.officer_badge = ofc.badge_no
        self.officer_unit = ofc.unit
      end
      self.officer_id = nil
    end
    true
  end
end

# == Schema Information
#
# Table name: commissaries
#
#  id               :integer          not null, primary key
#  person_id        :integer          not null
#  officer          :string(255)      default(""), not null
#  officer_unit     :string(255)      default(""), not null
#  deposit          :decimal(12, 2)   default(0.0), not null
#  withdraw         :decimal(12, 2)   default(0.0), not null
#  created_by       :integer          default(1)
#  updated_by       :integer          default(1)
#  created_at       :datetime
#  updated_at       :datetime
#  officer_badge    :string(255)      default(""), not null
#  report_datetime  :datetime
#  receipt_no       :string(255)      default(""), not null
#  memo             :string(255)      default(""), not null
#  transaction_date :date
#  jail_id          :integer          not null
#  category         :string(255)      default(""), not null
#
# Indexes
#
#  index_commissaries_on_booking_id  (person_id)
#  index_commissaries_on_created_by  (created_by)
#  index_commissaries_on_jail_id     (jail_id)
#  index_commissaries_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  commissaries_created_by_fk  (created_by => users.id)
#  commissaries_jail_id_fk     (jail_id => jails.id)
#  commissaries_person_id_fk   (person_id => people.id)
#  commissaries_updated_by_fk  (updated_by => users.id)
#
