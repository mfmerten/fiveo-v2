class Revocation < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :arrest, inverse_of: :revocations                          # constraint
  belongs_to :docket, inverse_of: :revocations
  has_one :hold, dependent: :destroy, inverse_of: :revocation           # constraint
  has_one :doc_record, class_name: 'Doc', dependent: :destroy, inverse_of: :revocation  # constraint
  has_many :revocation_consecutives, dependent: :destroy                # constraint
  has_many :consecutive_dockets, through: :revocation_consecutives, source: :docket  # constrained through revocation_consecutives

  scope :unprocessed, -> { where processed: false }
  
  attr_accessor :force, :consecutives

  before_validation :check_arrest
  after_create :check_docket
  before_save :process, :check_creator
  after_update :save_children

  validates_presence_of :judge
  validates_numericality_of :total_hours, :total_days, :total_months, :total_years,
    :total_life, :parish_hours, :parish_days, :parish_months, :parish_years,
    only_integer: true, greater_than_or_equal_to: 0
  validates_date :revocation_date
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_court
  end
  
  def self.desc
    'Revocations for Dockets'
  end
  
  def self.hours_served(*revocation_ids)
    rev_ids = revocation_ids.flatten.compact.uniq
    revocations = where(id: rev_ids)
    
    rholds = revocations.collect{|r| r.hold_or_doc_hold}.compact

    # get time range from each hold
    ranges = rholds.collect{|h| h.sentence_time_range}.compact
    return 0 if ranges.empty?
    
    # get time range from each temporary release hold
    bholds = rholds.collect{|h| h.booking.temp_release_holds}.flatten.compact.uniq
    exclude_ranges = bholds.collect{|h| h.sentence_time_range}

    # calculate total hours using TimeServed
    ts = TimeServed.new(ranges, nil_value: Time.now)
    ts.add_excludes(exclude_ranges) unless exclude_ranges.empty?
    ts.hours
  end
  
  ## Instance Methods #############################################
  
  # returns revocation hold unless nil, where it attempts to return a doc_record hold
  # if it can.  If both fail, nil is returned
  def hold_or_doc_hold
    if hold.nil?
      if doc_record.nil? || doc_record.hold.nil?
        nil
      else
        doc_record.hold
      end
    else
      hold
    end
  end
  
  def consecutives=(consecutives_string)
    docket_numbers = consecutives_string.split(/,/)
    return true if docket_numbers.empty?
    self.consecutive_dockets.clear
    docket_numbers.each do |d|
      if dckt = Docket.find_by_docket_no_and_person_id(d.trim, docket.person_id)
        self.consecutive_dockets << dckt
      end
    end
    true
  end
  
  def consecutive_holds
    return [] if consecutive_type.blank?
    if consecutive_type == "Any Current"
      hold_list = []
      unless docket.person.bookings.empty? || docket.person.bookings.last.release_datetime? || docket.person.bookings.last.holds.empty?
        docket.person.bookings.last.holds.active.reject{|h| h.hold_type != 'Serving Sentence'}.each do |h|
          hold_list.push(hold)
        end
      end
      return hold_list
    elsif consecutive_type == "Other Dockets" || consecutive_type == "This Docket"
      hold_list = []
      if consecutive_type == "Other Dockets"
        docket_list = consecutive_dockets
      else
        docket_list = [docket]
      end
      docket_list.each do |d|
        d.da_charges.each do |c|
          # if the charge has not been processed, we can't calculate consecutives yet
          return nil unless c.processed?
          # if the charge does not have a sentence, skip it
          next if c.sentence.nil?
          # if the sentence is this one, skip it
          next if c.sentence.id == self.id
          # if the sentence does not have a hold or doc record, its not for serving time
          next if c.sentence.hold.nil? && c.sentence.doc_record.nil?
          if c.sentence.doc_record.nil?
            # return sentence hold
            hold_list.push(c.sentence.hold)
          else
            return nil if c.sentence.doc_record.hold.nil?
            hold_list.push(c.sentence.doc_record.hold)
          end
        end
      end
      return hold_list
    else
      # unknown consecutive_type - ignore it
      return []
    end
  end
  
  def hours_served
    self.class.hours_served(self.id)
  end
  
  def jail_time?
    return false if !revoked? && parish_string.blank?
    true
  end
  
  def parish_string
    AppUtils.sentence_to_string(parish_hours, parish_days, parish_months, parish_years)
  end
  
  def revocation_time_served
    return 0 if arrest.nil?
    return 0 if arrest.holds.empty?
    return 0 unless book = arrest.holds.first.booking
    release = (book.release_datetime? ? book.release_datetime : Time.now)
    return AppUtils.hours_diff(release, arrest.datetime)
  end
  
  def status
    txt = []
    if doc?
      txt.push('[DOC]')
    end
    if processed?
      txt.push('[Processed]')
    else
      txt.push('[Pending]')
    end
    unless total_string.blank? && parish_string.blank?
      txt.push('[Sentenced]')
      if hold.nil?
        txt.push('[Missing Hold]')
      else
        if hold.cleared_datetime?
          txt.push('[Time Served]')
        else
          txt.push('[Serving Time]')
        end
      end
      if doc? && doc_record.nil?
        txt.push('[Missing DOC]')
      end
    else
      txt.push('[Not Sentenced]')
    end
    return txt.join(' ')
  end
  
  def total_of_sentences
    AppUtils.hours_to_sentence(docket.total_sentence_length)
  end
  
  def total_of_time_served
    hrs_served = 0
    hrs_served += Arrest.hours_served(docket.arrest_ids | docket.da_charge_arrest_ids)
    hrs_served += Sentence.hours_served(docket.sentence_ids)
    hrs_served += Revocation.hours_served(docket.revocation_ids)
    return hrs_served
  end
  
  def total_string
    AppUtils.sentence_to_string(total_hours, total_days, total_months, total_years, total_life, total_death)
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def check_arrest
    if arrest_id?
      if arrest.nil?
        self.errors.add(:arrest_id, "is not a valid arrest!")
        return false
      elsif arrest.person_id != docket.person_id
        self.errors.add(:arrest_id, "is not for this person!")
        return false
      end
    end
    true
  end
  
  def check_docket
    unless processed?
      if docket.processed?
        self.docket.update_attribute(:processed, false)
      end
    end
    true
  end
  
  def process
    self.processed = false
    unless !docket.processed?
      self.docket.update_attribute(:processed, false)
    end
    success = true
    self.process_status = ""
    
    total_to_serve = 0
    if revoked?
      unless total_string.blank?
        total_to_serve = AppUtils.sentence_to_hours(total_hours, total_days, total_months, total_years, total_life, total_death)
      end
    else
      unless parish_string.blank?
        total_to_serve = AppUtils.sentence_to_hours(parish_hours, parish_days, parish_months, parish_years)
      end
    end
    remaining_to_serve = total_to_serve
    if time_served? && remaining_to_serve > 0
      ts = total_of_time_served
      if ts > 0
        remaining_to_serve -= ts
        rem = "\nTo Serve time reflects #{ts} Hours Credit for Time Served."
      end
    end
    remaining_hours, remaining_days, remaining_months, remaining_years, remaining_life, remaining_death = AppUtils.hours_to_sentence(remaining_to_serve)   
    
    if success
      # check consecutive holds
      consecs = consecutive_holds
      if consecs.nil?
        # not ready for processing
        self.process_status << "[Revocation Consecutive to Unprocessed Sentences]"
        success = false
      end
    end
    
    if success && jail_time?
      # validate booking
      if docket.person.bookings.empty? || (hold.nil? && docket.person.bookings.last.release_datetime?)
        # create booking if required to force processing of revocation
        if force == true && (docket.person.bookings.empty? || (docket.person.bookings.last.release_datetime.to_date < revocation_date))
          # creating a booking because:
          # 1. forcing processing and no bookings exist
          # 1. forcing processing and last booking was released before the court date
          book = Booking.new(person_id: docket.person_id, disable_timers: true, created_by: updated_by, updated_by: updated_by, remarks: 'Created Automatically By Revocation Processing.')
          book.booking_datetime = Time.now
          book.release_datetime = book.booking_datetime
          book.release_officer = book.booking_officer
          book.release_officer_unit = book.booking_officer_unit
          book.release_officer_badge = book.booking_officer_badge
          book.released_because = "Automatic Release"
          book.remarks = 'Created and Released Automatically By Revocation Processing.'
          book.save(validate: false)
        elsif force == true && docket.person.bookings.last.release_datetime?
          book = docket.person.bookings.last
        else
          success = false
          self.process_status << '[Invalid Booking]'
        end
      else
        book = docket.person.bookings.last
      end
      
      if success
        # should be ready to post revocation
        if doc?
          # Hard Labor Dept of Corrections
          
          # check is parish hold exists
          if !hold.nil? && !hold.doc?
            # oops, has parish hold, get rid of it
            hold.destroy
          end
          
          d = doc_record
          if d.nil?
            d = self.build_doc_record
          end
          d.sentence_hours = total_hours
          d.sentence_days = total_days
          d.sentence_months = total_months
          d.sentence_years = total_years
          d.sentence_life = total_life
          d.sentence_death = total_death
          d.to_serve_hours = remaining_hours
          d.to_serve_days = remaining_days
          d.to_serve_months = remaining_months
          d.to_serve_years = remaining_years
          d.to_serve_life = remaining_life
          d.to_serve_death = remaining_death
          d.conviction = docket.sentence_summary
          d.remarks = (rem.nil? ? remarks : remarks + rem)
          unless d.created_by?
            d.created_by = updated_by
          end
          d.updated_by = updated_by
          d.booking_id = docket.person.bookings.last.id
          d.person_id = docket.person_id
          if force == true
            d.force = true
          end
          unless d.valid?
            success = false
            self.process_status << '[Unable to Create DOC Record]'
            self.process_status << "(#{d.errors.full_messages.join(', ')})"
          end
        else
          # parish jail
          # make sure there is not a doc record already
          unless doc_record.nil?
            # oops, get rid of it
            doc_record.destroy
          end
          
          h = hold
          if h.nil?
            h = self.build_hold
          end
          served = book.hours_served_since([AppUtils.get_datetime(revocation_date, '12:00'),book.booking_datetime].sort.last)
          h.booking_id = book.id
          h.hold_datetime = AppUtils.get_datetime(revocation_date, "12:00")
          h.hold_type = "Serving Sentence"
          h.hold_by = creator.sort_name
          h.hold_by_unit = creator.unit
          h.hold_by_badge = creator.badge
          h.remarks = "Added Automatically by Revocation Processing"
          unless h.created_by?
            h.created_by = updated_by
          end
          h.updated_by = updated_by
          unless h.hours_served? && h.hours_served > 0
            h.hours_served = served
          end
          h.hours_total = total_to_serve
          if to_serve_death?
            h.death_sentence = true
          end
          if time_served?
            unless h.hours_time_served? && h.hours_time_served > 0
              h.hours_time_served = total_of_time_served
            end
          else
            h.hours_time_served = 0
          end
          unless h.datetime_counted?
            h.datetime_counted = Time.now
          end
          if force == true && book.release_datetime?
            h.cleared_datetime = h.hold_datetime
            h.cleared_because = "Automatic Release"
            h.cleared_by = h.hold_by
            h.cleared_by_unit = h.hold_by_unit
            h.cleared_by_badge = h.hold_by_badge
            h.explanation = 'Inmate Released Prior To Revocation Processing because of time served or transfer.'
            h.remarks = 'Automatically Added And Cleared by Revocation Processing.'
          end
          h.deny_goodtime = deny_goodtime
          if h.valid?
            # need to close all pending sentence holds, if any
            pendings = book.holds.where({cleared_datetime: nil, hold_type: "Pending Sentence"}).all
            unless pendings.empty?
              pendings.each do |p|
                p.cleared_datetime = Time.now
                p.cleared_because = 'Automatic Release'
                p.cleared_by = updater.sort_name
                p.cleared_by_unit = updater.unit
                p.cleared_by_badge = updater.badge
                p.explanation = "Automatically cleared by Revocation Processing."
                p.updated_by = updated_by
                p.save(validate: false)
              end
            end
          else
            self.process_status << '[Unable to Create Sentence Hold]'
            self.process_status << "(#{h.errors.full_messages.join(', ')})"
            success = false
          end
        end
      end
    end
    if success
      if revoked?
        unless docket.probations.empty?
          docket.probations.each do |p|
            p.status = 'Revoked'
            p.revoked_date = revocation_date
            p.revoked_for = "Revoked by Revocation Processing of Revocation ID: #{id}"
            p.updated_by = updated_by
            p.save(validate: false)
          end
        end
      end
      self.processed = true
      self.process_status = ""
    end
    self.force = false
    true
  end
  
  def save_children
    unless doc_record.nil?
      self.doc_record.save(validate: false)
    end
    unless hold.nil?
      self.hold.save(validate: false)
      self.hold.blockers.clear
      c = consecutive_holds
      unless c.nil? || c.empty?
        self.hold.blockers << c
      end
    end
  end
end

# == Schema Information
#
# Table name: revocations
#
#  id               :integer          not null, primary key
#  revocation_date  :date
#  arrest_id        :integer
#  revoked          :boolean          default(FALSE), not null
#  time_served      :boolean          default(FALSE), not null
#  total_days       :integer          default(0), not null
#  total_months     :integer          default(0), not null
#  total_years      :integer          default(0), not null
#  doc              :boolean          default(FALSE), not null
#  parish_days      :integer          default(0), not null
#  parish_months    :integer          default(0), not null
#  parish_years     :integer          default(0), not null
#  processed        :boolean          default(FALSE), not null
#  remarks          :text             default(""), not null
#  created_by       :integer          default(1)
#  updated_by       :integer          default(1)
#  created_at       :datetime
#  updated_at       :datetime
#  process_status   :string(255)      default(""), not null
#  total_hours      :integer          default(0), not null
#  parish_hours     :integer          default(0), not null
#  delay_execution  :boolean          default(FALSE), not null
#  deny_goodtime    :boolean          default(FALSE), not null
#  total_life       :integer          default(0), not null
#  total_death      :boolean          default(FALSE), not null
#  consecutive_type :string(255)      default(""), not null
#  judge            :string(255)      default(""), not null
#  docket_id        :integer          not null
#
# Indexes
#
#  index_revocations_on_arrest_id   (arrest_id)
#  index_revocations_on_created_by  (created_by)
#  index_revocations_on_docket_id   (docket_id)
#  index_revocations_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  fk_rails_e1d0a12111        (docket_id => dockets.id)
#  revocations_arrest_id_fk   (arrest_id => arrests.id)
#  revocations_created_by_fk  (created_by => users.id)
#  revocations_updated_by_fk  (updated_by => users.id)
#
