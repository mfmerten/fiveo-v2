class Investigation < ActiveRecord::Base
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'          # constraint with nullify
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'          # constraint with nullify
  belongs_to :owner, class_name: "User", foreign_key: 'owned_by'              # constraint with nullify
  has_many :notes, class_name: 'InvestNote', dependent: :destroy              # constraint
  has_many :invest_arrests, -> { includes arrest: [:person, :charges] }, dependent: :destroy  # constraint
  has_many :arrests, through: :invest_arrests                                 # constrained through invest_arrests
  has_many :invest_cases, -> { includes :call }, dependent: :destroy          # constraint
  has_many :cases, through: :invest_cases, source: :call                      # constrained through invest_cases
  has_many :invest_contacts, -> { includes contact: :user }, dependent: :destroy  # constraint
  has_many :contacts, through: :invest_contacts                               # constrained through invest_contacts
  has_many :crimes, class_name: 'InvestCrime', dependent: :destroy            # constraint
  has_many :criminals, class_name: 'InvestCriminal', dependent: :destroy      # constraint
  has_many :events, dependent: :destroy                                       # constraint
  has_many :invest_evidences, -> { includes :evidence }, dependent: :destroy  # constraint
  has_many :evidences, through: :invest_evidences                             # constrained through invest_evidences
  has_many :invest_offenses, -> { includes :offense }, dependent: :destroy    # constraint
  has_many :offenses, through: :invest_offenses                               # constrained through invest_offenses
  has_many :photos, class_name: 'InvestPhoto', dependent: :destroy            # constraint
  has_many :reports, -> { includes :owner }, class_name: 'InvestReport', dependent: :destroy # constraint
  has_many :invest_sources, -> { includes :person }, dependent: :destroy      # constraint
  has_many :sources, through: :invest_sources, source: :person                # constrained through invest_sources
  has_many :invest_stolens, -> { includes stolen: :person }, dependent: :destroy  # constraint
  has_many :stolens, through: :invest_stolens                                 # constrained through invest_stolens
  has_many :vehicles, class_name: 'InvestVehicle', dependent: :destroy        # constraint
  has_many :invest_victims, -> { includes :person }, dependent: :destroy      # constraint
  has_many :victims, through: :invest_victims, source: :person                # constrained through invest_victims
  has_many :invest_warrants, dependent: :destroy                              # constraint
  has_many :warrants, -> { includes :charges }, through: :invest_warrants  # constrained through invest_warrants
  has_many :invest_witnesses, dependent: :destroy                             # constraint
  has_many :witnesses, through: :invest_witnesses, source: :person            # constrained through invest_witnesses
    
  accepts_nested_attributes_for :invest_arrests, allow_destroy: true, reject_if: ->(a) { a['arrest_id'].blank? || !Arrest.exists?(a['arrest_id'])}
  accepts_nested_attributes_for :invest_cases, allow_destroy: true, reject_if: ->(c) { c['call_id'].blank? || !Call.exists?(c['call_id'])}
  accepts_nested_attributes_for :invest_contacts, allow_destroy: true, reject_if: ->(c) { c['contact_id'].blank? || !Contact.exists?(c['contact_id'])}
  accepts_nested_attributes_for :invest_evidences, allow_destroy: true, reject_if: ->(e) { e['evidence_id'].blank? || !Evidence.exists?(e['evidence_id'])}
  accepts_nested_attributes_for :invest_offenses, allow_destroy: true, reject_if: ->(o) { o['offense_id'].blank? || !Offense.exists?(o['offense_id'])}
  accepts_nested_attributes_for :invest_sources, allow_destroy: true, reject_if: ->(s) { s['person_id'].blank? || !Person.exists?(s['person_id'])}
  accepts_nested_attributes_for :invest_stolens, allow_destroy: true, reject_if: ->(s) { s['stolen_id'].blank? || !Stolen.exists?(s['stolen_id'])}
  accepts_nested_attributes_for :invest_victims, allow_destroy: true, reject_if: ->(v) { v['person_id'].blank? || !Person.exists?(v['person_id'])}
  accepts_nested_attributes_for :invest_warrants, allow_destroy: true, reject_if: ->(w) { w['warrant_id'].blank? || !Warrant.exists?(w['warrant_id'])}
  accepts_nested_attributes_for :invest_witnesses, allow_destroy: true, reject_if: ->(w) { w['person_id'].blank? || !Person.exists?(w['person_id'])}
  
  scope :active, -> { where "LOWER(investigations.status) != 'solved'" }
  scope :by_reverse_id, -> { order id: :desc }
  
  # filter scopes
  scope :with_investigation_id, ->(i_id) { where id: i_id }
  scope :with_investigation_date, ->(start,stop) { AppUtils.for_daterange self, :investigation_datetime, start: start, stop: stop }
  scope :with_detective, ->(name,unit,badge) { AppUtils.for_officer self, :detective, name, unit, badge }
  scope :with_status, ->(s) { AppUtils.like self, :status, s }
  scope :with_notes, ->(n) { AppUtils.like self, :note, n, association: :notes }
  scope :with_details, ->(d) { AppUtils.like self, :details, d }
  scope :with_remarks, ->(r) { AppUtils.like self, :remarks, r }
  
  attr_accessor :photo_date, :photo_desc, :photo_taken_by, :photo_taken_by_unit
  attr_accessor :photo_taken_by_badge, :photo_taken_by_id, :photo_loc, :photo_batch
  
  before_save :check_creator, :clear_sources_if_new_owner
  after_save :check_owner, :update_private
  before_validation :check_photo_date, :lookup_officers
  after_update :save_photos

  validates_presence_of :details, unless: :photo_batch
  validates_date :investigation_datetime, on_or_before: :now, unless: :photo_batch
  validates_presence_of :photo_desc, :photo_taken_by, :photo_loc, :photo_date, if: :photo_batch

  InvestigationStatus = ["Active", "Inactive", "Solved", "Trial"].freeze

  VehicleType = ["Truck", "Van", "Car", "Motorcycle", "Boat", "Bicycle", 'Scooter','Skateboard', 'Bus', 'SUV', 'Convertable', 'RV', 'Motor Home', 'ATV', 'Jeep', 'Yacht', 'Airplane', 'Watercraft', 'Sailboat'].freeze
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_investigation
  end
  
  def self.desc
    'Detective Investigations'
  end
  
  def self.find_all_by_case_no(case_no)
    includes(:cases).where(calls: {case_no: case_no}).to_a
  end
  
  def self.related_cases(case_number)
    joins(:cases).where(calls: {case_no: case_number}).all
      .collect{|i| i.cases.collect{|c| c.case_no}}.flatten.compact.uniq
      .reject{|c| c == case_number}
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_investigation_id(query[:id]) unless query[:id].blank?
    result = result.with_investigation_date(query[:investigation_date_from],query[:investigation_date_to]) unless query[:investigation_date_from].blank? && query[:investigation_date_to].blank?
    result = result.with_detective(query[:detective],query[:detective_unit],query[:detective_badge]) unless query[:detective].blank? && query[:detective_unit].blank? && query[:detective_badge].blank?
    result = result.with_status(query[:status]) unless query[:status].blank?
    result = result.with_notes(query[:notes]) unless query[:notes].blank?
    result = result.with_details(query[:details]) unless query[:details].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def new_photos=(photo_attributes)
    photo_attributes.each do |attributes|
      unless attributes[:invest_photo].blank?
        attributes.update({
          investigation_id: id,
          private: self.private,
          owned_by: owned_by,
          description: photo_desc,
          date_taken: AppUtils.valid_date(photo_date),
          location_taken: photo_loc,
          taken_by: photo_taken_by,
          taken_by_unit: photo_taken_by_unit,
          taken_by_badge: photo_taken_by_badge,
          taken_by_id: photo_taken_by_id,
          created_by: updated_by,
          updated_by: updated_by
        })
        photos.build(attributes)
      end
    end
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def check_owner
    # none of the following records have the ability to change the owner, so
    # they do not need one of these methods
    records = crimes + criminals + photos + reports + vehicles + events
    records.each{|r| r.update_attribute(:owned_by, owned_by) unless r.owned_by == owned_by}
  end
  
  def check_photo_date
    if photo_batch
      if photo_date.blank?
        errors.add(:photo_date, 'is required!')
        return false
      end
      tmp = AppUtils.valid_date(photo_date)
      if tmp.nil?
        errors.add(:photo_date, 'is not a valid date!')
        return false
      end
    end
    true
  end
  
  def clear_sources_if_new_owner
    self.sources.clear if owned_by_changed?
  end
  
  def lookup_officers
    if photo_batch
      unless photo_taken_by_id.blank?
        if ofc = Contact.find_by_id(photo_taken_by_id)
          self.photo_taken_by = ofc.sort_name
          self.photo_taken_by_badge = ofc.badge_no
          self.photo_taken_by_unit = ofc.unit
        end
        self.photo_taken_by_id = nil
      end
    end
    true
  end
  
  def save_photos
    photos.each{|p| p.save(validate: false)}
  end
  
  def update_private
    records = crimes + criminals + photos + reports + vehicles + events
    records.each{|r| r.update_attribute(:private, self.private) if r.private != self.private}
  end
end

# == Schema Information
#
# Table name: investigations
#
#  id                     :integer          not null, primary key
#  private                :boolean          default(TRUE), not null
#  details                :text             default(""), not null
#  remarks                :text             default(""), not null
#  created_by             :integer          default(1)
#  updated_by             :integer          default(1)
#  created_at             :datetime
#  updated_at             :datetime
#  owned_by               :integer          default(1)
#  investigation_datetime :datetime
#  status                 :string(255)      default(""), not null
#
# Indexes
#
#  index_investigations_on_created_by  (created_by)
#  index_investigations_on_owned_by    (owned_by)
#  index_investigations_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  investigations_created_by_fk  (created_by => users.id)
#  investigations_owned_by_fk    (owned_by => users.id)
#  investigations_updated_by_fk  (updated_by => users.id)
#
