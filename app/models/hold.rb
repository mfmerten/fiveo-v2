class Hold < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper

  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :booking, inverse_of: :holds                               # constraint
  belongs_to :arrest, inverse_of: :holds                                # constraint
  belongs_to :probation, inverse_of: :hold                              # constraint
  has_many :hold_blockers, dependent: :destroy                          # constraint
  has_many :blockers, through: :hold_blockers, source: :blocker         # constrained through hold_blockers
  has_many :hold_blocks, class_name: 'HoldBlocker', foreign_key: :blocker_id, dependent: :destroy  # constraint
  has_many :blocks, through: :hold_blocks, source: :hold                # constrained through hold_blocks
  belongs_to :transfer, inverse_of: :hold                               # constraint
  belongs_to :sentence, inverse_of: :hold                               # constraint
  belongs_to :revocation, inverse_of: :hold                             # constraint
  has_one :doc_record, class_name: 'Doc', dependent: :nullify, inverse_of: :hold  # constraint
  has_many :hold_bonds, dependent: :destroy                             # constraint
  has_many :bonds, through: :hold_bonds                                 # constrained through hold_bonds
    
  delegate :jail, to: :booking
  
  scope :active, -> { where cleared_datetime: nil }
  scope :active_at, ->(time) { where("holds.hold_datetime <= ? AND (holds.cleared_datetime IS NULL OR holds.cleared_datetime > ?)",time,time) }
  scope :active_on, ->(date) { where("holds.hold_datetime <= ? and (holds.cleared_datetime IS NULL OR holds.cleared_datetime > ?)",date.to_time, date.to_time.end_of_day) }
  scope :served, -> { where "holds.hours_total > 0 AND (holds.hours_total - holds.hours_served - holds.hours_goodtime - holds.hours_time_served <= 0)" }
  scope :expired, -> { where("(holds.hold_type = '48 Hour' AND holds.hold_datetime < ?) OR (holds.hold_type = '72 Hour' AND holds.hold_datetime < ?)", Time.now - 48.hours, Time.now - 72.hours)}
  scope :doc, -> { where doc: true }
  scope :doc_billable, -> { where doc_billable: true }
  scope :of_type, ->(hold_type) { where(hold_type: hold_type) }
  scope :sentenced, -> { of_type ['Serving Sentence', 'Fines/Costs'] }
  scope :by_reverse_date, -> { order hold_datetime: :desc }
  scope :belonging_to_jail, ->(j) { AppUtils.for_lower self, :jail_id, j, association: :booking }
  # filter scopes
  scope :with_hold_id, ->(h_id) { where id: h_id }
  scope :with_booking_id, ->(b_id) { where booking_id: b_id }
  scope :with_arrest_id, ->(a_id) { where arrest_id: a_id }
  scope :with_hold_date, ->(start,stop) { AppUtils.for_daterange self, :hold_datetime, start: start, stop: stop }
  scope :with_hold_type, ->(h_type) { AppUtils.like self, :hold_type, h_type }
  scope :with_doc, ->(d) { AppUtils.for_bool self, :doc, d }
  scope :with_doc_billable, ->(db) { AppUtils.for_bool self, :doc_billable, db }
  scope :with_hold_by, ->(name,unit,badge) { AppUtils.for_officer self, :hold_by, name, unit, badge }
  scope :with_cleared_date, ->(start,stop) { AppUtils.for_daterange self, :cleared_datetime, start: start, stop: stop }
  scope :with_cleared_because, ->(bec) { AppUtils.like self, :cleared_because, bec }
  scope :with_cleared_by, ->(name,unit,badge) { AppUtils.for_officer self, :cleared_by, name, unit, badge }
  scope :with_explanation, ->(exp) { AppUtils.like self, :explanation, exp }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  
  attr_accessor :hold_by_id, :cleared_by_id, :agency_id, :transfer_officer_id
  
  before_validation :lookup_contacts, :check_for_transfer, :check_for_other_agency, :check_for_fines
  before_destroy :check_destroyable
  before_save :check_creator

  validates_presence_of :hold_by
  validates_presence_of :cleared_because, :explanation, if: :cleared_datetime?
  validates_presence_of :cleared_datetime, if: :cleared_because?
  validates_numericality_of :fines, greater_than_or_equal_to: 0, less_than: 10 ** 10
  validates_datetime :hold_datetime, on_or_before: :now
  validates_datetime :cleared_datetime, on_or_before: :now, allow_blank: true
  validates_date :bill_from_date, allow_blank: true
  validates_numericality_of :hours_served, :hours_total, :hours_time_served, only_integer: true, greater_than_or_equal_to: 0
  validates_numericality_of :hours_goodtime, greater_than_or_equal_to: 0

  HoldType = {
    "72 Hour" => false, "48 Hour" => true, "No Bond" => false, "Bondable" => false,
    "Pending Court" => true, "Fines/Costs" => true, "Serving Sentence" => false,
    "Probation/Parole (State)" => true, "Probation (Parish)" => true,
    "Hold for Questioning" => true, "Temporary Housing" => false,
    "Temporary Transfer" => false, "Pending Sentence" => true,
    "Hold for Other Agency" => true, "Hold for City" => true, "Legacy Hold" => false,
    "Temporary Release" => false, "Contempt of Court" => true, "Judicial" => true
  }.freeze

  ClearType = [
    "48 Hour Expired Per Judge", "Bonded", "DSS/Child Support Paid",
    "Fines/Costs Paid", "Escaped", "Released By Judge",
    "Released By Probation/Parole", "Time Served", "Transferred",
    "Released W/O Charges", "Manual Release", "48 Hour Signed by Judge",
    "72 Hour Hearing Completed", "Preset Bond", "Returned From Transfer",
    "Automatic Release"
  ].freeze
  
  BondableTypes = ["Bondable", "Probation/Parole (State)", "Probation (Parish)", "Hold for Other Agency", "Hold for City"].freeze

  TempReleaseReason = ["Work Release", "Awaiting Execution Of Sentence", "Escaped"].freeze
  
  ## Class Methods #############################################
  
  def self.add(book_id,arr_id,type,agency_name=nil)
    retval = nil
    if HoldType.include?(type)
      # we do nothing unless there is a valid arrest and booking specified
      arr = Arrest.find_by_id(arr_id)
      book = Booking.find_by_id(book_id)
      unless arr.nil? || book.nil?
        temp = book.holds.build(hold_type: type, created_by: arr.updated_by, updated_by: arr.updated_by, arrest_id: arr_id)
        # set hold datetime to whichever is later, arrest or booking
        temp.hold_datetime = [book.booking_datetime, arr.arrest_datetime].sort.last
        if arr.officers.empty?
          temp.hold_by = "Unknown"
        else
          temp.hold_by = arr.officers.first.officer
          temp.hold_by_unit = arr.officers.first.officer_unit
          temp.hold_by_badge = arr.officers.first.officer_badge
        end
        retval = temp
        temp.agency = agency_name || ""
        if type == "Bondable"
          temp.fines = arr.bond_amt
          if arr.hour_72_datetime?
            temp.hold_datetime = [book.booking_datetime, arr.hour_72_datetime].sort.last
          end
          # need to attach all bonds for the arrest (bondable holds) to this hold
          arr.bonds.each do |b|
            # have to do it this way because there could be probation holds attached
            # to the arrest that have their own bonds and we don't want to grab them
            # as well.
            if b.holds.empty? || b.holds.last.hold_type.blank? || b.holds.last.hold_type == 'Bondable'
              temp.bonds << b
            end
          end
        elsif type == "No Bond"
          if arr.hour_72_datetime?
            temp.hold_datetime = [book.booking_datetime, arr.hour_72_datetime].sort.last
          end
        end
        temp.save(validate: false)
      end
    end
    return retval
  end
  
  def self.base_perms
    :view_booking
  end
  
  def self.desc
    'Booking Hold Records'
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_hold_id(query[:id]) unless query[:id].blank?
    result = result.with_booking_id(query[:booking_id]) unless query[:booking_id].blank?
    result = result.with_arrest_id(query[:arrest_id]) unless query[:arrest_id].blank?
    result = result.with_hold_date(query[:hold_date_from],query[:hold_date_to]) unless query[:hold_date_from].blank? && query[:hold_date_to].blank?
    result = result.with_hold_type(query[:hold_type]) unless query[:hold_type].blank?
    result = result.with_doc(query[:doc]) unless query[:doc].blank?
    result = result.with_doc_billable(query[:doc_billable]) unless query[:doc_billable].blank?
    result = result.with_hold_by(query[:hold_by], query[:hold_by_unit], query[:hold_by_badge]) unless query[:hold_by].blank? && query[:hold_by_unit].blank? && query[:hold_by_badge].blank?
    result = result.with_cleared_date(query[:cleared_date_from], query[:cleared_date_to]) unless query[:cleared_date_from].blank? && query[:cleared_date_to].blank?
    result = result.with_cleared_because(query[:cleared_because]) unless query[:cleared_because].blank?
    result = result.with_cleared_by(query[:cleared_by], query[:cleared_by_unit], query[:cleared_by_badge]) unless query[:cleared_by].blank? && query[:cleared_by_unit].blank? && query[:cleared_by_badge].blank?
    result = result.with_explanation(query[:explanation]) unless query[:explanation].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def active_blockers?
    blockers.active.count > 0
  end
  
  def bond_total
    return BigDecimal.new('0.0',2) if bonds.active.count == 0
    bonds.active.sum(:bond_amt)
  end
  
  def can_bond?
    !cleared_datetime? && BondableTypes.include?(hold_type)
  end
  
  def clear_release(user_id)
    return unless cleared_datetime?
    self.update_attributes(cleared_datetime: nil, cleared_by: '', cleared_by_unit: '', cleared_by_badge: '', updated_by: user_id, cleared_because: '', explanation: '')
    unless hold_type == 'Temporary Transfer'
      self.update_attributes(agency: '', transfer_officer: '', transfer_officer_badge: '', transfer_officer_unit: '')
    end
  end
  
  def days_remaining
    hrs = hours_remaining
    return -1 if hrs < 0
    return 0 if hrs == 0
    (hrs / 24.0).round
  end
  
  def expired?
    return true if hold_type == "48 Hour" && AppUtils.hours_diff(Time.now, hold_datetime) > 48
    return true if hold_type == "72 Hour" && AppUtils.hours_diff(Time.now, hold_datetime) > 72
    return false
  end
  
  def find_hours_total(total)
    return [0] unless total.is_a?(Integer)
    return [-1] if total < 0 || hours_remaining < 0
    return [ total + hours_remaining ] if blocks.empty?
    retarray = []
    blocks.each do |b|
      temp = b.find_hours_total(total + hours_remaining)
      return -1 if temp < 0
      retarray.push(temp)
    end
    retarray.flatten
  end
  
  def hold_reason
    case
    when hold_type.blank?
      "Unknown"
    when hold_type == 'Fines/Costs'
      "Must pay #{number_to_currency(fines)} or serve #{hours_total} hours (approx. #{((hours_total? && hours_total > 0 ? hours_total : 0) / 24).round} days) in jail"
    when !sentence.nil?
      sentence.conviction + "(#{sentence.conviction_count})"
    when ['Temporary Housing', 'Hold for City', 'Hold for Other Agency', 'Temporary Transfer'].include?(hold_type)
      self.agency
    when !arrest.nil?
      arrest.district_charges.join(', ')
    when !doc_record.nil?
      doc_record.conviction? ? doc_record.conviction : "Not On File"
    when hold_type == 'Temporary Release'
      temp_release_reason.blank? ? 'Unknown' : temp_release_reason
    when !revocation.nil?
      "Probation Revoked: #{revocation.docket.sentence_summary.blank? ? 'Convictions Not On File' : revocation.docket.sentence_summary}"
    else
      ""
    end
  end
  
  def hold_reason_name
    case
    when hold_type.blank?
      ""
    when hold_type == 'Fines/Costs'
      "#{agency}:"
    when !sentence.nil? || !doc_record.nil? || !revocation.nil?
      "Conviction:"
    when ['Temporary Housing', 'Hold for City', 'Hold for Other Agency', 'Temporary Transfer'].include?(hold_type)
      "Agency:"
    when !arrest.nil?
      "Charged:"
    when hold_type == 'Temporary Release'
      "Reason:"
    else
      ""
    end
  end
  
  def hours_remaining
    return nil if hold_type != "Serving Sentence" && hold_type != "Fines/Costs"
    return 0 if cleared_datetime? || hours_total == 0
    return -1 if hours_total < 0
    remaining = hours_total - hours_served - hours_goodtime.round - hours_time_served
    remaining < 0 ? 0 : remaining
  end
  
  def hours_served_between(start_time, stop_time)
    return 0 unless start_time.is_a?(Time) && stop_time.is_a?(Time)
    return 0 if booking_logs.empty?
    hrs = 0
    BookingLog.where(['(start_datetime BETWEEN SYMMETRIC ? AND ?) OR (start_datetime < ? AND (stop_datetime IS NULL OR (stop_datetime BETWEEN SYMMETRIC ? AND ?))',start_time,stop_time,start_time,start_time,stop_time]).all.each do |l|
      hrs += l.hours_served(start_time,stop_time)
    end
    return hrs
  end
  
  def reasons_not_destroyable
    reasons = []
    reasons.push("Has Bonds") unless bonds.empty?
    reasons.push("Temporary Release Not Cleared") if hold_type == 'Temporary Release' && !cleared_datetime?
    reasons.empty? ? nil : reasons.join(', ')
  end
  
  def satisfied?
    return true unless fines? && fines > 0
    bond_total >= fines
  end
  
  def update_hour_counters
    return if booking.booking_logs.empty?
    runtime = Time.now
    unless datetime_counted?
      self.update_attribute(:datetime_counted, 1.hour.ago)
    end
    minutes_since = booking.minutes_served_since(datetime_counted)
    if minutes_since < 30
      self.update_attribute(:datetime_counted, runtime)
      return
    end
    hours_since = (minutes_since / 60.0).round
    self.hours_served += hours_since
    if hold_type != "Fines/Costs" && !deny_goodtime? && booking.good_time? && !doc?
      unless booking.jail.goodtime_hours_per_day <= 0
        multiplier = booking.jail.goodtime_hours_per_day / 24.0
        self.hours_goodtime += (hours_since.to_f * multiplier)
      end
    end
    self.updated_by = 1
    self.datetime_counted = runtime
    self.save(validate: false)
  end
  
  def to_s
    if hold_type.blank?
      "Unknown"
    else
      hold_type
    end
  end
  
  def sentence_time_range
    if booking.nil?
      [hold_datetime, cleared_datetime]
    else
      # sometimes we end up with hold dates that don't agree with booking dates,
      # in this case we want to go with whichever is later.
      datetime = [booking.booking_datetime, hold_datetime].sort.pop
      [datetime, cleared_datetime]
    end
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def check_destroyable
    reasons_not_destroyable.nil?
  end
  
  def check_for_fines
    if hold_type == 'Fines/Costs'
      unless fines > 0
        self.errors.add(:fines, 'must be a number greater than zero!')
        return false
      end
      unless agency?
        self.errors.add(:agency, "must be entered for 'Fines/Costs' holds'")
        return false
      end
      unless hours_total > 0
        self.errors.add(:hours_total, "must be a number greater than zero.")
        return false
      end
    end
    true
  end
  
  def check_for_other_agency
    if hold_type == 'Hold for Other Agency'
      unless agency?
        self.errors.add(:agency, "must be entered if hold type is 'Hold for Other Agency'")
        return false
      end
    end
    true
  end
  
  def check_for_transfer
    status = true
    if hold_type == 'Temporary Transfer' || hold_type == 'Temporary Housing'
      unless agency?
        self.errors.add(:agency, "cannot be blank for Temporary Transfers!")
        status = false
      end
      unless transfer_officer?
        self.errors.add(:transfer_officer, "cannot be blank for Temporary Transfers!")
        status = false
      end
    end
    unless hold_type == 'Temporary Transfer'
      self.other_billable = false
    end
    if cleared_because == 'Transferred'
      unless agency?
        self.errors.add(:agency, "cannot be blank for Transfers!")
        status = false
      end
      unless transfer_officer?
        self.errors.add(:transfer_officer, "cannot be blank for Transfers!")
        status = false
      end
    end
    status
  end
  
  def lookup_contacts
    unless hold_by_id.blank?
      if ofc = Contact.find_by_id(hold_by_id)
        self.hold_by = ofc.sort_name
        self.hold_by_badge = ofc.badge_no
        self.hold_by_unit = ofc.unit
      end
      self.hold_by_id = nil
    end
    unless cleared_by_id.blank?
      if ofc = Contact.find_by_id(cleared_by_id)
        self.cleared_by = ofc.sort_name
        self.cleared_by_badge = ofc.badge_no
        self.cleared_by_unit = ofc.unit
      end
      self.cleared_by_id = nil
    end
    unless transfer_officer_id.blank?
      if ofc = Contact.find_by_id(transfer_officer_id)
        self.transfer_officer = ofc.sort_name
        self.transfer_officer_badge = ofc.badge_no
        self.transfer_officer_unit = ofc.unit
      end
      self.transfer_officer_id = nil
    end
    unless agency_id.blank?
      if ag = Contact.find_by_id(agency_id)
        self.agency = ag.sort_name
      end
      self.agency_id = nil
    end
    true
  end
end

# == Schema Information
#
# Table name: holds
#
#  id                     :integer          not null, primary key
#  booking_id             :integer          not null
#  hold_by                :string(255)      default(""), not null
#  hold_by_unit           :string(255)      default(""), not null
#  cleared_by             :string(255)      default(""), not null
#  cleared_by_unit        :string(255)      default(""), not null
#  remarks                :text             default(""), not null
#  created_by             :integer          default(1)
#  updated_by             :integer          default(1)
#  created_at             :datetime
#  updated_at             :datetime
#  arrest_id              :integer
#  hold_by_badge          :string(255)      default(""), not null
#  cleared_by_badge       :string(255)      default(""), not null
#  doc                    :boolean          default(FALSE), not null
#  probation_id           :integer
#  billing_report_date    :date
#  agency                 :string(255)      default(""), not null
#  transfer_officer       :string(255)      default(""), not null
#  transfer_officer_badge :string(255)      default(""), not null
#  transfer_officer_unit  :string(255)      default(""), not null
#  fines                  :decimal(12, 2)   default(0.0), not null
#  datetime_counted       :datetime
#  doc_billable           :boolean          default(FALSE), not null
#  hours_served           :integer          default(0), not null
#  hours_goodtime         :float            default(0.0), not null
#  sentence_id            :integer
#  transfer_id            :integer
#  explanation            :string(255)      default(""), not null
#  bill_from_date         :date
#  legacy                 :boolean          default(FALSE), not null
#  revocation_id          :integer
#  billed_retroactive     :date
#  hours_total            :integer          default(0), not null
#  hours_time_served      :integer          default(0), not null
#  deny_goodtime          :boolean          default(FALSE), not null
#  other_billable         :boolean          default(FALSE), not null
#  death_sentence         :boolean          default(FALSE), not null
#  hold_datetime          :datetime
#  cleared_datetime       :datetime
#  hold_type              :string(255)      default(""), not null
#  cleared_because        :string(255)      default(""), not null
#  temp_release_reason    :string(255)      default(""), not null
#
# Indexes
#
#  index_holds_on_arrest_id      (arrest_id)
#  index_holds_on_booking_id     (booking_id)
#  index_holds_on_created_by     (created_by)
#  index_holds_on_probation_id   (probation_id)
#  index_holds_on_revocation_id  (revocation_id)
#  index_holds_on_sentence_id    (sentence_id)
#  index_holds_on_transfer_id    (transfer_id)
#  index_holds_on_updated_by     (updated_by)
#
# Foreign Keys
#
#  holds_arrest_id_fk      (arrest_id => arrests.id)
#  holds_booking_id_fk     (booking_id => bookings.id)
#  holds_created_by_fk     (created_by => users.id)
#  holds_probation_id_fk   (probation_id => probations.id)
#  holds_revocation_id_fk  (revocation_id => revocations.id)
#  holds_sentence_id_fk    (sentence_id => sentences.id)
#  holds_transfer_id_fk    (transfer_id => transfers.id)
#  holds_updated_by_fk     (updated_by => users.id)
#
