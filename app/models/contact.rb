class Contact < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'
  has_one :user, inverse_of: :contact, dependent: :destroy
  belongs_to :agency, class_name: "Contact", foreign_key: 'agency_id', inverse_of: :employees
  has_many :employees, class_name: "Contact", foreign_key: 'agency_id', dependent: :nullify
  has_many :meds_as_pharmacy, class_name: "Medication", foreign_key: 'pharmacy_id', dependent: :nullify
  has_many :meds_as_physician, class_name: "Medication", foreign_key: 'physician_id', dependent: :nullify
  has_many :invest_contacts, dependent: :destroy
  has_many :investigations, through: :invest_contacts
  has_many :contact_bondsman_companies, dependent: :destroy
  has_many :bondsmen, through: :contact_bondsman_companies, source: :bondsman, dependent: :destroy
  has_many :bond_companies, through: :contact_bondsman_companies, source: :company, dependent: :destroy

  ## Attributes #############################################
  
  accepts_nested_attributes_for :contact_bondsman_companies, allow_destroy: true, reject_if: ->(c) { c['company_id'].blank? || !Contact.exists?(c['company_id']) }
    
  ## Scopes #############################################
  
  scope :by_name, -> { order :sort_name }
  scope :by_name_and_unit, -> { order :sort_name, :unit }
  scope :by_unit, -> { order 'length(contacts.unit), contacts.unit, contacts.sort_name' }
  scope :is_active, -> { where active: true }
  scope :is_printable, -> { where printable: true }
  scope :for_agency, ->(agency_id) { where(agency_id: agency_id) }
  scope :is_pawn_company, -> { where pawn_co: true, business: true }
  scope :is_bond_company, -> { where bonding_company: true, business: true }
  scope :is_judge, -> { where judge: true, business: false }
  scope :is_officer, -> { where officer: true, business: false }
  scope :is_agency, -> { where officer: true, business: true }
  scope :is_physician, -> { where physician: true, business: false }
  scope :is_pharmacy, -> { where pharmacy: true, business: true }
  scope :is_detective, -> { where detective: true, officer: true, business: false }
  scope :is_bondsman, -> { where bondsman: true, business: false }
  scope :is_individual, -> { where business: false }
  scope :is_business, -> { where business: true }
  
  # filter scopes
  scope :with_contact_id, ->(c_id) { where id: c_id }
  scope :with_address, ->(addr) { AppUtils.like self, [:street, :street2, :city, :state, :zip], addr }
  scope :with_title, ->(ti) { AppUtils.like self, :title, ti }
  scope :with_unit, ->(un) { AppUtils.for_lower self, :unit, un }
  scope :with_badge, ->(ba) { AppUtils.for_lower self, :badge_no, ba }
  scope :with_agency_id, ->(a_id) { where agency_id: a_id }
  scope :with_business, ->(bus) { AppUtils.for_bool self, :business, bus }
  scope :with_active, ->(act) { AppUtils.for_bool self, :active, act }
  scope :with_officer, ->(off) { AppUtils.for_bool self, :officer, off }
  scope :with_detective, ->(det) { AppUtils.for_bool self, :detective, det }
  scope :with_bondsman, ->(bmn) { AppUtils.for_bool self, :bondsman, bmn }
  scope :with_bonding_company, -> (bco) { AppUtils.for_bool self, :bonding_company, bco }
  scope :with_judge, ->(jdg) { AppUtils.for_bool self, :judge, jdg }
  scope :with_pawn_co, ->(pco) { AppUtils.for_bool self, :pawn_co, pco }
  scope :with_physician, ->(phy) { AppUtils.for_bool self, :physician, phy }
  scope :with_pharmacy, ->(pha) { AppUtils.for_bool self, :pharmacy, pha }
  scope :with_notes, ->(nts) { AppUtils.like self, :notes, nts }
  scope :with_email, ->(eml) { AppUtils.like self, [:pri_email, :alt_email], eml }
  scope :with_web_site, ->(wst) { AppUtils.like self, :web_site, wst }
  scope :with_contact_name, ->(fname) { AppUtils.for_person self, fname, simple: true }
  
  ## Callbacks #############################################
  
  before_save :check_creator
  before_validation :fix_name, :fix_unit_and_badge
  before_destroy :check_destroyable

  ## Validations #############################################
  
  validates_presence_of :sort_name
  validates_phone :phone1, :phone2, :phone3, allow_blank: true
  validates_zip :zip, allow_blank: true
  validates_url :web_site, allow_blank: true
  validates_contact :agency_id, allow_nil: true

  ## Constants #############################################
  
  PhoneType = ["Alternate", "Cell", "Emergency", "FAX", "Home", "Pager", "Primary", "Toll Free", "Unlisted", "Voice Mail", "Work", "Other"].freeze
  
  ## Class Methods #############################################
  
  def self.agencies_for_select(allow_inactive=false)
    a = is_agency.by_name
    unless allow_inactive
      a = a.is_active
    end
    a.all.collect{|a| [a.sort_name, a.id]}
  end
  
  def self.base_perms
    :view_contact
  end
  
  def self.bond_companies_for_select
    is_bond_company.by_name.all.collect{|p| [p.sort_name, p.id]}
  end
  
  def self.bondsmen_for_select(allow_inactive=false)
    b = is_bondsman.by_name
    unless allow_inactive
      b = b.is_active
    end
    b.all.collect{|o| [o.sort_name,o.id]}
  end
  
  def self.company_names_for_select(allow_inactive=false)
    b = is_business.by_name
    unless allow_inactive
      b = b.is_active
    end
    b.all.collect{|a| [a.sort_name, a.sort_name]}
  end
  
  def self.companies_for_select(allow_inactive=false,limit_printable=false)
    b = is_business.by_name
    unless allow_inactive
      b = b.is_active
    end
    if limit_printable
      b = b.is_printable
    end
    b.all.collect{|a| [a.sort_name, a.id]}
  end
  
  def self.contacts_for_select(allow_inactive=false)
    i = is_individual.by_name_and_unit
    unless allow_inactive
      i = i.is_active
    end
    i.all.collect{|o| ["#{o.sort_name} #{o.unit}", o.id]}
  end
  
  def self.contacts_and_companies_for_select(allow_inactive=false)
    c = by_name_and_unit
    unless allow_inactive
      c = c.is_active
    end
    c.all.collect{|o| ["#{o.sort_name} #{o.unit}", o.id]}
  end
  
  def self.desc
    "Contacts (rolodex)"
  end

  def self.detective_users_for_select
    is_active.is_detective.by_name_and_unit.joins(:user).all
          .collect{|o| ["#{o.sort_name} #{o.unit}",o.user.id]}
  end
  
  def self.detectives_for_select(allow_inactive=false)
    d = is_detective.by_name_and_unit
    unless allow_inactive
      d = d.is_active
    end
    d.all.collect{|o| ["#{o.sort_name} #{o.unit}",o.id]}
  end
  
  def self.judges_for_select(allow_inactive=false)
    j = is_judge.by_name
    unless allow_inactive
      j = j.is_active
    end
    j.all.collect{|j| [j.sort_name, j.sort_name]}
  end
  
  def self.local_agencies_for_select(allow_inactive=false)
    a = nil
    unless SiteConfig.local_agencies.blank?
      locals = SiteConfig.local_agencies.split(',')
      a = is_agency.by_name
      unless allow_inactive
        a = a.is_active
      end
      a.where(id: locals).all.collect{|c| [c.sort_name, c.id]}
    end
    if a.nil? || a.empty?
      agencies_for_select(allow_inactive)
    else
      a
    end
  end
  
  def self.officers_for_select(allow_inactive=false)
    o = is_officer.by_name_and_unit
    unless allow_inactive
      o = o.is_active
    end
    o.all.collect{|o| ["#{o.sort_name} #{o.unit}",o.id]}
  end
  
  def self.pawn_companies_for_select(allow_inactive=false)
    cos = is_pawn_company.by_name
    unless allow_inactive
      cos = cos.is_active
    end
    cos.all.collect{|p| [p.sort_name, p.sort_name]}
  end
  
  def self.physicians_for_select(allow_inactive=false)
    p = is_physician.by_name
    unless allow_inactive
      p = p.is_active
    end
    p.all.collect.collect{|o| [o.sort_name, o.id]}
  end

  def self.pharmacies_for_select(allow_inactive=false)
    p = is_pharmacy.by_name
    unless allow_inactive
      p = p.is_active
    end
    p.all.collect{|a| [a.sort_name, a.id]}
  end
  
  # process query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_contact_id(query[:id]) unless query[:id].blank?
    result = result.with_contact_name(query[:fullname]) unless query[:fullname].blank?
    result = result.with_address(query[:address]) unless query[:address].blank?
    result = result.with_title(query[:title]) unless query[:title].blank?
    result = result.with_unit(query[:unit]) unless query[:unit].blank?
    result = result.with_badge_no(query[:badge_no]) unless query[:badge_no].blank?
    result = result.with_agency_id(query[:agency_id]) unless query[:agency_id].blank?
    result = result.with_business(query[:business]) unless query[:business].blank?
    result = result.with_active(query[:active]) unless query[:active].blank?
    result = result.with_officer(query[:officer]) unless query[:officer].blank?
    result = result.with_detective(query[:detective]) unless query[:detective].blank?
    result = result.with_bondsman(query[:bondsman]) unless query[:bondsman].blank?
    result = result.with_bonding_company(query[:bonding_company]) unless query[:bonding_company].blank?
    result = result.wtih_judge(query[:judge]) unless query[:judge].blank?
    result = result.with_pawn_co(query[:pawn_co]) unless query[:pawn_co].blank?
    result = result.with_physician(query[:physician]) unless query[:physician].blank?
    result = result.with_pharmacy(query[:pharmacy]) unless query[:pharmacy].blank?
    result = result.with_notes(query[:notes]) unless query[:notes].blank?
    result = result.with_email(query[:email]) unless query[:email].blank?
    result = result.with_web_site(query[:web_site]) unless query[:web_site].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def address
    [street, street2, address_line3].reject{|a| a.blank?}.join(', ')
  end
  
  def address_line1
    street || ''
  end
  
  def address_line2
    street2 || ''
  end
  
  def address_line3
    txt = ""
    # don't return anything if only state exists
    if city? || zip?
      if city?
        txt << city
      end
      if state?
        unless txt.empty?
          txt << ", "
        end
        txt << state
      end
      if zip?
        unless txt.empty?
          txt << " "
        end
        txt << zip
      end
    end
    txt
  end
  
  def bond_companies_string
    return "" unless bondsman?
    txt = "[".concat(bond_companies.collect{|c| c.sort_name}.join("] [")).concat("]")
    if txt == "[]"
      txt = notes
    end
    txt
  end
  
  def reasons_not_destroyable
    reasons = []
    reasons.push("User Profile") unless user.nil?
    reasons.push("Linked to Investigation") unless investigations.empty?
    reasons.push("Has Linked Employees") unless employees.empty?
    reasons.empty? ? nil : reasons.join(', ')
  end
  
  # returns name in (given family suffix) order
  def display_name
    if business?
      sort_name
    else
      AppUtils.parse_name(sort_name,true)
    end
  end
  
  # returns name (first middle last suffix) with phone1 if present
  def display_name_with_phone
    if phone1.blank?
      display_name
    else
      "#{display} (#{phone1})"
    end
  end
  
  def phones
    txt = []
    if phone1?
      txt.push("#{phone1_type.blank? ? 'Phone' : phone1_type}: #{phone1}#{phone1_unlisted? ? ' (UNL)' : ''}")
    end
    if phone2?
      txt.push("#{phone2_type.blank? ? 'Phone' : phone2_type}: #{phone1}#{phone2_unlisted? ? ' (UNL)' : ''}")
    end
    if phone3?
      txt.push("#{phone3_type.blank? ? 'Phone' : phone3_type}: #{phone1}#{phone3_unlisted? ? ' (UNL)' : ''}")
    end
    txt.join(", ")
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def check_destroyable
    reasons_not_destroyable.nil?
  end
  
  # for personal names, parse entered text and rearrange to (last, first middle suffix) order
  def fix_name
    unless business?
      self.sort_name = AppUtils.parse_name(sort_name)
    end
    true
  end
  
  def fix_unit_and_badge
    unless unit.nil?
      self.unit = unit.gsub(/\s/,'')
    end
    unless badge_no.nil?
      self.badge_no = badge_no.gsub(/\s/,'')
    end
    true
  end
end

# == Schema Information
#
# Table name: contacts
#
#  id              :integer          not null, primary key
#  title           :string(255)      default(""), not null
#  unit            :string(255)      default(""), not null
#  street          :string(255)      default(""), not null
#  city            :string(255)      default(""), not null
#  zip             :string(255)      default(""), not null
#  pri_email       :string(255)      default(""), not null
#  alt_email       :string(255)      default(""), not null
#  web_site        :string(255)      default(""), not null
#  notes           :string(255)      default(""), not null
#  created_at      :datetime
#  updated_at      :datetime
#  officer         :boolean          default(FALSE), not null
#  street2         :string(255)      default(""), not null
#  created_by      :integer          default(1)
#  updated_by      :integer          default(1)
#  badge_no        :string(255)      default(""), not null
#  active          :boolean          default(FALSE), not null
#  detective       :boolean          default(FALSE), not null
#  physician       :boolean          default(FALSE), not null
#  pharmacy        :boolean          default(FALSE), not null
#  legacy          :boolean          default(FALSE), not null
#  business        :boolean          default(FALSE), not null
#  sort_name       :string(255)      default(""), not null
#  agency_id       :integer
#  printable       :boolean          default(TRUE), not null
#  state           :string(255)      default(""), not null
#  phone1          :string(255)      default(""), not null
#  phone2          :string(255)      default(""), not null
#  phone3          :string(255)      default(""), not null
#  phone1_unlisted :boolean          default(FALSE), not null
#  phone2_unlisted :boolean          default(FALSE), not null
#  phone3_unlisted :boolean          default(FALSE), not null
#  bondsman        :boolean          default(FALSE), not null
#  remarks         :text             default(""), not null
#  phone1_type     :string(255)      default(""), not null
#  phone2_type     :string(255)      default(""), not null
#  phone3_type     :string(255)      default(""), not null
#  judge           :boolean          default(FALSE), not null
#  pawn_co         :boolean          default(FALSE), not null
#  bonding_company :boolean          default(FALSE), not null
#
# Indexes
#
#  index_contacts_on_agency_id   (agency_id)
#  index_contacts_on_created_by  (created_by)
#  index_contacts_on_sort_name   (sort_name)
#  index_contacts_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  contacts_agency_id_fk   (agency_id => contacts.id)
#  contacts_created_by_fk  (created_by => users.id)
#  contacts_updated_by_fk  (updated_by => users.id)
#
