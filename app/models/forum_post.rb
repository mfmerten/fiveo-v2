# adapted from "forem" engine (https://github.com/radar/forem)
#
# Copyright 2011 Ryan Bigg, Philip Arndt and Josh Adams
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
class ForumPost < ActiveRecord::Base
  belongs_to :forum_topic, inverse_of: :forum_posts                        # constraint
  belongs_to :user, inverse_of: :forum_posts                               # constraint
  belongs_to :reply_to, class_name: "ForumPost", inverse_of: :replies      # constraint
  has_many :replies, class_name: "ForumPost", foreign_key: "reply_to_id", dependent: :nullify  # constraint
  
  delegate :forum, to: :forum_topic
  
  scope :by_created_at, -> { order :created_at }
  scope :visible, -> { joins(:forum_topic).where(forum_topics: { hidden: false }) }
  
  after_create :set_topic_last_post_at
  after_create :subscribe_replier, if: :user_auto_subscribe?

  after_save :message_topic_subscribers, unless: :notified?
  
  validates_presence_of :text

  ## Class Methods #############################################
  
  def self.base_perms
    :view_forum
  end
  
  def self.desc
    'Discussion Forum Posts'
  end
  
  ## Instance Methods #############################################
  
  def owner_or_admin?(other_user)
    user == other_user || other_user.is_forum_admin?
  end
  
  def user_auto_subscribe?
    user && user.respond_to?(:forum_auto_subscribe) && user.forum_auto_subscribe?
  end
  
  protected
  
  ## Protected Instance Methods #############################################
  
  def message_topic_subscribers
    forum_topic.forum_subscriptions.includes(:subscriber).find_each do |subscription|
      if subscription.subscriber != user
        subscription.send_notification(id)
      end
    end
    update_attribute(:notified, true)
  end
  
  def set_topic_last_post_at
    forum_topic.update_attribute(:last_post_at, created_at)
  end

  def subscribe_replier
    if forum_topic && user
      forum_topic.subscribe_user(user.id)
    end
  end
end

# == Schema Information
#
# Table name: forum_posts
#
#  id             :integer          not null, primary key
#  forum_topic_id :integer          not null
#  user_id        :integer          not null
#  reply_to_id    :integer
#  text           :text             default(""), not null
#  notified       :boolean          default(FALSE), not null
#  created_at     :datetime
#  updated_at     :datetime
#
# Indexes
#
#  index_forum_posts_on_forum_topic_id  (forum_topic_id)
#  index_forum_posts_on_reply_to_id     (reply_to_id)
#  index_forum_posts_on_user_id         (user_id)
#
# Foreign Keys
#
#  forum_posts_forum_topic_id_fk  (forum_topic_id => forum_topics.id)
#  forum_posts_reply_to_id_fk     (reply_to_id => forum_posts.id)
#  forum_posts_user_id_fk         (user_id => users.id)
#
