class Case < ActiveRecord::Base
  
  ## Class Methods #############################################
  
  def self.desc
    "Case Number sequence table"
  end

  # Generates the next case number in sequence.
  # * If records exist, the current year is first compared to the year
  #   prefix of the first record.  If different, the table is cleared.
  # * If no records exist, the first record is created and the case number
  #   is generated using the current year and the record id (1).
  # * If records already exist, a record is appended and the case number is
  #   generated using the prefix from the first record and the id from the
  #   new record.
  def self.get_next_case_number
    if Case.count > 0
      mycentury = first.assigned.slice(0,4)
      if mycentury < Date.today.year.to_s
        sql = Case.connection
        sql.execute "TRUNCATE TABLE cases RESTART IDENTITY"
        mycentury = Date.today.year.to_s
      end
    else
      mycentury = Date.today.year.to_s
    end
    mycase = create(assigned: nil)
    caseno = "#{mycentury}#{mycase.id.to_s.rjust(5,'0')}"
    mycase.update_attribute(:assigned, caseno)
    return caseno
  end

  # returns true if specified case number is valid (used somewhere in
  # a record, not necessarily in the cases table)
  def self.valid_case(case_no)
    models = []
    Dir.chdir(File.join(Rails.root, "app/models")) do 
      models = Dir["*.rb"]
    end
    @items = []
    models.each do |m|
      model_name = m.sub(/\.rb$/,'')
      class_name = model_name.camelize
      next if class_name == 'InvestCase'
      if (class_name.constantize.respond_to?('column_names') && class_name.constantize.column_names.include?('case_no')) || model_name == 'investigation'
        class_name.constantize.where(['case_no = ?',case_no]).all.each do |c|
          next if c.respond_to?('private') && c.private?
          return true
        end
      end
    end
    return false
  end
end

# == Schema Information
#
# Table name: cases
#
#  id         :integer          not null, primary key
#  assigned   :string(255)
#  created_at :datetime
#  updated_at :datetime
#
