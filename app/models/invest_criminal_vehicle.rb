class InvestCriminalVehicle < ActiveRecord::Base
  belongs_to :invest_criminal, inverse_of: :invest_criminal_vehicles       # constraint
  belongs_to :invest_vehicle, inverse_of: :invest_criminal_vehicles        # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestCriminal Vehicles'
  end
end

# == Schema Information
#
# Table name: invest_criminal_vehicles
#
#  id                 :integer          not null, primary key
#  invest_criminal_id :integer          not null
#  invest_vehicle_id  :integer          not null
#  created_at         :datetime
#  updated_at         :datetime
#
# Indexes
#
#  index_invest_criminal_vehicles_on_invest_criminal_id  (invest_criminal_id)
#  index_invest_criminal_vehicles_on_invest_vehicle_id   (invest_vehicle_id)
#
# Foreign Keys
#
#  invest_criminal_vehicles_invest_criminal_id_fk  (invest_criminal_id => invest_criminals.id)
#  invest_criminal_vehicles_invest_vehicle_id_fk   (invest_vehicle_id => invest_vehicles.id)
#
