# adapted from "forem" engine (https://github.com/radar/forem)
#
# Copyright 2011 Ryan Bigg, Philip Arndt and Josh Adams
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
class Forum < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :forum_category, inverse_of: :forums                       # constraint
  has_many :forum_views, dependent: :destroy                            # constraint
  has_many :forum_topics, dependent: :destroy                           # constraint
  has_many :forum_posts, through: :forum_topics                         # constrained through forum_topics
  
  default_scope -> { order :name }
  
  scope :uncategorized, -> { where forum_category_id: nil }
  
  validates_presence_of :name, :description
  validates_uniqueness_of :name
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_forum
  end
  
  def self.desc
    'Discussion Forums'
  end
  
  def self.forums_for_select
    all.collect{|f| [f.name,f.id]}
  end
  
  ## Instance Methods #############################################
  
  def last_visible_post(user)
    if user && user.is_forum_admin?
      forum_posts.last
    else
      forum_posts.visible.last
    end
  end
  
  def register_view_by(user)
    return unless user
    
    view = forum_views.find_or_create_by(user_id: user.id)
    view.increment!("count")
    increment!(:views_count)
    
    # update current_viewed_at if more than 15 minutes ago
    if view.current_viewed_at.nil?
      view.past_viewed_at = view.current_viewed_at = Time.now
    end
    
    # Update the current_viewed_at if it is BEFORE 15 minutes ago.
    if view.current_viewed_at < 15.minutes.ago
      view.past_viewed_at = view.current_viewed_at
      view.current_viewed_at = Time.now
      view.save
    end
  end

  def view_for(user)
    forum_views.find_by(user_id: user.id)
  end
end

# == Schema Information
#
# Table name: forums
#
#  id                :integer          not null, primary key
#  name              :string(255)      default(""), not null
#  description       :text             default(""), not null
#  forum_category_id :integer
#  views_count       :integer          default(0), not null
#  created_by        :integer          default(1)
#  updated_by        :integer          default(1)
#  created_at        :datetime
#  updated_at        :datetime
#
# Indexes
#
#  index_forums_on_created_by         (created_by)
#  index_forums_on_forum_category_id  (forum_category_id)
#  index_forums_on_updated_by         (updated_by)
#
# Foreign Keys
#
#  forums_created_by_fk         (created_by => users.id)
#  forums_forum_category_id_fk  (forum_category_id => forum_categories.id)
#  forums_updated_by_fk         (updated_by => users.id)
#
