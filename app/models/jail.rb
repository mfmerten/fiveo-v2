class Jail < ActiveRecord::Base
  
  ## Associations #############################################
  
  has_many :bookings, dependent: :destroy                               # constraint
  has_many :visitors, through: :bookings                                # constrained through bookings
  has_many :transfers, through: :bookings                               # constrained through bookings
  has_many :holds, through: :bookings                                   # constrained through bookings
  has_many :absents, dependent: :destroy                                # constraint
  has_many :commissaries, dependent: :destroy                           # constraint
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  has_many :commissary_items, dependent: :destroy                       # constraint
  has_many :cells, class_name: 'JailCell', dependent: :destroy          # constraint
  has_many :absent_types, class_name: 'JailAbsentType', dependent: :destroy  # constraint
  
  ## Attributes #############################################
  
  accepts_nested_attributes_for :cells, allow_destroy: true, reject_if: ->(c) { c['name'].blank?}
  accepts_nested_attributes_for :absent_types, allow_destroy: true, reject_if: ->(t) { t['name'].blank?}
  
  ## Scopes #############################################
  
  scope :by_name, -> { order :name }
  
  ## Callbacks #############################################
  
  before_save :check_creator
  before_destroy :check_destroyable
  
  ## Validations ###########################################
  
  validates_numericality_of :goodtime_hours_per_day, greater_than_or_equal_to: 0, less_than: 10 ** 10
  
  ## Class Constants #############################################
  
  NotifyFrequency = ["Daily","Weekly","Semi-Monthly","Monthly"].freeze
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_jail
  end
  
  def self.desc
    "Jail Facilities"
  end
  
  ## Instance Methods #############################################
  
  def address
    "#{street}, #{city}, #{state} #{zip}"
  end
  
  def admin_permission
    "admin_jail_#{permset.to_s}".to_sym
  end
  
  def headcount(date, status = nil)
    date = Date.today - 1.day if date.blank?
    status = 'ALL' if status.blank?
    return [] if !date.is_a?(Date) || date >= Date.today
    return [] unless ['Temporary Release', 'DOC', 'DOC Billable', 'City', 'Parish', 'Other', 'ALL', 'Not Billable'].include?(status)
    list = bookings.active_on(date).by_name.includes(:holds).all
    if status == 'ALL'
      other = []
      doc = []
      doc_bill = []
      city = []
      parish = []
      temp = []
      nb = []
      err = []
      list.each do |b|
        stat = b.billing_status(date)
        case stat
        when 'Error'
          err.push(b)
        when 'DOC'
          doc.push(b)
        when 'DOC Billable'
          doc_bill.push(b)
        when 'City'
          city.push(b)
        when 'Parish'
          parish.push(b)
        when 'Temporary Release'
          temp.push(b)
        when 'Not Billable'
          nb.push(b)
        else
          other.push(b)
        end
      end
      return [temp, doc, doc_bill, city, parish, other, nb]
    elsif status == 'Other'
      list.reject{|b| ['Error', 'DOC', 'DOC Billable', 'City', 'Parish','Temporary Release', 'Not Billable'].include?(b.billing_status(date)) }
    else
      list.reject{|b| b.billing_status(date) != status }
    end
  end
  
  def problems
    bookings.active.collect{|b| [b, b.problems]}.reject{|b| b[1].blank?}
  end
  
  def reasons_not_destroyable
    reasons = []
    reasons.push("Has Bookings") unless bookings.empty?
    reasons.push("Has Absents") unless absents.empty?
    reasons.push("Has Commissary Transactions") unless commissaries.empty?
    reasons.empty? ? nil : reasons.join(', ')
  end
  
  def update_permission
    "update_jail_#{permset.to_s}".to_sym
  end
  
  def valid_cells_for_select
    cells.collect{|c| [c.name,c.name]}.sort{|a,b| a[0] <=> b[0]}
  end
  
  def valid_absent_types_for_select
    absent_types.collect{|t| [t.name,t.name]}.sort{|a,b| a[0] <=> b[0]}
  end
  
  def view_permission
    "view_jail_#{permset.to_s}".to_sym
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def check_destroyable
    reasons_not_destroyable.nil?
  end
end

# == Schema Information
#
# Table name: jails
#
#  id                     :integer          not null, primary key
#  name                   :string(255)      default(""), not null
#  acronym                :string(255)      default(""), not null
#  street                 :string(255)      default(""), not null
#  city                   :string(255)      default(""), not null
#  state                  :string(255)      default(""), not null
#  zip                    :string(255)      default(""), not null
#  phone                  :string(255)      default(""), not null
#  fax                    :string(255)      default(""), not null
#  juvenile               :boolean          default(FALSE), not null
#  male                   :boolean          default(FALSE), not null
#  female                 :boolean          default(FALSE), not null
#  permset                :integer          default(1), not null
#  goodtime_by_default    :boolean          default(FALSE), not null
#  goodtime_hours_per_day :decimal(12, 2)   default(0.0), not null
#  afis_enabled           :boolean          default(FALSE), not null
#  remarks                :text             default(""), not null
#  created_by             :integer          default(1)
#  updated_by             :integer          default(1)
#  created_at             :datetime
#  updated_at             :datetime
#  notify_frequency       :string(255)      default(""), not null
#
# Indexes
#
#  index_jails_on_created_by  (created_by)
#  index_jails_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  jails_created_by_fk  (created_by => users.id)
#  jails_updated_by_fk  (updated_by => users.id)
#
