class HoldBlocker < ActiveRecord::Base
  belongs_to :blocker, class_name: 'Hold', foreign_key: 'blocker_id', inverse_of: :hold_blocks  # constraint
  belongs_to :hold, inverse_of: :hold_blockers                                                  # constraint

  ## Class Methods #############################################
  
  def self.desc
    "Consecutive Sentence Hold Blocks"
  end
end

# == Schema Information
#
# Table name: hold_blockers
#
#  id         :integer          not null, primary key
#  blocker_id :integer          not null
#  hold_id    :integer          not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_hold_blockers_on_blocker_id  (blocker_id)
#  index_hold_blockers_on_hold_id     (hold_id)
#
# Foreign Keys
#
#  hold_blockers_blocker_id_fk  (blocker_id => holds.id)
#  hold_blockers_hold_id_fk     (hold_id => holds.id)
#
