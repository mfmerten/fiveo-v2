# adapted from "forem" engine (https://github.com/radar/forem)
#
# Copyright 2011 Ryan Bigg, Philip Arndt and Josh Adams
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
class ForumSubscription < ActiveRecord::Base
  belongs_to :forum_topic, inverse_of: :forum_subscriptions                     # constraint
  belongs_to :subscriber, class_name: 'User', inverse_of: :forum_subscriptions  # constraint

  validates_presence_of :subscriber_id

  ## Class Methods #############################################
  
  def self.base_perms
    :view_forum
  end
  
  def self.desc
    'Discussion Forum Subscriptions'
  end
  
  ## Instance Methods #############################################
  
  # modified to use system messages instead of email
  def send_notification(post_id)
    unless forum_topic.hidden? && !subscriber.is_forum_admin?
      if post = ForumPost.find_by_id(post_id)
        txt = AutomatedMessagesController.new.notify(post, 'updated')
        Message.create(receiver_id: subscriber.id, sender_id: 1, subject: "Subscribed Topic (#{post.forum_topic}) Has a New Reply", body: txt)
      end
    end
  end
end

# == Schema Information
#
# Table name: forum_subscriptions
#
#  id             :integer          not null, primary key
#  subscriber_id  :integer          not null
#  forum_topic_id :integer          not null
#  created_at     :datetime
#  updated_at     :datetime
#
# Indexes
#
#  index_forum_subscriptions_on_forum_topic_id  (forum_topic_id)
#  index_forum_subscriptions_on_subscriber_id   (subscriber_id)
#
# Foreign Keys
#
#  forum_subscriptions_forum_topic_id_fk  (forum_topic_id => forum_topics.id)
#  forum_subscriptions_subscriber_id_fk   (subscriber_id => users.id)
#
