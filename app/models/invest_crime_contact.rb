class InvestCrimeContact < ActiveRecord::Base
  belongs_to :invest_crime, inverse_of: :invest_crime_contacts             # constraint
  belongs_to :invest_contact, inverse_of: :invest_crime_contacts           # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestCrime Contacts'
  end
end

# == Schema Information
#
# Table name: invest_crime_contacts
#
#  id                :integer          not null, primary key
#  invest_crime_id   :integer          not null
#  invest_contact_id :integer          not null
#  created_at        :datetime
#  updated_at        :datetime
#
# Indexes
#
#  index_invest_crime_contacts_on_invest_contact_id  (invest_contact_id)
#  index_invest_crime_contacts_on_invest_crime_id    (invest_crime_id)
#
# Foreign Keys
#
#  invest_crime_contacts_invest_contact_id_fk  (invest_contact_id => invest_contacts.id)
#  invest_crime_contacts_invest_crime_id_fk    (invest_crime_id => invest_crimes.id)
#
