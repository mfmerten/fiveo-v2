class ArrestOfficer < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :arrest, inverse_of: :officers, counter_cache: true
  
  ## Attributes #############################################
  
  attr_accessor :officer_id
  
  ## Callbacks #############################################
  
  before_validation :update_officer
  
  ## Validations #############################################
  
  validates_presence_of :officer
  
  ## Class Methods #############################################
  
  def self.desc
    "Arresting Officers"
  end
  
  ## Instance Methods #############################################
  
  def to_s
    "#{officer_unit? ? officer_unit + ' ' : ''}#{officer}"
  end
  
  def update_officer
    unless officer_id.blank?
      if ofc = Contact.find_by_id(officer_id)
        self.officer = ofc.sort_name
        self.officer_badge = ofc.badge_no
        self.officer_unit = ofc.unit
      end
      self.officer_id = nil
    end
    true
  end
end

# == Schema Information
#
# Table name: arrest_officers
#
#  id            :integer          not null, primary key
#  arrest_id     :integer          not null
#  officer       :string(255)      default(""), not null
#  officer_unit  :string(255)      default(""), not null
#  officer_badge :string(255)      default(""), not null
#  created_at    :datetime
#  updated_at    :datetime
#
# Indexes
#
#  index_arrest_officers_on_arrest_id  (arrest_id)
#
# Foreign Keys
#
#  arrest_officers_arrest_id_fk  (arrest_id => arrests.id)
#
