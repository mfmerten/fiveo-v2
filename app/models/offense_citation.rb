class OffenseCitation < ActiveRecord::Base
  belongs_to :offense, inverse_of: :offense_citations                      # constraint
  belongs_to :citation, inverse_of: :offense_citations                     # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'Offense Citations'
  end
end

# == Schema Information
#
# Table name: offense_citations
#
#  id          :integer          not null, primary key
#  offense_id  :integer          not null
#  citation_id :integer          not null
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_offense_citations_on_citation_id  (citation_id)
#  index_offense_citations_on_offense_id   (offense_id)
#
# Foreign Keys
#
#  offense_citations_citation_id_fk  (citation_id => citations.id)
#  offense_citations_offense_id_fk   (offense_id => offenses.id)
#
