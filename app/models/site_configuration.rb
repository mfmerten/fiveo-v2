# provides a mechanism for updating default and site configuration files
class SiteConfiguration < ActiveRecord::Base
  belongs_to :ticket_admin, class_name: 'User', foreign_key: 'support_ticket_admin_id' # constraint
  
  before_validation :missing_to_default
  
  validates_presence_of :agency, :agency_street, :agency_city, :agency_state,
    :agency_zip, :agency_acronym, :motto, :ceo, :ceo_title,
    :header_title, :header_subtitle1, :footer_text, :site_email, :colorscheme,
    :jurisdiction_title, :court_title, :prosecutor_title, :chief_justice_title,
    :court_clerk_name, :court_clerk_title
  validates_numericality_of :support_ticket_admin_id, :message_log_retention,
    :activity_retention, :session_life, :session_active, :session_max,
    :message_retention, :adult_age, only_integer: true, greater_than_or_equal_to: 0
  validates_format_of :pdf_header_bg, :pdf_header_txt, :pdf_section_bg, :pdf_section_txt,
    :pdf_em_bg, :pdf_em_txt, :pdf_row_odd, :pdf_row_even, :pdf_heading_txt, :pdf_heading_bg,
    with: /\A[a-fA-F0-9]{6}\Z/
  validates_format_of :agency_acronym, with: /\A[A-Z]+[A-Z0-9]*\Z/,
    message: "must begin with a capital letter and contain only capital letters and numbers. It cannot contain spaces or punctuation."
  validates_numericality_of :mileage_fee, :noservice_fee, greater_than_or_equal_to: 0, less_than: 10 ** 10
  validates_time_string :dispatch_shift1_start, :dispatch_shift1_stop, :dispatch_shift2_start,
    :dispatch_shift2_stop, :dispatch_shift3_start, :dispatch_shift3_stop, :patrol_shift1_start,
    :patrol_shift1_stop, :patrol_shift2_start, :patrol_shift2_stop, :patrol_shift3_start,
    :patrol_shift3_stop, :patrol_shift4_start, :patrol_shift4_stop, :patrol_shift5_start,
    :patrol_shift5_stop, :patrol_shift6_start, :patrol_shift6_stop, allow_blank: true
  
  # don't include this in databases index
  def self.desc
    nil
  end
  
  def self.load_configuration(site=true)
    if site
      last
    else
      first
    end
  end
  
  def self.base_perms
    :admin
  end
  
  def districts
    district_values.gsub(/\s/,'').split(',')
  end
  
  def zones
    zone_values.gsub(/\s/,'').split(',')
  end
  
  def wards
    ward_values.gsub(/\s/,'').split(',')
  end
  
  private
  
  #some fields automatically get default values when nil
  def missing_to_default
    default_attributes = self.class.load_configuration(false)
    [:pdf_header_bg, :pdf_header_txt, :pdf_section_bg, :pdf_section_txt, :pdf_em_bg,
      :pdf_em_txt, :pdf_row_odd, :pdf_row_even, :pdf_heading_txt, :pdf_heading_bg, :colorscheme].each do |field|
      self.send("#{field}=", default_attributes.send(field)) if self.send(field).blank?
    end
    true
  end
end

# == Schema Information
#
# Table name: site_configurations
#
#  id                         :integer          not null, primary key
#  message_log_retention      :integer          default(90)
#  activity_retention         :integer          default(90)
#  session_life               :integer          default(720)
#  session_active             :integer          default(15)
#  session_max                :integer          default(960)
#  colorscheme                :string(255)      default("blue_brown")
#  show_history               :boolean          default(FALSE)
#  allow_user_photos          :boolean          default(TRUE)
#  user_update_contact        :boolean          default(TRUE)
#  mileage_fee                :decimal(12, 2)   default(0.88)
#  noservice_fee              :decimal(12, 2)   default(5.0)
#  agency                     :string(255)      default("FiveO")
#  agency_acronym             :string(255)      default("FiveO")
#  motto                      :string(255)      default("Law Enforcement Software")
#  ceo                        :string(255)      default("Michael Merten")
#  ceo_title                  :string(255)      default("Midlake Technical Services")
#  local_zips                 :string(255)
#  local_agencies             :string(255)
#  site_email                 :string(255)
#  header_title               :string(255)      default("FiveO")
#  header_subtitle1           :string(255)      default("Midlake Technical Services")
#  header_subtitle2           :string(255)      default("Michael Merten, Owner")
#  header_subtitle3           :string(255)      default("http://midlaketech.com - mike@midlaketech.com")
#  footer_text                :string(255)      default("Law Enforcement Software")
#  pdf_header_bg              :string(255)      default("ddddff")
#  pdf_header_txt             :string(255)      default("000099")
#  pdf_section_bg             :string(255)      default("ddddff")
#  pdf_section_txt            :string(255)      default("000099")
#  pdf_em_bg                  :string(255)      default("ffdddd")
#  pdf_em_txt                 :string(255)      default("990000")
#  pdf_row_odd                :string(255)      default("ffffff")
#  pdf_row_even               :string(255)      default("eeeeee")
#  dispatch_shift1_start      :string(255)      default("05:30")
#  dispatch_shift1_stop       :string(255)      default("17:30")
#  dispatch_shift2_start      :string(255)      default("17:30")
#  dispatch_shift2_stop       :string(255)      default("05:30")
#  dispatch_shift3_start      :string(255)
#  dispatch_shift3_stop       :string(255)
#  probation_fund1            :string(255)      default("Fines")
#  probation_fund2            :string(255)      default("Costs")
#  probation_fund3            :string(255)      default("Probation Fees")
#  probation_fund4            :string(255)      default("IDF")
#  probation_fund5            :string(255)      default("DARE")
#  probation_fund6            :string(255)      default("Restitution")
#  probation_fund7            :string(255)      default("DA Fees")
#  probation_fund8            :string(255)
#  probation_fund9            :string(255)
#  paper_pay_type1            :string(255)      default("Cash")
#  paper_pay_type2            :string(255)      default("Check")
#  paper_pay_type3            :string(255)      default("Money Order")
#  paper_pay_type4            :string(255)      default("Credit Card")
#  paper_pay_type5            :string(255)
#  patrol_shift1_start        :string(255)      default("06:00")
#  patrol_shift1_stop         :string(255)      default("18:00")
#  patrol_shift2_start        :string(255)      default("18:00")
#  patrol_shift2_stop         :string(255)      default("06:00")
#  patrol_shift3_start        :string(255)
#  patrol_shift3_stop         :string(255)
#  patrol_shift1_supervisor   :string(255)
#  patrol_shift2_supervisor   :string(255)
#  patrol_shift3_supervisor   :string(255)
#  patrol_shift4_start        :string(255)
#  patrol_shift4_stop         :string(255)
#  patrol_shift5_start        :string(255)
#  patrol_shift5_stop         :string(255)
#  patrol_shift6_start        :string(255)
#  patrol_shift6_stop         :string(255)
#  patrol_shift4_supervisor   :string(255)
#  patrol_shift5_supervisor   :string(255)
#  patrol_shift6_supervisor   :string(255)
#  created_at                 :datetime
#  updated_at                 :datetime
#  pdf_heading_bg             :string(255)      default("dddddd")
#  pdf_heading_txt            :string(255)      default("000000")
#  afis_ori                   :string(255)
#  afis_agency                :string(255)
#  afis_street                :string(255)
#  afis_city                  :string(255)
#  afis_state                 :string(255)
#  afis_zip                   :string(255)
#  agency_street              :string(255)
#  agency_city                :string(255)
#  agency_state               :string(255)
#  agency_zip                 :string(255)
#  enable_internal_commissary :boolean          default(FALSE)
#  enable_cashless_systems    :boolean          default(FALSE)
#  csi_ftp_user               :string(255)
#  csi_ftp_password           :string(255)
#  csi_ftp_url                :string(255)
#  csi_ftp_filename           :string(255)
#  csi_provide_ssn            :boolean          default(FALSE)
#  csi_provide_dob            :boolean          default(FALSE)
#  csi_provide_race           :boolean          default(FALSE)
#  csi_provide_sex            :boolean          default(FALSE)
#  csi_provide_addr           :boolean          default(FALSE)
#  message_policy             :text
#  message_retention          :integer          default(0)
#  jurisdiction_title         :string(255)      default("Sabine Parish")
#  court_title                :string(255)      default("Eleventh Judicial District")
#  prosecutor_title           :string(255)      default("District Attorney")
#  chief_justice_title        :string(255)      default("Judge")
#  court_clerk_name           :string(255)      default("Michael Merten")
#  court_clerk_title          :string(255)      default("Owner")
#  recaptcha_enabled          :boolean          default(FALSE)
#  recaptcha_public_key       :string(255)
#  recaptcha_private_key      :string(255)
#  recaptcha_proxy            :string(255)
#  adult_age                  :integer          default(17)
#  district_values            :string(255)      default(""), not null
#  zone_values                :string(255)      default(""), not null
#  ward_values                :string(255)      default(""), not null
#
