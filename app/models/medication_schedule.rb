class MedicationSchedule < ActiveRecord::Base
   belongs_to :medication, inverse_of: :schedules                          # constraint
   
   validates_presence_of :dose
   validates_time_string :time
   
   ## Class Methods #############################################
   
   def self.desc
      'Medicine Schedules'
   end
   
   ## Instance Methods #############################################
   
   def to_s
     "#{time}: #{dose}"
   end
end

# == Schema Information
#
# Table name: medication_schedules
#
#  id            :integer          not null, primary key
#  medication_id :integer          not null
#  dose          :string(255)      default(""), not null
#  time          :string(255)      default(""), not null
#  created_at    :datetime
#  updated_at    :datetime
#
# Indexes
#
#  index_med_schedules_on_medication_id  (medication_id)
#
# Foreign Keys
#
#  medication_schedules_medication_id_fk  (medication_id => medications.id)
#
