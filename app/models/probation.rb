class Probation < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :person, inverse_of: :probations                           # constraint
  has_one :hold, dependent: :destroy, inverse_of: :probation            # constraint
  has_many :probation_payments, dependent: :destroy                     # constraint
  belongs_to :docket, inverse_of: :probations                           # constraint
  belongs_to :court, inverse_of: :probations                            # constraint
  belongs_to :da_charge, inverse_of: :probation                         # constraint
  has_one :sentence, inverse_of: :probation, dependent: :nullify        # constraint
  
  scope :by_name, -> { includes(:person).order('people.family_name, people.given_name, people.suffix, probations.id') }
  scope :active, -> { where "LOWER(probations.status) IN ('active','pending')" }
  scope :current, -> { where "LOWER(probations.status) = 'active'" }
  
  # filter scopes
  scope :with_probation_id, ->(p_id) { where id: p_id }
  scope :with_person_id, ->(p_id) { where person_id: p_id }
  scope :with_start_date, ->(start,stop) { AppUtils.for_daterange self, :start_date, start: start, stop: stop, date: true }
  scope :with_end_date, ->(start,stop) { AppUtils.for_daterange self, :end_date, start: start, stop: stop, date: true }
  scope :with_completed_date, ->(start,stop) { AppUtils.for_daterange self, :completed_date, start: start, stop: stop, date: true }
  scope :with_revoked_date, ->(start,stop) { AppUtils.for_daterange self, :revoked_date, start: start, stop: stop, date: true }
  scope :with_hold_if_arrested, ->(hia) { AppUtils.for_bool self, :hold_if_arrested, hia }
  scope :with_status, ->(stat) { AppUtils.like self, :status, stat }
  scope :with_terminate_reason, ->(ter) { AppUtils.like self, :terminate_reason, ter }
  scope :with_docket_no, ->(d_no) { AppUtils.for_lower self, :docket_no, d_no }
  scope :with_probation_officer, ->(name,unit,badge) { AppUtils.for_officer self, :probation_officer, name, unit, badge }
  scope :with_notes, ->(n) { AppUtils.like self, :notes, n }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }

  attr_accessor :probation_officer_id
  
  before_validation :lookup_officers, :check_person_and_docket, :update_end_date
  before_create :update_fund_names
  before_save :check_creator

  validates_presence_of :status
  validates_date :start_date, :end_date, :completed_date, :revoked_date, :restitution_date, allow_blank: true
  validates_presence_of :revoked_date, if: :revoked_for?
  validates_presence_of :revoked_for, if: :revoked_date?
  validates_numericality_of :probation_hours, :probation_days, :probation_months,
    :probation_years, :sentence_hours, :sentence_days, :sentence_months, :sentence_years,
    :sentence_life, :default_hours, :default_days, :default_months, :default_years,
    :suspended_except_hours, :suspended_except_days, :suspended_except_months, :suspended_except_years,
    :suspended_except_life, only_integer: true, greater_than_or_equal_to: 0
  validates_numericality_of :fund1_charged, :fund1_paid, :fund2_charged, :fund2_paid,
    :fund3_charged, :fund3_paid, :fund4_charged, :fund4_paid, :fund5_charged, :fund5_paid,
    :fund6_charged, :fund6_paid, :fund7_charged, :fund7_paid, :fund8_charged, :fund8_paid,
    :fund9_charged, :fund9_paid, :costs, :fines, :probation_fees, :idf_amount,
    :dare_amount, :restitution_amount, greater_than_or_equal_to: 0, less_than: 10 ** 10
  
  ProbationStatus = ['Active','Completed','Pending','Revoked','State','Terminated', 'Unsupervised'].freeze
  
  ProbationType = ['State','Parish','Unsupervised'].freeze
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_probation
  end
  
  def self.desc
    "Probation Database"
  end
  
  def self.update_probation_status
    where(["(status = 'State' OR status = 'Unsupervised') AND end_date < ?",Date.today]).all.each do |p|
      p.status = 'Completed'
      p.completed_date = Date.today
      p.updated_by = 1
      p.save(validate: false)
    end
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_probation_id(query[:id]) unless query[:id].blank?
    result = result.with_person_id(query[:person_id]) unless query[:person_id].blank?
    result = result.with_start_date(query[:start_from],query[:start_to]) unless query[:start_from].blank? && query[:start_to].blank?
    result = result.with_end_date(query[:end_from],query[:end_to]) unless query[:end_from].blank? && query[:end_to].blank?
    result = result.with_completed_date(query[:completed_from],query[:completed_to]) unless query[:completed_from].blank? && query[:completed_to].blank?
    result = result.with_revoked_date(query[:revoked_from],query[:revoked_to]) unless query[:revoked_from].blank? && query[:revoked_to].blank?
    result = result.with_hold_if_arrested(query[:hold_if_arrested]) unless query[:hold_if_arrested].blank?
    result = result.with_status(query[:status]) unless query[:status].blank?
    result = result.with_terminate_reason(query[:terminate_reason]) unless query[:terminate_reason].blank?
    result = result.with_docket_no(query[:docket_no]) unless query[:docket_no].blank?
    result = result.with_probation_officer(query[:probation_officer],query[:probation_officer_unit],query[:probation_officer_badge]) unless query[:probation_officer].blank? && query[:probation_officer_unit].blank? && query[:probation_officer_badge].blank?
    result = result.with_notes(query[:notes]) unless query[:notes].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def active?
    status == "Active"
  end
  
  def balance_on(start_date)
    sdate = AppUtils.valid_date(start_date) or return total_balance
    bal = total_balance
    probation_payments.on_or_after(sdate).each do |p|
      bal = bal - p.total_charges + p.total_payments
    end
    bal
  end
  
  def community_service_complete?
    if service_days?
      if service_served?
        return service_days <= service_served
      else
        return false
      end
    elsif service_hours?
      if service_served?
        return service_hours <= service_served
      else
        return false
      end
    end
    return true
  end
  
  def current_month_payments
    probation_payments.on_or_after(Date.today.beginning_of_month).all
  end
  
  def default_string
    AppUtils.sentence_to_string(default_hours, default_days, default_months, default_years)
  end
  
  def fund1_balance
    fund1_charged - fund1_paid
  end
  
  def fund2_balance
    fund2_charged - fund2_paid
  end
  
  def fund3_balance
    fund3_charged - fund3_paid
  end
  
  def fund4_balance
    fund4_charged - fund4_paid
  end
  
  def fund5_balance
    fund5_charged - fund5_paid
  end
  
  def fund6_balance
    fund6_charged - fund6_paid
  end
  
  def fund7_balance
    fund7_charged - fund7_paid
  end
  
  def fund8_balance
    fund8_charged - fund8_paid
  end
  
  def fund9_balance
    fund9_charged - fund9_paid
  end
  
  def fund1_name=(new_name)
    unless !fund1_name.blank? || !new_name.is_a?(String) || new_name.blank?
      super(new_name)
    end
  end
  
  def fund2_name=(new_name)
    unless !fund2_name.blank? || !new_name.is_a?(String) || new_name.blank?
      super(new_name)
    end
  end
  
  def fund3_name=(new_name)
    unless !fund3_name.blank? || !new_name.is_a?(String) || new_name.blank?
      super(new_name)
    end
  end
  
  def fund4_name=(new_name)
    unless !fund4_name.blank? || !new_name.is_a?(String) || new_name.blank?
      super(new_name)
    end
  end
  
  def fund5_name=(new_name)
    unless !fund5_name.blank? || !new_name.is_a?(String) || new_name.blank?
      super(new_name)
    end
  end
  
  def fund6_name=(new_name)
    unless !fund6_name.blank? || !new_name.is_a?(String) || new_name.blank?
      super(new_name)
    end
  end
  
  def fund7_name=(new_name)
    unless !fund7_name.blank? || !new_name.is_a?(String) || new_name.blank?
      super(new_name)
    end
  end
  
  def fund8_name=(new_name)
    unless !fund8_name.blank? || !new_name.is_a?(String) || new_name.blank?
      super(new_name)
    end
  end
  
  def fund9_name=(new_name)
    unless !fund9_name.blank? || !new_name.is_a?(String) || new_name.blank?
      super(new_name)
    end
  end
  
  def past_days(days = 30)
    probation_payments.on_or_after(days.days.ago).all
  end
  
  def payment_months
    total = probation_hours + (probation_days * 24) + (probation_months * 720) + (probation_years * 8760)
    ((total / 24.0) / 30.0).ceil
  end
  
  def prior_month_payments
    probation_payments.between(1.month.ago.beginning_of_month, 1.month.ago.end_of_month).all
  end
  
  def probation_string
    AppUtils.sentence_to_string(probation_hours, probation_days, probation_months, probation_years)
  end
  
  def sentence_string
    AppUtils.sentence_to_string(sentence_hours, sentence_days, sentence_months, sentence_years, sentence_life, sentence_death)
  end
  
  def suspended_except_string
    AppUtils.sentence_to_string(suspended_except_hours, suspended_except_days, suspended_except_months, suspended_except_years, suspended_except_life)
  end
  
  def total_balance
    total_charged - total_paid
  end
  
  def total_charged
    total = BigDecimal.new("0.0",2)
    (1..9).each do |x|
      total += send("fund#{x}_charged")
    end
    total
  end
  
  def total_paid
    total = BigDecimal.new("0.0",2)
    (1..9).each do |x|
      total += send("fund#{x}_paid")
    end
    total
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def check_person_and_docket
    retval = true
    # first, check the person
    if person_id?
      if person.nil?
        self.errors.add(:person_id, 'is not a valid Person ID!')
        retval = false
      end
    else
      self.errors.add(:person_id, 'is required!')
      retval = false
    end
    # now check docket for that person
    if person_id? && !docket_id?
      if docket_no?
        # see if docket exists
        if d = Docket.where({docket_no: docket_no, person_id: person_id}).first
          # no docket id entered, but we have a matching docket by docket_no
          if da_charge_id?
            probs = Probation.where({person_id: person_id, docket_no: docket_no, da_charge_id: da_charge_id}).all.reject{|p| p.id = self.id}
            if probs.size > 0
              self.errors.add(:da_charge_id, 'already has a probation record associated with it.  You cannot add another probation for that charge!')
              retval = false
            end
            if c = DaCharge.find_by_id(da_charge_id)
              unless c.sentence.nil?
                self.errors.add(:da_charge_id, 'has already been sentenced, you cannot add a manual probation for that charge!')
                retval = false
              end
            end
            unless d.da_charges.collect{|c| c.id}.include?(da_charge_id)
              # pre-entry with da_charge_id that is bogus
              self.errors.add(:da_charge_id, 'is not valid for this docket!')
              retval = false
            end
          else
            # manual entry (without charge id) and docket already exists - disallow it
            self.errors.add(:docket_no, 'exists, cannot do manual probation entries for existing dockets without specifying a Charge ID!')
            self.errors.add(:da_charge_id, 'must be specified when creating probation entries for existing dockets!')
            retval = false
          end
        end
      else
        self.errors.add(:docket_no, 'is required!')
        retval = false
      end
    end
    retval
  end
  
  def lookup_officers
    unless probation_officer_id.blank?
      if ofc = Contact.find_by_id(probation_officer_id)
        self.probation_officer = ofc.sort_name
        self.probation_officer_badge = ofc.badge_no
        self.probation_officer_unit = ofc.unit
      end
      self.probation_officer_id = nil
    end
    true
  end
  
  def update_end_date
    unless end_date?
      if start_date? && (probation_hours? || probation_days? || probation_months? || probation_years?)
        hrs = AppUtils.sentence_to_hours(probation_hours, probation_days, probation_months, probation_years)
        self.end_date = start_date + hrs.hours
      end
    end
  end
  
  def update_fund_names
    (1..9).each do |x|
      self.send("fund#{x}_name=",SiteConfig.send("probation_fund#{x}"))
    end
    return true
  end
end

# == Schema Information
#
# Table name: probations
#
#  id                      :integer          not null, primary key
#  person_id               :integer          not null
#  start_date              :date
#  end_date                :date
#  completed_date          :date
#  revoked_date            :date
#  revoked_for             :string(255)      default(""), not null
#  hold_if_arrested        :boolean          default(TRUE), not null
#  probation_days          :integer          default(0), not null
#  probation_months        :integer          default(0), not null
#  probation_years         :integer          default(0), not null
#  costs                   :decimal(12, 2)   default(0.0), not null
#  notes                   :text             default(""), not null
#  remarks                 :text             default(""), not null
#  created_by              :integer          default(1)
#  updated_by              :integer          default(1)
#  created_at              :datetime
#  updated_at              :datetime
#  probation_fees          :decimal(12, 2)   default(0.0), not null
#  idf_amount              :decimal(12, 2)   default(0.0), not null
#  dare_amount             :decimal(12, 2)   default(0.0), not null
#  restitution_amount      :decimal(12, 2)   default(0.0), not null
#  probation_officer       :string(255)      default(""), not null
#  probation_officer_unit  :string(255)      default(""), not null
#  probation_officer_badge :string(255)      default(""), not null
#  fines                   :decimal(12, 2)   default(0.0), not null
#  docket_id               :integer
#  court_id                :integer
#  da_charge_id            :integer
#  doc                     :boolean          default(FALSE), not null
#  parish_jail             :boolean          default(FALSE), not null
#  trial_result            :string(255)      default(""), not null
#  default_days            :integer          default(0), not null
#  default_months          :integer          default(0), not null
#  default_years           :integer          default(0), not null
#  pay_by_date             :date
#  sentence_days           :integer          default(0), not null
#  sentence_months         :integer          default(0), not null
#  sentence_years          :integer          default(0), not null
#  sentence_suspended      :boolean          default(FALSE), not null
#  suspended_except_days   :integer          default(0), not null
#  suspended_except_months :integer          default(0), not null
#  suspended_except_years  :integer          default(0), not null
#  reverts_to_unsup        :boolean          default(FALSE), not null
#  reverts_conditions      :string(255)      default(""), not null
#  credit_served           :boolean          default(FALSE), not null
#  service_days            :integer          default(0), not null
#  service_served          :integer          default(0), not null
#  sap                     :boolean          default(FALSE), not null
#  sap_complete            :boolean          default(FALSE), not null
#  driver                  :boolean          default(FALSE), not null
#  driver_complete         :boolean          default(FALSE), not null
#  anger                   :boolean          default(FALSE), not null
#  anger_complete          :boolean          default(FALSE), not null
#  sat                     :boolean          default(FALSE), not null
#  sat_complete            :boolean          default(FALSE), not null
#  random_screens          :boolean          default(FALSE), not null
#  no_victim_contact       :boolean          default(FALSE), not null
#  art893                  :boolean          default(FALSE), not null
#  art894                  :boolean          default(FALSE), not null
#  art895                  :boolean          default(FALSE), not null
#  sentence_notes          :text             default(""), not null
#  docket_no               :string(255)      default(""), not null
#  conviction              :string(255)      default(""), not null
#  conviction_count        :integer          default(1), not null
#  restitution_date        :date
#  probation_hours         :integer          default(0), not null
#  default_hours           :integer          default(0), not null
#  sentence_hours          :integer          default(0), not null
#  suspended_except_hours  :integer          default(0), not null
#  service_hours           :integer          default(0), not null
#  fund1_name              :string(255)      default(""), not null
#  fund2_name              :string(255)      default(""), not null
#  fund3_name              :string(255)      default(""), not null
#  fund4_name              :string(255)      default(""), not null
#  fund5_name              :string(255)      default(""), not null
#  fund6_name              :string(255)      default(""), not null
#  fund7_name              :string(255)      default(""), not null
#  fund8_name              :string(255)      default(""), not null
#  fund9_name              :string(255)      default(""), not null
#  fund1_charged           :decimal(12, 2)   default(0.0), not null
#  fund2_charged           :decimal(12, 2)   default(0.0), not null
#  fund3_charged           :decimal(12, 2)   default(0.0), not null
#  fund4_charged           :decimal(12, 2)   default(0.0), not null
#  fund5_charged           :decimal(12, 2)   default(0.0), not null
#  fund6_charged           :decimal(12, 2)   default(0.0), not null
#  fund7_charged           :decimal(12, 2)   default(0.0), not null
#  fund8_charged           :decimal(12, 2)   default(0.0), not null
#  fund9_charged           :decimal(12, 2)   default(0.0), not null
#  fund1_paid              :decimal(12, 2)   default(0.0), not null
#  fund2_paid              :decimal(12, 2)   default(0.0), not null
#  fund3_paid              :decimal(12, 2)   default(0.0), not null
#  fund4_paid              :decimal(12, 2)   default(0.0), not null
#  fund5_paid              :decimal(12, 2)   default(0.0), not null
#  fund6_paid              :decimal(12, 2)   default(0.0), not null
#  fund7_paid              :decimal(12, 2)   default(0.0), not null
#  fund8_paid              :decimal(12, 2)   default(0.0), not null
#  fund9_paid              :decimal(12, 2)   default(0.0), not null
#  sentence_life           :integer          default(0), not null
#  sentence_death          :boolean          default(FALSE), not null
#  suspended_except_life   :integer          default(0), not null
#  status                  :string(255)      default(""), not null
#  pay_during_prob         :boolean          default(FALSE), not null
#  terminate_reason        :string(255)      default(""), not null
#
# Indexes
#
#  index_probations_on_court_id      (court_id)
#  index_probations_on_created_by    (created_by)
#  index_probations_on_da_charge_id  (da_charge_id)
#  index_probations_on_docket_id     (docket_id)
#  index_probations_on_person_id     (person_id)
#  index_probations_on_updated_by    (updated_by)
#
# Foreign Keys
#
#  probations_court_id_fk      (court_id => courts.id)
#  probations_created_by_fk    (created_by => users.id)
#  probations_da_charge_id_fk  (da_charge_id => da_charges.id)
#  probations_docket_id_fk     (docket_id => dockets.id)
#  probations_person_id_fk     (person_id => people.id)
#  probations_updated_by_fk    (updated_by => users.id)
#
