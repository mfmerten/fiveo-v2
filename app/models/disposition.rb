class Disposition < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'
  belongs_to :person, inverse_of: :dispositions
  has_many :arrests, dependent: :nullify
  
  ## Scopes #############################################
  
  scope :by_id, -> { order :id }
  scope :active, -> { where status: (-1..2) }
  scope :final, -> { where status: 2 }
  scope :with_disposition, -> { where "dispositions.disposition != ''" }
  
  # filter scopes
  scope :with_disposition_id, ->(d_id) { where id: d_id }
  scope :with_atn, ->(at) { where atn: at }
  scope :with_person_id, ->(p_id) { where person_id: p_id }
  scope :with_status, ->(stat) { where status: stat }
  scope :with_disposition, ->(disp) { AppUtils.like self, :disposition, disp }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  scope :with_person_name, ->(name) { AppUtils.for_person self, name, association: :person }
  scope :with_report_date, ->(start,stop) { AppUtils.for_daterange self, :report_date, start: start, stop: stop, date: true }
  
  ## Callbacks #############################################
  
  before_validation :strip_atn
  before_save :check_creator
  
  ## Validations #############################################
  
  validates_presence_of :atn
  validates_uniqueness_of :atn
  validates_person :person_id
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_court
  end
  
  def self.desc
    "Docket Disposition (AFIS) Records"
  end
  
  def self.update_status
    active.each do |d|
      d.update_status
    end
  end
  
  # process query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_disposition_id(query[:id]) unless query[:id].blank?
    result = result.with_atn(query[:atn]) unless query[:atn].blank?
    result = result.with_person_id(query[:person_id]) unless query[:person_id].blank?
    result = result.with_person_name(query[:name]) unless query[:name].blank?
    result = result.with_status(query[:status]) unless query[:status].blank?
    result = result.with_report_date(query[:report_from],query[:report_to]) unless query[:report_from].blank? && query[:report_to].blank?
    result = result.with_disposition(query[:disposition]) unless query[:disposition].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  # cleanup dispositions (remove ones with no remaining arrests)
  def self.cleanup
    
  end
  
  ## Instance Methods #############################################
  
  def arrest_date
    return nil if arrests.empty?
    arrests.collect{|a| a.arrest_datetime}.sort.first
  end
  
  def docket_charge_string
    docket_charges.collect{|c| "[#{c.arrest_charge.nil? ? '*' : c.arrest_charge.id.to_s}]: #{c.count} #{c.count == 1 ? 'Count' : 'Counts'} #{c.charge}"}.join("\n")
  end
  
  def docket_charges
    dchgs = original_charges.collect{|c| c.da_charges}.flatten.compact.uniq
    dchgs.concat(arrests.collect{|a| a.da_charges}.flatten.compact.uniq).uniq
    dchgs
  end
  
  def original_charge_string
    original_charges.collect{|c| "[#{c.id.to_s}]: #{c.count} #{c.count == 1 ? 'Count' : 'Counts'} #{c.charge}"}.join("\n")
  end
  
  def original_charges
    arrests.collect{|a| a.charges}.flatten.compact.uniq
  end
  
  def sentence_date
    docket_charges.reject{|c| c.sentence.nil?}.collect{|c| c.sentence.sentence_date}.compact.sort.last
  end
  
  def status_name
    case status
    when -1
      "[INVALID]"
    when 1
      "[Pending]"
    when 2
      "[Final]"
    when 3
      "[Reported]"
    else
      "[Unknown]"
    end
  end
  
  # builds a disposition text from all finalized charges (will not be complete
  # if status returns [Pending])
  def suggested_disposition(*args)
    options = args.extract_options!
    txt = ""
    if status < 2
      return txt
    end
    docket_charges.each do |d|
      next if d.info_only?
      prefix = "[#{d.arrest_charge.nil? ? '*' : d.arrest_charge.id}]"
      if options[:afis]
        chg_text = "#{prefix} #{d.count} CTS #{d.charge}"
      else
        chg_text = "#{d.count} #{d.count == 1 ? 'Count' : 'Counts'} #{d.charge}"
      end
      if d.dropped_by_da?
        if options[:afis]
          txt << "#{chg_text} NP\n"
        else
          txt << "#{chg_text} Dropped by DA\n"
        end
      elsif d.sentence.nil?
        if options[:afis]
          txt << "#{chg_text} Pending SENT\n"
        else
          txt << "#{chg_text} Pending Sentence.\n"
        end
      else
        txt << "#{prefix} #{d.sentence.sentence_summary(options.update(with_costs: true, with_conditions: true))}\n"
      end
    end
    txt
  end
  
  def update_status
    if report_date?
      self.update_attribute(:status, 3) and return unless status == 3 # Reported
    end
    if arrests.empty? || (original_charges.empty? && docket_charges.empty?)
      self.update_attribute(:status, 0) and return unless status == -1 # Invalid
    end
    final = true
    arrests.each do |a|
      a.charges.each do |c|
        if c.da_charges.empty?
          # not yet linked to da_charges
          final = false
          break
        end
        c.da_charges.each do |d|
          if !d.info_only? && d.sentence.nil? && !d.dropped_by_da?
            final = false
            break
          end
        end
      end
      # catch the extra charges added by DA
      a.da_charges.each do |d|
        if !d.info_only? && d.sentence.nil? && !d.dropped_by_da?
          final = false
          break
        end
      end
    end
    if final
      self.update_attribute(:status, 2) unless status == 2 # Final
    else
      self.update_attribute(:status, 1) unless status == 1 # Pending
    end
  end
  
  private
  
  ## Instance Methods #############################################
  
  def strip_atn
    if atn.blank?
      self.atn = nil
    else
      self.atn = atn.strip
    end
    true
  end
end

# == Schema Information
#
# Table name: dispositions
#
#  id          :integer          not null, primary key
#  atn         :string(255)      default(""), not null
#  person_id   :integer          not null
#  report_date :date
#  disposition :text             default(""), not null
#  remarks     :text             default(""), not null
#  status      :integer          default(0), not null
#  created_by  :integer          default(1)
#  updated_by  :integer          default(1)
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_dispositions_on_atn         (atn)
#  index_dispositions_on_created_by  (created_by)
#  index_dispositions_on_person_id   (person_id)
#  index_dispositions_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  dispositions_created_by_fk  (created_by => users.id)
#  dispositions_person_id_fk   (person_id => people.id)
#  dispositions_updated_by_fk  (updated_by => users.id)
#
