class InvestVictim < ActiveRecord::Base
  belongs_to :person, inverse_of: :invest_victims                          # constraint
  belongs_to :investigation, inverse_of: :invest_victims                   # constraint
  has_many :invest_crime_victims, dependent: :destroy                      # constraint
  has_many :crimes, through: :invest_crime_victims, source: :invest_crime  # constrained through invest_crime_victims
  has_many :invest_report_victims, dependent: :destroy                     # constraint
  has_many :reports, through: :invest_report_victims, source: :invest_report  # constrained through invest_report_victims
  
  ## Class Methods #############################################
  
  def self.desc
    'Investigation Victims'
  end
end

# == Schema Information
#
# Table name: invest_victims
#
#  id               :integer          not null, primary key
#  person_id        :integer          not null
#  investigation_id :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_invest_victims_on_investigation_id  (investigation_id)
#  index_invest_victims_on_person_id         (person_id)
#
# Foreign Keys
#
#  invest_victims_investigation_id_fk  (investigation_id => investigations.id)
#  invest_victims_person_id_fk         (person_id => people.id)
#
