class Group < ActiveRecord::Base
  has_many :user_groups, dependent: :destroy                            # constraint
  has_many :users, through: :user_groups                                # constrained through user_groups
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'    # constraint with nullify

  serialize :permissions
  
  scope :by_name, -> { order :name }
  
  # filter scopes
  scope :with_group_id, ->(g_id) { where id: g_id }
  scope :with_name, ->(n) { AppUtils.like self, :name, n }
  scope :with_description, ->(d) { AppUtils.like self, :description, d }

  before_save :check_creator
  before_destroy :check_destroyable

  validates_presence_of :name, :description
  validates_uniqueness_of :name

  ## Class Methods #############################################
  
  def self.base_perms
    :view_group
  end
  
  def self.desc
    "Groups of permissions"
  end

  def self.names_for_select
    by_name.all.collect{|g| [g.name, g.id]}
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_group_id(query[:id]) unless query[:id].blank?
    result = result.with_name(query[:name]) unless query[:name].blank?
    result = result.with_description(query[:description]) unless query[:description].blank?
    result
  end

  ## Instance Methods #############################################
  
  # I define the permissions field reader to return [] instead of nil
  # when there are no permissions set.
  def permissions
    tmp = super
    if tmp.blank?
      []
    else
      tmp
    end
  end

  # I define the permissions field writer to set [] instead of nil
  # when there are no permissions specified.  Also ensures permission
  # values are integer even when specified as numeric strings.
  def permissions=(perms)
    if perms.nil? || perms.empty?
      values = []
    else
      values = perms.collect{|v| v.to_i}
    end
    super(values)
  end
  
  def reasons_not_destroyable
    name == 'Admin' ? "Cannot Delete Admin Group" : nil
  end

  private

  ## Instance Methods #############################################
  
  def check_destroyable
    reasons_not_destroyable == nil
  end
end

# == Schema Information
#
# Table name: groups
#
#  id          :integer          not null, primary key
#  name        :string(255)      default(""), not null
#  description :string(255)      default(""), not null
#  created_by  :integer          default(1)
#  updated_by  :integer          default(1)
#  created_at  :datetime
#  updated_at  :datetime
#  permissions :string(255)      default(""), not null
#
# Indexes
#
#  index_groups_on_created_by  (created_by)
#  index_groups_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  groups_created_by_fk  (created_by => users.id)
#  groups_updated_by_fk  (updated_by => users.id)
#
