class History < ActiveRecord::Base

  scope :by_name, -> { order :name }
  
  # filter scopes
  scope :with_history_id, ->(h_id) { where id: h_id }
  scope :with_key, ->(k) { where key: k }
  scope :with_name, ->(n) { AppUtils.like self, :name, n }
  scope :with_type_id, ->(t_id) { where type_id: t_id }
  scope :with_details, ->(det) { AppUtils.like self, :details, det }
  
  before_save :refuse

  HistoryType = {}.freeze
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_history
  end
  
  def self.desc
    "History (legacy data)"
  end

  def self.types_for_select
    HistoryType.invert.keys.sort.collect{|t| [t.key, t[key]]}
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_history_id(query[:id]) unless query[:id].blank?
    result = result.with_key(query[:key]) unless query[:key].blank?
    result = result.with_name(query[:name]) unless query[:name].blank?
    result = result.with_type_id(query[:type_id]) unless query[:type_id].blank?
    result = result.with_details(query[:details]) unless query[:details].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def type_name
    HistoryType[type_id]
  end
  
  private

  def refuse
    false
  end
end

# == Schema Information
#
# Table name: histories
#
#  id         :integer          not null, primary key
#  key        :string(255)      default(""), not null
#  name       :string(255)      default(""), not null
#  type_id    :integer          default(0), not null
#  details    :text             default(""), not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_histories_on_key      (key)
#  index_histories_on_name     (name)
#  index_histories_on_type_id  (type_id)
#
