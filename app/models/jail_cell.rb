class JailCell < ActiveRecord::Base
  belongs_to :jail, inverse_of: :cells                                     # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    "Jail Cells"
  end
end

# == Schema Information
#
# Table name: jail_cells
#
#  id          :integer          not null, primary key
#  jail_id     :integer          not null
#  name        :string(255)      default(""), not null
#  description :string(255)      default(""), not null
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_jail_cells_on_jail_id  (jail_id)
#
# Foreign Keys
#
#  jail_cells_jail_id_fk  (jail_id => jails.id)
#
