class InvestReportVictim < ActiveRecord::Base
  belongs_to :invest_report, inverse_of: :invest_report_victims            # constraint
  belongs_to :invest_victim, inverse_of: :invest_report_victims            # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'Investigation Report Victims'
  end
end

# == Schema Information
#
# Table name: invest_report_victims
#
#  id               :integer          not null, primary key
#  invest_report_id :integer          not null
#  invest_victim_id :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_invest_report_victims_on_invest_report_id  (invest_report_id)
#  index_invest_report_victims_on_invest_victim_id  (invest_victim_id)
#
# Foreign Keys
#
#  invest_report_victims_invest_report_id_fk  (invest_report_id => invest_reports.id)
#  invest_report_victims_invest_victim_id_fk  (invest_victim_id => invest_victims.id)
#
