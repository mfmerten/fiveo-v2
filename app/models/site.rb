# site configuration model built on ActiveAttr::Model using SiteStore as the
# attribute storage model.
class Site
  include ActiveAttr::Model
  include ApplicationValidations
  include ActiveModel::Dirty
  
  # define site attributes
  attribute :agency,                        type: String,  default: "FiveO"
  attribute :agency_acronym,                type: String,  default: "FiveO"
  attribute :agency_street,                 type: String,  default: ""
  attribute :agency_city,                   type: String,  default: ""
  attribute :agency_state,                  type: String,  default: ""
  attribute :agency_zip,                    type: String,  default: ""
  attribute :motto,                         type: String,  default: "Law Enforcement Software"
  attribute :ceo,                           type: String,  default: "Michael Merten"
  attribute :ceo_title,                     type: String,  default: "Midlake Technical Services"
  attribute :site_email,                    type: String,  default: ""
  attribute :local_zips,                    type: String,  default: ""
  attribute :local_agencies,                type: String,  default: ""
  attribute :colorscheme,                   type: String,  default: "blue_brown"
  attribute :activity_retention,            type: Integer, default: 90
  attribute :district_values,               type: String,  default: ""
  attribute :zone_values,                   type: String,  default: ""
  attribute :ward_values,                   type: String,  default: ""
  attribute :show_history,                  type: Boolean, default: false
  
  # message attributes
  attribute :message_log_retention,         type: Integer, default: 90
  attribute :message_policy,                type: String,  default: ""
  attribute :message_retention,             type: Integer, default: 0
  
  # user attributes
  attribute :session_life,                  type: Integer, default: 720
  attribute :session_active,                type: Integer, default: 15
  attribute :session_max,                   type: Integer, default: 960
  attribute :allow_user_photos,             type: Boolean, default: true
  attribute :user_update_contact,           type: Boolean, default: true
  
  # paper service attributes
  attribute :mileage_fee,                   type: BigDecimal, default: 0.88
  attribute :noservice_fee,                 type: BigDecimal, default: 5.0
  attribute :paper_pay_type1,               type: String,     default: "Cash"
  attribute :paper_pay_type2,               type: String,     default: "Check"
  attribute :paper_pay_type3,               type: String,     default: "Money Order"
  attribute :paper_pay_type4,               type: String,     default: "Credit Card"
  attribute :paper_pay_type5,               type: String,     default: ""
  
  # PDF document attributes
  attribute :header_title,                  type: String,  default: "FiveO"
  attribute :header_subtitle1,              type: String,  default: "Midlake Technical Services"
  attribute :header_subtitle2,              type: String,  default: "Michael Merten, Owner"
  attribute :header_subtitle3,              type: String,  default: "http://midlaketech.com - mike@midlaketech.com"
  attribute :footer_text,                   type: String,  default: "Law Enforcement Software"
  attribute :pdf_header_bg,                 type: String,  default: "ddddff"
  attribute :pdf_header_txt,                type: String,  default: "000099"
  attribute :pdf_section_bg,                type: String,  default: "ddddff"
  attribute :pdf_section_txt,               type: String,  default: "000099"
  attribute :pdf_em_bg,                     type: String,  default: "ffdddd"
  attribute :pdf_em_txt,                    type: String,  default: "990000"
  attribute :pdf_row_odd,                   type: String,  default: "ffffff"
  attribute :pdf_row_even,                  type: String,  default: "eeeeee"
  attribute :pdf_heading_bg,                type: String,  default: "dddddd"
  attribute :pdf_heading_txt,               type: String,  default: "000000"
  attribute :jurisdiction_title,            type: String,  default: "Sabine Parish"
  attribute :court_title,                   type: String,  default: "Eleventh Judicial District"
  attribute :prosecutor_title,              type: String,  default: "District Attorney"
  attribute :chief_justice_title,           type: String,  default: "Judge"
  attribute :court_clerk_name,              type: String,  default: "Michael Merten"
  attribute :court_clerk_title,             type: String,  default: "Owner"
  
  # dispatch attributes
  attribute :dispatch_shift1_start,         type: String,  default: "05:30"
  attribute :dispatch_shift1_stop,          type: String,  default: "17:30"
  attribute :dispatch_shift2_start,         type: String,  default: "17:30"
  attribute :dispatch_shift2_stop,          type: String,  default: "05:30"
  attribute :dispatch_shift3_start,         type: String,  default: ""
  attribute :dispatch_shift3_stop,          type: String,  default: ""
  
  # probation attributes
  attribute :probation_fund1,               type: String,  default: "Fines"
  attribute :probation_fund2,               type: String,  default: "Costs"
  attribute :probation_fund3,               type: String,  default: "Probation Fees"
  attribute :probation_fund4,               type: String,  default: "IDF"
  attribute :probation_fund5,               type: String,  default: "DARE"
  attribute :probation_fund6,               type: String,  default: "Restitution"
  attribute :probation_fund7,               type: String,  default: "DA Fees"
  attribute :probation_fund8,               type: String,  default: ""
  attribute :probation_fund9,               type: String,  default: ""
                                                               
  # patrol attributes                                          
  attribute :patrol_shift1_start,           type: String,  default: "06:00"
  attribute :patrol_shift1_stop,            type: String,  default: "18:00"
  attribute :patrol_shift2_start,           type: String,  default: "18:00"
  attribute :patrol_shift2_stop,            type: String,  default: "06:00"
  attribute :patrol_shift3_start,           type: String,  default: ""
  attribute :patrol_shift3_stop,            type: String,  default: ""
  attribute :patrol_shift1_supervisor,      type: String,  default: ""
  attribute :patrol_shift2_supervisor,      type: String,  default: ""
  attribute :patrol_shift3_supervisor,      type: String,  default: ""
  attribute :patrol_shift4_start,           type: String,  default: ""
  attribute :patrol_shift4_stop,            type: String,  default: ""
  attribute :patrol_shift5_start,           type: String,  default: ""
  attribute :patrol_shift5_stop,            type: String,  default: ""
  attribute :patrol_shift6_start,           type: String,  default: ""
  attribute :patrol_shift6_stop,            type: String,  default: ""
  attribute :patrol_shift4_supervisor,      type: String,  default: ""
  attribute :patrol_shift5_supervisor,      type: String,  default: ""
  attribute :patrol_shift6_supervisor,      type: String,  default: ""
                                                               
  # jail attributes                                            
  attribute :afis_ori,                      type: String,  default: ""
  attribute :afis_agency,                   type: String,  default: ""
  attribute :afis_street,                   type: String,  default: ""
  attribute :afis_city,                     type: String,  default: ""
  attribute :afis_state,                    type: String,  default: ""
  attribute :afis_zip,                      type: String,  default: ""
  attribute :adult_age,                     type: Integer, default: 17
  
  # commissary attributes
  attribute :enable_internal_commissary,    type: Boolean,  default: false
  attribute :enable_cashless_systems,       type: Boolean,  default: false
  attribute :csi_ftp_user,                  type: String,   default: ""
  attribute :csi_ftp_password,              type: String,   default: ""
  attribute :csi_ftp_url,                   type: String,   default: ""
  attribute :csi_ftp_filename,              type: String,   default: ""
  attribute :csi_provide_ssn,               type: Boolean,  default: false
  attribute :csi_provide_dob,               type: Boolean,  default: false
  attribute :csi_provide_race,              type: Boolean,  default: false
  attribute :csi_provide_sex,               type: Boolean,  default: false
  attribute :csi_provide_addr,              type: Boolean,  default: false
  
  # forum attributes
  attribute :posts_per_star,                type: Integer,   default: 100
  
  attribute_names.each do |name|
    define_method("#{name}=") do |value|
      typecast_value = typecast_attribute(typecaster_for(self.class._attribute_type(name)), value)
      send("#{name}_will_change!") unless typecast_value == send(name)
      super(value)
    end
    define_method("#{name}_at") do |value|
      # used to look up historical values - must resort to direct 
      # store lookup
      if value.is_a?(Time)
        SiteStore.send(name, value)
      else
        send(name)
      end
    end
  end
  
  validates_presence_of :agency, :agency_street, :agency_city, :agency_state,
    :agency_zip, :agency_acronym, :motto, :ceo, :ceo_title,
    :header_title, :header_subtitle1, :footer_text, :site_email, :colorscheme,
    :jurisdiction_title, :court_title, :prosecutor_title, :chief_justice_title,
    :court_clerk_name, :court_clerk_title
  validates_numericality_of :message_log_retention,
    :activity_retention, :session_life, :session_active, :session_max,
    :message_retention, :adult_age, :posts_per_star, only_integer: true,
    greater_than_or_equal_to: 0
  validates_format_of :pdf_header_bg, :pdf_header_txt, :pdf_section_bg, :pdf_section_txt,
    :pdf_em_bg, :pdf_em_txt, :pdf_row_odd, :pdf_row_even, :pdf_heading_txt, :pdf_heading_bg,
    with: /\A[a-fA-F0-9]{6}\Z/
  validates_format_of :agency_acronym, with: /\A[A-Z]+[A-Z0-9]*\Z/,
    message: "must begin with a capital letter and contain only capital letters and numbers. It cannot contain spaces or punctuation."
  validates_numericality_of :mileage_fee, :noservice_fee, greater_than_or_equal_to: 0
  validates_time_string :dispatch_shift1_start, :dispatch_shift1_stop, :dispatch_shift2_start,
    :dispatch_shift2_stop, :dispatch_shift3_start, :dispatch_shift3_stop, :patrol_shift1_start,
    :patrol_shift1_stop, :patrol_shift2_start, :patrol_shift2_stop, :patrol_shift3_start,
    :patrol_shift3_stop, :patrol_shift4_start, :patrol_shift4_stop, :patrol_shift5_start,
    :patrol_shift5_stop, :patrol_shift6_start, :patrol_shift6_stop, allow_blank: true
  
  def reload
    unless SiteStore.count == 0 # as it is when initially loading seeds
      attributes.each do |key,value|
        send("#{key}=", SiteStore.send(key))
      end
      clear_changes_information
    end
  end
  
  def save
    missing_to_default
    attributes.each do |key,value|
      if send("#{key}_changed?")
        SiteStore.send("#{key}=",value)
      end
    end
    changes_applied
  end
  
  def value_at(setting, time)
    return send(setting) unless time.is_a?(Time)
    SiteStore.send(setting, time)
  end
  
  def dispatch_shifts
    shifts = []
    (1..3).each do |s|
      unless send("dispatch_shift#{s}_start").blank? || send("dispatch_shift#{s}_stop").blank?
        shifts.push(s)
      end
    end
    shifts
  end

  def districts
    district_values.split(/\s*,\s*/)
  end

  def patrol_shifts
    shifts = []
    (1..6).each do |s|
      unless send("patrol_shift#{s}_start").blank? || send("patrol_shift#{s}_stop").blank?
        shifts.push(s)
      end
    end
    shifts
  end

  def wards
    ward_values.split(/\s*,\s*/)
  end

  def zones
    zone_values.split(/\s*,\s*/)
  end

  private
  
  #some fields automatically get default values when nil
  def missing_to_default
    default_attributes = self.class.new
    [:pdf_header_bg, :pdf_header_txt, :pdf_section_bg, :pdf_section_txt, :pdf_em_bg,
      :pdf_em_txt, :pdf_row_odd, :pdf_row_even, :pdf_heading_txt, :pdf_heading_bg,
      :colorscheme].each do |field|
      send("#{field}=", default_attributes.send(field)) if self.send(field).blank?
    end
    true
  end
end
  