class InvestEvidence < ActiveRecord::Base
  belongs_to :investigation, inverse_of: :invest_evidences                 # constraint
  belongs_to :evidence, inverse_of: :invest_evidences                      # constraint
  has_many :invest_crime_evidences, dependent: :destroy                    # constraint
  has_many :crimes, through: :invest_crime_evidences, source: :invest_crime  #
  has_many :invest_criminal_evidences, dependent: :destroy                 # constraint
  has_many :criminals, through: :invest_criminal_evidences, source: :invest_criminal  #
  
  ## Class Methods #############################################
  
  def self.desc
    'Investigation Evidence'
  end
end

# == Schema Information
#
# Table name: invest_evidences
#
#  id               :integer          not null, primary key
#  evidence_id      :integer          not null
#  investigation_id :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_invest_evidences_on_evidence_id       (evidence_id)
#  index_invest_evidences_on_investigation_id  (investigation_id)
#
# Foreign Keys
#
#  invest_evidences_evidence_id_fk       (evidence_id => evidences.id)
#  invest_evidences_investigation_id_fk  (investigation_id => investigations.id)
#
