class SiteImage < ActiveRecord::Base
  has_attached_file(:logo,
    default_url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/default_images/fiveo_:style.png",
    url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/images/:id/logo/:style_:basename.:extension",
    path: ":rails_root/public/system/images/:id/logo/:style_:basename.:extension",
    styles: {original: ['95x95#',:png], thumb: ['75x75#',:png]},
    default_style: :original,
    storage: :filesystem
  )
  
  has_attached_file(:ceo_img,
    default_url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/default_images/ceo_:style.png",
    url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/images/:id/ceo_img/:style_:basename.:extension",
    path: ":rails_root/public/system/images/:id/ceo_img/:style_:basename.:extension",
    styles: {original: ['200x200#',:png], thumb: ['75x75#',:png]},
    default_style: :original,
    storage: :filesystem
  )
  
  has_attached_file(:header_left_img,
    default_url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/default_images/header_:style.png",
    url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/images/:id/header_left_img/:style_:basename.:extension",
    path: ":rails_root/public/system/images/:id/header_left_img/:style_:basename.:extension",
    styles: {original: ['75x75#',:png], thumb: ['75x75#',:png]},
    default_style: :original,
    storage: :filesystem
  )
  
  has_attached_file(:header_right_img,
    default_url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/default_images/header_:style.png",
    url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/images/:id/header_right_img/:style_:basename.:extension",
    path: ":rails_root/public/system/images/:id/header_right_img/:style_:basename.:extension",
    styles: {original: ['75x75#', :png], thumb: ['75x75#',:png]},
    default_style: :original,
    storage: :filesystem
  )
  
  validates_attachment_content_type :logo, content_type: ["image/jpg","image/jpeg","image/gif","image/png","image/tiff"]
  validates_attachment_content_type :ceo_img, content_type: ["image/jpg","image/jpeg","image/gif","image/png","image/tiff"]
  validates_attachment_content_type :header_left_img, content_type: ["image/jpg","image/jpeg","image/gif","image/png","image/tiff"]
  validates_attachment_content_type :header_right_img, content_type: ["image/jpg","image/jpeg","image/gif","image/png","image/tiff"]
  
  
  # don't include this in databases index
  def self.desc
    nil
  end
  
  def self.load_configuration(site=true)
    if site
      last
    else
      first
    end
  end
  
  def self.base_perms
    :admin
  end
end

# == Schema Information
#
# Table name: site_images
#
#  id                            :integer          not null, primary key
#  logo_file_name                :string(255)
#  logo_content_type             :string(255)
#  logo_file_size                :integer
#  logo_updated_at               :datetime
#  ceo_img_file_name             :string(255)
#  ceo_img_content_type          :string(255)
#  ceo_img_file_size             :integer
#  ceo_img_updated_at            :datetime
#  header_left_img_file_name     :string(255)
#  header_left_img_content_type  :string(255)
#  header_left_img_file_size     :integer
#  header_left_img_updated_at    :datetime
#  header_right_img_file_name    :string(255)
#  header_right_img_content_type :string(255)
#  header_right_img_file_size    :integer
#  header_right_img_updated_at   :datetime
#
