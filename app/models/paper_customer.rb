class PaperCustomer < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  has_many :papers, dependent: :nullify                                 # constraint
  has_many :payments, class_name: 'PaperPayment', dependent: :destroy   # constraint

  scope :active, -> { where disabled: false }
  scope :by_name, -> { order :name }
  scope :billable, -> { joins(:papers).where("papers.billable is true and papers.invoice_datetime is null and (papers.served_datetime is not null or papers.returned_date is not null)") }
  scope :payable, -> { joins(:papers).where("papers.invoice_datetime is not null and papers.paid_date is null") }

  # filter scopes
  scope :with_paper_customer_id, ->(c_id) { where id: c_id }
  scope :with_name, ->(n) { AppUtils.like self, :name, n }
  scope :with_street, ->(s) { AppUtils.like self, :street, s }
  scope :with_city, ->(c) { AppUtils.like self, :city, c }
  scope :with_state, ->(s) { AppUtils.for_lower self, :state, s }
  scope :with_zip, ->(z) { AppUtils.like self, :zip, z }
  scope :with_attention, ->(a) { AppUtils.like self, :attention, a }
  scope :with_disabled, ->(d) { AppUtils.for_bool self, :disabled, d }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  
  before_save :check_creator
  before_validation :fix_fees
  before_destroy :check_destroyable

  validates_presence_of :name, :street, :city, :state
  validates_numericality_of :noservice_fee, :mileage_fee, greater_than_or_equal_to: 0, less_than: 10 ** 10
  validates_zip :zip
  validates_phone :phone, :fax, allow_blank: true

  ## Class Methods #############################################
  
  def self.base_perms
    :view_civil
  end
  
  def self.billable_names_for_select
    by_name.billable.collect{|c| [c.name, c.id]}
  end
  
  def self.common_names_for_select(allow_disabled=false)
    if allow_disabled == true
      Paper.find_by_sql('select paper_customer_id, count(paper_customer_id) as used, paper_customers.name as name from papers left outer join paper_customers on paper_customers.id = papers.paper_customer_id group by paper_customer_id, name order by used desc limit 5').collect{|p| [p.name,p.paper_customer_id]}
    else
      Paper.find_by_sql('select paper_customer_id, count(paper_customer_id) as used, paper_customers.name as name from papers left outer join paper_customers on paper_customers.id = papers.paper_customer_id where paper_customers.disabled IS FALSE group by paper_customer_id, name order by used desc limit 5').collect{|p| [p.name,p.paper_customer_id]}
    end
  end
  
  def self.desc
    "Paper Service Customers"
  end
  
  def self.names_for_select(allow_disabled=false)
    recs = by_name
    unless allow_disabled
      recs = recs.active
    end
    recs.all.collect{|c| [c.name, c.id]}
  end
  
  def self.payable_names_for_select
    by_name.payable.all.collect{|c| [c.name, c.id]}
  end
  
  def self.update_mileage_fee(price)
    update_all("mileage_fee = #{BigDecimal.new("#{price}",2)}")
  end
  
  def self.update_noservice_fee(price)
    update_all("noservice_fee = #{BigDecimal.new("#{price}",2)}")
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_paper_customer_id(query[:id]) unless query[:id].blank?
    result = result.with_name(query[:name]) unless query[:name].blank?
    result = result.with_street(query[:street]) unless query[:street].blank?
    result = result.with_city(query[:city]) unless query[:city].blank?
    result = result.with_state(query[:state]) unless query[:state].blank?
    result = result.with_zip(query[:zip]) unless query[:zip].blank?
    result = result.with_attention(query[:attention]) unless query[:attention].blank?
    result = result.with_disabled(query[:disabled]) unless query[:disabled].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def address
    [street, street2, address_line3].reject{|a| a.blank?}.join(', ')
  end
  
  def address_line1
    street
  end
  
  def address_line2
    street2
  end
  
  def address_line3
    txt = ""
    if city?
      txt << city
    end
    if state?
      unless txt.empty?
        txt << ", "
      end
      txt << state
    end
    if zip?
      unless txt.empty?
        txt << " "
      end
      txt << zip
    end
    return txt
  end
  
  def balance_due(edate = Date.today)
    return BigDecimal.new('0.0',2) unless edate.is_a?(Date)
    # add up unpaid invoices
    paper_balances = papers.reject{|p| (p.paid_date? && p.paid_date <= edate) || !p.invoice_datetime || !p.billable?}.collect{|p| p.invoice_due(edate)}
    # sum adjustment transactions
    adjust_balances = payments.reject{|p| !p.adjustment? || p.transaction_date > edate}.collect{|t| t.payment.blank? ? (t.charge.blank? ? BigDecimal.new('0.0',2) : t.charge) : (t.charge.blank? ? t.payment : t.payment - t.charge)}
    paper_balances.concat(adjust_balances).sum
  end
  
  def past_days(days = 30)
    return [] unless days.is_a?(Integer) && days >= 0
    date = days.days.ago
    payments.where('transaction_date >= ?',date).all
  end
  
  def payable_papers_for_select
    papers.payable.collect{|p| [p.id.to_s, p.id]}
  end
  
  def reasons_not_destroyable
    reasons = []
    push("Has Papers") unless papers.empty?
    push("Has Posted Payments") unless payments.posted.empty?
    reasons.empty? ? nil : reasons.join(', ')
  end
  
  def update_mileage_fee(price)
    self.update_attribute(:mileage_fee, BigDecimal.new("#{price}",2))
  end
  
  def update_noservice_fee(price)
    self.update_attribute(:noservice_fee, BigDecimal.new("#{price}",2))
  end
  
  private
  
  ## Instance Methods #############################################
  
  def check_destroyable
    reasons_not_destroyable.nil?
  end
  
  def fix_fees
    if noservice_fee.blank?
      self.noservice_fee = SiteConfig.noservice_fee
    end
    if mileage_fee.blank?
      self.mileage_fee = SiteConfig.mileage_fee
    end
    true
  end
end

# == Schema Information
#
# Table name: paper_customers
#
#  id            :integer          not null, primary key
#  name          :string(255)      default(""), not null
#  street        :string(255)      default(""), not null
#  city          :string(255)      default(""), not null
#  zip           :string(255)      default(""), not null
#  phone         :string(255)      default(""), not null
#  attention     :string(255)      default(""), not null
#  remarks       :text             default(""), not null
#  disabled      :boolean          default(FALSE), not null
#  created_by    :integer          default(1)
#  updated_by    :integer          default(1)
#  created_at    :datetime
#  updated_at    :datetime
#  noservice_fee :decimal(12, 2)   default(0.0), not null
#  mileage_fee   :decimal(12, 2)   default(0.0), not null
#  fax           :string(255)      default(""), not null
#  street2       :string(255)      default(""), not null
#  state         :string(255)
#
# Indexes
#
#  index_paper_customers_on_created_by  (created_by)
#  index_paper_customers_on_name        (name)
#  index_paper_customers_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  paper_customers_created_by_fk  (created_by => users.id)
#  paper_customers_updated_by_fk  (updated_by => users.id)
#
