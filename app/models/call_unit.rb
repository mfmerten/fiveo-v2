class CallUnit < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :call, inverse_of: :call_units

  ## Attributes #############################################
  
  attr_accessor :officer_id
  
  ## Callbacks #############################################
  
  before_validation :lookup_officers

  ## Validations #############################################
  
  validates_presence_of :officer

  ## Class Methods #############################################
  
  def self.desc
    "Dispatched Units for Call sheets."
  end
  
  ## Instance Methods #############################################
  
  def to_s
    "#{officer_unit} #{officer}"
  end
  
  def lookup_officers
    unless officer_id.blank?
      if ofc = Contact.find_by_id(officer_id)
        self.officer = ofc.sort_name
        self.officer_badge = ofc.badge_no
        self.officer_unit = ofc.unit
      end
      self.officer_id = nil
    end
    true
  end
end

# == Schema Information
#
# Table name: call_units
#
#  id            :integer          not null, primary key
#  call_id       :integer          not null
#  officer_unit  :string(255)      default(""), not null
#  officer       :string(255)      default(""), not null
#  created_at    :datetime
#  updated_at    :datetime
#  officer_badge :string(255)      default(""), not null
#
# Indexes
#
#  index_call_units_on_call_id  (call_id)
#
# Foreign Keys
#
#  call_units_call_id_fk  (call_id => calls.id)
#
