class InvestWarrant < ActiveRecord::Base
  belongs_to :investigation, inverse_of: :invest_warrants                  # constraint
  belongs_to :warrant, inverse_of: :invest_warrants                        # constraint
  has_many :invest_crime_warrants, dependent: :destroy                     # constraint
  has_many :crimes, through: :invest_crime_warrants, source: :invest_crime  # constrained through invest_crime_warrants
  has_many :invest_criminal_warrants, dependent: :destroy                  # constraint
  has_many :criminals, through: :invest_criminal_warrants, source: :invest_criminal  # constrained through invest_criminal_warrants
  
  ## Class Methods #############################################
  
  def self.desc
    'Investigation Warrants'
  end
end

# == Schema Information
#
# Table name: invest_warrants
#
#  id               :integer          not null, primary key
#  investigation_id :integer          not null
#  warrant_id       :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_invest_warrants_on_investigation_id  (investigation_id)
#  index_invest_warrants_on_warrant_id        (warrant_id)
#
# Foreign Keys
#
#  invest_warrants_investigation_id_fk  (investigation_id => investigations.id)
#  invest_warrants_warrant_id_fk        (warrant_id => warrants.id)
#
