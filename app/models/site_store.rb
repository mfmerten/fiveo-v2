class SiteStore < ActiveRecord::Base
  
  serialize :setting_value
  
  default_scope -> { order :created_at }
  
  # cannot delete settings outside of migrations
  before_destroy :refuse
  # cannot update settings outside of migrations
  # (setting changes are actually new records, not updates)
  before_update :refuse
  
  validates_presence_of :setting_name

  ## Class Methods #############################################
  
  def self.desc
    'Site Settings'
  end
  
  def self.setting_exists?(setting)
    Site.new.attributes.keys.include?(setting)
  end
  
  def self.respond_to?(method_sym, include_private = false)
    setting = method_sym.to_s.gsub(/=$/,'')
    if setting_exists?(setting)
      true
    else
      super
    end
  end
  
  def self.method_missing(method_sym, *arguments, &block)
    setting = method_sym.to_s.gsub(/=$/,'')
    if setting_exists?(setting)
      options = arguments.extract_options!
      if method_sym.to_s =~ /=$/
        # setter
        record = where('setting_name = ?',setting).last
        value = arguments.shift
        if value.nil?
          raise ArgumentError.new("SiteStore.#{setting}: setting value missing.")
        end
        # prevent duplicates
        if value == record.setting_value
          logger.info("Duplicate setting detected: #{value} == #{record.setting_value}")
        else
          create!(setting_name: setting, setting_value: value)
        end
      else
        # getter
        if valid_at = AppUtils.valid_datetime(arguments.shift)
          unless record = where('created_at <= ? AND setting_name = ?',valid_at,setting).last
            record = where('setting_name = ?',setting).first
          end
        else
          unless record = where('setting_name = ?',setting).last
            # no records exist yet, create the first one with Site default
            record = self.create(setting_name: setting, setting_value: Site.new.send(setting.to_s))
          end
        end
        return record.setting_value
      end
    else
      super
    end
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def refuse
    false
  end
end

# == Schema Information
#
# Table name: site_stores
#
#  id            :integer          not null, primary key
#  setting_name  :string(255)      not null
#  setting_value :text             not null
#  created_at    :datetime
#  updated_at    :datetime
#
# Indexes
#
#  index_site_stores_on_setting_name  (setting_name)
#
