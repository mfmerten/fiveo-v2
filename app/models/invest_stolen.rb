class InvestStolen < ActiveRecord::Base
  belongs_to :investigation, inverse_of: :invest_stolens                   # constraint
  belongs_to :stolen, inverse_of: :invest_stolens                          # constraint
  has_many :invest_crime_stolens, dependent: :destroy                      # constraint
  has_many :crimes, through: :invest_crime_stolens, source: :invest_crime  # constrained through invest_crime_stolens
  has_many :invest_criminal_stolens, dependent: :destroy                   # constraint
  has_many :criminals, through: :invest_criminal_stolens, source: :invest_criminal  # constrained through invest_criminal_stolens
  
  ## Class Methods #############################################
  
  def self.desc
    'Investigation Stolens'
  end
end

# == Schema Information
#
# Table name: invest_stolens
#
#  id               :integer          not null, primary key
#  stolen_id        :integer          not null
#  investigation_id :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_invest_stolens_on_investigation_id  (investigation_id)
#  index_invest_stolens_on_stolen_id         (stolen_id)
#
# Foreign Keys
#
#  invest_stolens_investigation_id_fk  (investigation_id => investigations.id)
#  invest_stolens_stolen_id_fk         (stolen_id => stolens.id)
#
