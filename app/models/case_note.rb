class CaseNote < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'    # constraint with nullify
  
  ## Scopes #############################################
  
  scope :by_reverse_date, -> { order updated_at: :desc }

  # filter scopes
  scope :with_case_no, ->(c_no) { where case_no: c_no }
  scope :with_note, ->(n) { AppUtils.like self, :note, n }
  
  ## Callbacks #############################################
  
  before_save :check_creator
  before_validation :adjust_case_no

  ## Validations #############################################
  
  validates_presence_of :note, :case_no
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_case
  end
  
  def self.desc
    "Case Notes"
  end
  
  # process the filter
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_case_no(query[:case_no]) unless query[:case_no].blank?
    result = result.with_note(query[:note]) unless query[:note].blank?
    result
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def adjust_case_no
    self.case_no = AppUtils.fix_case(case_no)
    true
  end
end

# == Schema Information
#
# Table name: case_notes
#
#  id         :integer          not null, primary key
#  case_no    :string(255)      default(""), not null
#  note       :text             default(""), not null
#  created_at :datetime
#  updated_at :datetime
#  created_by :integer          default(1)
#  updated_by :integer          default(1)
#
# Indexes
#
#  index_case_notes_on_case_no     (case_no)
#  index_case_notes_on_created_by  (created_by)
#  index_case_notes_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  case_notes_created_by_fk  (created_by => users.id)
#  case_notes_updated_by_fk  (updated_by => users.id)
#
