class InvestCrimeWarrant < ActiveRecord::Base
  belongs_to :invest_warrant, inverse_of: :invest_crime_warrants           # constraint
  belongs_to :invest_crime, inverse_of: :invest_crime_warrants             # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestCrime Warrants'
  end
end

# == Schema Information
#
# Table name: invest_crime_warrants
#
#  id                :integer          not null, primary key
#  invest_crime_id   :integer          not null
#  invest_warrant_id :integer          not null
#  created_at        :datetime
#  updated_at        :datetime
#
# Indexes
#
#  index_invest_crime_warrants_on_invest_crime_id    (invest_crime_id)
#  index_invest_crime_warrants_on_invest_warrant_id  (invest_warrant_id)
#
# Foreign Keys
#
#  invest_crime_warrants_invest_crime_id_fk    (invest_crime_id => invest_crimes.id)
#  invest_crime_warrants_invest_warrant_id_fk  (invest_warrant_id => invest_warrants.id)
#
