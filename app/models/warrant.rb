class Warrant < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :person, inverse_of: :warrants                             # constraint
  belongs_to :arrest, inverse_of: :warrants, counter_cache: true        # constraint
  has_many :warrant_exemptions, dependent: :destroy                     # constraint
  has_many :exempted_people, through: :warrant_exemptions, source: :person  # constrained through warrant_exemptions
  has_many :invest_warrants, dependent: :destroy                        # constraint
  has_many :investigations, through: :invest_warrants                   # constrained through invest_warrants
  has_many :charges, dependent: :destroy                                # constraint
  has_many :city_charges, -> { where charge_type: 'City' }, class_name: 'Charge'
  has_many :district_charges, -> { where charge_type: 'District' }, class_name: 'Charge'
  has_many :other_charges, -> { where charge_type: 'Other' }, class_name: 'Charge'
  has_many :untyped_charges, -> { where charge_type: '' }, class_name: 'Charge'
  has_many :warrant_possibles, dependent: :destroy
  has_many :ppeople, through: :warrant_possibles, source: :person
  
  ## Attributes #############################################
  
  accepts_nested_attributes_for :charges, allow_destroy: true, reject_if: ->(c) { c['charge'].blank? }
  attr_encrypted :ssn
  attr_accessor :full_name, :jurisdiction_id, :photo_form
  has_attached_file(:photo,
    url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/:attachment/:id/:style/:basename.:extension",
    path: ":rails_root/public/system/:attachment/:id/:style/:basename.:extension",
    styles: {thumb: ["80x80", :jpg], medium: ["300x300",:jpg], small: ["150x150",:jpg]},
    default_url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/default_images/missing_:style.jpg",
    default_style: :original,
    storage: :filesystem
  )
  
  ## Scopes #############################################
  
  scope :unlinked, -> { where person_id: nil }
  scope :active, -> { where dispo_date: nil }
  scope :by_name, -> { order :family_name, :given_name }
  scope :with_photo, -> { where.not(photo_file_name: '').where.not(photo_file_name: nil) }
  scope :with_city_charges, -> { includes(:charges).where(charges: {charge_type: 'City'}) }
  scope :with_district_charges, -> { includes(:charges).where(charges: {charge_type: 'District'}) }
  scope :with_other_charges, -> { includes(:charges).where(charges: {charge_type: 'Other'}) }
  # filter scopes
  scope :with_warrant_id, ->(w_id) { where id: w_id }
  scope :with_warrant_no, ->(w_no) { AppUtils.like self, :warrant_no, w_no }
  scope :with_person_id, ->(p_id) { where person_id: p_id }
  scope :with_full_name, ->(n) { AppUtils.for_person self, n }
  scope :with_ssn, ->(s) { where encrypted_ssn: SymmetricEncryption.encrypt(s) }
  scope :with_dob, ->(start,stop) { AppUtils.for_daterange self, :dob, start: start, stop: stop, date: true }
  scope :with_oln, ->(o) { AppUtils.like self, :oln, o }
  scope :with_oln_state, ->(s) { AppUtils.for_lower self, :oln_state, s }
  scope :with_race, ->(r) { AppUtils.like self, :race, r }
  scope :with_sex, ->(s) { where sex: s }
  scope :with_address, ->(a) { AppUtils.like self, [:street, :city, :state, :zip], a }
  # with_local - see Class Methods
  scope :with_issued_date, ->(start,stop) { AppUtils.for_daterange self, :issued_date, start: start, stop: stop, date: true }
  scope :with_special_type, ->(t) { where special_type: t }
  scope :with_jurisdiction, ->(j) { AppUtils.like self, :jurisdiction, j }
  scope :with_charge, ->(c) { AppUtils.like self, :charge, c, association: :charges }
  scope :with_disposition_date, ->(start,stop) { AppUtils.for_daterange self, :dispo_date, start: start, stop: stop, date: true }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  
  ## Callbacks #############################################

  before_save :check_creator, :check_sex
  after_save :update_person_matches
  before_validation :split_name, :check_disposition, :lookup_jurisdiction
  after_save :check_and_generate_warrant_no, :send_new_messages
  after_update :send_update_messages
  before_destroy :check_destroyable

  ## Validations #############################################
  
  validates_associated :charges
  validates_presence_of :family_name, :given_name, :jurisdiction
  validates_date :received_date, :issued_date
  validates_date :dispo_date, if: :disposition?
  validates_numericality_of :payable, :bond_amt, greater_than_or_equal_to: 0, less_than: 10 ** 10
  validates_numericality_of :sex, only_integer: true, greater_than_or_equal_to: 0, less_than: 3
  validates_person :person_id, allow_nil: true
  validates_phone :phone1, :phone2, :phone3, allow_blank: true
  validates_attachment_content_type :photo, content_type: ["image/jpg","image/jpeg","image/gif","image/png","image/tiff"]
  validates_ssn :ssn, allow_blank: true
  validates :encrypted_ssn, symmetric_encryption: true

  ## Class Constants #############################################
  
  WarrantDisposition = ["Served", "Recalled", "Paid", "Returned", "Unknown"].freeze
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_warrant
  end
  
  def self.desc
    "Wants and Warrants"
  end
  
  # returns true if warrants exist with photos available for photo_album
  def self.photos_available?
    warrant_photos = with_photo.pluck(:id)
    person_photos = joins(:person).where("people.front_mugshot_file_name != '' AND people.front_mugshot_file_name IS NOT NULL").references(:person).pluck(:id)
    warrant_ids = warrant_photos.concat(person_photos).uniq
    !warrant_ids.empty?
  end
  
  # retrieves relation for warrants with photos available for photo_album
  def self.photos_available
    warrant_photos = with_photo.pluck(:id)
    person_photos = joins(:person).where("people.front_mugshot_file_name != '' AND people.front_mugshot_file_name IS NOT NULL").references(:person).pluck(:id)
    warrant_ids = warrant_photos.concat(person_photos).uniq
    includes(:person).where(id: warrant_ids)
  end
  
  # retrieve a list of warrants that match a person name (using with_person_name scope)
  def self.warrants_for(person_id)
    # make sure person_id is valid
    return none unless p = Person.find_by(id: person_id)
    
    result = active.where(person_id: nil).with_full_name(p.sort_name)
    
    unless p.encrypted_ssn.blank?
      result = result.where("warrants.encrypted_ssn = ? OR warrants.encrypted_ssn = '' OR warrants.encrypted_ssn IS NULL",p.encrypted_ssn)
    end
    
    unless p.oln.blank?
      result = result.where("LOWER(warrants.oln) LIKE LOWER(?) OR warrants.oln = ''",p.oln)
    end
    
    unless p.oln_state.blank?
      result = result.where("LOWER(warrants.oln_state) = LOWER(?) OR warrants.oln_state = ''",p.oln_state)
    end
    
    if p.date_of_birth?
      result = result.where("warrants.dob = ? or warrants.dob IS NULL", p.date_of_birth)
    end
    
    result
  end
  
  def self.with_local(local=true)
    return self if local.blank? || SiteConfig.local_zips.blank?
    if local.is_a?(String)
      local = local.to_i
    end
    local_zips = SiteConfig.local_zips.split(/[ ,]+/).compact.uniq
    if local_zips.empty?
      self
    elsif local
      self.where('SUBSTR(warrants.zip,1,5) IN (?)', local_zips)
    else
      self.where('SUBSTR(warrants.zip,1,5) NOT IN (?)', local_zips)
    end
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_warrant_id(query[:id]) unless query[:id].blank?
    result = result.with_person_id(query[:person_id]) unless query[:person_id].blank?
    result = result.with_full_name(query[:full_name]) unless query[:full_name].blank?
    result = result.with_ssn(query[:ssn]) unless query[:ssn].blank?
    result = result.with_dob(query[:dob_from],query[:dob_to]) unless query[:dob_from].blank? && query[:dob_to].blank?
    result = result.with_oln(query[:oln]) unless query[:oln].blank?
    result = result.with_oln_state(query[:oln_state]) unless query[:oln_state].blank?
    result = result.with_race(query[:race]) unless query[:race].blank?
    result = result.with_sex(query[:sex]) unless query[:sex].blank?
    result = result.with_address(query[:address]) unless query[:address].blank?
    result = result.with_local(query[:local]) unless query[:local].blank?
    result = result.with_issued_date(query[:issued_from],query[:issued_to]) unless query[:issued_from].blank? && query[:issued_to].blank?
    result = result.with_special_type(query[:special_type]) unless query[:special_type].blank?
    result = result.with_jurisdiction(query[:jurisdiction]) unless query[:jurisdiction].blank?
    result = result.with_charge(query[:charge]) unless query[:charge].blank?
    result = result.with_disposition_date(query[:disposition_from],query[:disposition_to]) unless query[:disposition_from].blank? && query[:disposition_to].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def active?
    dispo_date.nil?
  end
  
  def has_photo
    photo_file_name != '' && !photo_file_name.nil?
  end
  
  def address1
    street
  end
  
  def address2
    addr = ""
    if city?
      addr << city
    end
    if state?
      unless addr.empty?
        addr << ", "
      end
      addr << state
    end
    if zip?
      unless addr.empty?
        addr << " "
      end
      addr << zip
    end
    return addr
  end
  
  def charge_summary
    charges.join(', ')
  end
  
  def charges_valid?
    untyped_charges.empty?
  end
  
  def sort_name
    n = Namae::Name.new(family: family_name, given: given_name, suffix: suffix)
    n.sort_order
  end
  
  def display_name
    n = Namae::Name.new(family: family_name, given: given_name, suffix: suffix)
    n.display_order
  end
  
  def is_local?
    local_zips = SiteConfig.local_zips
    return false if local_zips.blank?
    locals = local_zips.split(/[ ,]+/).compact.uniq
    return true unless zip?
    return true if local_zips.include?(zip)
    false
  end
  
  def one_line_address
    [address1,address2].reject{|a| a.blank?}.join(', ')
  end
  
  def phones
    txt = []
    if phone1?
      txt.push("#{phone1_type.blank? ? 'Phone' : phone1_type}: #{phone1}#{phone1_unlisted? ? ' (UNL)' : ''}")
    end
    if phone2?
      txt.push("#{phone2_type.blank? ? 'Phone' : phone2_type}: #{phone1}#{phone2_unlisted? ? ' (UNL)' : ''}")
    end
    if phone3?
      txt.push("#{phone3_type.blank? ? 'Phone' : phone3_type}: #{phone1}#{phone3_unlisted? ? ' (UNL)' : ''}")
    end
    txt.join(", ")
  end
  
  def race_abbreviation
    Person::Race[race] || race
  end
  
  def reasons_not_destroyable
    arrest.nil? ? nil : "Has Attached Arrest"
  end
  
  def sex_name(short=false)
    case sex
    when 0
      short ? 'U' : 'Unknown'
    when 1
      short ? 'M' : 'Male'
    when 2
      short ? 'F' : 'Female'
    else
      ''
    end
  end
  
  def special_name(short=false)
    case special_type
    when 1
      short ? 'BW' : 'Bench Warrant'
    when 2
      short ? 'WRIT' : 'Writ'
    else
      short ? 'WAR' : 'Warrant'
    end
  end
  
  # symmetric-encryption does not provide this
  def ssn?
    !ssn.blank?
  end
  
  def to_s
    "#{special_name != 'Warrant' ? '[' + special_name + ']' : ''} #{warrant_no} (ID:#{id}): #{jurisdiction.blank? ? 'Unknown' : jurisdiction}"
  end
  
  # called whenever a warrant is saved
  def update_person_matches
    self.ppeople.clear
    matches = Person.for_warrant(id)
    matches.each do |p|
      self.ppeople << p unless p.warrant_exemptions.include?(self)
    end
  end
  
  private
  
  ## Instance Methods #############################################
  
  def check_and_generate_warrant_no
    unless warrant_no?
      self.update_attribute(:warrant_no, id.to_s.rjust(6,'0'))
    end
    true
  end
  
  def check_destroyable
    reasons_not_destroyable.nil?
  end
  
  def check_disposition
    unless disposition?
      if dispo_date?
        self.errors.add(:dispo_date, "cannot exist if there is no disposition!")
        return false
      end
    end
    true
  end
  
  # ensures that sex is 0 if nil before attmpting to save the record
  def check_sex
    if sex.nil?
      self.sex = 0
    end
    true
  end
  
  def lookup_jurisdiction
    unless jurisdiction_id.blank?
      if ag = Contact.find_by_id(jurisdiction_id)
        self.jurisdiction = ag.sort_name
        self.phone1 = ag.phone1
        self.phone2 = ag.phone2
        self.phone3 = ag.phone3
        self.phone1_type = ag.phone1_type
        self.phone2_type = ag.phone2_type
        self.phone3_type = ag.phone3_type
        self.phone1_unlisted = ag.phone1_unlisted
        self.phone2_unlisted = ag.phone2_unlisted
        self.phone3_unlisted = ag.phone3_unlisted
      end
      self.jurisdiction_id = nil
    end
    true
  end
  
  def send_new_messages
    receivers = User.where(notify_warrant_new: true).all
    receivers.each do |r|
      next unless r.can?(self.class.base_perms)
      txt = AutomatedMessagesController.new.notify(self, 'new')
      Message.create(receiver_id: r.id, sender_id: updated_by, subject: "Warrant Added For: #{sort_name}", body: txt)
    end
  end
  
  def send_update_messages
    if previous_changes.include?('dispo_date')
      if dispo_date?
        # resolved warrant
        receivers = User.where(notify_warrant_resolved: true).all
        receivers.each do |r|
          next unless r.can?(self.class.base_perms)
          txt = AutomatedMessagesController.new.notify(self, 'resolved')
          Message.create(receiver_id: r.id, sender_id: updated_by, subject: "Warrant No: #{warrant_no} #{disposition.blank? ? 'processed' : disposition} for #{sort_name}", body: txt)
        end
      else
        # reactivated warrant
        receivers = User.where(notify_warrant_new: true).all
        receivers.each do |r|
          next unless r.can?(self.class.base_perms)
          txt = AutomatedMessagesController.new.notify(self, 'reactivated')
          Message.create(receiver_id: r.id, sender_id: updated_by, subject: "Warrant No: #{warrant_no} reactivated for #{sort_name}", body: txt)
        end
      end
    end
  end
  
  def split_name
    unless full_name.nil? || full_name.blank?
      n = Namae::Name.parse(full_name)
      self.family_name = n.family || ''
      self.given_name = n.given || ''
      self.suffix = n.suffix || ''
    end
    true
  end
end

# == Schema Information
#
# Table name: warrants
#
#  id                 :integer          not null, primary key
#  person_id          :integer
#  warrant_no         :string(255)      default(""), not null
#  received_date      :date
#  issued_date        :date
#  dispo_date         :date
#  remarks            :text             default(""), not null
#  created_at         :datetime
#  updated_at         :datetime
#  bond_amt           :decimal(12, 2)   default(0.0), not null
#  payable            :decimal(12, 2)   default(0.0), not null
#  created_by         :integer          default(1)
#  updated_by         :integer          default(1)
#  dob                :date
#  street             :string(255)      default(""), not null
#  oln                :string(255)      default(""), not null
#  given_name         :string(255)      default(""), not null
#  family_name        :string(255)      default(""), not null
#  suffix             :string(255)      default(""), not null
#  sex                :integer          default(0), not null
#  special_type       :integer          default(0), not null
#  city               :string(255)      default(""), not null
#  zip                :string(255)      default(""), not null
#  legacy             :boolean          default(FALSE), not null
#  state              :string(255)      default(""), not null
#  oln_state          :string(255)      default(""), not null
#  jurisdiction       :string(255)      default(""), not null
#  phone1             :string(255)      default(""), not null
#  phone2             :string(255)      default(""), not null
#  phone3             :string(255)      default(""), not null
#  phone1_unlisted    :boolean          default(FALSE), not null
#  phone2_unlisted    :boolean          default(FALSE), not null
#  phone3_unlisted    :boolean          default(FALSE), not null
#  race               :string(255)      default(""), not null
#  phone1_type        :string(255)      default(""), not null
#  phone2_type        :string(255)      default(""), not null
#  phone3_type        :string(255)      default(""), not null
#  disposition        :string(255)      default(""), not null
#  charge_type        :string(255)      default(""), not null
#  arrest_id          :integer
#  photo_file_name    :string(255)
#  photo_content_type :string(255)
#  photo_file_size    :integer
#  photo_updated_at   :datetime
#  encrypted_ssn      :string           default("")
#
# Indexes
#
#  index_warrants_on_arrest_id                   (arrest_id)
#  index_warrants_on_created_by                  (created_by)
#  index_warrants_on_family_name_and_given_name  (family_name,given_name)
#  index_warrants_on_person_id                   (person_id)
#  index_warrants_on_updated_by                  (updated_by)
#  index_warrants_on_warrant_no                  (warrant_no)
#
# Foreign Keys
#
#  warrants_arrest_id_fk   (arrest_id => arrests.id)
#  warrants_created_by_fk  (created_by => users.id)
#  warrants_person_id_fk   (person_id => people.id)
#  warrants_updated_by_fk  (updated_by => users.id)
#
