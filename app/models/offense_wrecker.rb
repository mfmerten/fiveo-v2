class OffenseWrecker < ActiveRecord::Base
  belongs_to :offense, inverse_of: :wreckers                               # constraint

  validates_presence_of :name

  ## Class Methods #############################################
  
  def self.desc
    'Wreckers for Offenses'
  end
end

# == Schema Information
#
# Table name: offense_wreckers
#
#  id         :integer          not null, primary key
#  offense_id :integer          not null
#  name       :string(255)      default(""), not null
#  by_request :boolean          default(FALSE), not null
#  location   :string(255)      default(""), not null
#  vin        :string(255)      default(""), not null
#  tag        :string(255)      default(""), not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_wreckers_on_offense_id  (offense_id)
#
# Foreign Keys
#
#  offense_wreckers_offense_id_fk  (offense_id => offenses.id)
#
