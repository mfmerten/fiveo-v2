class RevocationConsecutive < ActiveRecord::Base
  belongs_to :revocation, inverse_of: :revocation_consecutives             # constraint
  belongs_to :docket, inverse_of: :revocation_consecutives                 # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'Consecutive Dockets for Revocations'
  end
end

# == Schema Information
#
# Table name: revocation_consecutives
#
#  id            :integer          not null, primary key
#  docket_id     :integer          not null
#  revocation_id :integer          not null
#  created_at    :datetime
#  updated_at    :datetime
#
# Indexes
#
#  index_revocation_consecutives_on_docket_id      (docket_id)
#  index_revocation_consecutives_on_revocation_id  (revocation_id)
#
# Foreign Keys
#
#  revocation_consecutives_docket_id_fk      (docket_id => dockets.id)
#  revocation_consecutives_revocation_id_fk  (revocation_id => revocations.id)
#
