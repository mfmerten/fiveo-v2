class InvestCrime < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :owner, class_name: 'User', foreign_key: 'owned_by'        # constraint with nullify
  belongs_to :investigation, inverse_of: :crimes                           # constraint
  has_many :notes, class_name: 'InvestNote', dependent: :destroy        # constraint
  has_many :invest_crime_arrests, dependent: :destroy                      # constraint
  has_many :invest_arrests, through: :invest_crime_arrests                 # constrained through invest_crime_arrests
  has_many :arrests, -> { distinct }, through: :invest_arrests             # constrained through invest_crime_arrests
  has_many :invest_crime_cases, dependent: :destroy                        # constraint
  has_many :invest_cases, through: :invest_crime_cases                     # constrained through invest_crime_cases
  has_many :cases, -> { distinct }, through: :invest_cases, source: :call  # constrained through invest_crime_cases
  has_many :invest_crime_contacts, dependent: :destroy                     # constraint
  has_many :invest_contacts, through: :invest_crime_contacts               # constrained through invest_crime_contacts
  has_many :contacts, -> { distinct }, through: :invest_contacts           # constrained through invest_crime_contacts
  has_many :invest_crime_criminals, dependent: :destroy                    # constraint
  has_many :criminals, through: :invest_crime_criminals, source: :invest_criminal  # constrained through invest_crime_criminals
  has_many :invest_crime_evidences, dependent: :destroy                    # constraint
  has_many :invest_evidences, through: :invest_crime_evidences             # constrained through invest_crime_evidences
  has_many :evidences, -> { distinct }, through: :invest_evidences         # constrained through invest_crime_evidences
  has_many :invest_crime_offenses, dependent: :destroy                     # constraint
  has_many :invest_offenses, through: :invest_crime_offenses               # constrained through invest_crime_offenses
  has_many :offenses, -> { distinct }, through: :invest_offenses           # constrained through invest_crime_offenses
  has_many :invest_crime_photos, dependent: :destroy                       # constraint
  has_many :photos, through: :invest_crime_photos, source: :invest_photo # constrained through invest_crime_photos
  has_many :invest_crime_stolens, dependent: :destroy                      # constraint
  has_many :invest_stolens, through: :invest_crime_stolens                 # constrained through invest_crime_stolens
  has_many :stolens, -> { distinct }, through: :invest_stolens             # constrained through invest_crime_stolens
  has_many :invest_crime_vehicles, dependent: :destroy                     # constraint
  has_many :vehicles, through: :invest_crime_vehicles, source: :invest_vehicle  # constrained through invest_crime_vehicles
  has_many :invest_crime_victims, dependent: :destroy                      # constraint
  has_many :invest_victims, through: :invest_crime_victims                 # constrained through invest_crime_victims
  has_many :victims, -> { distinct }, through: :invest_victims, source: :person  # constrained through invest_crime_victims
  has_many :invest_crime_warrants, dependent: :destroy                     # constraint
  has_many :invest_warrants, through: :invest_crime_warrants               # constrained through invest_crime_warrants
  has_many :warrants, -> { distinct }, through: :invest_warrants           # constrained through invest_crime_warrants
  has_many :invest_crime_witnesses, dependent: :destroy                    # constraint
  has_many :invest_witnesses, through: :invest_crime_witnesses             # constrained through invest_crime_witnesses
  has_many :witnesses, -> { distinct }, through: :invest_witnesses, source: :person  # constrained through invest_crime_witnesses
  
  scope :by_reverse_date, -> { order occurred_datetime: :desc }
  
  # filter scopes
  scope :with_invest_crime_id, ->(c_id) { where id: c_id }
  scope :with_investigation_id, ->(i_id) { where investigation_id: i_id }
  scope :with_occurred_date, ->(start, stop) { AppUtils.for_daterange self, :occurred_datetime, start: start, stop: stop }
  scope :with_location, ->(loc) { AppUtils.like self, :location, loc }
  scope :with_occupied, ->(oc) { AppUtils.for_bool self, :occupied, oc }
  scope :with_mo, ->(m) { AppUtils.like self, [:motive, :entry_gained, :security, :impersonated, :weapons_used, :facts_prior, :actions_taken, :threats_made, :targets, :facts_after], m }
  scope :with_victim_relationshiip, ->(v) { AppUtils.like self, :victim_relationship, v }
  scope :with_details, ->(det) { AppUtils.like self, :details, det }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  
  before_save :check_creator

  validates_presence_of :location, :details
  validates_datetime :occurred_datetime, allow_blank: true

  ## Class Methods #############################################
  
  def self.base_perms
    :view_investigation
  end
  
  def self.desc
    "Crimes related to Detective Investigations"
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_invest_crime_id(query[:id]) unless query[:id].blank?
    result = result.with_investigation_id(query[:investigation_id]) unless query[:investigation_id].blank?
    result = result.with_occurred_date(query[:occurred_date_from], query[:occurred_date_to]) unless query[:occurred_date_from].blank? && query[:occurred_date_to].blank?
    result = result.with_location(query[:location]) unless query[:location].blank?
    result = result.with_occupied(query[:occupied]) unless query[:occupied].blank?
    result = result.with_mo(query[:mo]) unless query[:mo].blank?
    result = result.with_victim_relationship(query[:victim_relationship]) unless query[:victim_relationship].blank?
    result = result.with_details(query[:details]) unless query[:details].blank?
    result = result.wtih_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def linked_arrests=(selected_arrests)
    self.invest_arrest_ids = selected_arrests.reject{|a| a.blank?}.collect{|a| a.to_i}.uniq
  end
  
  def linked_cases=(selected_cases)
    self.invest_case_ids = selected_cases.reject{|c| c.blank?}.collect{|c| c.to_i}.uniq
  end
  
  def linked_contacts=(selected_contacts)
    self.invest_contact_ids = selected_contacts.reject{|c| c.blank?}.collect{|c| c.to_i}.uniq
  end
  
  def linked_criminals=(selected_criminals)
    self.criminal_ids = selected_criminals.reject{|c| c.blank?}.collect{|c| c.to_i}.uniq
  end
  
  def linked_evidences=(selected_evidences)
    self.invest_evidence_ids = selected_evidences.reject{|e| e.blank?}.collect{|e| e.to_i}.uniq
  end
  
  def linked_offenses=(selected_offenses)
    self.invest_offense_ids = selected_offenses.reject{|o| o.blank?}.collect{|o| o.to_i}.uniq
  end
  
  def linked_photos=(selected_photos)
    self.photo_ids = selected_photos.reject{|p| w.blank?}.collect{|p| p.to_i}.uniq
  end
  
  def linked_stolens=(selected_stolens)
    self.invest_stolen_ids = selected_stolens.reject{|s| s.blank?}.collect{|s| s.to_i}.uniq
  end
  
  def linked_vehicles=(selected_vehicles)
    self.vehicle_ids = selected_vehicles.reject{|v| v.blank?}.collect{|v| v.to_i}.uniq
  end
  
  def linked_victims=(selected_victims)
    self.invest_victim_ids = selected_victims.reject{|v| v.blank?}.collect{|v| v.to_i}.uniq
  end
  
  def linked_warrants=(selected_warrants)
    self.invest_warrant_ids = selected_warrants.reject{|w| w.blank?}.collect{|w| w.to_i}.uniq
  end
  
  def linked_witnesses=(selected_witnesses)
    self.invest_witness_ids = selected_witnesses.reject{|w| w.blank?}.collect{|w| w.to_i}.uniq
  end
end

# == Schema Information
#
# Table name: invest_crimes
#
#  id                  :integer          not null, primary key
#  investigation_id    :integer          not null
#  private             :boolean          default(TRUE), not null
#  motive              :string(255)      default(""), not null
#  location            :string(255)      default(""), not null
#  occupied            :boolean          default(FALSE), not null
#  entry_gained        :string(255)      default(""), not null
#  weapons_used        :string(255)      default(""), not null
#  facts_prior         :text             default(""), not null
#  facts_after         :text             default(""), not null
#  targets             :text             default(""), not null
#  security            :string(255)      default(""), not null
#  impersonated        :string(255)      default(""), not null
#  actions_taken       :text             default(""), not null
#  threats_made        :text             default(""), not null
#  details             :text             default(""), not null
#  remarks             :text             default(""), not null
#  created_by          :integer          default(1)
#  updated_by          :integer          default(1)
#  created_at          :datetime
#  updated_at          :datetime
#  owned_by            :integer          default(1)
#  occurred_datetime   :datetime
#  victim_relationship :string(255)      default(""), not null
#
# Indexes
#
#  index_invest_crimes_on_created_by        (created_by)
#  index_invest_crimes_on_investigation_id  (investigation_id)
#  index_invest_crimes_on_owned_by          (owned_by)
#  index_invest_crimes_on_updated_by        (updated_by)
#
# Foreign Keys
#
#  invest_crimes_created_by_fk        (created_by => users.id)
#  invest_crimes_investigation_id_fk  (investigation_id => investigations.id)
#  invest_crimes_owned_by_fk          (owned_by => users.id)
#  invest_crimes_updated_by_fk        (updated_by => users.id)
#
