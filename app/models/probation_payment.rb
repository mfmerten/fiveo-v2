class ProbationPayment < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :probation, inverse_of: :probation_payments                # constraint
  
  scope :by_name, -> { includes({probation: :person}).order('people.family_name, people.given_name, people.suffix, probations.id, probation_payments.id') }
  scope :on_or_after, ->(date) { AppUtils.for_daterange self, :transaction_date, start: date, stop: nil, date: true }
  scope :on_or_before, ->(date) { AppUtils.for_daterange self, :transaction_date, start: nil, stop: date, date: true }
  scope :between, ->(start,stop) { AppUtils.for_daterange self, :transaction_date, start: start, stop: stop, date: true }
  scope :unposted, -> { where report_datetime: nil }
  scope :posted_at, ->(time) { where('probation_payments.report_datetime = ?',time) }

  attr_accessor :officer_id

  before_save :process_transaction, :check_creator
  before_validation :validate_fund_names, :verify_unposted, :lookup_officers
  before_destroy :check_destroyable_and_unprocess

  validates_presence_of :officer
  validates_date :transaction_date
  validates_numericality_of :fund1_payment, :fund1_charge, :fund2_payment, :fund2_charge, :fund3_payment,
    :fund3_charge, :fund4_payment, :fund4_charge, :fund5_payment, :fund5_charge, :fund6_payment,
    :fund6_charge, :fund7_payment, :fund7_charge, :fund8_payment, :fund8_charge, :fund9_payment, :fund9_charge,
    greater_than_or_equal_to: 0, less_than: 10 ** 10

  ## Class Methods #############################################
  
  def self.desc
    'Probation Payment Records'
  end
  
  def self.base_perms
    :view_probation
  end

  def self.posting_dates
    postings = select('DISTINCT report_datetime').order(report_datetime: :desc).all.collect{|p| p.report_datetime.blank? ? nil : [AppUtils.format_date(p.report_datetime),p.report_datetime]}.compact
    if postings.size > 10
      postings.slice(-10,10)
    else
      postings
    end
  end

  ## Instance Methods #############################################
  
  def reasons_not_destroyable
    report_datetime? ? "Already Posted" : nil
  end
  
  def total_charges
    total = BigDecimal.new('0.0',2)
    (1..9).each do |x|
      unless send("fund#{x}_charge").nil?
        total += send("fund#{x}_charge")
      end
    end
    total
  end

  def total_payments
    total = BigDecimal.new('0.0',2)
    (1..9).each do |x|
      unless send("fund#{x}_payment").nil?
        total += send("fund#{x}_payment")
      end
    end
    total
  end

  private

  ## Instance Methods #############################################
  
  def check_destroyable_and_unprocess
    return false unless reasons_not_destroyable.nil?
    # perform a reverse processing on the transaction
    unless probation.nil?
      Probation.transaction do
        prob = Probation.find(probation_id).lock
        (1..9).each do |x|
          unless send("fund#{x}_charge").nil?
            prob.send("fund#{x}_charged=", prob.send("fund#{x}_charged") - send("fund#{x}_charge"))
          end
          unless send("fund#{x}_payment").nil?
            prob.send("fund#{x}_paid=", prob.send("fund#{x}_paid") - send("fund#{x}_payment"))
          end
        end
        prob.save!
      end
    end
    true
  end
  
  def lookup_officers
    unless officer_id.blank?
      if ofc = Contact.find_by_id(officer_id)
        self.officer = ofc.sort_name
        self.officer_badge = ofc.badge_no
        self.officer_unit = ofc.unit
      end
      self.officer_id = nil
    end
    true
  end
  
  def process_transaction
    unless report_datetime?
      unless probation.nil?
        Probation.transaction do
          prob = Probation.find(probation_id).lock
          (1..9).each do |x|
            # charge
            newval = send("fund#{x}_charge")
            if newval.nil?
              newval = BigDecimal.new('0.00',2)
            end
            oldval = BigDecimal.new('0.00',2)
            if send("fund#{x}_charge_changed?")
              unless send("fund#{x}_charge_was").nil?
                oldval = send("fund#{x}_charge_was")
              end
            end
            prob.send("fund#{x}_charged=", prob.send("fund#{x}_charged") + newval - oldval)
            # payment
            newval = send("fund#{x}_payment")
            if newval.nil?
              newval = BigDecimal.new('0.00',2)
            end
            oldval = BigDecimal.new('0.00',2)
            if send("fund#{x}_payment_changed?")
              unless send("fund#{x}_payment_was").nil?
                oldval = send("fund#{x}_payment_was")
              end
            end
            prob.send("fund#{x}_paid=", prob.send("fund#{x}_paid") + newval - oldval)
            # new fund?
            if prob.send("fund#{x}_name") != send("fund#{x}_name")
              prob.send("fund#{x}_name=", send("fund#{x}_name"))
            end
          end
          prob.save!
        end
      end
    end
    return true
  end
  
  def validate_fund_names
    retval = true
    (1..9).each do |x|
      if send("fund#{x}_charge?") || send("fund#{x}_payment?")
        unless send("fund#{x}_name?")
          self.errors.add("fund#{x}_name".to_sym, "cannot be blank if a charge or payment is made against it!")
          retval = false
        end
      end
    end
    retval
  end
  
  def verify_unposted
    if report_datetime?
      self.errors.add(:base, "You Cannot Change Posted Transactions!")
      return false
    end
    return true
  end
end

# == Schema Information
#
# Table name: probation_payments
#
#  id               :integer          not null, primary key
#  probation_id     :integer          not null
#  transaction_date :date
#  officer          :string(255)      default(""), not null
#  officer_unit     :string(255)      default(""), not null
#  officer_badge    :string(255)      default(""), not null
#  created_by       :integer          default(1)
#  updated_by       :integer          default(1)
#  created_at       :datetime
#  updated_at       :datetime
#  receipt_no       :string(255)      default(""), not null
#  report_datetime  :datetime
#  memo             :string(255)      default(""), not null
#  fund1_name       :string(255)      default(""), not null
#  fund1_charge     :decimal(12, 2)   default(0.0), not null
#  fund1_payment    :decimal(12, 2)   default(0.0), not null
#  fund2_name       :string(255)      default(""), not null
#  fund2_charge     :decimal(12, 2)   default(0.0), not null
#  fund2_payment    :decimal(12, 2)   default(0.0), not null
#  fund3_name       :string(255)      default(""), not null
#  fund3_charge     :decimal(12, 2)   default(0.0), not null
#  fund3_payment    :decimal(12, 2)   default(0.0), not null
#  fund4_name       :string(255)      default(""), not null
#  fund4_charge     :decimal(12, 2)   default(0.0), not null
#  fund4_payment    :decimal(12, 2)   default(0.0), not null
#  fund5_name       :string(255)      default(""), not null
#  fund5_charge     :decimal(12, 2)   default(0.0), not null
#  fund5_payment    :decimal(12, 2)   default(0.0), not null
#  fund6_name       :string(255)      default(""), not null
#  fund6_charge     :decimal(12, 2)   default(0.0), not null
#  fund6_payment    :decimal(12, 2)   default(0.0), not null
#  fund7_name       :string(255)      default(""), not null
#  fund7_charge     :decimal(12, 2)   default(0.0), not null
#  fund7_payment    :decimal(12, 2)   default(0.0), not null
#  fund8_name       :string(255)      default(""), not null
#  fund8_charge     :decimal(12, 2)   default(0.0), not null
#  fund8_payment    :decimal(12, 2)   default(0.0), not null
#  fund9_name       :string(255)      default(""), not null
#  fund9_charge     :decimal(12, 2)   default(0.0), not null
#  fund9_payment    :decimal(12, 2)   default(0.0), not null
#
# Indexes
#
#  index_probation_payments_on_created_by       (created_by)
#  index_probation_payments_on_probation_id     (probation_id)
#  index_probation_payments_on_report_datetime  (report_datetime)
#  index_probation_payments_on_updated_by       (updated_by)
#
# Foreign Keys
#
#  probation_payments_created_by_fk    (created_by => users.id)
#  probation_payments_probation_id_fk  (probation_id => probations.id)
#  probation_payments_updated_by_fk    (updated_by => users.id)
#
