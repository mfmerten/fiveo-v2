class OffenseVictim < ActiveRecord::Base
  belongs_to :offense, inverse_of: :offense_victims                        # constraint
  belongs_to :person, inverse_of: :offense_victims                         # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'Offense Victims'
  end
end

# == Schema Information
#
# Table name: offense_victims
#
#  id         :integer          not null, primary key
#  offense_id :integer          not null
#  person_id  :integer          not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_offense_victims_on_offense_id  (offense_id)
#  index_offense_victims_on_person_id   (person_id)
#
# Foreign Keys
#
#  offense_victims_offense_id_fk  (offense_id => offenses.id)
#  offense_victims_person_id_fk   (person_id => people.id)
#
