class JailAbsentType < ActiveRecord::Base
  belongs_to :jail, inverse_of: :absent_types                              # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    "Jail Absent Types"
  end
end

# == Schema Information
#
# Table name: jail_absent_types
#
#  id          :integer          not null, primary key
#  name        :string(255)      default(""), not null
#  description :string(255)      default(""), not null
#  created_at  :datetime
#  updated_at  :datetime
#  jail_id     :integer          not null
#
# Indexes
#
#  index_jail_absent_types_on_jail_id  (jail_id)
#
# Foreign Keys
#
#  jail_absent_types_jail_id_fk  (jail_id => jails.id)
#
