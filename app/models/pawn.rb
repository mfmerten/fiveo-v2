class Pawn < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'    # constraint with nullify
  has_many :pawn_items, dependent: :destroy                                # constraint
  
  ## Attributes #############################################
  
  accepts_nested_attributes_for :pawn_items, allow_destroy: true, reject_if: ->(i) { i['description'].blank? }
  attr_encrypted :ssn
  
  ## Scopes #############################################
  
  scope :by_reverse_date, -> { order ticket_date: :desc }
  scope :with_case_no, ->(c_no) { where case_no: c_no }
  
  # filter scopes
  # with_case_no defined above
  scope :with_pawn_id, ->(p_id) { where id: p_id }
  scope :with_name, ->(n) { AppUtils.like self, :sort_name, n }
  scope :with_ssn, ->(s) { where encrypted_ssn: SymmetricEncryption.encrypt(ssn) }
  scope :with_oln, ->(o) { where old: oln }
  scope :with_desc, ->(des) { AppUtils.like self, [:model_no, :serial_no, :description], des, association: :pawn_items }
  scope :with_pawn_co, ->(pco) { AppUtils.like self, :pawn_co, pco }
  scope :with_ticket_no, ->(tno) { AppUtils.like self, :ticket_no, tno }
  scope :with_date, ->(start,stop) { AppUtils.for_daterange self, :ticket_date, start: start, stop: stop, date: date }
  scope :with_ticket_type, ->(tty) { AppUtils.like self, :ticket_type, tty }
  scope :with_item_type, ->(ity) { AppUtils.like self, :item_type, ity, association: :pawn_items }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  
  ## Callbacks #############################################
  
  before_save :check_creator
  before_validation :adjust_case_no, :fix_name
  after_create :send_new_messages

  ## Validations #############################################

  validates_associated :pawn_items
  validates_presence_of :ticket_no, :sort_name, :pawn_co, :ticket_type
  validates_date :ticket_date
  validates_presence_of :oln_state, if: :oln?
  validates_ssn :ssn, allow_blank: true
  validates :encrypted_ssn, symmetric_encryption: true

  ## Class Constants #############################################

  PawnTicketType = ['Pawned','Picked Up','Sold','Purchased'].freeze
  
  ## Class Methods #############################################
  
  def self.desc
    "Pawn Tickets"
  end

  def self.base_perms
    :view_pawn
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_pawn_id(query[:id]) unless query[:id].blank?
    result = result.with_name(query[:full_name]) unless query[:full_name].blank?
    result = result.with_ssn(query[:ssn]) unless query[:ssn].blank?
    result = result.with_oln(query[:oln]) unless query[:oln].blank?
    result = result.with_desc(query[:desc]) unless query[:desc].blank?
    result = result.with_pawn_co(query[:pawn_co]) unless query[:pawn_co].blank?
    result = result.with_ticket_no(query[:ticket_no]) unless query[:ticket_no].blank?
    result = result.with_date(query[:date_from],query[:date_to]) unless query[:date_from].blank? && query[:date_to].blank?
    result = result.with_ticket_type(query[:ticket_type]) unless query[:ticket_type].blank?
    result = result.with_item_type(query[:item_type]) unless query[:item_type].blank?
    result = result.with_case_no(query[:case_no]) unless query[:case_no].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end

  ## Instance Methods #############################################
  
  def item_summary
    pawn_items.join(' --- ')
  end
  
  # return the name in (given family suffix) order
  def display_name
    AppUtils.parse_name(sort_name,true)
  end
  
  # symmetric-encryption does not provide this
  def ssn?
    !ssn.blank?
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def send_new_messages
    receivers = User.where('notify_pawn_new IS TRUE').all
    receivers.each do |r|
      next unless r.can?(self.class.base_perms)
      txt = AutomatedMessagesController.new.notify(self, 'new')
      Message.create(receiver_id: r.id, sender_id: updated_by, subject: "New Pawn Ticket (#{id})", body: txt)
    end
  end
  
  def adjust_case_no
    self.case_no = AppUtils.fix_case(case_no)
    true
  end

  def fix_name
    self.sort_name = AppUtils.parse_name(sort_name)
    true
  end
end

# == Schema Information
#
# Table name: pawns
#
#  id            :integer          not null, primary key
#  ticket_no     :string(255)      default(""), not null
#  ticket_date   :date
#  case_no       :string(255)      default(""), not null
#  remarks       :text             default(""), not null
#  created_at    :datetime
#  updated_at    :datetime
#  created_by    :integer          default(1)
#  updated_by    :integer          default(1)
#  sort_name     :string(255)      default(""), not null
#  oln           :string(255)      default(""), not null
#  address1      :string(255)      default(""), not null
#  address2      :string(255)      default(""), not null
#  legacy        :boolean          default(FALSE), not null
#  oln_state     :string(255)      default(""), not null
#  pawn_co       :string(255)      default(""), not null
#  ticket_type   :string(255)      default(""), not null
#  encrypted_ssn :string           default("")
#
# Indexes
#
#  index_pawns_on_case_no     (case_no)
#  index_pawns_on_created_by  (created_by)
#  index_pawns_on_sort_name   (sort_name)
#  index_pawns_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  pawns_created_by_fk  (created_by => users.id)
#  pawns_updated_by_fk  (updated_by => users.id)
#
