class InvestCrimeStolen < ActiveRecord::Base
  belongs_to :invest_crime, inverse_of: :invest_crime_stolens              # constraint
  belongs_to :invest_stolen, inverse_of: :invest_crime_stolens             # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestCrime Stolens'
  end
end

# == Schema Information
#
# Table name: invest_crime_stolens
#
#  id               :integer          not null, primary key
#  invest_crime_id  :integer          not null
#  invest_stolen_id :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_invest_crime_stolens_on_invest_crime_id   (invest_crime_id)
#  index_invest_crime_stolens_on_invest_stolen_id  (invest_stolen_id)
#
# Foreign Keys
#
#  invest_crime_stolens_invest_crime_id_fk   (invest_crime_id => invest_crimes.id)
#  invest_crime_stolens_invest_stolen_id_fk  (invest_stolen_id => invest_stolens.id)
#
