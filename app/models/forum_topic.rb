# adapted from "forem" engine (https://github.com/radar/forem)
#
# Copyright 2011 Ryan Bigg, Philip Arndt and Josh Adams
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
class ForumTopic < ActiveRecord::Base
  belongs_to :forum, inverse_of: :forum_topics                             # constraint
  belongs_to :user, inverse_of: :forum_topics                              # constraint
  has_many :forum_views, dependent: :destroy                               # constraint
  has_many :forum_subscriptions, dependent: :destroy                       # constraint
  has_many :forum_posts, -> { order :created_at }, dependent: :destroy  # constraint
  has_many :initial_post, -> { order(:created_at).limit(1) }, class_name: 'ForumPost'  # constrained through forum_posts
  
  accepts_nested_attributes_for :initial_post
  
  scope :visible, -> { where hidden: false }
  scope :by_pinned, -> { order pinned: :desc, id: :asc }
  scope :by_most_recent_post, -> { order last_post_at: :desc, id: :asc }
  scope :by_pinned_or_most_recent_post, -> { order pinned: :desc, last_post_at: :desc, id: :asc }
  
  after_create :subscribe_poster
  
  validates_associated :initial_post
  validates_presence_of :subject
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_forum
  end
  
  def self.desc
    'Discussion Forum Topics'
  end
  
  ## Instance Methods #############################################
  
  def can_be_replied_to?
    !locked?
  end
  
  def last_page(forum_user)
    (self.forum_posts.count.to_f / forum_user.items_per_page).ceil
  end
  
  # Cannot use method name lock! because it's reserved by AR::Base
  def lock_topic!
    update_attribute(:locked, true)
  end
  
  def owner_or_admin?(forum_user)
    forum_user.is_forum_admin? || forum_user == user
  end
  
  def pin!
    update_attribute(:pinned, true)
  end
  
  def register_view_by(user)
    return unless user
    
    view = forum_views.find_or_create_by(user_id: user.id)
    view.increment!("count")
    increment!(:views_count)
    
    # update current_viewed_at if more than 15 minutes ago
    if view.current_viewed_at.nil?
      view.past_viewed_at = view.current_viewed_at = Time.now
    end
    
    # Update the current_viewed_at if it is BEFORE 15 minutes ago.
    if view.current_viewed_at < 15.minutes.ago
      view.past_viewed_at = view.current_viewed_at
      view.current_viewed_at = Time.now
      view.save
    end
  end
  
  def subscribe_poster
    subscribe_user(user_id)
  end
  
  def subscribe_user(user_id)
    if user_id && !subscriber?(user_id)
      forum_subscriptions.create!(subscriber_id: user_id)
    end
  end
  
  def subscriber?(user_id)
    forum_subscriptions.exists?(subscriber_id: user_id)
  end
  
  def subscription_for(user_id)
    forum_subscriptions.where(subscriber_id: user_id).first
  end
  
  def to_s
    subject
  end
  
  def unlock_topic!
    update_attribute(:locked, false)
  end
  
  def unpin!
    update_attribute(:pinned, false)
  end
  
  def unsubscribe_user(user_id)
    forum_subscriptions.where(subscriber_id: user_id).destroy_all
  end
  
  def view_for(user)
    forum_views.find_by(user_id: user.id)
  end
end

# == Schema Information
#
# Table name: forum_topics
#
#  id           :integer          not null, primary key
#  forum_id     :integer          not null
#  user_id      :integer          not null
#  subject      :string(255)      default(""), not null
#  locked       :boolean          default(FALSE), not null
#  pinned       :boolean          default(FALSE), not null
#  hidden       :boolean          default(FALSE), not null
#  last_post_at :datetime
#  views_count  :integer          default(0), not null
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_forum_topics_on_forum_id  (forum_id)
#  index_forum_topics_on_user_id   (user_id)
#
# Foreign Keys
#
#  forum_topics_forum_id_fk  (forum_id => forums.id)
#  forum_topics_user_id_fk   (user_id => users.id)
#
