class EvidenceLocation < ActiveRecord::Base
  belongs_to :evidence, counter_cache: true, inverse_of: :evidence_locations     # constraint
  belongs_to :officer_contact, class_name: 'Contact', foreign_key: 'officer_id'  # constraint with nullify

  before_validation :lookup_officer

  validates_presence_of :location_from, :location_to, :officer, :remarks
  validates_datetime :storage_datetime

  ## Class Methods #############################################
  
  def self.desc
    "Chain of Evidence"
  end

  private
  
  ## Private Instance Methods #############################################
  
  def lookup_officer
    unless officer_id.blank?
      if ofc = Contact.find_by_id(officer_id)
        self.officer = ofc.sort_name
        self.officer_unit = ofc.unit
        self.officer_badge = ofc.badge_no
      end
      self.officer_id = nil
    end
    true
  end
end

# == Schema Information
#
# Table name: evidence_locations
#
#  id               :integer          not null, primary key
#  evidence_id      :integer          not null
#  location_from    :string(255)      default(""), not null
#  location_to      :string(255)      default(""), not null
#  officer          :string(255)      default(""), not null
#  remarks          :string(255)      default(""), not null
#  created_at       :datetime
#  updated_at       :datetime
#  officer_unit     :string(255)      default(""), not null
#  officer_badge    :string(255)      default(""), not null
#  storage_datetime :datetime
#  officer_id       :integer
#
# Indexes
#
#  index_evidence_locations_on_evidence_id  (evidence_id)
#  index_evidence_locations_on_officer_id   (officer_id)
#
# Foreign Keys
#
#  evidence_locations_evidence_id_fk  (evidence_id => evidences.id)
#
