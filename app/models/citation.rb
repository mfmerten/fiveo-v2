class Citation < ActiveRecord::Base
  
  ## Class Methods #############################################
  
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'
  belongs_to :person, inverse_of: :citations
  has_many :offense_citations, dependent: :destroy
  has_many :offenses, through: :offense_citations
  has_many :charges, dependent: :destroy
  has_many :da_charges, through: :charges
  has_many :dockets, -> { distinct }, through: :da_charges
  
  ## Class Methods #############################################
  
  accepts_nested_attributes_for :charges, allow_destroy: true, reject_if: ->(c) { c['charge'].blank? }
  attr_accessor :officer_id
  
  ## Class Methods #############################################
  
  scope :by_reverse_date, -> { order citation_datetime: :desc }
  
  # filter scopes 
  scope :with_citation_id, ->(c_id) { where id: c_id }
  scope :with_ticket_no, ->(t_no) { AppUtils.like self, :ticket_no, t_no }
  scope :with_charge, ->(chg) { AppUtils.like self, :charge, chg, association: :charges }
  scope :with_person_id, ->(p_id) { where person_id: p_id }
  scope :with_offense_id, ->(o_id) { where offense_citations: {offense_id: o_id} }
  scope :with_officer, ->(name,unit,badge) { AppUtils.for_officer self, :officer, name, unit, badge }
  scope :with_citation_date, ->(start,stop) { AppUtils.for_daterange self, :citation_datetime, start: start, stop: stop }
  scope :with_paid_date, ->(start,stop) { AppUtils.for_daterange self, :paid_date, start: start, stop: stop, date: true }
  scope :with_recalled_date, ->(start,stop) { AppUtils.for_daterange self, :recalled_date, start: start, stop: stop, date: true }
  scope :with_person_name, ->(name) { AppUtils.for_person self, name, association: :person }

  ## Class Methods #############################################
  
  before_save :check_creator
  before_validation :lookup_officer
  before_destroy :check_destroyable

  ## Class Methods #############################################
  
  validates_associated :charges
  validates_presence_of :ticket_no, :officer
  validates_datetime :citation_datetime
  validates_date :paid_date, :recalled_date, allow_blank: true
  validates_person :person_id

  ## Class Methods #############################################
  
  def self.base_perms
    :view_citation
  end
  
  def self.desc
    'Misdemeanor Citations Table'
  end

  # process the query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_citation_id(query[:id]) unless query[:id].blank?
    result = result.with_citation_date(query[:citation_date_from],query[:citation_date_to]) unless query[:citation_date_from].blank? && query[:citation_date_to].blank?
    result = result.with_ticket_no(query[:ticket_no]) unless query[:ticket_no].blank?
    result = result.with_charge(query[:charge]) unless query[:charge].blank?
    result = result.with_person_id(query[:person_id]) unless query[:person_id].blank?
    result = result.with_person_name(query[:name]) unless query[:name].blank?
    result = result.with_officer(query[:officer], query[:officer_unit], query[:officer_badge]) unless query[:officer].blank? && query[:officer_unit].blank? && query[:officer_badge].blank?
    result = result.with_offense_id(query[:offense_id]) unless query[:offense_id].blank?
    result = result.with_paid_date(query[:paid_date_from],query[:paid_date_to]) unless query[:paid_date_from].blank? && query[:paid_date_to].blank?
    result = result.with_recalled_date(query[:recalled_date_from],query[:recalled_date_to]) unless query[:recalled_date_from].blank? && query[:recalled_date_to].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def charge_summary
    charges.join(', ')
  end
  
  def reasons_not_destroyable
    reasons = []
    reasons.push("Linked to Docket(s)") unless da_charges.empty?
    reasons.push("Linked to Offense(s)") unless offenses.empty?
    reasons.empty? ? nil : reasons.join(', ')
  end
  
  private

  ## Private Instance Methods #############################################

  def check_destroyable
    reasons_not_destroyable.nil?
  end
  
  def lookup_officer
    unless officer_id.blank?
      if ofc = Contact.find_by_id(officer_id)
        self.officer = ofc.sort_name
        self.officer_badge = ofc.badge_no
        self.officer_unit = ofc.unit
      end
      self.officer_id = nil
    end
    true
  end
end

# == Schema Information
#
# Table name: citations
#
#  id                :integer          not null, primary key
#  person_id         :integer          not null
#  officer           :string(255)      default(""), not null
#  officer_unit      :string(255)      default(""), not null
#  officer_badge     :string(255)      default(""), not null
#  ticket_no         :string(255)      default(""), not null
#  paid_date         :date
#  recalled_date     :date
#  created_by        :integer          default(1)
#  updated_by        :integer          default(1)
#  created_at        :datetime
#  updated_at        :datetime
#  citation_datetime :datetime
#
# Indexes
#
#  index_citations_on_created_by  (created_by)
#  index_citations_on_person_id   (person_id)
#  index_citations_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  citations_created_by_fk  (created_by => users.id)
#  citations_person_id_fk   (person_id => people.id)
#  citations_updated_by_fk  (updated_by => users.id)
#
