class UserGroup < ActiveRecord::Base
  belongs_to :user, inverse_of: :user_groups                               # constraint
  belongs_to :group, inverse_of: :user_groups                              # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'User Groups'
  end
end

# == Schema Information
#
# Table name: user_groups
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  group_id   :integer          not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_user_groups_on_group_id  (group_id)
#  index_user_groups_on_user_id   (user_id)
#
# Foreign Keys
#
#  user_groups_group_id_fk  (group_id => groups.id)
#  user_groups_user_id_fk   (user_id => users.id)
#
