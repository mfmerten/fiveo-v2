class Sentence < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :court, inverse_of: :sentences                             # constraint
  belongs_to :probation, dependent: :destroy, inverse_of: :sentence     # constraint
  belongs_to :da_charge, inverse_of: :sentence                          # constraint
  has_many :sentence_consecutives, dependent: :destroy                  # constraint
  has_many :consecutive_dockets, through: :sentence_consecutives, source: :docket  # constrained through sentence_consecutives
  has_one :hold, dependent: :destroy, inverse_of: :sentence             # constraint
  has_one :doc_record, class_name: 'Doc', dependent: :destroy, inverse_of: :sentence  # constraint
  
  scope :unprocessed, -> { where processed: false }

  attr_accessor :force, :court_cost_amount, :consecutives
  
  before_validation :lookup_court_costs, :check_probation_type
  after_create :check_docket
  before_save :process, :check_creator
  after_update :save_children

  validates_presence_of :result
  validates_date :restitution_date, :pay_by_date, allow_blank: true
  validates_numericality_of :fines, :costs, :probation_fees, :idf_amount, :dare_amount,
    :restitution_amount, greater_than_or_equal_to: 0, less_than: 10 ** 10
  validates_numericality_of :default_hours, :default_days, :default_months, :default_years,
    :sentence_hours, :sentence_days, :sentence_months, :sentence_years, :sentence_life,
    :suspended_except_hours, :suspended_except_days, :suspended_except_months,
    :suspended_except_years, :suspended_except_life, :probation_hours, :probation_days,
    :probation_months, :probation_years, :community_service_days, only_integer: true,
    greater_than_or_equal_to: 0
    
  ## Class Methods #############################################
  
  def self.base_perms
    :view_court
  end
  
  def self.desc
    'Sentences for Dockets'
  end
  
  def self.hours_served(*sentence_ids)
    sent_ids = sentence_ids.flatten.compact.uniq
    sentences = where(id: sent_ids)
    
    sholds = sentences.collect{|s| s.hold_or_doc_hold}.compact
    
    # get served time ranges
    ranges = sholds.collect{|h| h.sentence_time_range}.compact
    return 0 if ranges.empty?
    
    # get list ranges from temp release holds
    bholds = sholds.collect{|h| h.booking.temp_release_holds}.flatten.compact.uniq
    exclude_ranges = bholds.collect{|h| h.sentence_time_range}
    
    # calculate time served using TimeServed
    ts = TimeServed.new(ranges, nil_value: Time.now)
    ts.add_excludes(exclude_ranges) unless exclude_ranges.empty?
    ts.hours
  end
  
  ## Instance Methods #############################################
  
  def docket
    court.docket
  end
  
  # returns sentence hold unless nil, where it attempts to return a doc_record hold
  # if it can.  If both fail, nil is returned
  def hold_or_doc_hold
    if hold.nil?
      if doc_record.nil? || doc_record.hold.nil?
        nil
      else
        doc_record.hold
      end
    else
      hold
    end
  end
  
  def consecutives=(consecutives_string)
    docket_numbers = consecutives_string.split(/,/)
    return true if docket_numbers.empty?
    self.consecutive_dockets.clear
    docket_numbers.each do |d|
      if dckt = Docket.find_by_docket_no_and_person_id(d.trim, docket.person_id)
        self.consecutive_dockets << dckt
      end
    end
    true
  end
  
  def consecutive_holds
    return [] if consecutive_type.blank? || !sentence_consecutive?
    if consecutive_type == "Any Current"
      hold_list = []
      unless docket.person.bookings.empty? || docket.person.bookings.last.release_datetime? || docket.person.bookings.last.holds.empty?
        docket.person.bookings.last.holds.active.reject{|h| h.hold_type != 'Serving Sentence'}.each do |h|
          hold_list.push(hold)
        end
      end
      return hold_list
    elsif consecutive_type == "Other Dockets" || consecutive_type == "This Docket"
      hold_list = []
      if consecutive_type == "Other Dockets"
        docket_list = consecutive_dockets
      else
        docket_list = [docket]
      end
      docket_list.each do |d|
        d.da_charges.each do |c|
          # if the charge has not been processed, we can't calculate consecutives yet
          return nil unless c.processed?
          # if the charge does not have a sentence, skip it
          next if c.sentence.nil?
          # if the sentence is this one, skip it
          next if c.sentence.id == self.id
          # if the sentence does not have a hold or doc record, its not for serving time
          next if c.sentence.hold.nil? && c.sentence.doc_record.nil?
          if c.sentence.doc_record.nil?
            # return sentence hold
            hold_list.push(c.sentence.hold)
          else
            return nil if c.sentence.doc_record.hold.nil?
            hold_list.push(c.sentence.doc_record.hold)
          end
        end
      end
      return hold_list
    else
      # unknown consecutive_type - ignore it
      return []
    end
  end
  
  def consecutive_string(*args)
    options = HashWithIndifferentAccess.new(costs: false)
    options.update(args.extract_options!)
    cons_txt = ""
    unless consecutive_type.blank? || (!fines_consecutive && !costs_consecutive && !sentence_consecutive)
      if consecutive_type == 'Any Current'
        cons_txt << "with any current sentences."
      elsif consecutive_type == 'This Docket'
        cons_txt << "with other sentences on this docket."
      elsif consecutive_type == 'Other Dockets'
        unless consecutive_dockets.empty?
          cons = consecutive_dockets.collect{|d| d.docket_no}.join(", ")
          cons_txt << "with dockets #{cons}."
        end
      end
    end
    fines_txt = "Fines to run #{fines_consecutive? ? 'consecutive' : 'concurrent'}#{cons_txt.blank? ? '' : ' ' + cons_txt}#{fines_notes.blank? ? '' : ' (Note: ' + fines_notes + ')'}."
    costs_txt = "Costs to run #{costs_consecutive? ? 'consecutive' : 'concurrent'}#{cons_txt.blank? ? '' : ' ' + cons_txt}#{costs_notes.blank? ? '' : ' (Note: ' + costs_notes + ')'}."
    sent_txt = "Sentence to run #{sentence_consecutive? ? 'consecutive' : 'concurrent'}#{cons_txt.blank? ? '' : ' ' + cons_txt}#{sentence_notes.blank? ? '' : ' (Note: ' + sentence_notes + ')'}."
    return "#{sent_txt}#{options[:costs] ? ' ' + fines_txt + ' ' + costs_txt : ''}"
  end
  
  def default_string
    AppUtils.sentence_to_string(default_hours, default_days, default_months, default_years)
  end
  
  def hours_served
    self.class.hours_served(self.id)
  end
  
  def jail_time?
    return false if sentence_string.blank?  # no sentence entered, no time to serve
    return false if suspended? && suspended_except_string.blank?  # sentence suspended except for a specified amount
    true
  end
  
  def parish_probation?
    return false if probation_string.blank? # no probation time entered
    return false if probation_type.blank? # can't determine probation type
    return (probation_type == 'Parish')
  end
  
  def probation_string
    AppUtils.sentence_to_string(probation_hours, probation_days, probation_months, probation_years)
  end
  
  def sentence_date
    return nil if court.nil?
    court.court_date
  end
  
  def sentence_string
    AppUtils.sentence_to_string(sentence_hours, sentence_days, sentence_months, sentence_years, sentence_life, sentence_death)
  end
  
  def sentence_summary(*args)
    options = HashWithIndifferentAccess.new(with_probation: true, with_costs: false, with_conditions: false, afis: false)
    options.update(args.extract_options!)
    if options[:afis]
      options[:with_costs] = true
      options[:with_conditions] = true
      options[:with_probation] = true
    end
    txt = ''
    unless result.blank?
      case result
      when "Dismissed"
        if options[:afis]
          return "DISM"
        else
          return "Charge Dismissed"
        end
      when "Not Guilty"
        return "Found Not Guilty"
      when "Pled Guilty"
        trial_result = "Pled Guilty to"
      when "Found Guilty"
        trial_result = "Found Guilty of"
      when "No Contest"
        trial_result = "Pled No Contest to"
      else
        trial_result = result
      end
    end
    txt << "#{trial_result} #{conviction_count} #{conviction_count == 1 ? 'count' : 'counts'} #{conviction != da_charge.charge ? 'of amended charge ' : ''}#{conviction}"
    unless txt.slice(-1,1) == '.'
      txt << '.'
    end
    unless sentence_string.blank?
      txt << " Sentenced to #{sentence_string}"
      if doc? && !parish_jail?
        txt << " at hard labor Louisiana Dept. of Corrections"
      elsif doc? && parish_jail?
        txt << " in Parish jail for Louisiana Dept. of Corrections"
      else
        txt << " in Parish jail"
      end
      if wo_benefit?
        txt << " without benefit of parole"
      end
      if deny_goodtime?
        txt << ", without benefit of 'Good Time'"
      end
      if credit_time_served?
        txt << ", with credit for time served"
      end
      if options[:with_probation]
        if suspended?
          txt << ", suspended"
          unless suspended_except_string.blank?
            txt << " except #{suspended_except_string}"
          end
          unless probation_string.blank?
            txt << ", and placed on #{probation_string} #{probation_type.blank? ? '' : probation_type + ' '}Probation"
            if probation_reverts?
              txt << " reverting to Unsupervised"
            end
            if probation_revert_conditions?
              txt << " after #{probation_revert_conditions}"
            end
          end
          txt << "."
        end
      end
      unless consecutive_type.blank?
        txt << " #{consecutive_string}"
      end
    end
    if options[:with_costs]
      if fines + costs > 0
        txt << " Must pay $#{sprintf("%.2f", fines + costs)} in Fines and Court Costs"
        if pay_by_date?
          txt << " by #{AppUtils.format_date(pay_by_date)}"
        elsif pay_during_prob?
          txt << " during probation"
        end
        if default_string.blank?
          txt << "."
        else
          txt << " or serve #{default_string} in Parish Jail."
        end
      end
    end
    if options[:with_conditions]
      conditions = []
      if restitution_amount?
        cond_txt = "pay $#{sprintf("%.2f", restitution_amount)} Restitution"
        if restitution_date?
          cond_txt << " by #{AppUtils.format_date(restitution_date)}"
        end
        conditions.push(cond_txt)
      end
      if dare_amount?
        conditions.push("pay $#{sprintf("%.2f", dare_amount)} to DARE")
      end
      if idf_amount?
        conditions.push("pay $#{sprintf("%.2f", idf_amount)} to IDF")
      end
      if random_drug_screens?
        conditions.push("random drug testing")
      end
      if no_victim_contact?
        conditions.push("no contact with victim")
      end
      if substance_abuse_program?
        conditions.push("substance abuse program")
      end
      if substance_abuse_treatment?
        conditions.push("substance abuse treatment")
      end
      if anger_management?
        conditions.push("anger management program")
      end
      if driver_improvement?
        conditions.push("driver improvement program")
      end
      if community_service_hours?
        conditions.push("#{community_service_hours} hrs com. service")
      end
      if community_service_days?
        conditions.push("#{community_service_days} days com. service")
      end
      unless conditions.empty?
        txt << " With Special Conditions: #{conditions.join(', ')}."
      end
    end
    if options[:afis]
      abbrev_for_afis(txt)
    else
      txt
    end
  end
  
  def suspended_except_string
    AppUtils.sentence_to_string(suspended_except_hours, suspended_except_days, suspended_except_months, suspended_except_years, suspended_except_life)
  end
  
  def total_hours
    if suspended?
      AppUtils.sentence_to_hours(suspended_except_hours, suspended_except_days, suspended_except_months, suspended_except_years, suspended_except_life)
    else
      AppUtils.sentence_to_hours(sentence_hours, sentence_days, sentence_months, sentence_years, sentence_life, sentence_death)
    end
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  # this attempts to abbreviate what it can according to the letter from
  # State of Louisiana, Public Safety Services detailing the requirements
  # for the AFIS final disposition report (green form)
  def abbrev_for_afis(charge_string)
    # Active = ACT
    charge_string = charge_string.gsub(/\bactive\b/i, 'ACT')
    # Bench Probation = BP
    charge_string = charge_string.gsub(/\bbench probation\b/i, 'BP')
    # Court Costs = CC
    charge_string = charge_string.gsub(/\bcourt costs?\b/i, 'CC')
    # Controlled Dangerous Substance = CDS
    charge_string = charge_string.gsub(/\bcontrolled dangerous substance\b/i, 'CDS')
    # Credit for time served / with credit for time served = CFTS
    charge_string = charge_string.gsub(/\b(with )?credit (for )?time served\b/i, 'CFTS')
    # Court = CRT
    charge_string = charge_string.gsub(/\bcourt\b/i, 'CRT')
    # Consecutive = CS
    charge_string = charge_string.gsub(/\bconsecutive\b/i, 'CS')
    # Counts = CTS
    charge_string = charge_string.gsub(/\bcounts?\b/i, 'CTS')
    # Dismissed = DISM
    charge_string = charge_string.gsub(/\bdismissed\b/i, 'DISM')
    # Drivers license = D/L (also OLN needs to become D/L)
    charge_string = charge_string.gsub(/\bdriver'?s license\b/i, 'D/L').gsub(/\boln\b/i, 'D/L')
    # Date of Arrest (or arrest date) (or date arrested) = DOA
    charge_string = charge_string.gsub(/\bdate (of )?arrest(ed)?\b/i, 'DOA').gsub(/\barrest date\b/i, 'DOA')
    # L(ouisian)a dep(artmen)t of corrections = LADOC
    charge_string = charge_string.gsub(/\bl(ouisian)?a dep(artmen)?t\.? of corrections\b/i, 'LADOC')
    # Dept (or department) of corrections = DOC (also D.O.C. should become DOC)
    charge_string = charge_string.gsub(/\bdep(artmen)?t\.? of corrections\b/i, 'DOC').gsub(/\bd\.o\.c\.\b/i, 'DOC')
    # Found Guilty as charged = FGAC, Found Guilty = FG
    charge_string = charge_string.gsub(/\bfound guilty as charged\b/i, 'FGAC').gsub(/\bfound guilty\b/i, 'FG')
    # hard labor = H/L
    charge_string = charge_string.gsub(/\bhard labor\b/i, 'H/L')
    # inactive = INACT
    charge_string = charge_string.gsub(/\binactive\b/i, 'INACT')
    # pre(-)sentence investigation = PSI
    charge_string = charge_string.gsub(/\bpre[ -]sentence investigation\b/i, 'PSI')
    # investigation = INVEST
    charge_string = charge_string.gsub(/\binvestigation\b/i, 'INVEST')
    # imposition = IMP
    charge_string = charge_string.gsub(/\bimposition\b/i, 'IMP')
    # month(s) = Mo(s), month = Mo
    charge_string = charge_string.gsub(/\bmonth(s)?\b/i, 'Mo\1')
    # nolle prosse = NP
    charge_string = charge_string.gsub(/\bnolle prosse\b/i, 'NP')
    # parole = PAROLE
    charge_string = charge_string.gsub(/\bparole\b/i, 'PAROLE')
    # police action sufficient = PAS
    charge_string = charge_string.gsub(/\bpolice action sufficient\b/i, 'PAS')
    # pled guilty as charges = PGAC, pled guilty = PG
    charge_string = charge_string.gsub(/\bpled guilty as charged\b/i, 'PGAC').gsub(/\bpled guilty\b/i, 'PG')
    # parish jail = PJ
    charge_string = charge_string.gsub(/\bparish jail\b/i, 'PJ')
    # parish prison = PP
    charge_string = charge_string.gsub(/\bparish prison\b/i, 'PP')
    # probation = PROB
    charge_string = charge_string.gsub(/\bprobation\b/i, 'PROB')
    # restitution = RESTIT
    charge_string = charge_string.gsub(/\brestitution\b/i, 'RESTIT')
    # revoked = REVOKED
    charge_string = charge_string.gsub(/\brevoked\b/i, 'REVOKED')
    # (sentence )suspended = SS
    charge_string = charge_string.gsub(/\b(sentence )?suspended\b/i, 'SS')
    # sentence = SENT
    charge_string = charge_string.gsub(/\bsentenced?\b/i, 'SENT')
    # state penitentiary = SPEN
    charge_string = charge_string.gsub(/\bstate penitentiary\b/i, 'SPEN')
    # supervised = SUPV
    charge_string = charge_string.gsub(/\bsupervised\b/i, 'SUPV')
    # terminated = TERMINATED
    charge_string = charge_string.gsub(/\bterminated\b/i, 'TERMINATED')
    # unsupervised = UNSUPV
    charge_string = charge_string.gsub(/\bunsupervised\b/i, 'UNSUPV')
    # with intent to distribute = WITD
    charge_string = charge_string.gsub(/\bwith intent to dist(ribute|\.)?\b/i, 'WITD')
    # with special conditions = W/SC
    charge_string = charge_string.gsub(/\b(with )?special conditions\b/i, 'W/SC')
    # Year(s) = Yr(s)
    charge_string = charge_string.gsub(/\byear(s)?\b/i, 'Yr\1') 
    # and placed on = with  ---- I added this one to shorten the probation part a bit
    charge_string = charge_string.gsub(/\band placed on\b/i, 'with')
    charge_string
  end
  
  def check_docket
    unless processed?
      if docket.processed?
        self.docket.update_attribute(:processed, false)
      end
      if court.processed?
        self.court.update_attribute(:processed, false)
      end
    end
    true
  end
  
  def check_probation_type
    if !probation_type.blank?
      unless (probation_hours? && probation_hours > 0) || (probation_days? && probation_days > 0) || (probation_years? && probation_years > 0) || (probation_months? && probation_months > 0)
        self.errors.add(:probation_type, "must be blank (---) if there is no probation time specified.")
        self.errors.add(:probation_hours, "must be specified when a probation type is selected.")
        self.errors.add(:probation_days, "must be specified when a probation type is selected.")
        self.errors.add(:probation_months, "must be specified when a probation type is selected.")
        self.errors.add(:probation_years, "must be specified when a probation type is selected.")
        return false
      end
    else
      if (probation_hours? && probation_hours > 0) || (probation_days? && probation_days > 0) || (probation_years? && probation_years > 0) || (probation_months? && probation_months > 0)
        self.errors.add(:probation_type, "cannot be blank (---) if there is probation time specified.")
        if probation_hours? && probation_hours > 0
          self.errors.add(:probation_hours, "cannot be specified when the probation type is blank (---).")
        end
        if probation_days? && probation_days > 0
          self.errors.add(:probation_days, "cannot be specified when the probation type is blank (---).")
        end
        if probation_months? && probation_months > 0
          self.errors.add(:probation_months, "cannot be specified when the probation type is blank (---).")
        end
        if probation_years? && probation_years > 0
          self.errors.add(:probation_years, "cannot be specified when the probation type is blank (---).")
        end
        return false
      end
    end
    true
  end
  
  def lookup_court_costs
    unless court_cost_amount.blank?
      self.costs = BigDecimal.new(court_cost_amount.to_s,2)
    end
    true
  end
  
  def process
    # since this record has possibly been altered, clear the processed
    # flag.
    self.processed = false
    # clear old status from process status field
    self.process_status = ""
    
    # at this point, need to make sure any attached docket an/or court
    # record get their processed flag unset as well
    unless docket.nil? || !docket.processed?
      docket.update_attribute(:processed, false)
    end
    unless court.nil? || !court.processed?
      court.update_attribute(:processed, false)
    end
    unless da_charge.nil? || !da_charge.processed?
      da_charge.update_attribute(:processed, false)
    end
    
    # default to everything ok
    success = true
    
    # check for prerequisits
    if docket.id != da_charge.docket_id
      success = false
      self.process_status << "[Charge for Different Docket]"
    end
    
    # update probation if necessary
    if success && !probation_string.blank?
      p = probation
      if p.nil?
        unless da_charge.nil?
          p = da_charge.probation
        end
        if p.nil?
          p = self.build_probation
        else
          self.probation_id = p.id
        end
      end
      
      # get appropriate probation status
      case
      when probation_type.blank? || probation_type == 'Parish'
        stat = "Active"
        hif = true
      when probation_type == 'State'
        stat = "State"
        hif = true
      when probation_type == 'Unsupervised'
        stat = "Unsupervised"
        hif = false
      end
      
      # calculate probation hours
      total_hours = AppUtils.sentence_to_hours(probation_hours, probation_days, probation_months, probation_years)
      
      p.person_id = docket.person_id
      # if probation is already completed, don't mess with status
      if p.status == "Completed"
        # just turn off hold_if_arrested flag
        p.hold_if_arrested = false
      else
        # new probation or status is not completed... process normally
        p.status = stat
        p.hold_if_arrested = hif
      end
      p.start_date = sentence_date
      p.end_date = sentence_date + total_hours.hours
      p.conviction = conviction
      p.conviction_count = conviction_count
      p.probation_hours = probation_hours
      p.probation_days = probation_days
      p.probation_months = probation_months
      p.probation_years = probation_years
      p.sentence_notes = consecutive_string + (notes.blank? ? '' : ' -- Additional Sentence Notes: ' + notes)
      unless p.created_by?
        p.created_by = updated_by
      end
      p.updated_by = updated_by
      p.probation_fees = probation_fees
      p.docket_id = docket.id
      p.docket_no = docket.docket_no
      p.court_id = court_id
      p.da_charge_id = da_charge_id
      p.doc = doc
      p.parish_jail = parish_jail
      p.trial_result = (result.blank? ? 'Unknown' : result)
      p.costs = costs
      p.fines = fines
      p.default_hours = default_hours
      p.default_days = default_days
      p.default_months = default_months
      p.default_years = default_years
      p.pay_by_date = pay_by_date
      p.pay_during_prob = pay_during_prob
      p.sentence_hours = sentence_hours
      p.sentence_days = sentence_days
      p.sentence_months = sentence_months
      p.sentence_years = sentence_years
      p.sentence_life = sentence_life
      p.sentence_death = sentence_death
      p.sentence_suspended = suspended
      p.suspended_except_hours = suspended_except_hours
      p.suspended_except_days = suspended_except_days
      p.suspended_except_months = suspended_except_months
      p.suspended_except_years = suspended_except_years
      p.suspended_except_life = suspended_except_life
      p.reverts_to_unsup = probation_reverts
      p.reverts_conditions = probation_revert_conditions
      p.credit_served = credit_time_served
      p.service_days = community_service_days
      p.service_hours = community_service_hours
      p.restitution_amount = restitution_amount
      p.restitution_date = restitution_date
      p.idf_amount = idf_amount
      p.dare_amount = dare_amount
      p.sap = substance_abuse_program
      p.driver = driver_improvement
      p.anger = anger_management
      p.sat = substance_abuse_treatment
      p.random_screens = random_drug_screens
      p.no_victim_contact = no_victim_contact
      p.art893 = art_893
      p.art894 = art_894
      p.art895 = art_895
      p.remarks = 'Automatically added by sentence processing.'
      unless p.valid?
        success = false
        self.process_status << "[Probation Has Errors]"
        self.process_status << "(#{p.errors.full_messages.join(', ')})"
      end
    end
    
    # Check for jail
    if success && jail_time?
      # validate booking
      if docket.person.bookings.empty? || (hold.nil? && docket.person.bookings.last.release_datetime?)
        # if delay_execution set, need to create booking record
        if force == true && (docket.person.bookings.empty? || (docket.person.bookings.last.release_datetime.to_date < court.court_date))
          # creating a booking because:
          # 1. forcing processing and no bookings exist
          # 2. forcing processing and last booking was released before the court date
          book = Booking.new(person_id: docket.person_id, disable_timers: true, created_by: updated_by, updated_by: updated_by, remarks: 'Created Automatically By Sentence Processing')
          book.booking_datetime = Time.now
          book.release_datetime = book.booking_datetime
          book.release_officer = book.booking_officer
          book.release_officer_unit = book.booking_officer_unit
          book.release_officer_badge = book.booking_officer_badge
          book.released_because = "Automatic Release"
          book.remarks = 'Created and Released Automatically By Sentence Processing.'
          book.save(validate: false)
        elsif force == true && docket.person.bookings.last.release_datetime?
          book = docket.person.bookings.last
        else
          success = false
          self.process_status << '[Invalid Booking]'
        end
      else
        # using last booking because its still open, or we are forcing processing
        book = docket.person.bookings.last
      end
      
      if success
        # should be ready now to post the jail hold
        
        if doc? && (!suspended? || (suspended? && !parish_jail?))
          # Hard Labor Dept of Corrections
          
          # check to make sure there is not a parish hold already
          if !hold.nil? && !hold.doc?
            # oops, get rid of it
            hold.destroy
          end
          
          # now place a doc record which should re-add the hold
          if suspended?
            total_hours = AppUtils.sentence_to_hours(suspended_except_hours, suspended_except_days, suspended_except_months, suspended_except_years)
          else
            total_hours = AppUtils.sentence_to_hours(sentence_hours, sentence_days, sentence_months, sentence_years)
          end
          if total_hours <= 0
            total_hours = 0
          end
          if credit_time_served?
            # subtract time served on this charge from total hours to be served
            time_credit = da_charge.time_served_in_hours
            # if life or death sentence, this is silly
            if time_credit > 0 && sentence_life == 0 && !sentence_death
              total_hours = total_hours - time_credit
              rem = remarks + "\nTo Serve time reflects #{time_credit} Hours credit for time served."
            end
          end
          
          d = doc_record
          if d.nil?
            d = self.build_doc_record
          end
          d.sentence_hours = sentence_hours
          d.sentence_days = sentence_days
          d.sentence_months = sentence_months
          d.sentence_years = sentence_years
          d.sentence_life = sentence_life
          d.sentence_death = sentence_death
          d.conviction = conviction_count.to_s + " count " + conviction
          d.remarks = (rem.nil? ? remarks : remarks + rem)
          unless d.created_by?
            d.created_by = updated_by
          end
          d.updated_by = updated_by
          d.booking_id = book.id
          d.person_id = docket.person_id
          d.to_serve_hours, d.to_serve_days, d.to_serve_months, d.to_serve_years = AppUtils.hours_to_sentence(total_hours)
          if sentence_life > 0
            d.to_serve_life = sentence_life
          end
          d.to_serve_death = sentence_death
          if force == true
            # if we are forcing, tell doc to use a closed booking
            d.force = true
          end
          unless d.valid?
            self.process_status << '[Unable to Create DOC Record]'
            self.process_status << "(#{d.errors.full_messages.join(', ')})"
            success = false
          end
        else
          # Parish Jail
          
          # make sure there is not a doc_record already
          unless doc_record.nil?
            # oops, get rid of it
            doc_record.destroy
          end
          
          # calculate total hours
          if suspended?
            total_hours = AppUtils.sentence_to_hours(suspended_except_hours, suspended_except_days, suspended_except_months, suspended_except_years, suspended_except_life)
          else
            total_hours = AppUtils.sentence_to_hours(sentence_hours, sentence_days, sentence_months, sentence_years, sentence_life, sentence_death)
          end
          if total_hours <= 0
            total_hours = 0
          end
          
          # get consecutives
          consecs = consecutive_holds
          if consecs.nil?
            # not ready for processing
            self.process_status << "[Sentence consecutive to unprocessed sentences]"
            success = false
          else
            served = book.hours_served_since([AppUtils.get_datetime(court.court_date, "12:00"),book.booking_datetime].sort.last)
            h = hold(true)
            if h.nil?
              h = self.build_hold
            end
            h.booking_id = book.id
            h.hold_datetime = AppUtils.get_datetime(court.court_date, "12:00")
            h.hold_type = "Serving Sentence"
            h.hold_by = updater.sort_name
            h.hold_by_unit = updater.unit
            h.hold_by_badge = updater.badge
            h.remarks = "Automatically Added by Sentence Processing."
            unless h.created_by?
              h.created_by = updated_by
            end
            h.updated_by = updated_by
            unless h.hours_served? && h.hours_served > 0
              h.hours_served = served
            end
            h.hours_total = total_hours
            if !suspended? && sentence_death?
              h.death_sentence = true
            end
            if credit_time_served?
              unless h.hours_time_served? && h.hours_time_served > 0
                ts = da_charge.time_served_in_hours
                if ts > 0
                  h.hours_time_served = ts
                else
                  h.hours_time_served = 0
                end
              end
            else
              h.hours_time_served = 0
            end
            unless h.datetime_counted?
              h.datetime_counted = Time.now
            end
            if force == true
              h.cleared_datetime = h.hold_datetime
              h.cleared_because = "Automatic Release"
              h.cleared_by = h.hold_by
              h.cleared_by_unit = h.hold_by_unit
              h.cleared_by_badge = h.hold_by_badge
              h.explanation = 'Inmate Released Prior To Sentence Processing because of time served or transfer.'
              h.remarks = 'Automatically Added And Cleared by Sentence Processing.'
            end
            h.deny_goodtime = deny_goodtime
            if h.valid?
              # need to close all pending sentence holds, if any
              pendings = book.holds.where({cleared_datetime: nil, hold_type: "Pending Sentence"}).all
              unless pendings.empty?
                pendings.each do |p|
                  p.cleared_datetime = Time.now
                  p.cleared_because = 'Automatic Release'
                  p.cleared_by = updater.sort_name
                  p.cleared_by_unit = updater.unit
                  p.cleared_by_badge = updater.badge
                  p.explanation = "Automatically cleared by Sentence Processing."
                  p.updated_by = updater
                  p.save(validate: false)
                end
              end
            else
              self.process_status << '[Unable to Create Sentence Hold]'
              self.process_status << "(#{h.errors.full_messages.join(', ')})"
              success = false
            end
          end
        end
      end
    end
    if success
      self.processed = true
      self.process_status = ""
      da_charge.update_attribute(:processed, true)
    end
    self.force = false
    true
  end
  
  def save_children
    if doc? && (!suspended? || (suspended? && !parish_jail?))
      unless doc_record.nil?
        self.doc_record.save(validate: false)
      end
    end
    unless hold.nil?
      self.hold.save(validate: false)
      self.hold.blockers.clear
      c = consecutive_holds
      unless c.empty? || c.nil?
        self.hold.blockers << c
      end
    end
  end
end

# == Schema Information
#
# Table name: sentences
#
#  id                          :integer          not null, primary key
#  court_id                    :integer          not null
#  probation_id                :integer
#  da_charge_id                :integer          not null
#  doc                         :boolean          default(FALSE), not null
#  fines                       :decimal(12, 2)   default(0.0), not null
#  costs                       :decimal(12, 2)   default(0.0), not null
#  default_days                :integer          default(0), not null
#  default_months              :integer          default(0), not null
#  default_years               :integer          default(0), not null
#  pay_by_date                 :date
#  sentence_days               :integer          default(0), not null
#  sentence_months             :integer          default(0), not null
#  sentence_years              :integer          default(0), not null
#  suspended                   :boolean          default(FALSE), not null
#  suspended_except_days       :integer          default(0), not null
#  suspended_except_months     :integer          default(0), not null
#  suspended_except_years      :integer          default(0), not null
#  probation_days              :integer          default(0), not null
#  probation_months            :integer          default(0), not null
#  probation_years             :integer          default(0), not null
#  credit_time_served          :boolean          default(FALSE), not null
#  community_service_days      :integer          default(0), not null
#  substance_abuse_program     :boolean          default(FALSE), not null
#  driver_improvement          :boolean          default(FALSE), not null
#  probation_fees              :decimal(12, 2)   default(0.0), not null
#  idf_amount                  :decimal(12, 2)   default(0.0), not null
#  dare_amount                 :decimal(12, 2)   default(0.0), not null
#  restitution_amount          :decimal(12, 2)   default(0.0), not null
#  anger_management            :boolean          default(FALSE), not null
#  substance_abuse_treatment   :boolean          default(FALSE), not null
#  random_drug_screens         :boolean          default(FALSE), not null
#  no_victim_contact           :boolean          default(FALSE), not null
#  art_893                     :boolean          default(FALSE), not null
#  art_894                     :boolean          default(FALSE), not null
#  art_895                     :boolean          default(FALSE), not null
#  probation_reverts           :boolean          default(FALSE), not null
#  probation_revert_conditions :string(255)      default(""), not null
#  wo_benefit                  :boolean          default(FALSE), not null
#  sentence_consecutive        :boolean          default(FALSE), not null
#  sentence_notes              :string(255)      default(""), not null
#  fines_consecutive           :boolean          default(FALSE), not null
#  fines_notes                 :string(255)      default(""), not null
#  costs_consecutive           :boolean          default(FALSE), not null
#  costs_notes                 :string(255)      default(""), not null
#  notes                       :text             default(""), not null
#  remarks                     :text             default(""), not null
#  processed                   :boolean          default(FALSE), not null
#  created_by                  :integer          default(1)
#  updated_by                  :integer          default(1)
#  created_at                  :datetime
#  updated_at                  :datetime
#  parish_jail                 :boolean          default(FALSE), not null
#  conviction                  :string(255)      default(""), not null
#  conviction_count            :integer          default(1), not null
#  restitution_date            :date
#  legacy                      :boolean          default(FALSE), not null
#  process_status              :string(255)      default(""), not null
#  default_hours               :integer          default(0), not null
#  sentence_hours              :integer          default(0), not null
#  suspended_except_hours      :integer          default(0), not null
#  probation_hours             :integer          default(0), not null
#  delay_execution             :boolean          default(FALSE), not null
#  community_service_hours     :integer          default(0), not null
#  deny_goodtime               :boolean          default(FALSE), not null
#  sentence_life               :integer          default(0), not null
#  sentence_death              :boolean          default(FALSE), not null
#  suspended_except_life       :integer          default(0), not null
#  consecutive_type            :string(255)      default(""), not null
#  probation_type              :string(255)      default(""), not null
#  result                      :string(255)      default(""), not null
#  pay_during_prob             :boolean          default(FALSE), not null
#
# Indexes
#
#  index_sentences_on_charge_id     (da_charge_id)
#  index_sentences_on_court_id      (court_id)
#  index_sentences_on_created_by    (created_by)
#  index_sentences_on_probation_id  (probation_id)
#  index_sentences_on_updated_by    (updated_by)
#
# Foreign Keys
#
#  sentences_court_id_fk      (court_id => courts.id)
#  sentences_created_by_fk    (created_by => users.id)
#  sentences_da_charge_id_fk  (da_charge_id => da_charges.id)
#  sentences_probation_id_fk  (probation_id => probations.id)
#  sentences_updated_by_fk    (updated_by => users.id)
#
