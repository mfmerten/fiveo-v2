class SentenceConsecutive < ActiveRecord::Base
  belongs_to :sentence, inverse_of: :sentence_consecutives                 # constraint
  belongs_to :docket, inverse_of: :sentence_consecutives                   # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'Consecutive Dockets for Sentences'
  end
end

# == Schema Information
#
# Table name: sentence_consecutives
#
#  id          :integer          not null, primary key
#  docket_id   :integer          not null
#  sentence_id :integer          not null
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_sentence_consecutives_on_docket_id    (docket_id)
#  index_sentence_consecutives_on_sentence_id  (sentence_id)
#
# Foreign Keys
#
#  sentence_consecutives_docket_id_fk    (docket_id => dockets.id)
#  sentence_consecutives_sentence_id_fk  (sentence_id => sentences.id)
#
