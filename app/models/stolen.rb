class Stolen < ActiveRecord::Base
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'    # constraint with nullify
  belongs_to :person, inverse_of: :stolens                              # constraint
  has_many :invest_stolens, dependent: :destroy                         # constraint
  has_many :investigations, through: :invest_stolens                    # constrained through invest_stolens
  has_many :crimes, through: :invest_stolens, source: :crimes           # constrained through invest_stolens
  has_many :criminals, through: :invest_stolens, source: :crimes        # constrained through invest_stolens

  scope :active, -> { where recovered_date: nil }
  scope :by_reverse_date, -> { order stolen_date: :desc }
  scope :with_case_no, ->(c_no) { where case_no: c_no }
  
  # filter scopes
  scope :with_stolen_id, ->(s_id) { where id: s_id }
  # with_case_no defined above
  scope :with_person_id, ->(p_id) { where person_id: p_id }
  scope :with_person_name, ->(p_name) { AppUtils.for_person self, name, association: :person }
  scope :with_item, ->(i) { AppUtils.like self, :item, i }
  scope :with_model_no, ->(m_no) { AppUtils.like self, :model_no, m_no }
  scope :with_serial_no, ->(s_no) { AppUtils.like self, :serial_no, s_no }
  scope :with_item_description, ->(des) { AppUtils.like self, :item_description, des }
  scope :with_stolen_date, ->(start,stop) { AppUtils.for_daterange self, :stolen_date, start: start, stop: stop, date: true }
  scope :with_is_recovered, ->(r) { AppUtils.for_null self, :recovered_date, r }
  scope :with_recovered_date, ->(start,stop) { AppUtils.for_daterange self, :recovered_date, start: start, stop: stop, date: true }
  scope :with_is_returned, ->(r) { AppUtils.for_null self, :returned_date, r }
  scope :with_returned_date, ->(start,stop) { AppUtils.for_daterange self, :returned_date, start: start, stop: stop, date: true }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  
  before_save :check_creator
  before_validation :adjust_case_no

  validates_numericality_of :item_value, greater_than_or_equal_to: 0, less_than: 10 ** 10
  validates_date :stolen_date, :recovered_date, :returned_date, allow_blank: true
  validates_person :person_id
  
  ## Class Methods #############################################
  
  def self.desc
    "Stolen Items Database"
  end
  
  def self.base_perms
    :view_stolen
  end
  
  # process the filter query
  def self.process_query(query)
    return self unless query.is_a?(Hash)
    result = self
    result = result.with_stolen_id(query[:id]) unless query[:id].blank?
    result = result.with_case_no(query[:case_no]) unless query[:case_no].blank?
    result = result.with_person_id(query[:person_id]) unless query[:person_id].blank?
    result = result.with_person_name(query[:person_name]) unless query[:person_name].blank?
    result = result.with_item(query[:item]) unless query[:item].blank?
    result = result.with_model_no(query[:model_no]) unless query[:model_no].blank?
    result = result.with_serial_no(query[:serial_no]) unless query[:serial_no].blank?
    result = result.with_item_description(query[:item_description]) unless query[:item_description].blank?
    result = result.with_stolen_date(query[:stolen_from],query[:stolen_to]) unless query[:stolen_from].blank? && query[:stolen_to].blank?
    result = result.with_is_recovered(query[:is_recovered]) unless query[:is_recovered].blank?
    result = result.with_recovered_date(query[:recovered_from],query[:recovered_to]) unless query[:recovered_from].blank? && query[:recovered_to].blank?
    result = result.with_is_returned(query[:is_returned]) unless query[:is_returned].blank?
    result = result.with_returned_date(query[:returned_from],query[:returned_to]) unless query[:returned_from].blank? && query[:returned_to].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result
  end
    
  private
  
  ## Private Instance Methods #############################################
  
  def adjust_case_no
    self.case_no = AppUtils.fix_case(case_no)
    true
  end
end

# == Schema Information
#
# Table name: stolens
#
#  id               :integer          not null, primary key
#  case_no          :string(255)      default(""), not null
#  person_id        :integer          not null
#  stolen_date      :date
#  model_no         :string(255)      default(""), not null
#  serial_no        :string(255)      default(""), not null
#  item_value       :decimal(12, 2)   default(0.0), not null
#  item             :string(255)      default(""), not null
#  item_description :text             default(""), not null
#  recovered_date   :date
#  returned_date    :date
#  remarks          :text             default(""), not null
#  created_by       :integer          default(1)
#  updated_by       :integer          default(1)
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_stolens_on_case_no     (case_no)
#  index_stolens_on_created_by  (created_by)
#  index_stolens_on_person_id   (person_id)
#  index_stolens_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  stolens_created_by_fk  (created_by => users.id)
#  stolens_person_id_fk   (person_id => people.id)
#  stolens_updated_by_fk  (updated_by => users.id)
#
