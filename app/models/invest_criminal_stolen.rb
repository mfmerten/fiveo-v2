class InvestCriminalStolen < ActiveRecord::Base
  belongs_to :invest_criminal, inverse_of: :invest_criminal_stolens        # constraint
  belongs_to :invest_stolen, inverse_of: :invest_criminal_stolens          # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'InvestCriminal Stolens'
  end
end

# == Schema Information
#
# Table name: invest_criminal_stolens
#
#  id                 :integer          not null, primary key
#  invest_criminal_id :integer          not null
#  invest_stolen_id   :integer          not null
#  created_at         :datetime
#  updated_at         :datetime
#
# Indexes
#
#  index_invest_criminal_stolens_on_invest_criminal_id  (invest_criminal_id)
#  index_invest_criminal_stolens_on_invest_stolen_id    (invest_stolen_id)
#
# Foreign Keys
#
#  invest_criminal_stolens_invest_criminal_id_fk  (invest_criminal_id => invest_criminals.id)
#  invest_criminal_stolens_invest_stolen_id_fk    (invest_stolen_id => invest_stolens.id)
#
