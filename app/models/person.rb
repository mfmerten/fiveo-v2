class Person < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: "User", foreign_key: 'created_by'    # constraint with nullify
  belongs_to :updater, class_name: "User", foreign_key: 'updated_by'    # constraint with nullify
  has_many :marks, class_name: 'PersonMark', dependent: :destroy        # constraint
  has_many :associates, class_name: 'PersonAssociate', dependent: :destroy  # constraint
  has_many :arrests, dependent: :destroy                                # constraint
  has_many :forbids, class_name: "Forbid", foreign_key: "forbidden_id", dependent: :destroy  # constraint
  has_many :active_forbids, -> { active }, class_name: "Forbid", foreign_key: 'forbidden_id'
  has_many :warrants, dependent: :nullify                               # constraint
  has_many :active_warrants, -> { active }, class_name: 'Warrant'
  has_many :bonds, dependent: :destroy                                  # constraint
  has_many :bonds_as_surety, class_name: 'Bond', foreign_key: 'surety_id', dependent: :nullify  # constraint
  has_many :bookings, dependent: :destroy                               # constraint
  has_many :transports, dependent: :destroy                             # constraint
  has_many :evidences, foreign_key: 'owner_id', dependent: :nullify     # constraint
  has_many :probations, dependent: :destroy                             # constraint
  has_many :active_probations, -> { where status: ['State','Unsupervised','Active]'] }, class_name: 'Probation' # constrained through probations
  has_many :stolens, dependent: :destroy                                # constraint
  has_many :dockets, dependent: :destroy                                # constraint
  has_many :citations, dependent: :destroy                              # constraint
  belongs_to :alias_for, class_name: 'Person', foreign_key: 'is_alias_for', inverse_of: :aliases  # constraint
  has_many :aliases, class_name: 'Person', foreign_key: 'is_alias_for', dependent: :nullify  # constraint
  has_many :warrant_exemptions, dependent: :destroy                     # constraint
  has_many :exempted_warrants, through: :warrant_exemptions, source: :warrant  # constrained through warrant_exemptions
  has_many :forbid_exemptions, dependent: :destroy                      # constraint
  has_many :exempted_forbids, through: :forbid_exemptions, source: :forbid  # constrained through forbid_exemptions
  has_many :warrant_possibles, dependent: :destroy
  has_many :possible_warrants, through: :warrant_possibles, source: :warrant
  has_many :forbid_possibles, dependent: :destroy
  has_many :possible_forbids, through: :forbid_possibles, source: :forbid
  has_many :vehicles, class_name: 'InvestVehicle', foreign_key: 'owner_id', dependent: :nullify  # constraint
  has_many :transfers, dependent: :destroy                              # constraint
  has_many :docs, dependent: :destroy                                   # constraint
  has_many :medications, dependent: :destroy                            # constraint
  has_many :commissaries, dependent: :destroy                           # constraint
  has_many :dispositions, dependent: :destroy                           # constraint
  has_many :offense_victims, dependent: :destroy                        # constraint
  has_many :victim_offenses, through: :offense_victims, source: :offense  # constrained through offense_victims
  has_many :offense_witnesses, dependent: :destroy                      # constraint
  has_many :witness_offenses, through: :offense_witnesses, source: :offense  # constrained through offense_witnesses
  has_many :offense_suspects, dependent: :destroy                       # constraint
  has_many :suspect_offenses, through: :offense_suspects, source: :offense  # constrained through offense_suspects
  has_many :invest_vehicles, foreign_key: 'owner_id', dependent: :nullify  # constraint
  has_many :invest_criminals, dependent: :nullify                       # constraint
  has_many :invest_witnesses, dependent: :destroy                       # constraint
  has_many :invest_victims, dependent: :destroy                         # constraint
  has_many :invest_sources, dependent: :destroy                         # constraint
  
  ## Attrubutes #############################################
  
  accepts_nested_attributes_for :associates, allow_destroy: true, reject_if: ->(a) { a['name'].blank? }
  accepts_nested_attributes_for :marks, allow_destroy: true, reject_if: ->(m) { m['location'].blank? }

  attr_encrypted :ssn
  attr_accessor :full_name, :photo_form
  has_attached_file(
    :front_mugshot,
    default_url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/default_images/missing_:style.jpg",
    url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/:attachment/:id/:style/:basename.:extension",
    path: ":rails_root/public/system/:attachment/:id/:style/:basename.:extension",
    styles: {thumb: ["80x80", :jpg], medium: ["300x300",:jpg], small: ["150x150",:jpg]},
    default_style: :original,
    storage: :filesystem
  )
  has_attached_file(
    :side_mugshot,
    default_url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/default_images/missing_:style.jpg",
    url: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/system/:attachment/:id/:style/:basename.:extension",
    path: ":rails_root/public/system/:attachment/:id/:style/:basename.:extension",
    styles: {thumb: ["80x80", :jpg], medium: ["300x300",:jpg], small: ["150x150",:jpg]},
    default_style: :original,
    storage: :filesystem
  )
  
  ## Scopes #############################################
  
  scope :by_name, -> { order :family_name, :given_name }
  scope :in_jail, ->(jail_id) { joins(:bookings).where('bookings.release_datetime IS NULL AND bookings.jail_id = ?', jail_id) }
  scope :with_front_mugshot, -> { where "people.front_mugshot_file_name != '' AND people.front_mugshot_file_name IS NOT NULL" }
  
  # filter scopes
  scope :with_person_id, ->(p_id) { where id: p_id }
  scope :with_name, ->(n) { AppUtils.for_person self, n }
  scope :with_aka, ->(a) { AppUtils.like self, :aka, a }
  scope :with_street, ->(s) { AppUtils.like self, [:phys_street, :mail_street], s }
  scope :with_city, ->(c) { AppUtils.like self, [:phys_city, :mail_city], c }
  scope :with_state, ->(s) { AppUtils.for_lower self, [:phys_state, :mail_state], s }
  scope :with_zip, ->(z) { AppUtils.like self, [:phys_zip, :mail_zip], z }
  scope :with_email, ->(e) { AppUtils.like self, :email, e }
  scope :with_dob, ->(start,stop) { AppUtils.for_daterange self, :date_of_birth, start: start, stop: stop, date: true }
  scope :with_ssn, ->(s) { where encrypted_ssn: SymmetricEncryption.encrypt(s) }
  scope :with_oln, ->(o) { where oln: o }
  scope :with_oln_state, ->(s) { AppUtils.for_lower self, :oln_state, s }
  scope :with_fbi, ->(f) { where fbi: f }
  scope :with_sid, ->(s) { where sid: s }
  scope :with_doc_number, ->(d) { where doc_number: d }
  scope :with_fingerprint_class, ->(f) { AppUtils.like self, :fingerprint_class, f }
  scope :with_dna_profile, ->(d) { AppUtils.like self, :dna_profile, d }
  scope :with_race, ->(r) { AppUtils.like self, :race, r }
  scope :with_sex, ->(s) { where sex: s }
  # with_height - see Class Methods
  # with_weight - see Class Methods
  scope :with_build_type, ->(b) { AppUtils.like self, :build_type, b }
  scope :with_skin_tone, ->(s) { AppUtils.like self, :skin_tone, b }
  scope :with_complexion, ->(c) { AppUtils.like self, :complexion, c }
  scope :with_hair_type, ->(ht) { AppUtils.like self, :hair_type, ht }
  scope :with_hair_color, ->(hc) { AppUtils.like self, :hair_color, hc }
  scope :with_mustache, ->(m) { AppUtils.like self, :mustache, m }
  scope :with_eyebrow, ->(e) { AppUtils.like self, :eyebrow, e }
  scope :with_eye_color, ->(ec) { AppUtils.like self, :eye_color, ec }
  scope :with_glasses, ->(g) { AppUtils.for_bool self, :glasses, g }
  scope :with_shoe_size, ->(ss) { AppUtils.like self, :shoe_size, ss }
  scope :with_occupation, ->(o) { AppUtils.like self, :occupation, o }
  scope :with_employer, ->(e) { AppUtils.like self, :employer, e }
  scope :with_gang_affiliation, ->(g) { AppUtils.like self, :gang_affiliation, g }
  scope :with_remarks, ->(rem) { AppUtils.like self, :remarks, rem }
  scope :with_associate, ->(a) { AppUtils.like self, :name, a, association: :associates }
  
  ## Callbacks #############################################
  
  before_save :check_creator, :update_doc_records, :check_sex
  after_save :update_warrant_matches, :update_forbid_matches
  before_validation :split_name, :check_alias
  before_destroy :check_destroyable

  ## Validations #############################################

  validates_associated :associates, :marks
  validates_uniqueness_of :id, allow_nil: true
  validates_presence_of :family_name, :given_name
  validates_zip :phys_zip, :mail_zip, allow_blank: true, allow_nil: true
  validates_phone :home_phone, :cell_phone, allow_nil: true, allow_blank: true
  validates_ssn :ssn, allow_blank: true
  validates :encrypted_ssn, symmetric_encryption: true, allow_blank: true
  validates_date :date_of_birth, :dna_swab_date, :dna_blood_date, allow_blank: true
  validates_numericality_of :sex, only_integer: true, greater_than_or_equal_to: 0, less_than: 3
  validates_presence_of :oln_state, if: :oln?
  validates_presence_of :oln, if: :oln_state?
  validates_numericality_of :id, :height_ft, :height_in, :weight, only_integer: true, allow_nil: true
  validates_person :is_alias_for, allow_nil: true
  validates_attachment_content_type :front_mugshot, content_type: ["image/jpg","image/jpeg","image/gif","image/png","image/tiff"]
  validates_attachment_content_type :side_mugshot, content_type: ["image/jpg","image/jpeg","image/gif","image/png","image/tiff"]
  
  ## Class Constants #############################################
  
  Race = {'American Indian' => 'AI', 'Asian' => 'A', 'Black' => 'B', 'Hispanic' => 'H', 'Pacific Islander' => 'PI', 'White' => 'W', 'Other' => 'O'}.freeze

  BuildType = ['Slender', 'Light', 'Medium', 'Heavy', 'Obese'].freeze

  EyeColor = ['Black','Blue','Brown','Gray','Green','Hazel','Red','Violet'].freeze

  HairColor = ['Blond','Red','Brown','Black','Gray','White','Salt&Pepper','Exotic'].freeze

  SkinTone = ['Light','Fair','Medium','Olive','Brown','Black'].freeze

  Complexion = ['Blotched','Ruddy','Pale','Pocked','Freckled','Acned', 'Tanned'].freeze

  HairType = ['Bald','Partial Bald','Short','Medium','Long', 'Short Afro','Afro','Full Afro'].freeze

  Mustache = ['Thin','Medium','Thick','Bushy', 'Handlebar','None'].freeze

  Beard = ['Stubble','Short','Medium','Long','Full','Sideburns','Van Dyke','Goatee','Fu-Manchu','Mutton Chop','None'].freeze

  Eyebrow = ['Thin','Thick','Bushy','None'].freeze
  
  Associate = [
    "Associate", "Aunt", "Boyfriend", "Brother", "Brother-in-Law", "Cousin",
    "Daughter", "Daughter-in-Law", "Employee", "Employer", "Ex-Husband", "Ex-Wife",
    "Father", "Father-in-Law", "Friend", "Girlfriend", "Grandchild", "Grandfather",
    "Grandmother", "Great Aunt", "Great Grandchild", "Great Grandfather",
    "Great Grandmother", "Great Uncle", "Husband", "Mother", "Mother-in-Law",
    "Nephew", "Niece", "Roommate", "Sister", "Sister-in-Law", "Step-Brother",
    "Step-Daughter", "Step-Father", "Step-Mother", "Step-Sister", "Step-Son",
    "Son", "Son-in-Law", "Uncle", "Wife"
  ].freeze
  
  Anatomy = [
    'Abdomen', 'Back', 'Chest', 'Face', 'Forehead', 'Left Arm', 'Left Buttock',
    'Left Foot', 'Left Hand', 'Left Leg', 'Left Shoulder', 'Left Wrist', 'Neck',
    'Right Arm', 'Right Buttock', 'Right Foot', 'Right Hand', 'Right Leg',
    'Right Shoulder', 'Right Wrist', 'Scalp'
  ].freeze
  
  ## Class Methods #############################################
  
  def self.base_perms
    :view_person
  end
  
  def self.desc
    "People of Interest"
  end
  
  def self.find_real_identity(person_id)
    return nil unless person_id.is_a?(Integer) || person_id.is_a?(String)
    pid = person_id.to_i
    unless person_record = find_by(id: pid)
      return nil
    end
    person_record.real_identity
  end
  
  # returns people who match provided warrant by name
  # this is the inverse of Warrant.warrants_for method
  #
  # Called from warrant_observer when a warrant is saved/updated
  # Does not consider exempted warrants, the observer method takes
  #   that into consideration.
  def self.for_warrant(warrant_id)
    # make sure warrant_id is valid
    return none unless w = Warrant.find_by(id: warrant_id)
    
    # don't return anything if warrant is linked to a particular person
    return none unless w.person.nil?
    
    result = with_name(w.sort_name)
    
    unless w.encrypted_ssn.blank?
      result = result.where("people.encrypted_ssn = ? OR people.encrypted_ssn = '' OR people.encrypted_ssn IS NULL",w.encrypted_ssn)
    end
    
    unless w.oln.blank?
      result = result.where("LOWER(people.oln) LIKE LOWER(?) OR people.oln = ''",w.oln)
    end
    
    unless w.oln_state.blank?
      result = result.where("LOWER(people.oln_state) = LOWER(?) OR people.oln_state = ''",w.oln_state)
    end
    
    if w.dob?
      result = result.where("people.date_of_birth = ? or people.date_of_birth IS NULL", w.dob)
    end
    
    result
  end
  
  # returns people who match provided forbid by name
  # this is the inverse of Forbid.forbids_for method
  #
  # Called from forbid_observer when forbid is saved/updated
  # Does not consider exempted forbids, the observer method takes
  #   that into consideration.
  def self.for_forbid(forbid_id)
    # make sure forbid_id is valid
    return none unless f = Forbid.find_by(id: forbid_id)
    
    # return nothing if forbid is linked to a person
    return none unless f.person.nil?
    
    with_name(f.sort_name)
  end
  
  # finder for height range
  def self.with_height(height_ft_from, height_in_from, height_ft_to, height_in_to)
    height_in_inches_from = 0
    unless height_ft_from.blank?
      height_in_inches_from += height_ft_from.to_i * 12
    end
    unless height_in_from.blank?
      height_in_inches_from += height_in_from.to_i
    end
    height_in_inches_to = 0
    unless height_ft_to.blank?
      height_in_inches_to += height_ft_to.to_i * 12
    end
    unless height_in_to.blank?
      height_in_inches_to += height_in_to.to_i
    end
    
    return self if height_in_inches_from == 0 && height_in_inches_to == 0
    
    if height_in_inches_from == 0
      where('(people.height_ft * 12) + people.height_in <= ?', height_in_inches_to)
    elsif height_in_inches_to == 0
      where('(people.height_ft * 12) + people.height_in >= ?', height_in_inches_from)
    else
      where('(people.height_ft * 12) + people.height_in <= ? AND (people.height_ft * 12) + people.height_in >= ?', height_in_inches_to, height_in_inches_from)
    end
  end
  
  # finder for weight range
  def self.with_weight(weight_from, weight_to)
    return self if weight_from.to_i == 0 && weight_to.to_i == 0
    if weight_from.to_i == 0
      where('people.weight <= ?', weight_to.to_i)
    elsif weight_to.to_i == 0
      where('people.weight >= ?', weight_from.to_i)
    else
      where('people.weight >= ? AND people.weight <= ?', weight_from.to_i, weight_to.to_i)
    end
  end
  
  # process the filter query
  def self.process_query(query)
    return none unless query.is_a?(Hash)
    result = all
    result = result.with_person_id(query[:id]) unless query[:id].blank?
    result = result.with_name(query[:name]) unless query[:name].blank?
    result = result.with_aka(query[:aka]) unless query[:aka].blank?
    result = result.with_street(query[:street]) unless query[:street].blank?
    result = result.with_city(query[:city]) unless query[:city].blank?
    result = result.with_state(query[:state]) unless query[:state].blank?
    result = result.with_zip(query[:zip]) unless query[:zip].blank?
    result = result.with_email(query[:email]) unless query[:email].blank?
    result = result.with_dob(query[:dob_from],query[:dob_to]) unless query[:dob_from].blank? && query[:dob_to].blank?
    result = result.with_ssn(query[:ssn]) unless query[:ssn].blank?
    result = result.with_oln(query[:oln]) unless query[:oln].blank?
    result = result.with_oln_state(query[:oln_state]) unless query[:oln_state].blank?
    result = result.with_fbi(query[:fbi]) unless query[:fbi].blank?
    result = result.with_sid(query[:sid]) unless query[:sid].blank?
    result = result.with_doc_number(query[:doc_number]) unless query[:doc_number].blank?
    result = result.with_fingerprint_class(query[:fingerprint_class]) unless query[:fingerprint_class].blank?
    result = result.with_dna_profile(query[:dna_profile]) unless query[:dna_profile].blank?
    result = result.with_race(query[:race]) unless query[:race].blank?
    result = result.with_sex(query[:sex]) unless query[:sex].blank?
    result = result.with_height(query[:height_from_ft], query[:height_from_in], query[:height_to_ft], query[:height_to_in]) unless query[:height_from_ft].blank? && query[:height_from_in].blank? && query[:height_to_ft].blank? && query[:height_to_in].blank?
    result = result.with_weight(query[:weight_from], query[:weight_to]) unless query[:weight_from].blank? && query[:weight_to].blank?
    result = result.with_build_type(query[:build_type]) unless query[:build_type].blank?
    result = result.with_skin_tone(query[:skin_tone]) unless query[:skin_tone].blank?
    result = result.with_complexion(query[:complexion]) unless query[:complexion].blank?
    result = result.with_hair_type(query[:hair_type]) unless query[:hair_type].blank?
    result = result.with_hair_color(query[:hair_color]) unless query[:hair_color].blank?
    result = result.with_mustache(query[:mustache]) unless query[:mustache].blank?
    result = result.with_eyebrow(query[:eyebrow]) unless query[:eyebrow].blank?
    result = result.with_eye_color(query[:eye_color]) unless query[:eye_color].blank?
    result = result.with_glasses(query[:glasses]) unless query[:glasses].blank?
    result = result.with_shoe_size(query[:shoe_size]) unless query[:shoe_size].blank?
    result = result.with_occupation(query[:occupation]) unless query[:occupation].blank?
    result = result.with_employer(query[:employer]) unless query[:employer].blank?
    result = result.with_gang_affiliation(query[:gang_affiliation]) unless query[:gang_affiliation].blank?
    result = result.with_remarks(query[:remarks]) unless query[:remarks].blank?
    result = result.with_associate(query[:associate]) unless query[:associate].blank?
    result
  end
  
  ## Instance Methods #############################################
  
  def has_front_mugshot
    front_mugshot_file_name != '' && !front_mugshot_file_name.nil?
  end
  
  def age
    return nil if deceased
    return nil unless date_of_birth?
    now = Date.today
    if (now.month < date_of_birth.month || now.month == date_of_birth.month) && (now.day < date_of_birth.day)
      theage = now.year - date_of_birth.year - 1
    else
      theage = now.year - date_of_birth.year
    end
    return (theage < 100) ? theage : nil
  end
  
  def all_aliases
    [real_identity, real_identity.back_aliases].flatten.compact.uniq
  end
  
  def associates_description
    d = []
    associates.each do |a|
      d.push("#{a.name}#{a.relationship.blank? ? '' : ' (' + a.relationship + ')'}#{a.remarks? ? ' [' + a.remarks + ']' : ''}")
    end
    return d.join(', ')
  end
  
  def back_aliases
    [aliases, aliases.collect{|a| a.back_aliases}].flatten.compact.uniq
  end
  
  def commissary_balance(edate = Date.today)
    return nil unless edate.is_a?(Date)
    if edate == Date.today
      commissaries.sum('deposit - withdraw').to_d
    else
      commissaries.on_or_before(edate).sum('deposit - withdraw').to_d
    end
  end
  
  def commissary_transactions(start_date=Date.today.beginning_of_month,stop_date=Date.today)
    return [] unless start_date.is_a?(Date) && stop_date.is_a?(Date)
    commissaries.between(start_date, stop_date).all
  end
  
  def father
    associates.find_by_relationship("Father").to_s
  end
  
  def is_forbidden?
    !active_forbids.empty? || !possible_forbids.empty?
  end
  
  def is_wanted?
    !active_warrants.empty? || !possible_warrants.empty?
  end
  
  def sort_name
    n = Namae::Name.new(family: family_name, given: given_name, suffix: suffix)
    n.sort_order
  end
  
  def display_name
    n = Namae::Name.new(family: family_name, given: given_name, suffix: suffix)
    n.display_order
  end
  
  def is_alias_loop(person_id)
    return false if person_id.nil?
    person_id = person_id.to_i
    if is_alias_for?
      return true if is_alias_for == person_id
      unless alias_for.nil?
        return alias_for.is_alias_loop(person_id)
      end
    end
    false
  end
  
  def mailing_address
    [mailing_line1, mailing_line2].reject{|a| a.blank?}.join(', ')
  end
  
  def mailing_line1
    mail_street? ? mail_street : physical_line1
  end
  
  def mailing_line2
    name = ""
    if mail_city?
      name << mail_city
    end
    if mail_state?
      unless name.blank?
        name << ", "
      end
      name << mail_state
    end
    if mail_zip?
      unless name.blank?
        name << " "
      end
      name << mail_zip
    end
    if name.blank?
      name = physical_line2
    end
    return name
  end
  
  def marks_description
    marks.join(', ')
  end
  
  def medication_summary
    medications.join(', ')
  end
  
  def mother
    associates.find_by_relationship("Mother").to_s
  end
  
  def on_probation?(*args)
    options = HashWithIndifferentAccess.new(hold_if_arrested: false)
    options.update(args.extract_options!)
    return false if probations.empty?
    probations.each do |p|
      if p.status == 'Active' || p.status == 'Pending'
        return true if (p.hold_if_arrested? && options[:hold_if_arrested] == true) || options[:hold_if_arrested] == false
      end
    end
    false
  end
  
  def past_days(days = 30)
    return [] unless days.is_a?(Integer) && days >= 0
    date = Date.today - days.days
    Commissary.where(['person_id = ? and transaction_date >= ?',id,date]).all
  end
  
  def person_description
    d = ""
    if race?
      d << race
    end
    if sex?
      unless d.blank?
        d << " "
      end
      d << sex_name
    end
    unless deceased?
      unless self.age.nil?
        unless d.blank?
          d << ", "
        end
        d << "#{age} years old"
      end
    end
    if height_ft?
      unless d.blank?
        d << ", "
      end
      d << "#{height_ft} feet"
    end
    if height_in?
      unless d.blank?
        if height_ft?
          d << " "
        else
          d << ","
        end
      end
      d << "#{height_in} inches tall"
    else
      if height_ft?
        d << " tall"
      end
    end
    if weight?
      unless d.blank?
        d << ", "
      end
      d << "#{weight} pounds"
    end
    if build_type?
      unless d.blank?
        d << ", "
      end
      d << "#{build_type} build"
    end
    if skin_tone?
      unless d.blank?
        d << ", "
      end
      d << "#{skin_tone} skin tone"
    end
    if complexion?
      unless d.blank?
        d << ", "
      end
      d << "#{complexion} complexion"
    end
    
    if hair_type? || hair_color?
      if d.blank?
        if hair_type? && hair_type == 'Bald'
          d << "Is "
        else
          d << "Has "
        end
      else
        if hair_type? && hair_type == 'Bald'
          d << ", is "
        else
          d << ", has "
        end
      end
    end
    if hair_type? && hair_color?
      if hair_type == 'Bald'
        d << "Bald"
      else
        d << "#{hair_type} #{hair_color} hair"
      end
    elsif hair_type?
      if hair_type == 'Bald'
        d << "Bald"
      else
        d << "#{hair_type} hair"
      end
    elsif hair_color?
      d << "#{hair_color} hair"
    end

    if beard? || mustache? || eyebrow?
      if d.blank?
        d << "Has"
      else
        if hair_type? || hair_color?
          d << " with "
        else
          d << ", has "
        end
      end
    end
    if beard?
      d << "#{beard == 'None' ? 'no' : beard}#{beard == 'Sideburns' ? '' : ' beard'}"
      if mustache? && eyebrow?
        d << ", "
      elsif mustache? || eyebrow?
        d << " and "
      end
    end
    if mustache?
      d << "#{mustache == 'None' ? 'no' : mustache} mustache"
      if eyebrow?
        d << " and "
      end
    end
    if eyebrow?
      d << "#{eyebrow == 'None' ? 'no' : eyebrow} eyebrows"
    end
    if eye_color?
      if d.blank?
        d << "Has "
      else
        d << ", "
      end
      d << "#{eye_color} eyes"
    end
    if glasses?
      if d.blank?
        d << "Wears glasses."
      else
        d << ", wears glasses."
      end
    else
      unless d.blank?
        d << "."
      end
    end
    if shoe_size?
      unless d.blank?
        d << " "
      end
      d << "Wears size #{shoe_size} shoes."
    end
    return d
  end
  
  def phones
    txt = ''
    if home_phone?
      txt << "Home: #{home_phone}"
    end
    if cell_phone?
      unless txt.blank?
        txt << " -- "
      end
      txt << "Cell: #{cell_phone}"
    end
    return txt
  end
  
  def physical_address
    [physical_line1, physical_line2].reject{|a| a.blank?}.join(', ')
  end
  
  def physical_line1
    phys_street? ? phys_street : ""
  end
  
  def physical_line2
    name = ""
    if phys_city?
      name << phys_city
    end
    if phys_state?
      unless name.blank?
        name << ", "
      end
      name << phys_state
    end
    if phys_zip?
      unless name.blank?
        name << " "
      end
      name << phys_zip
    end
    return name
  end
  
  def probation_balance
    probations.collect{|p| p.total_balance}.sum
  end
  
  def race_abbreviation
    Race[race] || race
  end
  
  def rap_sheet_data
    People::RapSheetData.new(id)
  end
  
  def real_identity
    p = self
    until p.alias_for.nil?
      p = p.alias_for
    end
    p
  end
  
  def reasons_not_destroyable
    reasons = []
    [:arrests, :bonds_as_surety, :bonds, :bookings, :citations, :commissaries,
      :dispositions, :docs, :dockets, :evidences, :forbids, :invest_criminals,
      :invest_sources, :invest_vehicles, :invest_victims, :invest_witnesses,
      :offense_victims, :offense_witnesses, :offense_suspects, :probations,
      :stolens, :transfers, :transports, :warrants].each do |assoc|
      reasons.push("Has linked #{assoc.to_s.titleize}") unless self.send(assoc.to_s).count == 0
    end
    reasons.push("Has Aliases") unless aliases.count == 0
    reasons.empty? ? nil : reasons.join(', ')
  end
  
  def sex_name(short=false)
    if short
      case sex
      when 0
        return "U"
      when 1
        return "M"
      when 2
        return "F"
      end
    else
      case sex
      when 0
        return "Unknown"
      when 1
        return "Male"
      when 2
        return "Female"
      end
    end
    nil
  end
  
  # attr_encrypted does not add this for us
  def ssn?
    !self.ssn.blank?
  end
  
  # called whenever a person is saved, or a warrant is removed from exempted_warrants
  def update_warrant_matches
    self.possible_warrants.clear
    matches = Warrant.warrants_for(id)
    matches.each do |w|
      self.possible_warrants << w unless exempted_warrants.include?(w)
    end
  end
  
  # called whenever a person is saved, or a forbid is removed from exempted_forbids
  def update_forbid_matches
    self.possible_forbids.clear
    matches = Forbid.forbids_for(id)
    matches.each do |f|
      self.possible_forbids << f unless exempted_forbids.include?(f)
    end
  end
  
  private
  
  ## Private Instance Methods #############################################
  
  def check_alias
     if is_alias_for?
        if is_alias_for == id
           self.errors.add(:is_alias_for, 'cannot be the same as this Person ID number!')
           return false
        end
        unless alias_for.nil?
          if alias_for.is_alias_loop(id)
            self.errors.add(:is_alias_for, 'cannot reference a Person that is an alias of this Person!')
            return false
          end
        end
     end
     return true
  end
  
  def check_destroyable
    reasons_not_destroyable.nil?
  end
  
  def check_sex
    if sex.nil?
      self.sex = 0
    end
    true
  end
  
  def split_name
    unless full_name.nil? || full_name.blank?
      n = Namae::Name.parse(full_name)
      self.family_name = n.family || ''
      self.given_name = n.given || ''
      self.suffix = n.suffix || ''
    end
    if emergency_contact?
      self.emergency_contact = AppUtils.parse_name(emergency_contact, true)
    end
    return true
  end
  
  def update_doc_records
    if doc_number_changed?
      docs.each do |d|
        if d.doc_number != doc_number
          d.update_attribute(:doc_number, doc_number)
        end
      end
    end
  end
end

# == Schema Information
#
# Table name: people
#
#  id                         :integer          not null, primary key
#  family_name                :string(255)      default(""), not null
#  given_name                 :string(255)      default(""), not null
#  suffix                     :string(255)      default(""), not null
#  aka                        :string(255)      default(""), not null
#  is_alias_for               :integer
#  phys_street                :string(255)      default(""), not null
#  phys_city                  :string(255)      default(""), not null
#  phys_zip                   :string(255)      default(""), not null
#  mail_street                :string(255)      default(""), not null
#  mail_city                  :string(255)      default(""), not null
#  mail_zip                   :string(255)      default(""), not null
#  date_of_birth              :date
#  place_of_birth             :string(255)      default(""), not null
#  sex                        :integer          default(0), not null
#  fbi                        :string(255)      default(""), not null
#  sid                        :string(255)      default(""), not null
#  oln                        :string(255)      default(""), not null
#  home_phone                 :string(255)      default(""), not null
#  cell_phone                 :string(255)      default(""), not null
#  email                      :string(255)      default(""), not null
#  height_ft                  :integer          default(0), not null
#  height_in                  :integer          default(0), not null
#  weight                     :integer          default(0), not null
#  glasses                    :boolean          default(FALSE), not null
#  shoe_size                  :string(255)      default(""), not null
#  occupation                 :string(255)      default(""), not null
#  employer                   :string(255)      default(""), not null
#  emergency_contact          :string(255)      default(""), not null
#  emergency_address          :string(255)      default(""), not null
#  emergency_phone            :string(255)      default(""), not null
#  med_allergies              :string(255)      default(""), not null
#  hepatitis                  :boolean          default(FALSE), not null
#  hiv                        :boolean          default(FALSE), not null
#  tb                         :boolean          default(FALSE), not null
#  deceased                   :boolean          default(FALSE), not null
#  dna_swab_date              :date
#  gang_affiliation           :string(255)      default(""), not null
#  remarks                    :text             default(""), not null
#  created_at                 :datetime
#  updated_at                 :datetime
#  doc_number                 :string(255)      default(""), not null
#  created_by                 :integer          default(1)
#  updated_by                 :integer          default(1)
#  front_mugshot_file_name    :string(255)
#  side_mugshot_file_name     :string(255)
#  front_mugshot_content_type :string(255)
#  side_mugshot_content_type  :string(255)
#  front_mugshot_file_size    :integer
#  side_mugshot_file_size     :integer
#  front_mugshot_updated_at   :datetime
#  side_mugshot_updated_at    :datetime
#  legacy                     :boolean          default(FALSE), not null
#  dna_blood_date             :date
#  dna_profile                :string(255)      default(""), not null
#  fingerprint_class          :string(255)      default(""), not null
#  phys_state                 :string(255)      default(""), not null
#  mail_state                 :string(255)      default(""), not null
#  oln_state                  :string(255)      default(""), not null
#  race                       :string(255)      default(""), not null
#  build_type                 :string(255)      default(""), not null
#  eye_color                  :string(255)      default(""), not null
#  hair_color                 :string(255)      default(""), not null
#  skin_tone                  :string(255)      default(""), not null
#  complexion                 :string(255)      default(""), not null
#  hair_type                  :string(255)      default(""), not null
#  mustache                   :string(255)      default(""), not null
#  beard                      :string(255)      default(""), not null
#  eyebrow                    :string(255)      default(""), not null
#  em_relationship            :string(255)      default(""), not null
#  sex_offender               :boolean          default(FALSE), not null
#  encrypted_ssn              :string           default("")
#
# Indexes
#
#  index_people_on_created_by                  (created_by)
#  index_people_on_family_name_and_given_name  (family_name,given_name)
#  index_people_on_is_alias_for                (is_alias_for)
#  index_people_on_updated_by                  (updated_by)
#
# Foreign Keys
#
#  people_created_by_fk    (created_by => users.id)
#  people_is_alias_for_fk  (is_alias_for => people.id)
#  people_updated_by_fk    (updated_by => users.id)
#
