class InvestSource < ActiveRecord::Base
  belongs_to :person, inverse_of: :invest_sources                          # constraint
  belongs_to :investigation, inverse_of: :invest_sources                   # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'Investigation Sources'
  end
end

# == Schema Information
#
# Table name: invest_sources
#
#  id               :integer          not null, primary key
#  person_id        :integer          not null
#  investigation_id :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_invest_sources_on_investigation_id  (investigation_id)
#  index_invest_sources_on_person_id         (person_id)
#
# Foreign Keys
#
#  invest_sources_investigation_id_fk  (investigation_id => investigations.id)
#  invest_sources_person_id_fk         (person_id => people.id)
#
