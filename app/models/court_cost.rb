class CourtCost < ActiveRecord::Base
  
  ## Associations #############################################
  
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'
  
  ## Scopes #############################################
  
  scope :by_category, -> { order :category }
  scope :by_name, -> { order :name }
  
  ## Callbacks #############################################
  
  before_save :check_creator
  
  ## Validations #############################################
  
  validates_presence_of :name, :category
  validates_uniqueness_of :name
  validates_numericality_of :amount, greater_than: 0, less_than: 10 ** 10
  
  ## Class Methods #############################################
  
  def self.base_perms
    :update_court
  end
  
  def self.court_costs_for_select
    all.collect{|c| [c.name, c.amount]}
  end
  
  def self.desc
    'Court Costs'
  end
end

# == Schema Information
#
# Table name: court_costs
#
#  id         :integer          not null, primary key
#  name       :string(255)      default(""), not null
#  category   :string(255)      default(""), not null
#  amount     :decimal(12, 2)   default(0.0), not null
#  created_by :integer          default(1)
#  updated_by :integer          default(1)
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_court_costs_on_created_by  (created_by)
#  index_court_costs_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  court_costs_created_by_fk  (created_by => users.id)
#  court_costs_updated_by_fk  (updated_by => users.id)
#
