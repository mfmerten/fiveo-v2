class Absent < ActiveRecord::Base
  
  ## Associations ###################################################
  
  belongs_to :creator, class_name: 'User', foreign_key: 'created_by'
  belongs_to :updater, class_name: 'User', foreign_key: 'updated_by'
  belongs_to :jail, inverse_of: :absents
  has_many :absent_prisoners, dependent: :destroy
  has_many :prisoners, through: :absent_prisoners, source: :booking
  has_many :people, through: :prisoners
  
  ## Attributes ###################################################
  
  accepts_nested_attributes_for :absent_prisoners, allow_destroy: true, reject_if: ->(p) { p['booking_id'].blank? || !Booking.exists?(p['booking_id'])}
  attr_accessor :escort_officer_id
  
  ## Scopes ###################################################
  
  scope :active, -> { where return_datetime: nil }
  scope :by_reverse_id, -> { order id: :desc }
  
  # scopes for filter query
  scope :with_absent_id, ->(a_id) { where(id: a_id) }
  scope :with_booking_id, ->(b_id) { includes(:absent_prisoners).where('absent_prisoners.booking_id = ?', b_id).references(:absent_prisoners) } 
  scope :with_person_id, ->(p_id) { includes(:people).where('people.id = ?',p_id).references(:people) }
  scope :with_absent_type, ->(a_type) { AppUtils.like self, :absent_type, a_type }
  scope :belonging_to_jail, ->(j_id) { where jail_id: j_id }
  scope :with_return, ->(start,stop) { AppUtils.for_daterange self, :return_datetime, start: start, stop: stop }
  scope :with_leave, ->(start,stop) {  AppUtils.for_daterange self, :leave_datetime, start: start, stop: stop }
  scope :with_officer, ->(name,unit,badge) { AppUtils.for_officer self, :escort_officer, name, unit, badge }
  scope :with_person_name, ->(name) { AppUtils.for_person self, name, association: :people }
  
  ## Callbacks ###################################################
  
  after_validation :check_creator
  before_validation :lookup_officers
  
  ## Validations ###################################################
  
  validates_presence_of :escort_officer, :absent_type
  validates_jail :jail_id
  validates_datetime :leave_datetime, :return_datetime, allow_blank: true

  ## Class Methods #############################################
  
  def self.base_perms
    :view_booking
  end
  
  def self.desc
    "Prisoner Absentee Records"
  end
  
  def self.process_query(query)
    return none unless query.is_a?(Hash)
    result = all
    result = result.with_absent_id(query[:id]) unless query[:id].blank?
    result = result.with_booking_id(query[:booking_id]) unless query[:booking_id].blank?
    result = result.with_person_id(query[:person_id]) unless query[:person_id].blank?
    result = result.with_person_name(query[:person_name]) unless query[:person_name].blank?
    result = result.with_leave(query[:leave_from], query[:leave_to]) unless query[:leave_from].blank? && query[:leave_to].blank?
    result = result.with_return(query[:return_from], query[:return_to]) unless query[:return_from].blank? && query[:return_to].blank?
    result = result.with_absent_type(query[:absent_type]) unless query[:absent_type].blank?
    result = result.with_officer(query[:escort_officer], query[:escort_officer_unit], query[:escort_officer_badge]) unless query[:escort_officer].blank? && query[:escort_officer_unit].blank? && query[:escort_officer_badge].blank?
    result
  end

  def lookup_officers
    unless escort_officer_id.blank?
      if ofc = Contact.find_by_id(escort_officer_id)
        self.escort_officer = ofc.sort_name
        self.escort_officer_badge = ofc.badge_no
        self.escort_officer_unit = ofc.unit
      end
      self.escort_officer_id = nil
    end
    true
  end
end

# == Schema Information
#
# Table name: absents
#
#  id                   :integer          not null, primary key
#  escort_officer       :string(255)      default(""), not null
#  escort_officer_unit  :string(255)      default(""), not null
#  created_by           :integer          default(1)
#  updated_by           :integer          default(1)
#  created_at           :datetime
#  updated_at           :datetime
#  escort_officer_badge :string(255)      default(""), not null
#  jail_id              :integer          not null
#  leave_datetime       :datetime
#  return_datetime      :datetime
#  absent_type          :string(255)      default(""), not null
#
# Indexes
#
#  index_absents_on_created_by       (created_by)
#  index_absents_on_jail_id          (jail_id)
#  index_absents_on_return_datetime  (return_datetime)
#  index_absents_on_updated_by       (updated_by)
#
# Foreign Keys
#
#  absents_created_by_fk  (created_by => users.id)
#  absents_jail_id_fk     (jail_id => jails.id)
#  absents_updated_by_fk  (updated_by => users.id)
#
