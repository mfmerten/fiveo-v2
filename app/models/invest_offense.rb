class InvestOffense < ActiveRecord::Base
  belongs_to :offense, inverse_of: :invest_offenses                        # constraint
  belongs_to :investigation, inverse_of: :invest_offenses                  # constraint
  has_many :invest_crime_offenses, dependent: :destroy                     # constraint
  has_many :crimes, through: :invest_crime_offenses, source: :invest_crime  # constrained through invest_crime_offenses
  has_many :invest_report_offenses, dependent: :destroy                    # constraint
  has_many :reports, through: :invest_report_offenses, source: :invest_report  # constrained through invest_report_offenses
  
  ## Class Methods #############################################
  
  def self.desc
    'Investigation Offenses'
  end
end

# == Schema Information
#
# Table name: invest_offenses
#
#  id               :integer          not null, primary key
#  investigation_id :integer          not null
#  offense_id       :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_invest_offenses_on_investigation_id  (investigation_id)
#  index_invest_offenses_on_offense_id        (offense_id)
#
# Foreign Keys
#
#  invest_offenses_investigation_id_fk  (investigation_id => investigations.id)
#  invest_offenses_offense_id_fk        (offense_id => offenses.id)
#
