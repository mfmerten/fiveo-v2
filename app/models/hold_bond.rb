class HoldBond < ActiveRecord::Base
  belongs_to :bond, inverse_of: :hold_bonds # constraint
  belongs_to :hold, inverse_of: :hold_bonds # constraint
  
  ## Class Methods #############################################
  
  def self.desc
    'Hold -> Bond links'
  end
end

# == Schema Information
#
# Table name: hold_bonds
#
#  id         :integer          not null, primary key
#  bond_id    :integer          not null
#  hold_id    :integer          not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_hold_bonds_on_bond_id  (bond_id)
#  index_hold_bonds_on_hold_id  (hold_id)
#
# Foreign Keys
#
#  hold_bonds_bond_id_fk  (bond_id => bonds.id)
#  hold_bonds_hold_id_fk  (hold_id => holds.id)
#
