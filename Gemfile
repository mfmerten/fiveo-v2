source 'http://rubygems.org'

gem 'rails', '4.2.1'

# Bundle edge Rails instead:
# gem 'rails',     git: 'git://github.com/rails/rails.git'

gem 'mysql2', '~> 0.3.12b5' # needed until after the migrations for 2.0.20 are complete
gem 'pg'
group :development, :test do
  # development and test use sqlite3
  gem 'sqlite3'
end

gem 'sass-rails', '~> 5.0'
gem 'coffee-rails', "~> 4.1.0"
gem 'uglifier', ">= 1.3.0"

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'

gem 'turbolinks'
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0', group: :doc
gem 'redcarpet', '~> 3.1.2'
gem 'dalli'

gem 'bcrypt', '~> 3.1.7'
gem 'symmetric-encryption'

group :development do
  gem 'capistrano', '~> 2.0'
  gem 'annotate'
  gem 'bullet'
end

group :development, :test do
  gem 'rspec-rails', '~> 3.1'
  gem 'rspec-activemodel-mocks'
  gem 'shoulda-matchers', require: false
  gem 'factory_girl_rails'
  gem 'database_cleaner'
end

# we use database sessions
gem 'activerecord-session_store'

# name parser
gem 'namae'

# For PDFs
gem 'prawn'
gem 'prawn-table'

# For exception email
gem 'exception_notification'

# for cron jobs
gem 'whenever', require: false

# for time validations
gem 'jc-validates_timeliness'

# for utilities
gem 'chronic'

# image handling
gem 'paperclip'

# for tableless models
gem 'active_attr'

# for pagination
gem 'kaminari'

# for textile
gem 'RedCloth'

# for forums
gem "gemoji"
