# README #

FiveO is a custom Ruby on Rails software application for law enforcement and has been released under the GNU General Purpose License Version 3 (GPL3).

Features include (but not limited to)

* dispatch and patrol
* wants & warrants
* evidence tracking
* support for an unlimited number of individually configurable jail facilities (arrests, bookings, bonds, visitor logs, work crews, transportation, transfer logs)
* criminal investigations
* integrated court record management (dockets, sentencing, probation, paper service, garnishments)
* pawn ticket database
* a collection of other useful utilities (events calendar, announcements, in-app user messaging, in-app forums)
* many more 

This application was developed specifically for the needs of my local Sheriff's Office. As such, it needs a lot of work before it will be useful to any other department. If your department is looking for an inexpensive (free) software solution, consider joining the FiveO development effort. Any help you can give would be appreciated, and I would be willing to volunteer my services to help you get your own server set up and running. Contact me (see below) and we can discuss your needs. If you want to take a look at the current version, I have a demo (test) server set up and running locally. I can give you an account on and access to the demo upon request.

*Note:* for contact information, please visit the FiveO Documentation website at http://fiveo.mertenusa.com