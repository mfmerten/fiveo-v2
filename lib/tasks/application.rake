namespace :app do
  desc "Run all hourly tasks"
  task(:update_hourly => :environment) do
    Booking.update_sentences_by_hour
    Docket.process_dockets
    Disposition.update_status
    AppUtils.purge_pdf_tmp
  end

  desc "Run all daily tasks"
  task(:update_daily => :environment) do
    Booking.send_problem_notifications("Daily")
    Probation.update_probation_status
    Message.purge
    Session.reap_expired_sessions
  end

  desc "Run all weekly tasks"
  task(:update_weekly => :environment) do
    Booking.send_problem_notifications("Weekly")
    Activity.purge_old_records
    MessageLog.purge
  end
  
  desc "Run all semi-monthly (1st and 15th) tasks"
  task(:update_semi_monthly => :environment) do
    Booking.send_problem_notifications("Semi-Monthly")
  end
  
  desc "Run all monthly (1st of month) tasks"
  task(:update_monthly => :environment) do
    Booking.send_problem_notifications("Monthly")
  end

  # note that this duplicates the hourly processing above,
  # but is included as a stand-alone rake task so that it
  # can be run manually as desired.
  desc "Process any waiting sentences/revocations"
  task(:process_dockets => :environment) do
    Docket.process_dockets
  end
  
  # this task is intended to be run by cron job, but no job is defined
  # for it.  The cron job must be created manually for the desired frequency
  # which will typically be every 15 minutes.
  desc "Update Cashless Systems INC Database"
  task(:update_cashless_systems => :environment) do
    Booking.update_cashless_systems
  end
  
  desc "Update the Reference Manual"
  task(:update_manual) do
    require 'reference_manual'
    ReferenceManual.update
  end
  
  namespace :initialize do
    # standard data is added via rake db:seed.  The stuff here is specific
    # to a particular Agency or State

    desc "Load Sabine Parish Sheriffs Office data."
    task(:spso => :environment) do
      # load site configuration
      conf = YAML.load_file("#{Rails.root}/db/initial_data/spso/site_configuration.yml")
      site = Site.new
      site.attributes = conf
      if site.valid?
        site.save
      else
        puts "WARNING: Could not load SPSO Configuration - Failed Validation!"
      end
      
      #load paper types
      options = YAML.load_file("#{Rails.root}/db/initial_data/spso/paper_types.yml")
      options.each do |o|
        PaperType.create(:name => o[:name], :cost => BigDecimal.new(o[:cost].to_s,2), :created_by => 1, :updated_by => 1)
      end
      
      # load paper customers
      options = YAML.load_file("#{Rails.root}/db/initial_data/spso/paper_customers.yml")
      options.each do |o|
        PaperCustomer.create(:name => o[:name], :street => o[:street], :city => o[:city], :state => o[:state], :disabled => o[:disabled], :created_by => 1, :updated_by => 1)
      end
      
      # load jails (spdc & spwf)
      options = YAML.load_file("#{Rails.root}/db/initial_data/spso/jails.yml")
      options.each do |o|
        Jail.create(:name => o[:name], :acronym => o[:acronym], :street => o[:street], :city => o[:city], :state => o[:state], :zip => o[:zip], :phone => o[:phone], :fax => o[:fax], :juvenile => o[:juvenile], :male => o[:male], :female => o[:female], :permset => 1, :goodtime_by_default => true, :goodtime_hours_per_day => 24, :afis_enabled => o[:afis_enabled], :created_by => 1, :updated_by => 1)
      end
      
      # load jail cells
      options = YAML.load_file("#{Rails.root}/db/initial_data/spso/jail_cells.yml")
      options.each do |o|
        JailCell.create(:name => o[:name], :jail_id => o[:jail_id])
      end
      
      # load commissary items
      options = YAML.load_file("#{Rails.root}/db/initial_data/spso/commissary_items.yml")
      options.each do |o|
        CommissaryItem.create(:description => o[:description], :jail_id => o[:jail_id], :price => BigDecimal.new(o[:price].to_s,2), :active => o[:active], :created_by => 1, :updated_by => 1)
      end
      
      # load signal codes
      options = YAML.load_file("#{Rails.root}/db/initial_data/spso/signal_codes.yml")
      options.each do |o|
        SignalCode.create(:code => o[:code], :name => o[:name], :created_by => 1, :updated_by => 1)
      end
    
      # load pawn types
      options = YAML.load_file("#{Rails.root}/db/initial_data/spso/pawn_item_types.yml")
      options.each do |o|
        PawnItemType.create(:item_type => o[:item_type], :created_by => 1, :updated_by => 1)
      end
    end
    
    desc "Load Louisiana data."
    task(:louisiana => :environment) do
      # load statutes
      options = YAML.load_file("#{Rails.root}/db/initial_data/louisiana/louisiana_revised_statutes_2011.yml")
      options.each do |o|
        Statute.create(:name => o, :created_by => 1, :updated_by => 1)
      end
    end
  end
end

namespace :sass do
  desc "Update all SASS stylesheets"
  task :update => :environment do
    Sass::Plugin.update_stylesheets
  end
end
