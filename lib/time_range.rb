# stores a single range of times
# does not allow nil values!
class TimeRange
  
  attr_reader :start, :stop
  
  def initialize(start_time, stop_time)
    if start_time.is_a?(Time)
      @start = start_time
    elsif start_time.respond_to?('to_time')
      @start = start_time.to_time
    else
      raise ArgumentError.new("start time must be Time or an object that responds to 'to_time'")
    end
  
    if stop_time.is_a?(Time)
      @stop = stop_time
    elsif stop_time.respond_to?('to_time')
      @stop = stop_time.to_time
    else
      raise ArgumentError.new("stop time must be Time or and object that responds to 'to_time'")
    end
    
    @dirty = true
    
    if @start > @stop
      @start, @stop = @stop, @start
    end
  end
  
  def start=(value)
    if value.is_a?(Time)
      new_value = value
    elsif value.respond_to?('to_time')
      new_value = value.to_time
    else
      raise ArgumentError.new("start time must be Time or an object that responds to 'to_time'")
    end
    unless @start == new_value
      @dirty = true
      @start = new_value
    end
  end
  
  def stop=(value)
    if value.is_a?(Time)
      new_value = value
    elsif value.respond_to?('to_time')
      new_value = value.to_time
    else
      raise ArgumentError.new("stop time must be Time or an object that responds to 'to_time'")
    end
    unless @stop == new_value
      @dirty = true
      @stop = new_value
    end
  end
  
  def dirty?
    @dirty
  end
  
  # alias for dirty?
  def changed?
    @dirty
  end
  
  def dirty=(value)
    @dirty = true & value
  end
  
  def hours
    ((@stop - @start) / (60*60)).round
  end
  
  def ==(timerange)
    unless timerange.is_a?(TimeRange)
      raise ArgumentError.new("comparison to invalid type: expected TimeRange, got #{timerange.class.name}")
    end
    
    @start == timerange.start && @stop == timerange.stop
  end
    
  def overlaps?(timerange)
    unless timerange.is_a?(TimeRange)
      raise ArgumentError.new("argument must be a TimeRange object: detected #{timerange.class.name}")
    end
    
    result = false
    # right side overlap
    result = true if timerange.start >= @start && timerange.start <= @stop
    # left side overlap
    result = true if timerange.stop >= @start && timerange.stop <= @stop
    # timerange covers self
    result = true if timerange.start <= @start && timerange.stop >= @stop
    # self covers timerange
    result = true if timerange.start >= @start && timerange.stop <= @stop
    result
  end
  
  def covers?(time)
    if time.is_a?(Time)
      time_value = time
    elsif time.respond_to?('to_time')
      time_value = time.to_time
    else
      raise ArgumentError.new("argument must be Time, or an object that responds to 'to_time'")
    end
    
    @start <= time_value && @stop >= time_value
  end
  
  def <=>(timerange)
    unless timerange.is_a?(TimeRange)
      raise ArgumentError.new("comparison to invalid type: expected TimeRange, got #{timerange.class.name}")
    end
    
    # return if TimeRange objects are equal
    return 0 if self == timerange
    # return if starts are not equal
    return -1 if @start < timerange.start
    return 1 if @start > timerange.start
    # otherwise sort based on stop times
    if @stop < timerange.stop
      -1
    else
      1
    end
  end
  
  # returns TimeRange object array
  def merge_if_overlap(timerange)
    unless timerange.is_a?(TimeRange)
      raise ArgumentError.new("argument must be TimeRange object: detected #{timerange.class.name}")
    end
    
    if !overlaps?(timerange)
      # no overlap
      [self, timerange]
    elsif timerange.covers?(@start) && timerange.covers?(@stop)
      # timerange fully covers self
      @dirty = true
      @start = timerange.start
      @stop = timerange.stop
      [self]
    elsif timerange.covers?(@stop)
      # timerange covers stop end
      unless @stop == timerange.stop
        @stop = timerange.stop
        @dirty = true
      end
      [self]
    elsif timerange.covers?(@start)
      # timerange overlaps start end of range
      unless @start == timerange.start
        @dirty = true
        @start = timerange.start
      end
      [self]
    else
      # self fully overlaps (covers) timerange
      [self]
    end
  end
  
  # returns TimeRange object array
  #
  # Start with only self to compare against excluded ranges
  # 
  # compare with each excluded range in turn, adjusting self if necessary
  # 
  # if an exclude range happens to occur fully within boundaries of self,
  #   adjust self to cover the beginning uncovered period and create an
  #   new untested range to cover the ending uncovered period.
  # 
  # unless totally covered by excluded ranges, add self to final_results
  #   array.
  #
  # repeat process with each new range that gets created
  #
  # return the array of tested range(s)
  def exclude_if_overlap(*excludes)
    # array to hold untested ranges
    untested_ranges = [self]
    # array to hold fully excluded ranges
    final_results = []
    
    # allow passing arrays of excludes
    excludes = excludes.flatten
    
    while r = untested_ranges.shift
      
      totally_excluded = false
      
      excludes.each do |timerange|
        # raise ArgumentError if not TimeRange
        unless timerange.is_a?(TimeRange)
          raise ArgumentError.new("argument(s) must be TimeRange object(s): detected #{timerange.class.name}")
        end
      
        # skip non-overlapping excludes
        next unless r.overlaps?(timerange)
        
        # check for fully excluded ranges
        if timerange.covers?(r.start) && timerange.covers?(r.stop)
          totally_excluded = true
          break
        end
        
        # check for start overlap
        if timerange.covers?(r.stop)
          # exclude overlaps stop end of range
          unless r.stop == timerange.start
            r.stop = timerange.start
            r.dirty = true
          end
          next
        end
        
        # check for stop overlap
        if timerange.covers?(r.start)
          # exclude overlaps start end of range
          unless r.start == timerange.stop
            r.start = timerange.stop
            r.dirty = true
          end
          next
        end
        
        # at this point, range fully overlaps exclude
        # create additional TimeRange to cover stop end
        #   and adjust our end accordingly
        untested_ranges.push(TimeRange.new(timerange.stop, r.stop))
        r.stop = timerange.start
        r.dirty = true
      end
      unless totally_excluded
        final_results.push(r)
      end
    end
    # give back results
    final_results
  end
end
