# This module is a collection of miscellaneous methods used throughout the
# FiveO application.

module AppUtils

  require 'active_support/core_ext/string/inflections'
  require 'chronic'
  
  # Called from a rake task, this methods cleans up old generated PDF files
  # from public/files. Maximum file age defaults to one hour, but if a
  # Time is given, it will be used as the maximum age.
  #
  # Arguments (optional):
  #
  #   maximum_age:: 
  #     (Time) all PDF files last modified before this Time will be deleted.
  #     The default is 1.hour.ago
  #
  # Returns no useful value.
  def self.purge_pdf_tmp(maximum_age=nil)
    unless maximum_age.is_a?(Time)
      maximum_age = 1.hour.ago
    end
    tmpdir = Rails.root.join("public", "files")
    pdfs = []
    unless File.directory?(tmpdir)
      # supposed to exist, make it if necessary
      FileUtils.mkdir_p(tmpdir)
    end
    pdfs = Dir["#{tmpdir}/*.pdf"]
    pdfs.each do |pdf|
      if File.exist?(pdf)
        mtime = File.mtime(pdf)
        if mtime < maximum_age
          File.unlink(pdf)
        end
      end
    end
    nil
  end
  
  # a simple case insensitive '=' finder. This is done by lower casing the value and the column
  # prior to comparison with '='.
  #
  # Arguments (required):
  #
  #   model_or_string::
  #     (Class | String) the model class or class_name to evaluate. Calls valid_model() to
  #     validate model class.
  #   field_symbols::
  #     (Array | Symbol | String) array of column(s) to search. Calls valid_fields() to verify
  #     string/symbol field names.
  #   value::
  #     (Object) the value to compare. can be any type of object that responds to 'to_s' method
  #     that returns a simple string. if value.to_s evaluates as blank, returns records with blank
  #     in the specified column(s).
  #  
  # Options:
  #
  #   association::
  #     (String | Symbol | nil) the association used to access each of the fields. defaults to nil.
  #   includes::
  #     (String | Symbol | Array | nil) the associations to include for the query. defaults to nil.
  #
  # Note: this is intended to support the scopes written to process filter queries... other uses
  #  may or may not work as intended.
  #
  # Returns a relation
  def self.for_lower(model_or_string, field_symbols, value, *args)
    options = HashWithIndifferentAccess.new(association: nil, includes: nil)
    options.update(args.extract_options!)
    
    # force includes if association is specified, but don't override specified value
    options[:includes] ||= options[:association]
    
    model_class = valid_model(model_or_string)
    
    # check field names
    fields = valid_fields(model_class, options[:association], field_symbols)
    
    # force value to be a string
    search_value = value.to_s
    
    # generate an appropriate field prefix
    prefix = valid_prefix(model_class, options[:association])

    # build an array of lower-case '=' clauses
    conditions = []
    fields.each do |f|
      conditions.push("LOWER(#{prefix}.#{f}) = LOWER(?)")
    end
    
    # combine the clauses
    sql_text = conditions.join(" OR ")
    
    # create an array condition with appropriate number of values
    condition = [sql_text]
    fields.size.times do
      condition.push(search_value)
    end
    
    # apply the scopes
    result = model_class.includes(options[:includes]).where(condition)
    if options[:association]
      result = result.references(options[:association])
    end
    result
  end
  
  # A simple NULL finder that is mostly useful when determining if a date has been
  # entered or not. For instance, to check whether a bond has been recalled by looking to see
  # if the recall_datetime is null. Used with data from a form where we're unsure ahead of
  # time whether we want to check for NULL or NOT NULL. 
  #
  # Arguments (required):
  #
  #   model_or_string::
  #     (Class | String | Symbol) A model class constant or a model class name. Validated
  #     by valid_model()
  #   field_symbol::
  #     (String | Symbol) The name of the column to check. Validated by valid_fields().
  #   value::
  #     (String | Integer | Boolean) false tests for IS NULL, true checks for IS NOT NULL.
  #     This is generally supplied as '1' or '0' (from radio button values)
  #
  # options:
  #
  #   association::
  #     (String, Symbol, nil) the association used to access each of the fields. defaults to nil.
  #   includes::
  #     (String | Symbol | Array | nil) the associations to include for the query. defaults to nil.
  #
  # Note: this is intended to support the scopes written to process filter queries... other uses
  #  may or may not work as intended.
  #
  # Returns a relation
  def self.for_null(model_or_string, field_symbol, value, *args)
    options = HashWithIndifferentAccess.new(association: nil, includes: nil)
    options.update(args.extract_options!)
    
    # force includes if association is specified, but don't override specified value
    options[:includes] ||= options[:association]
    
    # get model
    model_class = valid_model(model_or_string)
    
    # raise ArgumentError if field_symbol is blank
    field = valid_fields(model_class, options[:association], field_symbol, array: false)
    
    # get table prefix
    prefix = valid_prefix(model_class, options[:association])
    
    # don't allow nils to be interpreted as false
    if value.nil?
      return model_class.none
    end

    # translate value to boolean
    value = valid_bool(value)
    
    # this logic is determined by the way the filter controls are set up
    #   using radio buttons, not for any correlation between true/false and null/not null
    if value
      result = model_class.includes(options[:includes]).where("#{prefix}.#{field} IS NOT NULL")
    else
      result = model_class.includes(options[:includes]).where("#{prefix}.#{field} IS NULL")
    end
    
    if options[:association]
      result = result.references(options[:association])
    end
    result
  end
  
  # A simple date finder that validates the provided date string before
  # comparison.
  #
  # Arguments (required):
  #
  #   model_or_string::
  #     (Class | String | Symbol) Model class or Model class name. validated by valid_model()
  #   field_symbol::
  #     (String | Symbol) name of column to check. validated by valid_fields()
  #   datestring::
  #     (String) string representation of a date
  #
  # options:
  #
  #   association::
  #     (String | Symbol) the association used to access each of the columns. defaults to nil.
  #   includes::
  #     (String | Symbol | Array | nil) the associations to include for the query. defaults to nil.
  #   datetime::
  #     (Bool) if false, column is time/datetime.  if true, column is date.
  #
  # Note: this is intended to support the scopes written to process filter queries... other uses
  #  may or may not work as intended.
  #
  # Returns a relation
  def self.for_date(model_or_string, field_symbol, datestring, *args)
    options = HashWithIndifferentAccess.new(association: nil, includes: nil, datetime: false)
    options.update(args.extract_options!)
    
    # force includes if association is specified, but don't override specified value
    options[:includes] ||= options[:association]
      
    # get model class
    model_class = valid_model(model_or_string)
    
    # get table prefix
    prefix = valid_prefix(model_class, options[:association])
    
    # get field
    field = valid_fields(model_class, options[:association], field_symbol, array: false)
    
    # validate the date
    return model_class.none unless d = valid_date(datestring)
        
    if options[:datetime]
      # convert date to time range covering a single day and use for_daterange
      # to compare.  Otherwise could get incorrect matches due to the fact that
      # datetimes are entered as US Central, but stored as GMT. (CASTing database
      # times to DATE doesn't consider the -5 hours difference.  Maybe I'm
      # over-thinking this?)
      self.for_daterange(model_class, field_symbol, start: d.to_time.beginning_of_day, stop: d.to_time.end_of_day, association: options[:association], includes: options[:includes])
    else
      result = model_class.includes(options[:includes]).where("#{prefix}.#{field_symbol} = ?", d)
      if options[:association]
        result = result.references(options[:association])
      end
      result
    end
  end
  
  # a simple person finder - supports family_name, given_name and suffix
  #
  # Arguments (required):
  #
  #   model_or_string::
  #     (Class | String | Symbol) model class or model class name. Validated by valid_model()
  #   name::
  #     (String) full name of person to query for (i.e. "Michael Merten")
  #
  # Options:
  #
  #   association::
  #     (String | Symbol | nil) the association used to access each of the columns.
  #     defaults to nil.
  #   includes::
  #     (String | Symbol | Array | nil) the associations to include for the query. defaults to nil.
  #   simple::
  #     (Bool) if true, table has a single name column named :sort_name
  #   field::
  #     (String | Symbol) Used to specify the column name if it is not :sort_name.
  #     validated by valid_field() if options[:simple] is true.
  #
  # Note that the family name protion of the name is searched in both the family_name
  # and given_name columns (to also match maiden names), and each given name
  # is individually checked against given_name column and family_name column (to
  # match maiden names from the other perspective).
  #
  # Note: this is intended to support the scopes written to process filter queries... other uses
  #  may or may not work as intended.
  #
  # Returns a relation
  def self.for_person(model_or_string, name, *args)
    options = HashWithIndifferentAccess.new(association: nil, includes: nil, simple: false, field: :sort_name)
    options.update(args.extract_options!)
    
    # force includes if association is specified, but don't override specified value
    options[:includes] ||= options[:association]
    
    # get model class
    model_class = valid_model(model_or_string)
    
    # get table prefix
    prefix = valid_prefix(model_class, options[:association])
    
    pname = name.clone
    field = options[:field]
    if options[:simple]
      field = valid_fields(model_class, options[:association], options[:field], array: false)
    end
    
    # make sure person name is not blank
    if pname.blank?
      return model_class.none
    end
    
    # if name has only one word, force it to be family name by appending a comma
    if pname.split.size == 1
      pname << ','
    end
    
    # parse the provided name
    parts = Namae::Name.parse(pname)
    
    # name must result in at least a family name
    return model_class.none if parts.family.blank?
    
    # get list of first and all middle names
    if parts.given.blank?
      given_names = []
    else
      given_names = parts.given.split
    end
    
    # family name
    if options[:simple]
      result = model_class.includes(options[:includes]).where("LOWER(#{prefix}.#{field}) LIKE LOWER(?)", "%#{parts.family}%")
    else
      result = model_class.includes(options[:includes]).where("LOWER(#{prefix}.family_name) LIKE LOWER(?) OR LOWER(#{prefix}.given_name) LIKE LOWER(?)","%#{parts.family}%","%#{parts.family}%")
    end
    
    # now for given names
    
    # allow a match if the checked field is blank
    gname_subconditions = []
    unless given_names.empty?
      if options[:simple]
        # not needed for single-field name
      else
        gname_subconditions = ["#{prefix}.given_name = ''"]
      end
    end
    gname_arguments = []
    
    given_names.each do |n|
      if options[:simple]
        gname_subconditions.push("LOWER(#{prefix}.#{field}) LIKE LOWER(?)")
        gname_arguments.push("%#{n}%")
      else
        gname_subconditions.push("LOWER(#{prefix}.given_name) LIKE LOWER(?) OR LOWER(#{prefix}.family_name) LIKE LOWER(?)")
        2.times{ gname_arguments.push("%#{n}%") }
      end
    end

    given_name_condition = nil
    unless gname_subconditions.empty?
      given_name_condition = [gname_subconditions.join(' OR ')]
      given_name_condition.concat(gname_arguments)
      result = result.where(given_name_condition)
    end
    
    # add association reference if necessary
    if options[:association]
      result = result.references(options[:association])
    end
    result
  end
  
  # A simple officer finder... assumes standardized naming conventions (officer, officer_unit, officer_badge)
  # Empty values are skipped instead of matching empty columns. If all values are empty, and empty relation
  # is returned.
  #
  # Arguments (required):
  #
  #   model_or_string::
  #     (String | Symbol | Class) The model to search. Validated with valid_model()
  #   fields_array::
  #     (Array | String | Symbol) array of base names for officer columns (without _unit and _badge). Validated
  #     using valid_fields()
  #     raise ArgumentError if this is empty after rejecting non-strings and blank strings.
  #   name::
  #     (String) officer name value for query (like)
  #   unit::
  #     (String) officer unit value for query (=)
  #   badge::
  #     (String) officer badge value for query (=)
  #
  # Options:
  #
  #   association::
  #     (Class | String | Symbol) the association used to access each of the columns. defaults to nil.
  #   includes::
  #     (String | Symbol | Array | nil) the associations to include for the query. defaults to nil.
  #
  # Note: this is intended to support the scopes written to process filter queries... other uses
  #  may or may not work as intended.
  #
  # Returns a relation
  def self.for_officer(model_or_string, fields_array, name, unit, badge, *args)
    options = HashWithIndifferentAccess.new(association: nil, includes: nil)
    options.update(args.extract_options!)
    
    # force includes if association is specified, but don't override specified value
    options[:includes] ||= options[:association]
    
    # get model class
    model_class = valid_model(model_or_string)
    
    # get standardized list of fields
    fields = valid_fields(model_class, options[:association], fields_array)
    
    # get table prefix
    prefix = valid_prefix(model_class, options[:association])
    
    # generate the needed sql clauses
    conditions = []
    subclauses = []
    subrefs = []

    # build field clauses for each officer
    fields.each do |field|
      subsubclause = []
      unless name.blank?
        subsubclause.push("LOWER(#{prefix}.#{field}) LIKE LOWER(?)")
        subrefs.push("%#{name}%")
      end
      unless unit.blank?
        subsubclause.push("LOWER(#{prefix}.#{field}_unit) = LOWER(?)")
        subrefs.push(unit)
      end
      unless badge.blank?
        subsubclause.push("LOWER(#{prefix}.#{field}_badge) = LOWER(?)")
        subrefs.push(badge)
      end
      unless subsubclause.empty?
        subclauses.push("(#{subsubclause.join(' AND ')})")
      end
    end
    # combine fields clauses with OR
    unless subclauses.empty?
      conditions.push(subclauses.join(' OR '))
    end
    # add the value references
    unless subrefs.empty?
      conditions.concat(subrefs)
    end
    
    # abort if all values are blank
    return model_class.none if conditions.empty?
    
    # apply the scopes
    result = model_class.includes(options[:includes]).where(conditions)

    if options[:association]
      result = result.references(options[:association])
    end
    result
  end
  
  # A simple date range finder (only supports dates, but can compare with datetime/time columns).
  # Returns an empty relation if neither start nor stop options are specified.
  #
  # Arguments (required):
  #
  #   model_or_string::
  #     (Class | String | Symbol) model class or model class name. validated with valid_model()
  #   field_symbol::
  #     (String | Symbol) column name to search. validated with valid_fields()
  #
  # Options:
  #
  #   start::
  #     (String | nil) The start date of the range.  If nil, considers from beginning of recorded time.
  #   stop::
  #     (String | nil) The stop date of the range.  If nil, considers to the end of recorded time.
  #   association::
  #     (String | Symbol | nil) The association used to access each of the columns. defaults to nil.
  #   includes::
  #     (String | Symbol | Array | nil) the associations to include for the query. defaults to nil.
  #   date::
  #     (Bool) Specifies to use date values instead of time values (the default) when true
  #
  # Note: this is intended to support the scopes written to process filter queries... other uses
  #  may or may not work as intended.
  #
  # Returns a relation
  def self.for_daterange(model_or_string, field_symbol, *args)
    options = HashWithIndifferentAccess.new(association: nil, includes: nil, start: nil, stop: nil, date: false)
    options.update(args.extract_options!)

    # force includes if association is specified, but don't override specified value
    options[:includes] ||= options[:association]
    
    # get the model class
    model_class = valid_model(model_or_string)
    
    # make sure field name is not blank
    field = valid_fields(model_class, options[:association], field_symbol, array: false)
    
    # get table prefix
    prefix = valid_prefix(model_class, options[:association])
    
    # validate dates/times
    start = valid_date(options[:start])
    stop = valid_date(options[:stop])
    
    # return empty scope if both dates invalid
    return model_class.none if start.nil? && stop.nil?
    
    # swap dates if in wrong order
    unless start.nil? || stop.nil?
      if start > stop
        start, stop = [stop, start]
      end
    end
    
    # convert to time if necessary
    unless options[:date]
      unless start.nil?
        start = start.to_time
      end
      unless stop.nil?
        stop = stop.to_time.end_of_day
      end
    end
    
    # apply the scopes
    if start.nil?
      result = model_class.includes(options[:includes]).where("#{prefix}.#{field} <= ?", stop)
    elsif stop.nil?
      result = model_class.includes(options[:includes]).where("#{prefix}.#{field} >= ?", start)
    else
      result = model_class.includes(options[:includes]).where("#{prefix}.#{field} >= ? AND #{prefix}.#{field} <= ?", start, stop) 
    end

    if options[:association]
      result = result.references(options[:association])
    end
    result
  end

  # A simple boolean finder
  #
  # Arguments (required):
  #
  #   model_or_string::
  #     (Class | String | Symbol) model class or model class name. validated with valid_model()
  #   field_symbol::
  #     (String | Symbol) column name. validated with valid_fields()
  #   value::
  #     (Bool) the boolean value ('0'/'1'/0/1/true/false) to compare.  Note: to prevent nil
  #     from being considered a boolean value (false), this method returns empty relation
  #     if value is nil. If value is blank, it will be interpreted as 0 (false).
  #  
  # Options:
  #
  #   association::
  #     (String | Symbol) the association used to access each of the columns. defaults to nil.
  #   includes::
  #     (String | Symbol | Array | nil) the associations to include for the query. defaults to nil.
  #
  # Note: this is intended to support the scopes written to process filter queries... other uses
  #  may or may not work as intended.
  #
  # Returns a relation
  def self.for_bool(model_or_string, field_symbol, value, *args)
    options = HashWithIndifferentAccess.new(association: nil, includes: nil)
    options.update(args.extract_options!)

    # force includes if association is specified, but don't override specified value
    options[:includes] ||= options[:association]
    
    # get model class
    model_class = valid_model(model_or_string)
    
    # normalize the field names
    field = valid_fields(model_class, options[:association], field_symbol, array: false)
    
    # get the table prefix
    prefix = valid_prefix(model_class, options[:association])
    
    # don't allow nils to be false
    if value.nil?
      return model_class.none
    end
    
    # translate value to boolean
    value = valid_bool(value)
    
    # had to do it this way because SQLite does not have boolean values so cannot use
    # 'IS TRUE' and 'IS NOT TRUE' sql statement fragments
    if options[:association]
      result = model_class.includes(options[:includes]).where(prefix.to_sym => {field.to_sym => value}).references(options[:association])
    else
      result = model_class.includes(options[:includes]).where(field.to_sym => value)
    end
    result
  end
  
  # A simple "like" finder that supports multiple columns
  #
  # Arguments (required):
  #
  #   model_or_string::
  #     (Class | String | Symbol) model class or model class name. validated with valid_model()
  #   field_symbols::
  #     (Array | String | Symbol) array of column names. validated with valid_fields()
  #   value::
  #     (String) the string value to compare. Returns empty relation if this is blank
  #  
  # Options:
  #
  #   association::
  #     (String | Symbol) the association used to access each of the columns. defaults to nil.
  #   includes::
  #     (String | Symbol | Array | nil) the associations to include for the query. defaults to nil.
  #
  # Note: this is intended to support the scopes written to process filter queries... other uses
  #  may or may not work as intended.
  #
  # Returns a relation
  def self.like(model_or_string, field_symbols, value, *args)
    options = HashWithIndifferentAccess.new(association: nil, includes: nil)
    options.update(args.extract_options!)

    # force includes if association is specified, but don't override specified value
    options[:includes] ||= options[:association]
    
    # get model class
    model_class = valid_model(model_or_string)
    
    # get normalized fields
    fields = valid_fields(model_class, options[:association], field_symbols)
    
    # set the table prefix
    prefix = valid_prefix(model_class, options[:association])
    
    # return empty relation if value is blank
    return model_class.none if value.to_s.blank?
    
    # build an array of 'LIKE' clauses
    conditions = []
    fields.each do |f|
      conditions.push("LOWER(#{prefix}.#{f.to_s}) LIKE LOWER(?)")
    end
    
    # combine the clauses
    sql_text = conditions.join(" OR ")
    
    # create an array condition with appropriate number of values
    condition = [sql_text]
    fields.size.times do
      condition.push("%#{value}%")
    end
    
    # apply the scopes
    result = model_class.includes(options[:includes]).where(condition)
    if options[:association]
      result = result.references(options[:association])
    end
    result
  end
  
  # uses Namae::Name.parse to normalize the given name string and return it in
  # family-name first order.
  #
  # Arguments:
  #
  #   name::
  #     (String) string to normalize
  #   given_first::
  #     (Bool) If false, returns name in family-first order (default). If true, name is
  #     returned in given-first order.
  #
  # Returns String
  def self.parse_name(name, given_first=false)
    return '' if name.blank? || !name.is_a?(String)
    n = Namae::Name.parse(name)
    if given_first
      n.display_order
    else
      n.sort_order
    end
  end
  
  # Returns a string consisiting of a Person's id and full name.
  #
  # Arguments:
  #
  #   object::
  #     (Person | Integer) Person instance or id number
  #   show_id::
  #     (Bool) If true (default), includes ID with Name. If false, returns Name only.
  #
  # Returns String
  def self.show_person(object, show_id=true)
    if object.is_a?(Integer)
      object = Person.find_by(id: object)
    end
    return "Unknown" unless object.is_a?(Person)
    if show_id
      "[#{object.id}] " + object.sort_name
    else
      object.sort_name
    end
  end
  
  # Returns a string consisiting of a contact unit, name and badge.
  #
  # Arguments:
  #
  #   object::
  #     (Contact | User | Class) a model object containing the contact columns
  #
  # Options:
  #
  #   field::
  #     (String | Symbol) base column name (without _unit or _badge). Ignored if object is a Contact or User
  #   badge::
  #     (Bool) if true, includes Badge.  If false (default), no badge is shown.
  #
  # Returns String
  def self.show_contact(object, *args)
    options = HashWithIndifferentAccess.new(field: nil, badge: false)
    options.update(args.extract_options!)
    
    class_name = object.class.name
    
    case class_name
    when 'User'
      u = object.unit
      n = object.sort_name
      b = object.badge
    when 'Contact'
      u = object.unit
      n = object.sort_name
      b = object.badge_no
    else
      if options[:field].blank?
        u = ''
        n = "Unknown"
        b = ''
      else
        u = object.respond_to?("#{options[:field]}_unit") ? object.send("#{options[:field]}_unit") : ''
        n = object.respond_to?("#{options[:field]}") ? object.send("#{options[:field]}") : 'Unknown'
        b = object.respond_to?("#{options[:field]}_badge") ? object.send("#{options[:field]}_badge") : ''
      end
    end
    return "#{u.blank? ? '' : u + ' '}#{n}#{options[:badge] && !b.blank? ? ' (' + b + ')' : ''}"
  end
  
  # Returns a string consisting of a User name or login depending on whether
  # the user has a valid contact link. Invalid object returns 'Unknown'
  #
  # Arguments:
  #
  #   object::
  #     (User | Integer) The User or user.id to display
  #
  # Returns String
  def self.show_user(object)
    if object.is_a?(Integer)
      object = User.find_by(id: object)
    end
    if object.is_a?(User)
      object.display_name
    else
      'Unknown'
    end
  end
  
  # Strips all characters from case_no except for numbers, letters, and -
  #
  # Arguments:
  #
  #   case_no::
  #     (String) The case number to strip
  #
  # Returns String
  def self.fix_case(case_no)
    return nil unless case_no.is_a?(String)
    case_no.gsub(/[^\dA-Za-z-]/, '')
  end
  
  # Returns difference between two Times
  #
  # Arguments:
  #
  #   time1::
  #     (Time) Start Time
  #   time2::
  #     (Time) Stop Time. Defaults to current Time if nil.
  #
  # Returns Integer
  def self.datetime_diff(time1, time2=Time.now)
    if time1.is_a?(DateTime)
      time1 = time1.to_time
    end
    if time2.is_a?(DateTime)
      time2 = time2.to_time
    end
    return 0 unless time1.is_a?(Time) && time2.is_a?(Time)
    if time1 > time2
      time1 - time2
    else
      time2 - time1
    end
  end
  
  # Returns number of seconds between two Times
  #
  # Arguments:
  #
  #   time1::
  #     (Time) Start Time
  #   time2::
  #     (Time) Stop Time. Defaults to current Time if nil.
  #
  # Returns Integer
  def self.seconds_diff(time1, time2=Time.now)
    datetime_diff(time1,time2).round
  end
  
  # Returns number of minutes between two Times
  #
  # Arguments:
  #
  #   time1::
  #     (Time) Start Time
  #   time2::
  #     (Time) Stop Time. Defaults to current Time if nil.
  #
  # Returns Integer
  def self.minutes_diff(time1, time2=Time.now)
    (datetime_diff(time1,time2) / 60.0).round
  end
  
  # Returns number of hours between two Times
  #
  # Arguments:
  #
  #   time1::
  #     (Time) Start Time
  #   time2::
  #     (Time) Stop Time. Defaults to current Time if nil.
  #
  # Returns Integer
  def self.hours_diff(time1, time2=Time.now)
    (datetime_diff(time1,time2) / 3600.0).round
  end
  
  # Returns number of days between two Times
  #
  # Arguments:
  #
  #   time1::
  #     (Time) Start Time
  #   time2::
  #     (Time) Stop Time. Defaults to current Time if nil.
  #
  # Returns Integer
  def self.days_diff(time1, time2=Time.now)
    (datetime_diff(time1,time2) / 3600.0 / 24.0).round
  end
  
  # Returns number of months between two Times. A month is
  # considered to be 30 days.
  #
  # Arguments:
  #
  #   time1::
  #     (Time) Start Time
  #   time2::
  #     (Time) Stop Time. Defaults to current Time if nil.
  #
  # Returns Integer
  def self.months_diff(time1, time2=Time.now)
    (datetime_diff(time1,time2) / 3600.0 / 24.0 / 30.0).round
  end
  
  # Returns number of Years between two Times. A year is
  # considered to be 365 days.
  #
  # Arguments:
  #
  #   time1::
  #     (Time) Start Time
  #   time2::
  #     (Time) Stop Time. Defaults to current Time if nil.
  #
  # Returns Integer
  def self.years_diff(time1, time2=Time.now)
    (datetime_diff(time1,time2) / 3600.0 / 24.0 / 365.0).round
  end
  
  # Commonly used to format a date string for views with validation
  # 
  # Arguments:
  #
  #   date_or_time::
  #     (Date | Time | String) A Date or Time object. May also be string consisting
  #     of 'now' for Time, or 'today' for date.
  #   time_string::
  #     (String) a properly formatted time string (24 hour) as 'HH:MM' such as '18:30'.
  #     Default is nil (blank)
  #
  # Options:
  #
  #   format::
  #     (Symbol) One of :short (default), :long or :timestamp. :short = "MM/DD/YYYY HH:MM"
  #     for Time or "MM/DD/YYYY" for Date. :long = "Sunday, January 2, 2015 HH:MM TZ" for
  #     Time or "Sunday, January 2, 2015" for Date. :timestamp = "YYYYMMDDHHMMSS" for Time,
  #     or "YYYYMMDD" for Date.
  #
  # Returns String
  def self.format_date(date_or_time, *args)
    options = HashWithIndifferentAccess.new(format: :short)
    options.update(args.extract_options!)
    
    # if a time_string is specified, it will be first element remaining in args
    time_string = args.first
    
    # make sure format is valid
    if options[:format].is_a?(String)
      options[:format] = options[:format].to_sym
    end
    unless [:short, :long, :timestamp].include?(options[:format])
      options[:format] = :short
    end
    
    # date or time is required
    return "" if date_or_time.blank?
    
    # all string values of 'now' or 'today' in place of date or time
    if date_or_time.is_a?(String)
      if date_or_time == 'now'
        d = Time.now
      elsif date_or_time == 'today'
        d = Date.today
      end
    else
      d = date_or_time
    end
    return "" unless d.is_a?(Date) || d.is_a?(Time)
    
    # check for time_string
    if time_string.blank?
      value = d
    else
      # allow formatting date values with separate time string
      unless value = get_datetime(d,time_string)
        return ''
      end
    end
    
    # apply requested formatting
    if options[:format] == :timestamp
      value.to_s(:timestamp)
    elsif options[:format] == :long
      value.to_s(:us_long)
    else
      value.to_s(:us)
    end
  end
  
  # Converts a Date, a Date and a time_string, or a Time into a valid Time object
  #
  # Arguments:
  # 
  #   date::
  #     (Date | Time) A Date (or Time) object.
  #   time::
  #     (String) A valid time (24 Hour) string as 'HH:MM', such as '18:30'
  #
  # Returns Time object
  def self.get_datetime(date=nil,time=nil)
    return nil unless (date.is_a?(Date) && (time.nil? || (time.is_a?(String) && time =~ /\d\d:\d\d/))) || date.is_a?(Time)
    return date if date.is_a?(Time)
    time = "00:00" if time.blank?
    begin
      Time.parse("#{date} #{time}")
    rescue ArgumentError
      nil
    end
  end
  
  # Converts a Time object into separate Date and time string ('HH:MM' 24 Hour format)
  #
  # Arguments:
  #
  #   datetime::
  #     (Time) The Time object to convert. Works with DateTime too.
  #
  # Returns [Date, String] Array
  def self.get_date_and_time(datetime)
    return [nil,nil] unless datetime.is_a?(Time) || datetime.is_a?(DateTime)
    if datetime.is_a?(DateTime)
      datetime = Time.parse(datetime.to_s)
    end
    return [datetime.to_date, datetime.to_s(:time)]
  end
  
  # Accepts total hours and returns a sentence. This is for calculating sentence
  # and uses standard length months (30 days) and years (365 days) instead of actual
  # calendar durations.
  # 
  # Arguments:
  #
  #   total_hours::
  #     (Integer) Total hours to convert to sentence. Note: special case
  #     where hours = -999 is used to indicate a death sentence. Otherwise,
  #     when hours is negative, indicates that number of life sentences.
  #
  # Options:
  #
  #   returns::
  #     (Symbol | String) specifies return format.  If :array, returns a sentence
  #     Array (array of Integers representing hours, days, months, years, etc.
  #     If :string, returns sentence as a string for display. Defaults to :array
  #
  # Returns Array or String
  def self.hours_to_sentence(total_hours, *opts)
    options = HashWithIndifferentAccess.new(returns: :array)
    options.update(opts.extract_options!)
    
    if options[:returns].is_a?(String)
      options[:returns] = options[:returns].to_sym
    end
    unless [:array,:string].include?(options[:returns])
      options[:returns] = :array
    end
    
    total_hours = total_hours.to_i
    total_hours = 0 if total_hours.nil?
    
    
    death = false
    hours = 0
    life = 0
    years = 0
    months = 0
    days = 0
    if total_hours < 0
      death = true
    else
      hours = total_hours
      while hours >= 876000 do
        hours -= 876000
        life += 1
      end
      while hours >= 8760 do
        hours -= 8760
        years += 1
      end
      while hours >= 720 do
        hours -= 720
        months += 1
      end
      while hours >= 24 do
        hours -= 24
        days += 1
      end
    end
    
    if options[:returns] == :array
      return [hours,days,months,years,life,death]
    else
      return sentence_to_string(hours,days,months,years,life,death)
    end
  end
  
  # Converts a sentence array into total hours. This is for sentencing
  # purposes and considers standard length months (30 days) and years (365 days).
  #
  # Arguments:
  #
  #   hours::
  #     (Integer | Array)  This may be the hours portion of the sentence when passing
  #     sentence parts individually, or the actual sentence Array itself.
  #   args::
  #     (Integer values or nil) If passing sentence parts individually, args will
  #     have multiple Integer values representing days, months, years, life, death
  #     remaining after extracting options from it.
  #
  # Options:
  #
  #   returns::
  #     (String | Symbol) Specifies format to return (Integer hours, or String hours)
  #     Permissable values are :number and :string.  Defaults to :number.
  # 
  # Returns Integer or String
  def self.sentence_to_hours(hours, *args)
    options = HashWithIndifferentAccess.new(returns: :number)
    options.update(args.extract_options!)
    
    if options[:returns].is_a?(String)
      options[:returns] = options[:returns].to_sym
    end
    unless [:number,:string].include?(options[:returns])
      options[:returns] = :number
    end
    
    if hours.is_a?(Array)
      hours,days,months,years,life,death = hours
    else
      days,months,years,life,death = args
    end
    
    hours = hours.to_i
    days = days.to_i
    months = months.to_i
    years = years.to_i
    life = life.to_i
    total = 0
    if death == true
      total = -1
    else
      total += hours
      total += (days * 24)
      total += (months * 30 * 24)
      total += (years * 365 * 24)
      total += (life * 365 * 24 * 100)
    end
    
    if options[:returns] == :number
      return total
    else
      return hours_to_string(total)
    end
  end
  
  # Converts a sentence array into a string for display.
  #
  # Arguments:
  #
  #   hours::
  #     (Integer | Array)  This may be the hours portion of the sentence when passing
  #     sentence parts individually, or the actual sentence Array itself.
  #   args::
  #     (Integer values or nil) If passing sentence parts individually, args will
  #     have multiple Integer values representing days, months, years, life, death
  #     remaining after extracting options from it.
  #
  # Options:
  #
  #   short::
  #     (Bool) If true, labels are returned as single letters (H for hours, etc). If
  #     false (default) labels are full words (Hours for hours, etc).
  #   include_zeros::
  #     (Bool) If true, zero values are included in the string.  If false (default)
  #     only values greater than zero are included.
  # 
  # Returns String
  def self.sentence_to_string(hours, *args)
    options = HashWithIndifferentAccess.new(short: false, include_zeros: false)
    options.update(args.extract_options!)
    
    if hours.is_a?(Array)
      hours,days,months,years,life,death = hours
    else
      days, months, years, life, death = args
    end
    
    hours = hours.to_i
    days = days.to_i
    months = months.to_i
    years = years.to_i
    life = life.to_i
    parts = []
    if death == true
      return "Death Sentence"
    else
      if life > 0
        parts.push("#{life} Life Sentence#{life == 1 ? '' : 's'}")
      end
      if years > 0 || (years == 0 && options[:include_zeros] == true)
        parts.push("#{years} Year#{years == 1 ? '' : 's'}")
      end
      if months > 0 || (months == 0 && options[:include_zeros] == true)
        parts.push("#{months} Month#{months == 1 ? '' : 's'}")
      end
      if days > 0 || (days == 0 && options[:include_zeros] == true)
        parts.push("#{days} Day#{days == 1 ? '' : 's'}")
      end
      if hours > 0 || (hours == 0 && options[:include_zeros] == true)
        parts.push("#{hours} Hour#{hours == 1 ? '' : 's'}")
      end
    end
    txt = parts.join(', ')
    
    if options[:short] == true
      txt = txt.sub(/ Hours?/,'H').sub(/ Days?/,'D').sub(/ Months?/, 'M').sub(/ Years?/,'Y').sub(/ Life Sentences?/,'L')
    end
    
    return txt
  end
  
  # Converts total hours into a string specifying days, unless the result is less
  # than a day where it switches to hours.  If hours are negative, this is a special
  # case used to indicate a death sentence.
  #
  # Arguments:
  #
  #    hours::
  #      (Integer) Total number of hours remaining to serve.
  #
  # Returns String
  def self.hours_to_string(hours)
    if hours.respond_to?('to_i')
      hours = hours.to_i
    else
      hours = 0
    end
    
    if hours < 0
      return "Death Sentence"
    else
      days = hours / 24.0
      if days >= 1
        return "#{days.round} Day#{days.round == 1 ? '' : 's'}"
      else
        return "#{hours} Hour#{hours == 1 ? '' : 's'}"
      end
    end
  end
  
  # Accepts a string representation of date and returns a valid date
  # object if it can be parsed, or nil if not
  #
  # Arguments:
  #
  #   datestring::
  #     (String) any date string parsable by Chronic gem
  #
  # Options:
  #
  #   allow_nil::
  #     (Bool) If true (default), returns nil unless date string is valid. If False, returns
  #     current Date if date string is invalid.
  #
  # Returns Date object or nil.
  def self.valid_date(datestring, *opts)
    options = HashWithIndifferentAccess.new(allow_nil: true)
    options.update(opts.extract_options!)
    if options[:allow_nil] == false
      blank_value = Date.today
    else
      blank_value = nil
    end
    return blank_value if datestring.blank?
    return datestring if datestring.is_a?(Date)
    return datestring.to_date if datestring.is_a?(Time)
    return blank_value unless datestring.is_a?(String)
    begin
      return Chronic.parse(datestring).to_date
    rescue
      return blank_value
    end
  end
  
  # Accepts a string representation of date and time, and returns a valid time
  # object if it can be parsed, or nil if not
  #
  # Arguments:
  #
  #   datestring::
  #     (String) any date/time string parsable by Chronic gem
  #
  # Options:
  # 
  #   allow_nil::
  #     (Bool) If true (default), invalid dates return nil.  If false, invalid dates
  #     return current Time
  #
  # returns Time or nil
  def self.valid_datetime(datestring, *opts)
    options = HashWithIndifferentAccess.new(allow_nil: true)
    options.update(opts.extract_options!)
    
    if options[:allow_nil] == false
      blank_value = Time.now
    else
      blank_value = nil
    end
    
    return blank_value if datestring.blank?
    return Time.parse("#{datestring} 00:00") if datestring.is_a?(Date)
    return datestring if datestring.is_a?(Time)
    return blank_value unless datestring.is_a?(String)
    
    if datestring =~ /^\d{1,2}-\d{1,2}-\d{4}.*$/
      datestring = datestring.gsub(/-/,'/')
    end
    begin
      return Chronic.parse(datestring)
    rescue
      return blank_value
    end
  end
  
  # Checks a supplied time string... if it is a 4 digit string, it separates
  # the first two digits from the last two by inserting a colon (:).
  #
  # Arguments:
  #
  #   timestring::
  #     (String) Should be valid 24 Hour time string as 'HH:MM', with or without the colon
  #
  # Options:
  #
  #   allow_nil::
  #     (Bool) If true (default), returns nil for invalid time string.  If false, returns
  #     midnight (00:00) for invalid time strings.
  def self.valid_time(timestring, *opts)
    options = HashWithIndifferentAccess.new(allow_nil: true)
    options.update(opts.extract_options!)

    if options[:allow_nil] == false
      blank_value = "00:00"
    else
      blank_value = nil
    end

    return blank_value if timestring.blank?
    return blank_value unless timestring.is_a?(String)
    return blank_value unless timestring =~ /^\d{2}:?\d{2}:?\d{0,2}$/

    # strip seconds
    timestring = timestring.sub(/^(\d{2}:?\d{2}).*$/, '\1')
    hours = timestring.slice(0,2).to_i
    minutes = timestring.slice(-2,2).to_i
    if hours > 23
      hours = 23
    end
    if minutes > 59
      minutes = 59
    end
    return "#{hours.to_s.rjust(2,'0')}:#{minutes.to_s.rjust(2,'0')}"
  end
  
  # Returns string representing the time portion of Time.now (24 hour format)
  #
  # Returns String
  def self.current_time
    Time.now.strftime("%H:%M")
  end
  
  # Accepts two datetimes and return the difference in days, hours and minutes
  # as a string
  #
  # Arguments:
  #
  #   from_datetime::
  #     (Time) Starting Time object.
  #   to_datetime::
  #     (Time or nil) Ending Time object. Defaults to current time.
  #
  # Returns String
  def self.duration_in_words(from_datetime, to_datetime = Time.now)
    return "0 Minutes" unless from_datetime.is_a?(Time) && to_datetime.is_a?(Time)
    
    minutes = minutes_diff(from_datetime, to_datetime)
    days = 0
    hours = 0
    while minutes >= 1440 do
      minutes -= 1440
      days += 1
    end
    while minutes >= 60 do
      minutes -= 60
      hours += 1
    end
    
    txt = ""
    if days > 0
      unless txt.empty?
        txt << ", "
      end
      txt << "#{days} Day#{days == 1 ? '' : 's'}"
    end
    if hours > 0
      unless txt.empty?
        txt << ", "
      end
      txt << "#{hours} Hour#{hours == 1 ? '' : 's'}"
    end
    if minutes > 0
      unless txt.empty?
        txt << ", "
      end
      txt << "#{minutes} Minute#{minutes == 1 ? '' : 's'}"
    end
    txt
  end
  
  # Converts a 24 hour time(only) string to AM/PM
  # anything other than ##:## will return 00:00 AM
  #
  # Arguments:
  #
  #   time::
  #     (String) A string representing a time in 24 hour format as 'HH:MM'
  #
  # Returns String
  def self.mil_to_civ(time)
    return "00:00 AM" unless time.is_a?(String) && time =~ /^\d\d:\d\d$/
    
    hours, minutes = time.split(':')
    if hours.to_i > 23
      hours = 23
    else
      hours = hours.to_i
    end
    if minutes.to_i > 59
      minutes = 59
    else
      minutes = minutes.to_i
    end
    
    txt = ""
    suffix = ' AM'
    if hours > 12
      hours = hours - 12
      suffix = ' PM'
    end
    shours = hours.to_s
    if shours.size == 1
      shours = '0' + shours
    end
    sminutes = minutes.to_s
    if sminutes.size == 1
      sminutes = '0' + sminutes
    end
    shours + ':' + sminutes + suffix
  end
  
  # Accepts beginning odometer reading and ending odometer reading
  # and calculates total miles travelled.  If beginning is larger than
  # ending, assumes odometer rollover. Only handles Integer values (no tenths)
  #
  # Arguments:
  #
  #   start::
  #     (Integer) Starting odometer reading
  #   stop::
  #     (Integer) Ending odometer reading
  #
  # Returns Integer
  def self.total_miles(start,stop)
    return 0 unless start.is_a?(Integer) && stop.is_a?(Integer)
    stopm = stop
    if start > stopm
      # assume odometer roll-over ??
      digits = start.to_s.length
      roll = "1".ljust(digits + 1, "0")
      stopm + roll.to_i - start
    else
      stop - start
    end
  end
  
  # Adds a message to the audit log
  #
  # Arguments:
  #
  #   msg::
  #     (String) The message text to add.
  #   method_name::
  #     (String) The name of the calling method (accepts any object that responds to 'to_s')
  #   uid::
  #     (Integer) The id of the current user.  Defaults to 1 (admin).
  #   severity::
  #     (Integer) The severity level for the log entry. Defaults to 1
  #
  # Returns: no useful value
  def self.audit_log(msg, method_name, uid=1, severity=1)
    if method_name.blank?
      method_name = "Unknown Method"
    end
    if msg.respond_to?('to_s')
      message = msg.to_s
    else
      message = msg.inspect
    end
    message << " (UID #{uid})"
    Audit.add(severity, message, method_name)
  end
  
  # Accepts string and returns it with all whitespace sequences compressed
  # to a single space (including newlines and carriage returns)
  #
  # Arguments:
  #
  #   string::
  #     (String) String to compress
  #
  # Returns String
  def self.compress_spaces(string)
    string.to_s.gsub(/\s+/,' ')
  end
  
  # Validates a model class. Will raise an ArgumentError if the resulting class
  # is not defined or is not descended from ActiveRecord. The model can be supplied
  # as a camel-case or underscored model name, or as the model class itself
  # (usually by passing self from the model).
  #
  # Arguments:
  #
  #   model_or_string::
  #     (String | Symbol | Class) Should be, or represent a Model class (ActiveRecord descendent)
  #   
  # Returns: valid Model Class constant or raises ArgumentError exception
  def self.valid_model(model_or_string)
    if model_or_string.is_a?(String) || model_or_string.is_a?(Symbol)
      model_class = model_or_string.to_s.classify.safe_constantize
    else
      model_class = model_or_string
    end
    
    # check for valid Model
    if !model_class.respond_to?('descends_from_active_record?') || !model_class.descends_from_active_record?
      raise(ArgumentError.new("model class could not be determined from '#{model_or_string.inspect}'"))
    end
    model_class
  end
  
  # Validates that the supplied argument is an array of Strings or Symbols. Any elements that
  # fail this validation or are blank are removed from the array. If the argument is a String
  # or Symbol instead of an Array, it is wrapped in an Array before being returned.
  #
  # Arguments:
  #
  #   model_class::
  #     (Class) The Model Class to check for column names or optional association.
  #   association::
  #     (Symbol) Optional association to check for column names.
  #   array_or_string::
  #     (String | Symbol | Array) A column name or array of column names to check.
  #
  # Options:
  #
  #   array::
  #     (Boolean) true (default) if should return array of columns, false if should return
  #     single column string/symbol
  #
  # Returns: array of string/symbol values or single string/symbol value.
  def self.valid_fields(model_class, association, array_or_string, *args)
    options = HashWithIndifferentAccess.new(array: true)
    options.update(args.extract_options!)
    
    fields = []
    
    if (array_or_string.is_a?(Symbol) || array_or_string.is_a?(String)) && !array_or_string.to_s.blank?
      fields.push(array_or_string)
    elsif array_or_string.is_a?(Array)
      array_or_string.each do |s|
        fields.push(s) if (s.is_a?(String) || s.is_a?(Symbol)) && !s.to_s.blank?
      end
    end
    
    # if fields array empty, raise ArgumentError
    if fields.empty?
      raise(ArgumentError.new("no valid column names were found in '#{array_or_string.inspect}'"))
    else
      fields.each do |f|
        if association.blank?
          object = model_class.new
        else
          ref = model_class.reflect_on_association(association)
          tmp = valid_model(ref.table_name.singularize)
          object = tmp.new
        end
        unless object.respond_to?(f.to_s)
          raise(ArgumentError.new("column name '#{f}' is invalid for #{object.class.name}"))
        end
      end
    end
    if options[:array]
      fields
    else
      fields[0]
    end
  end
  
  # accepts strings, integers and boolen values and returns a boolean value so that
  # integer > 0, string.to_i > 0, 't', 'T', 'true', 'True', true == true
  # anything else = false!!!
  #
  # Arguments:
  #
  #   value::
  #     (String | Integer | Boolean) A true or false value. integer > 0, string.to_i > 0,
  #     't', 'T', 'true', 'True', true == true.  Anything else == false.
  #
  # Returns true/false (boolean)
  def self.valid_bool(value)
    result = false
    if value.is_a?(String)
      if ['t', 'true'].include?(value.downcase) || value.to_i > 0
        result = true
      end
    elsif value.is_a?(Integer) || value.is_a?(Float)
      if value.to_i > 0
        result = true
      end
    elsif value.is_a?(TrueClass)
      result = true
    end
    result
  end
  
  # generate a prefix suitable for sql query.  If association is supplied, the prefix is
  # set to the table_name for that association.  If no association supplied, the prefix
  # is set to the table name of the model.
  #
  # Arguments:
  #
  #   model::
  #     (Class) Must be a valid Model Class
  #   association::
  #     (String | Symbol) Must be valid association defined for Model Class
  #
  # returns table name for association (if given) or Model (if not)
  def self.valid_prefix(model, association = nil)
    return '' unless association.nil? || association.is_a?(String) || association.is_a?(Symbol)
    return '' unless model.respond_to?('descends_from_active_record?') && model.descends_from_active_record?
    prefix = model.table_name
    if association
      if ref = model.reflect_on_association(association.to_sym)
        prefix = ref.table_name
      else
        raise ArgumentError.new("'#{association}' is not a valid association for #{model}")
      end
    end
    prefix
  end
end