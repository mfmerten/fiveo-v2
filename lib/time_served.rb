# manipulates multiple TimeRange objects and calculate hours served
#
# nil_value: can only be set on initialize and cannot be changed!
class TimeServed
  attr_reader :ranges, :excludes, :adjusted_ranges, :options
    
  def initialize(*ranges)
    # initialize options
    @options = HashWithIndifferentAccess.new(nil_value: nil)
    @options.update ranges.extract_options!.symbolize_keys
      
    # initialize ranges
    @ranges = []
    @excludes = []
    @adjusted_ranges = []
    process_ranges(ranges)
  end
    
  def add_excludes(*ranges)
    process_excludes(ranges)
  end
    
  def add_ranges(*ranges)
    process_ranges(ranges)
  end
    
  def hours
    total = 0
    @adjusted_ranges.each{|r| total += r.hours}
    total
  end
    
  def options(key=nil)
    if key.blank?
      @options
    else
      @options[key.to_sym]
    end
  end
  
  private

  def dirty?
    d = false
    d = true unless @ranges.reject{|r| !r.dirty?}.empty?
    d = true unless @excludes.reject{|r| !r.dirty?}.empty?
    d
  end
  
  def merge_ranges
    # don't need to do this unless something has changed
    # return unless dirty?
    
    # merge the time ranges
    unless @ranges.empty?
      ranges = @ranges.sort
      @ranges = []
      
      if current_range = ranges.shift
        while true do
          unless next_range = ranges.shift
            current_range.dirty = false
            @ranges.push(current_range)
            break
          end
          current_range, next_range = current_range.merge_if_overlap(next_range)
          unless next_range.nil?
            @ranges.push(current_range)
            current_range = next_range
          end
        end
      end
    end
    
    # merge the excluded ranges
    unless @excludes.empty?
      ranges = @excludes.sort
      @excludes = []
      
      if current_range = ranges.shift
        while true do
          unless next_range = ranges.shift
            current_range.dirty = false
            @excludes.push(current_range)
            break
          end
          current_range, next_range = current_range.merge_if_overlap(next_range)
          unless next_range.nil?
            @excludes.push(current_range)
            current_range = next_range
          end
        end
      end
    end
    
    # clear out previously adjusted ranges
    @adjusted_ranges = []
    tmp = []
    ranges = []
    
    if @ranges.empty? || @excludes.empty?
      @adjusted_ranges = @ranges
      return
    end
    
    # adjust time ranges to exclude excluded ranges
    @ranges.each do |r|
      tmp.concat(r.exclude_if_overlap(@excludes))
    end
    ranges = tmp.compact.sort
    
    unless ranges.empty?
      if current_range = ranges.shift
        while true do
          unless next_range = ranges.shift
            current_range.dirty = false
            @adjusted_ranges.push(current_range)
            break
          end
          current_range, next_range = current_range.merge_if_overlap(next_range)
          unless next_range.nil?
            @adjusted_ranges.push(current_range)
            current_range = next_range
          end
        end
      end
    end
  end
    
  def process_excludes(array)
    return nil if array.empty?
    
    ranges = []
    array.flatten.each_slice(2){|a,b| ranges.push([a,b]) }
    
    ranges.each do |start,stop|
      # weed out completely nil ranges
      unless start.nil? && stop.nil?
        
        # adjust considering options
        if options[:nil_value].is_a?(Time)
          start_value = start || options[:nil_value]
          stop_value = stop || options[:nil_value]
        else
          start_value = start
          stop_value = stop
        end
        
        # weed out partially nil ranges
        unless start_value.nil? || stop_value.nil?
          if tmp = TimeRange.new(start_value,stop_value)
            @excludes.push(tmp)
          end
        end
      end
    end
    
    merge_ranges
  end
    
  def process_ranges(array)
    return nil if array.empty?
    
    ranges = []
    array.flatten.each_slice(2){|a,b| ranges.push([a,b]) }
    
    ranges.each do |start,stop|
      # weed out completely nil ranges
      unless start.nil? && stop.nil?
        
        # adjust considering options
        if options[:nil_value].is_a?(Time)
          start_value = start || options[:nil_value]
          stop_value = stop || options[:nil_value]
        else
          start_value = start
          stop_value = stop
        end
        
        # weed out partially nil ranges
        unless start_value.nil? || stop_value.nil?
          if tmp = TimeRange.new(start_value,stop_value)
            @ranges.push(tmp)
          end
        end
      end
    end
      
    merge_ranges
  end
end
