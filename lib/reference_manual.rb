# this module contains the code needed to update the reference manual html
# files from the source textile files. the update task is usually run via
# rake as app:update_manual.
#
# The RedCloth gem is used to build a set of html files (in the
# doc/reference_manual/html/ directory) from the *.textile files in the
# doc/reference_manual/textile/ directory.
#
# the html/ directory is symlinked into the application public/manual
# directory and access to the manual is as simple as
#    https://<server>/manual/index.html
#
# the html pages in the reference manual are also automatically linked into
# the software using the Help button that appears on each page. The manual
# page corresponding to the current controller name is displayed in a popup
# window when Help is clicked.
#
module ReferenceManual

  require 'redcloth'
  
  ManualBasePath = "#{Rails.root}/doc/reference_manual"
  ManualHtmlPath = "#{ManualBasePath}/html"
  ManualTextilePath = "#{ManualBasePath}/textile"
  
  # build the manual pages
  def self.update
    # get a list of the textile files
    pages = []
    Dir.chdir(ManualTextilePath) do 
      pages = Dir["*.textile"].sort
    end

    # we treat index, common and toc specially (not in sort order)
    pages.delete("index.textile")
    pages.delete("common.textile")
    pages.delete("toc.textile")

    # process the toc page first
    print "Processing: toc.textile... "
    html_page = ""
    html_page << header
    html_page << RedCloth.new(get_raw_page('toc')).to_html
    html_page << footer
    print "Writing toc.html... "
    write_page('toc', html_page)
    puts "Done."

    # process the index page
    print "Processing: index.textile... "
    html_page = ""
    html_page << header
    # for this page, have to insert navigation bars...
    raw_page = get_raw_page('index')
    navigation = get_nav_bar('index',nil,'common')
    navable_page = raw_page.gsub('<!-- navigation bar -->',navigation)
    html_page << RedCloth.new(navable_page).to_html
    print "Writing index.html... "
    write_page('index', html_page)
    puts "Done."

    # process the common page
    print "Processing: common.textile... "
    html_page = ""
    html_page << header
    # for this page, have to insert navigation bars...
    raw_page = get_raw_page('common')
    navigation = get_nav_bar('common','index',pages.first.dup.sub(/\.textile/,''))
    navable_page = raw_page.gsub('<!-- navigation bar -->',navigation)
    html_page << RedCloth.new(navable_page).to_html
    print "Writing common.html... "
    write_page('common', html_page)
    puts "Done."
    
    # now set up and do all other available pages
    prev_page = 'index'
    next_page = nil
    page_count = 3
    last_page = pages.size - 1
    
    (0..last_page).each do |p|
      print "Processing: #{pages[p]}... "
      page_count += 1
      page_name = pages[p].dup.sub(/\.textile$/,'')
      # get next page name if there is one
      if p == last_page
        next_page = nil
      else
        next_page = pages[p + 1].dup.sub(/\.textile$/,'')
      end
      # get source and add navigation bars
      raw_page = get_raw_page(page_name)
      navigation = get_nav_bar(page_name, prev_page, next_page)
      navable_page = raw_page.gsub('<!-- navigation bar -->', navigation)
      # generate html page
      html_page = ""
      html_page << header
      html_page << RedCloth.new(navable_page).to_html
      html_page << footer
      # write it
      print "Writing: #{page_name}.html... "
      write_page(page_name, html_page)
      # set prev_page to this page name
      prev_page = page_name
      # done
      puts "Done."
    end
    puts "Completed processing of #{page_count} pages."
  end
  
  private

  def self.header
    %(<!DOCTYPE html> 
<head>
  <title>FiveO Reference Manual</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <link href="manual.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class='title'>
  <img src='fiveo.png' style='width:85px;height:85px;float:left;'>
  <img src='fiveo.png' style='width:85px;height:85px;float:right;'>
  FiveO&#8482; Reference Manual</div>
)
  end
  
  def self.footer
    %(
</body>
</html>
)
  end

  # this takes a file name and looks up or generates the human readable version
  def self.get_proper_name(base_name)
    # special capitalization or punctuation needed for these names
    proper_names = {
      'index' => 'Introduction',
      'common' => 'Common Features',
      'doc' => "DOC",
      'fax_cover' => 'FAX Cover',
      'invest_crime' => 'Invest. Crime',
      'invest_criminal' => 'Invest. Criminal',
      'invest_photo' => 'Invest. Photo',
      'invest_report' => 'Invest. Report',
      'invest_vehicle' => 'Invest. Vehicle'
    }
    if proper_names[base_name].nil?
      # capitalization works normally
      human_name = base_name.gsub(/_/,' ').titleize
    else
      human_name = proper_names[base_name]
    end
    return human_name
  end

  # this takes a base file name (no extention) and reads in the raw contents
  # (from the textile/ directory)
  def self.get_raw_page(base_name)
    filename = "#{ManualTextilePath}/#{base_name}.textile"
    raw_file = File.read(filename)
    # strip from ###. to \n (used as comments in my document files
    raw_file.gsub(/###\..*\n/,'')
  end

  # this takes three page names and builds a navigation bar string
  def self.get_nav_bar(current_name, previous_name, next_name)
    # navigation bar with placeholders
    navbar = %(<div class='nav'><a href="toc.html">Index</a><span class='spacer'>&loz;</span>PREVIOUS<span class='spacer'>&larr;</span><span class='current'>CURRENT</span><span class='spacer'>&rarr;</span>NEXT</div>)
    # handle either end of the navigation line
    if previous_name.nil?
      previous_link = "The Beginning"
    else
      previous_link = "<a href='#{previous_name}.html'>#{get_proper_name(previous_name)}</a>"
    end
    if next_name.nil?
      next_link = "The End"
    else
      next_link = "<a href='#{next_name}.html'>#{get_proper_name(next_name)}</a>"
    end
    current_link = get_proper_name(current_name)
    navbar.dup.sub(/PREVIOUS/,previous_link).sub(/CURRENT/,current_link).sub(/NEXT/,next_link)
  end

  # this takes an html page (string) and writes it to a file in html/ directory
  def self.write_page(base_name, page_contents)
    filename = "#{ManualHtmlPath}/#{base_name}.html"
    f = File.new(filename, 'w')
    f.puts(page_contents)
    f.close
  end

end