FactoryGirl.define do
  factory :sentence do
    result "Pled Guilty"
    processed true
    court
    da_charge { FactoryGirl.create(:da_charge, docket: court.docket) }
  end
end
    