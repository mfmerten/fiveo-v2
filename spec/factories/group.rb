FactoryGirl.define do
  factory :group do
    sequence(:name, 10) {|n| "group#{n}"}
    description "test group"
  end
end