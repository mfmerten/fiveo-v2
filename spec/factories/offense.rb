FactoryGirl.define do
  factory :offense do
    officer "Merten"
    location "Ward 7"
    details "Test Data"
    offense_datetime { Time.now }
    call
  end
end