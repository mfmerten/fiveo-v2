FactoryGirl.define do
  factory :signal_code do
    sequence(:code, 100)
    name "Test Data"
  end
end