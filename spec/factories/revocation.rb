FactoryGirl.define do
  factory :revocation do
    judge "Shyster"
    revocation_date { Date.today }
    processed true
    docket
  end
end