FactoryGirl.define do
  factory :user do
    id nil
    sequence(:login, 1) { |n|  "uname#{n}" }
    keep_pass true
    association :contact, factory: :contact, sort_name: "Doe, John"
  end
end