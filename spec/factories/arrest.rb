FactoryGirl.define do
  factory :arrest do
    agency "TBPD"
    arrest_datetime { Time.now }
    person
  end
end
