FactoryGirl.define do
  factory :arrest_officer do
    officer "Doe, Jack"
    arrest
  end
end