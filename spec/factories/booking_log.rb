FactoryGirl.define do
  factory :booking_log do
    booking
    
    trait :started do
      start_datetime { Time.now }
    end
    
    trait :stopped do
      start_datetime { Time.now - 10.minutes }
      stop_datetime { Time.now }
    end
  end
end