FactoryGirl.define do
  factory :court do
    judge "Shyster"
    court_date { Date.today }
    docket
  end
end