FactoryGirl.define do
  factory :hold do
    hold_by "Merten"
    hold_datetime { Time.now }
    hold_type "48 Hour"
    booking
  end
end