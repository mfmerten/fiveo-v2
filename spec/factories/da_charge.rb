FactoryGirl.define do
  factory :da_charge do
    charge "Burglary"
    count 1
    docket
  end
end