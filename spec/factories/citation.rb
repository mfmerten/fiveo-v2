FactoryGirl.define do
  factory :citation do
    sequence(:ticket_no, 1000)
    citation_datetime { Time.now }
    officer "Merten"
    person
  end
end
    