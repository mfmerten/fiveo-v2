FactoryGirl.define do
  factory :bond do
    issued_datetime { Time.now }
    person
    surety factory: :person
  end
end