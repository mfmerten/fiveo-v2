FactoryGirl.define do
  factory :investigation do
    details "Test Investigation"
    investigation_datetime { Time.now }
  end
end