FactoryGirl.define do
  factory :jail do
    name 'Toledo Bend PD'
    acronym 'TBPD'
  
    trait :spso do
      name 'Sabine Parish SO'
      acronym 'SPSO'
    end
  
    trait :many do
      name 'Many PD'
      acronym 'MPD'
    end
  end
end