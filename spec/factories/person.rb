FactoryGirl.define do
  factory :person do
    family_name "Merten"
    given_name "Mike"
  end
end