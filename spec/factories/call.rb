FactoryGirl.define do
  factory :call do
    dispatcher "Merten"
    details "Test Data"
    call_datetime { Time.now }
    signal_code { create(:signal_code).code }
  end
end