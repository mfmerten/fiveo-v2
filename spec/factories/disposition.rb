FactoryGirl.define do
  factory :disposition do
    sequence(:atn, 1000)
    person
  end
end