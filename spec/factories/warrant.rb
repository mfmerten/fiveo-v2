FactoryGirl.define do
  factory :warrant do
    family_name "Doe"
    given_name "John"
    jurisdiction "Sabine Parish"
    received_date { Date.today }
    issued_date { Date.today }
    encrypted_ssn { SymmetricEncryption.encrypt('000-00-0000') }
  end
end