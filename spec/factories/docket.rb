FactoryGirl.define do
  factory :docket do
    sequence(:docket_no, 1000)
    person
    
    trait :with_court do
      court
    end
    
    trait :with_charge do
      association :da_charges, factory: :da_charge
    end
  end
end