FactoryGirl.define do
  factory :activity do
    section 'Activities'
    description 'Ran RSpec Tests'
    user
  end
end
    