FactoryGirl.define do
  factory :booking do
    booking_datetime { Time.now }
    booking_officer "Merten"
    cell "Holding"
    person
    jail
  end
end