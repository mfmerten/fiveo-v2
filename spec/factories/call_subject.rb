FactoryGirl.define do
  factory :call_subject do
    sort_name "John Doe"
    subject_type "Complainant"
    call
  end
end