require 'rails_helper'

RSpec.describe TimeRange do
  let(:t) { Time.now }
  let(:t1) { t - 12.hours }
  let(:t2) { t - 11.hours }
  let(:t3) { t - 10.hours }
  let(:t4) { t - 9.hours }
  let(:t5) { t - 8.hours }
  let(:t6) { t - 7.hours }
  let(:t7) { t - 6.hours }
  
  describe ".new" do
    it "should accept two Time values and return a TimeRange object" do
      expect(TimeRange.new(t2, t1)).to be_a(TimeRange)
    end
    
    it "should accept non-time values if they respond to 'to_time'" do
      expect(TimeRange.new("2015-01-01 12:30 PM", "1/1/2015 12:30")).to be_a(TimeRange)
    end
    
    it "should raise ArgumentError if either argument is not Time and does not respond to 'to_time'" do
      expect { TimeRange.new(42,nil) }.to raise_error(ArgumentError)
    end
  
    it "should swap times properly if presented in reverse order" do
      expect(TimeRange.new(t2, t1).start).to eq(t1)
    end
    
    it "should set 'dirty' attribute to true" do
      expect(TimeRange.new(t1,t2).dirty?).to be(true)
    end
  end
  
  describe "#start" do
    it "should return the start value" do
      expect(TimeRange.new(t1,t2).start).to eq(t1)
    end
  end
  
  describe "#start=" do
    it "should accept a Time value and update start time" do
      tr = TimeRange.new(t1,t3)
      expect(tr.start).to eq(t1)
      tr.start = t2
      expect(tr.start).to eq(t2)
    end
    
    it "should set 'dirty' attribute to true if value changes" do
      tr = TimeRange.new(t1,t3)
      tr.dirty = false
      expect(tr.dirty?).to be(false)
      tr.start = t2
      expect(tr.dirty?).to be(true)
    end
    
    it "should not set 'dirty' attribute to true if value does not change" do
      tr = TimeRange.new(t1,t3)
      tr.dirty = false
      expect(tr.dirty?).to be(false)
      tr.start = t1
      expect(tr.dirty?).to be(false)
    end
    
    it "should raise ArgumentError if argument is not Time and does not respond to 'to_time'" do
      tr = TimeRange.new(t1,t3)
      expect { tr.start = 42 }.to raise_error(ArgumentError)
    end
  end
  
  describe "#stop" do
    it "should return the stop value" do
      expect(TimeRange.new(t1,t2).stop).to eq(t2)
    end
  end
  
  describe "#stop=" do
    it "should accept a Time value and update stop time" do
      tr = TimeRange.new(t1,t3)
      expect(tr.stop).to eq(t3)
      tr.stop = t2
      expect(tr.stop).to eq(t2)
    end
    
    it "should set 'dirty' attribute to true if value changes" do
      tr = TimeRange.new(t1,t3)
      tr.dirty = false
      expect(tr.dirty?).to be(false)
      tr.stop = t2
      expect(tr.dirty?).to be(true)
    end
    
    it "should not set 'dirty' attribute to true if value does not change" do
      tr = TimeRange.new(t1,t3)
      tr.dirty = false
      expect(tr.dirty?).to be(false)
      tr.stop = t3
      expect(tr.dirty?).to be(false)
    end
    
    it "should raise ArgumentError if argument is not Time and does not respond to 'to_time'" do
      tr = TimeRange.new(t1,t3)
      expect { tr.stop = 42 }.to raise_error(ArgumentError)
    end
  end
  
  describe "#dirty?" do
    it "should return true/false state of dirty attribute" do
      tr = TimeRange.new(t1,t2)
      expect(tr.dirty?).to be(true)
      tr.dirty = false
      expect(tr.dirty?).to be(false)
    end
  end
  
  describe "#changed?" do
    it "should be an alias for dirty?" do
      tr = TimeRange.new(t1,t2)
      expect(tr.changed?).to be(tr.dirty?)
      tr.dirty = false
      expect(tr.changed?).to be(tr.dirty?)
    end
  end
  
  describe "#dirty=" do
    it "should set dirty attribute to boolean equivalent of argument" do
      tr = TimeRange.new(t1,t2)
      expect(tr.dirty?).to be(true)
      tr.dirty = false
      expect(tr.dirty?).to be(false)
      tr.dirty = ""
      expect(tr.dirty?).to be(true)
      tr.dirty = 42
      expect(tr.dirty?).to be(true)
      tr.dirty = nil
      expect(tr.dirty?).to be(false)
    end
  end
  
  describe "#hours" do
    it "should calculate hours covered by TimeRange object" do
      expect(TimeRange.new(t1, t7).hours).to eq(6)
    end
  end
  
  describe "#==" do
    it "should return true if timeranges start/stop times match" do
      expect(TimeRange.new(t1,t2) == TimeRange.new(t1,t2)).to be(true)
    end
    
    it "should return false if timeranges start/stop times do not match" do
      expect(TimeRange.new(t1,t2) == TimeRange.new(t1,t3)).to be(false)
      expect(TimeRange.new(t1,t3) == TimeRange.new(t2,t3)).to be(false)
    end
    
    it "should raise ArgumentError if object to compare is not a TimeRange" do
      expect { TimeRange.new(t1,t2) == 42 }.to raise_error(ArgumentError)
    end
  end
  
  describe "#overlaps?" do
    it "should raise ArgumentError if object to compare is not a TimeRange" do
      expect { TimeRange.new(t1,t2).overlaps?(42) }.to raise_error(ArgumentError)
    end
    
    it "should return false if timeranges do not overlap" do
      expect(TimeRange.new(t1,t2).overlaps?(TimeRange.new(t3,t4))).to be(false)
    end
  
    it "should return true if timeranges overlap" do
      expect(TimeRange.new(t2,t4).overlaps?(TimeRange.new(t1,t3))).to be(true)
      expect(TimeRange.new(t2,t4).overlaps?(TimeRange.new(t3,t5))).to be(true)
      expect(TimeRange.new(t1,t4).overlaps?(TimeRange.new(t2,t3))).to be(true)
      expect(TimeRange.new(t2,t3).overlaps?(TimeRange.new(t1,t4))).to be(true)
    end
  end
  
  describe "#covers?" do
    it "should raise ArgumentError if object to compare not a Time and does not respond to to_time" do
      expect { TimeRange.new(t1,t2).covers?(42) }.to raise_error(ArgumentError)
    end
    
    it "should return false if timerange does not cover time" do
      expect(TimeRange.new(t1,t2).covers?(t3)).to be(false)
    end
    
    it "should return true if timerange covers time (inclusive)" do
      expect(TimeRange.new(t1,t3).covers?(t2)).to be(true)
      expect(TimeRange.new(t1,t3).covers?(t1)).to be(true)
      expect(TimeRange.new(t1,t3).covers?(t3)).to be(true)
    end
  end
  
  describe "#<=>" do
    it "should raise ArgumentError if object being compared is not a TimeRange" do
      expect { [TimeRange.new(t1,t2), 42].sort }.to raise_error(ArgumentError)
    end
    
    it "should return 0 if argument is equal to self" do
      tr1 = TimeRange.new(t2,t3)
      tr2 = TimeRange.new(t2,t3)
      expect(tr1 <=> tr2).to eq(0)
    end
    
    it "should return -1 if argument is greater than self" do
      tr1 = TimeRange.new(t1,t3)
      tr2 = TimeRange.new(t2,t3)
      tr3 = TimeRange.new(t1,t4)
      expect(tr1 <=> tr2).to eq(-1)
      expect(tr1 <=> tr3).to eq(-1)
    end
    
    it "should return 1 if argument is less than self" do
      tr1 = TimeRange.new(t2,t4)
      tr2 = TimeRange.new(t1,t4)
      tr3 = TimeRange.new(t2,t3)
      expect(tr1 <=> tr2).to eq(1)
      expect(tr1 <=> tr3).to eq(1)
    end
  end
  
  describe "#merge_if_overlap" do
    it "should raise ArgumentError if object to compare is not a TimeRange" do
      expect { TimeRange.new(t1,t2).merge_if_overlap(42) }.to raise_error(ArgumentError)
    end
    
    it "should return both ranges if ranges do not overlap" do
      tr1 = TimeRange.new(t1,t2)
      tr2 = TimeRange.new(t3,t4)
      expect(tr1.merge_if_overlap(tr2)).to eq([tr1,tr2])
    end
    
    it "should expand range to cover overlap if ranges overlap (left)" do
      tr1 = TimeRange.new(t2,t3)
      tr2 = TimeRange.new(t1,t2)
      merged = TimeRange.new(t1,t3)
      tr1.dirty = false
      expect(tr1.merge_if_overlap(tr2)).to eq([merged])
      expect(tr1.dirty?).to be(true)
    end
    
    it "should expand range to cover overlap if ranges overlap (right)" do
      tr1 = TimeRange.new(t1,t2)
      tr2 = TimeRange.new(t2,t3)
      merged = TimeRange.new(t1,t3)
      tr1.dirty = false
      expect(tr1.merge_if_overlap(tr2)).to eq([merged])
      expect(tr1.dirty?).to be(true)
    end
    
    it "should expand range to cover overlap if argument overlaps fully (covers)" do
      tr1 = TimeRange.new(t2,t3)
      tr2 = TimeRange.new(t1,t4)
      merged = TimeRange.new(t1,t4)
      tr1.dirty = false
      expect(tr1.merge_if_overlap(tr2)).to eq([merged])
      expect(tr1.dirty?).to be(true)
    end
    
    it "should not expand range if argument is fully overlapped (covered)" do
      tr1 = TimeRange.new(t1,t4)
      tr2 = TimeRange.new(t2,t3)
      merged = TimeRange.new(t1,t4)
      tr1.dirty = false
      expect(tr1.merge_if_overlap(tr2)).to eq([merged])
      expect(tr1.dirty?).to be(false)
    end
  end
  
  describe "#exclude_if_overlap" do
    it "should raise ArgumentError if object to compare is not a TimeRange" do
      expect { TimeRange.new(t1,t2).exclude_if_overlap(42) }.to raise_error(ArgumentError)
    end
    
    it "should return self if ranges do not overlap" do
      tr1 = TimeRange.new(t1,t2)
      tr2 = TimeRange.new(t3,t4)
      excluded = TimeRange.new(t1,t2)
      expect(tr1.exclude_if_overlap(tr2)).to eq([excluded])
    end
    
    it "should contract range to exclude overlap if ranges overlap (left)" do
      tr1 = TimeRange.new(t2,t4)
      tr2 = TimeRange.new(t1,t3)
      excluded = TimeRange.new(t3,t4)
      tr1.dirty = false
      expect(tr1.exclude_if_overlap(tr2)).to eq([excluded])
      expect(tr1.dirty?).to be(true)
    end
    
    it "should contract range to exclude overlap if ranges overlap (right)" do
      tr1 = TimeRange.new(t1,t3)
      tr2 = TimeRange.new(t2,t4)
      excluded = TimeRange.new(t1,t2)
      tr1.dirty = false
      expect(tr1.exclude_if_overlap(tr2)).to eq([excluded])
      expect(tr1.dirty?).to be(true)
    end
    
    it "should return multiple ranges if initial range completely covers compare range" do
      tr1 = TimeRange.new(t1,t4)
      tr2 = TimeRange.new(t2,t3)
      excluded1 = TimeRange.new(t1,t2)
      excluded2 = TimeRange.new(t3,t4)
      tr1.dirty = false
      expect(tr1.exclude_if_overlap(tr2)).to eq([excluded1, excluded2])
      expect(tr1.dirty?).to be(true)
    end
    
    it "should handle multiple exclusion ranges" do
      tr1 = TimeRange.new(t1,t6)
      tr2 = TimeRange.new(t5,t7)
      tr3 = TimeRange.new(t2,t3)
      tr4 = TimeRange.new(t3,t4)
      excluded1 = TimeRange.new(t1,t2)
      excluded2 = TimeRange.new(t4,t5)
      tr1.dirty = false
      expect(tr1.exclude_if_overlap(tr2,tr3,tr4)).to eq([excluded1, excluded2])
      expect(tr1.dirty?).to be(true)
    end
  end
end  
