require 'rails_helper'

RSpec.describe TimeServed do
  let(:t) { Time.now }
  let(:t0) { nil }
  let(:t1) { t - 12.hours }
  let(:t2) { t - 11.hours }
  let(:t3) { t - 10.hours }
  let(:t4) { t - 9.hours }
  let(:t5) { t - 8.hours }
  let(:t6) { t - 7.hours }
  let(:t7) { t - 6.hours }
  
  describe ".new" do
    it "should accept two non-overlapping time ranges" do
      range1 = [t1,t2]
      range2 = [t3,t4]
      tr1 = TimeRange.new(t1,t2)
      tr1.dirty = false
      tr2 = TimeRange.new(t3,t4)
      tr2.dirty = false
      served = TimeServed.new(range1, range2)
      expect(served.ranges).to match_array([tr1, tr2])
    end
    
    it "should accept two overlapping time ranges and merge them into 1" do
      range1 = [t1,t3]
      range2 = [t2,t4]
      served = TimeServed.new(range1, range2)
      merged = TimeRange.new(t1,t4)
      merged.dirty = false
      expect(served.ranges).to match_array([merged])
    end
  
    it "should accept multiple dimension arrays of time ranges" do
      range1 = [t1,t3]
      range2 = [t2,t4] # overlapping
      range3 = [t5,t7] # nonoverlapping
      range4 = [t6,t7] # covered
      merged1 = TimeRange.new(t1,t4)
      merged1.dirty = false
      merged2 = TimeRange.new(t5,t7)
      merged2.dirty = false
      served = TimeServed.new([range1, [[range2, range3], range4]])
      expect(served.ranges).to match_array([merged1, merged2])
    end
    
    it "should not add ranges where stop and/or start are nil" do
      range1 = [nil,nil]
      range2 = [t1,t5]
      merged = TimeRange.new(t1,t5)
      merged.dirty = false
      served = TimeServed.new([range1, range2])
      expect(served.ranges).to match_array([merged])
    end
    
    it "should convert single nils to :nil_value if given" do
      range1 = [nil,nil]  # skipped
      range2 = [t1,nil]   # becomes [t1,t6]
      range3 = [t5,t7]
      merged = TimeRange.new(t1,t7)
      merged.dirty = false
      served = TimeServed.new([range1, range2, range3], nil_value: t6)
      expect(served.ranges).to match_array([merged])
    end
    
    it "should update the options hash if given a Hash" do
      served = TimeServed.new
      expect(served.options(:nil_value)).to be_nil
      served = TimeServed.new(nil_value: t6)
      expect(served.options(:nil_value)).to eq(t6)
    end
  end
  
  describe "#hours" do
    it "should return total number of hours" do
      range1 = [t4,nil]
      range2 = [t1,t5]
      served = TimeServed.new([range1, range2], nil_value: t6)
      expect(served.hours).to eq(5)
    end
  end
  
  describe "#add_ranges" do
    it "should accept two overlapping time ranges and merge them into 1" do
      range1 = [t1,t3]
      range2 = [t2,t4]
      served = TimeServed.new
      expect(served.ranges.size).to eq(0)
      served.add_ranges(range1, range2)
      expect(served.ranges.size).to eq(1)
    end
    
    it "should accept two overlapping time ranges and return the total hours - overlap" do
      range1 = [t1,t3]
      range2 = [t2,t4]
      served = TimeServed.new
      expect(served.hours).to eq(0)
      served.add_ranges(range1, range2)
      expect(served.hours).to eq(3)
    end
  
    it "should accept multiple dimension arrays of time ranges" do
      range1 = [t1,t3]
      range2 = [t2,t4] # overlapping
      range3 = [t5,t7] # nonoverlapping
      range4 = [t6,t7] # covered
      range5 = [t4,t5] # double overlap
      merged = TimeRange.new(t1,t7)
      merged.dirty = false
      served = TimeServed.new
      expect(served.hours).to eq(0)
      served.add_ranges([range1, [[range2, range3], range4]], range5)
      expect(served.ranges).to eq([merged])
      expect(served.hours).to eq(6)
    end
  end
  
  describe "#ranges" do
    it "should return array of TimeRange objects" do
      range1 = [t1,t3]
      range2 = [t2,t4]
      served = TimeServed.new
      merged = TimeRange.new(t1,t4)
      merged.dirty = false
      served.add_ranges(range1, range2)
      expect(served.ranges).to eq([merged])
    end
  end
  
  describe "#add_excludes" do
    it "should accept two overlapping time ranges and merge them into 1" do
      range1 = [t1,t3]
      range2 = [t2,t4]
      served = TimeServed.new
      expect(served.excludes.size).to eq(0)
      served.add_excludes(range1, range2)
      expect(served.excludes.size).to eq(1)
    end
    
    it "should accept one time range and one exclude range and return the total hours - exclude" do
      range1 = [t1,t3]
      range2 = [t2,t4]
      served = TimeServed.new
      expect(served.hours).to eq(0)
      served.add_ranges(range1)
      expect(served.hours).to eq(2)
      served.add_excludes(range2)
      expect(served.hours).to eq(1)
    end
  
    it "should accept multiple dimension arrays of time ranges" do
      range1 = [t1,t3]
      range2 = [t2,t4] # overlapping
      range3 = [t5,t7] # nonoverlapping
      range4 = [t6,t7] # covered
      range5 = [t4,t5] # double overlap
      served = TimeServed.new
      served.add_excludes([range1, [[range2, range3], range4]], range5)
      expect(served.excludes.size).to eq(1)
    end
  end
  
  describe "#excludes" do
    it "should return array of TimeRange objects" do
      range1 = [t1,t3]
      range2 = [t2,t4]
      served = TimeServed.new
      merged = TimeRange.new(t1,t4)
      merged.dirty = false
      served.add_excludes(range1, range2)
      expect(served.excludes).to eq([merged])
    end
  end
  
  describe "#options" do
    it "should return the options hash if called with no arguments" do
      expect(TimeServed.new.options).to be_a(Hash)
    end
    
    it "should return an option value if called with a key" do
      expect(TimeServed.new(nil_value: t1).options(:nil_value)).to eq(t1)
      expect(TimeServed.new(nil_value: t1).options('nil_value')).to eq(t1)
    end
  end
end
