require 'rails_helper'

RSpec.describe AppUtils do
  
  def self.invalid_model
    {
      '' => 'blank value',
      nil => 'nil value',
      'InvalidModel' => 'invalid model string',
      :invalid_model => 'invalid model symbol',
      Date => 'class that is not ActiveRecord Model'
    }
  end
  
  def self.invalid_fields
    {
      nil => 'nil value',
      [nil] => 'array with only nil elements',
      "" => 'blank value',
      [""] => 'array with only blank elements',
      :no_column => 'non-existent column'
    }
  end
  
  def self.invalid_field
    {
      "" => 'blank value',
      nil => 'nil value',
      :no_column => 'non-existent column'
    }
  end
  
  describe ".purge_pdf_tmp" do
    before(:context) do
      # create some bogus pdf files with older mtimes
      file_path = Rails.root.join('public', 'files')
      @test1_file = File.join(file_path, 'rspec_test1.pdf')
      @test2_file = File.join(file_path, 'rspec_test2.pdf')
    
      FileUtils.mkdir_p(file_path) unless File.directory?(file_path)
      FileUtils.touch(@test1_file, mtime: Time.now - 2.hours)
      FileUtils.touch(@test2_file, mtime: Time.now - 30.minutes)
    end
    
    it "should have created test files" do
      expect(File.exist?(@test1_file)).to be(true)
      expect(File.exist?(@test2_file)).to be(true)
    end
    
    it "should remove test1_file but not test2_file using default maximum_age" do
      AppUtils.purge_pdf_tmp
      expect(File.exist?(@test1_file)).to be(false)
      expect(File.exist?(@test2_file)).to be(true)
    end
    
    it "should remove test2_file using 20.minutes.ago maximum age" do
      AppUtils.purge_pdf_tmp(20.minutes.ago)
      expect(File.exist?(@test2_file)).to be_falsy
    end
  end
  
  describe ".for_lower" do
    let!(:c1) { create(:contact, sort_name: 'Contact, RSpec', title: 'PrOgRaMmEr') }
    let!(:c2) { create(:contact, sort_name: 'Contact, Luser', title: 'Not Programmer') }
    let!(:u1) { create(:user, login: 'rspeclogin', contact: c1) }
    let!(:u2) { create(:user, login: 'luserlogin', contact: c2) }
    
    invalid_model.each do |value,msg|
      it "should raise exception if model_or_string is #{msg}" do
        expect {AppUtils.for_lower(value, :sort_name, "Merten")}.to raise_error(ArgumentError)
      end
    end
    
    invalid_fields.each do |value,msg|
      it "should raise exception if field_symbols is #{msg}" do
        expect {AppUtils.for_lower(Contact, value, "Merten")}.to raise_error(ArgumentError)
      end
    end
    
    it "should not raise exception if arguments are the correct type" do
      expect {AppUtils.for_lower(Contact, :sort_name, "Merten")}.not_to raise_error
    end
    
    it "should return ActiveRecord::Relation if arguments are correct" do
      expect(AppUtils.for_lower(Contact, :sort_name, "Merten")).to be_a(ActiveRecord::Relation)
    end
    
    it "should match lower case title for Contact 1 and not match Contact 2" do
      query = AppUtils.for_lower(Contact, :title, "programmer").pluck(:id)
      expect(query).to include(c1.id)
      expect(query).not_to include(c2.id)
    end
    
    it "should match lower case title for User 1 and not match User 2 with contact association" do
      query = AppUtils.for_lower(User, :title, "programmer", association: :contact).pluck(:id)
      expect(query).to include(u1.id)
      expect(query).not_to include(u2.id)
    end
    
    it "should return empty ActiveRecord::Relation if no matches" do
      expect(AppUtils.for_lower(Contact, :title, 'cannot_be_matched')).to match_array([])
    end
  end
  
  describe ".for_null" do
    let!(:p1) { create(:person, family_name: "Merten", given_name: "Michael", date_of_birth: Date.today - 40.years) }
    let!(:p2) { create(:person, family_name: "Merten", given_name: "Rebecca") }
    let!(:c1) { create(:citation, ticket_no: "1234", person: p2) }
    let!(:c2) { create(:citation, ticket_no: '4321', person: p1) }
    
    invalid_model.each do |value,msg|
      it "should raise exception if model_or_string is #{msg}" do
        expect {AppUtils.for_null(value, :date_of_birth, true)}.to raise_error(ArgumentError)
      end
    end
    
    invalid_field.each do |value, msg|
      it "should raise exception if field_symbol is #{msg}" do
        expect {AppUtils.for_null(Person, value, true)}.to raise_error(ArgumentError)
      end
    end
    
    it "should not raise exception if arguments are correct type" do
      expect {AppUtils.for_null(Person, :date_of_birth, true)}.not_to raise_error
    end
    
    it "should match person 1 and not person 2 when argument is true" do
      query = AppUtils.for_null(Person, :date_of_birth, true).pluck(:id)
      expect(query).to include(p1.id)
      expect(query).not_to include(p2.id)
    end
    
    it "should match citation 1 and not citation 2 when argument is false and person association is used" do
      query = AppUtils.for_null(Citation, :date_of_birth, false, association: :person).pluck(:id)
      expect(query).to include(c1.id)
      expect(query).not_to include(c2.id)
    end
  end
  
  describe ".for_date" do
    let(:d1) { AppUtils.valid_date("1960-09-18") }
    let(:d2) { AppUtils.valid_date("1980-01-01") }
    let!(:p1) { create(:person, date_of_birth: d1) }
    let!(:p2) { create(:person, date_of_birth: d2) }
    let!(:c1) { create(:citation, person: p1) }
    let!(:c2) { create(:citation, person: p2) }

    invalid_model.each do |value, msg|
      it "should raise exception if model_or_string is #{msg}" do
        expect {AppUtils.for_date(value, :date_of_birth, "1960-09-18")}.to raise_error(ArgumentError)
      end
    end
    
    invalid_field.each do |value,msg|
      it "should raise exception if field_symbol is #{msg}" do
        expect {AppUtils.for_date(Person, value, "1960-09-18")}.to raise_error(ArgumentError)
      end
    end
    
    it "should not raise exception if given correct argument types" do
      expect {AppUtils.for_date(Person, :date_of_birth, "1960-09-18")}.not_to raise_error
    end
    
    it "should return empty ActiveRecord::Relation if datestring is not valid" do
      query = AppUtils.for_date(Person, :date_of_birth, "1960-19-18")
      expect(query).to be_a(ActiveRecord::Relation).and match_array([])
    end
    
    it "should match person 1 and not person 2 with date_of_birth == #{@d1}" do
      query = AppUtils.for_date(Person, :date_of_birth, d1.to_s(:db)).pluck(:id)
      expect(query).to include(p1.id)
      expect(query).not_to include(p2.id)
    end
    
    it "should match citation 2 and not citation 1 with date_of_birth == #{@d2} with person association" do
      query = AppUtils.for_date(Citation, :date_of_birth, d2.to_s(:db), association: :person).pluck(:id)
      expect(query).to include(c2.id)
      expect(query).not_to include(c1.id)
    end
    
    # handles Time/DateTime using the for_daterange() tested separately
    it "should call for_daterange() once if datetime option is true" do
      allow(AppUtils).to receive(:for_daterange).and_return(nil)
      AppUtils.for_date(Citation, :citation_datetime, Time.now.to_s(:db), datetime: true)
      expect(AppUtils).to have_received(:for_daterange)
    end
  end
  
  describe ".for_person" do
    let!(:p1) { create(:person, family_name: "Merten", given_name: "Michael") }
    let!(:p2) { create(:person, family_name: "Merten", given_name: "Rebecca") }
    let!(:c1) { create(:citation, officer: "John Doe", person: p1) }
    let!(:c2) { create(:citation, officer: "Jane Doe Smith", person: p2) }
    
    invalid_model.each do |value, msg|
      it "should raise exception if model_or_string is #{msg}" do
        expect {AppUtils.for_person(value, "Michael Merten")}.to raise_error(ArgumentError)
      end
    end
    
    invalid_field.each do |value, msg|
      it "should raise exception if simple: true and field: is #{msg}" do
        expect {AppUtils.for_person(Citation, "John Doe", simple: true, field: value)}.to raise_error(ArgumentError)
      end 
    end
    
    it "should return empty relation if name is blank" do
      expect(AppUtils.for_person(Person, "")).to be_a(ActiveRecord::Relation).and match_array([])
    end
    
    it "should match person 1 and not person 2 with 'Michael Merten' as name" do
      query = AppUtils.for_person(Person, "Michael Merten").pluck(:id)
      expect(query).to include(p1.id)
      expect(query).not_to include(p2.id)
    end
    
    it "should match citation 1 and not citation 2 with 'Michael Merten' as name using person association" do
      query = AppUtils.for_person(Citation, "Michael Merten", association: :person).pluck(:id)
      expect(query).to include(c1.id)
      expect(query).not_to include(c2.id)
    end
    
    it "should match citation 1 and not citation 2 with 'John Doe' as officer using simple field match" do
      query = AppUtils.for_person(Citation, "John Doe", simple: true, field: :officer).pluck(:id)
      expect(query).to include(c1.id)
      expect(query).not_to include(c2.id)
    end
  end
  
  describe ".for_officer" do
    let!(:p1) { create(:person) }
    let!(:p2) { create(:person) }
    let!(:c1) { create(:citation, officer: "Michael Merten", officer_unit: "MTS1", officer_badge: "1234", person: p1) }
    let!(:c2) { create(:citation, officer: "Jane Doe Smith", officer_unit: "MTS2", officer_badge: "4321", person: p2) }
    
    invalid_model.each do |value, msg|
      it "should raise exception if model_or_string is #{msg}" do
        expect {AppUtils.for_officer(value, :officer, "Michael Merten", "MTS1", "1234")}.to raise_error(ArgumentError)
      end
    end
    
    invalid_fields.each do |value,msg|
      it "should raise exception if field_symbols is #{msg}" do
        expect {AppUtils.for_officer(Citation, value, "Michael Merten", "MTS1", "1234")}.to raise_error(ArgumentError)
      end
    end
    
    it "should return empty relation if name is blank" do
      expect(AppUtils.for_officer(Citation, :officer, "", "", "")).to be_a(ActiveRecord::Relation).and match_array([])
    end
    
    it "should not raise exception if arguments are of correct type" do
      expect {AppUtils.for_officer(Citation, :officer, "Michael Merten", "MTS1", "1234")}.not_to raise_error
    end
    
    it "should match Citation 1 and not Citation 2 on officer string" do
      query = AppUtils.for_officer(Citation, :officer, "Michael Merten", "", "").pluck(:id)
      expect(query).to include(c1.id)
      expect(query).not_to include(c2.id)
    end
    
    it "should match Citation 2 and not Citation 1 on unit string" do
      query = AppUtils.for_officer(Citation, :officer, "", "MTS2", "").pluck(:id)
      expect(query).to include(c2.id)
      expect(query).not_to include(c1.id)
    end
    
    it "should match Person 1 and not Person 2 on badge string using citations association" do
      query = AppUtils.for_officer(Person, :officer, "", "", "1234", association: :citations).pluck(:id)
      expect(query).to include(p1.id)
      expect(query).not_to include(p2.id)
    end
  end
  
  describe ".for_daterange" do
    let!(:p1) { create(:person, date_of_birth: AppUtils.valid_date("1960-09-18")) }
    let!(:p2) { create(:person, date_of_birth: AppUtils.valid_date("1980-01-01")) }
    let!(:c1) { create(:citation, citation_datetime: Time.parse("2015-05-15 12:00"), person: p1) }
    let!(:c2) { create(:citation, citation_datetime: Time.parse("2015-04-15 12:00"), person: p2) }
    
    invalid_model.each do |value, msg|
      it "should raise exception if model_or_string is #{msg}" do
        expect {AppUtils.for_daterange(value, :citation_datetime, start: Date.today, stop: Date.today)}.to raise_error(ArgumentError)
      end
    end
    
    invalid_model.each do |value, msg|
      it "should raise exception if simple: true and field: is #{msg}" do
        expect {AppUtils.for_daterange(Citation, value, start: Date.today, stop: Date.today)}.to raise_error(ArgumentError)
      end 
    end
    
    it "should return empty relation if start and stop are both blank" do
      expect(AppUtils.for_daterange(Citation, :citation_datetime, start: nil, stop: nil)).to be_a(ActiveRecord::Relation).and match_array([])
    end
    
    it "should not raise exception if arguments are of correct type" do
      expect {AppUtils.for_daterange(Citation, :citation_datetime, start: Date.today, stop: Date.today)}.not_to raise_error
    end
    
    it "should match citation 1 but not citation 2 with start = '2015-05-01' and stop = '2015-06-01'" do
      query = AppUtils.for_daterange(Citation, :citation_datetime, start: '2015-05-01', stop: '2015-06-01').pluck(:id)
      expect(query).to include(c1.id)
      expect(query).not_to include(c2.id)
    end
    
    it "should match person 2 but not person 1 with start = '2015-04-01' and stop = '2015-05-01' with citation association" do
      query = AppUtils.for_daterange(Person, :citation_datetime, start: '2015-04-01', stop: '2015-05-01', association: :citations).pluck(:id)
      expect(query).to include(p2.id)
      expect(query).not_to include(p1.id)
    end
    
    it "should match person 1 but not person 2 with start = '1960-09-01' and stop = '1960-09-30' with date: true" do
      query = AppUtils.for_daterange(Person, :date_of_birth, start: "1960-09-01", stop: "1960-09-30", date: true).pluck(:id)
      expect(query).to include(p1.id)
      expect(query).not_to include(p2.id)
    end
    
    it "should match citation 1 but not citation 2 with start = '2015-05-01' and stop = nil" do
      query = AppUtils.for_daterange(Citation, :citation_datetime, start: '2015-05-01').pluck(:id)
      expect(query).to include(c1.id)
      expect(query).not_to include(c2.id)
    end
    
    it "should match citation 2 but not citation 1 with start = nil and stop = '2015-05-01'" do
      query = AppUtils.for_daterange(Citation, :citation_datetime, stop: '2015-05-01').pluck(:id)
      expect(query).to include(c2.id)
      expect(query).not_to include(c1.id)
    end
  end
  
  describe ".for_bool" do
    let!(:p1) { create(:person, glasses: true) }
    let!(:p2) { create(:person) }
    let!(:c1) { create(:citation, person: p1) }
    let!(:c2) { create(:citation, person: p2) }
    
    invalid_model.each do |value, msg|
      it "should raise exception if model_or_string is #{msg}" do
        expect {AppUtils.for_bool(value, :glasses, true)}.to raise_error(ArgumentError)
      end
    end
    
    invalid_field.each do |value,msg|
      it "should raise exception if field_symbol is #{msg}" do
        expect {AppUtils.for_bool(Person, value, true)}.to raise_error(ArgumentError)
      end
    end
    
    it "should return empty relation if value is blank" do
      expect(AppUtils.for_bool(Person, :glasses, nil)).to be_a(ActiveRecord::Relation).and match_array([])
    end
    
    it "should not raise exception if arguments are of correct type" do
      expect {AppUtils.for_bool(Person, :glasses, true)}.not_to raise_error
    end
    
    it "should match person 2 but not person 1 on 'glasses' when value is false" do
      query = AppUtils.for_bool(Person, :glasses, false).pluck(:id)
      expect(query).to include(p2.id)
      expect(query).not_to include(p1.id)
    end
    
    it "should match citation 1 but not citation 2 on 'glasses' when value is true using person association" do
      query = AppUtils.for_bool(Citation, :glasses, true, association: :person).pluck(:id)
      expect(query).to include(c1.id)
      expect(query).not_to include(c2.id)
    end
  end
  
  describe ".like" do
    let!(:p1) { create(:person, family_name: "Merten", given_name: "Michael") }
    let!(:p2) { create(:person, family_name: "Ellison", given_name: "Rebecca") }
    let!(:c1) { create(:citation, person: p1) }
    let!(:c2) { create(:citation, person: p2) }
    
    invalid_model.each do |value, msg|
      it "should raise exception if model_or_string is #{msg}" do
        expect {AppUtils.like(value, :given_name, "ael")}.to raise_error(ArgumentError)
      end
    end
    
    invalid_fields.each do |value,msg|
      it "should raise exception if field_symbols is #{msg}" do
        expect {AppUtils.like(Person, value, 'ael')}.to raise_error(ArgumentError)
      end
    end
    
    it "should return empty relation if value is blank" do
      expect(AppUtils.like(Person, :given_name, '')).to be_a(ActiveRecord::Relation).and match_array([])
    end
    
    it "should not raise exception if arguments are of correct type" do
      expect {AppUtils.like(Person, :given_name, 'ael')}.not_to raise_error
    end
    
    it "should match person 1 but not person 2 on given_name with 'ael'" do
      query = AppUtils.like(Person, :given_name, 'ael').pluck(:id)
      expect(query).to include(p1.id)
      expect(query).not_to include(p2.id)
    end
    
    it "should match citation 2 but not citation 1 in person.given_name with 'reb'" do
      query = AppUtils.like(Citation, :given_name, 'reb', association: :person).pluck(:id)
      expect(query).to include(c2.id)
      expect(query).not_to include(c1.id)
    end
    
    it "should match both people with 'el' in [given_name, family_name] combination" do
      query = AppUtils.like(Person, [:given_name, :family_name], 'el').pluck(:id)
      expect(query).to include(p1.id).and include(p2.id)
    end
    
    it "should not match either person with 'xyz' in given_name" do
      query = AppUtils.like(Person, :given_name, 'xyz').pluck(:id)
      expect(query).not_to include(p1.id)
      expect(query).not_to include(p2.id)
    end
  end
  
  describe ".parse_name" do
    it "should return 'Merten, Michael F.', given 'Michael F. Merten'" do
      expect(AppUtils.parse_name('Michael F. Merten')).to eq("Merten, Michael F.")
    end
    
    it "should return 'Michael F. Merten', given 'Merten, Michael F.', true" do
      expect(AppUtils.parse_name('Merten, Michael F.',true)).to eq("Michael F. Merten")
    end
    
    it "should return empty string if given empty string" do
      expect(AppUtils.parse_name('')).to eq('')
    end
    
    it "should return empty string if given non-string value" do
      expect(AppUtils.parse_name(Date)).to eq('')
    end
  end
  
  describe ".show_person" do
    let!(:p1) { create(:person, family_name: "Merten", given_name: "Michael") }
    
    it "should return '[id] sort_name' from person when given person object" do
      expect(AppUtils.show_person(p1)).to eq("[#{p1.id}] #{p1.sort_name}")
    end
    
    it "should return '[id] sort_name' from person when given person id" do
      expect(AppUtils.show_person(p1.id)).to eq("[#{p1.id}] #{p1.sort_name}")
    end
    
    it "should return 'sort_name' from person if second argument is false" do
      expect(AppUtils.show_person(p1,false)).to eq(p1.sort_name)
    end
    
    it "should return 'Unknown' for invalid person id" do
      expect(AppUtils.show_person(Person.last.id + 1)).to eq("Unknown")
    end
    
    it "should return 'Unknown' if given object other than Integer or Person" do
      expect(AppUtils.show_person(Date.today)).to eq("Unknown")
    end
  end
  
  describe ".show_contact" do
    let!(:c2) { create(:contact, sort_name: 'Merten, Michael', unit: 'MTS1', badge_no: '1234') }
    let!(:u1) { create(:user, contact: c2) }
    let!(:c1) { create(:citation, officer: "Merten, Michael", officer_unit: 'MTS1', officer_badge: '1234') }
    
    it "should return 'MTS1 Merten, Michael' when given the test user" do
      expect(AppUtils.show_contact(u1)).to eq("MTS1 Merten, Michael")
    end
    
    it "should return 'MTS1 Merten, Michael (1234)' when given the test user and option badge: true" do
      expect(AppUtils.show_contact(u1, badge: true)).to eq("MTS1 Merten, Michael (1234)")
    end
    
    it "should return 'MTS1 Merten, Michael' when given the test contact" do
      expect(AppUtils.show_contact(c2)).to eq("MTS1 Merten, Michael")
    end
    
    it "should return 'MTS1 Merten, Michael' when given the test citation with option field: :officer" do
      expect(AppUtils.show_contact(c1, field: :officer)).to eq("MTS1 Merten, Michael")
    end
    
    it "should return 'Unknown' when given the test citation without field option" do
      expect(AppUtils.show_contact(c1)).to eq("Unknown")
    end
    
    it "should return 'Unknown' when given Object other than Model" do
      expect(AppUtils.show_contact(Date.today, field: :officer)).to eq("Unknown")
    end
  end
  
  describe ".show_user" do
    let!(:c1) { create(:contact, sort_name: 'Merten, Michael', unit: 'MTS1', badge_no: '1234') }
    let!(:u1) { create(:user, login: 'mmerten', contact: c1) }
    
    it "should return 'Michael Merten' when given user id" do
      expect(AppUtils.show_user(u1.id)).to eq("Michael Merten")
    end
    
    it "should return 'Michael Merten' when given user" do
      expect(AppUtils.show_user(u1)).to eq("Michael Merten")
    end
    
    it "should return 'Unknown' when given bad user id" do
      expect(AppUtils.show_user(User.last.id + 1)).to eq("Unknown")
    end
    
    it "should return 'Unknown' when given non-User object" do
      expect(AppUtils.show_user(c1)).to eq("Unknown")
    end
  end
  
  describe ".fix_case" do
    it "should return 'Xx123' if given 'X*x 1:2/3'" do
      expect(AppUtils.fix_case('X*x 1:2/3')).to eq('Xx123')
    end
    
    it "should return nil if given anything other than a String" do
      expect(AppUtils.fix_case(123)).to eq(nil)
    end
  end
  
  describe ".datetime_diff" do
    it "should return 10 when given Time.now and Time.now - 10.seconds" do
      t = Time.now
      expect(AppUtils.datetime_diff(t, t - 10.seconds)).to eq(10)
    end
    
    it "should return 10 when given Time.now - 10.seconds and Time.now" do
      t = Time.now
      expect(AppUtils.datetime_diff(t - 10.seconds, t)).to eq(10)
    end
    
    it "should return 10 when given DateTime.now and DateTime.now - 10.seconds" do
      t = DateTime.now
      expect(AppUtils.datetime_diff(t, t - 10.seconds)).to eq(10)
    end
    
    it "should return 0 when given invalid argument types" do
      expect(AppUtils.datetime_diff(10, 20)).to eq(0)
    end
  end
  
  describe ".seconds_diff" do
    it "should return 10 when given Time.now and Time.now - 10.seconds" do
      t = Time.now
      expect(AppUtils.seconds_diff(t, t - 10.seconds)).to eq(10)
    end
  end
  
  describe ".minutes_diff" do
    it "should return 10 when given Time.now and Time.now - 10.minutes" do
      t = Time.now
      expect(AppUtils.minutes_diff(t, t - 10.minutes)).to eq(10)
    end
  end
  
  describe ".hours_diff" do
    it "should return 10 when given Time.now and Time.now - 10.hours" do
      t = Time.now
      expect(AppUtils.hours_diff(t, t - 10.hours)).to eq(10)
    end
  end
  
  describe ".days_diff" do
    it "should return 10 when given Time.now and Time.now - 10.days" do
      t = Time.now
      expect(AppUtils.days_diff(t, t - 10.days)).to eq(10)
    end
  end
  
  # note: 1 month = 30 days, not 1 calendar month which is variable
  describe ".months_diff" do
    it "should return 10 when given Time.now and Time.now - 300.days" do
      t = Time.now
      expect(AppUtils.months_diff(t, t - 300.days)).to eq(10)
    end
  end
  
  # note: 1 year = 365 days, not 12.months or 1.year which are variable
  describe ".minutes_diff" do
    it "should return 10 when given Time.now and Time.now - 3650.days" do
      t = Time.now
      expect(AppUtils.years_diff(t, t - 3650.days)).to eq(10)
    end
  end
  
  describe ".format_date" do
    # this should match beginning of Time.now() and should match Date.today()
    let(:today_date) { Date.today }
    let(:today_date_string) { Date.today.to_s(:us) }
    
    # test values and results
    let(:test_date) { Date.new(2015, 6, 15) }
    let(:test_time) { Time.new(2015, 6, 15, 16, 00, 00) }
    let(:test_date_short) { "06/15/2015" }
    let(:test_date_long) { "Monday, June 15, 2015" }
    let(:test_date_timestamp) { "20150615" }
    let(:test_time_short) { "06/15/2015 16:00 #{test_time.zone}" }
    let(:test_time_long) { "Monday, June 15, 2015 16:00 #{test_time.zone}" }
    let(:test_time_timestamp) { "20150615160000000#{test_time.zone}" }
    
    it "should return '' (blank string) if date_or_time is blank" do
      expect(AppUtils.format_date('')).to eq('')
    end
    
    it "should return string beginning with today_date_string if date_or_time is 'now'" do
      expect(AppUtils.format_date('now')).to start_with(today_date_string)
    end
    
    it "should return string matching today_date_string if date_or_time is 'today'" do
      expect(AppUtils.format_date('today')).to eq(today_date_string)
    end
    
    it "should return '' (blank string) if date_or_time is any string other than 'now' or 'today'" do
      expect(AppUtils.format_date('last_friday')).to eq('')
    end
    
    it "should return '' (blank string) if invalid time string is supplied as second argument" do
      expect(AppUtils.format_date(test_date, "25:00")).to eq('')
    end
    
    it "should return '' (blank string) if date_or_time is any object other than String, Date or Time" do
      expect(AppUtils.format_date(42)).to eq('')
    end
    
    it "should return test_time_short if date_or_time is test_date and time_string is '16:00'" do
      expect(AppUtils.format_date(test_date, "16:00")).to eq(test_time_short)
    end
    
    it "should return test_date_short if date_or_time is test_date" do
      expect(AppUtils.format_date(test_date)).to eq(test_date_short)
    end
    
    it "should return test_date_long if date_or_time is test_date and format: :long" do
      expect(AppUtils.format_date(test_date, format: :long)).to eq(test_date_long)
    end
    
    it "should return test_date_timestamp if date_or_time is test_date and format: :timestamp" do
      expect(AppUtils.format_date(test_date, format: :timestamp)).to eq(test_date_timestamp)
    end
    
    it "should return test_time_short if date_or_time is test_time" do
      expect(AppUtils.format_date(test_time)).to eq(test_time_short)
    end
    
    it "should return test_time_long if date_or_time is test_time and format: :long" do
      expect(AppUtils.format_date(test_time, format: :long)).to eq(test_time_long)
    end
    
    it "should return test_time_timestamp if date_or_time is test_time and format: :timestamp" do
      expect(AppUtils.format_date(test_time, format: :timestamp)).to eq(test_time_timestamp)
    end
    
    it "should return test_date_short if date_or_time is test_date and format: is not valid" do
      expect(AppUtils.format_date(test_date, format: :wrong)).to eq(test_date_short)
    end
  end
  
  describe ".get_datetime" do
    it "should return nil if date is a Date and time is not a valid time string" do
      expect(AppUtils.get_datetime(Date.today, "an hour ago")).to eq(nil)
    end
    
    it "should return nil if date is a Date and time string has hours or minutes out of range" do
      expect(AppUtils.get_datetime(Date.today, "24:60")).to eq(nil)
    end
    
    it "should return nil if date is not a Date or Time" do
      expect(AppUtils.get_datetime(42)).to eq(nil)
    end
    
    it "should return Time if date is a Date" do
      expect(AppUtils.get_datetime(Date.today)).to be_a(Time)
    end
    
    it "should return Time with HH:MM = 00:00 if date is a Date and no time string specified" do
      expect(AppUtils.get_datetime(Date.today).strftime("%H:%M")).to eq("00:00")
    end
    
    it "should return Time if date is a Date an time is '19:30'" do
      expect(AppUtils.get_datetime(Date.today, '19:30')).to be_a(Time)
    end
    
    it "should return Time with HH:MM = 19:30 if date is a Date and time string is '19:30'" do
      expect(AppUtils.get_datetime(Date.today, '19:30').strftime("%H:%M")).to eq("19:30")
    end
  end
  
  describe ".get_date_and_time" do
    it "should return [nil, nil] unless datetime is a DateTime or Time" do
      expect(AppUtils.get_date_and_time(Date)).to match_array([nil,nil])
    end
    
    it "should return [Date, time_string] if datetime is a Time" do
      date, time = AppUtils.get_date_and_time(Time.now)
      expect(date).to be_a(Date)
      expect(time).to match(/\d{2}:\d{2}/)
    end
  
    it "should return [Date, time_string] if datetime is a DateTime" do
      date, time = AppUtils.get_date_and_time(DateTime.now)
      expect(date).to be_a(Date)
      expect(time).to match(/\d{2}:\d{2}/)
    end
  end
  
  describe ".hours_to_sentence" do
    it "should return [0,0,0,0,0,false] array given 0 total_hours" do
      expect(AppUtils.hours_to_sentence(0)).to eq([0,0,0,0,0,false])
    end
    
    it "should return [1,1,1,1,0,false] array given 9505 total_hours" do
      expect(AppUtils.hours_to_sentence(9505)).to eq([1,1,1,1,0,false])
    end
    
    it "should return [0,0,0,0,1,false] array given 876000 total_hours" do
      expect(AppUtils.hours_to_sentence(876000)).to eq([0,0,0,0,1,false])
    end
    
    it "should return [0,0,0,0,0,true] array given -1 total_hours" do
      expect(AppUtils.hours_to_sentence(-1)).to eq([0,0,0,0,0,true])
    end
    
    it "should return return '1 Year, 1 Month, 1 Day, 1 Hour' string given 9505 total_hours with :returns :string" do
      expect(AppUtils.hours_to_sentence(9505, returns: :string)).to eq("1 Year, 1 Month, 1 Day, 1 Hour")
    end
  end
  
  describe ".sentence_to_hours" do
    it "should return 0 given [0,0,0,0,0,false] array" do
      expect(AppUtils.sentence_to_hours([0,0,0,0,0,false])).to eq(0)
    end
    
    it "should return 0 given 0,0,0,0,0,false arguments" do
      expect(AppUtils.sentence_to_hours(0,0,0,0,0,false)).to eq(0)
    end
    
    it "should return 9505 given [1,1,1,1,0,false] array" do
      expect(AppUtils.sentence_to_hours([1,1,1,1,0,false])).to eq(9505)
    end
    
    it "should return '396 Days' string given [1,1,1,1,0,false] array with returns: :string" do
      expect(AppUtils.sentence_to_hours([1,1,1,1,0,false], returns: :string)).to eq("396 Days")
    end
    
    it "should return '23 Hours' string given [23,0,0,0,0,false] array with returns: :string" do
      expect(AppUtils.sentence_to_hours([23,0,0,0,0,false], returns: :string)).to eq("23 Hours")
    end
    
    it "should return -1 given [0,0,0,0,0,true] array" do
      expect(AppUtils.sentence_to_hours([0,0,0,0,0,true])).to eq(-1)
    end
  end
  
  describe ".sentence_to_string" do
    it "should return '1 Year, 1 Month, 1 Day, 1 Hour' string given [1,1,1,1,0,false] array" do
      expect(AppUtils.sentence_to_string([1,1,1,1,0,false])).to eq("1 Year, 1 Month, 1 Day, 1 Hour")
    end
    
    it "should return '1 Life Sentence' given [0,0,0,0,1,false] array" do
      expect(AppUtils.sentence_to_string([0,0,0,0,1,false])).to eq("1 Life Sentence")
    end
    
    it "should return 'Death Sentence' given [0,0,0,0,0,true] array" do
      expect(AppUtils.sentence_to_string([0,0,0,0,0,true])).to eq("Death Sentence")
    end
    
    it "should return '0 Years, 0 Months, 0 Days, 1 Hour' string given [1,0,0,0,0,false] array with :include_zeros true" do
      expect(AppUtils.sentence_to_string([1,0,0,0,0,false], include_zeros: true)).to eq("0 Years, 0 Months, 0 Days, 1 Hour")
    end
    
    it "should return '1L, 1Y, 1M, 1D, 1H' string given [1,1,1,1,1,false] with :short true" do
      expect(AppUtils.sentence_to_string([1,1,1,1,1,false], short: true)).to eq("1L, 1Y, 1M, 1D, 1H")
    end
    
    it "should return 'Death Sentence' string given [1,1,1,1,1,true] with :short true" do
      expect(AppUtils.sentence_to_string([1,1,1,1,1,true], short: true)).to eq("Death Sentence")
    end
  end
  
  describe ".hours_to_string" do
    it "should return '1 Day' string given 24 hours" do
      expect(AppUtils.hours_to_string(24)).to eq('1 Day')
    end
    
    it "should return '23 Hours' string given 23 hours" do
      expect(AppUtils.hours_to_string(23)).to eq('23 Hours')
    end
    
    it "should return 'Death Sentence' string given -1 hours" do
      expect(AppUtils.hours_to_string(-1)).to eq('Death Sentence')
    end
    
    it "should return '0 Hours' string given invalid object" do
      expect(AppUtils.hours_to_string(Date)).to eq('0 Hours')
    end
  end
  
  describe ".valid_date" do
    it "should return nil if datestring is blank" do
      expect(AppUtils.valid_date('')).to eq(nil)
    end
    
    it "should return Date.today if datestring is blank and :allow_nil false" do
      expect(AppUtils.valid_date('', allow_nil: false)).to eq(Date.today)
    end
    
    it "should return given Date unaltered" do
      d = Date.today - 2.days
      expect(AppUtils.valid_date(d)).to eq(d)
    end
    
    it "should return Date if given Time" do
      t = Time.now
      d = t.to_date
      expect(AppUtils.valid_date(t)).to eq(d)
    end
    
    it "should return nil if datestring is not a String, Date or Time" do
      expect(AppUtils.valid_date(42)).to eq(nil)
    end
    
    it "should return nil if datestring cannot be parsed by Chronic gem" do
      expect(AppUtils.valid_date("not_parsable")).to eq(nil)
    end
    
    it "should return Date if datestring can be parsed by Chronic gem" do
      d = Date.today
      expect(AppUtils.valid_date(d.to_s)).to eq(d)
      expect(AppUtils.valid_date('today')).to eq(d)
    end
  end
  
  describe ".valid_datetime" do
    it "should return nil if datestring is blank" do
      expect(AppUtils.valid_datetime('')).to eq(nil)
    end
    
    it "should return Time.now if datestring is blank and :allow_nil false" do
      # times will never match, so convert to date and compare
      d = Time.now.to_date
      expect(AppUtils.valid_datetime('', allow_nil: false).to_date).to eq(d)
    end
    
    it "should return Time at '00:00' if given Date" do
      expect(AppUtils.valid_datetime(Date.today).to_s(:time)).to eq("00:00")
    end
    
    it "should return Time values unchanged" do
      t = Time.now
      expect(AppUtils.valid_datetime(t)).to eq(t)
    end
    
    it "should return nil if datestring is not String, Date or Time" do
      expect(AppUtils.valid_datetime(42)).to eq(nil)
    end
    
    it "should return nil if datestring cannot be parsed using Chronic gem" do
      expect(AppUtils.valid_datetime("unparsable")).to eq(nil)
    end
    
    it "should return Time if datestring can be parsed using Chronic gem" do
      expect(AppUtils.valid_datetime(Time.now.to_s)).to be_a(Time)
      expect(AppUtils.valid_datetime('now')).to be_a(Time)
    end
  end
  
  describe ".valid_time" do
    it "should return nil if timestring is blank" do
      expect(AppUtils.valid_time('')).to eq(nil)
    end
    
    it "should return '00:00' string if timestring is blank and :allow_nil is false" do
      expect(AppUtils.valid_time('', allow_nil: false)).to eq('00:00')
    end
    
    it "should return '12:00' if given '1200'" do
      expect(AppUtils.valid_time('1200')).to eq('12:00')
    end
    
    it "should return '12:00' if given '120030'" do
      expect(AppUtils.valid_time('120030')).to eq('12:00')
    end
    
    it "should return '23:59' if given '2460'" do
      expect(AppUtils.valid_time('2460')).to eq("23:59")
    end
    
    it "should return nil unless timestring is a String" do
      expect(AppUtils.valid_time(42)).to eq(nil)
    end
  end
  
  describe ".current_time" do
    it "should return pattern matching /\d{2}:\d{2}/" do
      expect(AppUtils.current_time).to match(/\d{2}:\d{2}/)
    end
  end
  
  describe ".duration_in_words" do
    it "should return '0 Minutes' if arguments are not Time" do
      expect(AppUtils.duration_in_words(Date.today - 1.day, Date.today)).to eq('0 Minutes')
    end
    
    it "should return '5 Minutes' if given Time.now - 5.minutes and Time.now" do
      t = Time.now
      expect(AppUtils.duration_in_words(t - 5.minutes, t)).to eq('5 Minutes')
    end
    
    it "should return '1 Hour' given Time.now - 1.hour and Time.now" do
      t = Time.now
      expect(AppUtils.duration_in_words(t - 1.hour, t)).to eq('1 Hour')
    end
    
    it "should return '1 Day' given Time.now - 1.day and Time.now" do
      t = Time.now
      expect(AppUtils.duration_in_words(t - 1.day, t)).to eq('1 Day')
    end
  end
  
  describe ".mil_to_civ" do
    it "should return '00:00 AM' unless time is a string matching /^\d{2}:\d{2}$/" do
      expect(AppUtils.mil_to_civ('bad_time_string')).to eq("00:00 AM")
    end
    
    it "should return '08:00 AM' if given '08:00'" do
      expect(AppUtils.mil_to_civ('08:00')).to eq('08:00 AM')
    end
    
    it "should return '08:00 PM' if given '20:00'" do
      expect(AppUtils.mil_to_civ('20:00')).to eq('08:00 PM')
    end
    
    it "should return '11:59 PM' if given '24:60'" do
      expect(AppUtils.mil_to_civ('24:60')).to eq('11:59 PM')
    end
  end
  
  describe ".total_miles" do
    it "should return 0 if arguments are not integers" do
      expect(AppUtils.total_miles('a',10)).to eq(0)
      expect(AppUtils.total_miles(10,'a')).to eq(0)
    end
    
    it "should return 10 if given 2010 and 2020" do
      expect(AppUtils.total_miles(2010,2020)).to eq(10)
    end
    
    it "should return 10 if given 9990 and 0" do
      expect(AppUtils.total_miles(9990,0)).to eq(10)
    end
  end
  
  describe ".audit_log" do
    it "should make a call to Audit.add once given msg and method_name" do
      allow(Audit).to receive(:add)
      AppUtils.audit_log("Test", "describe('audit_log()')")
      expect(Audit).to have_received(:add)
    end
  end
  
  describe ".compress_spaces" do
    it "should return 'forty two' string if given 'forty        two' string" do
      expect(AppUtils.compress_spaces('forty        two')).to eq('forty two')
    end
  end
  
  describe ".valid_model" do
    it "should raise ArgumentError exception if model_or_string is an object that is not ActiveRecord descendent" do
      expect {AppUtils.valid_model(Date)}.to raise_error(ArgumentError)
    end
    
    it "should raise ArgumentError exception if model_or_string is a String that is not a valid Model name" do
      expect {AppUtils.valid_model("ArrestsController")}.to raise_error(ArgumentError)
    end
    
    it "should return Model Class if given a Model Class" do
      expect(AppUtils.valid_model(Arrest)).to eq(Arrest)
    end
    
    it "should return Model Class if given a Model Class name (underscored String)" do
      expect(AppUtils.valid_model('arrest')).to eq(Arrest)
    end
    
    it "should return Model Class if given a Model Class name (symbol)" do
      expect(AppUtils.valid_model(:arrest)).to eq(Arrest)
    end
  end
  
  describe ".valid_fields" do
    it "should raise exception if array_or_string is blank" do
      expect {AppUtils.valid_fields(Arrest, nil, '')}.to raise_error(ArgumentError)
    end
    
    it "should raise exception if array_or_string is blank after removing non-string and non-symbol elements" do
      expect {AppUtils.valid_fields(Arrest, nil, [123,true])}.to raise_error(ArgumentError)
    end
    
    it "should raise exception if array_or_string is column name that does not exist in Model" do
      expect {AppUtils.valid_fields(Arrest, nil, :invalid_column)}.to raise_error(ArgumentError)
    end
    
    it "should raise exception if array_or_string is array of column names that do not exist in association when association is given" do
      expect {AppUtils.valid_fields(Arrest, :person, [:invalid_column])}.to raise_error(ArgumentError)
    end
    
    it "should return [:arrest_datetime] if model is Arrest and array_or_string is :arrest_datetime" do
      expect(AppUtils.valid_fields(Arrest, nil, :arrest_datetime)).to eq([:arrest_datetime])
    end
    
    it "should return :date_of_birth if model is Arrest, association is :person, array_or_string is [:date_of_birth] and :array is false" do
      expect(AppUtils.valid_fields(Arrest, :person, [:date_of_birth], array: false)).to eq(:date_of_birth)
    end
  end
  
  describe ".valid_bool" do
    it "should return true if given integer > 0, '1', 't', 'true' or true" do
      expect(AppUtils.valid_bool(1)).to be true
      expect(AppUtils.valid_bool('1')).to be true
      expect(AppUtils.valid_bool('t')).to be true
      expect(AppUtils.valid_bool('true')).to be true
      expect(AppUtils.valid_bool(true)).to be true
    end
    
    it "should return false if given anything other than integer > 0, '1', 't', 'true' or true" do
      expect(AppUtils.valid_bool(0)).to be false
      expect(AppUtils.valid_bool('forty_two')).to be false
      expect(AppUtils.valid_bool(Date)).to be false
      expect(AppUtils.valid_bool(false)).to be false
    end
  end
  
  describe ".valid_prefix" do
    it "should return '' (empty string) unless model is ActiveRecord descendent" do
      expect(AppUtils.valid_prefix(Date)).to eq('')
    end
    
    it "should return '' (empty string) unless association is nil, String or Symbol" do
      expect(AppUtils.valid_prefix(Arrest, 42)).to eq('')
    end
    
    it "should raise exception if association is not a valid association for model" do
      expect {AppUtils.valid_prefix(Arrest, :courts)}.to raise_error(ArgumentError)
    end
    
    it "should return 'arrests' if given Arrest model class with no association" do
      expect(AppUtils.valid_prefix(Arrest)).to eq('arrests')
    end
    
    it "should return 'people' if given Arrest model class and :person association" do
      expect(AppUtils.valid_prefix(Arrest, :person)).to eq('people')
    end
  end
end