# == Schema Information
#
# Table name: signal_codes
#
#  id         :integer          not null, primary key
#  code       :string(255)      default(""), not null
#  name       :string(255)      default(""), not null
#  created_by :integer          default(1)
#  updated_by :integer          default(1)
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_signal_codes_on_code        (code)
#  index_signal_codes_on_created_by  (created_by)
#  index_signal_codes_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  signal_codes_created_by_fk  (created_by => users.id)
#  signal_codes_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe SignalCode do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
