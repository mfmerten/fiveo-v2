# == Schema Information
#
# Table name: invest_vehicles
#
#  id               :integer          not null, primary key
#  investigation_id :integer          not null
#  private          :boolean          default(TRUE), not null
#  owner_id         :integer
#  make             :string(255)      default(""), not null
#  model            :string(255)      default(""), not null
#  vin              :string(255)      default(""), not null
#  license          :string(255)      default(""), not null
#  color            :string(255)      default(""), not null
#  tires            :string(255)      default(""), not null
#  model_year       :string(255)      default(""), not null
#  description      :text             default(""), not null
#  remarks          :text             default(""), not null
#  created_by       :integer          default(1)
#  updated_by       :integer          default(1)
#  created_at       :datetime
#  updated_at       :datetime
#  owned_by         :integer          default(1)
#  license_state    :string(255)      default(""), not null
#  vehicle_type     :string(255)      default(""), not null
#
# Indexes
#
#  index_invest_vehicles_on_created_by        (created_by)
#  index_invest_vehicles_on_investigation_id  (investigation_id)
#  index_invest_vehicles_on_owned_by          (owned_by)
#  index_invest_vehicles_on_owner_id          (owner_id)
#  index_invest_vehicles_on_updated_by        (updated_by)
#
# Foreign Keys
#
#  invest_vehicles_created_by_fk        (created_by => users.id)
#  invest_vehicles_investigation_id_fk  (investigation_id => investigations.id)
#  invest_vehicles_owned_by_fk          (owned_by => users.id)
#  invest_vehicles_owner_id_fk          (owner_id => people.id)
#  invest_vehicles_updated_by_fk        (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe InvestVehicle do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
