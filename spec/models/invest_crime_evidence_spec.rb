# == Schema Information
#
# Table name: invest_crime_evidences
#
#  id                 :integer          not null, primary key
#  invest_crime_id    :integer          not null
#  invest_evidence_id :integer          not null
#  created_at         :datetime
#  updated_at         :datetime
#
# Indexes
#
#  index_invest_crime_evidences_on_invest_crime_id     (invest_crime_id)
#  index_invest_crime_evidences_on_invest_evidence_id  (invest_evidence_id)
#
# Foreign Keys
#
#  invest_crime_evidences_invest_crime_id_fk     (invest_crime_id => invest_crimes.id)
#  invest_crime_evidences_invest_evidence_id_fk  (invest_evidence_id => invest_evidences.id)
#

require 'rails_helper'

RSpec.describe InvestCrimeEvidence do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
