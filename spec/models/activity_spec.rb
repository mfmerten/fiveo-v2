# == Schema Information
#
# Table name: activities
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  section     :string(255)      default(""), not null
#  description :string(255)      default(""), not null
#  created_at  :datetime
#  updated_at  :datetime
#  username    :string(255)      default(""), not null
#
# Indexes
#
#  index_activities_on_user_id  (user_id)
#
# Foreign Keys
#
#  activities_user_id_fk  (user_id => users.id)
#

require 'rails_helper'

RSpec.describe Activity do
  
  describe "Table Columns" do
    it { is_expected.to have_db_column(:user_id).of_type(:integer) }
    it { is_expected.to have_db_column(:section).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:description).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:username).of_type(:string).with_options(default: '', null: false) }
  end
  
  describe "Table Indices" do
    it { is_expected.to have_db_index(:user_id) }
  end
  
  describe "Associations" do
    it { is_expected.to belong_to(:user).inverse_of(:activities) }
  end
  
  describe 'Scopes' do
    let!(:a1) { create(:activity) }
    let!(:a2) { create(:activity) }
    let!(:a3) { create(:activity) }
    
    describe ":by_reverse_id" do
      it "should return activities in reverse id order" do
        query = Activity.by_reverse_id.where(id: [a1.id, a2.id, a3.id]).pluck(:id)
        expect(query[0]).to be > query[1]
        expect(query[1]).to be > query[2]
      end
    end
    
    describe ":with_activity_id" do
      it "should return activity specified by id" do
        expect(Activity.with_activity_id(a1.id).pluck(:id)).to eq([a1.id])
      end
    end
    
    describe ":with_username" do
      it "should call AppUtils.like to find activities with username text" do
        allow(AppUtils).to receive(:like)
        Activity.with_username("rmerten")
        expect(AppUtils).to have_received(:like).with(Activity, :username, 'rmerten')
      end
    end
    
    describe ":with_section" do
      it "should call AppUtils.for_lower to find activities with section text" do
        allow(AppUtils).to receive(:for_lower)
        Activity.with_section("specs")
        expect(AppUtils).to have_received(:for_lower).with(Activity, :section, 'specs')
      end
    end
    
    describe ":with_description" do
      it "should call AppUtils.like to find activities with description text" do
        allow(AppUtils).to receive(:like)
        Activity.with_description('tests')
        expect(AppUtils).to have_received(:like).with(Activity, :description, 'tests')
      end
    end
    
    describe ":with_created" do
      it "should call AppUtils.for_daterange to find activities created between two dates" do
        allow(AppUtils).to receive(:for_daterange)
        Activity.with_created('06/01/2015','06/02/2015')
        expect(AppUtils).to have_received(:for_daterange).with(Activity, :created_at, start: '06/01/2015', stop: '06/02/2015')
      end
    end
  end
  
  describe "Class Methods" do
    describe ".desc" do
      it { is_expected.to have_defined_desc }
    end
    
    describe ".base_perms" do
      it { is_expected.to have_defined_base_perms }
    end
    
    describe ".add" do
      it "should add Activity records and create log entries" do
        allow(Activity).to receive(:create)
        allow(Rails.logger).to receive(:warn)
        Activity.add("Section", "Description", 1, "admin")
        expect(Activity).to have_received(:create).with(section: 'Section', description: 'Description', user_id: 1, username: 'admin (1)')
        expect(Rails.logger).to have_received(:warn)
      end
    end
    
    describe ".purge_old_records" do
      it "should delete all records before SiteConfig.activity_retention days ago" do
        expiration = SiteConfig.activity_retention
        if expiration == 0
          # expiration disabled, activate it
          SiteConfig.activity_retention = 30
          expiration = 30
        end
        a1 = create(:activity, section: "Purge Test", created_at: (expiration + 2).days.ago)
        a2 = create(:activity, section: "Purge Test", created_at: (expiration + 4).days.ago)
        a3 = create(:activity, section: "Purge Test", created_at: (expiration + 6).days.ago)
        a4 = create(:activity, section: "Purge Test", created_at: (expiration - 2).days.ago)
      
        Activity.purge_old_records
    
        expect(Activity.where(section: "Purge Test").pluck(:id)).to eq([a4.id])
      end
    end
    
    describe ".process_query" do
      before(:example) do
        allow(Activity).to receive(:with_activity_id).and_call_original
        allow(Activity).to receive(:with_created).and_call_original
        allow(Activity).to receive(:with_username).and_call_original
        allow(Activity).to receive(:with_section).and_call_original
        allow(Activity).to receive(:with_description).and_call_original
      end
      
      let(:full_query) {
        {
          id: '1',
          created_from: '01/01/2015',
          created_to: '01/02/2015',
          username: 'mmerten',
          section: 'Activities',
          description: 'Testing'
        }
      }
      let(:empty_query) {
        {
          id: '',
          created_from: '',
          created_to: '',
          username: '',
          section: '',
          description: ''
        }
      }
      
      it "should return empty relation when called with anything other than a Hash" do
        expect(Activity.process_query(Date)).to be_a(ActiveRecord::Relation)
        expect(Activity.process_query(Date)).to match_array([])
      end
      
      it "should execute appropriate scopes when given query hash" do
        Activity.process_query(full_query)
        expect(Activity).to have_received(:with_activity_id).with('1')
        expect(Activity).to have_received(:with_username).with('mmerten')
        expect(Activity).to have_received(:with_section).with('Activities')
        expect(Activity).to have_received(:with_description).with('Testing')
        expect(Activity).to have_received(:with_created).with('01/01/2015', '01/02/2015')
      end
      
      it "should not call any scopes when given empty query" do
        Activity.process_query(empty_query)
        expect(Activity).not_to have_received(:with_activity_id)
        expect(Activity).not_to have_received(:with_username)
        expect(Activity).not_to have_received(:with_section)
        expect(Activity).not_to have_received(:with_description)
        expect(Activity).not_to have_received(:with_created)
      end
    end
  end
  
end
