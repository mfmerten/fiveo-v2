# == Schema Information
#
# Table name: absents
#
#  id                   :integer          not null, primary key
#  escort_officer       :string(255)      default(""), not null
#  escort_officer_unit  :string(255)      default(""), not null
#  created_by           :integer          default(1)
#  updated_by           :integer          default(1)
#  created_at           :datetime
#  updated_at           :datetime
#  escort_officer_badge :string(255)      default(""), not null
#  jail_id              :integer          not null
#  leave_datetime       :datetime
#  return_datetime      :datetime
#  absent_type          :string(255)      default(""), not null
#
# Indexes
#
#  index_absents_on_created_by       (created_by)
#  index_absents_on_jail_id          (jail_id)
#  index_absents_on_return_datetime  (return_datetime)
#  index_absents_on_updated_by       (updated_by)
#
# Foreign Keys
#
#  absents_created_by_fk  (created_by => users.id)
#  absents_jail_id_fk     (jail_id => jails.id)
#  absents_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Absent do
  
  describe "Table Columns" do
    it { is_expected.to have_db_column(:escort_officer).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:escort_officer_unit).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:escort_officer_badge).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
    it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_options(default: 1) }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:jail_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:leave_datetime).of_type(:datetime) }
    it { is_expected.to have_db_column(:return_datetime).of_type(:datetime) }
    it { is_expected.to have_db_column(:absent_type).of_type(:string).with_options(null: false, default: '') }
  end
  
  describe "Table Indices" do
    it { is_expected.to have_db_index(:created_by) }
    it { is_expected.to have_db_index(:updated_by) }
    it { is_expected.to have_db_index(:jail_id) }
    it { is_expected.to have_db_index(:updated_by) }
  end
  
  describe "Associations" do
    it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key('created_by') }
    it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key('updated_by') }
    it { is_expected.to belong_to(:jail).inverse_of(:absents) }
    it { is_expected.to have_many(:absent_prisoners).dependent(:destroy) }
    it { is_expected.to have_many(:prisoners).through(:absent_prisoners).source(:booking) }
    it { is_expected.to have_many(:people).through(:prisoners) }
  end
  
  describe "Scopes" do
    let!(:j1) { create(:jail) }
    let!(:j2) { create(:jail, :spso) }
    let!(:p1) { create(:person, family_name: "Smith", given_name: "John") }
    let!(:p2) { create(:person, family_name: "Merten", given_name: "Mike") }
    let!(:b1) { create(:booking, person: p1, jail: j1) }
    let!(:b2) { create(:booking, person: p2, jail: j2) }
    let!(:a1) { create(:absent, jail: j1) }
    let!(:a2) { create(:absent, jail: j2, leave_datetime: Time.now - 6.hours) }
    let!(:a3) { create(:absent, jail: j2, leave_datetime: Time.now - 5.hours, return_datetime: Time.now) }
    let!(:pr1) { create(:absent_prisoner, booking: b1, absent: a1) }
    let!(:pr2) { create(:absent_prisoner, booking: b2, absent: a2) }
    
    describe ":active" do
      it "should return only active absents" do
        expect(Absent.active.pluck(:id)).to include(a1.id, a2.id)
        expect(Absent.active.pluck(:id)).not_to include(a3.id)
      end
    end
    
    describe ":by_reverse_id" do
      it "should return absents in reverse id order" do
        query = Absent.by_reverse_id.where(id: [a1.id, a2.id, a3.id]).pluck(:id)
        expect(query[0]).to be > query[1]
        expect(query[1]).to be > query[2]
      end
    end
    
    describe ":with_absent_id" do
      it "should return an absent specified by id" do
        expect(Absent.with_absent_id(a2.id).pluck(:id)).to eq([a2.id])
      end
    end
    
    describe ":with_booking_id" do
      it "should return absents with specified booking id" do
        expect(Absent.includes(:absent_prisoners).with_booking_id(b1.id).pluck(:id)).to eq([a1.id])
      end
    end
    
    describe ":with_person_id" do
      it "should return absents with specified person id" do
        expect(Absent.includes(:people).with_person_id(p2.id).pluck(:id)).to eq([a2.id])
      end
    end
    
    describe ":belonging_to_jail" do
      it "should return absents with specified jail id" do
        expect(Absent.belonging_to_jail(j1.id).pluck(:id)).to eq([a1.id])
      end
    end
    
    describe ":with_absent_type" do
      it "should call AppUtils.like to find absents with absent type text" do
        allow(AppUtils).to receive(:like).and_return(Absent.none)
        Absent.with_absent_type("Work Crew")
        expect(AppUtils).to have_received(:like).with(Absent, :absent_type, "Work Crew")
      end
    end
    
    describe ":with_return" do
      it "should call AppUtils.for_daterange to find absents by return date" do
        allow(AppUtils).to receive(:for_daterange).and_return(Absent.none)
        Absent.with_return(Date.today - 1.day, Date.today)
        expect(AppUtils).to have_received(:for_daterange).with(Absent, :return_datetime, start: Date.today - 1.day, stop: Date.today)
      end
    end
    
    describe ":with_leave" do
      it "should call AppUtils.for_daterange to find absents by leave date" do
        allow(AppUtils).to receive(:for_daterange).and_return(Absent.none)
        Absent.with_leave(Date.today - 1.day, Date.today)
        expect(AppUtils).to have_received(:for_daterange).with(Absent, :leave_datetime, start: Date.today - 1.day, stop: Date.today)
      end
    end
    
    describe ":with_officer" do
      it "should call AppUtils.for_officer to find absents with specified escort officer" do
        allow(AppUtils).to receive(:for_officer).and_return(Absent.none)
        Absent.with_officer("Merten","","")
        expect(AppUtils).to have_received(:for_officer).with(Absent, :escort_officer, "Merten", "", "")
      end
    end
    
    describe ":with_person_name" do
      it "should call AppUtils.for_person to find absents with specified person name text" do
        allow(AppUtils).to receive(:for_person).and_return(Absent.none)
        Absent.with_person_name("Michael Merten")
        expect(AppUtils).to have_received(:for_person).with(Absent, "Michael Merten", association: :people)
      end
    end
  end

  describe "Attributes" do
    it { is_expected.to accept_nested_attributes_for(:absent_prisoners).allow_destroy(true) }
    it { is_expected.to have_attr_accessor(:escort_officer_id) }
  end
  
  describe "before_validation" do

    describe ":lookup_officers" do
      it "should call lookup_officers method" do
        a1 = build(:absent)
        allow(a1).to receive(:lookup_officers)
        a1.valid?
        expect(a1).to have_received(:lookup_officers).with(no_args)
      end
    end
  end
    
  describe "after_validation" do
    
    describe ":check_creator" do
      it "should call check_creator method" do
        a1 = build(:absent)
        allow(a1).to receive(:check_creator)
        a1.valid?
        expect(a1).to have_received(:check_creator)
      end
    end
  end
  
  describe "Validations" do
    describe ":escort_officer" do
      it { is_expected.to validate_presence_of(:escort_officer) }
    end
    
    describe ":absent_type" do
      it { is_expected.to validate_presence_of(:absent_type) }
    end
    
    describe ":leave_datetime" do
      it { is_expected.to validate_datetime(:leave_datetime).allow_blank }
    end
    
    describe ":return_datetime" do
      it { is_expected.to validate_datetime(:return_datetime).allow_blank }
    end
  end
  
  describe "Class Methods" do
    describe ".desc" do
      it { is_expected.to have_defined_desc }
    end
    
    describe ".base_perms" do
      it { is_expected.to have_defined_base_perms }
    end
    
    describe ".process_query" do
      before(:example) do
        allow(Absent).to receive(:with_absent_id).and_call_original
        allow(Absent).to receive(:with_person_id).and_call_original
        allow(Absent).to receive(:with_absent_type).and_call_original
        allow(Absent).to receive(:with_return).and_call_original
        allow(Absent).to receive(:with_leave).and_call_original
        allow(Absent).to receive(:with_officer).and_call_original
        allow(Absent).to receive(:with_person_name).and_call_original
        allow(Absent).to receive(:with_booking_id).and_call_original
      end
      
      let(:full_query) {
        {
          id: '1',
          booking_id: '2',
          person_id: '3',
          person_name: 'Merten',
          leave_from: '01/01/2015',
          leave_to: '01/02/2015',
          return_from: '01/03/2015',
          return_to: '01/04/2015',
          absent_type: 'Work Crew',
          escort_officer: 'John Doe',
          escort_officer_unit: 'APD1',
          escort_officer_badge: '1234'
        }
      }
      
      let(:empty_query) {
        {
          id: '',
          booking_id: '',
          person_id: '',
          person_name: '',
          leave_from: '',
          leave_to: '',
          return_from: '',
          return_to: '',
          absent_type: '',
          escort_officer: '',
          escort_officer_unit: '',
          escort_officer_badge: ''
        }
      }
      
      it "should return empty relation when called with anything other than a Hash" do
        expect(Absent.process_query(Date)).to be_a(ActiveRecord::Relation)
        expect(Absent.process_query(Date)).to match_array([])
      end
      
      it "should execute appropriate scopes when given query hash" do
        Absent.process_query(full_query)
        expect(Absent).to have_received(:with_absent_id).with('1')
        expect(Absent).to have_received(:with_booking_id).with('2')
        expect(Absent).to have_received(:with_person_id).with('3')
        expect(Absent).to have_received(:with_person_name).with('Merten')
        expect(Absent).to have_received(:with_leave).with('01/01/2015', '01/02/2015')
        expect(Absent).to have_received(:with_return).with('01/03/2015', '01/04/2015')
        expect(Absent).to have_received(:with_absent_type).with('Work Crew')
        expect(Absent).to have_received(:with_officer).with('John Doe', 'APD1', '1234')
      end
      
      it "should not call any scopes when given empty query" do
        Absent.process_query(empty_query)
        expect(Absent).not_to have_received(:with_absent_id)
        expect(Absent).not_to have_received(:with_booking_id)
        expect(Absent).not_to have_received(:with_person_id)
        expect(Absent).not_to have_received(:with_person_name)
        expect(Absent).not_to have_received(:with_leave)
        expect(Absent).not_to have_received(:with_return)
        expect(Absent).not_to have_received(:with_absent_type)
        expect(Absent).not_to have_received(:with_officer)
      end
    end
  end
  
  describe "Instance Methods" do
    
    describe "#lookup_officers" do
      it "should update escort officer if escort_officer_id is not blank" do
        # set escort officer id
        c1 = create(:contact, sort_name: "Merten, Michael")
        a1 = build(:absent, escort_officer: "Merten", escort_officer_id: c1.id)
        a1.lookup_officers
        expect(a1.escort_officer).to eq("Merten, Michael")
      end
      
      it "should not change escort officer if escort_officer_id is blank" do
        # leave escort officer id blank
        a1 = build(:absent, escort_officer: "Merten")
        a1.lookup_officers
        expect(a1.escort_officer).to eq("Merten")
      end
    end
  end

end
