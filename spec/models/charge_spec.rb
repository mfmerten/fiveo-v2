# == Schema Information
#
# Table name: charges
#
#  id          :integer          not null, primary key
#  citation_id :integer
#  charge      :string(255)      default(""), not null
#  count       :integer          default(1), not null
#  created_at  :datetime
#  updated_at  :datetime
#  warrant_id  :integer
#  agency      :string(255)      default(""), not null
#  arrest_id   :integer
#  charge_type :string(255)      default(""), not null
#
# Indexes
#
#  index_charges_on_arrest_id    (arrest_id)
#  index_charges_on_citation_id  (citation_id)
#  index_charges_on_warrant_id   (warrant_id)
#
# Foreign Keys
#
#  charges_arrest_id_fk    (arrest_id => arrests.id)
#  charges_citation_id_fk  (citation_id => citations.id)
#  charges_warrant_id_fk   (warrant_id => warrants.id)
#

require 'rails_helper'

RSpec.describe Charge do
  
  ## table columns
  #it { is_expected.to have_db_column(:citation_id).of_type(:integer) }
  #it { is_expected.to have_db_column(:warrant_id).of_type(:integer) }
  #it { is_expected.to have_db_column(:arrest_id).of_type(:integer) }
  #it { is_expected.to have_db_column(:charge).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:count).of_type(:integer).with_options(null: false, default: 1) }
  #it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:agency).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:charge_type).of_type(:string).with_options(null: false, default: '') }
  #
  ## table indexes
  #it { is_expected.to have_db_index(:arrest_id) }
  #it { is_expected.to have_db_index(:citation_id) }
  #it { is_expected.to have_db_index(:warrant_id) }
  #
  ## associations
  #it { is_expected.to belong_to(:arrest).inverse_of(:charges) }
  #it { is_expected.to belong_to(:citation).inverse_of(:charges) }
  #it { is_expected.to belong_to(:warrant).inverse_of(:charges) }
  #it { is_expected.to have_many(:da_charges).dependent(:nullify) }
  #
  ## attributes
  #it { is_expected.to have_attr_accessor(:agency_id) }
  #
  ## scopes
  #describe "scopes" do
  #  before(:context) do
  #    # create test records
  #  end
  #  
  #  it "should return charges with type City when using :city scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return charges with type District when using :district scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return charges with type Other when using :other scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return charges with no type when using :untyped scope" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## callbacks
  #describe "before_validation" do
  #  describe ":lookup_agency" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  ## validations
  #it { is_expected.to validate_presence_of(:charge) }
  #it { is_expected.to validate_numericality_of(:count).only_integer(true).greater_than(0) }
  #
  ## constants
  #describe "ChargeType" do
  #  it "should return array of charge types" do
  #    expect(Charge::ChargeType).to match_array(['District','City','Other'])
  #  end
  #end
  #
  ## methods
  #it { is_expected.to have_defined_desc }
  #
  #describe "#is_final?" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#to_s" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
end
