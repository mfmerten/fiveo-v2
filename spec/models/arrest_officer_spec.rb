# == Schema Information
#
# Table name: arrest_officers
#
#  id            :integer          not null, primary key
#  arrest_id     :integer          not null
#  officer       :string(255)      default(""), not null
#  officer_unit  :string(255)      default(""), not null
#  officer_badge :string(255)      default(""), not null
#  created_at    :datetime
#  updated_at    :datetime
#
# Indexes
#
#  index_arrest_officers_on_arrest_id  (arrest_id)
#
# Foreign Keys
#
#  arrest_officers_arrest_id_fk  (arrest_id => arrests.id)
#

require 'rails_helper'

RSpec.describe ArrestOfficer do
  
  describe "Table Columns" do
    it { is_expected.to have_db_column(:arrest_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:officer).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:officer_unit).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:officer_badge).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  end
  
  describe "Table Indices" do
    it { is_expected.to have_db_index(:arrest_id) }
  end
  
  describe "Associations" do
    it { is_expected.to belong_to(:arrest).inverse_of(:officers).counter_cache(true) }
  end
  
  describe "Attributes" do
    it { is_expected.to have_attr_accessor(:officer_id) }
  end
  
  describe "before_validation" do

    describe ":update_officer" do
      it "should call update_officer method" do
        o1 = build(:arrest_officer)
        allow(o1).to receive(:update_officer)
        o1.valid?
        expect(o1).to have_received(:update_officer).with(no_args)
      end
    end
  end
  
  describe "Validations" do
    describe ":officer" do
      it { is_expected.to validate_presence_of(:officer) }
    end
  end
  
  describe "Class Methods" do
    describe ".desc" do
      it { is_expected.to have_defined_desc }
    end
  end
  
  describe "Instance Methods" do
    describe "#to_s" do
      it "should return a String value" do
        expect(build(:arrest_officer).to_s).to be_a(String)
      end
    
      it "should return officer name, prefixed by unit if unit is not blank" do
        expect(build(:arrest_officer, officer: "Merten", officer_unit: "TBPD1").to_s).to eq("TBPD1 Merten")
      end
    end
    
    describe "#update_officer" do
      it "should update officer information if :officer_id not blank" do
        c1 = create(:contact, sort_name: "Merten, Michael")
        a1 = create(:arrest)
        o1 = build(:arrest_officer, officer: "Merten", arrest: a1, officer_id: c1.id)
        o1.update_officer
        expect(o1.officer).to eq("Merten, Michael")
      end
      
      it "should not change officer information if officer_id is blank" do
        a1 = create(:arrest)
        o2 = build(:arrest_officer, officer: "Merten", arrest: a1)
        o2.update_officer
        expect(o2.officer).to eq("Merten")
      end
    end
  end

end
