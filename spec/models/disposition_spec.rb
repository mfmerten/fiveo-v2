# == Schema Information
#
# Table name: dispositions
#
#  id          :integer          not null, primary key
#  atn         :string(255)      default(""), not null
#  person_id   :integer          not null
#  report_date :date
#  disposition :text             default(""), not null
#  remarks     :text             default(""), not null
#  status      :integer          default(0), not null
#  created_by  :integer          default(1)
#  updated_by  :integer          default(1)
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_dispositions_on_atn         (atn)
#  index_dispositions_on_created_by  (created_by)
#  index_dispositions_on_person_id   (person_id)
#  index_dispositions_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  dispositions_created_by_fk  (created_by => users.id)
#  dispositions_person_id_fk   (person_id => people.id)
#  dispositions_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Disposition do
  
  ## table column
  #it { is_expected.to have_db_column(:atn).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:person_id).of_type(:integer).with_options(null: false) }
  #it { is_expected.to have_db_column(:report_date).of_type(:date) }
  #it { is_expected.to have_db_column(:disposition).of_type(:text).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:remarks).of_type(:text).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:status).of_type(:integer).with_options(null: false, default: 0) }
  #it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  #
  ## table indexes
  #it { is_expected.to have_db_index(:atn) }
  #it { is_expected.to have_db_index(:person_id) }
  #it { is_expected.to have_db_index(:created_by) }
  #it { is_expected.to have_db_index(:updated_by) }
  #
  ## associations
  #it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:created_by) }
  #it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key(:updated_by) }
  #it { is_expected.to belong_to(:person).inverse_of(:dispositions) }
  #
  ## scopes
  #describe "scopes" do
  #  before(:context) do
  #    # create test records
  #  end
  #
  #  it "should return dispositions in id order using :by_id scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return only active dispositions using :active scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return only inactive dispositions using :final scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return dispositions where disposition is not blank using :with_disposition scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return a particular disposition using :with_disposition_id scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return dispositions with a particular atn using :with_atn scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return dispositions for a particular person using :with_person_id scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return dispositions with a particular status using :with_status scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_disposition scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_remarks scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_person when using :with_person_name scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_daterange when using :with_report_date scope" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## callbacks
  #
  #describe "before_validation" do
  #  describe ":strip_atn" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  #describe "before_save" do
  #  describe ":check_creator" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  ## validations
  #
  #it { is_expected.to validate_presence_of(:atn) }
  #it { is_expected.to validate_uniqueness_of(:atn) }
  #it { is_expected.to validate_person(:person_id) }
  #
  ## methods
  #it { is_expected.to have_defined_desc }
  #it { is_expected.to have_defined_base_perms }
  #
  #describe ".update_status" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".process_query" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#arrest_date" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#arrests" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#docket_charge_string" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#docket_charges" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#original_charge_string" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#original_charges" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#sentence_date" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#status_name" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#suggested_disposition" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#update_status" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
end
