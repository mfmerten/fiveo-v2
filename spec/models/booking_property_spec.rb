# == Schema Information
#
# Table name: booking_properties
#
#  id          :integer          not null, primary key
#  booking_id  :integer          not null
#  quantity    :integer          default(1), not null
#  description :string(255)      default(""), not null
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_properties_on_booking_id  (booking_id)
#
# Foreign Keys
#
#  booking_properties_booking_id_fk  (booking_id => bookings.id)
#

require 'rails_helper'

RSpec.describe BookingProperty do
  
  describe "Table Columns" do
    it { is_expected.to have_db_column(:booking_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:quantity).of_type(:integer).with_options(null: false, default: 1) }
    it { is_expected.to have_db_column(:description).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  end
  
  describe "Table Indices" do
    it { is_expected.to have_db_index(:booking_id) }
  end
  
  describe "Associations" do
    it { is_expected.to belong_to(:booking).inverse_of(:properties) }
  end
  
  describe "Validations" do
    it { is_expected.to validate_presence_of(:description) }
    it { is_expected.to validate_numericality_of(:quantity).only_integer }
  end
  
  describe "Class Methods" do
    it { is_expected.to have_defined_desc }
  end
  
  describe "Instance Methods" do
    describe "#to_s" do
      it "should return description and quantity as a string" do
        bp = build(:booking_property, description: "Test", quantity: 1)
        expect(bp.to_s).to eq("Test (1)")
      end
    end
  end
end
