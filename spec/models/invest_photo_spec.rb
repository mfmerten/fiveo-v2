# == Schema Information
#
# Table name: invest_photos
#
#  id                        :integer          not null, primary key
#  investigation_id          :integer          not null
#  invest_photo_file_name    :string(255)
#  invest_photo_content_type :string(255)
#  invest_photo_file_size    :integer
#  invest_photo_updated_at   :datetime
#  private                   :boolean          default(TRUE), not null
#  description               :string(255)      default(""), not null
#  date_taken                :date
#  location_taken            :string(255)      default(""), not null
#  taken_by                  :string(255)      default(""), not null
#  taken_by_unit             :string(255)      default(""), not null
#  taken_by_badge            :string(255)      default(""), not null
#  remarks                   :text             default(""), not null
#  created_by                :integer          default(1)
#  updated_by                :integer          default(1)
#  created_at                :datetime
#  updated_at                :datetime
#  owned_by                  :integer          default(1)
#
# Indexes
#
#  index_invest_photos_on_created_by        (created_by)
#  index_invest_photos_on_investigation_id  (investigation_id)
#  index_invest_photos_on_owned_by          (owned_by)
#  index_invest_photos_on_updated_by        (updated_by)
#
# Foreign Keys
#
#  invest_photos_created_by_fk        (created_by => users.id)
#  invest_photos_investigation_id_fk  (investigation_id => investigations.id)
#  invest_photos_owned_by_fk          (owned_by => users.id)
#  invest_photos_updated_by_fk        (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe InvestPhoto do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
