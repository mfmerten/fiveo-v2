# == Schema Information
#
# Table name: forums
#
#  id                :integer          not null, primary key
#  name              :string(255)      default(""), not null
#  description       :text             default(""), not null
#  forum_category_id :integer
#  views_count       :integer          default(0), not null
#  created_by        :integer          default(1)
#  updated_by        :integer          default(1)
#  created_at        :datetime
#  updated_at        :datetime
#
# Indexes
#
#  index_forums_on_created_by         (created_by)
#  index_forums_on_forum_category_id  (forum_category_id)
#  index_forums_on_updated_by         (updated_by)
#
# Foreign Keys
#
#  forums_created_by_fk         (created_by => users.id)
#  forums_forum_category_id_fk  (forum_category_id => forum_categories.id)
#  forums_updated_by_fk         (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Forum do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
