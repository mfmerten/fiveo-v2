# == Schema Information
#
# Table name: sentences
#
#  id                          :integer          not null, primary key
#  court_id                    :integer          not null
#  probation_id                :integer
#  da_charge_id                :integer          not null
#  doc                         :boolean          default(FALSE), not null
#  fines                       :decimal(12, 2)   default(0.0), not null
#  costs                       :decimal(12, 2)   default(0.0), not null
#  default_days                :integer          default(0), not null
#  default_months              :integer          default(0), not null
#  default_years               :integer          default(0), not null
#  pay_by_date                 :date
#  sentence_days               :integer          default(0), not null
#  sentence_months             :integer          default(0), not null
#  sentence_years              :integer          default(0), not null
#  suspended                   :boolean          default(FALSE), not null
#  suspended_except_days       :integer          default(0), not null
#  suspended_except_months     :integer          default(0), not null
#  suspended_except_years      :integer          default(0), not null
#  probation_days              :integer          default(0), not null
#  probation_months            :integer          default(0), not null
#  probation_years             :integer          default(0), not null
#  credit_time_served          :boolean          default(FALSE), not null
#  community_service_days      :integer          default(0), not null
#  substance_abuse_program     :boolean          default(FALSE), not null
#  driver_improvement          :boolean          default(FALSE), not null
#  probation_fees              :decimal(12, 2)   default(0.0), not null
#  idf_amount                  :decimal(12, 2)   default(0.0), not null
#  dare_amount                 :decimal(12, 2)   default(0.0), not null
#  restitution_amount          :decimal(12, 2)   default(0.0), not null
#  anger_management            :boolean          default(FALSE), not null
#  substance_abuse_treatment   :boolean          default(FALSE), not null
#  random_drug_screens         :boolean          default(FALSE), not null
#  no_victim_contact           :boolean          default(FALSE), not null
#  art_893                     :boolean          default(FALSE), not null
#  art_894                     :boolean          default(FALSE), not null
#  art_895                     :boolean          default(FALSE), not null
#  probation_reverts           :boolean          default(FALSE), not null
#  probation_revert_conditions :string(255)      default(""), not null
#  wo_benefit                  :boolean          default(FALSE), not null
#  sentence_consecutive        :boolean          default(FALSE), not null
#  sentence_notes              :string(255)      default(""), not null
#  fines_consecutive           :boolean          default(FALSE), not null
#  fines_notes                 :string(255)      default(""), not null
#  costs_consecutive           :boolean          default(FALSE), not null
#  costs_notes                 :string(255)      default(""), not null
#  notes                       :text             default(""), not null
#  remarks                     :text             default(""), not null
#  processed                   :boolean          default(FALSE), not null
#  created_by                  :integer          default(1)
#  updated_by                  :integer          default(1)
#  created_at                  :datetime
#  updated_at                  :datetime
#  parish_jail                 :boolean          default(FALSE), not null
#  conviction                  :string(255)      default(""), not null
#  conviction_count            :integer          default(1), not null
#  restitution_date            :date
#  legacy                      :boolean          default(FALSE), not null
#  process_status              :string(255)      default(""), not null
#  default_hours               :integer          default(0), not null
#  sentence_hours              :integer          default(0), not null
#  suspended_except_hours      :integer          default(0), not null
#  probation_hours             :integer          default(0), not null
#  delay_execution             :boolean          default(FALSE), not null
#  community_service_hours     :integer          default(0), not null
#  deny_goodtime               :boolean          default(FALSE), not null
#  sentence_life               :integer          default(0), not null
#  sentence_death              :boolean          default(FALSE), not null
#  suspended_except_life       :integer          default(0), not null
#  consecutive_type            :string(255)      default(""), not null
#  probation_type              :string(255)      default(""), not null
#  result                      :string(255)      default(""), not null
#  pay_during_prob             :boolean          default(FALSE), not null
#
# Indexes
#
#  index_sentences_on_charge_id     (da_charge_id)
#  index_sentences_on_court_id      (court_id)
#  index_sentences_on_created_by    (created_by)
#  index_sentences_on_probation_id  (probation_id)
#  index_sentences_on_updated_by    (updated_by)
#
# Foreign Keys
#
#  sentences_court_id_fk      (court_id => courts.id)
#  sentences_created_by_fk    (created_by => users.id)
#  sentences_da_charge_id_fk  (da_charge_id => da_charges.id)
#  sentences_probation_id_fk  (probation_id => probations.id)
#  sentences_updated_by_fk    (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Sentence do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
