# == Schema Information
#
# Table name: booking_logs
#
#  id                  :integer          not null, primary key
#  booking_id          :integer          not null
#  start_datetime      :datetime
#  stop_datetime       :datetime
#  start_officer       :string(255)      default(""), not null
#  start_officer_badge :string(255)      default(""), not null
#  start_officer_unit  :string(255)      default(""), not null
#  stop_officer        :string(255)      default(""), not null
#  stop_officer_badge  :string(255)      default(""), not null
#  stop_officer_unit   :string(255)      default(""), not null
#  created_at          :datetime
#  updated_at          :datetime
#
# Indexes
#
#  index_booking_logs_on_booking_id  (booking_id)
#
# Foreign Keys
#
#  booking_logs_booking_id_fk  (booking_id => bookings.id)
#

require 'rails_helper'

RSpec.describe BookingLog do
  
  describe "Table Columns" do
    it { is_expected.to have_db_column(:booking_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:start_datetime).of_type(:datetime) }
    it { is_expected.to have_db_column(:stop_datetime).of_type(:datetime) }
    it { is_expected.to have_db_column(:start_officer).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:start_officer_unit).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:start_officer_badge).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:stop_officer).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:stop_officer_unit).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:stop_officer_badge).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  end
  
  describe "Table Indices" do
    it { is_expected.to have_db_index(:booking_id) }
  end
  
  describe "Associations" do
    it { is_expected.to belong_to(:booking).inverse_of(:booking_logs) }
  end
  
  describe "Scopes" do
    describe ":on_or_after" do
      it "should return an ActiveRecord::Relation" do
        expect(BookingLog.on_or_after(Time.now)).to be_a(ActiveRecord::Relation)
      end
      
      it "should return BookingLog records that start on or after the Time specified" do
        bk = create(:booking, disable_timers: true)
        bl = create(:booking_log, :started, booking: bk)
        bl2 = create(:booking_log, start_datetime: Time.now - 1.hour, stop_datetime: Time.now - 30.minutes, booking: bk)
        expect(BookingLog.on_or_after(Time.now - 1.minute).pluck(:id)).to match_array([bl.id])
      end
      
      it "should raise exception if no time specified" do
        expect { BookingLog.on_or_after() }.to raise_error(ArgumentError)
      end
    end
  end

  describe "Class Methods" do
    it { is_expected.to have_defined_desc }
    it { is_expected.to have_defined_base_perms }
  end
  
  describe "Instance Methods" do
    describe "#hours_served" do
      it "should return 0 if either argument is not nil or a Time" do
        bk = create(:booking, disable_timers: true)
        bl = create(:booking_log, :stopped, booking: bk)
        expect(bl.hours_served(Date.today, nil)).to eq(0)
        expect(bl.hours_served(nil, Date.today)).to eq(0)
      end
      
      it "should return 0 if stop Time is less than log start Time" do
        bk = create(:booking, disable_timers: true)
        bl = create(:booking_log, :stopped, booking: bk)
        expect(bl.hours_served(Time.now - 2.days, Time.now - 1.day)).to eq(0)
      end
      
      it "should call AppUtils.hours_diff to perform time calculation" do
        allow(AppUtils).to receive(:hours_diff)
        bk = create(:booking, disable_timers: true)
        bl = create(:booking_log, :stopped, booking: bk)
        bl.hours_served(Time.now - 2.days, Time.now)
        expect(AppUtils).to have_received(:hours_diff).with(bl.stop_datetime, bl.start_datetime)
      end
    end
    
    describe "#minutes_served" do
      it "should return 0 if argument is not nil or a Time" do
        bk = create(:booking, disable_timers: true)
        bl = create(:booking_log, :stopped, booking: bk)
        expect(bl.minutes_served(Date.today)).to eq(0)
      end
      
      it "should return 0 if booking log has not been started yet" do
        bk = create(:booking, disable_timers: true)
        bl = create(:booking_log, booking: bk)
        expect(bl.minutes_served(Time.now - 1.day)).to eq(0)
      end
      
      it "should return 0 if argument is later than log stop time" do
        bk = create(:booking, disable_timers: true)
        bl = create(:booking_log, booking: bk, start_datetime: Time.now - 1.day, stop_datetime: Time.now - 30.minutes)
        expect(bl.minutes_served(Time.now)).to eq(0)
      end
      
      it "should call AppUtils.minutes_diff to perform time calculation" do
        allow(AppUtils).to receive(:minutes_diff)
        bk = create(:booking, disable_timers: true)
        bl = create(:booking_log, :stopped, booking: bk)
        since_time = Time.now - 1.minute
        bl.minutes_served(since_time)
        expect(AppUtils).to have_received(:minutes_diff).with(bl.stop_datetime, since_time)
      end
    end
  end
end
