# == Schema Information
#
# Table name: message_logs
#
#  id               :integer          not null, primary key
#  receiver_deleted :boolean          default(FALSE), not null
#  receiver_purged  :boolean          default(FALSE), not null
#  sender_deleted   :boolean          default(FALSE), not null
#  sender_purged    :boolean          default(FALSE), not null
#  read_at          :datetime
#  receiver_id      :integer          not null
#  sender_id        :integer          not null
#  subject          :string(255)      default(""), not null
#  body             :text             default(""), not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_message_logs_on_receiver_id  (receiver_id)
#  index_message_logs_on_sender_id    (sender_id)
#
# Foreign Keys
#
#  message_logs_receiver_id_fk  (receiver_id => users.id)
#  message_logs_sender_id_fk    (sender_id => users.id)
#

require 'rails_helper'

RSpec.describe MessageLog do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
