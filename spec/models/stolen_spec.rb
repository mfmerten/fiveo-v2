# == Schema Information
#
# Table name: stolens
#
#  id               :integer          not null, primary key
#  case_no          :string(255)      default(""), not null
#  person_id        :integer          not null
#  stolen_date      :date
#  model_no         :string(255)      default(""), not null
#  serial_no        :string(255)      default(""), not null
#  item_value       :decimal(12, 2)   default(0.0), not null
#  item             :string(255)      default(""), not null
#  item_description :text             default(""), not null
#  recovered_date   :date
#  returned_date    :date
#  remarks          :text             default(""), not null
#  created_by       :integer          default(1)
#  updated_by       :integer          default(1)
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_stolens_on_case_no     (case_no)
#  index_stolens_on_created_by  (created_by)
#  index_stolens_on_person_id   (person_id)
#  index_stolens_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  stolens_created_by_fk  (created_by => users.id)
#  stolens_person_id_fk   (person_id => people.id)
#  stolens_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Stolen do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
