# == Schema Information
#
# Table name: forbids
#
#  id                      :integer          not null, primary key
#  call_id                 :integer
#  case_no                 :string(255)      default(""), not null
#  forbidden_id            :integer
#  request_date            :date
#  receiving_officer       :string(255)      default(""), not null
#  receiving_officer_unit  :string(255)      default(""), not null
#  signed_date             :date
#  serving_officer         :string(255)      default(""), not null
#  serving_officer_unit    :string(255)      default(""), not null
#  recall_date             :date
#  details                 :text             default(""), not null
#  remarks                 :text             default(""), not null
#  created_by              :integer          default(1)
#  updated_by              :integer          default(1)
#  created_at              :datetime
#  updated_at              :datetime
#  receiving_officer_badge :string(255)      default(""), not null
#  serving_officer_badge   :string(255)      default(""), not null
#  complainant             :string(255)      default(""), not null
#  comp_street             :string(255)      default(""), not null
#  comp_city_state_zip     :string(255)      default(""), not null
#  comp_phone              :string(255)      default(""), not null
#  family_name             :string(255)      default(""), not null
#  given_name              :string(255)      default(""), not null
#  suffix                  :string(255)      default(""), not null
#  forbid_street           :string(255)      default(""), not null
#  forbid_city_state_zip   :string(255)      default(""), not null
#  forbid_phone            :string(255)      default(""), not null
#  legacy                  :boolean          default(FALSE), not null
#
# Indexes
#
#  index_forbids_on_call_id                     (call_id)
#  index_forbids_on_case_no                     (case_no)
#  index_forbids_on_created_by                  (created_by)
#  index_forbids_on_family_name_and_given_name  (family_name,given_name)
#  index_forbids_on_forbidden_id                (forbidden_id)
#  index_forbids_on_updated_by                  (updated_by)
#
# Foreign Keys
#
#  forbids_call_id_fk       (call_id => calls.id)
#  forbids_created_by_fk    (created_by => users.id)
#  forbids_forbidden_id_fk  (forbidden_id => people.id)
#  forbids_updated_by_fk    (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Forbid do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
