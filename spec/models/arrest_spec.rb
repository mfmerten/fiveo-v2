# == Schema Information
#
# Table name: arrests
#
#  id                     :integer          not null, primary key
#  person_id              :integer          not null
#  case_no                :string(255)      default(""), not null
#  agency                 :string(255)      default(""), not null
#  ward                   :string(255)      default(""), not null
#  district               :string(255)      default(""), not null
#  victim_notify          :boolean          default(FALSE), not null
#  dwi_test_officer       :string(255)      default(""), not null
#  dwi_test_officer_unit  :string(255)      default(""), not null
#  dwi_test_results       :string(255)      default(""), not null
#  attorney               :string(255)      default(""), not null
#  condition_of_bond      :string(255)      default(""), not null
#  remarks                :text             default(""), not null
#  created_by             :integer          default(1)
#  updated_by             :integer          default(1)
#  created_at             :datetime
#  updated_at             :datetime
#  bond_amt               :decimal(12, 2)   default(0.0), not null
#  bondable               :boolean          default(FALSE), not null
#  dwi_test_officer_badge :string(255)      default(""), not null
#  legacy                 :boolean          default(FALSE), not null
#  atn                    :string(255)      default(""), not null
#  arrest_datetime        :datetime
#  hour_72_datetime       :datetime
#  hour_72_judge          :string(255)      default(""), not null
#  zone                   :string(255)      default(""), not null
#  warrants_count         :integer          default(0), not null
#  arrest_officers_count  :integer          default(0), not null
#  disposition_id         :integer
#
# Indexes
#
#  index_arrests_on_atn             (atn)
#  index_arrests_on_case_no         (case_no)
#  index_arrests_on_created_by      (created_by)
#  index_arrests_on_disposition_id  (disposition_id)
#  index_arrests_on_person_id       (person_id)
#  index_arrests_on_updated_by      (updated_by)
#
# Foreign Keys
#
#  arrests_created_by_fk  (created_by => users.id)
#  arrests_person_id_fk   (person_id => people.id)
#  arrests_updated_by_fk  (updated_by => users.id)
#  fk_rails_1f65d0c1f6    (disposition_id => dispositions.id)
#

require 'rails_helper'

RSpec.describe Arrest do
  # set up some standardized Times that will be useful when creating and clearing
  # holds
  let(:t) { Time.now }
  let(:t1) { t - 12.hours }
  let(:t2) { t - 11.hours }
  let(:t3) { t - 10.hours }
  let(:t4) { t - 9.hours }
  let(:t5) { t - 8.hours }
  let(:t6) { t - 7.hours }
  let(:t7) { t - 6.hours }
  
  
  describe "Table Columns" do
    it { is_expected.to have_db_column(:person_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:case_no).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:agency).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:ward).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:district).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:zone).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:victim_notify).of_type(:boolean).with_options(null: false, default: false) }
    it { is_expected.to have_db_column(:dwi_test_officer).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:dwi_test_officer_unit).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:dwi_test_officer_badge).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:dwi_test_results).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:attorney).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:condition_of_bond).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:remarks).of_type(:text).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
    it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_options(default: 1) }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:bond_amt).of_type(:decimal).with_options(scale: 2, precision: 12, null: false, default: 0.0) }
    it { is_expected.to have_db_column(:bondable).of_type(:boolean).with_options(null: false, default: false) }
    it { is_expected.to have_db_column(:legacy).of_type(:boolean).with_options(null: false, default: false) }
    it { is_expected.to have_db_column(:atn).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:arrest_datetime).of_type(:datetime) }
    it { is_expected.to have_db_column(:hour_72_datetime).of_type(:datetime) }
    it { is_expected.to have_db_column(:hour_72_judge).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:warrants_count).of_type(:integer).with_options(null: false, default: 0) }
    it { is_expected.to have_db_column(:arrest_officers_count).of_type(:integer).with_options(null: false, default: 0) }
    it { is_expected.to have_db_column(:disposition_id).of_type(:integer) }
  end
  
  describe "Table Indices" do
    it { is_expected.to have_db_index(:atn) }
    it { is_expected.to have_db_index(:case_no) }
    it { is_expected.to have_db_index(:created_by) }
    it { is_expected.to have_db_index(:updated_by) }
    it { is_expected.to have_db_index(:person_id) }
    it { is_expected.to have_db_index(:disposition_id)}
  end
  
  describe "Associations" do
    it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:created_by) }
    it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key(:updated_by) }
    it { is_expected.to belong_to(:person).inverse_of(:arrests) }
    it { is_expected.to belong_to(:disposition).inverse_of(:arrests)}
    it { is_expected.to have_many(:offense_arrests).dependent(:destroy) }
    it { is_expected.to have_many(:offenses).through(:offense_arrests) }
    it { is_expected.to have_many(:warrants).dependent(:nullify) }
    it { is_expected.to have_many(:holds).dependent(:destroy).autosave(true) }
    it { is_expected.to have_many(:bookings).order(:id).through(:holds) }
    it { is_expected.to have_many(:bonds).dependent(:nullify) }
    it { is_expected.to have_many(:revocations).dependent(:nullify) }
    it { is_expected.to have_many(:da_charges).dependent(:nullify) }
    it { is_expected.to have_many(:da_charge_dockets).through(:da_charges).class_name('Docket').source(:docket) }
    it { is_expected.to have_many(:charges) }
    it { is_expected.to have_many(:charge_da_charges).through(:charges).class_name('DaCharge').source(:da_charges) }
    it { is_expected.to have_many(:charge_dockets).through(:charge_da_charges).class_name('Docket').source(:docket) }
    it { is_expected.to have_many(:officers).class_name('ArrestOfficer').dependent(:destroy) }
    it { is_expected.to have_many(:arrest_charges).conditions(warrant_id: nil).class_name('Charge') }
    it { is_expected.to have_many(:invest_arrests).dependent(:destroy) }
    it { is_expected.to have_many(:investigations).through(:invest_arrests) }
    it { is_expected.to have_many(:crimes).through(:invest_arrests).source(:crimes) }
    it { is_expected.to have_many(:criminals).through(:invest_arrests).source(:criminals) }
    it { is_expected.to have_many(:reports).through(:invest_arrests).source(:reports) }
    it { is_expected.to have_many(:city_charges).conditions(charge_type: 'City').class_name('Charge') }
    it { is_expected.to have_many(:district_charges).conditions(charge_type: 'District').class_name('Charge') }
    it { is_expected.to have_many(:other_charges).conditions(charge_type: 'Other').class_name('Charge') }
    it { is_expected.to have_many(:untyped_charges).conditions(charge_type: '').class_name('Charge') }
  end
  
  describe "Attributes" do
    it { is_expected.to accept_nested_attributes_for(:officers).allow_destroy(true) }
    it { is_expected.to accept_nested_attributes_for(:arrest_charges).allow_destroy(true) }
    it { is_expected.to have_attr_accessor(:parish_probation) }
    it { is_expected.to have_attr_accessor(:state_probation) }
    it { is_expected.to have_attr_accessor(:dwi_test_officer_id) }
    it { is_expected.to have_attr_accessor(:agency_id) }
    it { is_expected.to have_attr_accessor(:assigned_warrants) }
  end
  
  describe "Scopes" do
    let!(:p1) { create(:person) }
    let!(:p2) { create(:person) }
    let!(:o1) { create(:offense) }
    let!(:o2) { create(:offense) }
    let!(:a1) { create(:arrest, arrest_datetime: Time.now - 1.hour, person: p1, ward: '1', district: '1', zone: '1') }
    let!(:a2) { create(:arrest, arrest_datetime: Time.now - 5.minutes, person: p2, ward: '2', district: '2', zone: '2') }
    let!(:a3) { create(:arrest) }
    
    describe ":by_reverse_id" do
      it "should return arrests in reverse id order" do
        query = Arrest.by_reverse_id.pluck(:id)
        expect(query[0]).to be > query[1]
        expect(query[1]).to be > query[2]
      end
    end
    
    describe ":on_or_before" do
      it "should return arrests on or before a specified datetime" do
        query = Arrest.on_or_before(Time.now - 10.minutes).pluck(:id)
        expect(query).to include(a1.id)
        expect(query).not_to include(a2.id)
      end
    end
    
    describe ":on_or_after" do
      it "should return arrests on or after a specified datetime" do
        query = Arrest.on_or_after(Time.now - 10.minutes).pluck(:id)
        expect(query).to include(a2.id)
        expect(query).not_to include(a1.id)
      end
    end
    
    describe ":between" do
      it "should return arrests between two datetimes" do
        query = Arrest.between(Time.now - 20.minutes, Time.now).pluck(:id)
        expect(query).to include(a2.id)
        expect(query).not_to include(a1.id)
      end
    end
    
    describe ":with_arrest_id" do
      it "should return an arrest specified by id" do
        expect(Arrest.with_arrest_id(a1.id).pluck(:id)).to eq([a1.id])
      end
    end
    
    describe ":with_person_id" do
      it "should return arrests for the specified person id" do
        expect(Arrest.with_person_id(p1.id).pluck(:id)).to eq([a1.id])
      end
    end
    
    describe ":with_offense_id" do
      it "should return arrests linked to the specified offense id" do
        a1.offenses << o1
        a2.offenses << o2
        expect(Arrest.with_offense_id(o1.id).pluck(:id)).to eq([a1.id])
      end
    end
    
    describe ":with_case_no" do
      it "should return arrests for the specified case number" do
        expect(Arrest.with_case_no(a2.case_no).pluck(:id)).to eq([a2.id])
      end
    end
    
    describe ":with_agency" do
      it "should call AppUtils.like to return arrests with agency text" do
        allow(AppUtils).to receive(:like)
        Arrest.with_agency("SPSO")
        expect(AppUtils).to have_received(:like).with(Arrest, :agency, "SPSO")
      end
    end
    
    describe ":with_dwi_test_results" do
      it "should call AppUtils.like to return arrests with DWI test results text" do
        allow(AppUtils).to receive(:like)
        Arrest.with_dwi_test_results("0.1")
        expect(AppUtils).to have_received(:like).with(Arrest, :dwi_test_results, "0.1")
      end
    end
    
    describe ":with_warrant_no" do
      it "should call AppUtils.like to return arrests with warrant number text" do
        allow(AppUtils).to receive(:like)
        Arrest.with_warrant_no("1234")
        expect(AppUtils).to have_received(:like).with(Arrest, :warrant_no, "1234", association: :warrants)
      end
    end
    
    describe ":with_charge" do
      it "should call AppUtils.like to return arrests with charge text" do
        allow(AppUtils).to receive(:like)
        Arrest.with_charge("burglary")
        expect(AppUtils).to have_received(:like).with(Arrest, :charge, "burglary", association: :charges)
      end
    end
    
    describe ":with_ward" do
      it "should return arrests for the specified ward" do
        expect(Arrest.with_ward('1').pluck(:id)).to eq([a1.id])
      end
    end
    
    describe ":with_district" do
      it "should return arrests for the specified district" do
        expect(Arrest.with_district('2').pluck(:id)).to eq([a2.id])
      end
    end
    
    describe ":with_zone" do
      it "should return arrests for the specified zone" do
        expect(Arrest.with_zone('1').pluck(:id)).to eq([a1.id])
      end
    end
    
    describe ":with_victim_notify" do
      it "should call AppUtils.for_bool to find arrests by victim notify flag" do
        allow(AppUtils).to receive(:for_bool)
        Arrest.with_victim_notify(true)
        expect(AppUtils).to have_received(:for_bool).with(Arrest, :victim_notify, true)
      end
    end
    
    describe ":with_attorney" do
      it "should call AppUtils.like to find arrests with attorney text" do
        allow(AppUtils).to receive(:like)
        Arrest.with_attorney("Merten")
        expect(AppUtils).to have_received(:like).with(Arrest, :attorney, "Merten")
      end
    end
    
    describe ":with_remarks" do
      it "should call AppUtils.like to find arrests with remarks text" do
        allow(AppUtils).to receive(:like)
        Arrest.with_remarks("Test")
        expect(AppUtils).to have_received(:like).with(Arrest, :remarks, "Test")
      end
    end
    
    describe ":with_arrest_date" do
      it "should call AppUtils.for_daterange to find arrests by arrest date" do
        allow(AppUtils).to receive(:for_daterange)
        Arrest.with_arrest_date((Date.today - 1.day).to_s, Date.today.to_s)
        expect(AppUtils).to have_received(:for_daterange).with(Arrest, :arrest_datetime, start: (Date.today - 1.day).to_s, stop: Date.today.to_s)
      end
    end
    
    describe ":with_officer" do
      it "should call AppUtils.for_officer to find arrests by arrest officer" do
        allow(AppUtils).to receive(:for_officer)
        Arrest.with_officer("Merten","TBPD1","1234")
        expect(AppUtils).to have_received(:for_officer).with(Arrest, :officer, "Merten", "TBPD1", "1234", association: :officers )
      end
    end
    
    describe ":with_dwi_test_officer" do
      it "should call AppUtils.for_officer to find arrests by DWI testing officer" do
        allow(AppUtils).to receive(:for_officer)
        Arrest.with_dwi_test_officer("Merten","TBPD1","1234")
        expect(AppUtils).to have_received(:for_officer).with(Arrest, :dwi_test_officer, "Merten", "TBPD1", "1234")
      end
    end
    
    describe ":with_person_name" do
      it "should call AppUtils.for_person to find arrests by person name" do
        allow(AppUtils).to receive(:for_person)
        Arrest.with_person_name("Merten")
        expect(AppUtils).to have_received(:for_person).with(Arrest, "Merten", association: :person)
      end
    end
  end
  
  # Callbacks
  
  describe "before_validation" do
  
    describe ":adjust_case_no" do
      it "should call adjust_case_no method" do
        a1 = build(:arrest)
        allow(a1).to receive(:adjust_case_no)
        a1.valid?
        expect(a1).to have_received(:adjust_case_no).with(no_args)
      end
    end
      
    describe ":adjust_atn" do
      it "should call adjust_atn method" do
        a1 = build(:arrest)
        allow(a1).to receive(:adjust_atn)
        a1.valid?
        expect(a1).to have_received(:adjust_atn).with(no_args)
      end
    end
      
    describe ":lookup_contacts" do
      it "should call lookup_contacts method" do
        a1 = build(:arrest)
        allow(a1).to receive(:lookup_contacts)
        a1.valid?
        expect(a1).to have_received(:lookup_contacts).with(no_args)
      end
    end
      
    describe ":check_72_hour" do
      it "should call check_72_hour method" do
        a1 = build(:arrest)
        allow(a1).to receive(:check_72_hour)
        a1.valid?
        expect(a1).to have_received(:check_72_hour).with(no_args)
      end
    end
  end

  describe "after_validation" do
      
    describe ":check_creator" do
      it "should call check_creator method" do
        a1 = build(:arrest)
        allow(a1).to receive(:check_creator)
        a1.valid?
        expect(a1).to have_received(:check_creator).with(no_args)
      end
    end
  end
  
  describe "after_save" do
      
    describe ":process_warrants" do
      it "should call process_warrants" do
        a1 = create(:arrest)
        allow(a1).to receive(:process_warrants).and_return(false)
        a1.assigned_warrants = [1]
        a1.save(validate: false)
        expect(a1).to have_received(:process_warrants).with(no_args)
      end
    end
      
    describe ":check_and_generate_case" do
      it "should call check_and_generate_case method" do
        a1 = create(:arrest)
        allow(a1).to receive(:check_and_generate_case)
        a1.save(validate: false)
        expect(a1).to have_received(:check_and_generate_case).with(no_args)
      end
    end
      
    describe ":process_72_hour" do
      it "should call process_72_hour method" do
        a1 = create(:arrest)
        allow(a1).to receive(:process_72_hour)
        a1.save(validate: false)
        expect(a1).to have_received(:process_72_hour).with(no_args)
      end
    end
      
    describe ":process_holds" do
      it "should call process_holds method" do
        a1 = create(:arrest)
        allow(a1).to receive(:process_holds)
        a1.save(validate: false)
        expect(a1).to have_received(:process_holds).with(no_args)
      end
    end
      
    describe ":process_afis" do
      it "should call process_afis method" do
        a1 = create(:arrest)
        allow(a1).to receive(:process_afis)
        a1.save(validate: false)
        expect(a1).to have_received(:process_afis).with(no_args)
      end
    end
  end
  
  describe "after_create" do
      
    describe ":check_probation" do
      it "should call check_probation method" do
        a1 = build(:arrest)
        allow(a1).to receive(:check_probation)
        a1.save(validate: false)
        expect(a1).to have_received(:check_probation).with(no_args)
      end
        
      it "should not call check_probation method on update" do
        a1 = create(:arrest)
        allow(a1).to receive(:check_probation)
        a1.update(agency: "None")
        expect(a1).not_to have_received(:check_probation)
      end
    end
      
    describe ":send_messages" do
      it "should call send_messages method" do
        a1 = build(:arrest)
        allow(a1).to receive(:send_messages)
        a1.save(validate: false)
        expect(a1).to have_received(:send_messages).with(no_args)
      end
      
      it "should not call send_messages method on update" do
        a1 = create(:arrest)
        allow(a1).to receive(:send_messages)
        a1.update(agency: 'None')
        expect(a1).not_to have_received(:send_messages)
      end
    end
  end
    
  describe "before_destroy" do

    describe ":check_destroyable" do
      it "should call check_destroyable method" do
        a1 = create(:arrest)
        allow(a1).to receive(:check_destroyable)
        a1.destroy
        expect(a1).to have_received(:check_destroyable).with(no_args)
      end
    end
    
    describe ":process_dependents" do
      it "should call process_dependents method" do
        a1 = create(:arrest)
        allow(a1).to receive(:process_dependents)
        a1.destroy
        expect(a1).to have_received(:process_dependents).with(no_args)
      end
    end
  end
  
  describe "Validations" do
    
    describe ":officers" do
      it { is_expected.to validate_associated(:officers) }
    end
    
    describe ":charges" do
      it { is_expected.to validate_associated(:charges) }
    end
    
    describe ":agency" do
      it { is_expected.to validate_presence_of(:agency) }
    end
    
    describe ":arrest_datetime" do
      it { is_expected.to validate_datetime(:arrest_datetime) }
    end
    
    describe ":hour_72_datetime" do
      it { is_expected.to validate_datetime(:hour_72_datetime).allow_blank }
    end
    
    describe ":bond_amt" do
      subject { build(:arrest, hour_72_datetime: Time.now, hour_72_judge: "Smith") }
      it { is_expected.to validate_numericality_of(:bond_amt).is_greater_than_or_equal_to(0).is_less_than(10 ** 10) }
    end
    
    describe ":person_id" do
      it { is_expected.to validate_person(:person_id) }
    end
  end
  
  describe "Class Methods" do
    describe ".desc" do
      it { is_expected.to have_defined_desc }
    end
    
    describe ".base_perms" do
      it { is_expected.to have_defined_base_perms }
    end
    
    describe ".arrest_summary" do
      let!(:c1) { create(:contact, sort_name: "SPSO", business: true, officer: true) }
      let!(:c2) { create(:contact, sort_name: "Merten", officer: true, agency: c1) }
      let!(:c3) { create(:contact, sort_name: "Smith", officer: true, agency: c1) }
      let!(:c4) { create(:contact, sort_name: "Jones", officer: true) }
      let!(:a1) { create(:arrest) }
      let!(:a2) { create(:arrest) }
      let!(:a3) { create(:arrest) }
      let!(:a4) { create(:arrest) }
      let!(:a5) { create(:arrest, arrest_datetime: Time.now - 2.days) }
      let!(:o1) { create(:arrest_officer, officer_id: c2.id, arrest: a1) }
      let!(:o2) { create(:arrest_officer, officer_id: c3.id, arrest: a2) }
      let!(:o3) { create(:arrest_officer, officer_id: c2.id, arrest: a3) }
      let!(:o4) { create(:arrest_officer, officer_id: c4.id, arrest: a3) }
      let!(:o5) { create(:arrest_officer, officer_id: c4.id, arrest: a4) }
      
      it "should return arrests for officers from an agency when given an agency_name" do
        expect(Arrest.arrest_summary(Time.now - 1.hour, Time.now, "SPSO").pluck(:id)).to match_array([a1.id,a2.id,a3.id])
      end
      
      it "should return return all arrests during given period if agency_name is blank" do
        expect(Arrest.arrest_summary(Time.now - 1.hour, Time.now).pluck(:id)).to match_array([a1.id,a2.id,a3.id,a4.id])
      end
    end
    
    describe ".arrests_by_officer" do
      let!(:c1) { create(:contact, sort_name: "Merten", unit: "TBPD1", badge_no: "1234", officer: true) }
      let!(:c2) { create(:contact, sort_name: "Smith", officer: true) }
      let!(:c3) { create(:contact, sort_name: "Jones", officer: true) }
      let!(:a1) { create(:arrest) }
      let!(:a2) { create(:arrest) }
      let!(:a3) { create(:arrest) }
      let!(:a4) { create(:arrest) }
      let!(:a5) { create(:arrest, arrest_datetime: Time.now - 2.days) }
      let!(:o1) { create(:arrest_officer, officer_id: c1.id, arrest: a1) }
      let!(:o2) { create(:arrest_officer, officer_id: c2.id, arrest: a2) }
      let!(:o3) { create(:arrest_officer, officer: "Merten", officer_unit: "TBPD1", arrest: a3) }
      let!(:o4) { create(:arrest_officer, officer_id: c3.id, arrest: a3) }
      let!(:o5) { create(:arrest_officer, officer_id: c3.id, arrest: a4) }
      let!(:o6) { create(:arrest_officer, officer_id: c2.id, arrest: a5) }
      
      it "should return arrests for a given officer name without dates given" do
        expect(Arrest.arrests_by_officer("Smith", "").pluck(:id)).to match_array([a2.id, a5.id])
      end
      
      it "should return arrests for a given officer name within dates given" do
        expect(Arrest.arrests_by_officer("Smith", "", start: Time.now, stop: Time.now).pluck(:id)).to match_array([a2.id])
      end
      
      it "should match officers by name, unit and badge when badge is given" do
        expect(Arrest.arrests_by_officer("Merten", "TBPD1", "1234").pluck(:id)).to match_array([a1.id])
      end
    end
    
    describe ".hours_served" do
      it "should calculate total hours served for an arrest" do
        # I don't know of any way to avoid this complexity and actually test the method
        #   at this time.
        
        # intercept Booking.update_sentences_by_hour method so that it doesn't introduce wierd errors
        #   if this happens to cross an hour boundary and cron triggers an update
        allow(Booking).to receive(:update_sentences_by_hour).and_return(true)
        
        # start with the person
        @person = create(:person, family_name: "Smith", given_name: "John")
        # he is booked into jail on jan 1 2000 noon for disturbing the peace
        @jail = create(:jail, name: "Toledo Bend PD", acronym: "TBPD")
        @officer = create(:contact, sort_name: "Merten", unit: "TBPD1", officer: true)
        @booking1 = create(:booking, booking_datetime: "1/1/2000 12:00", person: @person, booking_officer_id: @officer.id, release_officer_id: @officer.id, release_datetime: "1/2/2000 15:00", released_because: 'Bonded')
        # charge is added
        @arrest1 = create(:arrest, arrest_datetime: "1/1/2000 12:00", person: @person, agency: "TBPD")
        @charge1 = create(:charge, charge: "Disturbing The Peace", charge_type: 'District', arrest: @arrest1)
        # add a 72 Hour hold that was cleared on 1/2/2000 12:00 - elapsed time 24 hours
        create(:hold, hold_datetime: "1/1/2000 12:00", hold_by_id: @officer.id, hold_type: "72 Hour", arrest: @arrest1, booking: @booking1, cleared_datetime: "1/2/2000 12:00", cleared_by_id: @officer.id, cleared_because: "72 Hour", explanation: "Per Judge", hours_served: 24, hours_total: 24)
        # add a bondable hold on 1/2/2000 12:00 that was cleared on 1/2/2000 15:00
        #   - elapsed time 3 Hours
        create(:hold, hold_datetime: "1/2/2000 12:00", hold_by_id: @officer.id, hold_type: "Bondable", arrest: @arrest1, booking: @booking1, cleared_datetime: "1/2/2000 15:00", cleared_by_id: @officer.id, cleared_because: "Bonded", explanation: "Bonded", hours_served: 3, hours_total: 3)
        # so far, 27 Hours
        # person goes to court on charges and is sentenced to 48 hours in parish jail
        # I'm skipping the docket/court/sentence here for simplicity (thats a joke)
        # booked 2/1/2000 12:00 - released 2/3/2000 12:00 - elapsed time 48 Hours
        @booking2 = create(:booking, booking_datetime: "2/1/2000 12:00", person: @person, booking_officer_id: @officer.id, release_officer_id: @officer.id, release_datetime: "2/3/2000 12:00", released_because: 'Time Served')
        # add sentence hold for that entire period
        create(:hold, hold_datetime: "2/1/2000 12:00", hold_by_id: @officer.id, hold_type: "Serving Sentence", arrest: @arrest1, booking: @booking2, cleared_datetime: "2/3/2000 12:00", cleared_by_id: @officer.id, cleared_because: "Sentence Served", explanation: "served sentence", hours_served: 48, hours_total: 48)
        # add one more hold for a work release (8 hours) on the 2nd - should exclude 8 hours
        create(:hold, hold_datetime: "2/2/2000 08:00", hold_by_id: @officer.id, hold_type: "Temporary Release", booking: @booking2, cleared_datetime: "2/2/2000 16:00", cleared_by_id: @officer.id, cleared_because: "Returned", explanation: 'returned', hours_served: 8, hours_total: 8)
        # should total to 67 hours excluding the temp release time
        expect(Arrest.hours_served(@arrest1.id)).to eq(67)
      end
    end
    
    describe ".process_query" do
      before(:example) do
        allow(Arrest).to receive(:with_arrest_id).and_return(Arrest.none)
        allow(Arrest).to receive(:with_person_id).and_return(Arrest.none)
        allow(Arrest).to receive(:with_offense_id).and_return(Arrest.none)
        allow(Arrest).to receive(:with_case_no).and_return(Arrest.none)
        allow(Arrest).to receive(:with_agency).and_return(Arrest.none)
        allow(Arrest).to receive(:with_dwi_test_results).and_return(Arrest.none)
        allow(Arrest).to receive(:with_warrant_no).and_return(Arrest.none)
        allow(Arrest).to receive(:with_charge).and_return(Arrest.none)
        allow(Arrest).to receive(:with_ward).and_return(Arrest.none)
        allow(Arrest).to receive(:with_district).and_return(Arrest.none)
        allow(Arrest).to receive(:with_zone).and_return(Arrest.none)
        allow(Arrest).to receive(:with_victim_notify).and_return(Arrest.none)
        allow(Arrest).to receive(:with_attorney).and_return(Arrest.none)
        allow(Arrest).to receive(:with_remarks).and_return(Arrest.none)
        allow(Arrest).to receive(:with_arrest_date).and_return(Arrest.none)
        allow(Arrest).to receive(:with_officer).and_return(Arrest.none)
        allow(Arrest).to receive(:with_dwi_test_officer).and_return(Arrest.none)
        allow(Arrest).to receive(:with_person_name).and_return(Arrest.none)
      end
      
      let(:full_query) {
        {
          id: '1',
          person_id: '2',
          name: 'Merten',
          offense_id: "3",
          case_no: '4',
          arrest_date_from: '01/01/2015',
          arrest_date_to: '01/02/2015',
          agency: 'TBPD',
          officer: 'Smith',
          officer_unit: 'TBPD1',
          officer_badge: '5',
          dwi_test_officer: "Jones",
          dwi_test_officer_unit: 'TBPD2',
          dwi_test_officer_badge: '6',
          dwi_test_results: '0.23',
          warrant_no: '7',
          charge: 'Burglary',
          ward: '8',
          district: '9',
          zone: '10',
          victim_notify: '1',
          attorney: 'Shyster',
          remarks: 'Test Data'
        }
      }
      
      let(:empty_query) {
        {
          id: '',
          person_id: '',
          name: '',
          offense_id: "",
          case_no: '',
          arrest_date_from: '',
          arrest_date_to: '',
          agency: '',
          officer: '',
          officer_unit: '',
          officer_badge: '',
          dwi_test_officer: "",
          dwi_test_officer_unit: '',
          dwi_test_officer_badge: '',
          dwi_test_results: '',
          warrant_no: '',
          charge: '',
          ward: '',
          district: '',
          zone: '',
          victim_notify: '',
          attorney: '',
          remarks: ''
        }
      }
      
      it "should return empty relation when called with anything other than a Hash" do
        expect(Arrest.process_query(Date)).to be_a(ActiveRecord::Relation)
        expect(Arrest.process_query(Date)).to eq(Arrest.none)
      end
      
      it "should execute appropriate scopes when given query hash" do
        Arrest.process_query(full_query)
        expect(Arrest).to have_received(:with_arrest_id).with('1')
        expect(Arrest).to have_received(:with_person_id).with('2')
        expect(Arrest).to have_received(:with_offense_id).with('3')
        expect(Arrest).to have_received(:with_case_no).with('4')
        expect(Arrest).to have_received(:with_agency).with('TBPD')
        expect(Arrest).to have_received(:with_dwi_test_results).with('0.23')
        expect(Arrest).to have_received(:with_warrant_no).with('7')
        expect(Arrest).to have_received(:with_charge).with('Burglary')
        expect(Arrest).to have_received(:with_ward).with('8')
        expect(Arrest).to have_received(:with_district).with('9')
        expect(Arrest).to have_received(:with_zone).with('10')
        expect(Arrest).to have_received(:with_victim_notify).with('1')
        expect(Arrest).to have_received(:with_attorney).with('Shyster')
        expect(Arrest).to have_received(:with_remarks).with('Test Data')
        expect(Arrest).to have_received(:with_arrest_date).with('01/01/2015', '01/02/2015')
        expect(Arrest).to have_received(:with_officer).with('Smith','TBPD1','5')
        expect(Arrest).to have_received(:with_dwi_test_officer).with('Jones','TBPD2','6')
        expect(Arrest).to have_received(:with_person_name).with('Merten')
      end
      
      it "should not call any scopes when given empty query" do
        Absent.process_query(empty_query)
        expect(Arrest).not_to have_received(:with_arrest_id)
        expect(Arrest).not_to have_received(:with_person_id)
        expect(Arrest).not_to have_received(:with_offense_id)
        expect(Arrest).not_to have_received(:with_case_no)
        expect(Arrest).not_to have_received(:with_agency)
        expect(Arrest).not_to have_received(:with_dwi_test_results)
        expect(Arrest).not_to have_received(:with_warrant_no)
        expect(Arrest).not_to have_received(:with_charge)
        expect(Arrest).not_to have_received(:with_ward)
        expect(Arrest).not_to have_received(:with_district)
        expect(Arrest).not_to have_received(:with_zone)
        expect(Arrest).not_to have_received(:with_victim_notify)
        expect(Arrest).not_to have_received(:with_attorney)
        expect(Arrest).not_to have_received(:with_remarks)
        expect(Arrest).not_to have_received(:with_arrest_date)
        expect(Arrest).not_to have_received(:with_officer)
        expect(Arrest).not_to have_received(:with_dwi_test_officer)
        expect(Arrest).not_to have_received(:with_person_name)
      end
    end
  end
  
  describe "Instance Methods" do
    
    describe "#booking" do
      it "should return nil if no bookings linked to arrest" do
        a1 = create(:arrest)
        expect(a1.booking).to be_nil
      end
      
      it "should return latest booking linked to arrest" do
        a1 = create(:arrest)
        h1 = create(:hold, arrest: a1)
        h2 = create(:hold, arrest: a1)
        expect(a1.booking).to eq(h2.booking)
      end
    end
    
    describe "#docket_charges" do
      let!(:arr1) { create(:arrest) }
      let!(:arr2) { create(:arrest) }
      let!(:dock) { create(:docket, person: arr1.person) }
      let!(:chg) { create(:charge, arrest: arr1) }
      let!(:da_chg1) { create(:da_charge, docket: dock, arrest_charge: chg) }
      let!(:da_chg2) { create(:da_charge, docket: dock, arrest: arr1)}
      let!(:da_chg3) { create(:da_charge, docket: dock) }
      
      it "should return empty relation when no dockets are linked" do
        expect(arr2.docket_charges).to match_array([])
        expect(arr2.docket_charges).to be_a(ActiveRecord::Relation)
      end
      
      it "should return docket charges linked to arrest directly, and through charges" do
        expect(arr1.docket_charges.pluck(:id)).to match_array([da_chg1.id, da_chg2.id])
      end
    end
    
    describe "#dockets" do
      let!(:arr1) { create(:arrest) }
      let!(:arr2) { create(:arrest) }
      let!(:dock1) { create(:docket, person: arr1.person) }
      let!(:dock2) { create(:docket, person: arr1.person) }
      let!(:dock3) { create(:docket, person: arr1.person) }
      let!(:chg) { create(:charge, arrest: arr1) }
      let!(:da_chg1) { create(:da_charge, docket: dock1, arrest_charge: chg) }
      let!(:da_chg2) { create(:da_charge, docket: dock2, arrest: arr1)}
      
      it "should return empty relation when no dockets are linked" do
        expect(arr2.dockets).to match_array([])
        expect(arr2.dockets).to be_a(ActiveRecord::Relation)
      end
      
      it "should return dockets linked through charges and da_charges" do
        expect(arr1.dockets.pluck(:id)).to match_array([dock1.id, dock2.id])
      end
    end
    
    describe "#hours_served" do
      it "should call .hours_served with its id" do
        arr = create(:arrest)
        allow(Arrest).to receive(:hours_served)
        arr.hours_served
        expect(Arrest).to have_received(:hours_served).with(arr.id)
      end
    end
    
    describe "#is_final?" do
      it "should return true if there are no district charges" do
        arr = create(:arrest)
        expect(arr.is_final?).to be(true)
      end
      
      it "should return true if all district charges are final" do
        arr = create(:arrest)
        chg = create(:charge, charge_type: 'District', arrest: arr)
        dock = create(:docket, person: arr.person)
        d_chg = create(:da_charge, docket: dock, arrest_charge: chg)
        court = create(:court, docket: dock)
        sent = create(:sentence, court: court, da_charge: d_chg, processed: true)
        expect(arr.is_final?).to be(true)
      end
      
      it "should return false if any district charge is not final" do
        arr = create(:arrest)
        chg = create(:charge, charge_type: 'District', arrest: arr)
        dock = create(:docket, person: arr.person)
        d_chg = create(:da_charge, docket: dock, arrest_charge: chg)
        expect(arr.is_final?).to be(false)
      end
      
      it "should return false if any district charge has no docket charge links" do
        arr = create(:arrest)
        chg = create(:charge, charge_type: 'District', arrest: arr)
        expect(arr.is_final?).to be(false)
      end
    end
    
    describe "#not_rebookable" do
      it "should return error text if there are no holds for the arrest" do
        arr = create(:arrest)
        expect(arr.not_rebookable).to include("No Holds")
      end
      
      it "should return error text if there are currently active holds" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        hld = create(:hold, hold_type: '48 Hour', arrest: arr, booking: bk)
        expect(arr.not_rebookable).to include("Active Holds")
      end
      
      it "should return error text if arrested person has no current booking" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person, release_datetime: Time.now, released_because: "Bonded", release_officer: "Merten")
        hld = create(:hold, hold_type: '48 Hour', arrest: arr, booking: bk, cleared_by: "Merten", cleared_datetime: Time.now, cleared_because: "Bonded", explanation: "Bonded")
        expect(arr.not_rebookable).to include("No Booking")
      end
      
      it "should return error text if person booking is same as arrest booking" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        hld = create(:hold, hold_type: '48 Hour', arrest: arr, booking: bk, cleared_by: "Merten", cleared_datetime: Time.now, cleared_because: "Bonded", explanation: "Bonded")
        expect(arr.not_rebookable).to include("Same Booking")
      end
      
      it "should return false if no error conditions detected" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person, release_datetime: Time.now, released_because: "Bonded", release_officer: "Merten")
        hld = create(:hold, hold_type: '48 Hour', arrest: arr, booking: bk, cleared_by: "Merten", cleared_datetime: Time.now, cleared_because: "Bonded", explanation: "Bonded")
        new_bk = create(:booking, person: arr.person)
        expect(arr.not_rebookable).to be_nil
      end
    end
    
    describe "#reasons_not_destroyable" do
      it "should include error message if arrest has bonds" do
        arr = create(:arrest)
        bnd = create(:bond, person: arr.person, arrest: arr)
        expect(arr.reasons_not_destroyable).to include("Has Bonds")
      end
      
      it "should include error message if arrest has revocations" do
        arr = create(:arrest)
        dock = create(:docket, person: arr.person)
        rev = create(:revocation, arrest: arr, docket: dock)
        expect(arr.reasons_not_destroyable).to include("Has Revocations")
      end
      
      it "should include error message if arrest has docket charges" do
        arr = create(:arrest)
        dock = create(:docket, person: arr.person)
        chg = create(:da_charge, docket: dock, arrest: arr)
        expect(arr.reasons_not_destroyable).to include("Has Docket Charges")
      end
      
      it "should include error message if arrest has investigations" do
        arr = create(:arrest)
        inv = create(:investigation)
        inv.arrests << arr
        expect(arr.reasons_not_destroyable).to include("Linked to Investigation")
      end
      
      it "should include error message if arrest has Offense" do
        arr = create(:arrest)
        off = create(:offense)
        off.arrests << arr
        expect(arr.reasons_not_destroyable).to include("Linked to Offense")
      end
      
      it "should return nil if no links found" do
        arr = create(:arrest)
        expect(arr.reasons_not_destroyable).to be_nil
      end
    end
    
    describe "#rebook" do
      it "should call #not_rebookable and #process_holds methods" do
        arr = create(:arrest)
        allow(arr).to receive(:not_rebookable).and_return(nil)
        allow(arr).to receive(:process_holds).and_return(true)
        arr.rebook
        expect(arr).to have_received(:not_rebookable)
        expect(arr).to have_received(:process_holds)
      end
      
      it "should return error message unless process_holds succeeded" do
        arr = create(:arrest)
        allow(arr).to receive(:not_rebookable).and_return(nil)
        allow(arr).to receive(:process_holds).and_return(false)
        expect(arr.rebook).to include("Failed")
      end
    end
    
    describe "#process_warrants" do
      it "should link warrants given in assigned_warrants array" do
        war = create(:warrant)
        chg = create(:charge, warrant: war)
        arr = create(:arrest)
        
        # assign war to arr in the normal way
        arr.assigned_warrants = [war.id]
        arr.process_warrants
        expect(arr.warrants(true)).to eq([war])
    
        # war is no longer in sync with database record, reload it
        war.reload
        expect(war.dispo_date).to eq(Date.today)
        expect(war.person_id).to eq(arr.person_id)
        expect(war.disposition).to eq('Served')
    
        # make sure warrant charges were assigned to arrest
        expect(arr.charges(true)).to match_array(war.charges(true))
      end
      
      it "should unlink warrants not given in assigned_warrants array" do
        war = create(:warrant)
        arr = create(:arrest)
    
        # first assign war to arr in the same way as before
        arr.assigned_warrants = [war.id]
        arr.process_warrants
        
        # now test removal of war from arr in the normal way
        arr.assigned_warrants = []
        arr.process_warrants
        expect(arr.warrants(true)).to eq([])
    
        # war is no longer in sync with database record. reload it
        war.reload
        expect(war.dispo_date).to be_nil
        expect(war.person_id).to be_nil
        expect(war.disposition).to be_blank
    
        # make sure warrant charges were removed from arrest
        expect(arr.charges(true)).to be_empty
      end
    end
    
    describe "#check_and_generate_case" do
      it "should return true for legacy arrests" do
        arr = create(:arrest, legacy: true)
        expect(arr.case_no).to eq('')
        expect(arr.check_and_generate_case).to be(true)
        expect(arr.case_no).to eq('')
      end
      
      it "should pick up case number from linked offense if case_no blank" do
        call = create(:call)
        off = create(:offense, call: call)
        per = create(:person)
        arr = off.arrests.create(arrest_datetime: Time.now, agency: "TBPD", person: per)
        expect(arr.check_and_generate_case).to be(true)
        expect(arr.case_no).to eq(call.case_no)
      end
      
      it "should create new case if case_no blank and no offenses linked" do
        arr = create(:arrest)
        expect(arr.check_and_generate_case).to be(true)
        expect(arr.case_no).to eq(Call.last.case_no)
      end
      
      it "should generate its own case_no if unable to create new call" do
        allow(Call).to receive(:new).and_return(nil)
        arr = create(:arrest)
        expect(arr.check_and_generate_case).to be(true)
        expect(arr.case_no).not_to eq('')
        expect(Call.last).to be_nil
      end
    end
    
    describe "#adjust_atn" do
      it "should strip blanks from :atn if :atn is provided" do
        a1 = build(:arrest, atn: "   1234   ")
        a1.adjust_atn
        expect(a1.atn).to eq("1234")
      end
    end
    
    describe "#adjust_case_no" do
      it "should call AppUtils.fix_case" do
        a1 = build(:arrest, case_no: "1/2%3")
        allow(AppUtils).to receive(:fix_case)
        a1.adjust_case_no
        expect(AppUtils).to have_received(:fix_case).with("1/2%3")
      end
    end
    
    describe "#check_72_hour" do
      it "should fail validation if condition of bond and 72Hour datetime is provided, but not 72Hour judge" do
        expect(build(:arrest, hour_72_datetime: Time.now, condition_of_bond: 'pay fine').valid?).to be(false)
      end
      
      it "should fail validation if bond amount and judge is provided, but not 72Hour datetime" do
        expect(build(:arrest, hour_72_judge: "Merten", bond_amt: "500.00").valid?).to be(false)
      end
      
      it "should pass validation if condition of bond and judge and 72Hour datetime are provided" do
        expect(build(:arrest, hour_72_judge: "Merten", hour_72_datetime: Time.now, condition_of_bond: "pay fine").valid?).to be(true)
      end
    end
    
    describe "#check_destroyable" do
      it "should call #reasons_not_destroyable and return true if blank" do
        arr = create(:arrest)
        allow(arr).to receive(:reasons_not_destroyable).and_return(nil)
        expect(arr.check_destroyable).to be(true)
      end
      
      it "should call #reasons_not_destroyable and return false if reasons exist" do
        arr = create(:arrest)
        allow(arr).to receive(:reasons_not_destroyable).and_return("because I said so")
        expect(arr.check_destroyable).to be(false)
      end
    end
    
    describe "#check_probation" do
      it "should create new probation hold if person is on probation and hold_if_arrested is true" do
        per = create(:person)
        allow(per).to receive(:on_probation?).and_return(true)
        book = create(:booking, person: per)
        arr = create(:arrest, person: per)
        arr.check_probation
        expect(arr.holds.last).not_to be_nil
        expect(arr.holds.last.hold_type).to eq("Probation (Parish)")
      end
    end
    
    describe "#lookup_contacts" do
      it "should update DWI testing officer if dwi_test_officer_id not blank" do
        c1 = create(:contact, sort_name: "Merten, Michael")
        a1 = build(:arrest, dwi_test_officer: "None", dwi_test_officer_id: c1.id)
        a1.valid?
        expect(a1.dwi_test_officer).to eq("Merten, Michael")
      end
      
      it "should not change DWI testing officer if dwi_test_officer_id is blank" do
        a1 = build(:arrest, dwi_test_officer: "None")
        a1.valid?
        expect(a1.dwi_test_officer).to eq("None")
      end
      
      it "should update agency name if agency_id is not blank" do
        c1 = create(:contact, sort_name: "Toledo Bend PD", business: true)
        a1 = build(:arrest, agency: "None", agency_id: c1.id)
        a1.valid?
        expect(a1.agency).to eq("Toledo Bend PD")
      end
      
      it "should not change agency name if agency_id is blank" do
        a1 = build(:arrest, agency: "None")
        a1.valid?
        expect(a1.agency).to eq("None")
      end
    end
    
    describe "#process_72_hour" do
      it "should return true if hour_72_datetime is nil" do
        arr = create(:arrest)
        expect(arr.process_72_hour).to be(true)
      end
      
      it "should return true if no 72 Hour hold exists" do
        arr = create(:arrest, hour_72_datetime: Time.now, hour_72_judge: "Merten")
        expect(arr.process_72_hour).to be(true)
      end
      
      it "should return true if 72 Hour hold is already cleared" do
        arr = create(:arrest, hour_72_datetime: Time.now, hour_72_judge: "Merten")
        book = create(:booking, person: arr.person)
        h72 = create(:hold, arrest: arr, booking: book, hold_type: "72 Hour", cleared_by: "Merten", cleared_datetime: Time.now, cleared_because: "Bonded", explanation: "Bonded")
        expect(arr.process_72_hour).to be(true)
      end
      
      it "should clear 72 Hour hold and create Bondable hold if bondable? is true" do
        arr = create(:arrest, hour_72_datetime: Time.now, bondable: true, hour_72_judge: "Merten")
        book = create(:booking, person: arr.person)
        h72 = create(:hold, arrest: arr, booking: book, hold_type: "72 Hour")
        expect(arr.process_72_hour).to be(true)
        expect(h72.reload.cleared_datetime).not_to be_nil
        expect(arr.holds.last.hold_type).to eq("Bondable")
      end
      
      it "should clear 72 Hour hold and create No Bond hold if not bondable?" do
        arr = create(:arrest, hour_72_datetime: Time.now, bondable: false, hour_72_judge: "Merten")
        book = create(:booking, person: arr.person)
        h72 = create(:hold, arrest: arr, booking: book, hold_type: "72 Hour")
        expect(arr.process_72_hour).to be(true)
        expect(h72.reload.cleared_datetime).not_to be_nil
        expect(arr.holds.last.hold_type).to eq("No Bond")
      end
    end
    
    describe "#process_afis" do
      it "should add disposition if arrest has atn entered" do
        arr = create(:arrest)
        expect(arr.reload.disposition).to be_nil
        arr.update!(atn: "12345")
        expect(arr.reload.disposition).not_to be_nil
      end
        
      it "should remove disposition if arrest atn changes" do
        arr = create(:arrest, atn: '12345')
        expect(arr.disposition).not_to be_nil
        arr.update!(atn: '')
        expect(arr.reload.disposition).to be_nil
        expect(Disposition.count).to eq(0)
      end
    end
    
    describe "#link_to_or_create_disposition" do
      it "should link to existing disposition if one exists" do
        arr = create(:arrest)
        disp = create(:disposition, atn: '12345', person: arr.person)
        expect(arr.disposition).to be_nil
        arr.update_attribute(:atn, '12345')
        arr.link_to_or_create_disposition
        expect(arr.reload.disposition.id).to eq(disp.id)
        expect(Disposition.count).to eq(1)
      end
      
      it "should create new disposition if none exists" do
        arr = create(:arrest)
        arr.update_attribute(:atn, '12345')
        arr.link_to_or_create_disposition
        expect(arr.reload.disposition).not_to be_nil
        expect(arr.disposition.atn).to eq(arr.atn)
        expect(Disposition.count).to eq(1)
      end
      
      it "should not create or link disposition if atn is blank" do
        arr = create(:arrest)
        arr.link_to_or_create_disposition
        expect(Disposition.count).to eq(0)
      end
    end
    
    describe "#process_dependents" do
      it "should remove disposition if only linked to this one arrest" do
        arr = create(:arrest, atn: '12345')
        expect(Disposition.count).to eq(1)
        arr.process_dependents
        expect(Disposition.count).to eq(0)
      end
      
      it "should not remove disposition if linked to other arrests" do
        arr1 = create(:arrest, atn: '12345')
        arr2 = create(:arrest, atn: '12345', person: arr1.person)
        expect(Disposition.count).to eq(1)
        arr1.process_dependents
        expect(Disposition.count).to eq(1)
      end
      
      it "should reactivate attached warrants and unlink them from arrest" do
        arr = create(:arrest)
        war = create(:warrant, person: arr.person)
        war.update!(dispo_date: Date.today, disposition: "Served")
        arr.warrants << war
        expect(arr.reload.warrants.count).to eq(1)
        arr.process_dependents
        expect(arr.reload.warrants.count).to eq(0)
        expect(war.reload.dispo_date).to be_nil
        expect(war.disposition).to eq('')
      end
      
      it "should remove arrest charges and unlink warrant charges from arrest" do
        arr = create(:arrest)
        war = create(:warrant, person: arr.person)
        chg1 = create(:charge, arrest: arr)
        chg2 = create(:charge, arrest: arr, warrant: war)
        war.update!(dispo_date: Date.today, disposition: "Served")
        arr.warrants << war
        expect(arr.reload.charges.count).to eq(2)
        expect(war.reload.charges.count).to eq(1)
        arr.process_dependents
        expect(arr.reload.charges.count).to eq(0)
        expect(war.reload.charges.count).to eq(1)
      end
    end
    
    describe "#process_holds" do
      it "should return true if arrested person has no current booking" do
        arr = create(:arrest)
        chg = create(:charge, arrest: arr, charge: 'burglary', charge_type: 'District', count: 1)
        expect(arr.reload.process_holds).to be(true)
        expect(arr.holds.count).to eq(0)
      end
      
      it "should return true if arrest has no charges" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        expect(arr.process_holds).to be(true)
        expect(arr.holds.count).to eq(0)
      end
      
      # district charges
      it "should add 72 Hour hold if one does not already exist with district charges" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        chg = create(:charge, arrest: arr, charge: 'burglary', charge_type: 'District', count: 1)
        expect(arr.holds.count).to eq(0)
        arr.process_holds
        expect(arr.reload.holds.count).to eq(1)
        expect(arr.holds.last.hold_type).to eq('72 Hour')
        expect(bk.reload.holds.count).to eq(1)
      end
      
      it "should not add 72 Hour hold if one already exists with district charges" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        chg = create(:charge, arrest: arr, charge: 'burglary', charge_type: 'District', count: 1)
        arr.process_holds
        expect(arr.reload.holds.count).to eq(1)
        # second time should not add another 72 hour
        arr.process_holds
        expect(arr.reload.holds.count).to eq(1)
      end
      
      # city charges
      it "should add Hold for City hold for each agency with city charges" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        chg = create(:charge, arrest: arr, charge: 'burglary', charge_type: 'City', agency: 'MPD', count: 1)
        chg = create(:charge, arrest: arr, charge: 'robbery', charge_type: 'City', agency: 'TPD', count: 1)
        expect(arr.holds.count).to eq(0)
        arr.process_holds
        expect(arr.reload.holds.count).to eq(2)
        expect(arr.holds.first.hold_type).to eq('Hold for City')
        expect(arr.holds.last.hold_type).to eq('Hold for City')
        expect(bk.reload.holds.count).to eq(2)
      end
      
      it "should not add Hold for City hold for agencies that already have holds" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        chg = create(:charge, arrest: arr, charge: 'burglary', charge_type: 'City', agency: 'MPD', count: 1)
        chg = create(:charge, arrest: arr, charge: 'robbery', charge_type: 'City', agency: 'TPD', count: 1)
        arr.process_holds
        expect(arr.reload.holds.count).to eq(2)
        # second time should not add any holds
        arr.process_holds
        expect(arr.reload.holds.count).to eq(2)
      end
      
      it "should remove City holds for agencies that do not have city charges" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        chg = create(:charge, arrest: arr, charge: 'burglary', charge_type: 'City', agency: 'MPD', count: 1)
        chg2 = create(:charge, arrest: arr, charge: 'robbery', charge_type: 'City', agency: 'TPD', count: 1)
        arr.process_holds
        expect(arr.holds.count).to eq(2)
        chg2.destroy
        arr.reload.process_holds
        expect(arr.holds.count).to eq(1)
        expect(arr.holds.first.agency).to eq('Mpd') # titleizes hold agencies
      end
      
      it "should not remove City holds for agencies that do not have city charges if they have bonds attached" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        chg = create(:charge, arrest: arr, charge: 'burglary', charge_type: 'City', agency: 'MPD', count: 1)
        chg2 = create(:charge, arrest: arr, charge: 'robbery', charge_type: 'City', agency: 'TPD', count: 1)
        arr.process_holds
        expect(arr.holds.count).to eq(2)
        hold = arr.holds(true).last
        bnd = create(:bond, arrest: arr, person: arr.person, surety: arr.person)
        bnd.holds << hold
        chg2.destroy
        arr.process_holds
        expect(arr.holds(true).count).to eq(2)
      end
      
      # other charges
      it "should add Hold for Other Agency hold for each agency with other charges" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        war = create(:warrant, person: arr.person, jurisdiction: 'New York P.D.')
        chg = create(:charge, agency: "New York P.D.", charge_type: 'Other')
        war.charges << chg
        arr.assigned_warrants = [war.id]
        arr.save!
        expect(arr.other_charges.count).to eq(1)
        expect(arr.holds(true).count).to eq(1)
        expect(arr.holds.last.hold_type).to eq('Hold for Other Agency')
      end
      
      it "should not add Hold for Other Agency hold for agencies that already have holds" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        war = create(:warrant, person: arr.person, jurisdiction: 'New York P.D.')
        chg = create(:charge, agency: "New York P.D.", charge_type: 'Other')
        war.charges << chg
        arr.assigned_warrants = [war.id]
        arr.save!
        expect(arr.holds(true).count).to eq(1)
        # second time
        arr.process_holds
        expect(arr.holds(true).count).to eq(1)
      end
      
      it "should remove Other Agency holds for agencies that do not have other charges" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        war = create(:warrant, person: arr.person, jurisdiction: 'New York P.D.')
        chg = create(:charge, agency: "New York P.D.", charge_type: 'Other')
        war.charges << chg
        war2 = create(:warrant, person: arr.person, jurisdiction: 'New Orleans P.D.')
        chg2 = create(:charge, agency: "New Orleans P.D.", charge_type: 'Other')
        war2.charges << chg2
        arr.assigned_warrants = [war.id, war2.id]
        arr.save!
        expect(arr.holds.count).to eq(2)
        arr.assigned_warrants = [war.id]
        arr.process_warrants
        expect(arr.other_charges(true).count).to eq(1)
        arr.process_holds
        expect(arr.holds(true).count).to eq(1)
      end
      
      it "should not remove City holds for agencies that do not have city charges if they have bonds attached" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        war = create(:warrant, person: arr.person, jurisdiction: 'New York P.D.')
        chg = create(:charge, agency: "New York P.D.", charge_type: 'Other')
        war.charges << chg
        war2 = create(:warrant, person: arr.person, jurisdiction: 'New Orleans P.D.')
        chg2 = create(:charge, agency: "New Orleans P.D.", charge_type: 'Other')
        war2.charges << chg2
        arr.assigned_warrants = [war.id, war2.id]
        arr.save!
        expect(arr.holds(true).count).to eq(2)
        bnd = create(:bond, arrest: arr, person: arr.person, surety: arr.person)
        bnd.holds << arr.holds(true).last
        arr.assigned_warrants = [war.id]
        arr.process_warrants
        expect(arr.other_charges(true).count).to eq(1)
        arr.process_holds
        expect(arr.holds(true).count).to eq(2)
      end
      
      # probation holds
      it "should add parish probation hold if :parish_probation is true" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        expect(arr.holds(true).count).to eq(0)
        arr.parish_probation = '1' # from form check box
        arr.process_holds
        expect(arr.holds(true).count).to eq(1)
        expect(arr.holds.first.hold_type).to eq("Probation (Parish)")
      end
      
      it "should not add parish probation hold if :parish_probation is true and one already exists" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        arr.parish_probation = '1' # from form check box
        arr.process_holds
        expect(arr.holds(true).count).to eq(1)
        arr.parish_probation = '1'
        arr.process_holds
        expect(arr.holds(true).count).to eq(1)
      end
      
      it "should add state probation hold if :state_probation is true" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        expect(arr.holds(true).count).to eq(0)
        arr.state_probation = '1' # from form check box
        arr.process_holds
        expect(arr.holds(true).count).to eq(1)
        expect(arr.holds.first.hold_type).to eq("Probation/Parole (State)")
      end
      
      it "should not add state probation hold if :state_probation is true and one already exists" do
        arr = create(:arrest)
        bk = create(:booking, person: arr.person)
        arr.state_probation = '1' # from form check box
        arr.process_holds
        expect(arr.holds(true).count).to eq(1)
        arr.state_probation = '1'
        arr.process_holds
        expect(arr.holds(true).count).to eq(1)
      end
    end
    
    describe "#send_messages" do
      it "should send new arrest notification to each registered user with permission" do
        u = create(:user, notify_arrest_new: true)
        u2 = create(:user, notify_arrest_new: true)
        u3 = create(:user, notify_arrest_new: false)
        g1 = create(:group, permissions: [Perm.get(:view_arrest)])
        g2 = create(:group)
        u.groups << g1
        u2.groups << g2
        u3.groups << g2
        allow(Message).to receive(:create)
        arr = create(:arrest)
        expect(Message).to have_received(:create).with(hash_including(receiver_id: u.id))
      end
    end
  end
end
