# == Schema Information
#
# Table name: damage_items
#
#  id          :integer          not null, primary key
#  damage_id   :integer          not null
#  description :string(255)      default(""), not null
#  estimate    :decimal(12, 2)   default(0.0), not null
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_damage_items_on_damage_id  (damage_id)
#
# Foreign Keys
#
#  damage_items_damage_id_fk  (damage_id => damages.id)
#

require 'rails_helper'

RSpec.describe DamageItem do
  
  ## table columns
  #it { is_expected.to have_db_column(:damage_id).of_type(:integer).with_options(null: false) }
  #it { is_expected.to have_db_column(:description).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:estimate).of_type(:decimal).with_options(scale: 12, precision: 2, null: false, default: 0.0) }
  #it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  #
  ## table index
  #it { is_expected.to have_db_index(:damage_id) }
  #
  ## associations
  #
  #it { is_expected.to belong_to(:damage).inverse_of(:damage_items) }
  #
  ## validations
  #it { is_expected.to validate_presence_of(:description) }
  #it { is_expected.to validate_numericality_of(:estimate).is_greater_than_or_equal_to(0).is_less_than(10 ** 10) }
  #
  ## methods
  #it { is_expected.to have_defined_desc }
end
