# == Schema Information
#
# Table name: fax_covers
#
#  id             :integer          not null, primary key
#  name           :string(255)      default(""), not null
#  from_name      :string(255)      default(""), not null
#  from_fax       :string(255)      default(""), not null
#  from_phone     :string(255)      default(""), not null
#  include_notice :boolean          default(TRUE), not null
#  notice_title   :string(255)      default(""), not null
#  notice_body    :text             default(""), not null
#  created_by     :integer          default(1)
#  updated_by     :integer          default(1)
#  created_at     :datetime
#  updated_at     :datetime
#
# Indexes
#
#  index_fax_covers_on_created_by  (created_by)
#  index_fax_covers_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  fax_covers_created_by_fk  (created_by => users.id)
#  fax_covers_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe FaxCover do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
