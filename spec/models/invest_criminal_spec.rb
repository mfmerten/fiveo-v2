# == Schema Information
#
# Table name: invest_criminals
#
#  id                :integer          not null, primary key
#  investigation_id  :integer          not null
#  person_id         :integer
#  sort_name         :string(255)      default(""), not null
#  description       :string(255)      default(""), not null
#  height            :string(255)      default(""), not null
#  weight            :string(255)      default(""), not null
#  shoes             :string(255)      default(""), not null
#  fingerprint_class :string(255)      default(""), not null
#  dna_profile       :string(255)      default(""), not null
#  remarks           :text             default(""), not null
#  created_by        :integer          default(1)
#  updated_by        :integer          default(1)
#  created_at        :datetime
#  updated_at        :datetime
#  private           :boolean          default(TRUE), not null
#  owned_by          :integer          default(1)
#
# Indexes
#
#  index_invest_criminals_on_created_by        (created_by)
#  index_invest_criminals_on_investigation_id  (investigation_id)
#  index_invest_criminals_on_owned_by          (owned_by)
#  index_invest_criminals_on_person_id         (person_id)
#  index_invest_criminals_on_sort_name         (sort_name)
#  index_invest_criminals_on_updated_by        (updated_by)
#
# Foreign Keys
#
#  invest_criminals_created_by_fk        (created_by => users.id)
#  invest_criminals_investigation_id_fk  (investigation_id => investigations.id)
#  invest_criminals_owned_by_fk          (owned_by => users.id)
#  invest_criminals_person_id_fk         (person_id => people.id)
#  invest_criminals_updated_by_fk        (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe InvestCriminal do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
