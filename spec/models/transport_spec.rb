# == Schema Information
#
# Table name: transports
#
#  id              :integer          not null, primary key
#  person_id       :integer          not null
#  officer1        :string(255)      default(""), not null
#  officer1_unit   :string(255)      default(""), not null
#  officer2        :string(255)      default(""), not null
#  officer2_unit   :string(255)      default(""), not null
#  begin_miles     :integer          default(0), not null
#  end_miles       :integer          default(0), not null
#  estimated_miles :integer          default(0), not null
#  from            :string(255)      default(""), not null
#  from_street     :string(255)      default(""), not null
#  from_city       :string(255)      default(""), not null
#  from_zip        :string(255)      default(""), not null
#  to              :string(255)      default(""), not null
#  to_street       :string(255)      default(""), not null
#  to_city         :string(255)      default(""), not null
#  to_zip          :string(255)      default(""), not null
#  remarks         :text             default(""), not null
#  created_by      :integer          default(1)
#  updated_by      :integer          default(1)
#  created_at      :datetime
#  updated_at      :datetime
#  officer1_badge  :string(255)      default(""), not null
#  officer2_badge  :string(255)      default(""), not null
#  from_state      :string(255)      default(""), not null
#  to_state        :string(255)      default(""), not null
#  begin_datetime  :datetime
#  end_datetime    :datetime
#
# Indexes
#
#  index_transports_on_created_by  (created_by)
#  index_transports_on_person_id   (person_id)
#  index_transports_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  transports_created_by_fk  (created_by => users.id)
#  transports_person_id_fk   (person_id => people.id)
#  transports_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Transport do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
