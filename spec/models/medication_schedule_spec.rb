# == Schema Information
#
# Table name: medication_schedules
#
#  id            :integer          not null, primary key
#  medication_id :integer          not null
#  dose          :string(255)      default(""), not null
#  time          :string(255)      default(""), not null
#  created_at    :datetime
#  updated_at    :datetime
#
# Indexes
#
#  index_med_schedules_on_medication_id  (medication_id)
#
# Foreign Keys
#
#  medication_schedules_medication_id_fk  (medication_id => medications.id)
#

require 'rails_helper'

RSpec.describe MedicationSchedule do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
