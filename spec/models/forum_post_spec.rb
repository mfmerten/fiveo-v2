# == Schema Information
#
# Table name: forum_posts
#
#  id             :integer          not null, primary key
#  forum_topic_id :integer          not null
#  user_id        :integer          not null
#  reply_to_id    :integer
#  text           :text             default(""), not null
#  notified       :boolean          default(FALSE), not null
#  created_at     :datetime
#  updated_at     :datetime
#
# Indexes
#
#  index_forum_posts_on_forum_topic_id  (forum_topic_id)
#  index_forum_posts_on_reply_to_id     (reply_to_id)
#  index_forum_posts_on_user_id         (user_id)
#
# Foreign Keys
#
#  forum_posts_forum_topic_id_fk  (forum_topic_id => forum_topics.id)
#  forum_posts_reply_to_id_fk     (reply_to_id => forum_posts.id)
#  forum_posts_user_id_fk         (user_id => users.id)
#

require 'rails_helper'

RSpec.describe ForumPost do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
