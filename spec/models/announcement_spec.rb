# == Schema Information
#
# Table name: announcements
#
#  id          :integer          not null, primary key
#  subject     :string(255)      default(""), not null
#  body        :text             default(""), not null
#  created_at  :datetime
#  updated_at  :datetime
#  created_by  :integer          default(1)
#  updated_by  :integer          default(1)
#  unignorable :boolean          default(FALSE), not null
#
# Indexes
#
#  index_announcements_on_created_by  (created_by)
#  index_announcements_on_updated_at  (updated_at)
#  index_announcements_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  announcements_created_by_fk  (created_by => users.id)
#  announcements_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Announcement do

  describe "Table Columns" do
    it { is_expected.to have_db_column(:subject).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:body).of_type(:text).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
    it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_options(default: 1) }
    it { is_expected.to have_db_column(:unignorable).of_type(:boolean).with_options(default: false, null: false) }
  end
  
  describe "Table Indices" do
    it { is_expected.to have_db_index(:created_by) }
    it { is_expected.to have_db_index(:updated_by) }
    it { is_expected.to have_db_index(:updated_at) }
  end
  
  describe "Associations" do
    it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key('created_by') }
    it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key('updated_by') }
    it { is_expected.to have_many(:announcement_ignores).dependent(:destroy) }
    it { is_expected.to have_many(:ignorers).through(:announcement_ignores).source(:user) }
  end
  
  describe "Scopes" do
    let!(:c1) { create(:contact, sort_name: 'Merten, Michael') }
    let!(:c2) { create(:contact, sort_name: 'Merten, Rebecca') }
    let!(:u1) { create(:user, login: 'mmerten', contact: c1) }
    let!(:u2) { create(:user, login: 'rmerten', contact: c2) }
    let!(:a1) { create(:announcement, creator: u1) }
    let!(:a2) { create(:announcement, creator: u2) }
    
    describe ":by_reverse_updated" do
      it "should return announcements in reverse updated order" do
        a1.update_column(:updated_at, 1.day.ago)
        query = Announcement.by_reverse_updated.where(id: [a1.id, a2.id]).pluck(:id)
        expect(query[0]).to be > query[1]
      end
    end
    
    describe ":with_announcement_id" do
      it "should return announcement specified by id" do
        expect(Announcement.with_announcement_id(a2.id).pluck(:id)).to eq([a2.id])
      end
    end
    
    describe ":with_created_by" do
      it "should return announcements created by a specific user" do
        expect(Announcement.with_created_by(u1.id).pluck(:id)).to eq([a1.id])
      end
    end
    
    describe ":with_subject" do
      it "should call AppUtils.like to find announcements with subject text" do
        allow(AppUtils).to receive(:like)
        Announcement.with_subject("RSpec")
        expect(AppUtils).to have_received(:like).with(Announcement, :subject, "RSpec")
      end
    end
    
    describe ":with_text" do
      it "should call AppUtils.like to find announcements with subject or body text" do
        allow(AppUtils).to receive(:like)
        Announcement.with_text("test")
        expect(AppUtils).to have_received(:like).with(Announcement, [:body, :subject], "test")
      end
    end
  end

  describe "after_validation" do
    describe ":check_creator" do
      it "should call #check_creator method" do
        u1 = create(:user)
        a1 = build(:announcement)
        allow(a1).to receive(:check_creator)
        a1.valid?
        expect(a1).to have_received(:check_creator).with(no_args)
      end
    end
  end
  
  describe "Validations" do
    describe ":subject" do
      it { is_expected.to validate_presence_of(:subject) }
    end
    
    describe ":body" do
      it { is_expected.to validate_presence_of(:body) }
    end
  end
  
  describe "Class Methods" do
    describe ".desc" do
      it { is_expected.to have_defined_desc }
    end
    
    describe ".base_perms" do
      it { is_expected.to have_defined_base_perms }
    end
    
    describe ".process_query" do
      before(:example) do
        allow(Announcement).to receive(:with_announcement_id).and_call_original
        allow(Announcement).to receive(:with_created_by).and_call_original
        allow(Announcement).to receive(:with_subject).and_call_original
        allow(Announcement).to receive(:with_text).and_call_original
      end
      let(:full_query) {
        {
          id: '1',
          created_by: '2',
          subject: 'test',
          text: 'testing'
        }
      }
      let(:empty_query) {
        {
          id: '',
          created_by: '',
          subject: '',
          text: ''
        }
      }
      
      it "should return empty relation when called with anything other than a Hash" do
        expect(Announcement.process_query(Date)).to be_a(ActiveRecord::Relation)
        expect(Announcement.process_query(Date)).to match_array([])
      end
      
      it "should execute appropriate scopes when given query hash" do
        Announcement.process_query(full_query)
        expect(Announcement).to have_received(:with_announcement_id).with('1')
        expect(Announcement).to have_received(:with_created_by).with('2')
        expect(Announcement).to have_received(:with_subject).with('test')
        expect(Announcement).to have_received(:with_text).with('testing')
      end
      
      it "should not call any scopes when given empty query" do
        Announcement.process_query(empty_query)
        expect(Announcement).not_to have_received(:with_announcement_id)
        expect(Announcement).not_to have_received(:with_created_by)
        expect(Announcement).not_to have_received(:with_subject)
        expect(Announcement).not_to have_received(:with_text)
      end
    end
  end
end
