# == Schema Information
#
# Table name: medications
#
#  id              :integer          not null, primary key
#  person_id       :integer          not null
#  prescription_no :string(255)      default(""), not null
#  physician_id    :integer
#  pharmacy_id     :integer
#  drug_name       :string(255)      default(""), not null
#  dosage          :string(255)      default(""), not null
#  warnings        :string(255)      default(""), not null
#  created_by      :integer          default(1)
#  updated_by      :integer          default(1)
#  created_at      :datetime
#  updated_at      :datetime
#
# Indexes
#
#  index_medications_on_created_by    (created_by)
#  index_medications_on_person_id     (person_id)
#  index_medications_on_pharmacy_id   (pharmacy_id)
#  index_medications_on_physician_id  (physician_id)
#  index_medications_on_updated_by    (updated_by)
#
# Foreign Keys
#
#  medications_created_by_fk    (created_by => users.id)
#  medications_person_id_fk     (person_id => people.id)
#  medications_pharmacy_id_fk   (pharmacy_id => contacts.id)
#  medications_physician_id_fk  (physician_id => contacts.id)
#  medications_updated_by_fk    (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Medication do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
