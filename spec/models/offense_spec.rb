# == Schema Information
#
# Table name: offenses
#
#  id                  :integer          not null, primary key
#  call_id             :integer
#  location            :string(255)      default(""), not null
#  details             :text             default(""), not null
#  pictures            :boolean          default(FALSE), not null
#  restitution_form    :boolean          default(FALSE), not null
#  victim_notification :boolean          default(FALSE), not null
#  perm_to_search      :boolean          default(FALSE), not null
#  misd_summons        :boolean          default(FALSE), not null
#  fortyeight_hour     :boolean          default(FALSE), not null
#  medical_release     :boolean          default(FALSE), not null
#  other_documents     :text             default(""), not null
#  officer             :string(255)      default(""), not null
#  remarks             :text             default(""), not null
#  created_at          :datetime
#  updated_at          :datetime
#  created_by          :integer          default(1)
#  updated_by          :integer          default(1)
#  officer_unit        :string(255)      default(""), not null
#  officer_badge       :string(255)      default(""), not null
#  legacy              :boolean          default(FALSE), not null
#  offense_datetime    :datetime
#
# Indexes
#
#  index_offenses_on_call_id     (call_id)
#  index_offenses_on_created_by  (created_by)
#  index_offenses_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  offenses_call_id_fk     (call_id => calls.id)
#  offenses_created_by_fk  (created_by => users.id)
#  offenses_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Offense do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
