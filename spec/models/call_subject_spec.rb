# == Schema Information
#
# Table name: call_subjects
#
#  id           :integer          not null, primary key
#  call_id      :integer          not null
#  sort_name    :string(255)      default(""), not null
#  address1     :string(255)      default(""), not null
#  address2     :string(255)      default(""), not null
#  home_phone   :string(255)      default(""), not null
#  cell_phone   :string(255)      default(""), not null
#  created_at   :datetime
#  updated_at   :datetime
#  work_phone   :string(255)      default(""), not null
#  subject_type :string(255)      default(""), not null
#
# Indexes
#
#  index_call_subjects_on_call_id    (call_id)
#  index_call_subjects_on_sort_name  (sort_name)
#
# Foreign Keys
#
#  call_subjects_call_id_fk  (call_id => calls.id)
#

require 'rails_helper'

RSpec.describe CallSubject do
  
  describe "Table Columns" do
    it { is_expected.to have_db_column(:call_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:sort_name).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:address1).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:address2).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:home_phone).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:cell_phone).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:work_phone).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:subject_type).of_type(:string).with_options(null: false, default: '') }
  end
  
  describe "Table Indices" do
    it { is_expected.to have_db_index(:call_id) }
    it { is_expected.to have_db_index(:sort_name) }
  end
  
  describe "Associations" do
    it { is_expected.to belong_to(:call).counter_cache(true).inverse_of(:call_subjects) }
  end
  
  describe "Callbacks" do
    describe "before_validation" do
      describe ":fix_name" do
        it "should call #fix_name before validation" do
          c = build(:call_subject)
          allow(c).to receive(:fix_name)
          c.valid?
          expect(c).to have_received(:fix_name).with(no_args)
        end
      end
    end
  end
  
  describe "Validations" do
    it { is_expected.to validate_presence_of(:sort_name) }
    it { is_expected.to validate_presence_of(:subject_type) }
    it { is_expected.to validate_phone(:home_phone).allow_blank }
    it { is_expected.to validate_phone(:cell_phone).allow_blank }
    it { is_expected.to validate_phone(:work_phone).allow_blank }
  end
  
  describe "Class Methods" do
    it { is_expected.to have_defined_desc }
  
    describe ".types" do
      it "should return array of allowable subject types" do
        expect(CallSubject.types).to be_a(Array)
        expect(CallSubject.types).to include("Complainant")
      end
    end
  end
  
  describe "Instance Methods" do
    describe "#address" do
      it "should return a string that includes address1" do
        c = create(:call_subject, address1: "Main Street")
        expect(c.address).to include("Main Street")
      end
    end
  
    describe "#display_name" do
      it "should call AppUtils.parse_name to produce display name" do
        allow(AppUtils).to receive(:parse_name)
        c = build(:call_subject, sort_name: "John Doe")
        c.display_name
        expect(AppUtils).to have_received(:parse_name).with("John Doe",true)
      end
    end
  
    describe "#to_s" do
      it "should return a string including the sort_name" do
        c = create(:call_subject)
        expect(c.to_s).to include(c.sort_name)
      end
    end
    
    describe "#fix_name" do
      it "should call AppUtils.parse_name to fix entered name" do
        allow(AppUtils).to receive(:parse_name)
        c = build(:call_subject, sort_name: "John Doe")
        c.fix_name
        expect(AppUtils).to have_received(:parse_name).with("John Doe")
      end
    end
  end
end
