# == Schema Information
#
# Table name: bookings
#
#  id                    :integer          not null, primary key
#  person_id             :integer          not null
#  cash_at_booking       :decimal(12, 2)   default(0.0), not null
#  cash_receipt          :string(255)      default(""), not null
#  booking_officer       :string(255)      default(""), not null
#  booking_officer_unit  :string(255)      default(""), not null
#  remarks               :text             default(""), not null
#  phone_called          :string(255)      default(""), not null
#  intoxicated           :boolean          default(FALSE), not null
#  sched_release_date    :date
#  afis                  :boolean          default(FALSE), not null
#  release_officer       :string(255)      default(""), not null
#  release_officer_unit  :string(255)      default(""), not null
#  created_by            :integer          default(1)
#  updated_by            :integer          default(1)
#  created_at            :datetime
#  updated_at            :datetime
#  booking_officer_badge :string(255)      default(""), not null
#  release_officer_badge :string(255)      default(""), not null
#  good_time             :boolean          default(TRUE), not null
#  disable_timers        :boolean          default(FALSE), not null
#  attorney              :string(255)      default(""), not null
#  legacy                :boolean          default(FALSE), not null
#  jail_id               :integer          not null
#  cell                  :string(255)      default(""), not null
#  booking_datetime      :datetime
#  release_datetime      :datetime
#  released_because      :string(255)      default(""), not null
#
# Indexes
#
#  index_bookings_on_created_by  (created_by)
#  index_bookings_on_jail_id     (jail_id)
#  index_bookings_on_person_id   (person_id)
#  index_bookings_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  bookings_created_by_fk  (created_by => users.id)
#  bookings_jail_id_fk     (jail_id => jails.id)
#  bookings_person_id_fk   (person_id => people.id)
#  bookings_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Booking do
  
  describe "Table Columns" do
    it { is_expected.to have_db_column(:person_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:cash_at_booking).of_type(:decimal).with_options(precision: 12, scale: 2, null: false, default: 0.0) }
    it { is_expected.to have_db_column(:cash_receipt).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:booking_officer).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:booking_officer_unit).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:booking_officer_badge).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:remarks).of_type(:text).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:phone_called).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:intoxicated).of_type(:boolean).with_options(default: false, null: false) }
    it { is_expected.to have_db_column(:sched_release_date).of_type(:date) }
    it { is_expected.to have_db_column(:afis).of_type(:boolean).with_options(default: false, null: false) }
    it { is_expected.to have_db_column(:release_officer).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:release_officer_unit).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:release_officer_badge).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
    it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_options(deafult: 1) }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:good_time).of_type(:boolean).with_options(default: true, null: false) }
    it { is_expected.to have_db_column(:disable_timers).of_type(:boolean).with_options(default: false, null: false) }
    it { is_expected.to have_db_column(:attorney).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:legacy).of_type(:boolean).with_options(default: false, null: false) }
    it { is_expected.to have_db_column(:jail_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:cell).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:booking_datetime).of_type(:datetime) }
    it { is_expected.to have_db_column(:release_datetime).of_type(:datetime) }
    it { is_expected.to have_db_column(:released_because).of_type(:string).with_options(default: '', null: false) }
  end
  
  describe "Table Indices" do
    it { is_expected.to have_db_index(:created_by) }
    it { is_expected.to have_db_index(:updated_by) }
    it { is_expected.to have_db_index(:jail_id) }
    it { is_expected.to have_db_index(:person_id) }
  end
  
  describe "Associations" do
    it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:created_by) }
    it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key(:updated_by) }
    it { is_expected.to belong_to(:jail).inverse_of(:bookings) }
    it { is_expected.to belong_to(:person).inverse_of(:bookings) }
    it { is_expected.to have_many(:holds).dependent(:destroy) }
    it { is_expected.to have_many(:temp_release_holds).conditions(hold_type: 'Temporary Release').class_name('Hold') }
    it { is_expected.to have_many(:doc_records).conditions(holds: {cleared_datetime: nil}).through(:holds).source(:doc_record) }
    it { is_expected.to have_many(:docs).dependent(:destroy) }
    it { is_expected.to have_many(:arrests).through(:holds) }
    it { is_expected.to have_many(:medicals).dependent(:destroy) }
    it { is_expected.to have_many(:visitors).dependent(:destroy) }
    it { is_expected.to have_many(:absent_prisoners).dependent(:destroy) }
    it { is_expected.to have_many(:absents).through(:absent_prisoners) }
    it { is_expected.to have_many(:transfers).dependent(:destroy) }
    it { is_expected.to have_many(:booking_logs).dependent(:destroy) }
    it { is_expected.to have_many(:properties).class_name('BookingProperty').dependent(:destroy) }
  end
  
  describe "Attributes" do
    it { is_expected.to accept_nested_attributes_for(:properties).allow_destroy(true) }
    it { is_expected.to have_attr_accessor(:booking_officer_id) }
    it { is_expected.to have_attr_accessor(:release_officer_id) }
  end
  
  describe "Scopes" do
    let!(:p1) { create(:person, family_name: 'Smith', given_name: 'John') }
    let!(:p2) { create(:person, family_name: 'Merten', given_name: 'Mike') }
    let!(:bk1) { create(:booking, person: p1, booking_datetime: Time.now - 2.days, release_datetime: Time.now - 1.day, release_officer: "Merten", released_because: "Bonded") }
    let!(:bk2) { create(:booking, person: p2) }
    
    describe ":by_id" do
      it "should return bookings in ID order" do
        # actually, this is rather useless as written...
        # I'm still trying to remember why I did a scope ordering by ID, since that's
        #    the default order
        expect(Booking.by_id.pluck(:id)).to eq([bk1.id, bk2.id])
      end
    end
    
    describe ":active" do
      it "should return bookings that are active (release_datetime nil)" do
        expect(Booking.active.pluck(:id)).to eq([bk2.id])
      end
    end
    
    describe ":active_on" do
      it "should return bookings that were active on a specified date" do
        expect(Booking.active_on(Date.today).pluck(:id)).to match_array([bk2.id])
      end
      
      it "should raise exception if no argument given" do
        expect { Booking.active_on }.to raise_error(ArgumentError)
      end
    end
    
    describe ":in_jail_between" do
      it "should return bookings that were active between two Times" do
        expect(Booking.in_jail_between(Time.now - 36.hours, Time.now).pluck(:id)).to match_array([bk1.id, bk2.id])
      end
      
      it "should raise exception if either or both arguments are missing" do
        expect { Booking.in_jail_between(Time.now) }.to raise_error(ArgumentError)
        expect { Booking.in_jail_between }.to raise_error(ArgumentError)
      end
    end
    
    describe ":by_name" do
      it "should return bookings in person name order" do
        expect(false).to be(true)
      end
    end
    
    describe ":belonging_to_jail" do
      it "should return bookings for a specified jail id" do
        expect(false).to be(true)
      end
    end
    
    describe ":with_mugshot" do
      it "should return bookings where person has a front mugshot" do
        expect(false).to be(true)
      end
    end
    
    describe ":with_booking_id" do
      it "should return a specified booking by id" do
        expect(false).to be(true)
      end
    end
    
    describe ":with_person_id" do
      it "should return bookings for the specified person" do
        expect(false).to be(true)
      end
    end
    
    describe ":with_cell" do
      it "should call AppUtils.like with the specified cell text" do
        expect(false).to be(true)
      end
    end
    
    describe ":with_afis" do
      it "should call AppUtils.for_bool with the specified state" do
        expect(false).to be(true)
      end
    end
    
    describe ":with_released_because" do
      it "should call AppUtils.like with the specified release reason text" do
        expect(false).to be(true)
      end
    end
    
    describe ":with_person_name" do
      it "should call AppUtils.for_person with the specified person name" do
        expect(false).to be(true)
      end
    end
    
    describe ":with_booking_officer" do
      it "should call AppUtils.for_officer with specified booking officer" do
        expect(false).to be(true)
      end
    end
    
    describe ":with_release_officer" do
      it "should call AppUtils.for_officer with specified release officer" do
        expect(false).to be(true)
      end
    end
    
    describe ":with_sched_release_date" do
      it "should call AppUtils.for_daterange with the specified start and stop dates" do
        expect(false).to be(true)
      end
    end
    
    describe ":with_release_date" do
      it "should call AppUtils.for_daterange with the specified start and stop dates" do
        expect(false).to be(true)
      end
    end
  end
  
  describe "Callbacks" do
    describe "before_save" do
      describe ":check_creator" do
        it "needs to be completed" do
          expect(false).to be(true)
        end
      end
    end
    
    describe "before_validation" do
      describe ":lookup_officers" do
        it "needs to be completed" do
          expect(false).to be(true)
        end
      end
  
      describe ":check_person" do
        it "needs to be completed" do
          expect(false).to be(true)
        end
      end
    end
    
    describe "after_save" do
      describe ":check_timers" do
        it "needs to be completed" do
          expect(false).to be(true)
        end
      end
      
      describe ":process_bond_recalls" do
        it "needs to be completed" do
          expect(false).to be(true)
        end
      end
    end
  
    describe "after_update" do
      describe ":send_release_messages" do
        it "needs to be completed" do
          expect(false).to be(true)
        end
      end
    end
  
    describe "after_create" do
      describe ":send_new_messages" do
        it "needs to be completed" do
          expect(false).to be(true)
        end
      end
    end
  
    describe "before_destroy" do
      describe ":check_destroyable" do
        it "needs to be completed" do
          expect(false).to be(true)
        end
      end
    end
  end
  
  describe "Validations" do
    it { is_expected.to validate_associated(:properties) }
    it { is_expected.to validate_presence_of(:booking_officer) }
    it { is_expected.to validate_presence_of(:cell) }
    it { is_expected.to validate_datetime(:booking_datetime) }
    it { is_expected.to validate_date(:sched_release_date).allow_blank }
    it { is_expected.to validate_numericality_of(:cash_at_booking).is_greater_than_or_equal_to(0).is_less_than(10 ** 10) }
    it { is_expected.to validate_person(:person_id) }
    it { is_expected.to validate_datetime(:release_datetime).allow_blank }
    it { is_expected.to validate_phone(:phone_called).allow_blank }
    it { is_expected.to validate_jail(:jail_id) }
    
    describe "If Not Released" do
      subject { build(:booking, release_datetime: nil) }
      it { is_expected.to_not validate_presence_of(:released_because) }
      it { is_expected.not_to validate_presence_of(:release_officer) }
    end
    
    describe "If Released" do
      subject { build(:booking, release_datetime: Time.now) }
      it { is_expected.to validate_presence_of(:released_because) }
      it { is_expected.to validate_presence_of(:release_officer) }
    end
  end
  
  describe "Class Methods" do
    it { is_expected.to have_defined_desc }
    it { is_expected.to have_defined_base_perms }
    
    describe ".release_reasons" do
      it "should return array of release reasons" do
        expect(Booking.release_reasons).to be_a(Array)
        expect(Booking.release_reasons).to include("Bonded")
      end
    end
      
    describe ".update_sentences_by_hour" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  
    describe ".update_cashless_systems" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  
    describe ".send_problem_notifications" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  
    describe ".process_query" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  end
  
  describe "Instance Methods" do
    describe "#allow_arraignment_notice?" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  
    describe "#billing_status" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  
    describe "#reasons_not_destroyable" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  
    describe "#current_location" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  
    describe "#doc?" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  
    describe "#doc_billable?" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  
    describe "#doc_incomplete?" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  
    describe "#hold_for_probation" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  
    describe "#hold_summary" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  
    describe "#hours_served_since" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  
    describe "#is_expired" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#is_housing" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#is_serving_time" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#is_time_served" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#is_transferred" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#last_booking_logs" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#minutes_served_since" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#problems" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#property_summary" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#releasable?" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#simple_time_served_by_log" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#start_time" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#status" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#stop_time" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#temporary_release?" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#temporary_release_reason" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#time_served_by_log" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#total_remaining_hours" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#total_remaining_days" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#total_remaining_text" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#check_destroyable" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#check_person" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#check_timers" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#lookup_officers" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#process_bond_recalls" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#send_new_messages" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#send_release_messages" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  end
end
