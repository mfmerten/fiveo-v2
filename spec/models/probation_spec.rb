# == Schema Information
#
# Table name: probations
#
#  id                      :integer          not null, primary key
#  person_id               :integer          not null
#  start_date              :date
#  end_date                :date
#  completed_date          :date
#  revoked_date            :date
#  revoked_for             :string(255)      default(""), not null
#  hold_if_arrested        :boolean          default(TRUE), not null
#  probation_days          :integer          default(0), not null
#  probation_months        :integer          default(0), not null
#  probation_years         :integer          default(0), not null
#  costs                   :decimal(12, 2)   default(0.0), not null
#  notes                   :text             default(""), not null
#  remarks                 :text             default(""), not null
#  created_by              :integer          default(1)
#  updated_by              :integer          default(1)
#  created_at              :datetime
#  updated_at              :datetime
#  probation_fees          :decimal(12, 2)   default(0.0), not null
#  idf_amount              :decimal(12, 2)   default(0.0), not null
#  dare_amount             :decimal(12, 2)   default(0.0), not null
#  restitution_amount      :decimal(12, 2)   default(0.0), not null
#  probation_officer       :string(255)      default(""), not null
#  probation_officer_unit  :string(255)      default(""), not null
#  probation_officer_badge :string(255)      default(""), not null
#  fines                   :decimal(12, 2)   default(0.0), not null
#  docket_id               :integer
#  court_id                :integer
#  da_charge_id            :integer
#  doc                     :boolean          default(FALSE), not null
#  parish_jail             :boolean          default(FALSE), not null
#  trial_result            :string(255)      default(""), not null
#  default_days            :integer          default(0), not null
#  default_months          :integer          default(0), not null
#  default_years           :integer          default(0), not null
#  pay_by_date             :date
#  sentence_days           :integer          default(0), not null
#  sentence_months         :integer          default(0), not null
#  sentence_years          :integer          default(0), not null
#  sentence_suspended      :boolean          default(FALSE), not null
#  suspended_except_days   :integer          default(0), not null
#  suspended_except_months :integer          default(0), not null
#  suspended_except_years  :integer          default(0), not null
#  reverts_to_unsup        :boolean          default(FALSE), not null
#  reverts_conditions      :string(255)      default(""), not null
#  credit_served           :boolean          default(FALSE), not null
#  service_days            :integer          default(0), not null
#  service_served          :integer          default(0), not null
#  sap                     :boolean          default(FALSE), not null
#  sap_complete            :boolean          default(FALSE), not null
#  driver                  :boolean          default(FALSE), not null
#  driver_complete         :boolean          default(FALSE), not null
#  anger                   :boolean          default(FALSE), not null
#  anger_complete          :boolean          default(FALSE), not null
#  sat                     :boolean          default(FALSE), not null
#  sat_complete            :boolean          default(FALSE), not null
#  random_screens          :boolean          default(FALSE), not null
#  no_victim_contact       :boolean          default(FALSE), not null
#  art893                  :boolean          default(FALSE), not null
#  art894                  :boolean          default(FALSE), not null
#  art895                  :boolean          default(FALSE), not null
#  sentence_notes          :text             default(""), not null
#  docket_no               :string(255)      default(""), not null
#  conviction              :string(255)      default(""), not null
#  conviction_count        :integer          default(1), not null
#  restitution_date        :date
#  probation_hours         :integer          default(0), not null
#  default_hours           :integer          default(0), not null
#  sentence_hours          :integer          default(0), not null
#  suspended_except_hours  :integer          default(0), not null
#  service_hours           :integer          default(0), not null
#  fund1_name              :string(255)      default(""), not null
#  fund2_name              :string(255)      default(""), not null
#  fund3_name              :string(255)      default(""), not null
#  fund4_name              :string(255)      default(""), not null
#  fund5_name              :string(255)      default(""), not null
#  fund6_name              :string(255)      default(""), not null
#  fund7_name              :string(255)      default(""), not null
#  fund8_name              :string(255)      default(""), not null
#  fund9_name              :string(255)      default(""), not null
#  fund1_charged           :decimal(12, 2)   default(0.0), not null
#  fund2_charged           :decimal(12, 2)   default(0.0), not null
#  fund3_charged           :decimal(12, 2)   default(0.0), not null
#  fund4_charged           :decimal(12, 2)   default(0.0), not null
#  fund5_charged           :decimal(12, 2)   default(0.0), not null
#  fund6_charged           :decimal(12, 2)   default(0.0), not null
#  fund7_charged           :decimal(12, 2)   default(0.0), not null
#  fund8_charged           :decimal(12, 2)   default(0.0), not null
#  fund9_charged           :decimal(12, 2)   default(0.0), not null
#  fund1_paid              :decimal(12, 2)   default(0.0), not null
#  fund2_paid              :decimal(12, 2)   default(0.0), not null
#  fund3_paid              :decimal(12, 2)   default(0.0), not null
#  fund4_paid              :decimal(12, 2)   default(0.0), not null
#  fund5_paid              :decimal(12, 2)   default(0.0), not null
#  fund6_paid              :decimal(12, 2)   default(0.0), not null
#  fund7_paid              :decimal(12, 2)   default(0.0), not null
#  fund8_paid              :decimal(12, 2)   default(0.0), not null
#  fund9_paid              :decimal(12, 2)   default(0.0), not null
#  sentence_life           :integer          default(0), not null
#  sentence_death          :boolean          default(FALSE), not null
#  suspended_except_life   :integer          default(0), not null
#  status                  :string(255)      default(""), not null
#  pay_during_prob         :boolean          default(FALSE), not null
#  terminate_reason        :string(255)      default(""), not null
#
# Indexes
#
#  index_probations_on_court_id      (court_id)
#  index_probations_on_created_by    (created_by)
#  index_probations_on_da_charge_id  (da_charge_id)
#  index_probations_on_docket_id     (docket_id)
#  index_probations_on_person_id     (person_id)
#  index_probations_on_updated_by    (updated_by)
#
# Foreign Keys
#
#  probations_court_id_fk      (court_id => courts.id)
#  probations_created_by_fk    (created_by => users.id)
#  probations_da_charge_id_fk  (da_charge_id => da_charges.id)
#  probations_docket_id_fk     (docket_id => dockets.id)
#  probations_person_id_fk     (person_id => people.id)
#  probations_updated_by_fk    (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Probation do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
