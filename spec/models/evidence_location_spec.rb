# == Schema Information
#
# Table name: evidence_locations
#
#  id               :integer          not null, primary key
#  evidence_id      :integer          not null
#  location_from    :string(255)      default(""), not null
#  location_to      :string(255)      default(""), not null
#  officer          :string(255)      default(""), not null
#  remarks          :string(255)      default(""), not null
#  created_at       :datetime
#  updated_at       :datetime
#  officer_unit     :string(255)      default(""), not null
#  officer_badge    :string(255)      default(""), not null
#  storage_datetime :datetime
#  officer_id       :integer
#
# Indexes
#
#  index_evidence_locations_on_evidence_id  (evidence_id)
#  index_evidence_locations_on_officer_id   (officer_id)
#
# Foreign Keys
#
#  evidence_locations_evidence_id_fk  (evidence_id => evidences.id)
#

require 'rails_helper'

RSpec.describe EvidenceLocation do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
