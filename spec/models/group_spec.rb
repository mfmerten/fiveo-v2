# == Schema Information
#
# Table name: groups
#
#  id          :integer          not null, primary key
#  name        :string(255)      default(""), not null
#  description :string(255)      default(""), not null
#  created_by  :integer          default(1)
#  updated_by  :integer          default(1)
#  created_at  :datetime
#  updated_at  :datetime
#  permissions :string(255)      default(""), not null
#
# Indexes
#
#  index_groups_on_created_by  (created_by)
#  index_groups_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  groups_created_by_fk  (created_by => users.id)
#  groups_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Group do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
