# == Schema Information
#
# Table name: docs
#
#  id                :integer          not null, primary key
#  transfer_id       :integer
#  sentence_days     :integer          default(0), not null
#  sentence_months   :integer          default(0), not null
#  sentence_years    :integer          default(0), not null
#  to_serve_days     :integer          default(0), not null
#  to_serve_months   :integer          default(0), not null
#  to_serve_years    :integer          default(0), not null
#  conviction        :string(255)      default(""), not null
#  billable          :boolean          default(FALSE), not null
#  bill_from_date    :date
#  remarks           :text             default(""), not null
#  created_by        :integer          default(1)
#  updated_by        :integer          default(1)
#  created_at        :datetime
#  updated_at        :datetime
#  hold_id           :integer
#  sentence_id       :integer
#  doc_number        :string(255)      default(""), not null
#  booking_id        :integer          not null
#  person_id         :integer          not null
#  revocation_id     :integer
#  sentence_hours    :integer          default(0), not null
#  to_serve_hours    :integer          default(0), not null
#  problems          :string(255)      default(""), not null
#  sentence_life     :integer          default(0), not null
#  sentence_death    :boolean          default(FALSE), not null
#  to_serve_life     :integer          default(0), not null
#  to_serve_death    :boolean          default(FALSE), not null
#  transfer_datetime :datetime
#
# Indexes
#
#  index_docs_on_booking_id     (booking_id)
#  index_docs_on_created_by     (created_by)
#  index_docs_on_hold_id        (hold_id)
#  index_docs_on_person_id      (person_id)
#  index_docs_on_revocation_id  (revocation_id)
#  index_docs_on_sentence_id    (sentence_id)
#  index_docs_on_transfer_id    (transfer_id)
#  index_docs_on_updated_by     (updated_by)
#
# Foreign Keys
#
#  docs_booking_id_fk     (booking_id => bookings.id)
#  docs_created_by_fk     (created_by => users.id)
#  docs_hold_id_fk        (hold_id => holds.id)
#  docs_person_id_fk      (person_id => people.id)
#  docs_revocation_id_fk  (revocation_id => revocations.id)
#  docs_sentence_id_fk    (sentence_id => sentences.id)
#  docs_transfer_id_fk    (transfer_id => transfers.id)
#  docs_updated_by_fk     (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Doc do
  
  ## table column
  #it { is_expected.to have_db_column(:transfer_id).of_type(:integer) }
  #it { is_expected.to have_db_column(:sentence_days).of_type(:integer).with_options(null: false, default: 0) }
  #it { is_expected.to have_db_column(:sentence_months).of_type(:integer).with_options(null: false, default: 0) }
  #it { is_expected.to have_db_column(:sentence_years).of_type(:integer).with_options(null: false, default: 0) }
  #it { is_expected.to have_db_column(:sentence_life).of_type(:integer).with_options(null: false, default: 0) }
  #it { is_expected.to have_db_column(:sentence_death).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:to_serve_days).of_type(:integer).with_options(null: false, default: 0) }
  #it { is_expected.to have_db_column(:to_serve_months).of_type(:integer).with_options(null: false, default: 0) }
  #it { is_expected.to have_db_column(:to_serve_years).of_type(:integer).with_options(null: false, default: 0) }
  #it { is_expected.to have_db_column(:to_serve_life).of_type(:integer).with_options(null: false, default: 0) }
  #it { is_expected.to have_db_column(:to_serve_death).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:sentence_hours).of_type(:integer).with_options(null: false, default: 0) }
  #it { is_expected.to have_db_column(:to_serve_hours).of_type(:integer).with_options(null: false, default: 0) }
  #it { is_expected.to have_db_column(:conviction).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:billable).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:bill_from_date).of_type(:date) }
  #it { is_expected.to have_db_column(:remarks).of_type(:text).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:hold_id).of_type(:integer).with_options(null: false) }
  #it { is_expected.to have_db_column(:sentence_id).of_type(:integer) }
  #it { is_expected.to have_db_column(:doc_number).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:booking_id).of_type(:integer).with_options(null: false) }
  #it { is_expected.to have_db_column(:person_id).of_type(:integer).with_options(null: false) }
  #it { is_expected.to have_db_column(:revocation_id).of_type(:integer) }
  #it { is_expected.to have_db_column(:problems).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:transfer_datetime).of_type(:datetime) }
  #
  ## table indexes
  #it { is_expected.to have_db_index(:booking_id) }
  #it { is_expected.to have_db_index(:created_by) }
  #it { is_expected.to have_db_index(:hold_id) }
  #it { is_expected.to have_db_index(:person_id) }
  #it { is_expected.to have_db_index(:revocation_id) }
  #it { is_expected.to have_db_index(:sentence_id) }
  #it { is_expected.to have_db_index(:transfer_id) }
  #it { is_expected.to have_db_index(:updated_by) }
  #
  ## associations
  #it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:created_by) }
  #it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key(:updated_by) }
  #it { is_expected.to belong_to(:transfer).inverse_of(:doc) }
  #it { is_expected.to belong_to(:hold).dependent(:destroy).inverse_of(:doc_record) }
  #it { is_expected.to belong_to(:booking).inverse_of(:docs) }
  #it { is_expected.to belong_to(:person).inverse_of(:docs) }
  #it { is_expected.to belong_to(:sentence).inverse_of(:doc_record) }
  #it { is_expected.to belong_to(:revocation).inverse_of(:doc_record) }
  #
  ## attributes
  #describe ":force" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## scopes
  #describe "scopes" do
  #  before(:context) do
  #    # create test records
  #  end
  #  
  #  it "should return docs with problems using :with_problems scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return active docs using :active scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return docs ordered by name using :by_name scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return specific doc using :with_doc_id scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return docs for specific doc_number using :with_doc_number scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return docs for specific booking using :with_booking_id scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return docs for specific person using :with_person_id scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return docs for specific sex using :with_sex scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_conviction scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_bool when using :with_billable scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_remarks scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_person when using :with_person_name scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_daterange when using :with_transfer_date" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## callbacks
  #describe "before_validation" do
  #  describe ":validate_booking" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  #describe "before_save" do
  #  describe ":fix_numbers" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #  
  #  describe ":process" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #  
  #  describe ":check_creator" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  #describe "after_update" do
  #  describe ":update_person" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  ## validations
  #it { is_expected.to validate_numericality_of(:to_serve_hours).only_integer(true).allow_nil(true) }
  #it { is_expected.to validate_numericality_of(:to_serve_days).only_integer(true).allow_nil(true) }
  #it { is_expected.to validate_numericality_of(:to_serve_months).only_integer(true).allow_nil(true) }
  #it { is_expected.to validate_numericality_of(:to_serve_years).only_integer(true).allow_nil(true) }
  #it { is_expected.to validate_numericality_of(:to_serve_life).only_integer(true).allow_nil(true) }
  #it { is_expected.to validate_numericality_of(:sentence_hours).only_integer(true).allow_nil(true) }
  #it { is_expected.to validate_numericality_of(:sentence_days).only_integer(true).allow_nil(true) }
  #it { is_expected.to validate_numericality_of(:sentence_months).only_integer(true).allow_nil(true) }
  #it { is_expected.to validate_numericality_of(:sentence_years).only_integer(true).allow_nil(true) }
  #it { is_expected.to validate_numericality_of(:sentence_life).only_integer(true).allow_nil(true) }
  #
  ## methods
  #it { is_expected.to have_defined_desc }
  #it { is_expected.to have_defined_base_perms }
  #
  #describe ".process_docs" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".process_query" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#missing_information" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#needs_information" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#sentence_string" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#to_serve_string" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
end
