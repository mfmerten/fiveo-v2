# == Schema Information
#
# Table name: external_links
#
#  id          :integer          not null, primary key
#  name        :string(255)      default(""), not null
#  short_name  :string(255)      default(""), not null
#  url         :string(255)      default(""), not null
#  description :text             default(""), not null
#  front_page  :boolean          default(FALSE), not null
#  created_by  :integer          default(1)
#  updated_by  :integer          default(1)
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_external_links_on_created_by  (created_by)
#  index_external_links_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  external_links_created_by_fk  (created_by => users.id)
#  external_links_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe ExternalLink do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
