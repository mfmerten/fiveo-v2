# == Schema Information
#
# Table name: users
#
#  id                      :integer          not null, primary key
#  login                   :string(255)      default(""), not null
#  locked                  :boolean          default(FALSE), not null
#  old_hashed_password     :string(255)      default(""), not null
#  salt                    :string(255)      default(""), not null
#  items_per_page          :integer          default(15), not null
#  expires                 :date
#  created_at              :datetime
#  contact_id              :integer
#  created_by              :integer          default(1)
#  updated_by              :integer          default(1)
#  updated_at              :datetime
#  disabled_text           :string(255)      default(""), not null
#  user_photo_file_name    :string(255)
#  user_photo_content_type :string(255)
#  user_photo_file_size    :integer
#  user_photo_updated_at   :datetime
#  send_email              :boolean          default(FALSE), not null
#  show_tips               :boolean          default(TRUE), not null
#  notify_booking_release  :boolean          default(FALSE), not null
#  notify_booking_new      :boolean          default(FALSE), not null
#  notify_arrest_new       :boolean          default(FALSE), not null
#  notify_call_new         :boolean          default(FALSE), not null
#  notify_offense_new      :boolean          default(FALSE), not null
#  notify_forbid_new       :boolean          default(FALSE), not null
#  notify_forbid_recalled  :boolean          default(FALSE), not null
#  notify_pawn_new         :boolean          default(FALSE), not null
#  notify_warrant_new      :boolean          default(FALSE), not null
#  notify_warrant_resolved :boolean          default(FALSE), not null
#  message_retention       :integer          default(0), not null
#  notify_booking_problems :boolean          default(FALSE), not null
#  forum_auto_subscribe    :boolean          default(FALSE), not null
#  hashed_password         :string(255)      default(""), not null
#
# Indexes
#
#  index_users_on_contact_id  (contact_id)
#  index_users_on_created_by  (created_by)
#  index_users_on_login       (login)
#  index_users_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  users_contact_id_fk  (contact_id => contacts.id)
#  users_created_by_fk  (created_by => users.id)
#  users_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe User do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
