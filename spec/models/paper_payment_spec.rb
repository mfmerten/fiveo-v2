# == Schema Information
#
# Table name: paper_payments
#
#  id                :integer          not null, primary key
#  paper_id          :integer
#  paper_customer_id :integer          not null
#  transaction_date  :date
#  officer           :string(255)      default(""), not null
#  officer_unit      :string(255)      default(""), not null
#  officer_badge     :string(255)      default(""), not null
#  receipt_no        :string(255)      default(""), not null
#  payment           :decimal(12, 2)   default(0.0), not null
#  charge            :decimal(12, 2)   default(0.0), not null
#  report_datetime   :datetime
#  memo              :string(255)      default(""), not null
#  created_by        :integer          default(1)
#  updated_by        :integer          default(1)
#  created_at        :datetime
#  updated_at        :datetime
#  payment_method    :string(255)      default(""), not null
#  paid_in_full      :boolean          default(FALSE), not null
#  adjustment        :boolean          default(FALSE), not null
#
# Indexes
#
#  index_paper_payments_on_created_by         (created_by)
#  index_paper_payments_on_paper_customer_id  (paper_customer_id)
#  index_paper_payments_on_paper_id           (paper_id)
#  index_paper_payments_on_updated_by         (updated_by)
#
# Foreign Keys
#
#  paper_payments_created_by_fk         (created_by => users.id)
#  paper_payments_paper_customer_id_fk  (paper_customer_id => paper_customers.id)
#  paper_payments_paper_id_fk           (paper_id => papers.id)
#  paper_payments_updated_by_fk         (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe PaperPayment do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
