# == Schema Information
#
# Table name: absent_prisoners
#
#  id         :integer          not null, primary key
#  absent_id  :integer          not null
#  booking_id :integer          not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_absent_prisoners_on_absent_id   (absent_id)
#  index_absent_prisoners_on_booking_id  (booking_id)
#
# Foreign Keys
#
#  absent_prisoners_absent_id_fk   (absent_id => absents.id)
#  absent_prisoners_booking_id_fk  (booking_id => bookings.id)
#

require 'rails_helper'

RSpec.describe AbsentPrisoner do
  
  describe "Table Columns" do
    it { is_expected.to have_db_column(:absent_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:booking_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  end
  
  describe "Table Indices" do
    it { is_expected.to have_db_index(:absent_id) }
    it { is_expected.to have_db_index(:booking_id) }
  end
  
  describe "Associations" do
    it { is_expected.to belong_to(:absent).inverse_of(:absent_prisoners) }
    it { is_expected.to belong_to(:booking).inverse_of(:absent_prisoners) }
  end
  
  describe "Class Methods" do
    describe ".desc" do
      it { is_expected.to have_defined_desc }
    end
  end

end
