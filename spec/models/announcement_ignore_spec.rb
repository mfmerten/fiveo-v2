# == Schema Information
#
# Table name: announcement_ignores
#
#  id              :integer          not null, primary key
#  announcement_id :integer          not null
#  user_id         :integer          not null
#  created_at      :datetime
#  updated_at      :datetime
#
# Indexes
#
#  index_announcement_ignores_on_announcement_id  (announcement_id)
#  index_announcement_ignores_on_user_id          (user_id)
#
# Foreign Keys
#
#  announcement_ignores_announcement_id_fk  (announcement_id => announcements.id)
#  announcement_ignores_user_id_fk          (user_id => users.id)
#

require 'rails_helper'

RSpec.describe AnnouncementIgnore do
  
  describe "Table Columns" do
    it { is_expected.to have_db_column(:announcement_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:user_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  end
  
  describe "Table Indices" do
    it { is_expected.to have_db_index(:announcement_id) }
    it { is_expected.to have_db_index(:user_id) }
  end
  
  describe "Associations" do
    it { is_expected.to belong_to(:announcement).inverse_of(:announcement_ignores) }
    it { is_expected.to belong_to(:user).inverse_of(:announcement_ignores) }
  end
  
  describe "Class Methods" do
    describe ".desc" do
      it { is_expected.to have_defined_desc }
    end
  end
  
end
