# == Schema Information
#
# Table name: commissary_items
#
#  id          :integer          not null, primary key
#  description :string(255)      default(""), not null
#  price       :decimal(12, 2)   default(0.0), not null
#  active      :boolean          default(TRUE), not null
#  created_by  :integer          default(1)
#  updated_by  :integer          default(1)
#  created_at  :datetime
#  updated_at  :datetime
#  jail_id     :integer          not null
#
# Indexes
#
#  index_commissary_items_on_created_by  (created_by)
#  index_commissary_items_on_jail_id     (jail_id)
#  index_commissary_items_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  commissary_items_created_by_fk  (created_by => users.id)
#  commissary_items_jail_id_fk     (jail_id => jails.id)
#  commissary_items_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe CommissaryItem do
  
  ## table columns
  #it { is_expected.to have_db_column(:description).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:price).of_type(:decimal).with_options(scale: 12, precision: 2, null: false, default: 0.0) }
  #it { is_expected.to have_db_column(:active).of_type(:boolean).with_options(null: false, default: true) }
  #it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_opitons(default: 1) }
  #it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:jail_id).of_type(:integer).with_options(null: false) }
  #
  ## table indexes
  #it { is_expected.to have_db_index(:created_by) }
  #it { is_expected.to have_db_index(:updated_by) }
  #it { is_expected.to have_db_index(:jail_id) }
  #
  ## associations
  #it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:created_by) }
  #it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key(:updated_by) }
  #it { is_expected.to belong_to(:jail).inverse_of(:commissary_items) }
  #
  ## scopes
  #describe "scopes" do
  #  before(:context) do
  #    # create test records
  #  end
  #  
  #  it "should return commissary items in description order using :by_description scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return commissary items belonging to jail using :belonging_to_jail scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return commissary item 1 but not commissary item 2 using :with_commissary_item_id scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_description scope" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## callbacks
  #
  #describe "after_save" do
  #  describe ":check_creator" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  #describe "before_validation" do
  #  describe ":fix_description" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  ## validations
  #
  #it { is_expected.to validate_presence_of(:description) }
  #it { is_expected.to validate_numericality_of(:price).is_greater_than(0).is_less_than(10 ** 10) }
  #
  ## methods
  #it { is_expected.to have_defined_desc }
  #it { is_expected.to have_defined_base_perms }
  #
  #describe ".process_query" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
end
