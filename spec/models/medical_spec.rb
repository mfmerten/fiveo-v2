# == Schema Information
#
# Table name: medicals
#
#  id              :integer          not null, primary key
#  booking_id      :integer          not null
#  arrest_injuries :string(255)      default(""), not null
#  officer         :string(255)      default(""), not null
#  officer_unit    :string(255)      default(""), not null
#  disposition     :string(255)      default(""), not null
#  rec_med_tmnt    :string(255)      default(""), not null
#  rec_dental_tmnt :string(255)      default(""), not null
#  rec_mental_tmnt :string(255)      default(""), not null
#  current_meds    :string(255)      default(""), not null
#  allergies       :string(255)      default(""), not null
#  injuries        :string(255)      default(""), not null
#  infestation     :string(255)      default(""), not null
#  alcohol         :string(255)      default(""), not null
#  drug            :string(255)      default(""), not null
#  withdrawal      :string(255)      default(""), not null
#  attempt_suicide :string(255)      default(""), not null
#  suicide_risk    :string(255)      default(""), not null
#  danger          :string(255)      default(""), not null
#  pregnant        :string(255)      default(""), not null
#  deformities     :string(255)      default(""), not null
#  heart_disease   :boolean          default(FALSE), not null
#  blood_pressure  :boolean          default(FALSE), not null
#  diabetes        :boolean          default(FALSE), not null
#  epilepsy        :boolean          default(FALSE), not null
#  hepatitis       :boolean          default(FALSE), not null
#  hiv             :boolean          default(FALSE), not null
#  tb              :boolean          default(FALSE), not null
#  ulcers          :boolean          default(FALSE), not null
#  venereal        :boolean          default(FALSE), not null
#  created_by      :integer          default(1)
#  updated_by      :integer          default(1)
#  created_at      :datetime
#  updated_at      :datetime
#  officer_badge   :string(255)      default(""), not null
#  screen_datetime :datetime
#
# Indexes
#
#  index_medicals_on_booking_id  (booking_id)
#  index_medicals_on_created_by  (created_by)
#  index_medicals_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  medicals_booking_id_fk  (booking_id => bookings.id)
#  medicals_created_by_fk  (created_by => users.id)
#  medicals_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Medical do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
