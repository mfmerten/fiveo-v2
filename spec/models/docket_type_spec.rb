# == Schema Information
#
# Table name: docket_types
#
#  id         :integer          not null, primary key
#  name       :string(255)      default(""), not null
#  created_by :integer          default(1)
#  updated_by :integer          default(1)
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_docket_types_on_created_by  (created_by)
#  index_docket_types_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  docket_types_created_by_fk  (created_by => users.id)
#  docket_types_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe DocketType do
  
  ## table columns
  #it { is_expected.to have_db_column(:name).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  #
  ## table indexes
  #it { is_expected.to have_db_index(:created_by) }
  #it { is_expected.to have_db_index(:updated_by) }
  #
  ## associations
  #it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:created_by) }
  #it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:updated_by) }
  #
  ## scopes
  #describe "scopes" do
  #  before(:context) do
  #    # create test records
  #  end
  #  
  #  it "should have created test records" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return records in name order using :by_name scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  after(:context) do
  #    # destroy test records unless nil
  #  end
  #end
  #
  ## callbacks
  #describe "before_save" do
  #  describe ":check_creator" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  ## validations
  #it { is_expected.to validate_uniqueness_of(:name) }
  #
  ## methods
  #it { is_expected.to have_defined_desc }
  #it { is_expected.to have_defined_base_perms }
  #
  #describe ".types_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
end
