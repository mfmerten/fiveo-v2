# == Schema Information
#
# Table name: dockets
#
#  id         :integer          not null, primary key
#  docket_no  :string(255)      default(""), not null
#  person_id  :integer          not null
#  created_by :integer          default(1)
#  updated_by :integer          default(1)
#  created_at :datetime
#  updated_at :datetime
#  processed  :boolean          default(FALSE), not null
#  notes      :text             default(""), not null
#  remarks    :text             default(""), not null
#  misc       :boolean          default(FALSE), not null
#
# Indexes
#
#  index_dockets_on_created_by  (created_by)
#  index_dockets_on_docket_no   (docket_no)
#  index_dockets_on_person_id   (person_id)
#  index_dockets_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  dockets_created_by_fk  (created_by => users.id)
#  dockets_person_id_fk   (person_id => people.id)
#  dockets_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Docket do
  
  ## table columns
  #it { is_expected.to have_db_column(:docket_no).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:person_id).of_type(:integer).with_options(null: false) }
  #it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:processed).of_type(:boolean).with_options(default: false, null: false) }
  #it { is_expected.to have_db_column(:notes).of_type(:text).with_options(default: '', null: false) }
  #it { is_expected.to have_db_column(:remarks).of_type(:text).with_options(default: '', null: false) }
  #it { is_expected.to have_db_column(:misc).of_type(:boolean).with_options(default: false, null: false) }
  #
  ## table indexes
  #it { is_expected.to have_db_index(:created_by) }
  #it { is_expected.to have_db_index(:updated_by) }
  #it { is_expected.to have_db_index(:person_id) }
  #it { is_expected.to have_db_index(:docket_no) }
  #
  ## associations
  #it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:created_by) }
  #it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key(:updated_by) }
  #it { is_expected.to belong_to(:person).inverse_of(:dockets) }
  #it { is_expected.to have_many(:probations).dependent(:destroy) }
  #it { is_expected.to have_many(:courts).dependent(:destroy) }
  #it { is_expected.to have_many(:sentences).dependent(:destroy).autosave(false) }
  #it { is_expected.to have_many(:revocations).dependent(:destroy) }
  #it { is_expected.to have_many(:da_charges).dependent(:destroy) }
  #it { is_expected.to have_many(:charges).through(:da_charges).source(:arrest_charge) }
  #it { is_expected.to have_many(:arrests).through(:charges) }
  #it { is_expected.to have_many(:citations).through(:charges) }
  #it { is_expected.to have_many(:warrants).through(:charges) }
  #it { is_expected.to have_many(:da_charge_arrests).through(:da_charges).source(:arrest) }
  #it { is_expected.to have_many(:revocation_consecutives).dependent(:destroy) }
  #it { is_expected.to have_many(:sentence_consecutives).dependent(:destroy) }
  #
  ## attributes
  #it { is_expected.to accept_nested_attributes_for(:da_charges).allow_destroy(:true) }
  #
  ## scopes
  #describe "scopes" do
  #  before(:context) do
  #    # create records for testing
  #  end
  #  
  #  it "should return non-misc dockets using :not_misc scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return dockets in docket_no order using :by_docket_no scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return dockets with arrests matching case number using :with_arrests_case_no scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return dockets da_charge arrests matching case number using :with_da_charge_arrests_case_no scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_docket_no scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return dockets for a particular person using :with_person_id scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_judge scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_docket_type scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return dockets with any arrest with a particular case number using :with_case_no scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_person when using :with_person_name scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_date when using :with_court_date scope" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## callbacks
  #
  #describe "before_save" do
  #  describe ":check_creator" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  #describe "before_validation" do
  #  describe ":fix_docket_no" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  #describe "after_save" do
  #  describe ":check_misc" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  ## validations
  #
  #describe ":da_charges" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #it { is_expected.to validate_presence_of(:docket_no) }
  #it { is_expected.to validate_uniqueness_of(:docket_no).scoped_to(:person_id).with_message("already exists for this person") }
  #it { is_expected.to validate_person(:person_id) }
  #
  ## constants
  #
  #describe "ConsecutiveType" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "CourtCostCategory" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "PleaType" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "TrialResult" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## methods
  #it { is_expected.to have_defined_desc }
  #it { is_expected.to have_defined_base_perms }
  #
  #describe ".dockets_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".find_all_by_case_no" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".process_dockets" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".process_query" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#bonds" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#case_numbers" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#charge_summary" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#linked_charges=" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#next_courts" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#original_sentence_date" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#process_docket" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#sentence_summary" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#times_served_in_hours" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#total_sentence_length" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
end
