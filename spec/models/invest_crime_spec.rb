# == Schema Information
#
# Table name: invest_crimes
#
#  id                  :integer          not null, primary key
#  investigation_id    :integer          not null
#  private             :boolean          default(TRUE), not null
#  motive              :string(255)      default(""), not null
#  location            :string(255)      default(""), not null
#  occupied            :boolean          default(FALSE), not null
#  entry_gained        :string(255)      default(""), not null
#  weapons_used        :string(255)      default(""), not null
#  facts_prior         :text             default(""), not null
#  facts_after         :text             default(""), not null
#  targets             :text             default(""), not null
#  security            :string(255)      default(""), not null
#  impersonated        :string(255)      default(""), not null
#  actions_taken       :text             default(""), not null
#  threats_made        :text             default(""), not null
#  details             :text             default(""), not null
#  remarks             :text             default(""), not null
#  created_by          :integer          default(1)
#  updated_by          :integer          default(1)
#  created_at          :datetime
#  updated_at          :datetime
#  owned_by            :integer          default(1)
#  occurred_datetime   :datetime
#  victim_relationship :string(255)      default(""), not null
#
# Indexes
#
#  index_invest_crimes_on_created_by        (created_by)
#  index_invest_crimes_on_investigation_id  (investigation_id)
#  index_invest_crimes_on_owned_by          (owned_by)
#  index_invest_crimes_on_updated_by        (updated_by)
#
# Foreign Keys
#
#  invest_crimes_created_by_fk        (created_by => users.id)
#  invest_crimes_investigation_id_fk  (investigation_id => investigations.id)
#  invest_crimes_owned_by_fk          (owned_by => users.id)
#  invest_crimes_updated_by_fk        (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe InvestCrime do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
