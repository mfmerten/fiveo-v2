# == Schema Information
#
# Table name: invest_reports
#
#  id               :integer          not null, primary key
#  investigation_id :integer          not null
#  details          :text             default(""), not null
#  remarks          :text             default(""), not null
#  private          :boolean          default(TRUE), not null
#  created_by       :integer          default(1)
#  updated_by       :integer          default(1)
#  created_at       :datetime
#  updated_at       :datetime
#  owned_by         :integer          default(1)
#  report_datetime  :datetime
#  supplemental     :boolean          default(FALSE), not null
#
# Indexes
#
#  index_invest_reports_on_created_by        (created_by)
#  index_invest_reports_on_investigation_id  (investigation_id)
#  index_invest_reports_on_owned_by          (owned_by)
#  index_invest_reports_on_updated_by        (updated_by)
#
# Foreign Keys
#
#  invest_reports_created_by_fk        (created_by => users.id)
#  invest_reports_investigation_id_fk  (investigation_id => investigations.id)
#  invest_reports_owned_by_fk          (owned_by => users.id)
#  invest_reports_updated_by_fk        (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe InvestReport do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
