# == Schema Information
#
# Table name: forum_topics
#
#  id           :integer          not null, primary key
#  forum_id     :integer          not null
#  user_id      :integer          not null
#  subject      :string(255)      default(""), not null
#  locked       :boolean          default(FALSE), not null
#  pinned       :boolean          default(FALSE), not null
#  hidden       :boolean          default(FALSE), not null
#  last_post_at :datetime
#  views_count  :integer          default(0), not null
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_forum_topics_on_forum_id  (forum_id)
#  index_forum_topics_on_user_id   (user_id)
#
# Foreign Keys
#
#  forum_topics_forum_id_fk  (forum_id => forums.id)
#  forum_topics_user_id_fk   (user_id => users.id)
#

require 'rails_helper'

RSpec.describe ForumTopic do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
