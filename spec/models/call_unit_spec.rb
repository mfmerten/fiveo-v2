# == Schema Information
#
# Table name: call_units
#
#  id            :integer          not null, primary key
#  call_id       :integer          not null
#  officer_unit  :string(255)      default(""), not null
#  officer       :string(255)      default(""), not null
#  created_at    :datetime
#  updated_at    :datetime
#  officer_badge :string(255)      default(""), not null
#
# Indexes
#
#  index_call_units_on_call_id  (call_id)
#
# Foreign Keys
#
#  call_units_call_id_fk  (call_id => calls.id)
#

require 'rails_helper'

RSpec.describe CallUnit do
  
  describe "Table Columns" do
    it { is_expected.to have_db_column(:call_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:officer).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:officer_unit).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:officer_badge).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  end
  
  describe "Table Indices" do
    it { is_expected.to have_db_index(:call_id) }
  end
  
  describe "Associations" do
    it { is_expected.to belong_to(:call).inverse_of(:call_units) }
  end
  
  describe "Attributes" do
    it { is_expected.to have_attr_accessor(:officer_id) }
  end
  
  describe "Callbacks" do
    describe "before_validation" do
      describe ":lookup_officers" do
        it "should call #lookup_officers prior to validation" do
          c = build(:call_unit)
          allow(c).to receive(:lookup_officers)
          c.valid?
          expect(c).to have_received(:lookup_officers).with(no_args)
        end
      end
    end
  end
  
  describe "Validations" do
    it { is_expected.to validate_presence_of(:officer) }
  end
  
  describe "Class Methods" do
    it { is_expected.to have_defined_desc }
  end
  
  describe "Instance Methods" do
    describe "#to_s" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#lookup_officers" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  end
end
