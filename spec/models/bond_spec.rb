# == Schema Information
#
# Table name: bonds
#
#  id                :integer          not null, primary key
#  surety_id         :integer
#  person_id         :integer          not null
#  arrest_id         :integer
#  bond_amt          :decimal(12, 2)   default(0.0), not null
#  remarks           :text             default(""), not null
#  power             :string(255)      default(""), not null
#  bond_no           :string(255)      default(""), not null
#  created_by        :integer          default(1)
#  updated_by        :integer          default(1)
#  created_at        :datetime
#  updated_at        :datetime
#  issued_datetime   :datetime
#  status_datetime   :datetime
#  final             :boolean          default(FALSE), not null
#  status            :string(255)      default(""), not null
#  bond_type         :string(255)      default(""), not null
#  bondsman_name     :string(255)      default(""), not null
#  bond_company      :string(255)      default(""), not null
#  bondsman_address1 :string(255)      default(""), not null
#  bondsman_address2 :string(255)      default(""), not null
#  bondsman_address3 :string(255)      default(""), not null
#
# Indexes
#
#  index_bonds_on_arrest_id   (arrest_id)
#  index_bonds_on_created_by  (created_by)
#  index_bonds_on_person_id   (person_id)
#  index_bonds_on_surety_id   (surety_id)
#  index_bonds_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  bonds_arrest_id_fk   (arrest_id => arrests.id)
#  bonds_created_by_fk  (created_by => users.id)
#  bonds_person_id_fk   (person_id => people.id)
#  bonds_surety_id_fk   (surety_id => people.id)
#  bonds_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Bond do
  
  describe "Table Columns" do
    it { is_expected.to have_db_column(:surety_id).of_type(:integer) }
    it { is_expected.to have_db_column(:person_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:arrest_id).of_type(:integer) }
    it { is_expected.to have_db_column(:bond_amt).of_type(:decimal).with_options(scale: 2, precision: 12, null: false, default: 0.0) }
    it { is_expected.to have_db_column(:remarks).of_type(:text).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:power).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:bond_no).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
    it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_options(default: 1) }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:issued_datetime).of_type(:datetime) }
    it { is_expected.to have_db_column(:status_datetime).of_type(:datetime) }
    it { is_expected.to have_db_column(:final).of_type(:boolean).with_options(default: false, null: false) }
    it { is_expected.to have_db_column(:status).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:bond_type).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:bondsman_name).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:bond_company).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:bondsman_address1).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:bondsman_address2).of_type(:string).with_options(default: '', null: false) }
    it { is_expected.to have_db_column(:bondsman_address3).of_type(:string).with_options(default: '', null: false) }
  end
  
  describe "Table Indices" do
    it { is_expected.to have_db_index(:arrest_id) }
    it { is_expected.to have_db_index(:created_by) }
    it { is_expected.to have_db_index(:person_id) }
    it { is_expected.to have_db_index(:surety_id) }
    it { is_expected.to have_db_index(:updated_by) }
  end
  
  describe "Associations" do
    it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:created_by) }
    it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key(:updated_by) }
    it { is_expected.to belong_to(:surety).class_name('Person').with_foreign_key(:surety_id).inverse_of(:bonds_as_surety) }
    it { is_expected.to belong_to(:arrest).inverse_of(:bonds) }
    it { is_expected.to belong_to(:person).inverse_of(:bonds) }
    it { is_expected.to have_many(:hold_bonds).dependent(:destroy) }
    it { is_expected.to have_many(:holds).through(:hold_bonds) }
  end
  
  describe "Attributes" do
    it { is_expected.to have_attr_accessor(:bondsman_id) }
    it { is_expected.to have_attr_accessor(:bonding_company_id) }
  end
  
  describe "Scopes" do
    let!(:p1) { create(:person, family_name: "Adams", given_name: "Jack") }
    let!(:p2) { create(:person, family_name: "Smith", given_name: "John") }
    let!(:a1) { create(:arrest, person: p1) }
    let!(:a2) { create(:arrest, person: p2) }
    let!(:b1) { create(:bond, arrest: a2, final: true, status: "Finalized", issued_datetime: Time.now - 1.hour, person: p2, surety: p1) }
    let!(:b2) { create(:bond, arrest: a1, issued_datetime: Time.now, person: p1) }
    
    it "should return active bonds when using :active scope" do
      expect(Bond.active.pluck(:id)).to eq([b2.id])
    end
    
    it "should return bonds in reverse issued_datetime order when using :by_reverse_date scope" do
      expect(Bond.by_reverse_date.pluck(:id)).to eq([b2.id,b1.id])
    end
    
    it "should return bonds in lastname, firstname order when using :by_name scope" do
      expect(Bond.by_name.pluck(:id)).to eq([b2.id,b1.id])
    end
    
    it "should return specified bond when using :with_bond_id scope" do
      expect(Bond.with_bond_id(b2.id).pluck(:id)).to eq([b2.id])
    end
    
    it "should call AppUtils.like when using :with_bondsman_name scope" do
      allow(AppUtils).to receive(:like)
      Bond.with_bondsman_name("Tucker")
      expect(AppUtils).to have_received(:like).with(Bond, :bondsman_name, "Tucker")
    end
    
    it "should call AppUtils.like when using :with_bond_company scope" do
      allow(AppUtils).to receive(:like)
      Bond.with_bond_company("Acme")
      expect(AppUtils).to have_received(:like).with(Bond, :bond_company, "Acme")
    end
    
    it "should return bond selected by surety id when using :with_surety_id scope" do
      expect(Bond.with_surety_id(p1.id).pluck(:id)).to eq([b1.id])
    end
    
    it "should return bond selected by person id when using :with_person_id scope" do
      expect(Bond.with_person_id(p1.id).pluck(:id)).to eq([b2.id])
    end
    
    it "should return bond selected by arrest if when using :with_arrest_id scope" do
      expect(Bond.with_arrest_id(a1.id).pluck(:id)).to eq([b2.id])
    end
    
    it "should call AppUtils.like when using :with_bond_no scope" do
      allow(AppUtils).to receive(:like)
      Bond.with_bond_no("123")
      expect(AppUtils).to have_received(:like).with(Bond, :bond_no, "123")
    end
    
    it "should call AppUtils.like when using :with_bond_type scope" do
      allow(AppUtils).to receive(:like)
      Bond.with_bond_type("Professional")
      expect(AppUtils).to have_received(:like).with(Bond, :bond_type, "Professional")
    end
    
    it "should return active/inactive bonds when using :with_active scope" do
      expect(Bond.with_active.pluck(:id)).to eq([b2.id])
    end
    
    it "should call AppUtils.like when using :with_status scope" do
      allow(AppUtils).to receive(:like)
      Bond.with_status("Finalized")
      expect(AppUtils).to have_received(:like).with(Bond, :status, "Finalized")
    end
    
    it "should call AppUtils.like when using :with_remarks scope" do
      allow(AppUtils).to receive(:like)
      Bond.with_remarks("Test")
      expect(AppUtils).to have_received(:like).with(Bond, :remarks, "Test")
    end
    
    it "should call AppUtils.for_person using :with_surety_name scope" do
      allow(AppUtils).to receive(:for_person)
      Bond.with_surety_name("Smith")
      expect(AppUtils).to have_received(:for_person).with(Bond, "Smith", association: :surety)
    end
    
    it "should call AppUtils.for_person using :with_person_name scope" do
      allow(AppUtils).to receive(:for_person)
      Bond.with_person_name("Smith")
      expect(AppUtils).to have_received(:for_person).with(Bond, "Smith", association: :person)
    end
    
    it "should call AppUtils.for_daterange using :with_issued_date scope" do
      allow(AppUtils).to receive(:for_daterange)
      Bond.with_issued_date("01/01/2015","01/02/2015")
      expect(AppUtils).to have_received(:for_daterange).with(Bond, :issued_datetime, start: "01/01/2015", stop: "01/02/2015")
    end
  end
  
  describe "Callbacks" do
    describe "after_validation" do
      describe ":check_creator" do
        it "should call #check_creator after validation" do
          b = build(:bond)
          allow(b).to receive(:check_creator).and_return(false)
          b.valid?
          expect(b).to have_received(:check_creator).with(no_args)
        end
      end
    
      describe ":check_bondsman" do
        it "should call #check_bondsman after validation" do
          b = build(:bond)
          allow(b).to receive(:check_bondsman).and_return(false)
          b.valid?
          expect(b).to have_received(:check_bondsman).with(no_args)
        end
      end
    
      describe ":check_other_agency" do
        it "should call #check_other_agency after save" do
          b = build(:bond)
          allow(b).to receive(:check_other_agency).and_return(false)
          b.valid?
          expect(b).to have_received(:check_other_agency).with(no_args)
        end
      end
    
      describe ":check_bond_no" do
        it "should call #check_bond_no after save" do
          b = build(:bond)
          allow(b).to receive(:check_bond_no).and_return(false)
          b.valid?
          expect(b).to have_received(:check_bond_no).with(no_args)
        end
      end
    end
  
    describe "before_validation" do
      describe ":lookup_bondsman" do
        it "should call #lookup_bondsman before validation" do
          b = build(:bond)
          allow(b).to receive(:lookup_bondsman).and_return(false)
          b.valid?
          expect(b).to have_received(:lookup_bondsman).with(no_args)
        end
      end
    
      describe ":check_surety" do
        it "should call #check_surety before validation" do
          b = build(:bond)
          allow(b).to receive(:check_surety).and_return(false)
          b.valid?
          expect(b).to have_received(:check_surety).with(no_args)
        end
      end
    
      describe ":check_arrest_id" do
        it "should call #check_arrest_id before validation" do
          b = build(:bond)
          allow(b).to receive(:check_arrest_id).and_return(false)
          b.valid?
          expect(b).to have_received(:check_arrest_id).with(no_args)
        end
      end
    end
  end
  
  describe "Validations With Bondsman" do
    subject { Bond.new(bondsman_name: "Smith") }
    it { is_expected.to validate_numericality_of(:bond_amt).is_greater_than_or_equal_to(0).is_less_than(10 ** 10) }
    it { is_expected.to validate_datetime(:issued_datetime) }
    it { is_expected.to validate_datetime(:status_datetime).allow_blank }
    it { is_expected.to validate_person(:person_id) }
  end
  
  describe "Validations Without Bondsman" do
    it "should validate surety_id if bondsman not specified" do
      b = Bond.new
      allow(b).to receive(:check_surety).and_return(true)
      subject { b }
      subject { is_expected.to validate_person(:surety_id) }
    end
  end
  
  describe "Class Methods" do
    it { is_expected.to have_defined_desc }
    it { is_expected.to have_defined_base_perms }
  
    describe ".statuses" do
      it "should return array of valid bond status values" do
        expect(Bond.statuses).to be_a(Array)
        expect(Bond.statuses).to include("Revoked")
      end
    end
  
    describe ".types" do
      it "should return array of valid bond type values" do
        expect(Bond.types).to be_a(Array)
        expect(Bond.types).to include("Professional")
      end
    end
  
    describe ".satisfied_but_unfinalized" do
      it "should return bonds for unfinalized report" do
        # set up active bond with no holds
        b1 = create(:bond)
        # set up active bond with probation hold
        a2 = create(:arrest)
        h2 = create(:hold, hold_type: "Probation (Parish)", arrest: a2)
        b2 = create(:bond, person: a2.person)
        b2.holds << h2
        # set up active bond with finalized arrest
        a3 = create(:arrest)
        chg2 = create(:charge, arrest: a3)
        h3 = create(:hold, hold_type: "Bondable", arrest: a3)
        d1 = create(:docket, person: a3.person)
        dc1 = create(:da_charge, docket: d1, arrest_charge: chg2, dropped_by_da: true)
        b3 = create(:bond, arrest: a3)
        b3.holds << h3
        # set up inactive bond
        b4 = create(:bond, status: "Recalled")
        # set up active bond with unfinalized arrest
        a1 = create(:arrest)
        chg1 = create(:charge, arrest: a1)
        h1 = create(:hold, arrest: a1, hold_type: "Bondable")
        b5 = create(:bond, person: a1.person, arrest: a1)
        b5.holds << h1
        # see if we pick up the desired bonds
        data = Bond.satisfied_but_unfinalized
        expect(data).to include(district: [b3])
        expect(data).to include(no_holds: [b1])
        expect(data).to include(probation: [b2])
      end
    end
  
    describe ".process_query" do
      before(:example) do
        allow(Bond).to receive(:with_bond_id).and_return(Bond.none)
        allow(Bond).to receive(:with_bondsman_name).and_return(Bond.none)
        allow(Bond).to receive(:with_bond_company).and_return(Bond.none)
        allow(Bond).to receive(:with_surety_id).and_return(Bond.none)
        allow(Bond).to receive(:with_surety_name).and_return(Bond.none)
        allow(Bond).to receive(:with_person_id).and_return(Bond.none)
        allow(Bond).to receive(:with_person_name).and_return(Bond.none)
        allow(Bond).to receive(:with_arrest_id).and_return(Bond.none)
        allow(Bond).to receive(:with_bond_no).and_return(Bond.none)
        allow(Bond).to receive(:with_issued_date).and_return(Bond.none)
        allow(Bond).to receive(:with_bond_type).and_return(Bond.none)
        allow(Bond).to receive(:with_active).and_return(Bond.none)
        allow(Bond).to receive(:with_status).and_return(Bond.none)
        allow(Bond).to receive(:with_remarks).and_return(Bond.none)
      end
      
      let(:full_query) {
        {
          id: '1',
          bondsman_name: 'Smith',
          bond_company: 'Acme',
          surety_id: '2',
          surety_name: 'Jones',
          person_id: '3',
          person_name: 'Merten',
          arrest_id: "4",
          bond_no: '5',
          issued_date_from: '01/01/2015',
          issued_date_to: '01/02/2015',
          bond_type: 'Professional',
          active: '1',
          status: 'Recalled',
          remarks: 'Test'
        }
      }
      
      let(:empty_query) {
        {
          id: '',
          bondsman_name: '',
          bond_company: '',
          surety_id: '',
          surety_name: '',
          person_id: '',
          person_name: '',
          arrest_id: "",
          bond_no: '',
          issued_date_from: '',
          issued_date_to: '',
          bond_type: '',
          active: '',
          status: '',
          remarks: ''
        }
      }
      
      it "should return empty relation when called with anything other than a Hash" do
        expect(Bond.process_query(Date)).to be_a(ActiveRecord::Relation)
        expect(Bond.process_query(Date)).to eq(Bond.none)
      end
      
      it "should execute appropriate scopes when given query hash" do
        Bond.process_query(full_query)
        expect(Bond).to have_received(:with_bond_id).with('1')
        expect(Bond).to have_received(:with_bondsman_name).with('Smith')
        expect(Bond).to have_received(:with_bond_company).with('Acme')
        expect(Bond).to have_received(:with_surety_id).with('2')
        expect(Bond).to have_received(:with_surety_name).with('Jones')
        expect(Bond).to have_received(:with_person_id).with('3')
        expect(Bond).to have_received(:with_person_name).with('Merten')
        expect(Bond).to have_received(:with_arrest_id).with('4')
        expect(Bond).to have_received(:with_bond_no).with('5')
        expect(Bond).to have_received(:with_issued_date).with('01/01/2015','01/02/2015')
        expect(Bond).to have_received(:with_bond_type).with('Professional')
        expect(Bond).to have_received(:with_active).with(no_args)
        expect(Bond).to have_received(:with_status).with('Recalled')
        expect(Bond).to have_received(:with_remarks).with('Test')
      end
      
      it "should not call any scopes when given empty query" do
        Bond.process_query(empty_query)
        expect(Bond).not_to have_received(:with_bond_id)
        expect(Bond).not_to have_received(:with_bondsman_name)
        expect(Bond).not_to have_received(:with_bond_company)
        expect(Bond).not_to have_received(:with_surety_id)
        expect(Bond).not_to have_received(:with_surety_name)
        expect(Bond).not_to have_received(:with_person_id)
        expect(Bond).not_to have_received(:with_person_name)
        expect(Bond).not_to have_received(:with_arrest_id)
        expect(Bond).not_to have_received(:with_bond_no)
        expect(Bond).not_to have_received(:with_issued_date)
        expect(Bond).not_to have_received(:with_bond_type)
        expect(Bond).not_to have_received(:with_active)
        expect(Bond).not_to have_received(:with_status)
        expect(Bond).not_to have_received(:with_remarks)
      end
    end
  end
  
  describe "Instance Methods" do
    describe "#bondsman_address" do
      it "should return string including bondsman_address1" do
        b = create(:bond, bondsman_address1: "Main Street")
        expect(b.bondsman_address).to include("Main Street")
      end
    end
    
    describe "#booking" do
      it "should return nil if there is no linked booking" do
        b = create(:bond)
        expect(b.booking).to be_nil
      end
      
      it "should return hold booking of last attached hold has valid booking" do
        bk = create(:booking)
        a = create(:arrest, person: bk.person)
        h = create(:hold, arrest: a, booking: bk)
        b = create(:bond, person: bk.person)
        b.holds << h
        expect(b.booking).to eq(bk)
      end
      
      it "should return arrest booking if hold booking is not available" do
        bk = create(:booking)
        a = create(:arrest, person: bk.person)
        h = create(:hold, arrest: a, booking: bk)
        b = create(:bond, person: bk.person, arrest: a)
        expect(b.booking).to eq(bk)
      end
    end
  
    describe "#check_recallable" do
      it "should return error message unless given correct new status" do
        b = create(:bond)
        expect(b.check_recallable("bad status")).to include("Invalid Recall Reason")
      end
      
      it "should return error message if surrender without open booking" do
        b = create(:bond)
        expect(b.check_recallable("surrender")).to include("No Open Booking")
      end
      
      it "should return error message if bond is final" do
        bk = create(:booking)
        b = create(:bond, person: bk.person, final: true)
        expect(b.check_recallable("surrender")).to include("Bond is Final")
      end
      
      it "should return error message if bond is inactive" do
        bk = create(:booking)
        b = create(:bond, person: bk.person, final: false, status: "Recalled")
        expect(b.check_recallable("surrender")).to include("Bond Is Recalled")
      end
      
      it "should return error message if bond does not have valid hold" do
        bk = create(:booking)
        b = create(:bond, person: bk.person)
        expect(b.check_recallable("surrender")).to include("Invalid")
      end
      
      it "should return error message if bond has bondable hold w/o arrest" do
        bk = create(:booking)
        h = create(:hold, booking: bk, hold_type: "Bondable")
        b = create(:bond, person: bk.person)
        b.holds << h
        expect(b.check_recallable("surrender")).to include("No Valid Arrest")
      end
      
      it "should return nil if there are no problems with recall" do
        bk = create(:booking)
        a = create(:arrest, person: bk.person)
        h = create(:hold, booking: bk, arrest: a, hold_type: "Bondable")
        b = create(:bond, person: bk.person)
        b.holds << h
        expect(b.check_recallable("surrender")).to be_nil
      end
    end
  
    describe "#check_undo_recallable" do
      it "should return error message if invalid status" do
        b = create(:bond, status: "bad status")
        expect(b.check_undo_recallable).to include("Invalid")
      end
      
      it "should return error message if bond not linked" do
        b = create(:bond, status: "Surrendered")
        expect(b.check_undo_recallable).to include("not linked")
      end
      
      it "should return error message if bond is final and not Revoked or Forfeited" do
        bk = create(:booking)
        a = create(:arrest, person: bk.person)
        b = create(:bond, person: bk.person, arrest: a, final: true, status: "Revoked")
        expect(b.check_undo_recallable).to include("Not Implemented")
      end
      
      it "should return nil if bond is not final and Revoked or Forfeited" do
        bk = create(:booking)
        a = create(:arrest, person: bk.person)
        b = create(:bond, person: bk.person, arrest: a, status: "Revoked")
        expect(b.check_undo_recallable).to be_nil
      end
    end
  
    describe "#recall" do
      it "should call #check_recallable if no booking is specified" do
        b = create(:bond)
        allow(b).to receive(:check_recallable).and_return("Test")
        expect(b.recall("Surrender")).to eq("Test")
        expect(b).to have_received(:check_recallable).with("Surrender")
      end
      
      it "should return error if user id is blank" do
        b = create(:bond)
        allow(b).to receive(:check_recallable).and_return(nil)
        expect(b.recall("Surrender", '')).to include("ID missing")
      end
      
      it "should return error if user id is invalid" do
        b = create(:bond)
        allow(b).to receive(:check_recallable).and_return(nil)
        expect(b.recall("surrender", 100)).to include("ID not found")
      end
      
      it "should return error if invalid booking_id is specified" do
        bk = create(:booking)
        b = create(:bond, person: bk.person)
        expect(b.recall("surrender", booking_id: bk.id + 1)).to include("Invalid Booking")
      end
      
      it "should be completed" do
        expect(false).to be(true)
      end
    end
  
    describe "#status_name" do
      it "should return status and final state as a test string" do
        b = create(:bond, status: "Revoked", final: true)
        expect(b.status_name).to eq("Revoked (Final)")
      end
    end
  
    describe "#surety_name" do
      it "should return bondsman name and company for professional bonds" do
        b = build(:bond, bondsman_name: "Smith", bond_company: "Acme", surety_id: nil)
        expect(b.surety_name).to eq("Bondsman: Smith (Acme)")
      end
      
      it "should return surety name for non-professional bonds" do
        p = create(:person, family_name: "Smith", given_name: "John")
        b = build(:bond, surety: p, bondsman_name: '', bond_company: '', bond_type: 'Property')
        expect(b.surety_name).to eq("Smith, John (Property)")
      end
      
      it "should return unknown for no bondsman or surety" do
        b = Bond.new
        expect(b.surety_name).to eq("Unknown")
      end
    end
  
    describe "#undo_recall" do
      # this method is not fully functional. These tests are written to exercise
      # the portions of the method that have been implemented so far.
      it "should call check_undo_recallable before processing bond" do
        u = create(:user)
        b = create(:bond)
        allow(b).to receive(:check_undo_recallable).and_return("Test")
        expect(b.undo_recall(u.id)).to eq("Test")
        expect(b).to have_received(:check_undo_recallable).with(no_args)
      end
      
      it "should return error message if user_id blank" do
        b = create(:bond)
        allow(b).to receive(:check_undo_recallable).and_return(nil)
        expect(b.undo_recall("")).to include("ID missing")
      end
      
      it "should return error message if user_id wrong type" do
        b = create(:bond)
        allow(b).to receive(:check_undo_recallable).and_return(nil)
        expect(b.undo_recall(Date.today)).to include("ID missing")
      end
      
      it "should return error message if user_id not found" do
        b = create(:bond)
        allow(b).to receive(:check_undo_recallable).and_return(nil)
        expect(b.undo_recall(100)).to include("ID not found")
      end
      
      it "should return error message if bond is final" do
        u = create(:user)
        b = create(:bond, final: true)
        allow(b).to receive(:check_undo_recallable).and_return(nil)
        expect(b.undo_recall(u.id)).to include("Not Implemented")
      end
      
      it "should make bond active again" do
        u = create(:user)
        b = create(:bond, final: false, status: "Revoked")
        allow(b).to receive(:check_undo_recallable).and_return(nil)
        expect(b.undo_recall(u.id)).to be_nil
        expect(b.status).to eq("")
        expect(b.updated_by).to eq(u.id)
      end
    end
    
    describe "#check_arrest_id" do
      it "should link to arrest if holds have valid arrest" do
        a = create(:arrest)
        h = create(:hold, arrest: a)
        b = build(:bond, person: a.person)
        b.holds << h
        b.check_arrest_id
        expect(b.arrest(true).id).to eq(a.id)
      end
    end
    
    describe "#check_bond_no" do
      it "should create bond number from bond ID if none entered" do
        b = build(:bond, bond_no: '')
        b.check_bond_no
        expect(b.bond_no).to eq(b.id.to_s.rjust(6,'0'))
      end
    end
    
    describe "#check_bondsman" do
      it "should force bond type to Professional if bondsman name given" do
        b = build(:bond, bondsman_name: "Smith", bond_type: "Property")
        b.check_bondsman
        expect(b.bond_type).to eq("Professional")
      end
    end
    
    describe "#lookup_bondsman" do
      it "should update bondsman_name and address if given bondsman_id" do
        c = create(:contact, sort_name: "Ho, Don", bondsman: true, active: true)
        b = build(:bond, bondsman_name: "Smith", bondsman_id: c.id)
        b.lookup_bondsman
        expect(b.bondsman_name).to eq("Don Ho")
      end
      
      it "should update bond_company if given bond_company_id" do
        c = create(:contact, sort_name: "Ho, Don", bondsman: true, active: true)
        c2 = create(:contact, sort_name: "Acme", business: true, bonding_company: true)
        c.bond_companies << c2
        b = build(:bond, bondsman_name: "Smith", bondsman_id: c.id, bond_company: "AAA", bonding_company_id: c2.id)
        b.lookup_bondsman
        expect(b.bond_company).to eq("Acme")
      end
    end
    
    describe "#check_other_agency" do
      it "should mark bond final if status is 'For Other Agency'" do
        b = build(:bond, status: 'For Other Agency', final: false)
        b.check_other_agency
        expect(b.final).to be(true)
      end
    end
    
    describe "#check_surety" do
      it "should return bondsman_id error if set to invalid contact id" do
        b = build(:bond, bondsman_id: 100)
        expect(b.check_surety).to be(false)
        expect(b.errors).to have_key(:bondsman_id)
      end
      
      it "should return bondsman_name error if surety_id and bondsman_name given" do
        b = build(:bond, bondsman_name: "Smith", surety: create(:person))
        expect(b.check_surety).to be(false)
        expect(b.errors).to have_key(:bondsman_name)
      end
      
      it "should return bondsman_name error if neither surety_id nor bondsman_name given" do
        b = build(:bond, bondsman_name: '', surety_id: nil)
        expect(b.check_surety).to be(false)
        expect(b.errors).to have_key(:bondsman_name)
      end
      
      it "should return true with valid bondsman_id given" do
        c = create(:contact, bondsman: true, active: true)
        b = build(:bond, bondsman_id: c.id)
        expect(b.check_surety).to be(true)
      end
      
      it "should return true if valid surety_id given (without bondsman_name/id)" do
        p = create(:person)
        b = build(:bond, surety_id: p.id)
        expect(b.check_surety).to be(true)
      end
      
      it "should return true if bondsman_name given (without surety_id/bondsman_id)" do
        b = build(:bond, bondsman_name: "Smith", bondsman_id: nil, surety_id: nil)
        expect(b.check_surety).to be(true)
      end
    end
  end
end
