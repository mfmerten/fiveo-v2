# == Schema Information
#
# Table name: contact_bondsman_companies
#
#  id          :integer          not null, primary key
#  contact_id  :integer          not null
#  bondsman_id :integer
#  company_id  :integer
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_contact_bondsman_companies_on_bondsman_id  (bondsman_id)
#  index_contact_bondsman_companies_on_company_id   (company_id)
#  index_contact_bondsman_companies_on_contact_id   (contact_id)
#
# Foreign Keys
#
#  contact_bondsman_companies_bondsman_id_fk  (bondsman_id => contacts.id)
#  contact_bondsman_companies_company_id_fk   (company_id => contacts.id)
#  contact_bondsman_companies_contact_id_fk   (contact_id => contacts.id)
#

require 'rails_helper'

RSpec.describe ContactBondsmanCompany do
  
  ## table columns
  #it { is_expected.to have_db_column(:contact_id).of_type(:integer).with_options(null: false) }
  #it { is_expected.to have_db_column(:bondsman_id).of_type(:integer) }
  #it { is_expected.to have_db_column(:company_id).of_type(:integer) }
  #it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  #
  ## table indexes
  #it { is_expected.to have_db_index(:bondsman_id) }
  #it { is_expected.to have_db_index(:company_id) }
  #it { is_expected.to have_db_index(:contact_id) }
  #
  ## associations
  #it { is_expected.to belong_to(:contact).inverse_of(:contact_bondsman_companies) }
  #it { is_expected.to belong_to(:bondsman).class_name('Contact').with_foreign_key(:bondsman_id) }
  #it { is_expected.to belong_to(:company).class_name('Contact').with_foreign_key(:company_id) }
  #
  ## methods
  #it { is_expected.to have_defined_desc }
end
