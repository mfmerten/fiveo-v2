# == Schema Information
#
# Table name: probation_payments
#
#  id               :integer          not null, primary key
#  probation_id     :integer          not null
#  transaction_date :date
#  officer          :string(255)      default(""), not null
#  officer_unit     :string(255)      default(""), not null
#  officer_badge    :string(255)      default(""), not null
#  created_by       :integer          default(1)
#  updated_by       :integer          default(1)
#  created_at       :datetime
#  updated_at       :datetime
#  receipt_no       :string(255)      default(""), not null
#  report_datetime  :datetime
#  memo             :string(255)      default(""), not null
#  fund1_name       :string(255)      default(""), not null
#  fund1_charge     :decimal(12, 2)   default(0.0), not null
#  fund1_payment    :decimal(12, 2)   default(0.0), not null
#  fund2_name       :string(255)      default(""), not null
#  fund2_charge     :decimal(12, 2)   default(0.0), not null
#  fund2_payment    :decimal(12, 2)   default(0.0), not null
#  fund3_name       :string(255)      default(""), not null
#  fund3_charge     :decimal(12, 2)   default(0.0), not null
#  fund3_payment    :decimal(12, 2)   default(0.0), not null
#  fund4_name       :string(255)      default(""), not null
#  fund4_charge     :decimal(12, 2)   default(0.0), not null
#  fund4_payment    :decimal(12, 2)   default(0.0), not null
#  fund5_name       :string(255)      default(""), not null
#  fund5_charge     :decimal(12, 2)   default(0.0), not null
#  fund5_payment    :decimal(12, 2)   default(0.0), not null
#  fund6_name       :string(255)      default(""), not null
#  fund6_charge     :decimal(12, 2)   default(0.0), not null
#  fund6_payment    :decimal(12, 2)   default(0.0), not null
#  fund7_name       :string(255)      default(""), not null
#  fund7_charge     :decimal(12, 2)   default(0.0), not null
#  fund7_payment    :decimal(12, 2)   default(0.0), not null
#  fund8_name       :string(255)      default(""), not null
#  fund8_charge     :decimal(12, 2)   default(0.0), not null
#  fund8_payment    :decimal(12, 2)   default(0.0), not null
#  fund9_name       :string(255)      default(""), not null
#  fund9_charge     :decimal(12, 2)   default(0.0), not null
#  fund9_payment    :decimal(12, 2)   default(0.0), not null
#
# Indexes
#
#  index_probation_payments_on_created_by       (created_by)
#  index_probation_payments_on_probation_id     (probation_id)
#  index_probation_payments_on_report_datetime  (report_datetime)
#  index_probation_payments_on_updated_by       (updated_by)
#
# Foreign Keys
#
#  probation_payments_created_by_fk    (created_by => users.id)
#  probation_payments_probation_id_fk  (probation_id => probations.id)
#  probation_payments_updated_by_fk    (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe ProbationPayment do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
