# == Schema Information
#
# Table name: warrants
#
#  id                 :integer          not null, primary key
#  person_id          :integer
#  warrant_no         :string(255)      default(""), not null
#  received_date      :date
#  issued_date        :date
#  dispo_date         :date
#  remarks            :text             default(""), not null
#  created_at         :datetime
#  updated_at         :datetime
#  bond_amt           :decimal(12, 2)   default(0.0), not null
#  payable            :decimal(12, 2)   default(0.0), not null
#  created_by         :integer          default(1)
#  updated_by         :integer          default(1)
#  dob                :date
#  street             :string(255)      default(""), not null
#  oln                :string(255)      default(""), not null
#  given_name         :string(255)      default(""), not null
#  family_name        :string(255)      default(""), not null
#  suffix             :string(255)      default(""), not null
#  sex                :integer          default(0), not null
#  special_type       :integer          default(0), not null
#  city               :string(255)      default(""), not null
#  zip                :string(255)      default(""), not null
#  legacy             :boolean          default(FALSE), not null
#  state              :string(255)      default(""), not null
#  oln_state          :string(255)      default(""), not null
#  jurisdiction       :string(255)      default(""), not null
#  phone1             :string(255)      default(""), not null
#  phone2             :string(255)      default(""), not null
#  phone3             :string(255)      default(""), not null
#  phone1_unlisted    :boolean          default(FALSE), not null
#  phone2_unlisted    :boolean          default(FALSE), not null
#  phone3_unlisted    :boolean          default(FALSE), not null
#  race               :string(255)      default(""), not null
#  phone1_type        :string(255)      default(""), not null
#  phone2_type        :string(255)      default(""), not null
#  phone3_type        :string(255)      default(""), not null
#  disposition        :string(255)      default(""), not null
#  charge_type        :string(255)      default(""), not null
#  arrest_id          :integer
#  photo_file_name    :string(255)
#  photo_content_type :string(255)
#  photo_file_size    :integer
#  photo_updated_at   :datetime
#  encrypted_ssn      :string           default("")
#
# Indexes
#
#  index_warrants_on_arrest_id                   (arrest_id)
#  index_warrants_on_created_by                  (created_by)
#  index_warrants_on_family_name_and_given_name  (family_name,given_name)
#  index_warrants_on_person_id                   (person_id)
#  index_warrants_on_updated_by                  (updated_by)
#  index_warrants_on_warrant_no                  (warrant_no)
#
# Foreign Keys
#
#  warrants_arrest_id_fk   (arrest_id => arrests.id)
#  warrants_created_by_fk  (created_by => users.id)
#  warrants_person_id_fk   (person_id => people.id)
#  warrants_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Warrant do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
