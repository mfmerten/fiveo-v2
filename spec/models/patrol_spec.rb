# == Schema Information
#
# Table name: patrols
#
#  id                      :integer          not null, primary key
#  officer                 :string(255)      default(""), not null
#  officer_unit            :string(255)      default(""), not null
#  officer_badge           :string(255)      default(""), not null
#  beginning_odometer      :integer          default(0), not null
#  ending_odometer         :integer          default(0), not null
#  shift                   :integer          default(0), not null
#  supervisor              :string(255)      default(""), not null
#  shift_start             :string(255)      default(""), not null
#  shift_stop              :string(255)      default(""), not null
#  roads_patrolled         :integer          default(0), not null
#  complaints              :integer          default(0), not null
#  traffic_stops           :integer          default(0), not null
#  citations_issued        :integer          default(0), not null
#  papers_served           :integer          default(0), not null
#  narcotics_arrests       :integer          default(0), not null
#  other_arrests           :integer          default(0), not null
#  public_assists          :integer          default(0), not null
#  warrants_served         :integer          default(0), not null
#  funeral_escorts         :integer          default(0), not null
#  remarks                 :text             default(""), not null
#  created_by              :integer          default(1)
#  updated_by              :integer          default(1)
#  created_at              :datetime
#  updated_at              :datetime
#  patrol_started_datetime :datetime
#  patrol_ended_datetime   :datetime
#
# Indexes
#
#  index_patrols_on_created_by  (created_by)
#  index_patrols_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  patrols_created_by_fk  (created_by => users.id)
#  patrols_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Patrol do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
