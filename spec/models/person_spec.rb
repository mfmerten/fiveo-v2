# == Schema Information
#
# Table name: people
#
#  id                         :integer          not null, primary key
#  family_name                :string(255)      default(""), not null
#  given_name                 :string(255)      default(""), not null
#  suffix                     :string(255)      default(""), not null
#  aka                        :string(255)      default(""), not null
#  is_alias_for               :integer
#  phys_street                :string(255)      default(""), not null
#  phys_city                  :string(255)      default(""), not null
#  phys_zip                   :string(255)      default(""), not null
#  mail_street                :string(255)      default(""), not null
#  mail_city                  :string(255)      default(""), not null
#  mail_zip                   :string(255)      default(""), not null
#  date_of_birth              :date
#  place_of_birth             :string(255)      default(""), not null
#  sex                        :integer          default(0), not null
#  fbi                        :string(255)      default(""), not null
#  sid                        :string(255)      default(""), not null
#  oln                        :string(255)      default(""), not null
#  home_phone                 :string(255)      default(""), not null
#  cell_phone                 :string(255)      default(""), not null
#  email                      :string(255)      default(""), not null
#  height_ft                  :integer          default(0), not null
#  height_in                  :integer          default(0), not null
#  weight                     :integer          default(0), not null
#  glasses                    :boolean          default(FALSE), not null
#  shoe_size                  :string(255)      default(""), not null
#  occupation                 :string(255)      default(""), not null
#  employer                   :string(255)      default(""), not null
#  emergency_contact          :string(255)      default(""), not null
#  emergency_address          :string(255)      default(""), not null
#  emergency_phone            :string(255)      default(""), not null
#  med_allergies              :string(255)      default(""), not null
#  hepatitis                  :boolean          default(FALSE), not null
#  hiv                        :boolean          default(FALSE), not null
#  tb                         :boolean          default(FALSE), not null
#  deceased                   :boolean          default(FALSE), not null
#  dna_swab_date              :date
#  gang_affiliation           :string(255)      default(""), not null
#  remarks                    :text             default(""), not null
#  created_at                 :datetime
#  updated_at                 :datetime
#  doc_number                 :string(255)      default(""), not null
#  created_by                 :integer          default(1)
#  updated_by                 :integer          default(1)
#  front_mugshot_file_name    :string(255)
#  side_mugshot_file_name     :string(255)
#  front_mugshot_content_type :string(255)
#  side_mugshot_content_type  :string(255)
#  front_mugshot_file_size    :integer
#  side_mugshot_file_size     :integer
#  front_mugshot_updated_at   :datetime
#  side_mugshot_updated_at    :datetime
#  legacy                     :boolean          default(FALSE), not null
#  dna_blood_date             :date
#  dna_profile                :string(255)      default(""), not null
#  fingerprint_class          :string(255)      default(""), not null
#  phys_state                 :string(255)      default(""), not null
#  mail_state                 :string(255)      default(""), not null
#  oln_state                  :string(255)      default(""), not null
#  race                       :string(255)      default(""), not null
#  build_type                 :string(255)      default(""), not null
#  eye_color                  :string(255)      default(""), not null
#  hair_color                 :string(255)      default(""), not null
#  skin_tone                  :string(255)      default(""), not null
#  complexion                 :string(255)      default(""), not null
#  hair_type                  :string(255)      default(""), not null
#  mustache                   :string(255)      default(""), not null
#  beard                      :string(255)      default(""), not null
#  eyebrow                    :string(255)      default(""), not null
#  em_relationship            :string(255)      default(""), not null
#  sex_offender               :boolean          default(FALSE), not null
#  encrypted_ssn              :string           default("")
#
# Indexes
#
#  index_people_on_created_by                  (created_by)
#  index_people_on_family_name_and_given_name  (family_name,given_name)
#  index_people_on_is_alias_for                (is_alias_for)
#  index_people_on_updated_by                  (updated_by)
#
# Foreign Keys
#
#  people_created_by_fk    (created_by => users.id)
#  people_is_alias_for_fk  (is_alias_for => people.id)
#  people_updated_by_fk    (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Person do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
