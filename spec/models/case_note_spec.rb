# == Schema Information
#
# Table name: case_notes
#
#  id         :integer          not null, primary key
#  case_no    :string(255)      default(""), not null
#  note       :text             default(""), not null
#  created_at :datetime
#  updated_at :datetime
#  created_by :integer          default(1)
#  updated_by :integer          default(1)
#
# Indexes
#
#  index_case_notes_on_case_no     (case_no)
#  index_case_notes_on_created_by  (created_by)
#  index_case_notes_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  case_notes_created_by_fk  (created_by => users.id)
#  case_notes_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe CaseNote do
  
  ## table columns
  #it { is_expected.to have_db_column(:case_no).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:note).of_type(:text).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
  #
  ## table indexes
  #it { is_expected.to have_db_index(:case_no) }
  #it { is_expected.to have_db_index(:created_by) }
  #it { is_expected.to have_db_index(:updated_by) }
  #
  ## associations
  #it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:created_by) }
  #it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key(:updated_by) }
  #
  ## scopes
  #describe "scopes" do
  #  before(:context) do
  #    # create test records
  #  end
  #  it "should return in reverse updated_at order when using :by_reverse_date scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return case note 1 and not case note 2 using :with_case_no with note 1 case number" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_note scope" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## callbacks
  #
  #describe "before_save" do
  #  describe ":check_creator" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  #describe "before_validation" do
  #  describe ":adjust_case_no" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  ## validations
  #it { is_expected.to validate_presence_of(:note) }
  #it { is_expected.to validate_presence_of(:case_no) }
  #
  ## methods
  #it { is_expected.to have_defined_desc }
  #it { is_expected.to have_defined_base_perms }
  #
  #describe ".process_query" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
end
