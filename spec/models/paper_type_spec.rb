# == Schema Information
#
# Table name: paper_types
#
#  id         :integer          not null, primary key
#  name       :string(255)      default(""), not null
#  cost       :decimal(12, 2)   default(0.0), not null
#  created_by :integer          default(1)
#  updated_by :integer          default(1)
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_paper_types_on_created_by  (created_by)
#  index_paper_types_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  paper_types_created_by_fk  (created_by => users.id)
#  paper_types_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe PaperType do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
