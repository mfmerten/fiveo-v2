# == Schema Information
#
# Table name: events
#
#  id               :integer          not null, primary key
#  start_date       :date
#  end_date         :date
#  name             :string(255)      default(""), not null
#  details          :text             default(""), not null
#  created_at       :datetime
#  updated_at       :datetime
#  created_by       :integer          default(1)
#  updated_by       :integer          default(1)
#  private          :boolean          default(FALSE), not null
#  investigation_id :integer
#  owned_by         :integer          default(1)
#
# Indexes
#
#  index_events_on_created_by        (created_by)
#  index_events_on_investigation_id  (investigation_id)
#  index_events_on_owned_by          (owned_by)
#  index_events_on_updated_by        (updated_by)
#
# Foreign Keys
#
#  events_created_by_fk        (created_by => users.id)
#  events_investigation_id_fk  (investigation_id => investigations.id)
#  events_owned_by_fk          (owned_by => users.id)
#  events_updated_by_fk        (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Event do
  
  ## table columns
  #it { is_expected.to have_db_column(:start_date).of_type(:date) }
  #it { is_expected.to have_db_column(:end_date).of_type(:date) }
  #it { is_expected.to have_db_column(:name).of_type(:string).with_options(default: '', null: false) }
  #it { is_expected.to have_db_column(:details).of_type(:text).with_options(default: '', null: false) }
  #it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:private).of_type(:boolean).with_options(default: false, null: false) }
  #it { is_expected.to have_db_column(:investigation_id).of_type(:integer) }
  #it { is_expected.to have_db_column(:owned_by).of_type(:integer).with_options(default: 1) }
  #
  ## table indexes
  #it { is_expected.to have_db_index(:created_by) }
  #it { is_expected.to have_db_index(:updated_by) }
  #it { is_expected.to have_db_index(:owned_by) }
  #it { is_expected.to have_db_index(:investigation_id) }
  #
  ## associations
  #it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:created_by) }
  #it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key(:updated_by) }
  #it { is_expected.to belong_to(:owner).class_name('User').with_foreign_key(:owned_by) }
  #it { is_expected.to belong_to(:investigation).inverse_of(:events) }
  #
  ## scopes
  #describe "scopes" do
  #  before(:context) do
  #    # create test records
  #  end
  #  
  #  it "should return events in start date order using :by_date scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return active events using :active scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  
  #  it "should return events between two dates using :valid_within scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_text scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return events for a particular user using :with_created_by scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return a particular event using :with_event_id scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_daterange when using :with_start_date scope" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## callbacks
  #
  #describe "before_save" do
  #  describe ":check_creator" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  #describe "before_validation" do
  #  describe ":fix_end_date" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  ## validations
  #it { is_expected.to validate_presence_of(:name) }
  #it { is_expected.to validate_date(:start_date) }
  #it { is_expected.to validate_date(:end_date) }
  #
  ## methods
  #it { is_expected.to have_defined_desc }
  #it { is_expected.to have_defined_base_perms }
  #
  #describe ".visible_to" do
  #  it "should return events a particular user is allowed to see using :visible_to scope" do
  #     expect(false).to be(true)
  #   end
  #end
  #
  #describe ".get_current_events" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".process_query" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#expired?" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
end
