# == Schema Information
#
# Table name: contacts
#
#  id              :integer          not null, primary key
#  title           :string(255)      default(""), not null
#  unit            :string(255)      default(""), not null
#  street          :string(255)      default(""), not null
#  city            :string(255)      default(""), not null
#  zip             :string(255)      default(""), not null
#  pri_email       :string(255)      default(""), not null
#  alt_email       :string(255)      default(""), not null
#  web_site        :string(255)      default(""), not null
#  notes           :string(255)      default(""), not null
#  created_at      :datetime
#  updated_at      :datetime
#  officer         :boolean          default(FALSE), not null
#  street2         :string(255)      default(""), not null
#  created_by      :integer          default(1)
#  updated_by      :integer          default(1)
#  badge_no        :string(255)      default(""), not null
#  active          :boolean          default(FALSE), not null
#  detective       :boolean          default(FALSE), not null
#  physician       :boolean          default(FALSE), not null
#  pharmacy        :boolean          default(FALSE), not null
#  legacy          :boolean          default(FALSE), not null
#  business        :boolean          default(FALSE), not null
#  sort_name       :string(255)      default(""), not null
#  agency_id       :integer
#  printable       :boolean          default(TRUE), not null
#  state           :string(255)      default(""), not null
#  phone1          :string(255)      default(""), not null
#  phone2          :string(255)      default(""), not null
#  phone3          :string(255)      default(""), not null
#  phone1_unlisted :boolean          default(FALSE), not null
#  phone2_unlisted :boolean          default(FALSE), not null
#  phone3_unlisted :boolean          default(FALSE), not null
#  bondsman        :boolean          default(FALSE), not null
#  remarks         :text             default(""), not null
#  phone1_type     :string(255)      default(""), not null
#  phone2_type     :string(255)      default(""), not null
#  phone3_type     :string(255)      default(""), not null
#  judge           :boolean          default(FALSE), not null
#  pawn_co         :boolean          default(FALSE), not null
#  bonding_company :boolean          default(FALSE), not null
#
# Indexes
#
#  index_contacts_on_agency_id   (agency_id)
#  index_contacts_on_created_by  (created_by)
#  index_contacts_on_sort_name   (sort_name)
#  index_contacts_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  contacts_agency_id_fk   (agency_id => contacts.id)
#  contacts_created_by_fk  (created_by => users.id)
#  contacts_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Contact do
  
  ## table columns
  #it { is_expected.to have_db_column(:title).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:unit).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:street).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:city).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:zip).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:pri_email).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:alt_email).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:web_site).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:notes).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:street2).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:badge_no).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:sort_name).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:state).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:phone1).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:phone2).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:phone3).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:phone1_type).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:phone2_type).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:phone3_type).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:officer).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:active).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:detective).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:physician).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:pharmacy).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:legacy).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:business).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:printable).of_type(:boolean).with_options(null: false, default: true) }
  #it { is_expected.to have_db_column(:phone1_unlisted).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:phone2_unlisted).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:phone3_unlisted).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:bondsman).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:judge).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:pawn_co).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:bonding_company).of_type(:boolean).with_options(null: false, default: false) }
  #it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:agency_id).of_type(:integer) }
  #it { is_expected.to have_db_column(:remarks).of_type(:text).with_options(default: '', null: false) }
  #
  ## table indexes
  #it { is_expected.to have_db_index(:agency_id) }
  #it { is_expected.to have_db_index(:created_by) }
  #it { is_expected.to have_db_index(:updated_by) }
  #it { is_expected.to have_db_index(:sort_name) }
  #
  ## associations
  #it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:created_by) }
  #it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key(:updated_by) }
  #it { is_expected.to belong_to(:agency).class_name('Contact').with_foreign_key(:agency_id).inverse_of(:employees) }
  #it { is_expected.to have_one(:user).inverse_of(:contact).dependent(:destroy) }
  #it { is_expected.to have_many(:employees).class_name('Contact').with_foreign_key(:agency_id).dependent(:nullify) }
  #it { is_expected.to have_many(:meds_as_pharmacy).class_name('Medication').with_foreign_key(:pharmacy_id).dependent(:nullify) }
  #it { is_expected.to have_many(:meds_as_physician).class_name('Medication').with_foreign_key(:physician_id).dependent(:nullify) }
  #it { is_expected.to have_many(:invest_contacts).dependent(:destroy) }
  #it { is_expected.to have_many(:investigations).through(:invest_contacts) }
  #it { is_expected.to have_many(:contact_bondsman_companies).dependent(:destroy) }
  #it { is_expected.to have_many(:bondsmen).through(:contact_bondsman_companies).source(:bondsman).dependent(:destroy) }
  #it { is_expected.to have_many(:bond_companies).through(:contact_bondsman_companies).source(:company).dependent(:destroy) }
  #
  ## attributes
  #it { is_expected.to accept_nested_attributes_for(:contact_bondsman_companies).allow_destroy(true) }
  #
  ## scopes
  #describe "scopes" do
  #  before(:context) do
  #    # create test records
  #  end
  #  
  #  it "should return contacts in sort_name order using :by_name scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return contacts in sort_name, unit order using :by_name_and_unit scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return contacts where active is true using :is_active scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return printable contacts using :is_printable scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return contacts with a particular agency using :for_agency scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return contacts that are pawn companies using :is_pawn_company scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return contacts that are bond companies using :is_bond_company scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return contacts that are judges using :is_judge scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return contacts that are officers using :is_officer scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return contacts that are agencies using :is_agency scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return contacts that are physicians using :is_physician scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return contacts that are pharmacies using :is_pharmacy scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return contacts that are detectives using :is_detective scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return contacts that are bondsmen using :is_bondsman scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return contacts that are individuals using :is_individual scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return contacts that are businesses using :is_business scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return individual contacts by ID using :with_contact_id scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_address scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_title scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_lower when using :with_unit scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_lower when using :with_badge scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return contacts belonging to agency using :with_agency_id scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_bool when using :with_business scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_bool when using :with_active scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_bool when using :with_officer scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_bool when using :with_detective scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_bool when using :with_bondsman scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_bool when using :with_bonding_company scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_bool when using :with_judge scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_bool when using :with_pawn_co scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_bool when using :with_physician scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_bool when using :with_pharmacy scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_notes scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_email scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_web_site scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_person when using :with_contact_name scope" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## callbacks
  #
  #describe "after_save" do
  #  describe ":check_creator" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  #describe "before_validation" do
  #  describe ":fix_name" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #  
  #  describe ":fix_unit_and_badge" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  #describe "before_destroy" do
  #  describe ":check_destroyable" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  ## validations
  #
  #it { is_expected.to validate_presence_of(:sort_name) }
  #
  #describe ":phone1" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ":phone2" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ":phone3" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ":zip" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ":web_site" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ":agency_id" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## constants
  #
  #describe "PhoneType" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## methods
  #it { is_expected.to have_defined_desc }
  #it { is_expected.to have_defined_base_perms }
  #
  #describe ".agencies_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".bond_companies_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".bondsmen_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".company_names_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".companies_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".contacts_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".contacts_and_companies_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".detective_users_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".detectives_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".judges_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".local_agencies_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".officers_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".pawn_companies_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".physicians_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".pharmacies_for_select" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe ".process_query" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#address" do
  #  it "needs to be completed()" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#address_line1" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#address_line2" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#address_line3" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#bond_companies_string" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#reasons_not_destroyable" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#display_name" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#display_name_with_phone" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#phones" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
end
