# == Schema Information
#
# Table name: revocations
#
#  id               :integer          not null, primary key
#  revocation_date  :date
#  arrest_id        :integer
#  revoked          :boolean          default(FALSE), not null
#  time_served      :boolean          default(FALSE), not null
#  total_days       :integer          default(0), not null
#  total_months     :integer          default(0), not null
#  total_years      :integer          default(0), not null
#  doc              :boolean          default(FALSE), not null
#  parish_days      :integer          default(0), not null
#  parish_months    :integer          default(0), not null
#  parish_years     :integer          default(0), not null
#  processed        :boolean          default(FALSE), not null
#  remarks          :text             default(""), not null
#  created_by       :integer          default(1)
#  updated_by       :integer          default(1)
#  created_at       :datetime
#  updated_at       :datetime
#  process_status   :string(255)      default(""), not null
#  total_hours      :integer          default(0), not null
#  parish_hours     :integer          default(0), not null
#  delay_execution  :boolean          default(FALSE), not null
#  deny_goodtime    :boolean          default(FALSE), not null
#  total_life       :integer          default(0), not null
#  total_death      :boolean          default(FALSE), not null
#  consecutive_type :string(255)      default(""), not null
#  judge            :string(255)      default(""), not null
#  docket_id        :integer          not null
#
# Indexes
#
#  index_revocations_on_arrest_id   (arrest_id)
#  index_revocations_on_created_by  (created_by)
#  index_revocations_on_docket_id   (docket_id)
#  index_revocations_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  fk_rails_e1d0a12111        (docket_id => dockets.id)
#  revocations_arrest_id_fk   (arrest_id => arrests.id)
#  revocations_created_by_fk  (created_by => users.id)
#  revocations_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Revocation do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
