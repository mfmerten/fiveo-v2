# == Schema Information
#
# Table name: evidences
#
#  id                          :integer          not null, primary key
#  evidence_number             :string(255)      default(""), not null
#  case_no                     :string(255)      default(""), not null
#  description                 :text             default(""), not null
#  owner_id                    :integer
#  remarks                     :text             default(""), not null
#  created_at                  :datetime
#  updated_at                  :datetime
#  created_by                  :integer          default(1)
#  updated_by                  :integer          default(1)
#  evidence_photo_file_name    :string(255)
#  evidence_photo_content_type :string(255)
#  evidence_photo_file_size    :integer
#  evidence_photo_updated_at   :datetime
#  legacy                      :boolean          default(FALSE), not null
#  dna_profile                 :string(255)      default(""), not null
#  fingerprint_class           :string(255)      default(""), not null
#  evidence_locations_count    :integer          default(0), not null
#  disposed                    :date
#  disposition                 :string(255)      default("")
#
# Indexes
#
#  index_evidences_on_case_no          (case_no)
#  index_evidences_on_created_by       (created_by)
#  index_evidences_on_disposed         (disposed)
#  index_evidences_on_evidence_number  (evidence_number)
#  index_evidences_on_owner_id         (owner_id)
#  index_evidences_on_updated_by       (updated_by)
#
# Foreign Keys
#
#  evidences_created_by_fk  (created_by => users.id)
#  evidences_owner_id_fk    (owner_id => people.id)
#  evidences_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Evidence do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
