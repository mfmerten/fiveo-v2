# == Schema Information
#
# Table name: transfers
#
#  id                 :integer          not null, primary key
#  booking_id         :integer          not null
#  person_id          :integer          not null
#  from_agency        :string(255)      default(""), not null
#  from_officer       :string(255)      default(""), not null
#  from_officer_unit  :string(255)      default(""), not null
#  from_officer_badge :string(255)      default(""), not null
#  to_agency          :string(255)      default(""), not null
#  to_officer         :string(255)      default(""), not null
#  to_officer_unit    :string(255)      default(""), not null
#  to_officer_badge   :string(255)      default(""), not null
#  remarks            :text             default(""), not null
#  reported_datetime  :datetime
#  created_by         :integer          default(1)
#  updated_by         :integer          default(1)
#  created_at         :datetime
#  updated_at         :datetime
#  transfer_datetime  :datetime
#  transfer_type      :string(255)      default(""), not null
#
# Indexes
#
#  index_transfers_on_booking_id  (booking_id)
#  index_transfers_on_created_by  (created_by)
#  index_transfers_on_person_id   (person_id)
#  index_transfers_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  transfers_booking_id_fk  (booking_id => bookings.id)
#  transfers_created_by_fk  (created_by => users.id)
#  transfers_person_id_fk   (person_id => people.id)
#  transfers_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Transfer do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
