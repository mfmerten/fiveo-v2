# == Schema Information
#
# Table name: garnishments
#
#  id             :integer          not null, primary key
#  sort_name      :string(255)      default(""), not null
#  suit_no        :string(255)      default(""), not null
#  description    :string(255)      default(""), not null
#  attorney       :string(255)      default(""), not null
#  details        :text             default(""), not null
#  received_date  :date
#  satisfied_date :date
#  remarks        :text             default(""), not null
#  created_by     :integer          default(1)
#  updated_by     :integer          default(1)
#  created_at     :datetime
#  updated_at     :datetime
#
# Indexes
#
#  index_garnishments_on_created_by  (created_by)
#  index_garnishments_on_sort_name   (sort_name)
#  index_garnishments_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  garnishments_created_by_fk  (created_by => users.id)
#  garnishments_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Garnishment do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
