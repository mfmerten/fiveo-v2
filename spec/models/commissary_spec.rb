# == Schema Information
#
# Table name: commissaries
#
#  id               :integer          not null, primary key
#  person_id        :integer          not null
#  officer          :string(255)      default(""), not null
#  officer_unit     :string(255)      default(""), not null
#  deposit          :decimal(12, 2)   default(0.0), not null
#  withdraw         :decimal(12, 2)   default(0.0), not null
#  created_by       :integer          default(1)
#  updated_by       :integer          default(1)
#  created_at       :datetime
#  updated_at       :datetime
#  officer_badge    :string(255)      default(""), not null
#  report_datetime  :datetime
#  receipt_no       :string(255)      default(""), not null
#  memo             :string(255)      default(""), not null
#  transaction_date :date
#  jail_id          :integer          not null
#  category         :string(255)      default(""), not null
#
# Indexes
#
#  index_commissaries_on_booking_id  (person_id)
#  index_commissaries_on_created_by  (created_by)
#  index_commissaries_on_jail_id     (jail_id)
#  index_commissaries_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  commissaries_created_by_fk  (created_by => users.id)
#  commissaries_jail_id_fk     (jail_id => jails.id)
#  commissaries_person_id_fk   (person_id => people.id)
#  commissaries_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Commissary do
  
  ## table columns
  #it { is_expected.to have_db_column(:person_id).of_type(:integer).with_options(null: false) }
  #it { is_expected.to have_db_column(:officer).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:officer_unit).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:officer_badge).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:deposit).of_type(:decimal).with_options(scale: 12, precision: 2, default: 0.0, null: false) }
  #it { is_expected.to have_db_column(:withdraw).of_type(:decimal).with_options(scale: 12, precision: 2, default: 0.0, null: false) }
  #it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:report_datetime).of_type(:datetime) }
  #it { is_expected.to have_db_column(:receipt_no).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:memo).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:transaction_date).of_type(:date) }
  #it { is_expected.to have_db_column(:jail_id).of_type(:integer).with_options(null: false) }
  #it { is_expected.to have_db_column(:category).of_type(:string).with_options(null: false, default: '') }
  #
  ## table indexes
  #it { is_expected.to have_db_index(:person_id) }
  #it { is_expected.to have_db_index(:jail_id) }
  #it { is_expected.to have_db_index(:created_by) }
  #it { is_expected.to have_db_index(:updated_by) }
  #
  ## associations
  #it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:created_by) }
  #it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key(:updated_by) }
  #it { is_expected.to belong_to(:person).inverse_of(:commissaries) }
  #it { is_expected.to belong_to(:jail).inverse_of(:commissaries) }
  #
  ## attributes
  #it { is_expected.to have_attr_accessor(:officer_id) }
  #
  ## scopes
  #describe "scopes" do
  #  before(:context) do
  #    # create test records
  #  end
  #  
  #  it "should return unposted commissaries using :unposted scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return commissaries posted on certain date using :posted_on scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return commissaries with deposits using :deposits scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return commissaries with withdrawals using :withdrawals scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return commissaries in person name order using :by_name scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return commissaries in transaction date order using :by_date scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_daterange when using :between scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_daterange when using :on_or_before scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return commissaries only for people with non-zero balance using :for_balance scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return commissaries for a particular person using :belonging_to_person scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return commissaries for a particular jail using :belonging_to_jail scope" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## callbacks
  #
  #describe "before_save" do
  #  describe ":check_creator" do
  #    it "should be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  #describe "before_validation" do
  #  describe ":check_amounts" do
  #    it "should be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #  
  #  describe ":lookup_officers" do
  #    it "should be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  #describe "before_destroy" do
  #  describe ":check_destroyable" do
  #    it "should be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  ## validations
  #
  #it { is_expected.to validate_presence_of(:officer) }
  #it { is_expected.to validate_date(:transaction_date) }
  #it { is_expected.to validate_numericality_of(:deposit).is_greater_than_or_equal_to(0).is_less_than(10 ** 10) }
  #it { is_expected.to validate_numericality_of(:withdraw).is_greater_than_or_equal_to(0).is_less_than(10 ** 10) }
  #it { is_expected.to validate_person(:person_id) }
  #
  #describe ":jail_id" do
  #  it "should be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## constants
  #
  #describe "CommissaryType" do
  #  it "should be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## methods
  #it { is_expected.to have_defined_desc }
  #it { is_expected.to have_defined_base_perms }
  #
  #describe ".posting_dates" do
  #  it "should be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#reasons_not_destroyable" do
  #  it "should be completed" do
  #    expect(false).to be(true)
  #  end
  #end
end
