# == Schema Information
#
# Table name: investigations
#
#  id                     :integer          not null, primary key
#  private                :boolean          default(TRUE), not null
#  details                :text             default(""), not null
#  remarks                :text             default(""), not null
#  created_by             :integer          default(1)
#  updated_by             :integer          default(1)
#  created_at             :datetime
#  updated_at             :datetime
#  owned_by               :integer          default(1)
#  investigation_datetime :datetime
#  status                 :string(255)      default(""), not null
#
# Indexes
#
#  index_investigations_on_created_by  (created_by)
#  index_investigations_on_owned_by    (owned_by)
#  index_investigations_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  investigations_created_by_fk  (created_by => users.id)
#  investigations_owned_by_fk    (owned_by => users.id)
#  investigations_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Investigation do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
