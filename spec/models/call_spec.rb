# == Schema Information
#
# Table name: calls
#
#  id                  :integer          not null, primary key
#  case_no             :string(255)      default(""), not null
#  received_by         :string(255)      default(""), not null
#  details             :text             default(""), not null
#  dispatcher          :string(255)      default(""), not null
#  ward                :string(255)      default(""), not null
#  district            :string(255)      default(""), not null
#  remarks             :text             default(""), not null
#  created_at          :datetime
#  updated_at          :datetime
#  created_by          :integer          default(1)
#  updated_by          :integer          default(1)
#  dispatcher_unit     :string(255)      default(""), not null
#  received_by_unit    :string(255)      default(""), not null
#  received_by_badge   :string(255)      default(""), not null
#  dispatcher_badge    :string(255)      default(""), not null
#  detective           :string(255)      default(""), not null
#  detective_unit      :string(255)      default(""), not null
#  detective_badge     :string(255)      default(""), not null
#  legacy              :boolean          default(FALSE), not null
#  call_datetime       :datetime
#  dispatch_datetime   :datetime
#  arrival_datetime    :datetime
#  concluded_datetime  :datetime
#  received_via        :string(255)      default(""), not null
#  disposition         :string(255)      default(""), not null
#  signal_code         :string(255)      default(""), not null
#  signal_name         :string(255)      default(""), not null
#  zone                :string(255)      default(""), not null
#  call_subjects_count :integer          default(0), not null
#
# Indexes
#
#  index_calls_on_case_no      (case_no)
#  index_calls_on_created_by   (created_by)
#  index_calls_on_signal_code  (signal_code)
#  index_calls_on_updated_by   (updated_by)
#
# Foreign Keys
#
#  calls_created_by_fk  (created_by => users.id)
#  calls_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Call do
  
  describe "Table Columns" do
    it { is_expected.to have_db_column(:case_no).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:received_by).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:received_by_unit).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:received_by_badge).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:details).of_type(:text).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:dispatcher).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:dispatcher_unit).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:dispatcher_badge).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:ward).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:district).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:zone).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:remarks).of_type(:text).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
    it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_options(default: 1) }
    it { is_expected.to have_db_column(:detective).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:detective_unit).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:detective_badge).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:legacy).of_type(:boolean).with_options(null: false, default: false) }
    it { is_expected.to have_db_column(:call_datetime).of_type(:datetime) }
    it { is_expected.to have_db_column(:dispatch_datetime).of_type(:datetime) }
    it { is_expected.to have_db_column(:arrival_datetime).of_type(:datetime) }
    it { is_expected.to have_db_column(:concluded_datetime).of_type(:datetime) }
    it { is_expected.to have_db_column(:received_via).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:disposition).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:signal_code).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:signal_name).of_type(:string).with_options(null: false, default: '') }
    it { is_expected.to have_db_column(:call_subjects_count).of_type(:integer).with_options(null: false, default: 0) }
  end
  
  describe "Table Indices" do
    it { is_expected.to have_db_index(:case_no) }
    it { is_expected.to have_db_index(:created_by) }
    it { is_expected.to have_db_index(:updated_by) }
    it { is_expected.to have_db_index(:signal_code) }
  end
  
  describe "Associations" do
    it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:created_by) }
    it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key(:updated_by) }
    it { is_expected.to have_many(:offenses).dependent(:nullify) }
    it { is_expected.to have_many(:forbids).dependent(:nullify) }
    it { is_expected.to have_many(:invest_cases).dependent(:destroy) }
    it { is_expected.to have_many(:investigations).through(:invest_cases) }
    it { is_expected.to have_many(:call_units).dependent(:destroy) }
    it { is_expected.to have_many(:call_subjects).dependent(:destroy) }
  end
  
  describe "Attributes" do
    it { is_expected.to accept_nested_attributes_for(:call_units).allow_destroy(true) }
    it { is_expected.to accept_nested_attributes_for(:call_subjects).allow_destroy(true) }
    it { is_expected.to have_attr_accessor(:followup_no) }
    it { is_expected.to have_attr_accessor(:received_by_id) }
    it { is_expected.to have_attr_accessor(:dispatcher_id) }
    it { is_expected.to have_attr_accessor(:detective_id) }
  end
  
  describe "Scopes" do
    let!(:c1) { create(:call, call_datetime: Time.now - 1.hour, ward: 2, district: 2, zone: 2) }
    let!(:c2) { create(:call, call_datetime: Time.now, ward: 1, district: 1, zone: 1) }
    
    it "should return calls in reverse ID order using :by_reverse_id scope" do
      expect(Call.by_reverse_id.pluck(:id)).to eq([c2.id,c1.id])
    end
    
    it "should return calls in reverse call_datetime order using :by_reverse_date scope" do
      expect(Call.by_reverse_date.pluck(:id)).to eq([c2.id,c1.id])
    end
    
    it "should return calls in call_datetime order using :by_date scope" do
      expect(Call.by_date.pluck(:id)).to eq([c1.id,c2.id])
    end
    
    it "should call AppUtils.like when using :with_signal_code scope" do
      allow(AppUtils).to receive(:like)
      Call.with_signal_code("98")
      expect(AppUtils).to have_received(:like).with(Call, :signal_code, "98")
    end
    
    it "should call AppUtils.like when using :with_signal_name scope" do
      allow(AppUtils).to receive(:like)
      Call.with_signal_name("Burglary")
      expect(AppUtils).to have_received(:like).with(Call, :signal_name, "Burglary")
    end
    
    it "should return calls with case number when using :with_case_no scope" do
      expect(Call.with_case_no(c2.case_no).pluck(:id)).to eq([c2.id])
    end
    
    it "should call AppUtils.like when using :with_received_via scope" do
      allow(AppUtils).to receive(:like)
      Call.with_received_via("911")
      expect(AppUtils).to have_received(:like).with(Call, :received_via, "911")
    end
    
    it "should return calls with ward when using :with_ward scope" do
      expect(Call.with_ward(2).pluck(:id)).to eq([c1.id])
    end
    
    it "should return calls with district when using :with_district scope" do
      expect(Call.with_district(1).pluck(:id)).to eq([c2.id])
    end
    
    it "should return calls with zone when using :with_zone scope" do
      expect(Call.with_zone(2).pluck(:id)).to eq([c1.id])
    end
    
    it "should call AppUtils.like when using :with_name scope" do
      allow(AppUtils).to receive(:like)
      Call.with_name("Smith")
      expect(AppUtils).to have_received(:like).with(Call, :name, "Smith", association: :call_subjects)
    end
    
    it "should call AppUtils.like when using :with_address scope" do
      allow(AppUtils).to receive(:like)
      Call.with_address("Main")
      expect(AppUtils).to have_received(:like).with(Call, [:address1, :address2], "Main", association: :call_subjects)
    end
    
    it "should call AppUtils.like when using :with_details scope" do
      allow(AppUtils).to receive(:like)
      Call.with_details("Test")
      expect(AppUtils).to have_received(:like).with(Call, :details, "Test")
    end
    
    it "should call AppUtils.like when using :with_units scope" do
      allow(AppUtils).to receive(:like)
      Call.with_units("SP1")
      expect(AppUtils).to have_received(:like).with(Call, [:officer, :officer_unit], "SP1", association: :call_units)
    end
    
    it "should call AppUtils.like when using :with_disposition scope" do
      allow(AppUtils).to receive(:like)
      Call.with_disposition("Officer")
      expect(AppUtils).to have_received(:like).with(Call, :disposition, "Officer")
    end
    
    it "should call AppUtils.like when using :with_remarks scope" do
      allow(AppUtils).to receive(:like)
      Call.with_remarks("Test")
      expect(AppUtils).to have_received(:like).with(Call, :remarks, "Test")
    end
    
    it "should call AppUtils.for_daterange when using :with_call_date scope" do
      allow(AppUtils).to receive(:for_daterange)
      Call.with_call_date("01/01/2015","01/02/2015")
      expect(AppUtils).to have_received(:for_daterange).with(Call, :call_datetime, start: "01/01/2015", stop: "01/02/2015")
    end
    
    it "should call AppUtils.for_officer when using :with_received_by scope" do
      allow(AppUtils).to receive(:for_officer)
      Call.with_received_by("Smith",nil,nil)
      expect(AppUtils).to have_received(:for_officer).with(Call, :received_by, "Smith", nil, nil)
    end
    
    it "should call AppUtils.for_officer when using :with_detective scope" do
      allow(AppUtils).to receive(:for_officer)
      Call.with_detective("Smith",nil,nil)
      expect(AppUtils).to have_received(:for_officer).with(Call, :detective, "Smith", nil, nil)
    end
    
    it "should call AppUtils.for_officer when using :with_dispatcher scope" do
      allow(AppUtils).to receive(:for_officer)
      Call.with_dispatcher("Smith",nil,nil)
      expect(AppUtils).to have_received(:for_officer).with(Call, :dispatcher, "Smith", nil, nil)
    end
  end
  
  describe "Callbacks" do
    describe "after_validation" do
      describe ":check_creator" do
        it "should call #check_creator after record is validated" do
          c1 = build(:call)
          allow(c1).to receive(:check_creator)
          c1.valid?
          expect(c1).to have_received(:check_creator).with(no_args)
        end
      end
    end
  
    describe "before_destroy" do
      describe ":check_destroyable" do
        it "should call #check_destroyable before destroy" do
          c1 = build(:call)
          allow(c1).to receive(:check_destroyable).and_return(false)
          c1.destroy
          expect(c1).to have_received(:check_destroyable).with(no_args)
        end
      end
    end
  
    describe "before_validation" do
      describe ":check_or_generate_case" do
        it "should call #check_or_generate_case before validation" do
          c1 = build(:call)
          allow(c1).to receive(:check_or_generate_case)
          c1.valid?
          expect(c1).to have_received(:check_or_generate_case).with(no_args)
        end
      end
    
      describe ":check_signal" do
        it "should call #check_signal before validation" do
          c1 = build(:call)
          allow(c1).to receive(:check_signal)
          c1.valid?
          expect(c1).to have_received(:check_signal).with(no_args)
        end
      end
    
      describe ":lookup_officers" do
        it "should call #lookup_officers before validation" do
          c1 = build(:call)
          allow(c1).to receive(:lookup_officers)
          c1.valid?
          expect(c1).to have_received(:lookup_officers).with(no_args)
        end
      end
    end
  
    describe "after_create" do
      describe ":send_messages" do
        it "should call #send_messages after creating new call" do
          c1 = build(:call)
          allow(c1).to receive(:send_messages).and_return(false)
          c1.save
          expect(c1).to have_received(:send_messages).with(no_args)
        end
      end
    end
  end
  
  describe "Validations" do
    # have to do this because of a "hidden" validation that prevents normal
    # validations from running unless a valid signal code is entered.
    # the "hidden" validation is tested below as #check_signal instance method
    subject { create(:call) }
    it { is_expected.to validate_associated(:call_units) }
    it { is_expected.to validate_associated(:call_subjects) }
    it { is_expected.to validate_presence_of(:details) }
    it { is_expected.to validate_presence_of(:dispatcher) }
    it { is_expected.to validate_datetime(:call_datetime) }
    it { is_expected.to validate_datetime(:dispatch_datetime).allow_blank }
    it { is_expected.to validate_datetime(:arrival_datetime).allow_blank }
    it { is_expected.to validate_datetime(:concluded_datetime).allow_blank }
  end
  
  describe "Class Methods" do
    it { is_expected.to have_defined_desc }
    it { is_expected.to have_defined_base_perms}
    
    describe ".dispositions" do
      it "should return hash if no key given" do
        expect(Call.dispositions).to include('Officer Report' => 'OFC')
        expect(Call.dispositions).to be_a(Hash)
      end
      
      it "should return value if key given" do
        expect(Call.dispositions("Officer Report")).to eq('OFC')
      end
    end
    
    describe ".vias" do
      it "should return array of call via options" do
        expect(Call.vias).to include('911')
        expect(Call.vias).to be_a(Array)
      end
    end
    
    describe ".for_shift" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  
    describe ".process_query" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  end
  
  describe "Instance Methods" do
    describe "#reasons_not_destroyable" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  
    describe "#subject_summary" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  
    describe "#units" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#check_destroyable" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#check_or_generate_case" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#check_signal" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#lookup_officers" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
    
    describe "#send_messages" do
      it "needs to be completed" do
        expect(false).to be(true)
      end
    end
  end
end
