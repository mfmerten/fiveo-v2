# == Schema Information
#
# Table name: holds
#
#  id                     :integer          not null, primary key
#  booking_id             :integer          not null
#  hold_by                :string(255)      default(""), not null
#  hold_by_unit           :string(255)      default(""), not null
#  cleared_by             :string(255)      default(""), not null
#  cleared_by_unit        :string(255)      default(""), not null
#  remarks                :text             default(""), not null
#  created_by             :integer          default(1)
#  updated_by             :integer          default(1)
#  created_at             :datetime
#  updated_at             :datetime
#  arrest_id              :integer
#  hold_by_badge          :string(255)      default(""), not null
#  cleared_by_badge       :string(255)      default(""), not null
#  doc                    :boolean          default(FALSE), not null
#  probation_id           :integer
#  billing_report_date    :date
#  agency                 :string(255)      default(""), not null
#  transfer_officer       :string(255)      default(""), not null
#  transfer_officer_badge :string(255)      default(""), not null
#  transfer_officer_unit  :string(255)      default(""), not null
#  fines                  :decimal(12, 2)   default(0.0), not null
#  datetime_counted       :datetime
#  doc_billable           :boolean          default(FALSE), not null
#  hours_served           :integer          default(0), not null
#  hours_goodtime         :float            default(0.0), not null
#  sentence_id            :integer
#  transfer_id            :integer
#  explanation            :string(255)      default(""), not null
#  bill_from_date         :date
#  legacy                 :boolean          default(FALSE), not null
#  revocation_id          :integer
#  billed_retroactive     :date
#  hours_total            :integer          default(0), not null
#  hours_time_served      :integer          default(0), not null
#  deny_goodtime          :boolean          default(FALSE), not null
#  other_billable         :boolean          default(FALSE), not null
#  death_sentence         :boolean          default(FALSE), not null
#  hold_datetime          :datetime
#  cleared_datetime       :datetime
#  hold_type              :string(255)      default(""), not null
#  cleared_because        :string(255)      default(""), not null
#  temp_release_reason    :string(255)      default(""), not null
#
# Indexes
#
#  index_holds_on_arrest_id      (arrest_id)
#  index_holds_on_booking_id     (booking_id)
#  index_holds_on_created_by     (created_by)
#  index_holds_on_probation_id   (probation_id)
#  index_holds_on_revocation_id  (revocation_id)
#  index_holds_on_sentence_id    (sentence_id)
#  index_holds_on_transfer_id    (transfer_id)
#  index_holds_on_updated_by     (updated_by)
#
# Foreign Keys
#
#  holds_arrest_id_fk      (arrest_id => arrests.id)
#  holds_booking_id_fk     (booking_id => bookings.id)
#  holds_created_by_fk     (created_by => users.id)
#  holds_probation_id_fk   (probation_id => probations.id)
#  holds_revocation_id_fk  (revocation_id => revocations.id)
#  holds_sentence_id_fk    (sentence_id => sentences.id)
#  holds_transfer_id_fk    (transfer_id => transfers.id)
#  holds_updated_by_fk     (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Hold do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
