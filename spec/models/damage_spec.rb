# == Schema Information
#
# Table name: damages
#
#  id              :integer          not null, primary key
#  person_id       :integer
#  case_no         :string(255)      default(""), not null
#  sort_name       :string(255)      default(""), not null
#  victim_address  :string(255)      default(""), not null
#  victim_phone    :string(255)      default(""), not null
#  crimes          :string(255)      default(""), not null
#  damage_datetime :datetime
#  created_by      :integer          default(1)
#  updated_by      :integer          default(1)
#  created_at      :datetime
#  updated_at      :datetime
#
# Indexes
#
#  index_damages_on_created_by  (created_by)
#  index_damages_on_person_id   (person_id)
#  index_damages_on_sort_name   (sort_name)
#  index_damages_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  damages_created_by_fk  (created_by => users.id)
#  damages_person_id_fk   (person_id => people.id)
#  damages_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Damage do
  
  ## table column
  #it { is_expected.to have_db_column(:person_id).of_type(:integer) }
  #it { is_expected.to have_db_column(:case_no).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:sort_name).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:victim_address).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:victim_phone).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:crimes).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:damage_datetime).of_type(:datetime) }
  #it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  #
  ## table indexes
  #it { is_expected.to have_db_index(:created_by) }
  #it { is_expected.to have_db_index(:updated_by) }
  #it { is_expected.to have_db_index(:sort_name) }
  #it { is_expected.to have_db_index(:person_id) }
  #
  ## associations
  #it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:created_by) }
  #it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key(:updated_by) }
  #it { is_expected.to belong_to(:person) }
  #it { is_expected.to have_many(:damage_items).dependent(:destroy) }
  #
  ## attributes
  #it { is_expected.to accept_nested_attributes_for(:damage_items) }
  #
  ## scopes
  #describe "scopes" do
  #  before(:context) do
  #    # create test records
  #  end
  #  
  #  it "should return particular damage record using :with_damage_id scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return damage records for a particular case using :with_case_no scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return damage for a particular person using :with_person_id scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_person when using :with_name scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_address scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_description scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_daterange when using :with_damage_date" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## callbacks
  #
  #describe "before_save" do
  #  describe ":check_creator" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  #describe "before_validation" do
  #  describe ":lookup_victim" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #  
  #  describe ":adjust_case_no" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  ## validations
  #
  #describe ":damage_items" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #it { is_expected.to validate_presence_of(:case_no) }
  #it { is_expected.to validate_presence_of(:sort_name) }
  #it { is_expected.to validate_presence_of(:victim_address) }
  #it { is_expected.to validate_presence_of(:victim_phone) }
  #it { is_expected.to validate_datetime(:damage_datetime) }
  #
  ## methods
  #it { is_expected.to have_defined_desc }
  #it { is_expected.to have_defined_base_perms }
  #
  #describe ".process_filter" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#estimate" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#display_name" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
end
