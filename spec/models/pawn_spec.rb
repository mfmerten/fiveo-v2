# == Schema Information
#
# Table name: pawns
#
#  id            :integer          not null, primary key
#  ticket_no     :string(255)      default(""), not null
#  ticket_date   :date
#  case_no       :string(255)      default(""), not null
#  remarks       :text             default(""), not null
#  created_at    :datetime
#  updated_at    :datetime
#  created_by    :integer          default(1)
#  updated_by    :integer          default(1)
#  sort_name     :string(255)      default(""), not null
#  oln           :string(255)      default(""), not null
#  address1      :string(255)      default(""), not null
#  address2      :string(255)      default(""), not null
#  legacy        :boolean          default(FALSE), not null
#  oln_state     :string(255)      default(""), not null
#  pawn_co       :string(255)      default(""), not null
#  ticket_type   :string(255)      default(""), not null
#  encrypted_ssn :string           default("")
#
# Indexes
#
#  index_pawns_on_case_no     (case_no)
#  index_pawns_on_created_by  (created_by)
#  index_pawns_on_sort_name   (sort_name)
#  index_pawns_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  pawns_created_by_fk  (created_by => users.id)
#  pawns_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Pawn do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
