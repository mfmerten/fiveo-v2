# == Schema Information
#
# Table name: citations
#
#  id                :integer          not null, primary key
#  person_id         :integer          not null
#  officer           :string(255)      default(""), not null
#  officer_unit      :string(255)      default(""), not null
#  officer_badge     :string(255)      default(""), not null
#  ticket_no         :string(255)      default(""), not null
#  paid_date         :date
#  recalled_date     :date
#  created_by        :integer          default(1)
#  updated_by        :integer          default(1)
#  created_at        :datetime
#  updated_at        :datetime
#  citation_datetime :datetime
#
# Indexes
#
#  index_citations_on_created_by  (created_by)
#  index_citations_on_person_id   (person_id)
#  index_citations_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  citations_created_by_fk  (created_by => users.id)
#  citations_person_id_fk   (person_id => people.id)
#  citations_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Citation do
  
  ## table columns
  #it { is_expected.to have_db_column(:person_id).of_type(:integer).with_options(null: false) }
  #it { is_expected.to have_db_column(:officer).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:officer_unit).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:officer_badge).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:ticket_no).of_type(:string).with_options(null: false, default: '') }
  #it { is_expected.to have_db_column(:paid_date).of_type(:date) }
  #it { is_expected.to have_db_column(:recalled_date).of_type(:date) }
  #it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:citation_datetime).of_type(:datetime) }
  #
  ## table indexes
  #it { is_expected.to have_db_index(:created_by) }
  #it { is_expected.to have_db_index(:updated_by) }
  #it { is_expected.to have_db_index(:person_id) }
  #
  ## associations
  #it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:created_by) }
  #it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key(:updated_by) }
  #it { is_expected.to belong_to(:person).inverse_of(:citations) }
  #it { is_expected.to have_many(:offense_citations).dependent(:destroy) }
  #it { is_expected.to have_many(:offenses).through(:offense_citations) }
  #it { is_expected.to have_many(:charges).dependent(:destroy) }
  #it { is_expected.to have_many(:da_charges).through(:charges) }
  #it { is_expected.to have_many(:dockets).conditions(distinct).through(:da_charges) }
  #
  ## attributes
  #it { is_expected.to accept_nested_attributes_for(:charges).allow_destroy(true) }
  #it { is_expected.to have_attr_accessor(:officer_id) }
  #
  ## scopes
  #describe "scopes" do
  #  before(:context) do
  #    # create records for testing
  #  end
  #  
  #  it "should return citations in reverse citation_datetime order using :by_reverse_date scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return citation 1 but not citation 2 using :with_citation_id scope with citation 1 id" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_ticket_no scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.like when using :with_charge scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return citation 2 but not citation 1 using :with_person_id scope with citation 2 person id" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should return citation 1 but not citation 2 using :with_offense_id scope whith citation 1 offense id" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_officer when using :with_officer scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_daterange when using :with_citation_date scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_daterange when using :with_paid_date scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_daterange when using :with_recalled_date scope" do
  #    expect(false).to be(true)
  #  end
  #  
  #  it "should call AppUtils.for_person when using :with_person_name scope" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## callbacks
  #describe "before_save" do
  #  describe ":check_creator" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  #describe "before_validation" do
  #  describe ":lookup_officer" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  #describe "before_destroy" do
  #  describe ":check_destroyable" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  ## validations
  #
  #describe ":charges" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #it { is_expected.to validate_presence_of(:ticket_no) }
  #it { is_expected.to validate_presence_of(:officer) }
  #it { is_expected.to validate_datetime(:citation_datetime) }
  #it { is_expected.to validate_date(:paid_date).allow_blank }
  #it { is_expected.to validate_date(:recalled_date).allow_blank }
  #it { is_expected.to validate_person(:person_id) }
  #
  ## methods
  #it { is_expected.to have_defined_desc }
  #it { is_expected.to have_defined_base_perms }
  #
  #describe ".process_query" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#charge_summary" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#reasons_not_destroyable" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
end
