# == Schema Information
#
# Table name: courts
#
#  id                     :integer          not null, primary key
#  court_date             :date
#  next_court_date        :date
#  bench_warrant_issued   :boolean          default(FALSE), not null
#  bond_forfeiture_issued :boolean          default(FALSE), not null
#  is_upset_wo_refix      :boolean          default(FALSE), not null
#  is_refixed             :boolean          default(FALSE), not null
#  refix_reason           :string(255)      default(""), not null
#  is_dismissed           :boolean          default(FALSE), not null
#  is_overturned          :boolean          default(FALSE), not null
#  overturned_date        :date
#  created_by             :integer          default(1)
#  updated_by             :integer          default(1)
#  created_at             :datetime
#  updated_at             :datetime
#  psi                    :boolean          default(FALSE), not null
#  processed              :boolean          default(FALSE), not null
#  fines_paid             :boolean          default(FALSE), not null
#  notes                  :text             default(""), not null
#  remarks                :text             default(""), not null
#  court_type             :string(255)      default(""), not null
#  next_court_type        :string(255)      default(""), not null
#  docket_type            :string(255)      default(""), not null
#  next_docket_type       :string(255)      default(""), not null
#  plea_type              :string(255)      default(""), not null
#  judge                  :string(255)      default(""), not null
#  next_judge             :string(255)      default(""), not null
#  writ_of_attach         :boolean          default(FALSE), not null
#  not_present            :boolean          default(FALSE), not null
#  docket_id              :integer          not null
#
# Indexes
#
#  index_courts_on_created_by  (created_by)
#  index_courts_on_docket_id   (docket_id)
#  index_courts_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  courts_created_by_fk  (created_by => users.id)
#  courts_updated_by_fk  (updated_by => users.id)
#  fk_rails_ac203dfa9b   (docket_id => dockets.id)
#

require 'rails_helper'

RSpec.describe Court do
  
  ## table columns
  #it { is_expected.to have_db_column(:docket_id).of_type(:integer).with_options(null: false) }
  #it { is_expected.to have_db_column(:court_date).of_type(:date) }
  #it { is_expected.to have_db_column(:next_court_date).of_type(:date) }
  #it { is_expected.to have_db_column(:bench_warrant_issued).of_type(:boolean).with_options(default: false, null: false) }
  #it { is_expected.to have_db_column(:bond_forfeiture_issued).of_type(:boolean).with_options(default: false, null: false) }
  #it { is_expected.to have_db_column(:is_upset_wo_refix).of_type(:boolean).with_options(default: false, null: false) }
  #it { is_expected.to have_db_column(:is_refixed).of_type(:boolean).with_options(default: false, null: false) }
  #it { is_expected.to have_db_column(:refix_reason).of_type(:string).with_options(default: '', null: false) }
  #it { is_expected.to have_db_column(:is_dismissed).of_type(:boolean).with_options(default: false, null: false) }
  #it { is_expected.to have_db_column(:is_overturned).of_type(:boolean).with_options(default: false, null: false) }
  #it { is_expected.to have_db_column(:overturned_date).of_type(:date) }
  #it { is_expected.to have_db_column(:created_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:updated_by).of_type(:integer).with_options(default: 1) }
  #it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:psi).of_type(:boolean).with_options(default: false, null: false) }
  #it { is_expected.to have_db_column(:processed).of_type(:boolean).with_options(default: false, null: false) }
  #it { is_expected.to have_db_column(:fines_paid).of_type(:boolean).with_options(default: false, null: false) }
  #it { is_expected.to have_db_column(:notes).of_type(:text).with_options(default: '', null: false) }
  #it { is_expected.to have_db_column(:remarks).of_type(:text).with_options(default: "", null: false) }
  #it { is_expected.to have_db_column(:court_type).of_type(:string).with_options(default: "", null: false) }
  #it { is_expected.to have_db_column(:next_court_type).of_type(:string).with_options(default: "", null: false) }
  #it { is_expected.to have_db_column(:docket_type).of_type(:string).with_options(default: "", null: false) }
  #it { is_expected.to have_db_column(:next_docket_type).of_type(:string).with_options(default: "", null: false) }
  #it { is_expected.to have_db_column(:plea_type).of_type(:string).with_options(default: "", null: false) }
  #it { is_expected.to have_db_column(:judge).of_type(:string).with_options(default: "", null: false) }
  #it { is_expected.to have_db_column(:next_judge).of_type(:string).with_options(default: "", null: false) }
  #it { is_expected.to have_db_column(:writ_of_attach).of_type(:boolean).with_options(default: false, null: false) }
  #it { is_expected.to have_db_column(:not_present).of_type(:boolean).with_options(default: false, null: false) }
  #
  ## table indexes
  #it { is_expected.to have_db_index(:created_by) }
  #it { is_expected.to have_db_index(:updated_by) }
  #it { is_expected.to have_db_index(:docket_id) }
  #
  ## associatons
  #it { is_expected.to belong_to(:creator).class_name('User').with_foreign_key(:created_by) }
  #it { is_expected.to belong_to(:updater).class_name('User').with_foreign_key(:updated_by) }
  #it { is_expected.to belong_to(:docket).inverse_of(:courts) }
  #it { is_expected.to have_many(:sentences).dependent(:destroy).autosave(:false) }
  #it { is_expected.to have_many(:probations).dependent(:destroy) }
  #
  ## scopes
  #describe "scopes" do
  #  before(:context) do
  #    # create records for test
  #  end
  #  
  #  it "should return courts for specific court date, judge and docket type using :for_letters scope" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## callbacks
  #describe "before_save" do
  #  describe ":check_creator" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  #describe "after_save" do
  #  describe ":generate_next_court" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  ## validations
  #it { is_expected.to validate_presence_of(:judge) }
  #it { is_expected.to validate_date(:court_date) }
  #it { is_expected.to validate_date(:next_court_date).allow_blank }
  #it { is_expected.to validate_date(:overturned_date).allow_blank }
  #
  #describe ":docket_id" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  ## methods
  #it { is_expected.to have_defined_desc }
  #it { is_expected.to have_defined_base_perms }
  #
  #describe "#process_court" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#status" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#to_s" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
end
