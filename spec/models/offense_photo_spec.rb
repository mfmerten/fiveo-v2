# == Schema Information
#
# Table name: offense_photos
#
#  id                             :integer          not null, primary key
#  offense_id                     :integer          not null
#  case_no                        :string(255)      default(""), not null
#  crime_scene_photo_file_name    :string(255)
#  crime_scene_photo_content_type :string(255)
#  crime_scene_photo_file_size    :integer
#  crime_scene_photo_updated_at   :datetime
#  description                    :string(255)      default(""), not null
#  date_taken                     :date
#  location_taken                 :string(255)      default(""), not null
#  taken_by                       :string(255)      default(""), not null
#  taken_by_unit                  :string(255)      default(""), not null
#  remarks                        :text             default(""), not null
#  created_by                     :integer          default(1)
#  updated_by                     :integer          default(1)
#  created_at                     :datetime
#  updated_at                     :datetime
#  taken_by_badge                 :string(255)      default(""), not null
#
# Indexes
#
#  index_offense_photos_on_case_no     (case_no)
#  index_offense_photos_on_created_by  (created_by)
#  index_offense_photos_on_offense_id  (offense_id)
#  index_offense_photos_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  offense_photos_created_by_fk  (created_by => users.id)
#  offense_photos_offense_id_fk  (offense_id => offenses.id)
#  offense_photos_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe OffensePhoto do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
