# == Schema Information
#
# Table name: paper_customers
#
#  id            :integer          not null, primary key
#  name          :string(255)      default(""), not null
#  street        :string(255)      default(""), not null
#  city          :string(255)      default(""), not null
#  zip           :string(255)      default(""), not null
#  phone         :string(255)      default(""), not null
#  attention     :string(255)      default(""), not null
#  remarks       :text             default(""), not null
#  disabled      :boolean          default(FALSE), not null
#  created_by    :integer          default(1)
#  updated_by    :integer          default(1)
#  created_at    :datetime
#  updated_at    :datetime
#  noservice_fee :decimal(12, 2)   default(0.0), not null
#  mileage_fee   :decimal(12, 2)   default(0.0), not null
#  fax           :string(255)      default(""), not null
#  street2       :string(255)      default(""), not null
#  state         :string(255)
#
# Indexes
#
#  index_paper_customers_on_created_by  (created_by)
#  index_paper_customers_on_name        (name)
#  index_paper_customers_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  paper_customers_created_by_fk  (created_by => users.id)
#  paper_customers_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe PaperCustomer do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
