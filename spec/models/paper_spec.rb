# == Schema Information
#
# Table name: papers
#
#  id                :integer          not null, primary key
#  suit_no           :string(255)      default(""), not null
#  plaintiff         :string(255)      default(""), not null
#  defendant         :string(255)      default(""), not null
#  paper_customer_id :integer
#  serve_to          :string(255)      default(""), not null
#  street            :string(255)      default(""), not null
#  city              :string(255)      default(""), not null
#  zip               :string(255)      default(""), not null
#  phone             :string(255)      default(""), not null
#  received_date     :date
#  returned_date     :date
#  noservice_extra   :string(255)      default(""), not null
#  served_to         :string(255)      default(""), not null
#  officer           :string(255)      default(""), not null
#  officer_badge     :string(255)      default(""), not null
#  officer_unit      :string(255)      default(""), not null
#  comments          :text             default(""), not null
#  billable          :boolean          default(TRUE), not null
#  invoice_datetime  :datetime
#  paid_date         :date
#  invoice_total     :decimal(12, 2)   default(0.0), not null
#  noservice_fee     :decimal(12, 2)   default(0.0), not null
#  mileage_fee       :decimal(12, 2)   default(0.0), not null
#  service_fee       :decimal(12, 2)   default(0.0), not null
#  memo              :string(255)      default(""), not null
#  remarks           :text             default(""), not null
#  created_by        :integer          default(1)
#  updated_by        :integer          default(1)
#  created_at        :datetime
#  updated_at        :datetime
#  court_date        :date
#  mileage           :decimal(5, 1)    default(0.0), not null
#  report_datetime   :datetime
#  mileage_only      :boolean          default(FALSE), not null
#  state             :string(255)      default(""), not null
#  served_datetime   :datetime
#  noservice_type    :string(255)      default(""), not null
#  service_type      :string(255)      default(""), not null
#  paid_in_full      :boolean          default(FALSE), not null
#  paper_type        :string(255)      default(""), not null
#
# Indexes
#
#  index_papers_on_created_by         (created_by)
#  index_papers_on_paper_customer_id  (paper_customer_id)
#  index_papers_on_updated_by         (updated_by)
#
# Foreign Keys
#
#  papers_created_by_fk         (created_by => users.id)
#  papers_paper_customer_id_fk  (paper_customer_id => paper_customers.id)
#  papers_updated_by_fk         (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Paper do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
