# == Schema Information
#
# Table name: da_charges
#
#  id            :integer          not null, primary key
#  docket_id     :integer          not null
#  charge_id     :integer
#  count         :integer          default(1), not null
#  charge        :string(255)      default(""), not null
#  dropped_by_da :boolean          default(FALSE), not null
#  processed     :boolean          default(FALSE), not null
#  created_at    :datetime
#  updated_at    :datetime
#  arrest_id     :integer
#  info_only     :boolean          default(FALSE), not null
#  da_diversion  :boolean          default(FALSE), not null
#
# Indexes
#
#  index_da_charges_on_arrest_id  (arrest_id)
#  index_da_charges_on_charge_id  (charge_id)
#  index_da_charges_on_docket_id  (docket_id)
#
# Foreign Keys
#
#  da_charges_arrest_id_fk  (arrest_id => arrests.id)
#  da_charges_charge_id_fk  (charge_id => charges.id)
#  da_charges_docket_id_fk  (docket_id => dockets.id)
#

require 'rails_helper'

RSpec.describe DaCharge do
  
  ## table columns
  #it { is_expected.to have_db_column(:docket_id).of_type(:integer).with_options(null: false) }
  #it { is_expected.to have_db_column(:charge_id).of_type(:integer) }
  #it { is_expected.to have_db_column(:count).of_type(:integer).with_options(default: 1, null: false) }
  #it { is_expected.to have_db_column(:charge).of_type(:string).with_options(default: '', null: false) }
  #it { is_expected.to have_db_column(:dropped_by_da).of_type(:boolean).with_options(default: false, null: false) }
  #it { is_expected.to have_db_column(:processed).of_type(:boolean).with_options(default: false, null: false) }
  #it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  #it { is_expected.to have_db_column(:arrest_id).of_type(:integer) }
  #it { is_expected.to have_db_column(:info_only).of_type(:boolean).with_options(default: false, null: false) }
  #it { is_expected.to have_db_column(:da_diversion).of_type(:boolean).with_options(default: false, null: false) }
  #
  ## table indexes
  #it { is_expected.to have_db_index(:arrest_id) }
  #it { is_expected.to have_db_index(:charge_id) }
  #it { is_expected.to have_db_index(:docket_id) }
  #
  ## associations
  #it { is_expected.to belong_to(:docket).inverse_of(:da_charges) }
  #it { is_expected.to belong_to(:arrest_charge).class_name('Charge').with_foreign_key(:charge_id).inverse_of(:da_charges) }
  #it { is_expected.to belong_to(:arrest).inverse_of(:da_charges) }
  #it { is_expected.to have_one(:sentence).dependent(:destroy).inverse_of(:da_charge).autosave(false) }
  #it { is_expected.to have_one(:probation).dependent(:destroy).inverse_of(:da_charge) }
  #
  ## callbacks
  #describe "after_save" do
  #  describe ":check_dropped" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #  
  #  describe ":check_info_only" do
  #    it "needs to be completed" do
  #      expect(false).to be(true)
  #    end
  #  end
  #end
  #
  ## validations
  #it { is_expected.to validate_presence_of(:count) }
  #it { is_expected.to validate_presence_of(:charge) }
  #
  ## methods
  #it { is_expected.to have_defined_desc }
  #
  #describe "#is_final?" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#time_served_in_hours" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
  #
  #describe "#to_s" do
  #  it "needs to be completed" do
  #    expect(false).to be(true)
  #  end
  #end
end
