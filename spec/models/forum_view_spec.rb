# == Schema Information
#
# Table name: forum_views
#
#  id                :integer          not null, primary key
#  user_id           :integer          not null
#  count             :integer          default(0), not null
#  current_viewed_at :datetime
#  past_viewed_at    :datetime
#  created_at        :datetime
#  updated_at        :datetime
#  forum_id          :integer
#  forum_topic_id    :integer
#
# Indexes
#
#  index_forum_views_on_forum_id        (forum_id)
#  index_forum_views_on_forum_topic_id  (forum_topic_id)
#  index_forum_views_on_updated_at      (updated_at)
#  index_forum_views_on_user_id         (user_id)
#
# Foreign Keys
#
#  forum_views_forum_id_fk        (forum_id => forums.id)
#  forum_views_forum_topic_id_fk  (forum_topic_id => forum_topics.id)
#  forum_views_user_id_fk         (user_id => users.id)
#

require 'rails_helper'

RSpec.describe ForumView do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
