# == Schema Information
#
# Table name: jails
#
#  id                     :integer          not null, primary key
#  name                   :string(255)      default(""), not null
#  acronym                :string(255)      default(""), not null
#  street                 :string(255)      default(""), not null
#  city                   :string(255)      default(""), not null
#  state                  :string(255)      default(""), not null
#  zip                    :string(255)      default(""), not null
#  phone                  :string(255)      default(""), not null
#  fax                    :string(255)      default(""), not null
#  juvenile               :boolean          default(FALSE), not null
#  male                   :boolean          default(FALSE), not null
#  female                 :boolean          default(FALSE), not null
#  permset                :integer          default(1), not null
#  goodtime_by_default    :boolean          default(FALSE), not null
#  goodtime_hours_per_day :decimal(12, 2)   default(0.0), not null
#  afis_enabled           :boolean          default(FALSE), not null
#  remarks                :text             default(""), not null
#  created_by             :integer          default(1)
#  updated_by             :integer          default(1)
#  created_at             :datetime
#  updated_at             :datetime
#  notify_frequency       :string(255)      default(""), not null
#
# Indexes
#
#  index_jails_on_created_by  (created_by)
#  index_jails_on_updated_by  (updated_by)
#
# Foreign Keys
#
#  jails_created_by_fk  (created_by => users.id)
#  jails_updated_by_fk  (updated_by => users.id)
#

require 'rails_helper'

RSpec.describe Jail do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
