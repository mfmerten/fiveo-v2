# == Schema Information
#
# Table name: visitors
#
#  id            :integer          not null, primary key
#  booking_id    :integer          not null
#  name          :string(255)      default(""), not null
#  street        :string(255)      default(""), not null
#  city          :string(255)      default(""), not null
#  zip           :string(255)      default(""), not null
#  phone         :string(255)      default(""), not null
#  visit_date    :date
#  sign_in_time  :string(255)      default(""), not null
#  sign_out_time :string(255)      default(""), not null
#  remarks       :text             default(""), not null
#  created_at    :datetime
#  updated_at    :datetime
#  legacy        :boolean          default(FALSE), not null
#  state         :string(255)      default(""), not null
#  relationship  :string(255)      default(""), not null
#
# Indexes
#
#  index_visitors_on_booking_id  (booking_id)
#
# Foreign Keys
#
#  visitors_booking_id_fk  (booking_id => bookings.id)
#

require 'rails_helper'

RSpec.describe Visitor do
  #it "is not completed yet" do
  #  expect(false).to be(true)
  #end
end
