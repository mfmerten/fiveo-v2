# Ensures the attribute accepts valid phone numbers, or optionally blank/nil
# Examples:
#   it { should validate_phone(:value) }
#   it { should validate_phone(:value).allow_blank } # or .allow_nil if you prefer
module Shoulda
  module Matchers
    module ActiveModel # :nodoc
 
      def validate_phone(attribute)
        ValidatePhoneMatcher.new(attribute)
      end
 
      class ValidatePhoneMatcher < ValidationMatcher # :nodoc:
        def initialize(attribute)
          @attribute = attribute
          @blank = false
          @writer = false
          @valid_with_blank = false
          @valid_with_bad_phone = false
          @valid_with_phone = false
        end
 
        def allow_blank
          @blank = true
          self
        end
        
        def allow_null
          @blank = true
          self
        end
 
        def matches?(subject)
          @subject = subject
 
          @writer = @subject.respond_to?("#{@attribute}=")
          
          @subject.send("#{@attribute}=", '')
          if @subject.valid? || !@subject.errors.has_key?(@attribute)
            @valid_with_blank = true
          end

          @subject.send("#{@attribute}=", '999-999-9999')
          if @subject.valid? || !@subject.errors.has_key?(@attribute)
            @valid_with_phone = true
          end

          @subject.send("#{@attribute}=", 'bad_phone')
          if @subject.valid? || !@subject.errors.has_key?(@attribute)
            @valid_with_bad_phone = true
          end
          
          v = true
          v = v && @writer
          v = v && !@valid_with_bad_phone
          v = v && @valid_with_phone
          v = v && @valid_with_blank if @blank
          v
        end
 
        def description
          msg = "validates #{@attribute} is a valid Phone Number "
          if @blank
            msg += "or blank"
          end
          msg
        end
 
        def failure_message
          msg = "Expected #{@attribute} to accept valid Phone Number "
          if @blank
            msg += "or blank "
          end
          
          if !@writer
            msg += "but #{@attribute} setter does not exist!"
          elsif @valid_with_bad_person
            msg += "but #{@attribute} actually accepts invalid Phone Number!"
          elsif !@valid_with_blank && @blank
            msg += "but #{@attribute} does not accept blank!"
          else
            msg += "but #{@attribute} does not accept valid Phone Number!"
          end
          msg
        end
 
        def negative_failure_message
          "validator is not expected to be used in a negative context"
        end
      end
    end
  end
end
