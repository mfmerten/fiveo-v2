# Ensures the attribute is an id for a valid Person record, or optionally blank/nil
# Examples:
#   it { should validate_person(:value) }
#   it { should validate_person(:value).allow_blank } # or .allow_nil if you prefer
module Shoulda
  module Matchers
    module ActiveModel # :nodoc
 
      def validate_person(attribute)
        ValidatePersonMatcher.new(attribute)
      end
 
      class ValidatePersonMatcher < ValidationMatcher # :nodoc:
        def initialize(attribute)
          @attribute = attribute
          @blank = false
          @writer = false
          @valid_with_blank = false
          @valid_with_person = false
          @valid_with_bad_person = false
        end
 
        def allow_blank
          @blank = true
          self
        end
        
        def allow_null
          @blank = true
          self
        end
 
        def matches?(subject)
          @subject = subject
          person = Person.create(family_name: "Merten", given_name: "Michael")
 
          @writer = @subject.respond_to?("#{@attribute}=")

          @subject.send("#{@attribute}=", nil)
          if @subject.valid? || !@subject.errors.has_key?(@attribute)
            @valid_with_blank = true
          end

          @subject.send("#{@attribute}=", person.id)
          if @subject.valid? || !@subject.errors.has_key?(@attribute)
            @valid_with_person = true
          end

          @subject.send("#{@attribute}=", Person.last.id + 1)
          if @subject.valid? || !@subject.errors.has_key?(@attribute)
            @valid_with_bad_person = true
          end
          
          person.destroy
          
          v = true
          v = v && @writer
          v = v && !@valid_with_bad_person
          v = v && @valid_with_person
          v = v && @valid_with_blank if @blank
          v
        end
 
        def description
          msg = "validate #{@attribute} is a valid Person ID "
          if @blank
            msg += "or blank"
          end
          msg
        end
 
        def failure_message
          msg = "Expected #{@attribute} to accept valid Person ID "
          if @blank
            msg += "or blank "
          end
          
          if !@writer
            msg += "but #{@attribute} setter does not exist!"
          elsif @valid_with_bad_person
            msg += "but #{@attribute} actually accepts invalid Person ID!"
          elsif !@valid_with_blank && @blank
            msg += "but #{@attribute} does not accept blank!"
          else
            msg += "but #{@attribute} does not accept valid Person ID!"
          end
          msg
        end
 
        def negative_failure_message
          "validator is not expected to be used in a negative context"
        end
      end
    end
  end
end
