# Ensures the attribute accepts valid datetimes, or optionally blank/nil
# Examples:
#   it { should validate_datetime(:value) }
#   it { should validate_datetime(:value).allow_blank } # or .allow_nil if you prefer
module Shoulda
  module Matchers
    module ActiveModel # :nodoc
 
      def validate_datetime(attribute)
        ValidateDatetimeMatcher.new(attribute)
      end
 
      class ValidateDatetimeMatcher < ValidationMatcher # :nodoc:
        def initialize(attribute)
          @attribute = attribute
          @blank = false
          @writer = false
          @valid_with_bad_datetime = false
          @valid_with_datetime = false
        end
 
        def allow_blank
          @blank = true
          self
        end
        
        def allow_null
          @blank = true
          self
        end
 
        def matches?(subject)
          @subject = subject
 
          @writer = @subject.respond_to?("#{@attribute}=")
          
          @subject.send("#{@attribute}=", '')
          if @subject.valid? || !@subject.errors.has_key?(@attribute)
            @valid_with_blank = true
          end

          @subject.send("#{@attribute}=", '01/15/2015 12:00')
          if @subject.valid? || !@subject.errors.has_key?(@attribute)
            @valid_with_datetime = true
          end

          @subject.send("#{@attribute}=", 'bad_date')
          if @subject.valid? || !@subject.errors.has_key?(@attribute)
            @valid_with_bad_datetime = true
          end
          
          v = true
          v = v && @writer
          v = v && !@valid_with_bad_datetime
          v = v && @valid_with_datetime
          v = v && @valid_with_blank if @blank
          v
        end
 
        def description
          msg = "validate #{@attribute} is a valid Datetime "
          if @blank
            msg += "or blank"
          end
          msg
        end
 
        def failure_message
          msg = "Expected #{@attribute} to accept valid Datetime "
          if @blank
            msg += "or blank "
          end
          
          if !@writer
            msg += "but #{@attribute} setter does not exist!"
          elsif @valid_with_bad_datetime
            msg += "but #{@attribute} actually accepts invalid Datetime!"
          elsif !@valid_with_blank && @blank
            msg += "but #{@attribute} does not accept blank!"
          else
            msg += "but #{@attribute} does not accept valid Datetime!"
          end
          msg
        end
 
        def negative_failure_message
          "validator is not expected to be used in a negative context"
        end
      end
    end
  end
end
