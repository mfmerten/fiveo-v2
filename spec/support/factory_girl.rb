RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods
  
  config.before(:suite) do
    # truncate all tables and force user 1 to exist
    DatabaseCleaner.strategy = :truncation
    DatabaseCleaner.clean
    User.create(login: 'admin', keep_pass: true)
    DatabaseCleaner.strategy = :transaction
    
    begin
      DatabaseCleaner.start
      FactoryGirl.lint
    ensure
      DatabaseCleaner.clean
    end
  end
end