# Ensures that validate is called on associated record when validating subject
# Examples:
#   it { should validate_associated(:association) }
module Shoulda
  module Matchers
    module ActiveRecord # :nodoc
 
      def validate_associated(attribute)
        ValidateAssociatedMatcher.new(attribute)
      end
 
      class ValidateAssociatedMatcher < ActiveModel::ValidationMatcher # :nodoc:
        def initialize(attribute)
          @attribute = attribute
          @has_association = false
          @associated = nil
          @validated = false
        end
 
        def matches?(subject)
          @subject = subject
          
          if ref = @subject.class.reflect_on_association(@attribute)
            @has_association = true
            
            has_many = ref.macro == :has_many
          
            if has_many
              @associated = @subject.send(@attribute).build
            else
              @associated = @subject.send("build_#{@attribute.singularize}")
            end
          
            unless @associated.nil?
              # check @subject to see if errors follow through
              if !@subject.valid?
                @validated = true
              end
            end
          end

          v = true
          v = v && @has_association
          v = v && !@associated.nil?
          v = v && @validated
          v
        end
 
        def description
          msg = "validate associated #{@attribute} when validating self"
        end
 
        def failure_message
          if !@has_association
            msg += "expected to find #{@attribute} association, but none found!"
          elsif @associated.nil?
            msg += "found association but could not build associated object!"
          else
            msg += "expected #{@attribute} to be validated, but it didn't happen!"
          end
        end
 
        def negative_failure_message
          "validator is not expected to be used in a negative context"
        end
      end
    end
  end
end
