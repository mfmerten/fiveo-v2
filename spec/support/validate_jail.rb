# Ensures the attribute is an id for a valid Jail record, or optionally blank/nil
# Examples:
#   it { should validate_jail(:value) }
#   it { should validate_jail(:value).allow_blank } # or .allow_nil if you prefer
module Shoulda
  module Matchers
    module ActiveModel # :nodoc
 
      def validate_jail(attribute)
        ValidateJailMatcher.new(attribute)
      end
 
      class ValidateJailMatcher < ValidationMatcher # :nodoc:
        def initialize(attribute)
          @attribute = attribute
          @blank = false
          @writer = false
          @valid_with_blank = false
          @valid_with_jail = false
          @valid_with_bad_jail = false
        end
 
        def allow_blank
          @blank = true
          self
        end
        
        def allow_null
          @blank = true
          self
        end
 
        def matches?(subject)
          @subject = subject
          jail = Jail.create(name: "Toledo Bend Detention Center", acronym: 'TBDC')
 
          @writer = @subject.respond_to?("#{@attribute}=")

          @subject.send("#{@attribute}=", nil)
          if @subject.valid? || !@subject.errors.has_key?(@attribute)
            @valid_with_blank = true
          end

          @subject.send("#{@attribute}=", jail.id)
          if @subject.valid? || !@subject.errors.has_key?(@attribute)
            @valid_with_jail = true
          end

          @subject.send("#{@attribute}=", Jail.last.id + 1)
          if @subject.valid? || !@subject.errors.has_key?(@attribute)
            @valid_with_bad_jail = true
          end
          
          jail.destroy
          
          v = true
          v = v && @writer
          v = v && !@valid_with_bad_jail
          v = v && @valid_with_jail
          v = v && @valid_with_blank if @blank
          v
        end
 
        def description
          msg = "validate #{@attribute} is a valid Jail ID "
          if @blank
            msg += "or blank"
          end
          msg
        end
 
        def failure_message
          msg = "Expected #{@attribute} to accept valid Jail ID "
          if @blank
            msg += "or blank "
          end
          
          if !@writer
            msg += "but #{@attribute} setter does not exist!"
          elsif @valid_with_bad_jail
            msg += "but #{@attribute} actually accepts invalid Jail ID!"
          elsif !@valid_with_blank && @blank
            msg += "but #{@attribute} does not accept blank!"
          else
            msg += "but #{@attribute} does not accept valid Jail ID!"
          end
          msg
        end
 
        def negative_failure_message
          "validator is not expected to be used in a negative context"
        end
      end
    end
  end
end
