# Ensures the model has .base_perms class method that returns a Perm symbol.
# Examples:
#   it { should have_defined_base_perms }
module Shoulda
  module Matchers
    module ActiveModel # :nodoc
 
      def have_defined_base_perms
        HaveDefinedBasePermsMatcher.new
      end
 
      class HaveDefinedBasePermsMatcher < ValidationMatcher # :nodoc:
        
        def initialize(dummy = nil)
          @perm = nil
        end
        
        def matches?(subject)
          @subject = subject
 
          @reader = @subject.class.respond_to?('base_perms')
          @string = @subject.class.base_perms.is_a?(Symbol)
          @perm = Perm.get(@subject.class.base_perms)
           
          v = true
          v = v && @reader
          v = v && @string
          v = v && (@perm.to_i > 0)
          v
        end
 
        def description
          "have class method 'base_perms' returning valid Perm integer"
        end
 
        def failure_message
          if !@reader
            "Expected #{@subject.class.name} to respond to 'base_perms'"
          elsif !@string
            "Expected #{@subject.class.name}.base_perms return a Symbol: got #{@subject.class.base_perms}"
          else
            "Expected Perm(#{@subject.class.name}.base_perms) to be integer > 0: got #{@perm}"
          end
        end
 
        def negative_failure_message
          "Expected #{@subject.class.name} not to respond to 'base_perms'"
        end
      end
    end
  end
end