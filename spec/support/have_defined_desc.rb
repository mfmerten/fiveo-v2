# Ensures the model has .desc class method that describes the model.
# Examples:
#   it { should have_defined_desc }
module Shoulda
  module Matchers
    module ActiveModel # :nodoc
 
      def have_defined_desc
        HaveDefinedDescMatcher.new
      end
 
      class HaveDefinedDescMatcher < ValidationMatcher # :nodoc:
        
        def initialize(dummy = nil)
        end
        
        def matches?(subject)
          @subject = subject
 
          reader = @subject.class.respond_to?('desc')
          string = @subject.class.desc.is_a?(String)
          empty = @subject.class.desc.blank?
 
          v = true
          v = v && reader
          v = v && string
          v = v && !empty
          v
        end
 
        def description
          "have class method 'desc' returning non-blank String"
        end
 
        def failure_message
          "Expected #{@subject.class.name} to respond to 'desc' and return non-blank String"
        end
 
        def negative_failure_message
          "Expected #{@subject.class.name} not to respond to 'desc'"
        end
      end
    end
  end
end