# includes validations for models, and some common callback methods
module ApplicationValidations

  require 'active_support/core_ext/string/inflections'

  def self.included(base)
    base.extend ClassMethods
  end
  
  # ensures creator/updater/owner keys are not null
  # reference this in an after_validation callback
  # (if using before_save instead, it will be called twice!)
  def check_creator
    if respond_to?('created_by')
      self.created_by = 1 unless created_by?
    end
    if respond_to?('updated_by')
      unless updated_by?
        if respond_to?('created_by') && created_by?
          self.updated_by = created_by
        else
          self.updated_by = 1
        end
      end
    end
    if respond_to?('owned_by')
      unless owned_by?
        if respond_to?('updated_by') && updated_by?
          self.owned_by = updated_by
        elsif respond_to?('created_by') && created_by?
          self.owned_by = created_by
        else
          self.owned_by = 1
        end
      end
    end
    true
  end

  module ClassMethods
    # validates that a time entry consists of 2 digits, a colon and another
    # 2 digits, and that the hours and minutes values are in the proper range.
    def validates_time_string(*attr_names)
      configuration = {}
      configuration.update(attr_names.extract_options!)
      validates_each(attr_names, configuration) do |record, attr_name, value|
        if value.nil? or value.blank?
          unless configuration[:allow_nil] || configuration[:allow_blank]
            record.errors.add(attr_name, "cannot be blank!")
          end
        else
          hours = value.slice(0,2).to_i
          mins = value.slice(-2.2).to_i
          unless value =~ /^\d{2}:\d{2}$/
            record.errors.add(attr_name, "must be entered as HH:MM!")
          end
          unless hours >= 0 && hours < 24
            record.errors.add(attr_name, "hour must be from 00 to 23!")
          end
          unless mins >= 0 && mins < 60
            record.errors.add(attr_name, "minute must be from 00 to 59!")
          end
        end
      end
    end

    # validates that a person is valid
    def validates_person(*attr_names)
      configuration = {}
      configuration.update(attr_names.extract_options!)
      validates_each(attr_names, configuration) do |record, attr_name, value|
        if value.nil? or value.blank?
          unless configuration[:allow_nil] || configuration[:allow_blank]
            record.errors.add(attr_name, "is required!")
          end
        else
          unless Person.find_by_id(value)
            record.errors.add(attr_name, "is not a valid Person ID!")
          end
        end
      end
    end
    
    # validates that a jail id is valid
    def validates_jail(*attr_names)
      configuration = {}
      configuration.update(attr_names.extract_options!)
      validates_each(attr_names, configuration) do |record, attr_name, value|
        if value.nil? or value.blank?
          unless configuration[:allow_nil] || configuration[:allow_blank]
            record.errors.add(attr_name, 'is required!')
          end
        else
          unless Jail.find_by_id(value)
            record.errors.add(attr_name, 'is not a valid Jail ID!')
          end
        end
      end
    end

    # validates that a booking is valid
    def validates_booking(*attr_names)
      configuration = {}
      configuration.update(attr_names.extract_options!)
      validates_each(attr_names, configuration) do |record, attr_name, value|
        if value.nil? or value.blank?
          unless configuration[:allow_nil] || configuration[:allow_blank]
            record.errors.add(attr_name, "is required!")
          end
        else
          unless Booking.find_by_id(value)
            record.errors.add(attr_name, "is not a valid Booking ID!")
          end
        end
      end
    end

    # validates that a arrest is valid
    def validates_arrest(*attr_names)
      configuration = {}
      configuration.update(attr_names.extract_options!)
      validates_each(attr_names, configuration) do |record, attr_name, value|
        if value.nil? or value.blank?
          unless configuration[:allow_nil] || configuration[:allow_blank]
            record.errors.add(attr_name, "is required!")
          end
        else
          unless Arrest.find_by_id(value)
            record.errors.add(attr_name, "is not a valid Arrest ID!")
          end
        end
      end
    end

    # validates that a call is valid
    def validates_call(*attr_names)
      configuration = {}
      configuration.update(attr_names.extract_options!)
      validates_each(attr_names, configuration) do |record, attr_name, value|
        if value.nil? or value.blank?
          unless configuration[:allow_nil] || configuration[:allow_blank]
            record.errors.add(attr_name, "is required!")
          end
        else
          unless Call.find_by_id(value)
            record.errors.add(attr_name, "is not a valid Call ID!")
          end
        end
      end
    end

    # validates that a probation is valid
    def validates_probation(*attr_names)
      configuration = {}
      configuration.update(attr_names.extract_options!)
      validates_each(attr_names, configuration) do |record, attr_name, value|
        if value.nil? or value.blank?
          unless configuration[:allow_nil] || configuration[:allow_blank]
            record.errors.add(attr_name, "is required!")
          end
        else
          unless Probation.find_by_id(value)
            record.errors.add(attr_name, "is not a valid Probation ID!")
          end
        end
      end
    end

    # validates that an offense is valid
    def validates_offense(*attr_names)
      configuration = {}
      configuration.update(attr_names.extract_options!)
      validates_each(attr_names, configuration) do |record, attr_name, value|
        if value.nil? or value.blank?
          unless configuration[:allow_nil] || configuration[:allow_blank]
            record.errors.add(attr_name, "is required!")
          end
        else
          unless Offense.find_by_id(value)
            record.errors.add(attr_name, "is not a valid Offense ID!")
          end
        end
      end
    end

    # validates that a warrant is valid
    def validates_warrant(*attr_names)
      configuration = {}
      configuration.update(attr_names.extract_options!)
      validates_each(attr_names, configuration) do |record, attr_name, value|
        if value.nil? or value.blank?
          unless configuration[:allow_nil] || configuration[:allow_blank]
            record.errors.add(attr_name, "is required!")
          end
        else
          unless Warrant.find_by_id(value)
            record.errors.add(attr_name, "is not a valid Warrant ID!")
          end
        end
      end
    end

    # validates that a docket is valid
    def validates_docket(*attr_names)
      configuration = {}
      configuration.update(attr_names.extract_options!)
      validates_each(attr_names, configuration) do |record, attr_name, value|
        if value.nil? or value.blank?
          unless configuration[:allow_nil] || configuration[:allow_blank]
            record.errors.add(attr_name, "is required!")
          end
        else
          unless Docket.find_by_id(value)
            record.errors.add(attr_name, "is not a valid Docket ID!")
          end
        end
      end
    end

    # validates that a contact is valie
    def validates_contact(*attr_names)
      configuration = {}
      configuration.update(attr_names.extract_options!)
      validates_each(attr_names, configuration) do |record, attr_name, value|
        if value.nil? or value.blank?
          unless configuration[:allow_nil] || configuration[:allow_blank]
            record.errors.add(attr_name, "is required!")
          end
        else
          unless Contact.find_by_id(value)
            record.errors.add(attr_name, "is not a valid Contact ID!")
          end
        end
      end
    end

    # validates that a user is valid
    def validates_user(*attr_names)
      configuration = {allow_locked: false}
      configuration.update(attr_names.extract_options!)
      validates_each(attr_names, configuration) do |record, attr_name, value|
        if value.nil? or value.blank?
          unless configuration[:allow_nil] || configuration[:allow_blank]
            record.errors.add(attr_name, "is required!")
          end
        else
          if u = User.find_by_id(value)
            unless configuration[:allow_locked]
              if u.locked? || u.disabled_text?
                record.errors.add(attr_name, "must not be a locked or disabled user account!")
              end
            end
          else
            record.errors.add(attr_name, "is not a valid Person ID!")
          end
        end
      end
    end

    # validates that a url is in the proper format
    def validates_url(*attr_names)
      configuration = {
        message: "must be valid URL format (http://site.host.domain/path/)",
        with: /\Ahttps?:\/\/([-\w\.]+)+(:\d+)?(\/([\w\/_\.]*(\?\S+)?)?)?\Z/
      }
      configuration.update(attr_names.extract_options!)
      validates_format_of attr_names, configuration
    end

    # validates that zip codes are in the proper 5 digit form
    def validates_zip(*attr_names)
      configuration = {
        message: "must be ZIP or ZIP+4 format",
        with: /\A[\d]{5}(-[\d]{4})?\Z/
      }
      configuration.update(attr_names.extract_options!)
      validates_format_of attr_names, configuration
    end

    # validates that social security numbers are of the proper format.
    def validates_ssn(*attr_names)
      configuration = {
        message: "must be of format 999-99-9999",
        with: /\A[\d]{3}-[\d]{2}-[\d]{4}\Z/
      }
      configuration.update(attr_names.extract_options!)
      validates_format_of attr_names, configuration 
    end

    # validates that phone numbers are of the proper format
    def validates_phone(*attr_names)
      configuration = {
        with: /\A[\d]{3}-[\d]{3}-[\d]{4}\Z/,
        message: "must be of format 999-999-9999"
      }
      configuration.update(attr_names.extract_options!)
      validates_format_of attr_names, configuration
    end
  end
end