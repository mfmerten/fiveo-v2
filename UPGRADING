Upgrading From Version 1.x.x to 2.0.0

Please read the following procedure completely and consider the warnings and
notices before attempting to upgrade FiveO installations from Version 1.x (V1)
to Version 2.0 (V2).

WARNING: Before attempting any of the following, you *MUST* obtain a backup
dump of your data.  Do this with something like:
    mysqldump -opt fiveo > fiveo_backup.dump
You must also create a backup of your external data files located in either
'/srv/rails/fiveo/shared/system' or '/usr/local/rails/fiveo/shared/system'.
These will be required should the worst case happen and you have to rebuild
the system from scratch.  If possible you should include a complete OS backup.

PART 1: Upgrading the Operating System (OS)

V2 of FiveO makes use of OS features that are not available in older versions
of Ubuntu Server, so upgrading to the latest version is required. The current
version of Ubuntu Server at the time of this writing is 12.04 (code named
'Precise Pangolin' or simply 'Precise').

WARNING: Before attempting to upgrade any version of Ubuntu Server, please
visit https://help.ubuntu.com/community/UpgradeNotes for more information.
You Have Been Warned!

OS upgrades must be made one version at a time.  For instance, if you are
running version 11.04 (Natty), you must first upgrade to 11.10 (Oneiric)
and then to 12.04 (Precise).  For each upgrade step, you *MUST* review the
release notes for the version you are upgrading to, otherwise you risk a
broken installation and will likely have to rebuild the server from scratch.

The instructions presented here assume that you are running Ubuntu Server
version 10.10 (Maverick) or higher. Older versions will require a different
procedure.

NOTE: the following assumes you are the root user. Obtain a root shell with:
          sudo -s
      and enter the fiveoadm password. Remain in the root shell for the
      remainder of the upgrade process unless instructed differently.

1. Find out which version of Ubuntu Server you are running:
       lsb_release -a

2. Update the package list for your current OS version:
       apt-get update

3. Update installed packages to their latest versions for your current OS
   version:
       apt-get upgrade

4. Install the 'update-manager-core' package if it is not already installed:
       apt-get install update-manager-core

5. Edit the '/etc/update-manager/release-upgrades' file and
   set 'Prompt=normal'.

6. Launch the upgrade tool:
       do-release-upgrade

7. Follow the on-screen instructions.
   NOTE: you will be asked during the upgrade process about overwriting
         specific configuration files that were modified after installation.
         In most cases you will NOT install the new version of the files.
         However, in the case of 'services', allow the installation process
         to install the new version of the file.
   NOTE: you will be asked at the end of this process whether to remove
         obsolete packages... select yes
   NOTE: you may be asked to change the MySQL password for the 'root' account,
         simply press enter on a blank line to retain the old password.  If
         you actually change the MySQL root account password, you will no
         longer be able to access MySQL from the command line until you update
         the '/home/fiveoadm/.my.cnf' file with the new password.

8: If a new version of '/etc/services' was installed during the upgrade,
   edit the new '/etc/services' file and add the following line to the
   end of the file:
       stsmtp        5000/tcp       # outgoing smtps via tunnel
       
9. Repeat steps 1-8 for each OS version until the system is running 12.04

You will likely not be able to get the application to run at this point
because of missing libraries. That will all be fixed in Part 2.

PART 2: Upgrading Support Applications

These steps will modify the OS and other applications to properly support
FiveO V2. This part requires root shell (sudo -s).

A. Upgrading Ruby

   1. Remove all currently installed Ruby gems by first getting a list of
      installed gems with:
          gem list
      and then uninstalling them with:
          gem uninstall <gem name>
      Repeat this until 'gem list' shows no gems installed.
      NOTE: some gems will complain about dependencies when you try to remove
            them. This is normal and you should select to remove them anyway.
      NOTE: some gems will have multiple versions installed, you should
            select to remove all versions.
      NOTE: some gems will ask whether to remove binary files too, you should
            select yes.
   
   2. Uninstall the rubygems package:
          apt-get remove --purge rubygems
   
   3. Uninstall the Ruby package:
          apt-get remove --purge ruby1.8
   
   4. Uninstall any automatically installed packages that are no longer
      needed with:
          apt-get autoremove
          apt-get autoclean
   
   5. Install Ruby 1.9.3
          apt-get install ruby1.9.1 ruby1.9.1-dev ri1.9.1 rubygems
   
   6. Install bundler gem:
          gem install bundler
   
   7. Install passenger gem:
          gem install passenger
   
   8. Install the passenger module for Apache:
          passenger-install-apache2-module
      NOTE: follow the on-screen instructions exactly. At some point you will
            be instructed to update the Apache configuration file with three
            lines of text.  To do this, edit the
            '/etc/apache2/conf.d/passenger' file and make the recommended
            changes.
      NOTE: you will be instructed to restart the Apache server. Wait until
            the next step to do this.
    9. Modify the apache site file (/etc/apache2/sites-available/default or
            whichever file was originally used to configure fiveo) by removing
            all rewrite rules and conditions. Save the file and restart apache
            with:
                service apache2 restart
            
B. Add Additional Packages
   
   1. Install the 'node.js' javascript interpreter required by V2:
          apt-get install nodejs nodejs-dev

C. Miscellaneous

   1. Fix SSL cert used by postfix so that Ruby 1.9 SSL library will deliver
      mail properly (Ruby 1.8 didn't enforce hostname matches, but 1.9 does):
      
      Change to the '/usr/share/ssl-cert' directory and edit the 'ssleay.cnf'
      file.  On the last line of the file you will find:
          commonName                   = @HostName@
      Obtain a root shell (sudo -s) and change it to:
          commonName                   = localhost
      Save the file.
      Execute the command:
          make-ssl-cert generate-default-snakeoil --force-overwrite
      When it is finished, mail will work properly.

PART 3: Preparing For Deployment

The procedure detailed below will prepare the server for the initial
deployment of FiveO V2. The steps detailed here require the user to be
'fiveoadm' (NOT root!).  Those steps requiring root access will explicitly
instruct you to 'sudo -s' or will explicitly specify 'sudo' in the command
shown.

A. Recompile Sphinx to work with new Apache

   1. Download Sphinx version 0.9.9 or locate the version stored in the
      'doc/sources' subdirectory of the FiveO installation and extract it into
      the fiveoadm home directory.  You might even find that sphinx-0.9.9
      is already extracted in the fiveoadm home directory.
      
   2. Change to the sphinx-0.9.9 subdirectory and issue the following to
      rebuild it:
          make clean
          ./configure
          make
          sudo make install

B. Prepare the fiveo directory for deployment

   1. Locate the fiveo directory on your server.  On older servers, it might
      be at '/usr/local/rails/fiveo' or at '/srv/rails/fiveo'.  Newer servers
      generally always use the '/srv/rails/fiveo' path.  Make a note of the
      actual path on your server.  The remainder of these steps will simply
      refer to it as <fiveo path>.
      
   2. Change to the '<fiveo path>/shared' directory and remove the
      'cached-copy' subdirectory with:
          rm -rf cached-copy
      This will cause the initial deployment to rebuild the cached copy of
      FiveO from the correct deployment branch.
      
C. Attempt Deployment

   This procedure is a work in progress and may not cover all of the problems
   you may encounter during deployment.  If you encounter problems during
   remote deployment of V2 to the server, you need to solve the issues and
   make note of them in this document.

CONCLUSION

   After the upgrade has been successfully completed, there will still be
   issues with the new installation.  Current issues include:
   
   * Because Rails 3 uses Bundler to handle gems, the rake command that is
     normally used to maintain the installation will need to be executed as
         bundle exec rake
     and this can cause problems.  I need to work out how to modify the
     deployment so that the 'bundle install' command includes the --binstubs
     flag. This will create a bin/ directory in the fiveo application root
     and install all of the gem executables into it.  This will allow running
     rake, etc as:
         bin/rake
     Then I can modify the .profile/.bash_profile/.bashrc files to include
     that as the first component of PATH making it possible once again to
     simply do:
         rake

   There will be many more issues and they will be noted here as I find them.
   
Michael Merten
mikemerten@suddenlink.net

