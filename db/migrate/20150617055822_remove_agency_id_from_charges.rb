class RemoveAgencyIdFromCharges < ActiveRecord::Migration
  def up
    remove_column :charges, :agency_id
  end
  
  def down
    add_column :charges, :agency_id, :integer
    add_index :charges, :agency_id
  end
end
