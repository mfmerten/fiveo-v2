class FixLocationRemarks < ActiveRecord::Migration
  def up
    change_column :evidence_locations, :remarks, :string, :null => false, :default => ''
  end

  def down
  end
end
