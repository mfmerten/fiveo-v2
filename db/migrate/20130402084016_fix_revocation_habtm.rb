class FixRevocationHabtm < ActiveRecord::Migration
  class Docket < ActiveRecord::Base
  end
  class Revocation < ActiveRecord::Base
    has_and_belongs_to_many :consecutive_dockets, :class_name => 'FixRevocationHabtm::Docket', :join_table => 'dockets_revocations'
    has_many :revocation_consecutives, :class_name => 'FixRevocationHabtm::RevocationConsecutive'
    has_many :xconsecutive_dockets, :through => :revocation_consecutives, :source => :docket
  end
  class Sentence < ActiveRecord::Base
    has_and_belongs_to_many :consecutive_dockets, :class_name => 'FixRevocationHabtm::Docket', :join_table => 'dockets_sentences'
    has_many :sentence_consecutives, :class_name => 'FixRevocationHabtm::SentenceConsecutive'
    has_many :xconsecutive_dockets, :through => :sentence_consecutives, :source => :docket
  end
  class RevocationConsecutive < ActiveRecord::Base
    belongs_to :docket, :class_name => 'FixRevocationHabtm::Docket'
    belongs_to :revocation, :class_name => 'FixRevocationHabtm::Revocation'
  end
  class SentenceConsecutive < ActiveRecord::Base
    belongs_to :docket, :class_name => 'FixRevocationHabtm::Docket'
    belongs_to :sentence, :class_name => 'FixRevocationHabtm::Sentence'
  end
  
  def up
    create_table :revocation_consecutives do |t|
      t.integer :docket_id
      t.integer :revocation_id
      t.timestamps
    end
    add_index :revocation_consecutives, :docket_id
    add_index :revocation_consecutives, :revocation_id
    
    create_table :sentence_consecutives do |t|
      t.integer :docket_id
      t.integer :sentence_id
      t.timestamps
    end
    add_index :sentence_consecutives, :docket_id
    add_index :sentence_consecutives, :sentence_id
    
    FixRevocationHabtm::RevocationConsecutive.reset_column_information
    FixRevocationHabtm::SentenceConsecutive.reset_column_information
    
    FixRevocationHabtm::Revocation.all.each do |r|
      r.xconsecutive_docket_ids = r.consecutive_docket_ids
    end
    FixRevocationHabtm::Sentence.all.each do |s|
      s.xconsecutive_docket_ids = s.consecutive_docket_ids
    end
    
    drop_table :dockets_revocations
    drop_table :dockets_sentences
  end

  def down
    create_table :dockets_revocations, :id => false do |t|
      t.integer :docket_id
      t.integer :revocation_id
    end
    add_index :dockets_revocations, :docket_id
    add_index :dockets_revocations, :revocation_id
    
    create_table :dockets_sentences, :id => false do |t|
      t.integer :docket_id
      t.integer :sentence_id
    end
    add_index :dockets_sentences, :docket_id
    add_index :dockets_sentences, :sentence_id
    
    FixRevocationHabtm::Revocation.all.each do |r|
      r.consecutive_docket_ids = r.xconsecutive_docket_ids
    end
    FixRevocationHabtm::Sentence.all.each do |s|
      s.consecutive_docket_ids = s.xconsecutive_docket_ids
    end
    
    drop_table :revocation_consecutives
    drop_table :sentence_consecutives
  end
end
