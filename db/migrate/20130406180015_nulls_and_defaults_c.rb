class NullsAndDefaultsC < ActiveRecord::Migration
  class Call < ActiveRecord::Base
  end
  class CallUnit < ActiveRecord::Base
  end
  class CallSubject < ActiveRecord::Base
  end
  class CaseNote < ActiveRecord::Base
  end
  class Charge < ActiveRecord::Base
  end
  class Citation < ActiveRecord::Base
  end
  class Commissary < ActiveRecord::Base
  end
  class CommissaryItem < ActiveRecord::Base
  end
  class Contact < ActiveRecord::Base
  end
  class ContactBondsmanCompany < ActiveRecord::Base
  end
  class Court < ActiveRecord::Base
  end
  class CourtCost < ActiveRecord::Base
  end
  class CourtType < ActiveRecord::Base
  end
  
  def up
    [:case_no, :received_by, :received_by_unit, :received_by_badge, :dispatcher,
      :dispatcher_unit, :dispatcher_badge, :detective, :detective_unit, :detective_badge,
      :received_via, :disposition, :signal_code, :signal_name].each do |col|
      NullsAndDefaultsC::Call.update_all("#{col} = ''","#{col} IS NULL")
      change_column :calls, col, :string, :null => false, :default => ''
    end
    [:details, :remarks].each do |col|
      NullsAndDefaultsC::Call.update_all("#{col} = ''", "#{col} IS NULL")
      change_column :calls, col, :text, :null => false, :default => ''
    end
    NullsAndDefaultsC::Call.update_all("legacy = FALSE","legacy IS UNKNOWN")
    change_column :calls, :legacy, :boolean, :null => false, :default => false
    
    NullsAndDefaultsC::CallUnit.delete_all('call_id IS NULL')
    change_column :call_units, :call_id, :integer, :null => false
    [:officer, :officer_unit, :officer_badge].each do |col|
      NullsAndDefaultsC::CallUnit.update_all("#{col} = ''", "#{col} IS NULL")
      change_column :call_units, col, :string, :null => false, :default => ''
    end
    
    NullsAndDefaultsC::CallSubject.delete_all('call_id IS NULL')
    change_column :call_subjects, :call_id, :integer, :null => false
    [:name, :address1, :address2, :home_phone, :cell_phone, :work_phone, :subject_type].each do |col|
      NullsAndDefaultsC::CallSubject.update_all("#{col} = ''", "#{col} IS NULL")
      change_column :call_subjects, col, :string, :null => false, :default => ''
    end
    
    NullsAndDefaultsC::CaseNote.delete_all('case_no IS NULL')
    change_column :case_notes, :case_no, :string, :null => false, :default => ''
    NullsAndDefaultsC::CaseNote.update_all("note = ''","note IS NULL")
    change_column :case_notes, :note, :text, :null => false, :default => ''
    
    [:charge, :agency, :charge_type].each do |col|
      NullsAndDefaultsC::Charge.update_all("#{col} = ''", "#{col} IS NULL")
      change_column :charges, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsC::Charge.update_all("count = 1","count IS NULL")
    change_column :charges, :count, :integer, :null => false, :default => 1
    
    NullsAndDefaultsC::Citation.delete_all('person_id IS NULL')
    change_column :citations, :person_id, :integer, :null => false
    [:officer, :officer_unit, :officer_badge, :ticket_no].each do |col|
      NullsAndDefaultsC::Citation.update_all("#{col} = ''", "#{col} IS NULL")
      change_column :citations, col, :string, :null => false, :default => ''
    end
    
    NullsAndDefaultsC::Commissary.delete_all('person_id IS NULL OR jail_id IS NULL')
    change_column :commissaries, :person_id, :integer, :null => false
    change_column :commissaries, :jail_id, :integer, :null => false
    [:officer, :officer_unit, :officer_badge, :receipt_no, :memo, :category].each do |col|
      NullsAndDefaultsC::Commissary.update_all("#{col} = ''", "#{col} IS NULL")
      change_column :commissaries, col, :string, :null => false, :default => ''
    end
    
    NullsAndDefaultsC::CommissaryItem.delete_all('jail_id IS NULL')
    change_column :commissary_items, :jail_id, :integer, :null => false
    NullsAndDefaultsC::CommissaryItem.update_all("description = ''", "description IS NULL")
    change_column :commissary_items, :description, :string, :null => false, :default => ''
    NullsAndDefaultsC::CommissaryItem.update_all("active = FALSE","active IS UNKNOWN")
    change_column :commissary_items, :active, :boolean, :null => false, :default => true
    
    [:title, :unit, :street, :city, :zip, :pri_email, :alt_email, :web_site,
      :notes, :street2, :badge_no, :fullname, :state, :phone1, :phone2, :phone3,
      :phone1_type, :phone2_type, :phone3_type].each do |col|
      NullsAndDefaultsC::Contact.update_all("#{col} = ''","#{col} IS NULL")
      change_column :contacts, col, :string, :null => false, :default => ''
    end
    [:officer, :active, :detective, :physician, :pharmacy, :legacy, :business,
      :phone1_unlisted, :phone2_unlisted, :phone3_unlisted, :bondsman, :judge,
      :pawn_co, :bonding_company].each do |col|
      NullsAndDefaultsC::Contact.update_all("#{col} = FALSE", "#{col} IS UNKNOWN")
      change_column :contacts, col, :boolean, :null => false, :default => false
    end
    NullsAndDefaultsC::Contact.update_all("printable = FALSE", "printable IS UNKNOWN")
    change_column :contacts, :printable, :boolean, :null => false, :default => true
    NullsAndDefaultsC::Contact.update_all("remarks = ''","remarks IS NULL")
    change_column :contacts, :remarks, :text, :null => false, :default => ''
  
    NullsAndDefaultsC::ContactBondsmanCompany.delete_all('contact_id IS NULL OR (bondsman_id IS NULL AND company_id IS NULL)')
    change_column :contact_bondsman_companies, :contact_id, :integer, :null => false
    
    NullsAndDefaultsC::Court.delete_all('docket_id IS NULL')
    change_column :courts, :docket_id, :integer, :null => false
    [:bench_warrant_issued, :bond_forfeiture_issued, :is_upset_wo_refix, :is_refixed,
      :is_dismissed, :is_overturned, :psi, :processed, :fines_paid, :writ_of_attach,
      :not_present].each do |col|
      NullsAndDefaultsC::Court.update_all("#{col} = FALSE","#{col} IS UNKNOWN")
      change_column :courts, col, :boolean, :null => false, :default => false
    end
    [:notes, :remarks].each do |col|
      NullsAndDefaultsC::Court.update_all("#{col} = ''", "#{col} IS NULL")
      change_column :courts, col, :text, :null => false, :default => ''
    end
    [:refix_reason, :court_type, :next_court_type, :docket_type, :next_docket_type,
      :plea_type, :judge, :next_judge].each do |col|
      NullsAndDefaultsC::Court.update_all("#{col} = ''", "#{col} IS NULL")
      change_column :courts, col, :string, :null => false, :default => ''
    end
    
    NullsAndDefaultsC::CourtCost.delete_all("name IS NULL")
    change_column :court_costs, :name, :string, :null => false, :default => ''
    NullsAndDefaultsC::CourtCost.update_all("category = ''", "category IS NULL")
    change_column :court_costs, :category, :string, :null => false, :default => ''
        
    NullsAndDefaultsC::CourtType.delete_all("name IS NULL")
    change_column :court_types, :name, :string, :null => false, :default => ''
  end

  def down
  end
end
