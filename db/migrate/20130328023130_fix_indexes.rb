class FixIndexes < ActiveRecord::Migration
  def up
    add_index :arrests, :case_no
    add_index :arrest_officers, :officer_id
    add_index :bonds, :bonding_company_id
    remove_column :bookings, :facility_id
    remove_column :bookings, :old_cell_id
    add_index :bookings, :jail_id
    add_index :call_units, :officer_id
    remove_index :calls, :dispatcher
    remove_index :calls, :dispatcher_unit
    remove_index :calls, :received_by
    remove_index :calls, :received_by_unit
    add_index :calls, :signal_code
    add_index :charges, :agency_id
    remove_index :citations, :officer
    remove_index :citations, :officer_unit
    remove_index :citations, :ticket_no
    remove_index :commissary_items, :description
    remove_index :contacts, :alt_email
    remove_index :contacts, :city
    remove_index :contacts, :notes
    remove_index :contacts, :pri_email
    remove_index :contacts, :street
    remove_index :contacts, :street2
    remove_index :contacts, :title
    remove_index :contacts, :unit
    remove_index :courts, :court_date
    remove_index :dispositions, :status
    remove_index :events, :start_date
    remove_index :events, :end_date
    add_index :events, :investigation_id
    add_index :events, :owned_by
    add_index :evidence_locations, :officer_id
    remove_index :fax_covers, :name
    remove_index :forbids, :complainant
    remove_index :forbids, :forbid_first
    remove_index :forbids, :forbid_last
    remove_index :forbids, :receiving_officer
    remove_index :forbids, :receiving_officer_unit
    remove_index :forbids, :serving_officer
    remove_index :forbids, :serving_officer_unit
    add_index :forums, :created_by
    add_index :forums, :updated_by
    remove_index :groups, :name
    add_index :invest_cases, :call_id
    add_index :invest_criminals, :created_by
    add_index :invest_criminals, :updated_by
    add_index :invest_notes, :invest_record_id
    add_index :invest_notes, :invest_record_type
    remove_index :invest_photos, :private
    add_index :invest_photos, :taken_by_id
    remove_index :invest_reports, :private
    remove_index :invest_vehicles, :private
    remove_index :investigations, :private
    remove_index :jails, :permset
    remove_index :medication_schedules, :name => :index_med_schedules_on_time
    remove_index :medications, :prescription_no
    remove_index :offenses, :location
    remove_index :offenses, :officer
    remove_index :offenses, :officer_unit
    remove_index :pawn_items, :model_no
    remove_index :pawn_items, :serial_no
    remove_index :pawns, :address1
    remove_index :pawns, :address2
    remove_index :pawns, :full_name
    remove_index :pawns, :oln
    remove_index :pawns, :ssn
    remove_index :pawns, :ticket_date
    remove_index :pawns, :ticket_no
    remove_index :people, :firstname
    remove_index :people, :middlename
    remove_index :people, :lastname
    remove_index :people, :suffix
    add_index :people, :is_alias_for
    add_index :signal_codes, :code
    remove_index :stolens, :item
    remove_index :stolens, :model_no
    remove_index :stolens, :serial_no
    remove_index :stolens, :stolen_date
    remove_index :visitors, :sign_in_time
    remove_index :visitors, :visit_date
    remove_index :warrants, :dispo_date
    remove_index :warrants, :dob
    remove_index :warrants, :firstname
    remove_index :warrants, :lastname
    remove_index :warrants, :zip
  end

  def down
  end
end
