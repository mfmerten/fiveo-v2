class RemoveCaseNoFromOffense < ActiveRecord::Migration
  def up
    remove_column :offenses, :case_no
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
