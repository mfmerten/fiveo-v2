class AddProgramOnlyToOptions < ActiveRecord::Migration
  class Option < ActiveRecord::Base
    belongs_to :option_type, :class_name => 'AddProgramOnlyToOptions::OptionType'
  end
  class OptionType < ActiveRecord::Base
    has_many :options, :class_name => 'AddProgramOnlyToOptions::Option', :dependent => :destroy
  end
  
  def self.up
    unless column_exists?(:options, :program_only)
      add_column :options, :program_only, :boolean, :default => false
    
      AddProgramOnlyToOptions::Option.reset_column_information
    
      if ot = AddProgramOnlyToOptions::OptionType.find_by_name("Hold Type")
        ot.options.each do |o|
          if o.short_name != "manual"
            o.program_only = true
          end
          o.short_name = nil
          o.save
        end
      end
    end
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
