class RemoveNodeleteFlag < ActiveRecord::Migration
  def up
    remove_column :paper_payments, :trans_not_deletable
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
