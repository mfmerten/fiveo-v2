class RemoveLegacyVisitors < ActiveRecord::Migration
  class Visitor < ActiveRecord::Base
  end
  
  def up
    RemoveLegacyVisitors::Visitor.delete_all('legacy IS TRUE')
  end

  def down
  end
end
