class AddConf < ActiveRecord::Migration
  class SiteConfiguration < ActiveRecord::Base
  end
  
  class SiteStore < ActiveRecord::Base
    serialize :setting_value
  end
  
  def up
    create_table :site_stores do |t|
      t.string :setting_name, :null => false
      t.text :setting_value, :null => false
      t.timestamps
    end
    add_index :site_stores, :setting_name
  
    AddConf::SiteStore.reset_column_information
    conf = AddConf::SiteConfiguration.last.attributes
    settings = conf.keys
    settings.delete('id')
    settings.delete('created_at')
    settings.delete('updated_at')
    settings.each do |s|
      AddConf::SiteStore.create(:setting_name => s, :setting_value => conf[s])
    end
  end

  def down
    drop_table :site_stores
  end
end
