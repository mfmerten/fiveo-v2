class RenameArrestCounterCache < ActiveRecord::Migration
  def change
    rename_column :arrests, :officers_count, :arrest_officers_count
  end
end
