class RemoveNullConstraintFromDocHoldId < ActiveRecord::Migration
  def change
    change_column_null :docs, :hold_id, true
  end
end
