class ConvertSignalCodes < ActiveRecord::Migration
  class Option < ActiveRecord::Base
    OptionTypes = {
      30 => 'Judge',
      32 => 'Pawn Company',
      33 => 'Pawn Ticket Type',
      34 => 'Pawn Type',
      38 => 'Probation Status',
      39 => 'Probation Type',
      41 => 'Release Reason',
      44 => 'Signal Code',
      46 => 'Subject Type',
      48 => 'Transfer Type',
      49 => 'Trial Result',
      50 => 'Vehicle Type',
      51 => 'Warrant Disposition'
    }.freeze
  end
  
  class SignalCode < ActiveRecord::Base
  end
  
  class Call < ActiveRecord::Base
    belongs_to :old_signal_code, :class_name => "ConvertSignalCodes::Option", :foreign_key => 'signal_code_id'
  end
  
  def up
    create_table :signal_codes, :force => true do |t|
      t.string :code
      t.string :name
      t.integer :created_by, :default => 1
      t.integer :updated_by, :default => 1
      t.timestamps
    end
    add_index :signal_codes, :created_by
    add_index :signal_codes, :updated_by
    
    add_column :calls, :signal_code, :string
    add_column :calls, :signal_name, :string
    
    ConvertSignalCodes::SignalCode.reset_column_information
    ConvertSignalCodes::Call.reset_column_information
    
    ConvertSignalCodes::Option.where(:option_type => 44).all.each do |o|
      unless o.disabled?
        ConvertSignalCodes::SignalCode.create(:code => o.short_name, :name => o.long_name)
      end
    end
    
    ConvertSignalCodes::Call.all.each do |c|
      unless c.old_signal_code.nil?
        c.signal_code = c.old_signal_code.short_name
        c.signal_name = c.old_signal_code.long_name
        c.save
      end
    end
    
    remove_column :calls, :signal_code_id
    
    ConvertSignalCodes::Option.where(:option_type => 44).all.each{|o| o.destroy}
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
