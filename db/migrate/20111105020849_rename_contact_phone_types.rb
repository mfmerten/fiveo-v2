class RenameContactPhoneTypes < ActiveRecord::Migration
  def up
    rename_column :contacts, :phone1_type, :phone1_type_id
    rename_column :contacts, :phone2_type, :phone2_type_id
    rename_column :contacts, :phone3_type, :phone3_type_id
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
