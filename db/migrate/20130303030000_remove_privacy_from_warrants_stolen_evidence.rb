class RemovePrivacyFromWarrantsStolenEvidence < ActiveRecord::Migration
  def up
    remove_column :warrants, :private
    remove_column :warrants, :owned_by
    remove_column :evidences, :private
    remove_column :evidences, :owned_by
    remove_column :stolens, :private
    remove_column :stolens, :owned_by
  end

  def down
    add_column :warrants, :private, :boolean
    add_column :warrants, :owned_by, :integer
    add_column :evidences, :private, :boolean
    add_column :evidences, :owned_by, :integer
    add_column :stolens, :private, :boolean
    add_column :stolens, :owned_by, :integer
  end
end
