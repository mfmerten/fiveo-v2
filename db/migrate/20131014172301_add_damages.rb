class AddDamages < ActiveRecord::Migration
  def up
    create_table :damages, :force => true do |t|
      t.integer :person_id
      t.string :case_no, :default => "", :null => false
      t.string :victim_name, :default => "", :null => false
      t.string :victim_address, :default => "", :null => false
      t.string :victim_phone, :default => "", :null => false
      t.string :crimes, :default => "", :null => false
      t.datetime :damage_datetime
      t.integer :created_by, :default => 1
      t.integer :updated_by, :default => 1
      t.timestamps
    end
    add_index :damages, :person_id
    add_index :damages, :created_by
    add_index :damages, :updated_by
    
    add_foreign_key :damages, :people, :dependent => :nullify
    add_foreign_key :damages, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :damages, :users, :column => 'updated_by', :dependent => :nullify
    
    create_table :damage_items, :force => true do |t|
      t.integer :damage_id, :null => false
      t.string :description, :default => "", :null => false
      t.decimal :estimate, :precision => 12, :scale => 2, :default => 0.0, :null => false
      t.timestamps
    end
    add_index :damage_items, :damage_id
    
    add_foreign_key :damage_items, :damages
  end

  def down
    drop_table :damage_items
    drop_table :damages
  end
end
