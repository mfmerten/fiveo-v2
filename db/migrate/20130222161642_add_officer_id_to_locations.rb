class AddOfficerIdToLocations < ActiveRecord::Migration
  def change
    add_column :evidence_locations, :officer_id, :integer
  end
end
