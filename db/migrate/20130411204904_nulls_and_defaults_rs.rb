class NullsAndDefaultsRs < ActiveRecord::Migration
  class Revocation < ActiveRecord::Base
  end
  class RevocationConsecutive < ActiveRecord::Base
  end
  class Sentence < ActiveRecord::Base
  end
  class SentenceConsecutive < ActiveRecord::Base
  end
  class SignalCode < ActiveRecord::Base
  end
  class Statute < ActiveRecord::Base
  end
  class Stolen < ActiveRecord::Base
  end
  class SupportTicket < ActiveRecord::Base
  end
  class SupportTicketComment < ActiveRecord::Base
  end
  class SupportTicketWatcher < ActiveRecord::Base
  end
  
  def up
    NullsAndDefaultsRs::Revocation.delete_all("docket_id IS NULL")
    change_column_null :revocations, :docket_id, false
    [:process_status, :consecutive_type, :judge].each do |col|
      NullsAndDefaultsRs::Revocation.update_all("#{col} = ''","#{col} IS NULL")
      change_column :revocations, col, :string, :null => false, :default => ''
    end
    [:revoked, :time_served, :doc, :processed, :delay_execution, :deny_goodtime, :total_death].each do |col|
      NullsAndDefaultsRs::Revocation.update_all("#{col} = FALSE","#{col} IS UNKNOWN")
      change_column_null :revocations, col, false
    end
    [:total_days, :total_months, :total_years, :parish_days, :parish_months,
      :parish_years, :total_hours, :parish_hours, :total_life].each do |col|
      NullsAndDefaultsRs::Revocation.update_all("#{col} = 0","#{col} IS NULL")
      change_column_null :revocations, col, false
    end
    NullsAndDefaultsRs::Revocation.update_all("remarks = ''","remarks IS NULL")
    change_column :revocations, :remarks, :text, :null => false, :default => ''
    
    NullsAndDefaultsRs::RevocationConsecutive.delete_all("docket_id IS NULL OR revocation_id IS NULL")
    change_column_null :revocation_consecutives, :docket_id, false
    change_column_null :revocation_consecutives, :revocation_id, false
  
    NullsAndDefaultsRs::Sentence.delete_all("docket_id IS NULL OR court_id IS NULL OR da_charge_id IS NULL")
    change_column_null :sentences, :docket_id, false
    change_column_null :sentences, :court_id, false
    change_column_null :sentences, :da_charge_id, false
    [:probation_revert_conditions, :sentence_notes, :fines_notes, :costs_notes,
      :conviction, :process_status, :consecutive_type, :probation_type, :result].each do |col|
      NullsAndDefaultsRs::Sentence.update_all("#{col} = ''","#{col} IS NULL")
      change_column :sentences, col, :string, :null => false, :default => ''
    end
    [:notes, :remarks].each do |col|
      NullsAndDefaultsRs::Sentence.update_all("#{col} = ''","#{col} IS NULL")
      change_column :sentences, col, :text, :null => false, :default => ''
    end
    [:fines, :costs, :probation_fees, :idf_amount, :dare_amount, :restitution_amount].each do |col|
      NullsAndDefaultsRs::Sentence.update_all("#{col} = 0.0","#{col} IS NULL")
      change_column_null :sentences, col, false
    end
    change_column_default :sentences, :community_service_days, 0
    change_column_default :sentences, :conviction_count, 1
    [:community_service_days, :conviction_count, :default_days, :default_months,
      :default_years, :sentence_days, :sentence_months, :sentence_years,
      :suspended_except_days, :suspended_except_months, :suspended_except_years,
      :probation_days, :probation_months, :probation_years, :default_hours,
      :sentence_hours, :suspended_except_hours, :probation_hours,
      :community_service_hours, :sentence_life, :suspended_except_life].each do |col|
      NullsAndDefaultsRs::Sentence.update_all("#{col} = 0","#{col} IS NULL")
      change_column_null :sentences, col, false
    end
    [:doc, :suspended, :credit_time_served, :substance_abuse_program, :driver_improvement,
      :anger_management, :substance_abuse_treatment, :random_drug_screens, :no_victim_contact,
      :art_893, :art_894, :art_895, :probation_reverts, :wo_benefit, :sentence_consecutive,
      :fines_consecutive, :costs_consecutive, :processed, :parish_jail, :legacy,
      :delay_execution, :deny_goodtime, :sentence_death, :pay_during_prob].each do |col|
      NullsAndDefaultsRs::Sentence.update_all("#{col} = FALSE","#{col} IS UNKNOWN")
      change_column_null :sentences, col, false
    end
  
    NullsAndDefaultsRs::SentenceConsecutive.delete_all("docket_id IS NULL OR sentence_id IS NULL")
    change_column_null :sentence_consecutives, :docket_id, false
    change_column_null :sentence_consecutives, :sentence_id, false
    
    [:code, :name].each do |col|
      NullsAndDefaultsRs::SignalCode.update_all("#{col} = ''","#{col} IS NULL")
      change_column :signal_codes, col, :string, :null => false, :default => ''
    end
    
    [:name, :common_name].each do |col|
      NullsAndDefaultsRs::Statute.update_all("#{col} = ''","#{col} IS NULL")
      change_column :statutes, col, :string, :null => false, :default => ''
    end
    
    NullsAndDefaultsRs::Stolen.delete_all("person_id IS NULL")
    change_column_null :stolens, :person_id, false
    [:case_no, :model_no, :serial_no, :item].each do |col|
      NullsAndDefaultsRs::Stolen.update_all("#{col} = ''","#{col} IS NULL")
      change_column :stolens, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsRs::Stolen.update_all("item_value = 0.0","item_value IS NULL")
    change_column_null :stolens, :item_value, false
    [:item_description, :remarks].each do |col|
      NullsAndDefaultsRs::Stolen.update_all("#{col} = ''","#{col} IS NULL")
      change_column :stolens, col, :text, :null => false, :default => ''
    end
    
    NullsAndDefaultsRs::SupportTicket.update_all("summary = ''","summary IS NULL")
    change_column :support_tickets, :summary, :string, :null => false, :default => ''
    NullsAndDefaultsRs::SupportTicket.update_all("details = ''","details IS NULL")
    change_column :support_tickets, :details, :text, :null => false, :default => ''
    NullsAndDefaultsRs::SupportTicket.update_all("bug_report_filed = FALSE","bug_report_filed IS UNKNOWN")
    change_column_null :support_tickets, :bug_report_filed, false
    [:priority, :status, :resolution].each do |col|
      NullsAndDefaultsRs::SupportTicket.update_all("#{col} = 0","#{col} IS NULL")
      change_column :support_tickets, col, :integer, :null => false, :default => 0
    end
    
    NullsAndDefaultsRs::SupportTicketComment.delete_all("support_ticket_id IS NULL OR user_id IS NULL")
    change_column_null :support_ticket_comments, :support_ticket_id, false
    change_column_null :support_ticket_comments, :user_id, false
    NullsAndDefaultsRs::SupportTicketComment.update_all("comments = ''","comments IS NULL")
    change_column :support_ticket_comments, :comments, :text, :null => false, :default => ''

    NullsAndDefaultsRs::SupportTicketWatcher.delete_all("support_ticket_id IS NULL OR user_id IS NULL")
    change_column_null :support_ticket_watchers, :support_ticket_id, false
    change_column_null :support_ticket_watchers, :user_id, false
  end

  def down
  end
end
