class AddHeadingColors < ActiveRecord::Migration
  def up
    add_column :site_configurations, :pdf_heading_bg, :string, :default => 'dddddd'
    add_column :site_configurations, :pdf_heading_txt, :string, :default => '000000'
  end

  def down
    remove_column :site_configurations, :pdf_heading_bg
    remove_column :site_configurations, :pdf_heading_txt
  end
end
