class FixForbidJoins < ActiveRecord::Migration
  class Person < ActiveRecord::Base
    has_and_belongs_to_many :forbids_exemptions, :class_name => 'FixForbidJoins::Forbid', :join_table => 'forbids_exemptions'
    has_many :forbid_exemptions, :class_name => 'FixForbidJoins::ForbidExemption'
    has_many :exempted_forbids, :through => :forbid_exemptions, :source => :forbid
  end
  
  class Offense < ActiveRecord::Base
    has_and_belongs_to_many :forbids, :class_name => 'FixForbidJoins::Forbid'
    has_many :offense_forbids, :class_name => 'FixForbidJoins::OffenseForbid'
    has_many :xforbids, :through => :offense_forbids, :source => :forbid
  end
  
  class Forbid < ActiveRecord::Base
    has_and_belongs_to_many :forbids_exemptions, :class_name => 'FixForbidJoins::Person', :join_table => 'forbids_exemptions'
    has_and_belongs_to_many :offenses, :class_name => 'FixForbidJoins::Offense'
    has_many :forbid_exemptions, :class_name => 'FixForbidJoins::ForbidExemption'
    has_many :exempted_people, :through => :forbid_exemptions, :source => :person
    has_many :offense_forbids, :class_name => 'FixForbidJoins::OffenseForbid'
    has_many :xoffenses, :through => :offense_forbids, :source => :offense
  end
  
  class ForbidExemption < ActiveRecord::Base
    belongs_to :person, :class_name => 'FixForbidJoins::Person'
    belongs_to :forbid, :class_name => 'FixForbidJoins::Forbid'
  end
  
  class OffenseForbid < ActiveRecord::Base
    belongs_to :forbid, :class_name => 'FixForbidJoins::Forbid'
    belongs_to :offense, :class_name => 'FixForbidJoins::Offense'
  end
  
  def up
    # fix join table so it won't clash
    rename_index :forbid_exemptions, "index_forbid_exemptions_on_forbid_id", "index_forbids_exemptions_on_forbid_id"
    rename_index :forbid_exemptions, "index_forbid_exemptions_on_person_id", "index_forbids_exemptions_on_person_id"
    rename_table :forbid_exemptions, :forbids_exemptions
    
    # define new forbid exemption join table
    create_table :forbid_exemptions, :force => true do |t|
      t.integer :forbid_id
      t.integer :person_id
      t.timestamps
    end
    add_index :forbid_exemptions, :forbid_id
    add_index :forbid_exemptions, :person_id
    
    # define new offense forbids join table
    create_table :offense_forbids, :force => true do |t|
      t.integer :forbid_id
      t.integer :offense_id
      t.timestamps
    end
    add_index :offense_forbids, :forbid_id
    add_index :offense_forbids, :offense_id
    
    # convert forbid exemptions and offenses
    FixForbidJoins::Forbid.reset_column_information
    FixForbidJoins::Forbid.all.each do |f|
      f.exempted_person_ids = f.forbids_exemption_ids
      f.xoffense_ids = f.offense_ids
    end
    
    # remove unwanted habtm join tables
    drop_table :forbids_exemptions
    drop_table :forbids_offenses
  end

  def down
    create_table :forbids_exemptions, :id => false, :force => true do |t|
      t.integer :forbid_id
      t.integer :person_id
    end
    add_index :forbids_exemptions, :forbid_id
    add_index :forbids_exemptions, :person_id
    
    create_table :forbids_offenses, :id => false, :force => true do |t|
      t.integer :forbid_id
      t.integer :offense_id
    end
    add_index :forbids_offenses, :forbid_id
    add_index :forbids_offenses, :offense_id
    
    FixForbidJoins::Forbid.reset_column_information
    FixForbidJoins::Forbid.all.each do |f|
      f.forbids_exemption_ids = f.exempted_person_ids
      f.offense_ids = f.xoffense_ids
    end
    
    drop_table :forbid_exemptions
    drop_table :offense_forbids
  
    rename_index :forbids_exemptions, "index_forbids_exemptions_on_forbid_id", "index_forbid_exemptions_on_forbid_id"
    rename_index :forbids_exemptions, "index_forbids_exemptions_on_person_id", "index_forbid_exemptions_on_person_id"
    rename_table :forbids_exemptions, :forbid_exemptions
  end
end
