class EncryptSsn < ActiveRecord::Migration
  class Person < ActiveRecord::Base
    attr_encrypted :ssn
  end
  
  def up
    EncryptSsn::Person.all.each do |p|
      p.ssn = p.old_ssn || ''
      p.save
    end
  end
  
  def down
    EncryptSsn::Person.update_all(ssn: "")
  end
end
