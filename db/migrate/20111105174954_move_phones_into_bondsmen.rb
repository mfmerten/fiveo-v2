class MovePhonesIntoBondsmen < ActiveRecord::Migration
  class Bondsman < ActiveRecord::Base
    has_many :phones, :class_name => 'MovePhonesIntoBondsmen::BondsmanPhone'
  end
  class BondsmanPhone < ActiveRecord::Base
    belongs_to :bondsman, :class_name => 'MovePhonesIntoBondsmen::Bondsman'
  end
  
  def up
    add_column :bondsmen, :phone1, :string
    add_column :bondsmen, :phone1_type_id, :integer
    add_column :bondsmen, :phone1_note, :string
    add_column :bondsmen, :phone2, :string
    add_column :bondsmen, :phone2_type_id, :integer
    add_column :bondsmen, :phone2_note, :string
    add_column :bondsmen, :phone3, :string
    add_column :bondsmen, :phone3_type_id, :integer
    add_column :bondsmen, :phone3_note, :string
    add_column :bondsmen, :remarks, :text
    add_index :bondsmen, :phone1_type_id
    add_index :bondsmen, :phone2_type_id
    add_index :bondsmen, :phone3_type_id
    
    MovePhonesIntoBondsmen::Bondsman.reset_column_information
    
    MovePhonesIntoBondsmen::Bondsman.all.each do |b|
      next if b.phones.empty?
      (0..b.phones.size - 1).each do |x|
        if x <= 2
          b.send("phone#{x+1}=",b.phones[x].value)
          b.send("phone#{x+1}_type_id=",b.phones[x].label_id)
          b.send("phone#{x+1}_note=",b.phones[x].note)
        else
          unless b.remarks?
            b.remarks = ""
          end
          b.remarks << "\nAdditional Phone: #{b.phones[x].label.long_name} #{b.phones[x].value} (#{b.phones[x].note})"
        end
      end
      b.save(:validate => false)
    end
    
    drop_table :bondsman_phones
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
