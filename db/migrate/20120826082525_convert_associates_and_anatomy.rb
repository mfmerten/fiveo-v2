class ConvertAssociatesAndAnatomy < ActiveRecord::Migration
  class Associate < ActiveRecord::Base
    belongs_to :old_relationship, :class_name => "ConvertAssociatesAndAnatomy::Option", :foreign_key => 'relationship_id'
  end
  class InvestCrime < ActiveRecord::Base
    belongs_to :old_victim_relationship, :class_name => "ConvertAssociatesAndAnatomy::Option", :foreign_key => 'victim_relationship_id'
  end
  class Person < ActiveRecord::Base
    belongs_to :old_em_relationship, :class_name => "ConvertAssociatesAndAnatomy::Option", :foreign_key => 'em_relationship_id'
  end
  class Visitor < ActiveRecord::Base
    belongs_to :old_relationship, :class_name => "ConvertAssociatesAndAnatomy::Option", :foreign_key => 'relationship_id'
  end
  class Mark < ActiveRecord::Base
    belongs_to :old_location, :class_name => "ConvertAssociatesAndAnatomy::Option", :foreign_key => 'location_id'
  end
  
  class Option < ActiveRecord::Base
    OptionTypes = {
      2 => 'Anatomy',
      3 => 'Associate Type',
      5 => 'Bond Type',
      10 => 'Call Disposition',
      11 => 'Call Via',
      13 => 'Commissary Type',
      15 => 'Consecutive Type',
      16 => 'Court Costs',
      17 => 'Court Type',
      18 => 'Docket Type',
      25 => 'Hold Type',
      26 => 'Investigation Status',
      30 => 'Judge',
      31 => 'Noservice Type',
      32 => 'Pawn Company',
      33 => 'Pawn Ticket Type',
      34 => 'Pawn Type',
      35 => 'Payment Type',
      36 => 'Phone Type',
      37 => 'Plea Type',
      38 => 'Probation Status',
      39 => 'Probation Type',
      41 => 'Release Reason',
      42 => 'Release Type',
      43 => 'Service Type',
      44 => 'Signal Code',
      46 => 'Subject Type',
      47 => 'Temporary Release Reason',
      48 => 'Transfer Type',
      49 => 'Trial Result',
      50 => 'Vehicle Type',
      51 => 'Warrant Disposition'
    }.freeze
  end
  
  def up
    # add string fields
    add_column :associates, :relationship, :string
    add_column :invest_crimes, :victim_relationship, :string
    add_column :people, :em_relationship, :string
    add_column :visitors, :relationship, :string
    add_column :marks, :location, :string
    
    ConvertAssociatesAndAnatomy::Associate.reset_column_information
    ConvertAssociatesAndAnatomy::InvestCrime.reset_column_information
    ConvertAssociatesAndAnatomy::Person.reset_column_information
    ConvertAssociatesAndAnatomy::Visitor.reset_column_information
    ConvertAssociatesAndAnatomy::Mark.reset_column_information
    
    ConvertAssociatesAndAnatomy::Associate.all.each do |a|
      unless a.old_relationship.nil?
        a.update_attribute(:relationship, a.old_relationship.long_name)
      end
    end
    ConvertAssociatesAndAnatomy::InvestCrime.all.each do |c|
      unless c.old_victim_relationship.nil?
        c.update_attribute(:victim_relationship, c.old_victim_relationship.long_name)
      end
    end
    ConvertAssociatesAndAnatomy::Person.all.each do |p|
      unless p.old_em_relationship.nil?
        p.update_attribute(:em_relationship, p.old_em_relationship.long_name)
      end
    end
    ConvertAssociatesAndAnatomy::Visitor.all.each do |v|
      unless v.old_relationship.nil?
        v.update_attribute(:relationship, v.old_relationship.long_name)
      end
    end
    ConvertAssociatesAndAnatomy::Mark.all.each do |m|
      unless m.old_location.nil?
        m.update_attribute(:location, m.old_location.long_name)
      end
    end
    
    remove_column :associates, :relationship_id
    remove_column :invest_crimes, :victim_relationship_id
    remove_column :people, :em_relationship_id
    remove_column :visitors, :relationship_id
    remove_column :marks, :location_id
    
    ConvertAssociatesAndAnatomy::Option.where(:option_type => [2,3]).all.each{|o| o.destroy}
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
