class RemoveSiteConfigImages < ActiveRecord::Migration
  def up
    ["logo","ceo_img","header_right_img","header_left_img"].each do |f|
      if File.exists?("#{Rails.root}/public/system/#{f}.png")
        File.unlink("#{Rails.root}/public/system/#{f}.png")
      end
    end
    ["fiveo_original.png","fiveo_thumb.png","ceo_original.png","ceo_thumb.png","header_original.png","header_thumb.png","missing_medium.jpg","missing_original.jpg","missing_small.jpg","missing_thumb.jpg","no_image_medium.jpg","no_image_original.jpg","no_image_small.jpg","no_image_thumb.jpg"].each do |f|
      unless File.directory?("#{Rails.root}/public/system/default_images")
        `mkdir -p #{Rails.root}/public/system/default_images`
      end  
      unless File.exists?("#{Rails.root}/public/system/default_images/#{f}")
        `cp #{Rails.root}/config/deploy/#{f} #{Rails.root}/public/system/default_images/#{f}`
      end
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
