class FixProbationStatus < ActiveRecord::Migration
  class Probation < ActiveRecord::Base
  end
  
  def up
    add_column :probations, :terminate_reason, :string
    
    FixProbationStatus::Probation.reset_column_information
    FixProbationStatus::Probation.where("status = 'Terminated Unsatisfactory'").all.each do |p|
      p.status = 'Terminated'
      p.terminate_reason = 'Unsatisfactory'
      p.save
    end
  end

  def down
    remove_column :probations, :terminate_reason, :string
  end
end
