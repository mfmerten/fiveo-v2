class FixMoreRemainingInvestigationJoins < ActiveRecord::Migration
  
  class Warrant < ActiveRecord::Base
    belongs_to :investigation, :class_name => 'FixMoreRemainingInvestigationJoins::Investigation'
    belongs_to :invest_crime, :class_name => 'FixMoreRemainingInvestigationJoins::InvestCrime'
    belongs_to :invest_criminal, :class_name => 'FixMoreRemainingInvestigationJoins::InvestCriminal'
    has_many :invest_warrants, :class_name => 'FixMoreRemainingInvestigationJoins::InvestWarrant'
    has_many :investigations, :through => :invest_warrants
  end
  
  class Investigation < ActiveRecord::Base
    has_many :warrants, :class_name => 'FixMoreRemainingInvestigationJoins::Warrant'
    has_many :invest_warrants, :class_name => 'FixMoreRemainingInvestigationJoins::InvestWarrant'
    has_many :xwarrants, :through => :invest_warrant, :source => :warrant
    has_many :warrant_crimes, :through => :invest_warrant, :source => :crimes
    has_many :warrant_criminals, :through => :invest_warrant, :source => :criminals
  end
  
  class InvestCrime < ActiveRecord::Base
    has_many :warrants, :class_name => 'FixMoreRemainingInvestigationJoins::Warrant'
    has_many :invest_crime_warrants, :class_name => 'FixMoreRemainingInvestigationJoins::InvestCrimeWarrant'
    has_many :invest_warrants, :through => :invest_crime_warrants
    has_many :xwarrants, :through => :invest_warrants
  end
  
  class InvestCriminal < ActiveRecord::Base
    has_many :warrants, :class_name => 'FixMoreRemainingInvestigationJoins::Warrant'
    has_many :invest_criminal_warrants, :class_name => 'FixMoreRemainingInvestigationJoins::InvestCriminalWarrant'
    has_many :invest_warrants, :through => :invest_criminal_warrants
    has_many :xwarrants, :through => :invest_warrants
  end
  
  class InvestWarrant < ActiveRecord::Base
    belongs_to :investigation, :class_name => 'FixMoreRemainingInvestigationJoins::Investigation'
    belongs_to :warrant, :class_name => 'FixMoreRemainingInvestigationJoins::Warrant'
    has_many :invest_crime_warrants, :class_name => 'FixMoreRemainingInvestigationJoins::InvestCrimeWarrant'
    has_many :crimes, :through => :invest_crime_warrants, :source => :invest_crime
    has_many :invest_criminal_warrants, :class_name => 'FixMoreRemainingInvestigationJoins::InvestCriminalWarrant'
    has_many :criminals, :through => :invest_criminal_warrants, :source => :invest_criminals
  end
  
  class InvestCrimeWarrant < ActiveRecord::Base
    belongs_to :invest_crime, :class_name => 'FixMoreRemainingInvestigationJoins::InvestCrime'
    belongs_to :invest_warrant, :class_name => 'FixMoreRemainingInvestigationJoins::InvestWarrant'
  end
  
  class InvestCriminalWarrant < ActiveRecord::Base
    belongs_to :invest_criminal, :class_name => 'FixMoreRemainingInvestigationJoins::InvestCriminal'
    belongs_to :invest_warrant, :class_name => 'FixMoreRemainingInvestigationJoins::InvestWarrant'
  end
  
  def up
    create_table :invest_warrants, :force => true do |t|
      t.integer :investigation_id
      t.integer :warrant_id
      t.timestamps
    end
    add_index :invest_warrants, :investigation_id
    add_index :invest_warrants, :warrant_id
    
    create_table :invest_crime_warrants, :force => true do |t|
      t.integer :invest_crime_id
      t.integer :invest_warrant_id
      t.timestamps
    end
    add_index :invest_crime_warrants, :invest_crime_id
    add_index :invest_crime_warrants, :invest_warrant_id
    
    create_table :invest_criminal_warrants, :force => true do |t|
      t.integer :invest_criminal_id
      t.integer :invest_warrant_id
      t.timestamps
    end
    add_index :invest_criminal_warrants, :invest_criminal_id
    add_index :invest_criminal_warrants, :invest_warrant_id
    
    FixMoreRemainingInvestigationJoins::InvestWarrant.reset_column_information
    FixMoreRemainingInvestigationJoins::InvestCrimeWarrant.reset_column_information
    FixMoreRemainingInvestigationJoins::InvestCriminalWarrant.reset_column_information
    FixMoreRemainingInvestigationJoins::Investigation.all.each do |i|
      i.warrants.each do |w|
        iw = i.invest_warrants.create(:warrant_id => w.id)
        unless w.invest_crime.nil?
          w.invest_crime.invest_warrants << iw
        end
        unless w.invest_criminal.nil?
          w.invest_criminal.invest_warrants << iw
        end
      end
    end
    
    remove_column :warrants, :investigation_id
    remove_column :warrants, :invest_crime_id
    remove_column :warrants, :invest_criminal_id
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
