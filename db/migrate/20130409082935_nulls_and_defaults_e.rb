class NullsAndDefaultsE < ActiveRecord::Migration
  class Event < ActiveRecord::Base
  end
  class Evidence < ActiveRecord::Base
  end
  class EvidenceLocation < ActiveRecord::Base
  end
  class ExternalLink < ActiveRecord::Base
  end
  
  def up
    NullsAndDefaultsE::Event.delete_all("name IS NULL")
    change_column :events, :name, :string, :null => false, :default => ''
    NullsAndDefaultsE::Event.update_all("details = ''","details IS NULL")
    change_column :events, :details, :text, :null => false, :default => ''
    NullsAndDefaultsE::Event.update_all("private = FALSE","private IS UNKNOWN")
    change_column_null :events, :private, false
    
    [:evidence_number, :case_no, :dna_profile, :fingerprint_class].each do |col|
      NullsAndDefaultsE::Evidence.update_all("#{col} = ''","#{col} IS NULL")
      change_column :evidences, col, :string, :null => false, :default => ''
    end
    [:description, :remarks].each do |col|
      NullsAndDefaultsE::Evidence.update_all("#{col} = ''","#{col} IS NULL")
      change_column :evidences, col, :text, :null => false, :default => ''
    end
    NullsAndDefaultsE::Evidence.update_all("legacy = FALSE","legacy IS UNKNOWN")
    change_column_null :evidences, :legacy, false

    NullsAndDefaultsE::EvidenceLocation.delete_all("evidence_id IS NULL")
    change_column_null :evidence_locations, :evidence_id, false
    [:location_from, :location_to, :officer, :officer_unit, :officer_badge].each do |col|
      NullsAndDefaultsE::EvidenceLocation.update_all("#{col} = ''","#{col} IS NULL")
      change_column :evidence_locations, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsE::EvidenceLocation.update_all("remarks = ''","remarks IS NULL")
    change_column :evidence_locations, :remarks, :text, :null => false, :default => ''
  
    [:name, :url].each do |col|
      NullsAndDefaultsE::ExternalLink.delete_all("#{col} IS NULL")
      change_column :external_links, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsE::ExternalLink.update_all("short_name = ''","short_name IS NULL")
    change_column :external_links, :short_name, :string, :null => false, :default => ''
    NullsAndDefaultsE::ExternalLink.update_all("description = ''","description IS NULL")
    change_column :external_links, :description, :text, :null => false, :default => ''
    NullsAndDefaultsE::ExternalLink.update_all("front_page = FALSE","front_page IS UNKNOWN")
    change_column_null :external_links, :front_page, false
  end

  def down
  end
end
