class AddCourtDocketTypeTables < ActiveRecord::Migration
  class DocketType < ActiveRecord::Base
  end
  class CourtType < ActiveRecord::Base
  end
  CourtTypes = [
    'Arraignment', 'Bond Hearing', 'Misdemeanor', 'Motion Hearing', 'Pre-Trial',
    'Probation/Parole', 'Sentencing', 'Trial'
  ].freeze
  DocketTypes = ['Arraignment', 'Misdemeanor', 'Motion', 'Pre-Trial', 'Trial'].freeze
  def up
    create_table :docket_types, :force => true do |t|
      t.string :name
      t.boolean :disabled
      t.integer :created_by, :default => 1
      t.integer :updated_by, :default => 1
      t.timestamps
    end
    add_index :docket_types, :created_by
    add_index :docket_types, :updated_by
    
    create_table :court_types, :force => true do |t|
      t.string :name
      t.boolean :disabled
      t.integer :created_by, :default => 1
      t.integer :updated_by, :default => 1
      t.timestamps
    end
    add_index :court_types, :created_by
    add_index :court_types, :updated_by
    
    AddCourtDocketTypeTables::CourtType.reset_column_information
    AddCourtDocketTypeTables::CourtType.reset_column_information
    
    AddCourtDocketTypeTables::CourtTypes.each do |t|
      AddCourtDocketTypeTables::CourtType.create(:name => t, :created_by => 1, :updated_by => 1)
    end
    AddCourtDocketTypeTables::DocketTypes.each do |t|
      AddCourtDocketTypeTables::DocketType.create(:name => t, :created_by => 1, :updated_by => 1)
    end
  end

  def down
    drop_table :docket_types
    drop_table :court_types
  end
end
