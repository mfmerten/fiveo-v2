class RemoveExternalSupport < ActiveRecord::Migration
  def up
    remove_column :bugs, :support_requested
    remove_column :site_configurations, :support_email
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
