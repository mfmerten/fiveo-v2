class AddGarnishments < ActiveRecord::Migration
  def self.up
    unless table_exists?(:garnishments)
      create_table :garnishments do |t|
        t.string :name
        t.string :suit_no
        t.string :description
        t.string :attorney
        t.text :details
        t.date :received_date
        t.date :satisfied_date
        t.text :remarks
        t.integer :created_by, :default => 1
        t.integer :updated_by, :default => 1
        t.timestamps
      end
      add_index :garnishments, :created_by
      add_index :garnishments, :updated_by
    end
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
