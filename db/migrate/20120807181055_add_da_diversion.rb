class AddDaDiversion < ActiveRecord::Migration
  def up
    add_column :da_charges, :da_diversion, :boolean
  end

  def down
    remove_column :da_charges, :da_diversion
  end
end
