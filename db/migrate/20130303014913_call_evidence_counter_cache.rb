class CallEvidenceCounterCache < ActiveRecord::Migration
  
  class Call < ActiveRecord::Base
    has_many :call_subjects, :class_name => 'CallEvidenceCounterCache::CallSubject'
  end
  
  class CallSubject < ActiveRecord::Base
    belongs_to :call, :class_name => 'CallEvidenceCounterCache::Call'
  end
  
  class Evidence < ActiveRecord::Base
    has_many :evidence_locations, :class_name => 'CallEvidenceCounterCache::EvidenceLocation'
  end
  
  class EvidenceLocation < ActiveRecord::Base
    belongs_to :evidence, :class_name => 'CallEvidenceCounterCache::Evidence'
  end
  
  def up
    add_column :calls, :call_subjects_count, :integer, :default => 0, :null => false
    add_column :evidences, :evidence_locations_count, :integer, :default => 0, :null => false
    
    CallEvidenceCounterCache::Call.reset_column_information
    CallEvidenceCounterCache::Call.all.each do |c|
      c.update_attribute(:call_subjects_count, c.call_subjects.length)
    end
    CallEvidenceCounterCache::Evidence.reset_column_information
    CallEvidenceCounterCache::Evidence.all.each do |e|
      e.update_attribute(:evidence_locations_count, e.evidence_locations.length)
    end
  end

  def down
    remove_column :calls, :call_subjects_count
    remove_column :evidences, :evidence_locations_count
  end
end
