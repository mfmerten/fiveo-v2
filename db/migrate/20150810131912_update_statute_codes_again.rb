class UpdateStatuteCodesAgain < ActiveRecord::Migration
  class Statute < ActiveRecord::Base
  end
  
  def up
    say_with_time "Updating Statute Codes..." do
      UpdateStatuteCodesAgain::Statute.find_each do |s|
        s.update_attribute(:code, s.name.clone.sub(/^[Rr]\.?\s?[Ss]\.? ([0-9]+:?[0-9]*.?[0-9]*) .*$/,'\1'))
      end
    end
  end
  
  def down
  end
end
