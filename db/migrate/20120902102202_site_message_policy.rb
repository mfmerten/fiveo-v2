class SiteMessagePolicy < ActiveRecord::Migration
  class SiteConfiguration < ActiveRecord::Base
  end
  
  def up
    add_column :site_configurations, :message_policy, :text
    SiteMessagePolicy::SiteConfiguration.reset_column_information
    policy = %(
h3. Messages Are Not Private!

All messages are recorded and may be reviewed by Site Administrators
for security and legal purposes.

h3. Offensive Messages Forbidden!

Any message containing racial slurs, overt sexual references or any
other material that may be reasonably considered offensive or abusive
will not be tolerated at any time!

h3. No Private or Confidential Information In Messages

Never include private or confidential information in a system
message.  These messages may be transmitted via plain text EMail on the
internet without your knowledge or consent. This message system IS NOT
CONSIDERED SECURE!

h3. Personal Messages Are Allowed

You may send personal (non-business) messages to other users on this
system as long as the messages conform to the conditions listed above.
    )
    SiteMessagePolicy::SiteConfiguration.all.each do |s|
      s.update_attribute(:message_policy, policy)
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
