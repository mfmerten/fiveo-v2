class ConvertHistoryType < ActiveRecord::Migration
  class Option < ActiveRecord::Base
    OptionTypes = {
      2 => 'Anatomy',
      3 => 'Associate Type',
      5 => 'Bond Type',
      9 => 'Build',
      10 => 'Call Disposition',
      11 => 'Call Via',
      13 => 'Commissary Type',
      14 => 'Complexion',
      15 => 'Consecutive Type',
      16 => 'Court Costs',
      17 => 'Court Type',
      18 => 'Docket Type',
      19 => 'Eye Color',
      20 => 'Facial Hair',
      22 => 'Hair Color',
      23 => 'Hair Type',
      24 => 'History Type',
      25 => 'Hold Type',
      26 => 'Investigation Status',
      30 => 'Judge',
      31 => 'Noservice Type',
      32 => 'Pawn Company',
      33 => 'Pawn Ticket Type',
      34 => 'Pawn Type',
      35 => 'Payment Type',
      36 => 'Phone Type',
      37 => 'Plea Type',
      38 => 'Probation Status',
      39 => 'Probation Type',
      40 => 'Race',
      41 => 'Release Reason',
      42 => 'Release Type',
      43 => 'Service Type',
      44 => 'Signal Code',
      46 => 'Subject Type',
      47 => 'Temporary Release Reason',
      48 => 'Transfer Type',
      49 => 'Trial Result',
      50 => 'Vehicle Type',
      51 => 'Warrant Disposition'
    }.freeze

    def self.find_by_option_type(option_type)
      otype = get_option_type(option_type)
      if otype.nil?
        []
      else
        where(['option_type = ?', otype]).all
      end
    end

    def self.get_option_type(value)
      ConvertHistoryType::Option::OptionTypes.merge(ConvertHistoryType::Option::OptionTypes.invert)[value]
    end
  end
  
  def up
    # remove any history type options - not used in any deployment
    options = ConvertHistoryType::Option.find_by_option_type("History Type")
    options.each{|o| o.destroy}
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
