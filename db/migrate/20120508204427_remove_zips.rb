class RemoveZips < ActiveRecord::Migration
  def up
    drop_table :zips
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
