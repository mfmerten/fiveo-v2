class AddInvestOffense < ActiveRecord::Migration
  def change
    create_table :invest_offenses, :force => true do |t|
      t.integer :investigation_id
      t.integer :offense_id
      t.timestamps
    end
    add_index :invest_offenses, :investigation_id
    add_index :invest_offenses, :offense_id
    
    create_table :invest_crime_offenses, :force => true do |t|
      t.integer :invest_crime_id
      t.integer :invest_offense_id
      t.timestamps
    end
    add_index :invest_crime_offenses, :invest_crime_id
    add_index :invest_crime_offenses, :invest_offense_id
  
    create_table :invest_report_offenses, :force => true do |t|
      t.integer :invest_report_id
      t.integer :invest_offense_id
      t.timestamps
    end
    add_index :invest_report_offenses, :invest_report_id
    add_index :invest_report_offenses, :invest_offense_id
  end
end
