class UpdateBondColumns < ActiveRecord::Migration
  class Bond < ActiveRecord::Base
    belongs_to :old_status, :class_name => 'UpdateBondColumns::Option', :foreign_key => 'status_id'
    belongs_to :hold, :class_name => 'UpdateBondColumns::Hold'
    belongs_to :arrest, :class_name => 'UpdateBondColumns::Arrest'
    has_and_belongs_to_many :holds, :class_name => 'UpdateBondColumns::Hold'
  end
  
  class Arrest < ActiveRecord::Base
    has_many :holds, :class_name => 'UpdateBondColumns::Hold'
  end
  
  class Hold < ActiveRecord::Base
    belongs_to :arrest, :class_name => 'UpdateBondColumns::Arrest'
    belongs_to :hold_type, :class_name => 'UpdateBondColumns::Option', :foreign_key => 'hold_type_id'
  end
  
  class Option < ActiveRecord::Base
    OptionTypes = {
      1 => 'Absent Type',
      2 => 'Anatomy',
      3 => 'Associate Type',
      4 => 'Bond Status',
      5 => 'Bond Type',
      # 6 => 'Bug Priority' -- obsolete, now defined in Bug model
      # 7 => 'Bug Resolution' -- obsolete, now defined in Bug model
      # 8 => 'Bug Status' -- obsolete, now defined in Bug model
      9 => 'Build',
      10 => 'Call Disposition',
      11 => 'Call Via',
      # 12 => 'Cell' -- obsolete, now defined per Jail
      13 => 'Commissary Type',
      14 => 'Complexion',
      15 => 'Consecutive Type',
      16 => 'Court Costs',
      17 => 'Court Type',
      18 => 'Docket Type',
      19 => 'Eye Color',
      20 => 'Facial Hair',
      # 21 => 'Facility' -- obsolete, now defined per Jail
      22 => 'Hair Color',
      23 => 'Hair Type',
      24 => 'History Type',
      25 => 'Hold Type',
      26 => 'Investigation Status',
      # 27 => 'Jail1 Cell',  Obsolete
      # 28 => 'Jail2 Cell',  Obsolete
      # 29 => 'Jail3 Cell',  Obsolete
      30 => 'Judge',
      31 => 'Noservice Type',
      32 => 'Pawn Company',
      33 => 'Pawn Ticket Type',
      34 => 'Pawn Type',
      35 => 'Payment Type',
      36 => 'Phone Type',
      37 => 'Plea Type',
      38 => 'Probation Status',
      39 => 'Probation Type',
      40 => 'Race',
      41 => 'Release Reason',
      42 => 'Release Type',
      43 => 'Service Type',
      44 => 'Signal Code',
      # 45 => 'State' -- obsolete, now defined in a pseudo model
      46 => 'Subject Type',
      47 => 'Temporary Release Reason',
      48 => 'Transfer Type',
      49 => 'Trial Result',
      50 => 'Vehicle Type',
      51 => 'Warrant Disposition'
    }.freeze

    def self.find_by_option_type(option_type)
      otype = get_option_type(option_type)
      if otype.nil?
        []
      else
        where(['option_type = ?', otype]).all
      end
    end

    def self.get_option_type(value)
      UpdateBondColumns::Option::OptionTypes.merge(UpdateBondColumns::Option::OptionTypes.invert)[value]
    end
  end
  
  def up
    # add bonds_holds join table
    create_table :bonds_holds, :id => false, :force => true do |t|
      t.integer :bond_id
      t.integer :hold_id
    end
    add_index :bonds_holds, :bond_id
    add_index :bonds_holds, :hold_id
    
    # add columns to bonds table
    add_column :bonds, :final, :boolean
    add_column :bonds, :status, :string
    
    UpdateBondColumns::Bond.reset_column_information
    UpdateBondColumns::Bond.all.each do |b|
      # fix status
      current_status = nil
      unless b.old_status.nil? || b.old_status.long_name == 'Active' || b.old_status.long_name == 'Final'
        current_status = b.old_status.long_name
      end
      b.status = current_status
      if current_status.blank?
        b.status_datetime = nil
      end
      if !current_status.blank? || b.old_status.nil? || b.old_status.long_name == 'Final'
        b.final = true
      end
      # fix hold joins
      holds = []
      if !b.hold.nil? && !b.hold.hold_type.nil? && b.hold.hold_type.long_name != 'Bondable'
        # bond attached to some hold other than Bondable - create single link
        b.holds << b.hold
      else
        # bond attached to 'Bondable' or bond hold invalid, get list from arrest
        unless b.arrest.nil? && (b.hold.nil? || b.hold.arrest.nil?)
          # path to arrest found
          arrest = b.arrest
          if arrest.nil?
            arrest = b.hold.arrest
          end
          if arrest.arrest_type == 0
            # district arrest - only one with possible multiple Bondable holds
            holds << arrest.holds.reject{|h| h.hold_type.nil? || h.hold_type.long_name != 'Bondable'}
          end
        end
        if holds.empty? && !b.hold.blank?
          # arrest lookup didn't work, try just adding single hold
          b.holds << b.hold
        else
          # arrest lookup worked, current hold should already be included
          b.holds << holds.compact.uniq
        end
      end
      # now catch the bonds with no holds and no arrest
      # will leave them in, but mark them final
      if holds.empty? && arrest.nil?
        b.final = true
      end
      b.save
    end
    
    # drop status_id and hold_id columns
    remove_column :bonds, :status_id
    remove_column :bonds, :hold_id
    
    # remove old Bond Status options
    options = UpdateBondColumns::Option.find_by_option_type("Bond Status")
    options.each{|o| o.destroy}
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
