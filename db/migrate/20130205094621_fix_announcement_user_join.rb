class FixAnnouncementUserJoin < ActiveRecord::Migration
  class User < ActiveRecord::Base
    has_many :announcement_ignores, :class_name => 'FixAnnouncementUserJoin::AnnouncementIgnore'
    has_many :announcements, :class_name => 'FixAnnouncementUserJoin::Announcement', :through => :announcement_ignores
    has_and_belongs_to_many :ignored_announcements, :class_name => 'FixAnnouncementUserJoin::Announcement', :join_table => 'announce_ignores'
  end
  
  class Announcement < ActiveRecord::Base
    has_many :announcement_ignores, :class_name => 'FixAnnouncementUserJoin::AnnouncementIgnore'
    has_many :users, :class_name => 'FixAnnouncementUserJoin::User', :through => :announcement_ignores
    has_and_belongs_to_many :ignorers, :class_name => 'FixAnnouncementUserJoin::User', :join_table => 'announce_ignores'
  end
  
  class AnnouncementIgnore < ActiveRecord::Base
    belongs_to :announcement, :class_name => 'FixAnnouncementUserJoin::Announcement'
    belongs_to :user, :class_name => 'FixAnnouncementUserJoin::User'
  end
  
  def up
    create_table :announcement_ignores, :force => true do |t|
      t.integer :announcement_id
      t.integer :user_id
      t.timestamps
    end
    add_index :announcement_ignores, :announcement_id
    add_index :announcement_ignores, :user_id
    
    FixAnnouncementUserJoin::Announcement.all.each do |a|
      a.user_ids = a.ignorer_ids
    end
    
    drop_table :announce_ignores
  end

  def down
    create_table :announce_ignores, :id => false, :force => true do |t|
      t.integer :announcement_id
      t.integer :user_id
    end
    add_index :announce_ignores, :announcement_id
    add_index :announce_ignores, :user_id
    
    FixAnnouncementUserJoin::Announcement.all.each do |a|
      a.ignorer_ids = a.user_ids
    end
    
    drop_table :announcement_ignores
  end
end
