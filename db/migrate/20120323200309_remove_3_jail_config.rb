class Remove3JailConfig < ActiveRecord::Migration
  # part of Rails3 upgrade
  # removing partial implementation of 3-jail system (bug 29)
  def up
    remove_column :site_configurations, :jail1_gt_hours_per_day
    remove_column :site_configurations, :jail2_gt_hours_per_day
    remove_column :site_configurations, :jail3_gt_hours_per_day
    remove_column :site_configurations, :jail1_enabled
    remove_column :site_configurations, :jail1_male
    remove_column :site_configurations, :jail1_female
    remove_column :site_configurations, :jail1_gt_by_default
    remove_column :site_configurations, :jail2_enabled
    remove_column :site_configurations, :jail2_male
    remove_column :site_configurations, :jail2_female
    remove_column :site_configurations, :jail2_gt_by_default
    remove_column :site_configurations, :jail3_enabled
    remove_column :site_configurations, :jail3_male
    remove_column :site_configurations, :jail3_female
    remove_column :site_configurations, :jail3_gt_by_default
    remove_column :site_configurations, :jail1_name
    remove_column :site_configurations, :jail1_short_name
    remove_column :site_configurations, :jail2_name
    remove_column :site_configurations, :jail2_short_name
    remove_column :site_configurations, :jail3_name
    remove_column :site_configurations, :jail3_short_name
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
