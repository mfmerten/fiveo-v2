class NullsAndDefaultsGh < ActiveRecord::Migration
  class Garnishment < ActiveRecord::Base
  end
  class Group < ActiveRecord::Base
  end
  class History < ActiveRecord::Base
  end
  class Hold < ActiveRecord::Base
  end
  class HoldBlocker < ActiveRecord::Base
  end
  class HoldBond < ActiveRecord::Base
  end
  
  def up
    [:name, :suit_no, :description, :attorney].each do |col|
      NullsAndDefaultsGh::Garnishment.update_all("#{col} = ''","#{col} IS NULL")
      change_column :garnishments, col, :string, :null => false, :default => ''
    end
    [:details, :remarks].each do |col|
      NullsAndDefaultsGh::Garnishment.update_all("#{col} = ''","#{col} IS NULL")
      change_column :garnishments, col, :text, :null => false, :default => ''
    end
  
    [:name, :description, :permissions].each do |col|
      NullsAndDefaultsGh::Group.update_all("#{col} = ''","#{col} IS NULL")
      change_column :groups, col, :string, :null => false, :default => ''
    end
    
    [:key, :name].each do |col|
      NullsAndDefaultsGh::History.update_all("#{col} = ''","#{col} IS NULL")
      change_column :histories, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsGh::History.update_all("type_id = 0","type_id IS NULL")
    change_column :histories, :type_id, :integer, :null => false, :default => 0
    NullsAndDefaultsGh::History.update_all("details = ''","details IS NULL")
    change_column :histories, :details, :text, :null => false, :default => ''
    
    NullsAndDefaultsGh::Hold.delete_all("booking_id IS NULL")
    change_column_null :holds, :booking_id, false
    [:hold_by, :hold_by_unit, :cleared_by, :cleared_by_unit, :hold_by_badge,
      :cleared_by_badge, :agency, :transfer_officer, :transfer_officer_badge,
      :transfer_officer_unit, :explanation, :hold_type, :cleared_because,
      :temp_release_reason].each do |col|
      NullsAndDefaultsGh::Hold.update_all("#{col} = ''","#{col} IS NULL")
      change_column :holds, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsGh::Hold.update_all("remarks = ''","remarks IS NULL")
    change_column :holds, :remarks, :text, :null => false, :default => ''
    [:doc, :doc_billable, :legacy, :deny_goodtime, :other_billable, :death_sentence].each do |col|
      NullsAndDefaultsGh::Hold.update_all("#{col} = FALSE","#{col} IS UNKNOWN")
      change_column_null :holds, col, false
    end
    NullsAndDefaultsGh::Hold.update_all("fines = 0.0","fines IS NULL")
    change_column_null :holds, :fines, false
    [:hours_served, :hours_total, :hours_time_served].each do |col|
      NullsAndDefaultsGh::Hold.update_all("#{col} = 0","#{col} IS NULL")
      change_column_null :holds, col, false
    end
    NullsAndDefaultsGh::Hold.update_all("hours_goodtime = 0.0","hours_goodtime IS NULL")
    change_column_null :holds, :hours_goodtime, false
    
    NullsAndDefaultsGh::HoldBlocker.delete_all("blocker_id IS NULL OR hold_id IS NULL")
    change_column_null :hold_blockers, :blocker_id, false
    change_column_null :hold_blockers, :hold_id, false
  
    NullsAndDefaultsGh::HoldBond.delete_all("bond_id IS NULL OR hold_id IS NULL")
    change_column_null :hold_bonds, :bond_id, false
    change_column_null :hold_bonds, :hold_id, false
  end
  
  def down
  end
end
