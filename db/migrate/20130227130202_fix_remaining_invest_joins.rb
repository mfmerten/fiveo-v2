class FixRemainingInvestJoins < ActiveRecord::Migration
  
  class Arrest < ActiveRecord::Base
    has_many :invest_arrests, :class_name => 'FixRemainingInvestJoins::InvestArrest'
    has_many :investigations, :through => :invest_arrests
  end
  
  class Call < ActiveRecord::Base
    has_many :invest_cases, :class_name => 'FixRemainingInvestJoins::InvestCase'
    has_many :investigations, :through => :invest_cases
  end
  
  class Contact < ActiveRecord::Base
    has_many :invest_contacts, :class_name => 'FixRemainingInvestJoins::InvestContact'
    has_many :investigations, :through => :invest_contacts
  end
  
  class Evidence < ActiveRecord::Base
    has_many :invest_evidences, :class_name => 'FixRemainingInvestJoins::InvestEvidence'
    has_many :investigations, :through => :invest_evidences
  end
  
  class Stolen < ActiveRecord::Base
    has_many :invest_stolens, :class_name => 'FixRemainingInvestJoins::InvestStolen'
    has_many :investigations, :through => :invest_stolens
  end
  
  class InvestsContact < ActiveRecord::Base
    has_and_belongs_to_many :investigations, :class_name => 'FixRemainingInvestJoins::Investigation', :foreign_key => 'invest_contact_id'
  end
  
  class Investigation < ActiveRecord::Base
    has_and_belongs_to_many :invests_contacts, :class_name => 'FixRemainingInvestJoins::InvestsContact', :foreign_key => 'investigation_id'
    has_many :invest_contacts, :class_name => 'FixRemainingInvestJoins::InvestContact'
    has_many :contacts, :through => :invest_contacts
    has_many :contact_crimes, :through => :invest_contacts, :source => :crimes, :uniq => true
    has_many :invest_cases, :class_name => 'FixRemainingInvestJoins::InvestCase'
    has_many :cases, :through => :invest_cases, :source => :call
    has_many :case_crimes, :through => :invest_cases, :source => :crimes, :uniq => true
    has_many :invest_arrests, :class_name => 'FixRemainingInvestJoins::InvestArrest'
    has_many :arrests, :through => :invest_arrests
    has_many :arrest_crimes, :through => :invest_arrests, :source => :crimes, :uniq => true
    has_many :arrest_criminals, :through => :invest_arrests, :source => :criminals, :uniq => true
    has_many :arrest_reports, :through => :invest_arrests, :source => :reports, :uniq => true
    has_many :invest_evidences, :class_name => 'FixRemainingInvestJoins::InvestEvidence'
    has_many :evidences, :through => :invest_evidences
    has_many :evidence_crimes, :through => :invest_evidences, :source => :crimes, :uniq => true
    has_many :evidence_criminals, :through => :invest_evidences, :source => :criminals, :uniq => true
    has_many :invest_stolens, :class_name => 'FixRemainingInvestJoins::InvestStolen'
    has_many :stolens, :through => :invest_stolens
    has_many :stolen_crimes, :through => :invest_stolens, :source => :crimes, :uniq => true
    has_many :stolen_criminals, :through => :invest_stolens, :source => :criminals, :uniq => true
    has_many :invest_notes, :class_name => 'FixRemainingInvestJoins::InvestNote'
    has_many :notes, :class_name => 'FixRemainingInvestJoins::InvestNote', :as => :invest_record
  end
  
  class InvestArrest < ActiveRecord::Base
    belongs_to :arrest, :class_name => 'FixRemainingInvestJoins::Arrest'
    belongs_to :investigation, :class_name => 'FixRemainingInvestJoins::Investigation'
    belongs_to :invest_crime, :class_name => 'FixRemainingInvestJoins::InvestCrime'
    belongs_to :invest_criminal, :class_name => 'FixRemainingInvestJoins::InvestCriminal'
    belongs_to :invest_report, :class_name => 'FixRemainingInvestJoins::InvestReport'
    has_many :invest_crime_arrests, :class_name => 'FixRemainingInvestJoins::InvestCrimeArrest'
    has_many :crimes, :through => :invest_crime_arrests, :source => :invest_crime
    has_many :invest_criminal_arrests, :class_name => 'FixRemainingInvestJoins::InvestCriminalArrest'
    has_many :criminals, :through => :invest_criminal_arrests, :source => :invest_criminal
    has_many :invest_report_arrests, :class_name => 'FixRemainingInvestJoins::InvestReportArrest'
    has_many :reports, :through => :invest_report_arrests, :source => :invest_report
  end
  
  class InvestCase < ActiveRecord::Base
    belongs_to :investigation, :class_name => 'FixRemainingInvestJoins::Investigation'
    belongs_to :call, :class_name => 'FixRemainingInvestJoins::Call'
    belongs_to :invest_crime, :class_name => 'FixRemainingInvestJoins::InvestCrime'
    has_many :invest_crime_cases, :class_name => 'FixRemainingInvestJoins::InvestCrimeCase'
    has_many :crimes, :through => :invest_crime_cases, :source => :invest_crime
  end
  
  class InvestContact < ActiveRecord::Base
    belongs_to :investigation, :class_name => 'FixRemainingInvestJoins::Investigation'
    belongs_to :contact, :class_name => 'FixRemainingInvestJoins::Contact'
    has_many :invest_crime_contacts, :class_name => 'FixRemainingInvestJoins::InvestCrimeContact'
    has_many :crimes, :through => :invest_crime_contacts, :source => :invest_crime
  end

  class InvestCrime < ActiveRecord::Base
    has_many :invest_crime_contacts, :class_name => 'FixRemainingInvestJoins::InvestCrimeContact'
    has_many :invest_contacts, :through => :invest_crime_contacts
    has_many :contacts, :through => :invest_contacts
    has_many :invest_crime_cases, :class_name => 'FixRemainingInvestJoins::InvestCrimeCase'
    has_many :invest_cases, :through => :invest_crime_cases
    has_many :cases, :through => :invest_cases, :source => :call, :uniq => true
    has_many :invest_crime_arrests, :class_name => 'FixRemainingInvestJoins::InvestCrimeArrest'
    has_many :invest_arrests, :through => :invest_crime_arrests
    has_many :arrests, :through => :invest_arrests, :uniq => true
    has_many :invest_crime_evidences, :class_name => 'FixRemainingInvestJoins::InvestCrimeEvidence'
    has_many :invest_evidences, :through => :invest_crime_evidences
    has_many :evidences, :through => :invest_evidences, :uniq => true
    has_many :invest_crime_stolens, :class_name => 'FixRemainingInvestJoins::InvestCrimeStolen'
    has_many :invest_stolens, :through => :invest_crime_stolens
    has_many :stolens, :through => :invest_stolens, :uniq => true
    has_many :invest_criminals, :class_name => 'FixRemainingInvestJoins::InvestCriminal'
    has_many :invest_crime_criminals, :class_name => 'FixRemainingInvestJoins::InvestCrimeCriminal'
    has_many :criminals, :through => :invest_crime_criminals, :source => :invest_criminal
    has_many :invest_photos, :class_name => 'FixRemainingInvestJoins::InvestPhoto'
    has_many :invest_crime_photos, :class_name => 'FixRemainingInvestJoins::InvestCrimePhoto'
    has_many :photos, :through => :invest_crime_photos, :source => :invest_photo
    has_many :invest_vehicles, :class_name => 'FixRemainingInvestJoins::InvestVehicle'
    has_many :invest_crime_vehicles, :class_name => 'FixRemainingInvestJoins::InvestCrimeVehcile'
    has_many :vehicles, :through => :invest_crime_vehicles, :source => :invest_vehicle
  end
  
  class InvestCriminal < ActiveRecord::Base
    has_and_belongs_to_many :invest_reports, :class_name => 'FixRemainingInvestJoins::InvestReport', :join_table => 'report_criminals'
    has_many :invest_criminal_arrests, :class_name => 'FixRemainingInvestJoins::InvestCriminalArrest'
    has_many :invest_arrests, :through => :invest_criminal_arrests
    has_many :arrests, :through => :invest_arrests, :uniq => true
    has_many :invest_criminal_evidences, :class_name => 'FixRemainingInvestJoins::InvestCriminalEvidence'
    has_many :invest_evidences, :through => :invest_criminal_evidences
    has_many :evidences, :through => :invest_evidences, :uniq => true
    has_many :invest_criminal_stolens, :class_name => 'FixRemainingInvestJoins::InvestCriminalStolen'
    has_many :invest_stolens, :through => :invest_criminal_stolens
    has_many :stolens, :through => :invest_stolens, :uniq => true
    belongs_to :invest_crime, :class_name => 'FixRemainingInvestJoins::InvestCrime'
    has_many :invest_crime_criminals, :class_name => 'FixRemainingInvestJoins::InvestCrimeCriminals'
    has_many :crimes, :through => :invest_crime_criminals, :source => :invest_crime
    has_many :invest_photos, :class_name => 'FixRemainingInvestJoins::InvestPhoto'
    has_many :invest_criminal_photos, :class_name => 'FixRemainingInvestJoins::InvestCriminalPhoto'
    has_many :photos, :through => :invest_criminal_photos, :source => :invest_photo
    has_many :invest_report_criminals, :class_name => 'FixRemainingInvestJoins::InvestReportCriminal'
    has_many :reports, :through => :invest_report_criminals, :source => :invest_report
  end
  
  class InvestEvidence < ActiveRecord::Base
    belongs_to :investigation, :class_name => 'FixRemainingInvestJoins::Investigation'
    belongs_to :evidence, :class_name => 'FixRemainingInvestJoins::Evidence'
    belongs_to :invest_crime, :class_name => 'FixRemainingInvestJoins::InvestCrime'
    belongs_to :invest_criminal, :class_name => 'FixRemainingInvestJoins::InvestCriminal'
    has_many :invest_crime_evidences, :class_name => 'FixRemainingInvestJoins::InvestCrimeEvidence'
    has_many :crimes, :through => :invest_crime_evidences, :source => :invest_crime
    has_many :invest_criminal_evidences, :class_name => 'FixRemainingInvestJoins::InvestCriminalEvidence'
    has_many :criminals, :through => :invest_criminal_evidences, :source => :invest_criminal
  end
  
  class InvestNote < ActiveRecord::Base
    belongs_to :investigation, :class_name => 'FixRemainingInvestJoins::Investigation'
    belongs_to :invest_record, :polymorphic => true
  end
  
  class InvestPhoto < ActiveRecord::Base
    has_and_belongs_to_many :invest_reports, :class_name => 'FixRemainingInvestJoins::InvestReport', :join_table => 'report_photos'
    belongs_to :investigation, :class_name => 'FixRemainingInvestJoins::Investigation'
    belongs_to :invest_crime, :class_name => 'FixRemainingInvestJoins::InvestCrime'
    belongs_to :invest_criminal, :class_name => 'FixRemainingInvestJoins::InvestCriminal'
    belongs_to :invest_vehicle, :class_name => 'FixRemainingInvestJoins::InvestVehicle'
    has_many :invest_crime_photos, :class_name => 'FixRemainingInvestJoins::InvestCrimePhoto'
    has_many :crimes, :through => :invest_crime_photos, :source => :invest_crime
    has_many :invest_criminal_photos, :class_name => 'FixRemainingInvestJoins::InvestCriminalPhoto'
    has_many :criminals, :through => :invest_criminal_photos, :source => :invest_criminal
    has_many :invest_vehicle_photos, :class_name => 'FixRemainingInvestJoins::InvestVehiclePhoto'
    has_many :vehicles, :through => :invest_vehicle_photos, :source => :invest_vehicle
    has_many :invest_report_photos, :class_name => 'FixRemainingInvestJoins::InvestReportPhoto'
    has_many :reports, :through => :invest_report_photos, :source => :invest_report
  end
  
  class InvestReport < ActiveRecord::Base
    has_and_belongs_to_many :invest_photos, :class_name => 'FixRemainingInvestJoins::InvestPhoto', :join_table => 'report_photos'
    has_and_belongs_to_many :invest_vehicles, :class_name => 'FixRemainingInvestJoins::InvestVehicle', :join_table => 'report_vehicles'
    has_and_belongs_to_many :invest_criminals, :class_name => 'FixRemainingInvestJoins::InvestCriminal', :join_table => 'report_criminals'
    has_many :invest_report_arrests, :class_name => 'FixRemainingInvestJoins::InvestReportArrest'
    has_many :invest_arrests, :through => :invest_report_arrests
    has_many :arrests, :through => :invest_arrests, :uniq => true
    has_many :invest_report_photos, :class_name => 'FixRemainingInvestJoins::InvestReportPhoto'
    has_many :photos, :through => :invest_report_photos, :source => :invest_photo
    has_many :invest_report_criminals, :class_name => 'FixRemainingInvestJoins::InvestReportCriminal'
    has_many :criminals, :through => :invest_report_criminals, :source => :invest_criminal
    has_many :invest_report_vehicles, :class_name => 'FixRemainingInvestJoins::InvestReportVehicle'
    has_many :vehicles, :through => :invest_report_vehicles, :source => :invest_vehicle
  end
  
  class InvestVehicle < ActiveRecord::Base
    has_and_belongs_to_many :invest_reports, :class_name => 'FixRemainingInvestJoins::InvestReport', :join_table => 'report_vehicles'
    belongs_to :investigation, :class_name => 'FixRemainingInvestJoins::Investigation'
    belongs_to :invest_crime, :class_name => 'FixRemainingInvestJoins::InvestCrime'
    belongs_to :invest_criminal, :class_name => 'FixRemainingInvestJoins::InvestCriminal'
    has_many :invest_photos, :class_name => 'FixRemainingInvestJoins::InvestPhoto'
    has_many :invest_vehicle_photos, :class_name => 'FixRemainingInvestJoins::InvestVehiclePhoto'
    has_many :photos, :through => :invest_vehicle_photos, :source => :invest_photo
    has_many :invest_report_vehicles, :class_name => 'FixRemainingInvestJoins::InvestReportVehicle'
    has_many :reports, :through => :invest_report_vehicles, :source => :invest_report
  end
  
  class InvestCrimeArrest < ActiveRecord::Base
    belongs_to :invest_crime, :class_name => 'FixRemainingInvestJoins::InvestCrime'
    belongs_to :invest_arrest, :class_name => 'FixRemainingInvestJoins::InvestArrest'
  end
  
  class InvestCrimeCase < ActiveRecord::Base
    belongs_to :invest_crime, :class_name => 'FixRemainingInvestJoins::InvestCrime'
    belongs_to :invest_case, :class_name => 'FixRemainingInvestJoins::InvestCase'
  end
  
  class InvestCrimeContact < ActiveRecord::Base
    belongs_to :invest_crime, :class_name => 'FixRemainingInvestJoins::InvestCrime'
    belongs_to :invest_contact, :class_name => 'FixRemainingInvestJoins::InvestContact'
  end
  
  class InvestCrimeCriminal < ActiveRecord::Base
    belongs_to :invest_crime, :class_name => 'FixRemainingInvestJoins::InvestCrime'
    belongs_to :invest_criminal, :class_name => 'FixRemainingInvestJoins::InvestCriminal'
  end
  
  class InvestCrimeEvidence < ActiveRecord::Base
    belongs_to :invest_crime, :class_name => 'FixRemainingInvestJoins::InvestCrime'
    belongs_to :invest_evidence, :class_name => 'FixRemainingInvestJoins::InvestEvidence'
  end
  
  class InvestCrimePhoto < ActiveRecord::Base
    belongs_to :invest_crime, :class_name => 'FixRemainingInvestJoins::InvestCrime'
    belongs_to :invest_photo, :class_name => 'FixRemainingInvestJoins::InvestPhoto'
  end
  
  class InvestCrimeStolen < ActiveRecord::Base
    belongs_to :invest_crime, :class_name => 'FixRemainingInvestJoins::InvestCrime'
    belongs_to :invest_stolen, :class_name => 'FixRemainingInvestJoins::InvestStolen'
  end
  
  class InvestCrimeVehicle < ActiveRecord::Base
    belongs_to :invest_crime, :class_name => 'FixRemainingInvestJoins::InvestCrime'
    belongs_to :invest_vehicle, :class_name => 'FixRemainingInvestJoins::InvestVehicle'
  end
  
  class InvestCriminalArrest < ActiveRecord::Base
    belongs_to :invest_criminal, :class_name => 'FixRemainingInvestJoins::InvestCriminal'
    belongs_to :invest_arrest, :class_name => 'FixRemainingInvestJoins::InvestArrest'
  end
  
  class InvestCriminalEvidence < ActiveRecord::Base
    belongs_to :invest_criminal, :class_name => 'FixRemainingInvestJoins::InvestCriminal'
    belongs_to :invest_evidence, :class_name => 'FixRemainingInvestJoins::InvestEvidence'
  end
  
  class InvestCriminalPhoto < ActiveRecord::Base
    belongs_to :invest_criminal, :class_name => 'FixRemainingInvestJoins::InvestCriminal'
    belongs_to :invest_photo, :class_name => 'FixRemainingInvestJoins::InvestPhoto'
  end
  
  class InvestCriminalStolen < ActiveRecord::Base
    belongs_to :invest_criminal, :class_name => 'FixRemainingInvestJoins::InvestCriminal'
    belongs_to :invest_stolen, :class_name => 'FixRemainingInvestJoins::InvestStolen'
  end
  
  class InvestCriminalVehicle < ActiveRecord::Base
    belongs_to :invest_criminal, :class_name => 'FixRemainingInvestJoins::InvestCriminal'
    belongs_to :invest_vehicle, :class_name => 'FixRemainingInvestJoins::InvestVehicle'
  end
  
  class InvestReportArrest < ActiveRecord::Base
    belongs_to :invest_report, :class_name => 'FixRemainingInvestJoins::InvestReport'
    belongs_to :invest_arrest, :class_name => 'FixRemainingInvestJoins::InvestArrest'
  end
  
  class InvestReportCriminal < ActiveRecord::Base
    belongs_to :invest_report, :class_name => 'FixRemainingInvestJoins::InvestReport'
    belongs_to :invest_criminal, :class_name => 'FixRemainingInvestJoins::InvestCriminal'
  end
  
  class InvestReportPhoto < ActiveRecord::Base
    belongs_to :invest_report, :class_name => 'FixRemainingInvestJoins::InvestReport'
    belongs_to :invest_photo, :class_name => 'FixRemainingInvestJoins::InvestPhoto'
  end
  
  class InvestReportVehicle < ActiveRecord::Base
    belongs_to :invest_report, :class_name => 'FixRemainingInvestJoins::InvestReport'
    belongs_to :invest_vehicle, :class_name => 'FixRemainingInvestJoins::InvestVehicle'
  end
  
  class InvestVehiclePhoto < ActiveRecord::Base
    belongs_to :invest_vehicle, :class_name => 'FixRemainingInvestJoins::InvestVehicle'
    belongs_to :invest_photo, :class_name => 'FixRemainingInvestJoins::InvestPhoto'
  end
  
  def up
    ### convert invest_contacts back to contact join (bug#313)
    # rename invest_contacts table temporarily
    rename_table :invest_contacts, :invests_contacts
    rename_table :invest_contacts_investigations, :investigations_invests_contacts
    
    create_table :invest_contacts, :force => true do |t|
      t.integer :investigation_id
      t.integer :contact_id
      t.timestamps
    end
    add_index :invest_contacts, :investigation_id
    add_index :invest_contacts, :contact_id
    
    create_table :invest_crime_contacts, :force => true do |t|
      t.integer :invest_crime_id
      t.integer :invest_contact_id
      t.timestamps
    end
    add_index :invest_crime_contacts, :invest_crime_id
    add_index :invest_crime_contacts, :invest_contact_id
    
    FixRemainingInvestJoins::InvestsContact.reset_column_information
    FixRemainingInvestJoins::InvestContact.reset_column_information
    FixRemainingInvestJoins::InvestCrimeContact.reset_column_information
    FixRemainingInvestJoins::InvestsContact.all.each do |ic|
      if c = FixRemainingInvestJoins::Contact.where(:fullname => ic.fullname, :business => ic.business).first
        ic.investigations.each{|i| i.contacts << c}
      end
    end
    
    # drop old invest_contacts table and join
    drop_table :invests_contacts
    drop_table :investigations_invests_contacts
    
    ### investigation joins need fixing (bug#317)
    # invest_arrest
    create_table :invest_crime_arrests, :force => true do |t|
      t.integer :invest_crime_id
      t.integer :invest_arrest_id
      t.timestamps
    end
    add_index :invest_crime_arrests, :invest_crime_id
    add_index :invest_crime_arrests, :invest_arrest_id
    
    create_table :invest_criminal_arrests, :force => true do |t|
      t.integer :invest_criminal_id
      t.integer :invest_arrest_id
      t.timestamps
    end
    add_index :invest_criminal_arrests, :invest_criminal_id
    add_index :invest_criminal_arrests, :invest_arrest_id
    
    create_table :invest_report_arrests, :force => true do |t|
      t.integer :invest_report_id
      t.integer :invest_arrest_id
      t.timestamps
    end
    add_index :invest_report_arrests, :invest_report_id
    add_index :invest_report_arrests, :invest_arrest_id
    
    FixRemainingInvestJoins::InvestCrimeArrest.reset_column_information
    FixRemainingInvestJoins::InvestCriminalArrest.reset_column_information
    FixRemainingInvestJoins::InvestReportArrest.reset_column_information
    FixRemainingInvestJoins::InvestArrest.all.each do |ia|
      unless ia.invest_crime.nil?
        ia.invest_crime.invest_arrests << ia
      end
      unless ia.invest_criminal.nil?
        ia.invest_criminal.invest_arrests << ia
      end
      unless ia.invest_report.nil?
        ia.invest_report.invest_arrests << ia
      end
    end
    
    remove_column :invest_arrests, :invest_crime_id
    remove_column :invest_arrests, :invest_criminal_id
    remove_column :invest_arrests, :invest_report_id
    
    # evidence
    create_table :invest_crime_evidences, :force => true do |t|
      t.integer :invest_crime_id
      t.integer :invest_evidence_id
      t.timestamps
    end
    add_index :invest_crime_evidences, :invest_crime_id
    add_index :invest_crime_evidences, :invest_evidence_id
    
    create_table :invest_criminal_evidences, :force => true do |t|
      t.integer :invest_criminal_id
      t.integer :invest_evidence_id
      t.timestamps
    end
    add_index :invest_criminal_evidences, :invest_criminal_id
    add_index :invest_criminal_evidences, :invest_evidence_id
    
    FixRemainingInvestJoins::InvestCrimeEvidence.reset_column_information
    FixRemainingInvestJoins::InvestCriminalEvidence.reset_column_information
    FixRemainingInvestJoins::InvestEvidence.all.each do |ie|
      unless ie.invest_crime.nil?
        ie.invest_crime.invest_evidences << ie
      end
      unless ie.invest_criminal.nil?
        ie.invest_criminal.invest_evidences << ie
      end
    end
    
    remove_column :invest_evidences, :invest_crime_id
    remove_column :invest_evidences, :invest_criminal_id
    
    # stolen
    create_table :invest_crime_stolens, :force => true do |t|
      t.integer :invest_crime_id
      t.integer :invest_stolen_id
      t.timestamps
    end
    add_index :invest_crime_stolens, :invest_crime_id
    add_index :invest_crime_stolens, :invest_stolen_id
    
    create_table :invest_criminal_stolens, :force => true do |t|
      t.integer :invest_criminal_id
      t.integer :invest_stolen_id
      t.timestamps
    end
    add_index :invest_criminal_stolens, :invest_criminal_id
    add_index :invest_criminal_stolens, :invest_stolen_id
    
    FixRemainingInvestJoins::InvestCrimeStolen.reset_column_information
    FixRemainingInvestJoins::InvestCriminalStolen.reset_column_information
    FixRemainingInvestJoins::InvestStolen.all.each do |ist|
      unless ist.invest_crime.nil?
        ist.invest_crime.invest_stolens << ist
      end
      unless ist.invest_criminal.nil?
        ist.invest_criminal.invest_stolens << ist
      end
    end
    
    remove_column :invest_stolens, :invest_crime_id
    remove_column :invest_stolens, :invest_criminal_id
    
    ### re-evaluate investigation joins (bug#295)
    # invest_cases
    remove_column :invest_cases, :created_by
    remove_column :invest_cases, :updated_by
    remove_column :invest_cases, :remarks
    remove_column :invest_cases, :private
    remove_column :invest_cases, :owned_by
    add_column :invest_cases, :call_id, :integer
    
    create_table :invest_crime_cases, :force => true do |t|
      t.integer :invest_crime_id
      t.integer :invest_case_id
      t.timestamps
    end
    add_index :invest_crime_cases, :invest_crime_id
    add_index :invest_crime_cases, :invest_case_id
    
    FixRemainingInvestJoins::InvestCase.reset_column_information
    FixRemainingInvestJoins::InvestCrimeCase.reset_column_information
    FixRemainingInvestJoins::InvestCase.all.each do |ic|
      if c = Call.where(:case_no => ic.case_no).first
        ic.update_attribute(:call_id, c.id)
        unless ic.invest_crime.nil?
          ic.invest_crime.invest_cases << ic
        end
      end
    end
    
    remove_column :invest_cases, :case_no
    remove_column :invest_cases, :invest_crime_id
    
    # invest_criminals
    create_table :invest_crime_criminals, :force => true do |t|
      t.integer :invest_crime_id
      t.integer :invest_criminal_id
      t.timestamps
    end
    add_index :invest_crime_criminals, :invest_crime_id
    add_index :invest_crime_criminals, :invest_criminal_id
    
    create_table :invest_report_criminals, :force => true do |t|
      t.integer :invest_report_id
      t.integer :invest_criminal_id
      t.timestamps
    end
    add_index :invest_report_criminals, :invest_report_id
    add_index :invest_report_criminals, :invest_criminal_id
    
    FixRemainingInvestJoins::InvestCrimeCriminal.reset_column_information
    FixRemainingInvestJoins::InvestReportCriminal.reset_column_information
    FixRemainingInvestJoins::InvestCriminal.all.each do |c|
      unless c.invest_crime.nil?
        c.invest_crime.criminals << c
      end
      c.invest_reports.each{|r| r.criminals << c}
    end
    
    remove_column :invest_criminals, :invest_crime_id
    drop_table :report_criminals
    
    # invest_vehicles
    create_table :invest_crime_vehicles, :force => true do |t|
      t.integer :invest_crime_id
      t.integer :invest_vehicle_id
      t.timestamps
    end
    add_index :invest_crime_vehicles, :invest_crime_id
    add_index :invest_crime_vehicles, :invest_vehicle_id
    
    create_table :invest_criminal_vehicles, :force => true do |t|
      t.integer :invest_criminal_id
      t.integer :invest_vehicle_id
      t.timestamps
    end
    add_index :invest_criminal_vehicles, :invest_criminal_id
    add_index :invest_criminal_vehicles, :invest_vehicle_id
    
    create_table :invest_report_vehicles, :force => true do |t|
      t.integer :invest_report_id
      t.integer :invest_vehicle_id
      t.timestamps
    end
    add_index :invest_report_vehicles, :invest_report_id
    add_index :invest_report_vehicles, :invest_vehicle_id
    
    FixRemainingInvestJoins::InvestCrimeVehicle.reset_column_information
    FixRemainingInvestJoins::InvestCriminalVehicle.reset_column_information
    FixRemainingInvestJoins::InvestReportVehicle.reset_column_information
    FixRemainingInvestJoins::InvestVehicle.all.each do |v|
      unless v.invest_crime.nil?
        v.invest_crime.vehicles << v
      end
      unless v.invest_criminal.nil?
        v.invest_criminal.vehicles << v
      end
      v.invest_reports.each{|r| r.vehicles << v}
    end
    
    remove_column :invest_vehicles, :invest_crime_id
    remove_column :invest_vehicles, :invest_criminal_id
    drop_table :report_vehicles
    
    ### fix invest photo joins (bug#294)
    create_table :invest_crime_photos, :force => true do |t|
      t.integer :invest_crime_id
      t.integer :invest_photo_id
      t.timestamps
    end
    add_index :invest_crime_photos, :invest_crime_id
    add_index :invest_crime_photos, :invest_photo_id
    
    create_table :invest_criminal_photos, :force => true do |t|
      t.integer :invest_criminal_id
      t.integer :invest_photo_id
      t.timestamps
    end
    add_index :invest_criminal_photos, :invest_criminal_id
    add_index :invest_criminal_photos, :invest_photo_id
    
    create_table :invest_vehicle_photos, :force => true do |t|
      t.integer :invest_vehicle_id
      t.integer :invest_photo_id
      t.timestamps
    end
    add_index :invest_vehicle_photos, :invest_vehicle_id
    add_index :invest_vehicle_photos, :invest_photo_id
    
    create_table :invest_report_photos, :force => true do |t|
      t.integer :invest_report_id
      t.integer :invest_photo_id
      t.timestamps
    end
    add_index :invest_report_photos, :invest_report_id
    add_index :invest_report_photos, :invest_photo_id
    
    FixRemainingInvestJoins::InvestCrimePhoto.reset_column_information
    FixRemainingInvestJoins::InvestCriminalPhoto.reset_column_information
    FixRemainingInvestJoins::InvestVehiclePhoto.reset_column_information
    FixRemainingInvestJoins::InvestReportPhoto.reset_column_information
    FixRemainingInvestJoins::InvestPhoto.all.each do |p|
      unless p.invest_crime.nil?
        p.invest_crime.photos << p
      end
      unless p.invest_criminal.nil?
        p.invest_criminal.photos << p
      end
      unless p.invest_vehicle.nil?
        p.invest_vehicle.photos << p
      end
      p.invest_reports.each{|r| r.photos << p}
    end
    
    remove_column :invest_photos, :invest_crime_id
    remove_column :invest_photos, :invest_criminal_id
    remove_column :invest_photos, :invest_vehicle_id
    drop_table :report_photos
    
    ### make notes polymorphic (bug#318)
    add_column :invest_notes, :invest_record_id, :integer
    add_column :invest_notes, :invest_record_type, :string
    
    FixRemainingInvestJoins::InvestNote.reset_column_information
    FixRemainingInvestJoins::InvestNote.all.each do |n|
      n.invest_record_id = n.investigation_id
      n.invest_record_type = 'Investigation'
      n.save
    end
    remove_column :invest_notes, :investigation_id
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
