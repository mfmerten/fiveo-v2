class RemoveOfficerIdFromArrestOfficers < ActiveRecord::Migration
  def up
    remove_column :arrest_officers, :officer_id
  end
  
  def down
    add_column :arrest_officers, :officer_id, :integer
    add_index :arrest_officers, :officer_id
  end
end
