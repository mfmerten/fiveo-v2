class AddCourtCosts < ActiveRecord::Migration
  class Option < ActiveRecord::Base
    OptionTypes = {
      16 => 'Court Costs',
      30 => 'Judge',
      32 => 'Pawn Company',
      33 => 'Pawn Ticket Type',
      34 => 'Pawn Type',
      38 => 'Probation Status',
      39 => 'Probation Type',
      41 => 'Release Reason',
      44 => 'Signal Code',
      46 => 'Subject Type',
      48 => 'Transfer Type',
      49 => 'Trial Result',
      50 => 'Vehicle Type',
      51 => 'Warrant Disposition'
    }.freeze
  end
  class CourtCost < ActiveRecord::Base
  end
  
  def up
    create_table :court_costs, :force => true do |t|
      t.string :name
      t.string :category
      t.decimal :amount, :precision => 12, :scale => 2, :default => 0.0
      t.integer :created_by, :default => 1
      t.integer :updated_by, :default => 1
      t.timestamps
    end
    add_index :court_costs, :created_by
    add_index :court_costs, :updated_by
    
    AddCourtCosts::CourtCost.reset_column_information
    AddCourtCosts::Option.where(:option_type => 16).all.each do |o|
      unless o.disabled?
        if o.long_name =~ /^Traffic/
          category = 'Traffic'
        elsif o.long_name =~ /Felony$/
          category = 'Felony'
        elsif o.long_name =~ /Misd.$/
          category = 'Misdemeanor'
        elsif o.long_name =~ /^DWI/
          category = 'DWI'
        elsif o.long_name =~ /^Wildlife/
          category = "Wildlife"
        else
          category = "Other"
        end
        AddCourtCosts::CourtCost.create(:name => o.long_name, :category => category, :amount => BigDecimal.new(o.short_name, 2))
      end
      o.destroy
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
