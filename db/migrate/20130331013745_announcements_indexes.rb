class AnnouncementsIndexes < ActiveRecord::Migration
  def up
    add_index :announcements, :updated_at
  end

  def down
    remove_index :announcements, :updated_at
  end
end
