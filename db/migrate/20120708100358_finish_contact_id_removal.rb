# Rails3 project
# finish removing contact_id fields from models
class FinishContactIdRemoval < ActiveRecord::Migration
  class Charge < ActiveRecord::Base
  end
  class Warrant < ActiveRecord::Base
  end
  class Contact < ActiveRecord::Base
  end

  def up
    # bookings
    remove_column :bookings, :booking_officer_id
    remove_column :bookings, :release_officer_id

    # calls
    remove_column :calls, :received_by_id
    remove_column :calls, :dispatcher_id
    remove_column :calls, :detective_id

    # call units
    remove_column :call_units, :officer_id

    # citations
    remove_column :citations, :officer_id

    # commissaries
    remove_column :commissaries, :officer_id

    # charge - add agency field, convert from agency_id then remove agency_id field
    add_column :charges, :agency, :string
    FinishContactIdRemoval::Charge.reset_column_information
    FinishContactIdRemoval::Charge.all.each do |c|
      if c.agency_id?
        a = FinishContactIdRemoval::Contact.find_by_id(c.agency_id)
        unless a.nil?
          c.agency = a.fullname
          c.save
        end
      end
    end
    remove_column :charges, :agency_id

    # warrant - add jurisdiction field, convert from jurisdiction_id then remove jurisdiction_id field.
    add_column :warrants, :jurisdiction, :string
    add_column :warrants, :phone1, :string
    add_column :warrants, :phone2, :string
    add_column :warrants, :phone3, :string
    add_column :warrants, :phone1_type_id, :integer
    add_column :warrants, :phone2_type_id, :integer
    add_column :warrants, :phone3_type_id, :integer
    add_column :warrants, :phone1_unlisted, :boolean
    add_column :warrants, :phone2_unlisted, :boolean
    add_column :warrants, :phone3_unlisted, :boolean
    add_column :warrants, :phone1_note, :string
    add_column :warrants, :phone2_note, :string
    add_column :warrants, :phone3_note, :string
    add_index :warrants, :phone1_type_id
    add_index :warrants, :phone2_type_id
    add_index :warrants, :phone3_type_id
    FinishContactIdRemoval::Warrant.reset_column_information
    FinishContactIdRemoval::Warrant.all.each do |w|
      if w.jurisdiction_id?
        a = FinishContactIdRemoval::Contact.find_by_id(w.jurisdiction_id)
        unless a.nil?
          w.jurisdiction = a.fullname
          w.phone1 = a.phone1
          w.phone2 = a.phone2
          w.phone3 = a.phone3
          w.phone1_type_id = a.phone1_type_id
          w.phone2_type_id = a.phone2_type_id
          w.phone3_type_id = a.phone3_type_id
          w.phone1_unlisted = a.phone1_unlisted
          w.phone2_unlisted = a.phone2_unlisted
          w.phone3_unlisted = a.phone3_unlisted
          w.phone1_note = a.phone1_note
          w.phone2_note = a.phone2_note
          w.phone3_note = a.phone3_note
          w.save
        end
      end
    end
    remove_column :warrants, :jurisdiction_id

    # holds
    remove_column :holds, :hold_by_id
    remove_column :holds, :cleared_by_id
    remove_column :holds, :agency_id
    remove_column :holds, :transfer_officer_id
    
    # evidence location
    remove_column :evidence_locations, :officer_id
    
    # forbids
    remove_column :forbids, :receiving_officer_id
    remove_column :forbids, :serving_officer_id
    
    # medicals
    remove_column :medicals, :officer_id

    # offense
    remove_column :offenses, :officer_id

    # offense photos
    remove_column :offense_photos, :taken_by_id

    # papers
    remove_column :papers, :officer_id

    # paper payments
    remove_column :paper_payments, :officer_id

    # patrols
    remove_column :patrols, :officer_id

    # probations
    remove_column :probations, :probation_officer_id

    # probation payments
    remove_column :probation_payments, :officer_id

    # transfers
    remove_column :transfers, :from_agency_id
    remove_column :transfers, :to_agency_id
    remove_column :transfers, :from_officer_id
    remove_column :transfers, :to_officer_id

    # transports
    remove_column :transports, :officer1_id
    remove_column :transports, :officer2_id
    remove_column :transports, :from_id
    remove_column :transports, :to_id
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
