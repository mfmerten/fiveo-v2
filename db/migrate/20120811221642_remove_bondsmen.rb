class RemoveBondsmen < ActiveRecord::Migration
  class Contact < ActiveRecord::Base
    before_validation :fix_name
    # uses the name parser to put names in last, first middle order unless
    # business is checked
    def fix_name
      unless business?
        n = Namae::Name.parse(fullname)
        self.fullname = n.sort_order
      end
      return true
    end
  end
  class Bond < ActiveRecord::Base
    belongs_to :bondsman, :class_name => 'RemoveBondsmen::Bondsman'
  end
  class Bondsman < ActiveRecord::Base
    has_many :bonds, :class_name => 'RemoveBondsmen::Bond'
  end
  def up
    # add needed fields
    add_column :contacts, :bondsman, :boolean
    add_column :contacts, :remarks, :text
    add_column :bonds, :contact_id, :integer
    
    # make the switch
    RemoveBondsmen::Contact.reset_column_information
    RemoveBondsmen::Bond.reset_column_information
    RemoveBondsmen::Bondsman.all.each do |bm|
      attrib = bm.attributes
      attrib.delete("id")
      attrib["notes"] = attrib["company"]
      attrib.delete("company")
      attrib["bondsman"] = true
      if attrib["remarks"].blank?
        attrib["remarks"] = "Added during Bondsmen Import"
      else
        attrib["remarks"] = "Added during Bondsmen Import -- " + attrib["remarks"]
      end
      attrib["updated_by"] = 1
      attrib["updated_at"] = Time.now
      attrib["active"] = !attrib["disabled"]
      attrib.delete('disabled')
      attrib["title"] = "Bondsman"
      attrib["fullname"] = attrib["name"]
      attrib.delete('name')
      c = RemoveBondsmen::Contact.create(attrib)
      bm.bonds.each{|b| b.update_attribute(:contact_id, c.id)}
    end
    
    # remove the old bondsman_id and rename contact_id to bondsman_id
    remove_column :bonds, :bondsman_id
    rename_column :bonds, :contact_id, :bondsman_id
    add_index :bonds, :bondsman_id
    
    # drop the bondsmen table
    drop_table :bondsmen
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
