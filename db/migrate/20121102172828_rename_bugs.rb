class RenameBugs < ActiveRecord::Migration
  class Bug < ActiveRecord::Base
  end
  class SupportTicketComment < ActiveRecord::Base
  end
  class Message < ActiveRecord::Base
  end
  
  def up
    RenameBugs::Bug.all.each do |b|
      txt = b.details.gsub(/\bDuplicate Bug\b/,'Duplicate').gsub(/\bbug report\b/,'support ticket').gsub(/\bbug\b/,'ticket').gsub(/\bBug\b/,'Ticket')
      if txt != b.details
        b.details = txt
        b.updated_by = 1
        b.save
      end
    end
    RenameBugs::SupportTicketComment.all.each do |c|
      txt = c.comments.gsub(/\bbug report\b/,'support ticket').gsub(/\bBug Report\b/,'Support Ticket').gsub(/\bbug\b/,'ticket').gsub(/\bBug\b/,'Ticket')
      if txt != c.comments
        c.comments = txt
        c.updated_by = 1
        c.save
      end
    end
    RenameBugs::Message.all.each do |m|
      txt = m.subject.gsub(/\bbug report\b/,'support ticket').gsub(/\bBug Report\b/,'Support Ticket').gsub(/\bbug\b/,'ticket').gsub(/\bBug\b/,'Ticket')
      if txt != m.subject
        m.subject = txt
        m.save
      end
    end
    
    add_column :bugs, :support_requested, :boolean
    add_column :bugs, :bug_report_filed, :boolean
    add_column :site_configurations, :bug_email, :string, :default => 'bugs@midlaketech.com'
    add_column :site_configurations, :support_email, :string, :default => 'ticket@midlaketech.com'
  end

  def down
    remove_column :bugs, :support_requested
    remove_column :bugs, :bug_report_filed
    remove_column :site_configurations, :bug_email
    remove_column :site_configurations, :support_email
  end
end
