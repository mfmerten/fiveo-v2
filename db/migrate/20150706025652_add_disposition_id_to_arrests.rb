class AddDispositionIdToArrests < ActiveRecord::Migration
  class Arrest < ActiveRecord::Base
    belongs_to :disposition, class_name: "AddDispositionIdToArrests::Disposition"
  end
  class Disposition < ActiveRecord::Base
    has_many :arrests, class_name: "AddDispositionIdToArrests::Arrest"
  end
  def up
    add_column :arrests, :disposition_id, :integer
    add_index :arrests, :disposition_id
    add_foreign_key :arrests, :dispositions
    
    say_with_time "Updating Arrests..." do
      Arrest.find_each do |a|
        if d = Disposition.find_by(atn: a.atn)
          a.disposition_id = d.id
        end
      end
    end
  end
  
  def down
    remove_foreign_key :arrests, :dispositions
    remove_index :arrests, column: :disposition_id
    remove_column :arrests, :disposition_id
  end
end
