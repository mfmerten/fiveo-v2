class FixInvestigationPeopleJoins < ActiveRecord::Migration
  
  class Person < ActiveRecord::Base
    has_and_belongs_to_many :icrimes_as_witness, :class_name => 'FixInvestigationPeopleJoins::InvestCrime', :join_table => 'crime_witnesses'
    has_and_belongs_to_many :icrimes_as_victim, :class_name => 'FixInvestigationPeopleJoins::InvestCrime', :join_table => 'crime_victims'
    has_and_belongs_to_many :icrimes_as_source, :class_name => 'FixInvestigationPeopleJoins::InvestCrime', :join_table => 'invest_crime_sources'
    has_and_belongs_to_many :iinvests_as_witness, :class_name => 'FixInvestigationPeopleJoins::Investigation', :join_table => 'invests_witnesses'
    has_and_belongs_to_many :iinvests_as_victim, :class_name => 'FixInvestigationPeopleJoins::Investigation', :join_table => 'invests_victims'
    has_and_belongs_to_many :iinvests_as_source, :class_name => 'FixInvestigationPeopleJoins::Investigation', :join_table => 'invests_sources'
    has_and_belongs_to_many :ireports_as_witness, :class_name => 'FixInvestigationPeopleJoins::InvestReport', :join_table => 'report_witnesses'
    has_and_belongs_to_many :ireports_as_victim, :class_name => 'FixInvestigationPeopleJoins::InvestReport', :join_table => 'report_victims'
    has_many :invest_witnesses, :class_name => 'FixInvestigationPeopleJoins::InvestWitness'
    has_many :witness_investigations, :through => :invest_witnesses, :source => :investigation
    has_many :witness_crimes, :through => :invest_witnesses, :source => :invest_crime
    has_many :witness_reports, :through => :invest_witnesses, :source => :invest_report
    has_many :invest_victims, :class_name => 'FixInvestigationPeopleJoins::InvestVictim'
    has_many :victim_investigations, :through => :invest_victims, :source => :investigation
    has_many :victim_crimes, :through => :invest_victims, :source => :invest_crime
    has_many :victim_reports, :through => :invest_victims, :source => :invest_report
    has_many :invest_sources, :class_name => 'FixInvestigationPeopleJoins::InvestSource'
    has_many :source_investigations, :through => :invest_sources, :source => :investigation
    has_many :source_crimes, :through => :invest_sources, :source => :invest_crime
  end
  
  class Investigation < ActiveRecord::Base
    has_and_belongs_to_many :victims, :class_name => 'FixInvestigationPeopleJoins::Person', :join_table => 'invests_victims'
    has_and_belongs_to_many :witnesses, :class_name => 'FixInvestigationPeopleJoins::Person', :join_table => 'invests_witnesses'
    has_and_belongs_to_many :sources, :class_name => 'FixInvestigationPeopleJoins::Person', :join_table => 'invests_sources'
    has_many :invest_victims, :class_name => 'FixInvestigationPeopleJoins::InvestVictim'
    has_many :victim_crimes, :through => :invest_victims, :source => :crimes
    has_many :victim_reports, :through => :invest_victims, :source => :reports
    has_many :xvictims, :through => :invest_victims, :source => :person
    has_many :invest_witnesses, :class_name => 'FixInvestigationPeopleJoins::InvestWitness'
    has_many :witness_crimes, :through => :invest_witnesses, :source => :crimes
    has_many :witness_reports, :through => :invest_witnesses, :source => :reports
    has_many :xwitnesses, :through => :invest_witnesses, :source => :person
    has_many :invest_sources, :class_name => 'FixInvestigationPeopleJoins::InvestSource'
    has_many :xsources, :through => :invest_sources, :source => :person
    has_many :crimes, :class_name => 'FixInvestigationPeopleJoins::InvestCrime'
    has_many :reports, :class_name => 'FixInvestigationPeopleJoins::InvestReport'
  end
  
  class InvestCrime < ActiveRecord::Base
    has_and_belongs_to_many :sources, :class_name => 'FixInvestigationPeopleJoins::Person', :join_table => 'invest_crime_sources'
    has_and_belongs_to_many :victims, :class_name => 'FixInvestigationPeopleJoins::Person', :join_table => 'crime_victims'
    has_and_belongs_to_many :witnesses, :class_name => 'FixInvestigationPeopleJoins::Person', :join_table => 'crime_witnesses'
    has_many :invest_crime_victims, :class_name => 'FixInvestigationPeopleJoins::InvestCrimeVictim'
    has_many :invest_victims, :through => :invest_crime_victims
    has_many :xvictims, :through => :invest_victims, :source => :person
    has_many :invest_crime_witnesses, :class_name => 'FixInvestigationPeopleJoins::InvestCrimeWitness'
    has_many :invest_witnesses, :through => :invest_crime_witnesses
    has_many :xwitnesses, :through => :invest_witnesses, :source => :person
    belongs_to :investigation, :class_name => 'FixInvestigationPeopleJoins::Investigation'
  end
  
  class InvestReport < ActiveRecord::Base
    has_and_belongs_to_many :victims, :class_name => 'FixInvestigationPeopleJoins::Person', :join_table => 'report_victims'
    has_and_belongs_to_many :witnesses, :class_name => 'FixInvestigationPeopleJoins::Person', :join_table => 'report_witnesses'
    has_many :invest_report_victims, :class_name => 'FixInvestigationPeopleJoins::InvestReportVictim'
    has_many :invest_victims, :through => :invest_report_victims
    has_many :xvictims, :through => :invest_victims, :source => :person
    has_many :invest_report_witnesses, :class_name => 'FixInvestigationPeopleJoins::InvestReportWitness'
    has_many :invest_witnesses, :through => :invest_report_witnesses
    has_many :xwitnesses, :through => :invest_witnesses, :source => :person
    belongs_to :investigation, :class_name => 'FixInvestigationPeopleJoins::Investigation'
  end
  
  class InvestWitness < ActiveRecord::Base
    belongs_to :person, :class_name => 'FixInvestigationPeopleJoins::Person'
    belongs_to :investigation, :class_name => 'FixInvestigationPeopleJoins::Investigation'
    has_many :invest_crime_witnesses, :class_name => 'FixInvestigationPeopleJoins::InvestCrimeWitness'
    has_many :crimes, :through => :invest_crime_witnesses, :source => :invest_crime
    has_many :invest_report_witnesses, :class_name => 'FixInvestigationPeopleJoins::InvestReportWitness'
    has_many :reports, :through => :invest_report_witnesses, :source => :invest_report
  end
  
  class InvestVictim < ActiveRecord::Base
    belongs_to :person, :class_name => 'FixInvestigationPeopleJoins::Person'
    belongs_to :investigation, :class_name => 'FixInvestigationPeopleJoins::Investigation'
    has_many :invest_crime_victims, :class_name => 'FixInvestigationPeopleJoins::InvestCrimeVictim'
    has_many :crimes, :through => :invest_crime_victims, :source => :invest_crime
    has_many :invest_report_victims, :class_name => 'FixInvestigationPeopleJoins::InvestReportVictim'
    has_many :reports, :through => :invest_report_victims, :source => :invest_report
  end
  
  class InvestSource < ActiveRecord::Base
    belongs_to :person, :class_name => 'FixInvestigationPeopleJoins::Person'
    belongs_to :investigation, :class_name => 'FixInvestigationPeopleJoins::Investigation'
  end
  
  class InvestCrimeWitness < ActiveRecord::Base
    belongs_to :invest_witness, :class_name => 'FixInvestigationPeopleJoins::InvestWitness'
    belongs_to :invest_crime, :class_name => 'FixInvestigationPeopleJoins::InvestCrime'
  end
  
  class InvestReportWitness < ActiveRecord::Base
    belongs_to :invest_witness, :class_name => 'FixInvestigationPeopleJoins::InvestWitness'
    belongs_to :invest_report, :class_name => 'FixInvestigationPeopleJoins::InvestReport'
  end
  
  class InvestCrimeVictim < ActiveRecord::Base
    belongs_to :invest_victim, :class_name => 'FixInvestigationPeopleJoins::InvestVictim'
    belongs_to :invest_crime, :class_name => 'FixInvestigationPeopleJoins::InvestCrime'
  end
  
  class InvestReportVictim < ActiveRecord::Base
    belongs_to :invest_victim, :class_name => 'FixInvestigationPeopleJoins::InvestVictim'
    belongs_to :invest_report, :class_name => 'FixInvestigationPeopleJoins::InvestReport'
  end
  
  def up
    # rename clashing join tables
    remove_index :invest_victims, :person_id
    remove_index :invest_victims, :investigation_id
    rename_table :invest_victims, :invests_victims
    add_index :invests_victims, :person_id
    add_index :invests_victims, :investigation_id
    
    remove_index :invest_witnesses, :person_id
    remove_index :invest_witnesses, :investigation_id
    rename_table :invest_witnesses, :invests_witnesses
    add_index :invests_witnesses, :person_id
    add_index :invests_witnesses, :investigation_id
    
    remove_index :invest_sources, :person_id
    remove_index :invest_sources, :investigation_id
    rename_table :invest_sources, :invests_sources
    add_index :invests_sources, :person_id
    add_index :invests_sources, :investigation_id
    
    # create the new join model tables
    create_table :invest_victims, :force => true do |t|
      t.integer :person_id
      t.integer :investigation_id
      t.timestamps
    end
    add_index :invest_victims, :person_id
    add_index :invest_victims, :investigation_id
    
    create_table :invest_witnesses, :force => true do |t|
      t.integer :person_id
      t.integer :investigation_id
      t.timestamps
    end
    add_index :invest_witnesses, :person_id
    add_index :invest_witnesses, :investigation_id
    
    create_table :invest_sources, :force => true do |t|
      t.integer :person_id
      t.integer :investigation_id
      t.timestamps
    end
    add_index :invest_sources, :person_id
    add_index :invest_sources, :investigation_id
    
    create_table :invest_crime_witnesses, :force => true do |t|
      t.integer :invest_crime_id
      t.integer :invest_witness_id
      t.timestamps
    end
    add_index :invest_crime_witnesses, :invest_crime_id
    add_index :invest_crime_witnesses, :invest_witness_id
    
    create_table :invest_report_witnesses, :force => true do |t|
      t.integer :invest_report_id
      t.integer :invest_witness_id
      t.timestamps
    end
    add_index :invest_report_witnesses, :invest_report_id
    add_index :invest_report_witnesses, :invest_witness_id
    
    create_table :invest_crime_victims, :force => true do |t|
      t.integer :invest_crime_id
      t.integer :invest_victim_id
      t.timestamps
    end
    add_index :invest_crime_victims, :invest_crime_id
    add_index :invest_crime_victims, :invest_victim_id
    
    create_table :invest_report_victims, :force => true do |t|
      t.integer :invest_report_id
      t.integer :invest_victim_id
      t.timestamps
    end
    add_index :invest_report_victims, :invest_report_id
    add_index :invest_report_victims, :invest_victim_id
    
    FixInvestigationPeopleJoins::InvestSource.reset_column_information
    FixInvestigationPeopleJoins::InvestWitness.reset_column_information
    FixInvestigationPeopleJoins::InvestVictim.reset_column_information
    FixInvestigationPeopleJoins::Investigation.reset_column_information
    FixInvestigationPeopleJoins::InvestCrime.reset_column_information
    FixInvestigationPeopleJoins::InvestReport.reset_column_information
    FixInvestigationPeopleJoins::Person.reset_column_information
    FixInvestigationPeopleJoins::InvestCrimeWitness.reset_column_information
    FixInvestigationPeopleJoins::InvestReportWitness.reset_column_information
    FixInvestigationPeopleJoins::InvestCrimeVictim.reset_column_information
    FixInvestigationPeopleJoins::InvestReportVictim.reset_column_information
    
    FixInvestigationPeopleJoins::Investigation.all.each do |i|
      i.victims.uniq.each do |v|
        iv = i.invest_victims.create(:person_id => v.id)
        i.crimes.each do |c|
          if c.victims.include?(v)
            iv.crimes << c
          end
        end
        i.reports.each do |r|
          if r.victims.include?(v)
            iv.reports << r
          end
        end
      end
      i.witnesses.uniq.each do |w|
        iw = i.invest_witnesses.create(:person_id => w.id)
        i.crimes.each do |c|
          if c.witnesses.include?(w)
            iw.crimes << c
          end
        end
        i.reports.each do |r|
          if r.witnesses.include?(w)
            iw.reports << r
          end
        end
      end
      i.xsource_ids = i.source_ids
    end
    
    drop_table :crime_witnesses
    drop_table :crime_victims
    drop_table :invest_crime_sources
    drop_table :invests_witnesses
    drop_table :invests_sources
    drop_table :invests_victims
    drop_table :report_victims
    drop_table :report_witnesses
  end

  def down
    create_table :crime_witnesses, :id => false, :force => true do |t|
      t.integer :person_id
      t.integer :invest_crime_id
    end
    add_index :crime_witnesses, :person_id
    add_index :crime_witnesses, :invest_crime_id
    
    create_table :crime_victims, :id => false, :force => true do |t|
      t.integer :person_id
      t.integer :invest_crime_id
    end
    add_index :crime_victims, :person_id
    add_index :crime_victims, :invest_crime_id
    
    create_table :invest_crime_sources, :id => false, :force => true do |t|
      t.integer :person_id
      t.integer :invest_crime_id
    end
    add_index :invest_crime_sources, :person_id
    add_index :invest_crime_sources, :invest_crime_id
    
    create_table :invests_witnesses, :id => false, :force => true do |t|
      t.integer :person_id
      t.integer :investigation_id
    end
    add_index :invests_witnesses, :person_id
    add_index :invests_witnesses, :investigation_id
    
    create_table :invests_sources, :id => false, :force => true do |t|
      t.integer :person_id
      t.integer :investigation_id
    end
    add_index :invests_sources, :person_id
    add_index :invests_sources, :investigation_id
    
    create_table :invests_victims, :id => false, :force => true do |t|
      t.integer :person_id
      t.integer :investigation_id
    end
    add_index :invests_victims, :person_id
    add_index :invests_victims, :investigation_id

    create_table :report_victims, :id => false, :force => true do |t|
      t.integer :person_id
      t.integer :invest_report_id
    end
    add_index :report_victims, :person_id
    add_index :report_victims, :invest_report_id
    
    create_table :report_witnesses, :id => false, :force => true do |t|
      t.integer :person_id
      t.integer :invest_report_id
    end
    add_index :report_witnesses, :person_id
    add_index :report_witnesses, :invest_report_id
    
    FixInvestigationPeopleJoins::InvestSource.reset_column_information
    FixInvestigationPeopleJoins::InvestWitness.reset_column_information
    FixInvestigationPeopleJoins::InvestVictim.reset_column_information
    FixInvestigationPeopleJoins::Investigation.reset_column_information
    FixInvestigationPeopleJoins::InvestCrime.reset_column_information
    FixInvestigationPeopleJoins::InvestReport.reset_column_information
    FixInvestigationPeopleJoins::Person.reset_column_information
    FixInvestigationPeopleJoins::InvestCrimeWitness.reset_column_information
    FixInvestigationPeopleJoins::InvestReportWitness.reset_column_information
    FixInvestigationPeopleJoins::InvestCrimeVictim.reset_column_information
    FixInvestigationPeopleJoins::InvestReportVictim.reset_column_information
    
    FixInvestigationPeopleJoins::Investigation.all.each do |i|
      i.source_ids = i.xsource_ids
      i.victim_ids = i.xvictim_ids
      i.witness_ids = i.xwitness_ids
    end
    FixInvestigationPeopleJoins::InvestCrime.all.each do |c|
      c.victim_ids = c.xvictim_ids
      c.witness_ids = c.xwitness_ids
    end
    FixInvestigationPeopleJoins::InvestReport.all.each do |r|
      r.victim_ids = r.xvictim_ids
      r.witness_ids = r.xwitness_ids
    end
    
    drop_table :invest_sources
    drop_table :invest_witnesses
    drop_table :invest_victims
    drop_table :invest_crime_witnesses
    drop_table :invest_report_witnesses
    drop_table :invest_crime_victims
    drop_table :invest_report_victims
    
    remove_index :invests_victims, :person_id
    remove_index :invests_victims, :investigation_id
    rename_table :invests_victims, :invest_victims
    add_index :invest_victims, :person_id
    add_index :invest_victims, :investigation_id
    
    remove_index :invests_witnesses, :person_id
    remove_index :invests_witnesses, :investigation_id
    rename_table :invests_witnesses, :invest_witnesses
    add_index :invest_witnesses, :person_id
    add_index :invest_witnesses, :investigation_id
  
    remove_index :invests_sources, :person_id
    remove_index :invests_sources, :investigation_id
    rename_table :invests_sources, :invest_sources
    add_index :invest_sources, :person_id
    add_index :invest_sources, :investigation_id
  end
end
