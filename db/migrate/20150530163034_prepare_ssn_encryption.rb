class PrepareSsnEncryption < ActiveRecord::Migration
  def up
    add_column :people, :encrypted_ssn, :string, default: ''
    rename_column :people, :ssn, :old_ssn
  end
  
  def down
    rename_column :people, :old_ssn, :ssn
    remove_column :people, :encrypted_ssn
  end
end
