class AddSupplementalToInvestReport < ActiveRecord::Migration
  def change
    add_column :invest_reports, :supplemental, :boolean
  end
end
