class NullsAndDefaultsTuvw < ActiveRecord::Migration
  class Transfer < ActiveRecord::Base
  end
  class Transport < ActiveRecord::Base
  end
  class User < ActiveRecord::Base
  end
  class UserGroup < ActiveRecord::Base
  end
  class Visitor < ActiveRecord::Base
  end
  class Warrant < ActiveRecord::Base
  end
  class WarrantExemption < ActiveRecord::Base
  end
  
  def up
    NullsAndDefaultsTuvw::Transfer.delete_all("booking_id IS NULL OR person_id IS NULL")
    change_column_null :transfers, :booking_id, false
    change_column_null :transfers, :person_id, false
    [:from_agency, :from_officer, :from_officer_unit, :from_officer_badge, :to_agency,
      :to_officer, :to_officer_unit, :to_officer_badge, :transfer_type].each do |col|
      NullsAndDefaultsTuvw::Transfer.update_all("#{col} = ''","#{col} IS NULL")
      change_column :transfers, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsTuvw::Transfer.update_all("remarks = ''","remarks IS NULL")
    change_column :transfers, :remarks, :text, :null => false, :default => ''
  
    NullsAndDefaultsTuvw::Transport.delete_all("person_id IS NULL")
    change_column_null :transports, :person_id, false
    [:officer1, :officer1_unit, :officer2, :officer2_unit, :from, :from_street,
      :from_city, :from_zip, :to, :to_street, :to_city, :to_zip, :officer1_badge,
      :officer2_badge, :from_state, :to_state].each do |col|
      NullsAndDefaultsTuvw::Transport.update_all("\"#{col}\" = ''","\"#{col}\" IS NULL")
      change_column :transports, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsTuvw::Transport.update_all("remarks = ''","remarks IS NULL")
    change_column :transports, :remarks, :text, :null => false, :default => ''
    [:begin_miles, :end_miles, :estimated_miles].each do |col|
      NullsAndDefaultsTuvw::Transport.update_all("#{col} = 0","#{col} IS NULL")
      change_column :transports, col, :integer, :null => false, :default => 0
    end
  
    [:login, :hashed_password, :salt, :disabled_text].each do |col|
      NullsAndDefaultsTuvw::User.update_all("#{col} = ''","#{col} IS NULL")
      change_column :users, col, :string, :null => false, :default => ''
    end
    [:locked, :send_email, :no_self_support_ticket_messages, :show_tips,
      :notify_booking_release, :notify_booking_new, :notify_arrest_new,
      :notify_support_ticket_new, :notify_support_ticket_resolved, :notify_call_new,
      :notify_offense_new, :notify_forbid_new, :notify_forbid_recalled, :notify_pawn_new,
      :notify_warrant_new, :notify_warrant_resolved, :notify_booking_problems,
      :forum_auto_subscribe].each do |col|
      NullsAndDefaultsTuvw::User.update_all("#{col} = FALSE","#{col} IS NULL")
      change_column_null :users, col, false
    end
    [:items_per_page, :message_retention].each do |col|
      NullsAndDefaultsTuvw::User.update_all("#{col} = 0","#{col} IS NULL")
      change_column_null :users, col, false
    end

    NullsAndDefaultsTuvw::UserGroup.delete_all("user_id IS NULL OR group_id IS NULL")
    change_column_null :user_groups, :user_id, false
    change_column_null :user_groups, :group_id, false

    NullsAndDefaultsTuvw::Visitor.delete_all("booking_id IS NULL")
    change_column_null :visitors, :booking_id, false
    [:name, :street, :city, :zip, :phone, :sign_in_time, :sign_out_time, :state, :relationship].each do |col|
      NullsAndDefaultsTuvw::Visitor.update_all("#{col} = ''","#{col} IS NULL")
      change_column :visitors, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsTuvw::Visitor.update_all("remarks = ''","remarks IS NULL")
    change_column :visitors, :remarks, :text, :null => false, :default => ''
    NullsAndDefaultsTuvw::Visitor.update_all("legacy = FALSE","legacy IS NULL")
    change_column_null :visitors, :legacy, false
    
    [:warrant_no, :ssn, :street, :oln, :firstname, :lastname, :suffix, :city, :zip,
      :middle1, :middle2, :middle3, :state, :oln_state, :jurisdiction, :phone1,
      :phone2, :phone3, :race, :phone1_type, :phone2_type, :phone3_type, :disposition,
      :charge_type].each do |col|
      NullsAndDefaultsTuvw::Warrant.update_all("#{col} = ''","#{col} IS NULL")
      change_column :warrants, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsTuvw::Warrant.update_all("remarks = ''","remarks IS NULL")
    change_column :warrants, :remarks, :text, :null => false, :default => ''
    [:bond_amt, :payable].each do |col|
      NullsAndDefaultsTuvw::Warrant.update_all("#{col} = 0.0","#{col} IS NULL")
      change_column_null :warrants, col, false
    end
    change_column_default :warrants, :sex, 0
    [:sex, :special_type].each do |col|
      NullsAndDefaultsTuvw::Warrant.update_all("#{col} = 0","#{col} IS NULL")
      change_column_null :warrants, col, false
    end
    [:legacy, :phone1_unlisted, :phone2_unlisted, :phone3_unlisted].each do |col|
      NullsAndDefaultsTuvw::Warrant.update_all("#{col} = FALSE","#{col} IS UNKNOWN")
      change_column_null :warrants, col, false
    end
  
    NullsAndDefaultsTuvw::WarrantExemption.delete_all("warrant_id IS NULL OR person_id IS NULL")
    change_column_null :warrant_exemptions, :warrant_id, false
    change_column_null :warrant_exemptions, :person_id, false
  end

  def down
  end
end
