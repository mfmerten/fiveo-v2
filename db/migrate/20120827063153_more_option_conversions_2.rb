class MoreOptionConversions2 < ActiveRecord::Migration
  class Option < ActiveRecord::Base
    OptionTypes = {
      #15 => 'Consecutive Type',
      16 => 'Court Costs',
      #17 => 'Court Type',
      #18 => 'Docket Type',
      #25 => 'Hold Type',
      #26 => 'Investigation Status',
      30 => 'Judge',
      #31 => 'Noservice Type',
      32 => 'Pawn Company',
      33 => 'Pawn Ticket Type',
      34 => 'Pawn Type',
      #37 => 'Plea Type',
      38 => 'Probation Status',
      39 => 'Probation Type',
      41 => 'Release Reason',
      #42 => 'Release Type',
      #43 => 'Service Type',
      44 => 'Signal Code',
      46 => 'Subject Type',
      #47 => 'Temporary Release Reason',
      48 => 'Transfer Type',
      49 => 'Trial Result',
      50 => 'Vehicle Type',
      51 => 'Warrant Disposition'
    }.freeze
    CourtType = [
      'Arraignment', 'Bond Hearing', 'Misdemeanor', 'Motion Hearing', 'Pre-Trial',
      'Probation/Parole', 'Sentencing', 'Trial'
    ].freeze
  end
  class Sentence < ActiveRecord::Base
    belongs_to :old_consecutive_type, :class_name => 'MoreOptionConversions2::Option', :foreign_key => 'consecutive_type_id'
  end
  class Revocation < ActiveRecord::Base
    belongs_to :old_consecutive_type, :class_name => 'MoreOptionConversions2::Option', :foreign_key => 'consecutive_type_id'
  end
  class Court < ActiveRecord::Base
    belongs_to :old_court_type, :class_name => 'MoreOptionConversions2::Option', :foreign_key => 'court_type_id'
    belongs_to :old_next_court_type, :class_name => 'MoreOptionConversions2::Option', :foreign_key => 'next_court_type_id'
    belongs_to :old_docket_type, :class_name => 'MoreOptionConversions2::Option', :foreign_key => 'docket_type_id'
    belongs_to :old_next_docket_type, :class_name => 'MoreOptionConversions2::Option', :foreign_key => 'next_docket_type_id'
    belongs_to :old_plea_type, :class_name => 'MoreOptionConversions2::Option', :foreign_key => 'plea_type_id'
  end
  class Hold < ActiveRecord::Base
    belongs_to :old_hold_type, :class_name => 'MoreOptionConversions2::Option', :foreign_key => 'hold_type_id'
    belongs_to :old_cleared_because, :class_name => 'MoreOptionConversions2::Option', :foreign_key => 'cleared_because_id'
    belongs_to :old_temp_release_reason, :class_name => "MoreOptionConversions2::Option", :foreign_key => 'temp_release_reason_id'
  end
  class Investigation < ActiveRecord::Base
    belongs_to :old_status, :class_name => "MoreOptionConversions2::Option", :foreign_key => 'status_id'
  end
  class Paper < ActiveRecord::Base
    belongs_to :old_noservice_type, :class_name => 'MoreOptionConversions2::Option', :foreign_key => 'noservice_type_id'
    belongs_to :old_service_type, :class_name => 'MoreOptionConversions2::Option', :foreign_key => 'service_type_id'
  end
  
  def up
    add_column :sentences, :consecutive_type, :string
    add_column :revocations, :consecutive_type, :string
    MoreOptionConversions2::Sentence.reset_column_information
    MoreOptionConversions2::Revocation.reset_column_information
    [MoreOptionConversions2::Sentence, MoreOptionConversions2::Revocation].each do |model|
      model.all.each do |m|
        unless m.old_consecutive_type.nil?
          m.update_attribute(:consecutive_type, m.old_consecutive_type.long_name)
        end
      end
    end
    remove_column :sentences, :consecutive_type_id
    remove_column :revocations, :consecutive_type_id
    
    add_column :courts, :court_type, :string
    add_column :courts, :next_court_type, :string
    add_column :courts, :docket_type, :string
    add_column :courts, :next_docket_type, :string
    add_column :courts, :plea_type, :string
    MoreOptionConversions2::Court.reset_column_information
    MoreOptionConversions2::Court.all.each do |c|
      unless c.old_court_type.nil?
        if MoreOptionConversions2::Option::CourtType.include?(c.old_court_type.long_name)
          c.court_type = c.old_court_type.long_name
        else
          c.court_type = "Motion Hearing"
        end
      end
      unless c.old_next_court_type.nil?
        if MoreOptionConversions2::Option::CourtType.include?(c.old_next_court_type.long_name)
          c.next_court_type = c.old_next_court_type.long_name
        else
          c.next_court_type = "Motion Hearing"
        end
      end
      unless c.old_docket_type.nil?
        c.docket_type = c.old_docket_type.long_name
      end
      unless c.old_next_docket_type.nil?
        c.next_docket_type = c.old_next_docket_type.long_name
      end
      unless c.old_plea_type.nil?
        c.plea_type = c.old_plea_type.long_name
      end
      if c.changed?
        c.save
      end
    end
    remove_column :courts, :court_type_id
    remove_column :courts, :next_court_type_id
    remove_column :courts, :docket_type_id
    remove_column :courts, :next_docket_type_id
    remove_column :courts, :plea_type_id
    
    add_column :holds, :hold_type, :string
    add_column :holds, :cleared_because, :string
    add_column :holds, :temp_release_reason, :string
    MoreOptionConversions2::Hold.reset_column_information
    MoreOptionConversions2::Hold.all.each do |h|
      unless h.old_hold_type.nil?
        h.hold_type = h.old_hold_type.long_name
      end
      unless h.old_cleared_because.nil?
        h.cleared_because = h.old_cleared_because.long_name
      end
      unless h.old_temp_release_reason.nil?
        h.temp_release_reason = h.old_temp_release_reason.long_name
      end
      if h.changed?
        h.save
      end
    end
    remove_column :holds, :hold_type_id
    remove_column :holds, :cleared_because_id
    remove_column :holds, :temp_release_reason_id
    
    add_column :investigations, :status, :string
    MoreOptionConversions2::Investigation.reset_column_information
    MoreOptionConversions2::Investigation.all.each do |i|
      unless i.old_status.nil?
        i.update_attribute(:status, i.old_status.long_name)
      end
    end
    remove_column :investigations, :status_id
    
    add_column :papers, :noservice_type, :string
    add_column :papers, :service_type, :string
    MoreOptionConversions2::Paper.reset_column_information
    MoreOptionConversions2::Paper.all.each do |p|
      unless p.old_noservice_type.nil?
        p.noservice_type = p.old_noservice_type.long_name
      end
      unless p.old_service_type.nil?
        p.service_type = p.old_service_type.long_name
      end
      if p.changed?
        p.save
      end
    end
    remove_column :papers, :noservice_type_id
    remove_column :papers, :service_type_id
    
    MoreOptionConversions2::Option.where(:option_type => [15,17,18,25,26,31,37,42,43,47]).all.each{|o| o.destroy}
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
