class EncryptWarrantPawnSsn < ActiveRecord::Migration
  class Warrant < ActiveRecord::Base
    attr_encrypted :ssn
  end
  class Pawn < ActiveRecord::Base
    attr_encrypted :ssn
  end
  
  def up
    EncryptWarrantPawnSsn::Warrant.all.each do |w|
      w.ssn = w.old_ssn || ''
      w.save
    end
    remove_column :warrants, :old_ssn
    
    EncryptWarrantPawnSsn::Pawn.all.each do |p|
      p.ssn = p.old_ssn || ''
      p.save
    end
    remove_column :pawns, :old_ssn
  end
  
  def down
    add_column :warrants, :old_ssn, :string, default: '', null: false
    EncryptWarrantPawnSsn::Warrant.all.each do |w|
      w.old_ssn = w.ssn || ''
      w.save
    end
    
    add_column :pawns, :old_ssn, :string, default: '', null: false
    EncryptWarrantPawnSsn::Pawn.all.each do |p|
      p.old_ssn = p.ssn || ''
      p.save
    end
  end
end
