class AddSexOffender < ActiveRecord::Migration
  def up
    add_column :people, :sex_offender, :boolean, :default => false
  end

  def down
    remove_column :people, :sex_offender
  end
end
