class AddInvestNotes < ActiveRecord::Migration
  def change
    create_table :invest_notes, :force => true do |t|
      t.integer :investigation_id
      t.text :note
      t.integer :created_by, :default => 1
      t.integer :updated_by, :default => 1
      t.timestamps
    end
    add_index :invest_notes, :investigation_id
    add_index :invest_notes, :created_by
    add_index :invest_notes, :updated_by
  end
end
