class MovePhonesIntoContacts < ActiveRecord::Migration
  class Contact < ActiveRecord::Base
    has_many :phones, :class_name => 'MovePhonesIntoContacts::Phone'
  end
  class Phone < ActiveRecord::Base
    belongs_to :contact, :class_name => 'MovePhonesIntoContacts::Contact'
  end
  def up
    add_column :contacts, :phone1, :string
    add_column :contacts, :phone2, :string
    add_column :contacts, :phone3, :string
    add_column :contacts, :phone1_type, :integer
    add_column :contacts, :phone2_type, :integer
    add_column :contacts, :phone3_type, :integer
    add_column :contacts, :phone1_unlisted, :boolean
    add_column :contacts, :phone2_unlisted, :boolean
    add_column :contacts, :phone3_unlisted, :boolean
    add_column :contacts, :phone1_note, :string
    add_column :contacts, :phone2_note, :string
    add_column :contacts, :phone3_note, :string
    
    MovePhonesIntoContacts::Contact.reset_column_information
    
    MovePhonesIntoContacts::Contact.all.each do |c|
      next if c.phones.empty?
      (0..c.phones.size - 1).each do |x|
        if x <= 2
          c.send("phone#{x+1}=",c.phones[x].value)
          c.send("phone#{x+1}_type=",c.phones[x].label_id)
          c.send("phone#{x+1}_note=",c.phones[x].note)
          c.send("phone#{x+1}_unlisted=",c.phones[x].unlisted)
        else
          unless c.remarks?
            c.remarks = ""
          end
          c.remarks << "-- #{c.phones[x].label.nil? ? 'Phone:' : c.phones[x].label.long_name} #{c.phones[x].value} #{c.phones[x].unlisted? ? '(Unlisted) ' : ''}#{c.phones[x].note? ? c.phones[x].note : ''} "
        end
      end
      c.save(:validate => false)
    end
    drop_table :phones
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
