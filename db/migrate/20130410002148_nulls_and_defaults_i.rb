class NullsAndDefaultsI < ActiveRecord::Migration
  class Arrest < ActiveRecord::Base
  end
  class Bond < ActiveRecord::Base
  end
  class Booking < ActiveRecord::Base
  end
  class Commissary < ActiveRecord::Base
  end
  class CommissaryItem < ActiveRecord::Base
  end
  class InvestArrest < ActiveRecord::Base
  end
  class InvestCase < ActiveRecord::Base
  end
  class InvestContact < ActiveRecord::Base
  end
  class InvestCrime < ActiveRecord::Base
  end
  class InvestCrimeArrest < ActiveRecord::Base
  end
  class InvestCrimeCase < ActiveRecord::Base
  end
  class InvestCrimeContact < ActiveRecord::Base
  end
  class InvestCrimeCriminal < ActiveRecord::Base
  end
  class InvestCrimeEvidence < ActiveRecord::Base
  end
  class InvestCrimeOffense < ActiveRecord::Base
  end
  class InvestCrimePhoto < ActiveRecord::Base
  end
  class InvestCrimeStolen < ActiveRecord::Base
  end
  class InvestCrimeVictim < ActiveRecord::Base
  end
  class InvestCrimeVehicle < ActiveRecord::Base
  end
  class InvestCrimeWarrant < ActiveRecord::Base
  end
  class InvestCrimeWitness < ActiveRecord::Base
  end
  class InvestCriminal < ActiveRecord::Base
  end
  class InvestCriminalArrest < ActiveRecord::Base
  end
  class InvestCriminalStolen < ActiveRecord::Base
  end
  class InvestCriminalPhoto < ActiveRecord::Base
  end
  class InvestCriminalVehicle < ActiveRecord::Base
  end
  class InvestCriminalWarrant < ActiveRecord::Base
  end
  class InvestEvidence < ActiveRecord::Base
  end
  class InvestNote < ActiveRecord::Base
  end
  class InvestOffense < ActiveRecord::Base
  end
  class InvestPhoto < ActiveRecord::Base
  end
  class InvestReport < ActiveRecord::Base
  end
  class InvestReportArrest < ActiveRecord::Base
  end
  class InvestReportCriminal < ActiveRecord::Base
  end
  class InvestReportOffense < ActiveRecord::Base
  end
  class InvestReportPhoto < ActiveRecord::Base
  end
  class InvestReportVehicle < ActiveRecord::Base
  end
  class InvestReportVictim < ActiveRecord::Base
  end
  class InvestReportWitness < ActiveRecord::Base
  end
  class InvestSource < ActiveRecord::Base
  end
  class InvestStolen < ActiveRecord::Base
  end
  class InvestVehicle < ActiveRecord::Base
  end
  class InvestVehiclePhoto < ActiveRecord::Base
  end
  class InvestVictim < ActiveRecord::Base
  end
  class InvestWarrant < ActiveRecord::Base
  end
  class InvestWitness < ActiveRecord::Base
  end
  class Investigation < ActiveRecord::Base
  end
  
  def up
    # some stuff I missed from previous migrations
    NullsAndDefaultsI::Arrest.update_all("bond_amt = 0.0","bond_amt IS NULL")
    change_column_null :arrests, :bond_amt, false
    NullsAndDefaultsI::Bond.update_all("bond_amt = 0.0","bond_amt IS NULL")
    change_column_null :bonds, :bond_amt, false
    NullsAndDefaultsI::Booking.update_all("cash_at_booking = 0.0","cash_at_booking IS NULL")
    change_column_null :bookings, :cash_at_booking, false
    [:deposit,:withdraw].each do |col|
      NullsAndDefaultsI::Commissary.update_all("#{col} = 0.0","#{col} IS NULL")
      change_column_null :commissaries, col, false
    end
    NullsAndDefaultsI::CommissaryItem.update_all("price = 0.0","price IS NULL")
    change_column_null :commissary_items, :price, false
    NullsAndDefaultsI::CourtCost.update_all("amount = 0.0","amount IS NULL")
    change_column_null :court_costs, :amount, false
    
    # now for the I's
    
    NullsAndDefaultsI::InvestArrest.delete_all("arrest_id IS NULL OR investigation_id IS NULL")
    change_column_null :invest_arrests, :arrest_id, false
    change_column_null :invest_arrests, :investigation_id, false
    
    NullsAndDefaultsI::InvestCase.delete_all("call_id IS NULL OR investigation_id IS NULL")
    change_column_null :invest_cases, :call_id, false
    change_column_null :invest_cases, :investigation_id, false
    
    NullsAndDefaultsI::InvestContact.delete_all("contact_id IS NULL OR investigation_id IS NULL")
    change_column_null :invest_contacts, :contact_id, false
    change_column_null :invest_contacts, :investigation_id, false
    
    NullsAndDefaultsI::InvestCrime.delete_all("investigation_id IS NULL")
    change_column_null :invest_crimes, :investigation_id, false
    [:motive, :location, :entry_gained, :weapons_used, :security, :impersonated,
      :victim_relationship].each do |col|
      NullsAndDefaultsI::InvestCrime.update_all("#{col} = ''","#{col} IS NULL")
      change_column :invest_crimes, col, :string, :null => false, :default => ''
    end
    [:occupied, :private].each do |col|
      NullsAndDefaultsI::InvestCrime.update_all("#{col} = FALSE","#{col} IS NULL")
      change_column_null :invest_crimes, col, false
    end
    [:facts_prior, :facts_after, :targets, :actions_taken, :threats_made, :details,
      :remarks].each do |col|
      NullsAndDefaultsI::InvestCrime.update_all("#{col} = ''","#{col} IS NULL")
      change_column :invest_crimes, col, :text, :null => false, :default => ''
    end
  
    NullsAndDefaultsI::InvestCrimeArrest.delete_all("invest_arrest_id IS NULL OR invest_crime_id IS NULL")
    change_column_null :invest_crime_arrests, :invest_arrest_id, false
    change_column_null :invest_crime_arrests, :invest_crime_id, false
  
    NullsAndDefaultsI::InvestCrimeCase.delete_all("invest_case_id IS NULL OR invest_crime_id IS NULL")
    change_column_null :invest_crime_cases, :invest_case_id, false
    change_column_null :invest_crime_cases, :invest_crime_id, false
  
    NullsAndDefaultsI::InvestCrimeContact.delete_all("invest_contact_id IS NULL OR invest_crime_id IS NULL")
    change_column_null :invest_crime_contacts, :invest_contact_id, false
    change_column_null :invest_crime_contacts, :invest_crime_id, false
  
    NullsAndDefaultsI::InvestCrimeCriminal.delete_all("invest_criminal_id IS NULL OR invest_crime_id IS NULL")
    change_column_null :invest_crime_criminals, :invest_criminal_id, false
    change_column_null :invest_crime_criminals, :invest_crime_id, false
  
    NullsAndDefaultsI::InvestCrimeEvidence.delete_all("invest_evidence_id IS NULL OR invest_crime_id IS NULL")
    change_column_null :invest_crime_evidences, :invest_evidence_id, false
    change_column_null :invest_crime_evidences, :invest_crime_id, false
  
    NullsAndDefaultsI::InvestCrimeOffense.delete_all("invest_offense_id IS NULL OR invest_crime_id IS NULL")
    change_column_null :invest_crime_offenses, :invest_offense_id, false
    change_column_null :invest_crime_offenses, :invest_crime_id, false
  
    NullsAndDefaultsI::InvestCrimePhoto.delete_all("invest_photo_id IS NULL OR invest_crime_id IS NULL")
    change_column_null :invest_crime_photos, :invest_photo_id, false
    change_column_null :invest_crime_photos, :invest_crime_id, false
  
    NullsAndDefaultsI::InvestCrimeStolen.delete_all("invest_stolen_id IS NULL OR invest_crime_id IS NULL")
    change_column_null :invest_crime_stolens, :invest_stolen_id, false
    change_column_null :invest_crime_stolens, :invest_crime_id, false
  
    NullsAndDefaultsI::InvestCrimeVehicle.delete_all("invest_vehicle_id IS NULL OR invest_crime_id IS NULL")
    change_column_null :invest_crime_vehicles, :invest_vehicle_id, false
    change_column_null :invest_crime_vehicles, :invest_crime_id, false
  
    NullsAndDefaultsI::InvestCrimeVictim.delete_all("invest_victim_id IS NULL OR invest_crime_id IS NULL")
    change_column_null :invest_crime_victims, :invest_victim_id, false
    change_column_null :invest_crime_victims, :invest_crime_id, false
  
    NullsAndDefaultsI::InvestCrimeWarrant.delete_all("invest_warrant_id IS NULL OR invest_crime_id IS NULL")
    change_column_null :invest_crime_warrants, :invest_warrant_id, false
    change_column_null :invest_crime_warrants, :invest_crime_id, false
  
    NullsAndDefaultsI::InvestCrimeWitness.delete_all("invest_witness_id IS NULL OR invest_crime_id IS NULL")
    change_column_null :invest_crime_witnesses, :invest_witness_id, false
    change_column_null :invest_crime_witnesses, :invest_crime_id, false
  
    NullsAndDefaultsI::InvestCriminal.delete_all("investigation_id IS NULL")
    change_column_null :invest_criminals, :investigation_id, false
    [:full_name, :description, :height, :weight, :shoes, :fingerprint_class, :dna_profile].each do |col|
      NullsAndDefaultsI::InvestCriminal.update_all("#{col} = ''","#{col} IS NULL")
      change_column :invest_criminals, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsI::InvestCriminal.update_all("remarks = ''","remarks IS NULL")
    change_column :invest_criminals, :remarks, :text, :null => false, :default => ''
    NullsAndDefaultsI::InvestCriminal.update_all("private = FALSE","private IS NULL")
    change_column_null :invest_criminals, :private, false
  
    NullsAndDefaultsI::InvestCriminalArrest.delete_all("invest_arrest_id IS NULL OR invest_criminal_id IS NULL")
    change_column_null :invest_criminal_arrests, :invest_arrest_id, false
    change_column_null :invest_criminal_arrests, :invest_criminal_id, false
  
    NullsAndDefaultsI::InvestCriminalPhoto.delete_all("invest_photo_id IS NULL OR invest_criminal_id IS NULL")
    change_column_null :invest_criminal_photos, :invest_photo_id, false
    change_column_null :invest_criminal_photos, :invest_criminal_id, false
  
    NullsAndDefaultsI::InvestCriminalStolen.delete_all("invest_stolen_id IS NULL OR invest_criminal_id IS NULL")
    change_column_null :invest_criminal_stolens, :invest_stolen_id, false
    change_column_null :invest_criminal_stolens, :invest_criminal_id, false
  
    NullsAndDefaultsI::InvestCriminalVehicle.delete_all("invest_vehicle_id IS NULL OR invest_criminal_id IS NULL")
    change_column_null :invest_criminal_vehicles, :invest_vehicle_id, false
    change_column_null :invest_criminal_vehicles, :invest_criminal_id, false
  
    NullsAndDefaultsI::InvestCriminalWarrant.delete_all("invest_warrant_id IS NULL OR invest_criminal_id IS NULL")
    change_column_null :invest_criminal_warrants, :invest_warrant_id, false
    change_column_null :invest_criminal_warrants, :invest_criminal_id, false
  
    NullsAndDefaultsI::InvestEvidence.delete_all("evidence_id IS NULL OR investigation_id IS NULL")
    change_column_null :invest_evidences, :evidence_id, false
    change_column_null :invest_evidences, :investigation_id, false
  
    NullsAndDefaultsI::InvestNote.update_all("note = ''","note IS NULL")
    change_column :invest_notes, :note, :text, :null => false, :default => ''
    
    NullsAndDefaultsI::InvestOffense.delete_all("offense_id IS NULL OR investigation_id IS NULL")
    change_column_null :invest_offenses, :offense_id, false
    change_column_null :invest_offenses, :investigation_id, false
    
    NullsAndDefaultsI::InvestPhoto.delete_all("investigation_id IS NULL")
    change_column_null :invest_photos, :investigation_id, false
    NullsAndDefaultsI::InvestPhoto.update_all("private = FALSE","private IS NULL")
    change_column_null :invest_photos, :private, false
    [:description, :location_taken, :taken_by, :taken_by_unit, :taken_by_badge].each do |col|
      NullsAndDefaultsI::InvestPhoto.update_all("#{col} = ''","#{col} IS NULL")
      change_column :invest_photos, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsI::InvestPhoto.update_all("remarks = ''","remarks IS NULL")
    change_column :invest_photos, :remarks, :text, :null => false, :default => ''
    
    NullsAndDefaultsI::InvestReport.delete_all("investigation_id IS NULL")
    change_column_null :invest_reports, :investigation_id, false
    [:details, :remarks].each do |col|
      NullsAndDefaultsI::InvestReport.update_all("#{col} = ''","#{col} IS NULL")
      change_column :invest_reports, col, :text, :null => false, :default => ''
    end
    [:supplemental, :private].each do |col|
      NullsAndDefaultsI::InvestReport.update_all("#{col} = FALSE","#{col} IS NULL")
      change_column_null :invest_reports, col, false
    end
    
    NullsAndDefaultsI::InvestReportArrest.delete_all("invest_report_id IS NULL OR invest_arrest_id IS NULL")
    change_column_null :invest_report_arrests, :invest_report_id, false
    change_column_null :invest_report_arrests, :invest_arrest_id, false
    
    NullsAndDefaultsI::InvestReportCriminal.delete_all("invest_report_id IS NULL OR invest_criminal_id IS NULL")
    change_column_null :invest_report_criminals, :invest_report_id, false
    change_column_null :invest_report_criminals, :invest_criminal_id, false
    
    NullsAndDefaultsI::InvestReportOffense.delete_all("invest_report_id IS NULL OR invest_offense_id IS NULL")
    change_column_null :invest_report_offenses, :invest_report_id, false
    change_column_null :invest_report_offenses, :invest_offense_id, false
    
    NullsAndDefaultsI::InvestReportPhoto.delete_all("invest_report_id IS NULL OR invest_photo_id IS NULL")
    change_column_null :invest_report_photos, :invest_report_id, false
    change_column_null :invest_report_photos, :invest_photo_id, false
    
    NullsAndDefaultsI::InvestReportVehicle.delete_all("invest_report_id IS NULL OR invest_vehicle_id IS NULL")
    change_column_null :invest_report_vehicles, :invest_report_id, false
    change_column_null :invest_report_vehicles, :invest_vehicle_id, false
    
    NullsAndDefaultsI::InvestReportVictim.delete_all("invest_report_id IS NULL OR invest_victim_id IS NULL")
    change_column_null :invest_report_victims, :invest_report_id, false
    change_column_null :invest_report_victims, :invest_victim_id, false
    
    NullsAndDefaultsI::InvestReportWitness.delete_all("invest_report_id IS NULL OR invest_witness_id IS NULL")
    change_column_null :invest_report_witnesses, :invest_report_id, false
    change_column_null :invest_report_witnesses, :invest_witness_id, false
  
    NullsAndDefaultsI::InvestSource.delete_all("person_id IS NULL OR investigation_id IS NULL")
    change_column_null :invest_sources, :person_id, false
    change_column_null :invest_sources, :investigation_id, false
  
    NullsAndDefaultsI::InvestVictim.delete_all("person_id IS NULL OR investigation_id IS NULL")
    change_column_null :invest_victims, :person_id, false
    change_column_null :invest_victims, :investigation_id, false
  
    NullsAndDefaultsI::InvestWitness.delete_all("person_id IS NULL OR investigation_id IS NULL")
    change_column_null :invest_witnesses, :person_id, false
    change_column_null :invest_witnesses, :investigation_id, false
  
    NullsAndDefaultsI::InvestStolen.delete_all("stolen_id IS NULL OR investigation_id IS NULL")
    change_column_null :invest_stolens, :stolen_id, false
    change_column_null :invest_stolens, :investigation_id, false
  
    NullsAndDefaultsI::InvestVehicle.delete_all("investigation_id IS NULL")
    change_column_null :invest_vehicles, :investigation_id, false
    NullsAndDefaultsI::InvestVehicle.update_all("private = FALSE","private IS NULL")
    change_column_null :invest_vehicles, :private, false
    [:make, :model, :vin, :license, :color, :tires, :model_year, :license_state, :vehicle_type].each do |col|
      NullsAndDefaultsI::InvestVehicle.update_all("#{col} = ''","#{col} IS NULL")
      change_column :invest_vehicles, col, :string, :null => false, :default => ''
    end
    [:description, :remarks].each do |col|
      NullsAndDefaultsI::InvestVehicle.update_all("#{col} = ''", "#{col} IS NULL")
      change_column :invest_vehicles, col, :text, :null => false, :default => ''
    end
    
    NullsAndDefaultsI::InvestVehiclePhoto.delete_all("invest_vehicle_id IS NULL OR invest_photo_id IS NULL")
    change_column_null :invest_vehicle_photos, :invest_vehicle_id, false
    change_column_null :invest_vehicle_photos, :invest_photo_id, false
  
    NullsAndDefaultsI::InvestWarrant.delete_all("warrant_id IS NULL OR investigation_id IS NULL")
    change_column_null :invest_warrants, :investigation_id, false
    change_column_null :invest_warrants, :warrant_id, false
  
    NullsAndDefaultsI::Investigation.update_all("private = FALSE","private IS UNKNOWN")
    change_column_null :investigations, :private, false
    NullsAndDefaultsI::Investigation.update_all("status = ''","status IS NULL")
    change_column :investigations, :status, :string, :null => false, :default => ''
    [:details, :remarks].each do |col|
      NullsAndDefaultsI::Investigation.update_all("#{col} = ''","#{col} IS NULL")
      change_column :investigations, col, :text, :null => false, :default => ''
    end
  end

  def down
  end
end
