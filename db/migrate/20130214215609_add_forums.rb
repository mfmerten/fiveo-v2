# adapted from "forem" engine (https://github.com/radar/forem)
#
# Copyright 2011 Ryan Bigg, Philip Arndt and Josh Adams
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
class AddForums < ActiveRecord::Migration
  def change
    create_table :forums, :force => true do |t|
      t.string :name
      t.text :description
      t.integer :forum_category_id
      t.integer :views_count, :default => 0
      t.integer :created_by, :default => 1
      t.integer :updated_by, :default => 1
      t.timestamps
    end
    add_index :forums, :forum_category_id
    
    create_table :forum_topics, :force => true do |t|
      t.integer :forum_id
      t.integer :user_id
      t.string :subject
      t.boolean :locked, :default => false
      t.boolean :pinned, :default => false
      t.boolean :hidden, :default => false
      t.datetime :last_post_at
      t.integer :views_count, :default => 0
      t.timestamps
    end
    add_index :forum_topics, :forum_id
    add_index :forum_topics, :user_id
    
    create_table :forum_posts, :force => true do |t|
      t.integer :forum_topic_id
      t.integer :user_id
      t.integer :reply_to_id
      t.text :text
      t.boolean :notified, :default => false
      t.timestamps
    end
    add_index :forum_posts, :forum_topic_id
    add_index :forum_posts, :user_id
    add_index :forum_posts, :reply_to_id
    
    create_table :forum_views, :force => true do |t|
      t.integer :viewable_id
      t.string :viewable_type
      t.integer :user_id
      t.integer :count, :default => 0
      t.datetime :current_viewed_at
      t.datetime :past_viewed_at
      t.timestamps
    end
    add_index :forum_views, :updated_at
    add_index :forum_views, :viewable_id
    add_index :forum_views, :viewable_type
    add_index :forum_views, :user_id
    
    create_table :forum_categories, :force => true do |t|
      t.string :name
      t.timestamps
    end
    add_index :forum_categories, :name
    
    create_table :forum_subscriptions, :force => true do |t|
      t.integer :subscriber_id
      t.integer :forum_topic_id
      t.timestamps
    end
    add_index :forum_subscriptions, :subscriber_id
    add_index :forum_subscriptions, :forum_topic_id
    
    add_column :users, :forum_auto_subscribe, :boolean, :default => false
  end
end
