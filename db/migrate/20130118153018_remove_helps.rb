class RemoveHelps < ActiveRecord::Migration
  def up
    drop_table :helps
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
