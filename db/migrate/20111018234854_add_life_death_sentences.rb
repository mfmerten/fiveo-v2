class AddLifeDeathSentences < ActiveRecord::Migration
  # added as part of Rails3 upgrade
  def up
    add_column :revocations, :total_life, :integer, :default => 0
    add_column :revocations, :total_death, :boolean, :default => false
    add_column :sentences, :sentence_life, :integer, :default => 0
    add_column :sentences, :sentence_death, :boolean, :default => false
    add_column :sentences, :suspended_except_life, :integer, :default => 0
    add_column :probations, :sentence_life, :integer, :default => 0
    add_column :probations, :sentence_death, :boolean, :default => false
    add_column :probations, :suspended_except_life, :integer, :default => 0
    add_column :docs, :sentence_life, :integer, :default => 0
    add_column :docs, :sentence_death, :boolean, :default => false
    add_column :docs, :to_serve_life, :integer, :default => 0
    add_column :docs, :to_serve_death, :boolean, :default => false
    add_column :holds, :life_sentence, :boolean, :default => false
    add_column :holds, :death_sentence, :boolean, :default => false
  end

  def down
    remove_column :revocations, :total_life
    remove_column :revocations, :total_death
    remove_column :sentences, :sentence_life
    remove_column :sentences, :sentence_death
    remove_column :sentences, :suspended_except_life
    remove_column :probations, :sentence_life
    remove_column :probations, :sentence_death
    remove_column :probations, :suspended_except_life
    remove_column :docs, :sentence_life
    remove_column :docs, :sentence_death
    remove_column :docs, :to_serve_life
    remove_column :docs, :to_serve_death
    remove_column :holds, :life_sentence
    remove_column :holds, :death_sentence
  end
end
