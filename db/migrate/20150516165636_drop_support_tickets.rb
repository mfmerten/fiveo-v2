class DropSupportTickets < ActiveRecord::Migration
  def up
    # support ticket tables
    drop_table :support_ticket_comments
    drop_table :support_ticket_watchers
    drop_table :support_tickets
    
    # user columns for support tickets
    remove_column :users, :no_self_support_ticket_messages
    remove_column :users, :notify_support_ticket_new
    remove_column :users, :notify_support_ticket_resolved
    
    # remove support ticket perms from all groups
    Group.all.to_a.each do |g|
      g.permissions.delete(68)
      g.permissions.delete(69)
      g.permissions.delete(70)
      g.save(validate: false)
    end
  
    # remove site configuration records for support tickets
    SiteStore.where(setting_name: "support_ticket_admin_id").delete_all
    SiteStore.where(setting_name: "support_ticket_maintainer_id").delete_all
    SiteStore.where(setting_name: "bug_email").delete_all
    
    # old site_configurations table which is still around
    remove_column :site_configurations, :support_ticket_admin_id
    remove_column :site_configurations, :support_ticket_maintainer_id
    remove_column :site_configurations, :bug_email
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
