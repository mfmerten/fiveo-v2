class AddLinks < ActiveRecord::Migration
  class ExternalLink < ActiveRecord::Base
  end
  
  def self.up
    unless table_exists?(:external_links)
      create_table :external_links, :force => true do |t|
        t.string :name
        t.string :short_name
        t.string :url
        t.text :description
        t.boolean :front_page
        t.integer :created_by, :default => 1
        t.integer :updated_by, :default => 1
        t.timestamps
      end
      add_index :external_links, :created_by
      add_index :external_links, :updated_by
    
      AddLinks::ExternalLink.reset_column_information
    
      AddLinks::ExternalLink.create(:name => "Louisiana State Police", :short_name => 'LASP', :url => 'http://www.lsp.org', :front_page => true, :created_by => 1, :updated_by => 1)
      AddLinks::ExternalLink.create(:name => "Federal Bureau of Investigation", :short_name => 'FBI', :url => 'http://www.fbi.gov', :front_page => true, :created_by => 1, :updated_by => 1)
      AddLinks::ExternalLink.create(:name => "National Missing and Unidentified Person System", :short_name => 'NamUs', :url => 'http://www.namus.gov', :front_page => true, :created_by => 1, :updated_by => 1)
      AddLinks::ExternalLink.create(:name => "National Center for Missing and Exploited Children", :short_name => 'NCMEC', :url => 'http://www.missingkids.com', :front_page => true, :created_by => 1, :updated_by => 1)
    end
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
