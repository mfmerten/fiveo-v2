class ChangeAgencyToString < ActiveRecord::Migration
  def up
    remove_column :invest_contacts, :printable
    remove_column :invest_contacts, :agency_id
    add_column :invest_contacts, :agency, :string
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
