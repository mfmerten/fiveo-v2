class NullsAndDefaultsJm < ActiveRecord::Migration
  class Jail < ActiveRecord::Base
  end
  class JailAbsentType < ActiveRecord::Base
  end
  class JailCell < ActiveRecord::Base
  end
  class Medical < ActiveRecord::Base
  end
  class Medication < ActiveRecord::Base
  end
  class MedicationSchedule < ActiveRecord::Base
  end
  class Message < ActiveRecord::Base
  end
  class MessageLog < ActiveRecord::Base
  end
  
  def up
    [:name, :acronym, :street, :city, :state, :zip, :phone, :fax, :notify_frequency].each do |col|
      NullsAndDefaultsJm::Jail.update_all("#{col} = ''","#{col} IS NULL")
      change_column :jails, col, :string, :null => false, :default => ''
    end
    [:juvenile, :male, :female, :goodtime_by_default, :afis_enabled].each do |col|
      NullsAndDefaultsJm::Jail.update_all("#{col} = FALSE","#{col} IS UNKNOWN")
      change_column_null :jails, col, false
    end
    NullsAndDefaultsJm::Jail.update_all("permset = 1","permset IS NULL")
    change_column :jails, :permset, :integer, :null => false, :default => 1
    NullsAndDefaultsJm::Jail.update_all("goodtime_hours_per_day = 0.0","goodtime_hours_per_day IS NULL")
    change_column_null :jails, :goodtime_hours_per_day, false
    NullsAndDefaultsJm::Jail.update_all("remarks = ''","remarks IS NULL")
    change_column :jails, :remarks, :text, :null => false, :default => ''
  
    NullsAndDefaultsJm::JailAbsentType.delete_all("jail_id IS NULL")
    change_column_null :jail_absent_types, :jail_id, false
    [:name, :description].each do |col|
      NullsAndDefaultsJm::JailAbsentType.update_all("#{col} = ''","#{col} IS NULL")
      change_column :jail_absent_types, col, :string, :null => false, :default => ''
    end
    
    NullsAndDefaultsJm::JailCell.delete_all("jail_id IS NULL")
    change_column_null :jail_cells, :jail_id, false
    [:name, :description].each do |col|
      NullsAndDefaultsJm::JailCell.update_all("#{col} = ''","#{col} IS NULL")
      change_column :jail_cells, col, :string, :null => false, :default => ''
    end
  
    NullsAndDefaultsJm::Medical.delete_all("booking_id IS NULL")
    change_column_null :medicals, :booking_id, false
    [:arrest_injuries, :officer, :officer_unit, :disposition, :rec_med_tmnt,
      :rec_dental_tmnt, :rec_mental_tmnt, :current_meds, :allergies, :injuries,
      :infestation, :alcohol, :drug, :withdrawal, :attempt_suicide, :suicide_risk,
      :danger, :pregnant, :deformities, :officer_badge].each do |col|
      NullsAndDefaultsJm::Medical.update_all("#{col} = ''","#{col} IS NULL")
      change_column :medicals, col, :string, :null => false, :default => ''
    end
    [:heart_disease, :blood_pressure, :diabetes, :epilepsy, :hepatitis, :hiv,
      :tb, :ulcers, :venereal].each do |col|
      NullsAndDefaultsJm::Medical.update_all("#{col} = FALSE","#{col} IS UNKNOWN")
      change_column_null :medicals, col, false
    end
    
    NullsAndDefaultsJm::Medication.delete_all("person_id IS NULL")
    change_column_null :medications, :person_id, false
    # changing prescription_no from integer to string...
    change_column :medications, :prescription_no, :string, :null => false, :default => ''
    [:drug_name, :dosage, :warnings].each do |col|
      NullsAndDefaultsJm::Medication.update_all("#{col} = ''","#{col} IS NULL")
      change_column :medications, col, :string, :null => false, :default => ''
    end
    
    NullsAndDefaultsJm::MedicationSchedule.delete_all("medication_id IS NULL")
    change_column_null :medication_schedules, :medication_id, false
    [:dose, :time].each do |col|
      NullsAndDefaultsJm::MedicationSchedule.update_all("#{col} = ''","#{col} IS NULL")
      change_column :medication_schedules, col, :string, :null => false, :default => ''
    end
    
    NullsAndDefaultsJm::Message.delete_all("receiver_id IS NULL OR sender_id IS NULL")
    change_column_null :messages, :receiver_id, false
    change_column_null :messages, :sender_id, false
    [:receiver_deleted, :receiver_purged, :sender_deleted, :sender_purged].each do |col|
      NullsAndDefaultsJm::Message.update_all("#{col} = FALSE","#{col} IS UNKNOWN")
      change_column_null :messages, col, false
    end
    NullsAndDefaultsJm::Message.update_all("subject = ''","subject IS NULL")
    change_column :messages, :subject, :string, :null => false, :default => ''
    NullsAndDefaultsJm::Message.update_all("body = ''","body IS NULL")
    change_column :messages, :body, :text, :null => false, :default => ''
  
    NullsAndDefaultsJm::MessageLog.delete_all("receiver_id IS NULL OR sender_id IS NULL")
    change_column_null :message_logs, :receiver_id, false
    change_column_null :message_logs, :sender_id, false
    [:receiver_deleted, :receiver_purged, :sender_deleted, :sender_purged].each do |col|
      NullsAndDefaultsJm::MessageLog.update_all("#{col} = FALSE","#{col} IS UNKNOWN")
      change_column_null :message_logs, col, false
    end
    NullsAndDefaultsJm::MessageLog.update_all("subject = ''","subject IS NULL")
    change_column :message_logs, :subject, :string, :null => false, :default => ''
    NullsAndDefaultsJm::MessageLog.update_all("body = ''","body IS NULL")
    change_column :message_logs, :body, :text, :null => false, :default => ''
  end

  def down
  end
end
