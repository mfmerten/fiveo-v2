class RemoveOfficerIdFromCallUnits < ActiveRecord::Migration
  def up
    remove_column :call_units, :officer_id
  end
  
  def down
    add_column :call_units, :officer_id, :integer
    add_index :call_units, :officer_id
  end
end
