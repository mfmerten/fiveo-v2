class NullsAndDefaultsF < ActiveRecord::Migration
  class FaxCover < ActiveRecord::Base
  end
  class Forbid < ActiveRecord::Base
  end
  class ForbidExemption < ActiveRecord::Base
  end
  class Forum < ActiveRecord::Base
  end
  class ForumCategory < ActiveRecord::Base
  end
  class ForumPost < ActiveRecord::Base
  end
  class ForumSubscription < ActiveRecord::Base
  end
  class ForumTopic < ActiveRecord::Base
  end
  
  def up
    [:name, :from_name, :from_fax, :from_phone, :notice_title].each do |col|
      NullsAndDefaultsF::FaxCover.update_all("#{col} = ''","#{col} IS NULL")
      change_column :fax_covers, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsF::FaxCover.update_all("notice_body = ''","notice_body IS NULL")
    change_column :fax_covers, :notice_body, :text, :null => false, :default => ''
    NullsAndDefaultsF::FaxCover.update_all("include_notice = FALSE","include_notice IS NULL")
    change_column_null :fax_covers, :include_notice, false

    [:case_no, :receiving_officer, :receiving_officer_unit, :receiving_officer_badge,
      :serving_officer, :serving_officer_unit, :serving_officer_badge, :complainant,
      :comp_street, :comp_city_state_zip, :comp_phone, :forbid_last, :forbid_first,
      :forbid_suffix, :forbid_street, :forbid_city_state_zip, :forbid_phone,
      :forbid_middle1, :forbid_middle2, :forbid_middle3].each do |col|
      NullsAndDefaultsF::Forbid.update_all("#{col} = ''","#{col} IS NULL")
      change_column :forbids, col, :string, :null => false, :default => ''
    end
    [:details, :remarks].each do |col|
      NullsAndDefaultsF::Forbid.update_all("#{col} = ''","#{col} IS NULL")
      change_column :forbids, col, :text, :null => false, :default => ''
    end
    NullsAndDefaultsF::Forbid.update_all("legacy = FALSE","legacy IS UNKNOWN")
    change_column_null :forbids, :legacy, false

    NullsAndDefaultsF::ForbidExemption.delete_all("forbid_id IS NULL OR person_id IS NULL")
    change_column_null :forbid_exemptions, :forbid_id, false
    change_column_null :forbid_exemptions, :person_id, false
  
    NullsAndDefaultsF::Forum.update_all("name = ''","name IS NULL")
    change_column :forums, :name, :string, :null => false, :default => ''
    NullsAndDefaultsF::Forum.update_all("description = ''","description IS NULL")
    change_column :forums, :description, :text, :null => false, :default => ''
    NullsAndDefaultsF::Forum.update_all("views_count = 0","views_count IS NULL")
    change_column_null :forums, :views_count, false
    
    NullsAndDefaultsF::ForumCategory.delete_all("name IS NULL")
    change_column :forum_categories, :name, :string, :null => false, :default => ''
    
    NullsAndDefaultsF::ForumPost.delete_all("forum_topic_id IS NULL OR user_id IS NULL")
    change_column_null :forum_posts, :forum_topic_id, false
    change_column_null :forum_posts, :user_id, false
    NullsAndDefaultsF::ForumPost.update_all("text = ''","text IS NULL")
    change_column :forum_posts, :text, :text, :null => false, :default => ''
    NullsAndDefaultsF::ForumPost.update_all("notified = FALSE","notified IS UNKNOWN")
    change_column_null :forum_posts, :notified, false
    
    NullsAndDefaultsF::ForumSubscription.delete_all("subscriber_id IS NULL OR forum_topic_id IS NULL")
    change_column_null :forum_subscriptions, :subscriber_id, false
    change_column_null :forum_subscriptions, :forum_topic_id, false
    
    NullsAndDefaultsF::ForumTopic.delete_all("forum_id IS NULL OR user_id IS NULL")
    change_column_null :forum_topics, :forum_id, false
    change_column_null :forum_topics, :user_id, false
    NullsAndDefaultsF::ForumTopic.update_all("subject = ''","subject IS NULL")
    change_column :forum_topics, :subject, :string, :null => false, :default => ''
    [:locked, :pinned, :hidden].each do |col|
      NullsAndDefaultsF::ForumTopic.update_all("#{col} = FALSE","#{col} IS UNKNOWN")
      change_column_null :forum_topics, col, false
    end
    NullsAndDefaultsF::ForumTopic.update_all("views_count = 0","views_count IS NULL")
    change_column_null :forum_topics, :views_count, false

    NullsAndDefaultsF::ForumView.delete_all("user_id IS NULL OR (forum_id IS NULL AND forum_topic_id IS NULL)")
    change_column_null :forum_views, :user_id, false
    NullsAndDefaultsF::ForumView.update_all("count = 0","count IS NULL")
    change_column_null :forum_views, :count, false
  end

  def down
  end
end
