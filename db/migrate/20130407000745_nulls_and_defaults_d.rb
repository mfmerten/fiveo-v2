class NullsAndDefaultsD < ActiveRecord::Migration
  class DocketType < ActiveRecord::Base
  end
  class DaCharge < ActiveRecord::Base
  end
  class Doc < ActiveRecord::Base
  end
  class Disposition < ActiveRecord::Base
  end
  class Docket < ActiveRecord::Base
  end
  
  def up
    NullsAndDefaultsD::DocketType.delete_all("name IS NULL")
    change_column :docket_types, :name, :string, :null => false, :default => ''
    
    NullsAndDefaultsD::DaCharge.delete_all("docket_id IS NULL")
    change_column_null :da_charges, :docket_id, false
    NullsAndDefaultsD::DaCharge.update_all("count = 1","count IS NULL")
    change_column_null :da_charges, :count, false
    NullsAndDefaultsD::DaCharge.update_all("charge = ''", "charge IS NULL")
    change_column :da_charges, :charge, :string, :null => false, :default => ''
    [:dropped_by_da, :processed, :info_only, :da_diversion].each do |col|
      NullsAndDefaultsD::DaCharge.update_all("#{col} = FALSE", "#{col} IS NULL")
      change_column_null :da_charges, col, false
    end
    
    NullsAndDefaultsD::Doc.delete_all("hold_id IS NULL OR booking_id IS NULL OR person_id IS NULL")
    [:hold_id, :booking_id, :person_id].each do |col|
      change_column_null :docs, col, false
    end
    [:sentence_days, :sentence_months, :sentence_years, :to_serve_days,
      :to_serve_months, :to_serve_years, :sentence_hours, :to_serve_hours,
      :sentence_life, :to_serve_life].each do |col|
      NullsAndDefaultsD::Doc.update_all("#{col} = 0","#{col} IS NULL")
      change_column_null :docs, col, false
    end
    [:billable, :sentence_death, :to_serve_death].each do |col|
      NullsAndDefaultsD::Doc.update_all("#{col} = FALSE","#{col} IS NULL")
      change_column_null :docs, col, false
    end
    [:conviction, :doc_number, :problems].each do |col|
      NullsAndDefaultsD::Doc.update_all("#{col} = ''","#{col} IS NULL")
      change_column :docs, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsD::Doc.update_all("remarks = ''","remarks IS NULL")
    change_column :docs, :remarks, :text, :null => false, :default => ''
    
    NullsAndDefaultsD::Disposition.delete_all("person_id IS NULL OR ATN IS NULL")
    change_column_null :dispositions, :person_id, false
    change_column :dispositions, :atn, :string, :null => false, :default => ''
    NullsAndDefaultsD::Disposition.update_all("status = 0","status IS NULL")
    change_column_null :dispositions, :status, false
    [:remarks, :disposition].each do |col|
      NullsAndDefaultsD::Disposition.update_all("#{col} = ''","#{col} IS NULL")
      change_column :dispositions, col, :text, :null => false, :default => ''
    end
    
    NullsAndDefaultsD::Docket.delete_all("person_id IS NULL")
    change_column_null :dockets, :person_id, false
    NullsAndDefaultsD::Docket.update_all("docket_no = ''","docket_no IS NULL")
    change_column :dockets, :docket_no, :string, :null => false, :default => ''
    [:notes, :remarks].each do |col|
      NullsAndDefaultsD::Docket.update_all("#{col} = ''","#{col} IS NULL")
      change_column :dockets, col, :text, :null => false, :default => ''
    end
    [:misc, :processed].each do |col|
      NullsAndDefaultsD::Docket.update_all("#{col} = FALSE", "#{col} IS UNKNOWN")
      change_column_null :dockets, col, false
    end
  end

  def down
  end
end
