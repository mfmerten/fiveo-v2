class Remove3JailOptions < ActiveRecord::Migration
  # part of Rails3 upgrade
  # remove partial implementation of 3-jail system - bug 29
  class Option < ActiveRecord::Base
  end
  
  def up
    Remove3JailOptions::Option.delete_all("option_type = 27 OR option_type = 28 OR option_type = 29")
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
