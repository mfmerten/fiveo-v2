class RemoveLifeFromHold < ActiveRecord::Migration
  def up
    remove_column :holds, :life_sentence
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
