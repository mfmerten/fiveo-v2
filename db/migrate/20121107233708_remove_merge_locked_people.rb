class RemoveMergeLockedPeople < ActiveRecord::Migration
  class Person < ActiveRecord::Base
    belongs_to :alias_for, :class_name => 'RemoveMergeLockedPeople::Person', :foreign_key => 'is_alias_for'
    has_many :aliases, :class_name => 'RemoveMergeLockedPeople::Person', :foreign_key => 'is_alias_for', :dependent => :nullify
    def real_identity
      p = self
      until p.alias_for.nil?
        p = p.alias_for
      end
      p
    end
    def cannot_be_deleted
      # don't allow deletion if it has aliases
      return true if merge_locked?
      return true unless aliases.empty?
      models = []
      Dir.chdir(File.join(Rails.root, "app/models")) do 
        models = Dir["*.rb"]
      end
      models.each do |m|
        class_name = m.sub(/\.rb$/,'').camelize
        if class_name.constantize.respond_to?('uses_person')
          return true if class_name.constantize.uses_person(id)
        end
      end
      false
    end
    def full_name(reverse = true)
      namestring = [firstname, middlename, suffix].join(' ')
      [lastname, namestring].join(', ')
    end
  end
  
  def up
    # look up all People that are merge_locked and process them as aka
    # before deleting them
    RemoveMergeLockedPeople::Person.where(:merge_locked => true).all.each do |p|
      if p.alias_for.nil?
        # invalid is_alias_for entry, cannot process this as an alias
        p.update_attribute(:merge_locked, false)
        p.update_attribute(:is_alias_for, nil)
        next
      end
      # get 'real id' (top of alias tree)
      realid = p.real_identity
      # if p has aliases, need to migrate them all to real id
      p.aliases.each do |a|
        a.update_attribute(:is_alias_for, realid.id)
      end
      # now, remove the merge_lock and test to see if it can be deleted
      p.update_attribute(:merge_locked, false)
      next if p.cannot_be_deleted
      # update realid aka field with p.full_name if different
      if realid.full_name != p.full_name
        realid.update_attribute(:aka, [realid.aka, p.full_name(false)].join(", "))
      end
      # finally, remove p
      p.destroy
    end
    
    # update completed, remove the merge_locked column
    remove_column :people, :merge_locked
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
