class AddUsernameToActivity < ActiveRecord::Migration
  class Activity < ActiveRecord::Base
    belongs_to :user, :class_name => 'AddUsernameToActivity::User'
  end
  class User < ActiveRecord::Base
    has_many :activities, :class_name => 'AddUsernameToActivity::Activity'
  end
  def up
    add_column :activities, :username, :string
    AddUsernameToActivity::Activity.reset_column_information
    AddUsernameToActivity::Activity.all.each do |a|
      if a.user.nil?
        a.update_attribute(:user_id, nil)
        a.update_attribute(:username, "Unknown (#{a.user_id})")
      else
        a.update_attribute(:username, "#{a.user.login} (#{a.user_id})")
      end
    end
  end
  
  def down
    remove_column :activities, :username
  end
end
