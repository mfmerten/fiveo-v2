class UpdateInvestContacts < ActiveRecord::Migration
  def change
    add_column :invest_contacts, :bondsman, :boolean
    add_column :invest_contacts, :judge, :boolean
    add_column :invest_contacts, :pawn_co, :boolean
  end
end
