class FixTicketHabtm < ActiveRecord::Migration
  class SupportTicket < ActiveRecord::Base
    has_and_belongs_to_many :watchers, :class_name => 'FixTicketHabtm::User', :join_table => 'watchers'
    has_many :support_ticket_watchers, :class_name => 'FixTicketHabtm::SupportTicketWatcher'
    has_many :xwatchers, :through => :support_ticket_watchers, :source => :user
  end
  
  class User < ActiveRecord::Base
  end
  
  class SupportTicketWatcher < ActiveRecord::Base
    belongs_to :support_ticket, :class_name => 'FixTicketHabtm::SupportTicket'
    belongs_to :user, :class_name => 'FixTicketHabtm::User'
  end
  
  def up
    create_table :support_ticket_watchers do |t|
      t.integer :support_ticket_id
      t.integer :user_id
      t.timestamps
    end
    add_index :support_ticket_watchers, :support_ticket_id
    add_index :support_ticket_watchers, :user_id
    
    FixTicketHabtm::SupportTicketWatcher.reset_column_information
    
    FixTicketHabtm::SupportTicket.all.each do |s|
      s.xwatcher_ids = s.watcher_ids
    end
    
    drop_table :watchers
  end

  def down
    create_table :watchers, :id => false do |t|
      t.integer :support_ticket_id
      t.integer :user_id
    end
    add_index :watchers, :support_ticket_id
    add_index :watchers, :user_id
    
    FixTicketHabtm::SupportTicket.all.each do |s|
      s.watcher_ids = s.xwatcher_ids
    end
    
    drop_table :support_ticket_watchers
  end
end
