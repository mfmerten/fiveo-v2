class ConvertPhysicalTraits < ActiveRecord::Migration
  class Option < ActiveRecord::Base
    OptionTypes = {
      2 => 'Anatomy',
      3 => 'Associate Type',
      5 => 'Bond Type',
      9 => 'Build',
      10 => 'Call Disposition',
      11 => 'Call Via',
      13 => 'Commissary Type',
      14 => 'Complexion',
      15 => 'Consecutive Type',
      16 => 'Court Costs',
      17 => 'Court Type',
      18 => 'Docket Type',
      19 => 'Eye Color',
      20 => 'Facial Hair',
      22 => 'Hair Color',
      23 => 'Hair Type',
      25 => 'Hold Type',
      26 => 'Investigation Status',
      30 => 'Judge',
      31 => 'Noservice Type',
      32 => 'Pawn Company',
      33 => 'Pawn Ticket Type',
      34 => 'Pawn Type',
      35 => 'Payment Type',
      36 => 'Phone Type',
      37 => 'Plea Type',
      38 => 'Probation Status',
      39 => 'Probation Type',
      40 => 'Race',
      41 => 'Release Reason',
      42 => 'Release Type',
      43 => 'Service Type',
      44 => 'Signal Code',
      46 => 'Subject Type',
      47 => 'Temporary Release Reason',
      48 => 'Transfer Type',
      49 => 'Trial Result',
      50 => 'Vehicle Type',
      51 => 'Warrant Disposition'
    }.freeze

    def self.find_by_option_type(option_type)
      otype = get_option_type(option_type)
      if otype.nil?
        []
      else
        where(['option_type = ?', otype]).all
      end
    end

    def self.get_option_type(value)
      ConvertPhysicalTraits::Option::OptionTypes.merge(ConvertPhysicalTraits::Option::OptionTypes.invert)[value]
    end
  end
  class Person < ActiveRecord::Base
    belongs_to :old_race, :class_name => "ConvertPhysicalTraits::Option", :foreign_key => 'race_id'
    belongs_to :old_build, :class_name => "ConvertPhysicalTraits::Option", :foreign_key => 'build_id'
    belongs_to :old_hair_color, :class_name => "ConvertPhysicalTraits::Option", :foreign_key => 'hair_color_id'
    belongs_to :old_hair_type, :class_name => "ConvertPhysicalTraits::Option", :foreign_key => 'hair_type_id'
    belongs_to :old_eye_color, :class_name => "ConvertPhysicalTraits::Option", :foreign_key => 'eye_color_id'
    belongs_to :old_complexion, :class_name => "ConvertPhysicalTraits::Option", :foreign_key => 'complexion_id'
    belongs_to :old_facial_hair, :class_name => "ConvertPhysicalTraits::Option", :foreign_key => 'facial_hair_id'
  end
  class Warrant < ActiveRecord::Base
    belongs_to :old_race, :class_name => "ConvertPhysicalTraits::Option", :foreign_key => 'race_id'
  end

  def up
    # add new string fields
    add_column :people, :race, :string
    add_column :people, :build, :string
    add_column :people, :eye_color, :string
    add_column :people, :hair_color, :string
    add_column :people, :skin_tone, :string
    add_column :people, :complexion, :string
    add_column :people, :hair_type, :string
    add_column :people, :mustache, :string
    add_column :people, :beard, :string
    add_column :people, :eyebrow, :string
    add_column :warrants, :race, :string
    
    ConvertPhysicalTraits::Person.reset_column_information
    ConvertPhysicalTraits::Warrant.reset_column_information
    
    # convert person fields to fit new constants
    ConvertPhysicalTraits::Person.all.each do |p|
      unless p.old_race.nil?
        if p.old_race.long_name == 'Am. Indian'
          p.race = 'American Indian'
        else
          p.race = p.old_race.long_name
        end
      end
      unless p.old_build.nil?
        p.build = p.old_build.long_name
      end
      unless p.old_eye_color.nil?
        if p.old_eye_color.long_name == 'Grey'
          p.eye_color = 'Gray'
        else
          p.eye_color = p.old_eye_color.long_name
        end
      end
      unless p.old_hair_color.nil?
        case p.old_hair_color.long_name
        when 'Bald'
          p.hair_type = 'Bald'
        when 'Blonde'
          p.hair_color = 'Blond'
        when 'Grey'
          p.hair_color = 'Gray'
        when 'Salt & Pepper'
          p.hair_color = 'Salt&Pepper'
        else
          p.hair_color = p.old_hair_color.long_name
        end
      end
      unless p.old_hair_type.nil?
        p.hair_type = p.old_hair_type.long_name
      end
      unless p.old_complexion.nil?
        case p.old_complexion.long_name
        when 'Acne'
          p.complexion = 'Acned'
        when 'Freckled'
          p.complexion = 'Freckled'
        when 'Other'
          # skip this
        when 'Pale'
          p.complexion = 'Pale'
        when 'Pocked'
          p.complexion = 'Pocked'
        when 'Ruddy'
          p.complexion = 'Ruddy'
        when 'Tanned'
          p.complexion = 'Tanned'
        else
          p.skin_tone = p.old_complexion.long_name
        end
      end
      unless p.old_facial_hair.nil?
        case p.old_facial_hair.long_name
        when 'Beard'
          p.beard = 'Medium'
        when 'Beard & Mustache','Beard/Mustache'
          p.beard = 'Medium'
          p.mustache = 'Medium'
        when 'Bushy Eyebrows'
          p.eyebrow = 'Bushy'
        when 'Fu-manchu'
          p.beard = 'Fu-Manchu'
        when 'Goatee'
          p.beard = 'Goatee'
        when 'Long Beard'
          p.beard = 'Long'
        when 'Mustache'
          p.mustache = 'Medium'
        when 'Mustache/Goatee'
          p.beard = 'Goatee'
          p.mustache = 'Medium'
        when 'Short Beard'
          p.beard = 'Short'
        when 'Side Burns','Sideburns'
          p.beard = 'Sideburns'
        when 'Side Burns/Bushy Eyebrows', 'Sideburns & Bushy Eyebrows'
          p.beard = 'Sideburns'
          p.eyebrow = 'Bushy'
        when 'Side Burns/Mustache', 'Sideburns & Mustache'
          p.mustache = 'Medium'
          p.beard = 'Sideburns'
        when 'Side Burns/Mustache/Bushy Eyebrows', 'Sideburns, Mustache & Bushy Eyebrows'
          p.mustache = 'Medium'
          p.beard = 'Sideburns'
          p.eyebrow = 'Bushy'
        when 'Unshaven'
          p.beard = 'Unshaven'
        end
      end
      if p.changed?
        p.save
      end
    end
    
    ConvertPhysicalTraits::Warrant.all.each do |w|
      unless w.old_race.nil?
        if w.old_race.long_name == 'Am. Indian'
          w.race = 'American Indian'
        else
          w.race = w.old_race.long_name
        end
        w.save
      end
    end
    
    # remove old person=>option fields
    remove_column :people, :race_id
    remove_column :people, :build_id
    remove_column :people, :hair_type_id
    remove_column :people, :hair_color_id
    remove_column :people, :eye_color_id
    remove_column :people, :complexion_id
    remove_column :people, :facial_hair_id
    remove_column :warrants, :race_id
    
    # remove option types
    ConvertPhysicalTraits::Option.find_by_option_type("Race").each{|o| o.destroy}
    ConvertPhysicalTraits::Option.find_by_option_type("Build").each{|o| o.destroy}
    ConvertPhysicalTraits::Option.find_by_option_type("Hair Color").each{|o| o.destroy}
    ConvertPhysicalTraits::Option.find_by_option_type("Hair Type").each{|o| o.destroy}
    ConvertPhysicalTraits::Option.find_by_option_type("Eye Color").each{|o| o.destroy}
    ConvertPhysicalTraits::Option.find_by_option_type("Complexion").each{|o| o.destroy}
    ConvertPhysicalTraits::Option.find_by_option_type("Facial Hair").each{|o| o.destroy}
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
