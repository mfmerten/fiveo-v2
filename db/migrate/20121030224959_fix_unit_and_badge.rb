class FixUnitAndBadge < ActiveRecord::Migration
  class Contact < ActiveRecord::Base
  end
  class InvestContact < ActiveRecord::Base
  end
  
  def up
    FixUnitAndBadge::Contact.all.each do |c|
      unless c.unit.nil?
        c.unit = c.unit.gsub(/\s/,'')
      end
      unless c.badge_no.nil?
        c.badge_no = c.badge_no.gsub(/\s/,'')
      end
      if c.changed?
        c.save
      end
    end
    FixUnitAndBadge::InvestContact.all.each do |c|
      unless c.unit.nil?
        c.unit = c.unit.gsub(/\s/,'')
      end
      unless c.badge_no.nil?
        c.badge_no = c.badge_no.gsub(/\s/,'')
      end
      if c.changed?
        c.save
      end
    end
  end

  def down
    # nothing to do
  end
end
