class AddToSentence < ActiveRecord::Migration
  def up
    add_column :sentences, :pay_during_prob, :boolean, :default => false
    add_column :probations, :pay_during_prob, :boolean, :default => false
    add_column :courts, :writ_of_attach, :boolean, :default => false
    add_column :courts, :not_present, :boolean, :default => false
  end

  def down
    remove_column :sentences, :pay_during_prob
    remove_column :probations, :pay_during_prob
    remove_column :courts, :writ_of_attach
    remove_column :courts, :not_present
  end
end
