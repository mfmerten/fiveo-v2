class RenameAfisReportsDispositions < ActiveRecord::Migration
  def up
    remove_index :afis_reports, :atn
    remove_index :afis_reports, :created_by
    remove_index :afis_reports, :person_id
    remove_index :afis_reports, :status
    remove_index :afis_reports, :updated_by
    rename_table :afis_reports, :dispositions
    add_index :dispositions, :atn
    add_index :dispositions, :person_id
    add_index :dispositions, :status
    add_index :dispositions, :created_by
    add_index :dispositions, :updated_by
  end

  def down
    remove_index :dispositions, :atn
    remove_index :dispositions, :person_id
    remove_index :dispositions, :status
    remove_index :dispositions, :created_by
    remove_index :dispositions, :updated_by
    rename_table :dispositions, :afis_reports
    add_index :afis_reports, :atn
    add_index :afis_reports, :created_by
    add_index :afis_reports, :person_id
    add_index :afis_reports, :status
    add_index :afis_reports, :updated_by
  end
end
