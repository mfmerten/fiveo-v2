class AddBondingCompaniesJoin < ActiveRecord::Migration
  def up
    create_table :bonding_companies, :id => false, :force => true do |t|
      t.integer :bondsman_id
      t.integer :company_id
    end
    add_index :bonding_companies, :bondsman_id
    add_index :bonding_companies, :company_id
    
    add_column :contacts, :bonding_company, :boolean, :default => false
    add_column :bonds, :bonding_company_id, :integer
  end

  def down
    drop_table :bonding_companies
    remove_column :contacts, :bonding_company
    remove_column :bonds, :bonding_company_id
  end
end
