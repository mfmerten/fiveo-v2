class FinishUnlinkingReports < ActiveRecord::Migration
  class InvestReport < ActiveRecord::Base
    has_many :invest_photos, :class_name => 'FinishUnlinkingReports::InvestPhoto'
    has_many :invest_criminals, :class_name => 'FinishUnlinkingReports::InvestCriminal'
    has_many :invest_vehicles, :class_name => 'FinishUnlinkingReports::InvestVehicle'
    has_and_belongs_to_many :photos, :class_name => 'FinishUnlinkingReports::InvestPhoto', :join_table => 'report_photos'
    has_and_belongs_to_many :criminals, :class_name => 'FinishUnlinkingReports::InvestCriminal', :join_table => 'report_criminals'
    has_and_belongs_to_many :vehicles, :class_name => 'FinishUnlinkingReports::InvestVehicle', :join_table => 'report_vehicles'
    has_and_belongs_to_many :victims, :class_name => 'FinishUnlinkingReports::Person', :join_table => 'report_victims'
    has_and_belongs_to_many :witnesses, :class_name => 'FinishUnlinkingReports::Person', :join_table => 'report_witnesses'
    has_and_belongs_to_many :sources, :class_name => 'FinishUnlinkingReports::Person', :join_table => 'invest_report_sources'
    belongs_to :investigation, :class_name => 'FinishUnlinkingReports::Investigation'
  end
  class InvestPhoto < ActiveRecord::Base
    belongs_to :invest_report, :class_name => 'FinishUnlinkingReports::InvestReport'
    has_and_belongs_to_many :reports, :class_name => 'FinishUnlinkingReports::InvestReport', :join_table => 'report_photos'
  end
  class InvestCriminal < ActiveRecord::Base
    belongs_to :invest_report, :class_name => 'FinishUnlinkingReports::InvestReport'
    has_and_belongs_to_many :reports, :class_name => 'FinishUnlinkingReports::InvestReport', :join_table => 'report_criminals'
  end
  class InvestVehicle < ActiveRecord::Base
    belongs_to :invest_report, :class_name => 'FinishUnlinkingReports::InvestReport'
    has_and_belongs_to_many :reports, :class_name => 'FinishUnlinkingReports::InvestReport', :join_table => 'report_vehicles'
  end
  class Person < ActiveRecord::Base
  end
  class Investigation < ActiveRecord::Base
    has_many :reports, :class_name => 'FinishUnlinkingReports::InvestReport'
    has_and_belongs_to_many :victims, :class_name => 'FinishUnlinkingReports::Person', :join_table => 'invest_victims'
    has_and_belongs_to_many :witnesses, :class_name => 'FinishUnlinkingReports::Person', :join_table => 'invest_witnesses'
    has_and_belongs_to_many :sources, :class_name => 'FinishUnlinkingReports::Person', :join_table => 'invest_sources'
  end
  def up
    # add join table for photos
    create_table :report_photos, :id => false, :force => true do |t|
      t.integer :invest_report_id
      t.integer :invest_photo_id
    end
    add_index :report_photos, :invest_report_id
    add_index :report_photos, :invest_photo_id
    # add join table for criminals
    create_table :report_criminals, :id => false, :force => true do |t|
      t.integer :invest_report_id
      t.integer :invest_criminal_id
    end
    add_index :report_criminals, :invest_report_id
    add_index :report_criminals, :invest_criminal_id
    # add join table for vehicles
    create_table :report_vehicles, :id => false, :force => true do |t|
      t.integer :invest_report_id
      t.integer :invest_vehicle_id
    end
    add_index :report_vehicles, :invest_report_id
    add_index :report_vehicles, :invest_vehicle_id
    
    FinishUnlinkingReports::InvestPhoto.reset_column_information
    FinishUnlinkingReports::InvestCriminal.reset_column_information
    FinishUnlinkingReports::InvestVehicle.reset_column_information
    FinishUnlinkingReports::InvestReport.reset_column_information
    
    FinishUnlinkingReports::InvestReport.all.each do |r|
      # convert photos
      unless r.invest_photos.empty?
        r.photo_ids = r.invest_photos.collect{|p| p.id}
      end
      # convert criminals
      unless r.invest_criminals.empty?
        r.criminal_ids = r.invest_criminals.collect{|c| c.id}
      end
      # convert vehicles
      unless r.invest_vehicles.empty?
        r.vehicle_ids = r.invest_vehicles.collect{|v| v.id}
      end
      # merge sources
      unless r.sources.empty?
        r.investigation.sources.merge(r.sources)
      end
      # merge victims
      unless r.victims.empty?
        r.investigation.victims.merge(r.victims)
      end
      # merge witnesses
      unless r.witnesses.empty?
        r.investigation.witnesses.merge(r.witnesses)
      end
      r.investigation.save(:validate => false)
      r.save(:validate => false)
    end
    
    # drop report sources
    drop_table :invest_report_sources
    # break photo links
    remove_column :invest_photos, :invest_report_id
    # break criminal links
    remove_column :invest_criminals, :invest_report_id
    # break vehicle links
    remove_column :invest_vehicles, :invest_report_id
    # report victims and witnesses joins stay the same, will just be handled
    # differently on the edit view
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
