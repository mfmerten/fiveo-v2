class FixWarrantJoins < ActiveRecord::Migration
  class Warrant < ActiveRecord::Base
    has_and_belongs_to_many :warrants_exemptions, :class_name => 'FixWarrantJoins::Person', :join_table => 'warrants_exemptions'
    has_many :warrant_exemptions, :class_name => 'FixWarrantJoins::WarrantExemption'
    has_many :exempted_people, :through => :warrant_exemptions, :source => :person
    belongs_to :arrest, :class_name => 'FixWarrantJoins::Arrest'
  end
  
  class Person < ActiveRecord::Base
    has_and_belongs_to_many :warrants_exemptions, :class_name => "FixWarrantJoins::Warrant", :join_table => 'warrants_exemptions'
    has_many :warrant_exemptions, :class_name => 'FixWarrantJoins::WarrantExemption'
    has_many :exempted_warrants, :through => :warrant_exemptions, :source => :warrant
  end
  
  class WarrantExemption < ActiveRecord::Base
    belongs_to :warrant, :class_name => 'FixWarrantJoins::Warrant'
    belongs_to :person, :class_name => 'FixWarrantJoins::Person'
  end
  
  class Arrest < ActiveRecord::Base
    has_many :warrants, :class_name => 'FixWarrantJoins::Warrant'
    has_many :officers, :class_name => 'FixWarrantJoins::ArrestOfficer'
  end
  
  class ArrestOfficer < ActiveRecord::Base
    belongs_to :arrest, :class_name => 'FixWarrantJoins::Arrest'
  end
  
  def up
    remove_index :warrant_exemptions, :warrant_id
    remove_index :warrant_exemptions, :person_id
    rename_table :warrant_exemptions, :warrants_exemptions
    add_index :warrants_exemptions, :warrant_id
    add_index :warrants_exemptions, :person_id
    
    create_table :warrant_exemptions, :force => true do |t|
      t.integer :warrant_id
      t.integer :person_id
      t.timestamps
    end
    add_index :warrant_exemptions, :warrant_id
    add_index :warrant_exemptions, :person_id
    
    FixWarrantJoins::WarrantExemption.reset_column_information
    FixWarrantJoins::Warrant.reset_column_information
    FixWarrantJoins::Person.reset_column_information
    
    add_column :arrests, :warrants_count, :integer, :null => false, :default => 0
    add_column :arrests, :officers_count, :integer, :null => false, :default => 0
    
    FixWarrantJoins::Arrest.reset_column_information
    
    FixWarrantJoins::Warrant.all.each do |w|
      w.exempted_person_ids = w.warrants_exemption_ids
    end
    FixWarrantJoins::Arrest.all.each do |a|
      a.update_attribute(:warrants_count, a.warrants.length)
      a.update_attribute(:officers_count, a.officers.length)
    end
    
    drop_table :warrants_exemptions
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
