class FixChargeType < ActiveRecord::Migration
  class Arrest < ActiveRecord::Base
    has_many :district_charges, :class_name => 'FixChargeType::Charge', :foreign_key => 'd_arrest_id', :dependent => :destroy
    has_many :city_charges, :class_name => 'FixChargeType::Charge', :foreign_key => 'c_arrest_id', :dependent => :destroy
    has_many :other_charges, :class_name => 'FixChargeType::Charge', :foreign_key => 'o_arrest_id', :dependent => :destroy
    has_and_belongs_to_many :district_warrants, :class_name => 'FixChargeType::Warrant', :join_table => 'district_warrants'
    has_and_belongs_to_many :city_warrants, :class_name => 'FixChargeType::Warrant', :join_table => 'city_warrants'
    has_and_belongs_to_many :other_warrants, :class_name => 'FixChargeType::Warrant', :join_table => 'other_warrants'
    has_many :charges, :class_name => 'FixChargeType::Charge'
    has_many :warrants, :class_name => 'FixChargeType::Warrant'
  end
  class Charge < ActiveRecord::Base
    belongs_to :district_arrest, :class_name => 'FixChargeType::Arrest', :foreign_key => 'd_arrest_id'
    belongs_to :city_arrest, :class_name => 'FixChargeType::Arrest', :foreign_key => 'c_arrest_id'
    belongs_to :other_arrest, :class_name => 'FixChargeType::Arrest', :foreign_key => 'o_arrest_id'
    belongs_to :warrant, :class_name => 'FixChargeType::Warrant'
    belongs_to :arrest_warrant, :class_name => 'FixChargeType::Warrant', :foreign_key => 'arrest_warrant_id'
    belongs_to :arrest, :class_name => 'FixChargeType::Arrest'
    belongs_to :citation, :class_name => 'FixChargeType::Citation'
    def get_arrest
      if !district_arrest.nil?
        district_arrest
      elsif !city_arrest.nil?
        city_arrest
      elsif !other_arrest.nil?
        other_arrest
      elsif !arrest.nil?
        arrest
      else
        nil
      end
    end
  end
  class Warrant < ActiveRecord::Base
    has_many :charges, :class_name => 'FixChargeType::Charge'
    has_many :arrest_charges, :class_name => 'FixChargeType::Charge', :foreign_key => 'arrest_warrant_id'
    has_and_belongs_to_many :district_arrests, :class_name => 'FixChargeType::Arrest', :join_table => 'district_warrants'
    has_and_belongs_to_many :city_arrests, :class_name => 'FixChargeType::Arrest', :join_table => 'city_warrants'
    has_and_belongs_to_many :other_arrests, :class_name => 'FixChargeType::Arrest', :join_table => 'other_warrants'
    belongs_to :arrest, :class_name => 'FixChargeType::Arrest'
    
    # returns the latest arrest linked to this warrant
    def arrest
      temp = district_arrests + city_arrests + other_arrests
      temp = temp.flatten.compact.uniq
      return nil if temp.empty?
      temp.sort{|a,b| b.id <=> a.id}.first
    end
    
    # this is called when charges are dual-linked as arrest_charges
    # instead of being duplicated like they are supposed to be
    def duplicate_charges
      # this turns charges into arrest_charges then creates new charges
      # to preserve any docket charge links
      dual_charges = charges(true).clone
      charges.clear
      dual_charges.each do |c|
        charges.create(:charge => c.charge, :count => c.count, :charge_type => c.charge_type, :created_by => updated_by, :updated_by => updated_by)
      end
    end
  end
  class Citation < ActiveRecord::Base
    has_many :charges, :class_name => 'FixChargeType::Charge'
  end

  ArrestType = {0 => "District", 1 => "City", 2 => "Other"}
  
  def up
    # add the necessary new columns
    add_column :charges, :arrest_id, :integer
    add_index :charges, :arrest_id
    add_column :charges, :charge_type, :string
    add_column :warrants, :charge_type, :string
    add_column :warrants, :arrest_id, :integer
    add_index :warrants, :arrest_id
    
    FixChargeType::Warrant.reset_column_information
    FixChargeType::Charge.reset_column_information
    FixChargeType::Arrest.reset_column_information
    
    # set all citation charges to charge type 'District'
    cnt = 0
    say_with_time "Updating Citation Charges..." do
      FixChargeType::Citation.all.each do |t|
        cnt += t.charges.size
        t.charges.each{|c| c.update_attribute(:charge_type, 'District')}
      end
    end
    say "#{cnt} Citation Charges Updated."
    
    # set arrest_id and charge_type for charges from arrest.id and
    #arrest.arrest_type for all existing arrests
    cnt = 0
    say_with_time "Updating Arrest charges..." do
      FixChargeType::Arrest.all.each do |a|
        a.district_charges.each do |c|
          c.charge_type = 'District'
          c.arrest_id = a.id
          c.save
          cnt += 1
        end
        a.city_charges.each do |c|
          c.charge_type = 'City'
          c.arrest_id = a.id
          c.save
          cnt += 1
        end
        a.other_charges.each do |c|
          c.charge_type = 'Other'
          c.arrest_id = a.id
          c.save
          cnt += 1
        end
      end
    end
    say "#{cnt} Arrest Charges Updated."
    
    # now, for every warrant linked to an arrest, make sure it is 'served',
    # doublecheck that the charges have been duplicated on the arrest,
    # make sure the charge_type is set correctly, then
    # remove the warrant charges and update the warrant-arrest charges with
    # warrant_id from warrant.id
    # warrant charges = charges
    # duplicated arrest charges = arrest_charges
    cnt_updated = 0
    cnt_removed = 0
    cnt_skipped = 0
    say_with_time "Updating Warrant charges..." do
      FixChargeType::Warrant.all.each do |w|
        # set up some useful variables and flags to save typing
        # if direct warrant links to arrest, set charge type from link type
        if !w.district_arrests.empty?
          warrant_charge_type = 'District'
        elsif !w.city_arrests.empty?
          warrant_charge_type = 'City'
        elsif !w.other_arrests.empty?
          warrant_charge_type = 'Other'
        else
          warrant_charge_type = nil
        end
        
        # set flags to simplify further processing
        # is the warrant linked to an arrest?
        warrant_linked = !warrant_charge_type.nil?
        # are arrest_charges and charges actually the same thing? (they normally are copies of each other).
        dual_linked = (w.arrest_charge_ids.sort == w.charge_ids.sort)
        # are there actually warrant charges?
        has_charges = !w.charges.empty?
        # get a subset of charges that are actually linked
        linked_charges = w.charges.reject{|c| c.district_arrest.nil? && c.city_arrest.nil? && c.other_arrest.nil?}
        # are there linked warrant charges?
        has_linked_charges = !linked_charges.empty?
        # are there arrest charges?
        has_arrest_charges = !w.arrest_charges.empty?
        # get a subset of arrest_charges that actually are linked
        linked_arrest_charges = w.arrest_charges.reject{|c| c.district_arrest.nil? && c.city_arrest.nil? && c.other_arrest.nil?}
        # are arrest charges linked?
        has_linked_arrest_charges = !linked_arrest_charges.empty?
        # is the warrant disposition 'Served'?
        warrant_served = (w.disposition == 'Served')
        # try to find an arrest the linked arrest if possible
        arrest = w.arrest 
        if arrest.nil? && has_linked_arrest_charges
          arrest = linked_arrest_charges.first.get_arrest
        end
        if arrest.nil? && has_linked_charges
          arrest = linked_charges.first.get_arrest
        end
        
        unless has_linked_charges || has_linked_arrest_charges
          # no linked charges, cannot process as served on warrant
          # clear any prior processing artifacts without touching
          # disposition
          if warrant_linked
            w.district_arrests.clear
            w.city_arrests.clear
            w.other_arrests.clear
            w.save
          end
          if has_arrest_charges
            if dual_linked
              w.arrest_charges.clear
            else
              cnt_removed += w.arrest_charges.size
              w.arrest_charges.each{|c| c.destroy}
            end
          else
            cnt_skipped += w.charges.size
          end
          next
        end
        
        # at this point we have linked charges, need to update warrant and
        # charges for new layout

        # process charges and update the warrant as served
        if warrant_linked
          w.charge_type = warrant_charge_type
        else
          w.charge_type = FixChargeType::ArrestType[arrest.arrest_type]
        end
        w.arrest_id = arrest.id
        if dual_linked
          w.arrest_charges.clear
          charges = w.charges
        elsif has_linked_arrest_charges
          cnt_removed += w.charges.size
          w.charges.each{|c| c.destroy}
          charges = w.arrest_charges
        else
          cnt_removed += w.arrest_charges.size
          w.arrest_charges.each{|c| c.destroy}
          charges = w.charges
        end
        charges.each do |c|
          # set charge type if needed
          if c.charge_type.blank?
            if warrant_linked
              c.charge_type = warrant_charge_type
            else
              c.charge_type = FixChargeType::ArrestType[arrest.arrest_type]
            end
          end
          # set warrant_id (dual-links the charge)
          if c.warrant_id != w.id
            c.warrant_id = w.id
          end
          if c.arrest_id.blank?
            c.arrest_id = arrest.id
          end
          if c.changed?
            c.save
          end
          cnt_updated += 1
        end
        # set warrant disposition if needed
        unless warrant_served
          w.disposition = "Served"
          w.dispo_date = arrest.arrest_datetime.to_date
        end
        w.save
        # done with this warrant
      end
    end
    say "#{cnt_updated} Warrant charges updated."
    say "#{cnt_updated} Warrant charges removed."
    say "#{cnt_skipped} Warrant charges not related to arrest and not updated."
    
    drop_table :district_warrants
    drop_table :city_warrants
    drop_table :other_warrants
    remove_column :charges, :d_arrest_id
    remove_column :charges, :c_arrest_id
    remove_column :charges, :o_arrest_id
    remove_column :charges, :arrest_warrant_id
    remove_column :arrests, :arrest_type
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
