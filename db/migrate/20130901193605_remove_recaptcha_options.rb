class RemoveRecaptchaOptions < ActiveRecord::Migration
  class SiteStore < ActiveRecord::Base
  end
  
  def up
    ["recaptcha_enabled","recaptcha_public_key","recaptcha_private_key","recaptcha_proxy"].each do |s|
      settings = SiteStore.find_all_by_setting_name(s)
      settings.each do |set|
        set.destroy
      end
    end
  end

  def down
  end
end
