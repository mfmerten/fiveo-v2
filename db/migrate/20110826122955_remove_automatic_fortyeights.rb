class RemoveAutomaticFortyeights < ActiveRecord::Migration
  class OptionType < ActiveRecord::Base
    has_many :options, :class_name => 'RemoveAutomaticFortyeights::Option'
  end
  class Option < ActiveRecord::Base
    belongs_to :option_type, :class_name => 'RemoveAutomaticFortyeights::OptionType'
    def self.get_option_type(option_type)
      ot = RemoveAutomaticFortyeights::OptionType.find_by_name(option_type)
      if ot.nil?
        nil
      else
        ot.id
      end
    end
  end
  class Hold < ActiveRecord::Base
    belongs_to :hold_type, :class_name => 'RemoveAutomaticFortyeights::Option', :foreign_key => 'hold_type_id'
    belongs_to :arrest, :class_name => 'RemoveAutomaticFortyeights::Arrest'
  end
  class Arrest < ActiveRecord::Base
    has_many :holds, :class_name => 'RemoveAutomaticFortyeights::Hold'
  end
  
  def self.up
    # Change 48 Hour to a manual type
    if table_exists?(:option_types)
      if ot = RemoveAutomaticFortyeights::OptionType.find_by_name("Hold Type")
        if o = RemoveAutomaticFortyeights::Option.where({:option_type_id => ot.id, :long_name => "48 Hour"}).first
          o.update_attribute(:program_only, false)
        end
      end
    else
      ot = RemoveAutomaticFortyeights::Option.get_option_type('Hold Type')
      if o = RemoveAutomaticFortyeights::Option.where({:option_type => ot, :long_name => "48 Hour"}).first
        o.update_attribute(:program_only, false)
      end
    end
    
    # Remove all 48 hour holds added by arrest regardless of cleared or not
    RemoveAutomaticFortyeights::Hold.all.each do |h|
      if !h.hold_type.nil? && h.hold_type.long_name == '48 Hour' && !h.arrest.nil?
        h.destroy
      end
    end
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
