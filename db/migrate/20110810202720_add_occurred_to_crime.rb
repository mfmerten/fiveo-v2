class AddOccurredToCrime < ActiveRecord::Migration
  def self.up
    unless column_exists?(:invest_crimes, :occurred_date)
      add_column :invest_crimes, :occurred_date, :date
      add_column :invest_crimes, :occurred_time, :string
    end
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
