class AddSiteImageModel < ActiveRecord::Migration
  class SiteConfiguration < ActiveRecord::Base
  end
  class SiteImage < ActiveRecord::Base
  end
  
  def up
    create_table "site_images", :force => true do |t|
      t.string   "logo_file_name"
      t.string   "logo_content_type"
      t.integer  "logo_file_size"
      t.datetime "logo_updated_at"
      t.string   "ceo_img_file_name"
      t.string   "ceo_img_content_type"
      t.integer  "ceo_img_file_size"
      t.datetime "ceo_img_updated_at"
      t.string   "header_left_img_file_name"
      t.string   "header_left_img_content_type"
      t.integer  "header_left_img_file_size"
      t.datetime "header_left_img_updated_at"
      t.string   "header_right_img_file_name"
      t.string   "header_right_img_content_type"
      t.integer  "header_right_img_file_size"
      t.datetime "header_right_img_updated_at"
    end
  
    AddSiteImageModel::SiteImage.reset_column_information
    # defaults
    AddSiteImageModel::SiteImage.create
    # current
    conf = AddSiteImageModel::SiteConfiguration.last
    images = AddSiteImageModel::SiteImage.create(
      :logo_file_name => conf.logo_file_name,
      :logo_content_type => conf.logo_content_type,
      :logo_file_size => conf.logo_file_size,
      :logo_updated_at => conf.logo_updated_at,
      :ceo_img_file_name => conf.ceo_img_file_name,
      :ceo_img_content_type => conf.ceo_img_content_type,
      :ceo_img_file_size => conf.ceo_img_file_size,
      :ceo_img_updated_at => conf.ceo_img_updated_at,
      :header_left_img_file_name => conf.header_left_img_file_name,
      :header_left_img_content_type => conf.header_left_img_content_type,
      :header_left_img_file_size => conf.header_left_img_file_size,
      :header_left_img_updated_at => conf.header_left_img_updated_at,
      :header_right_img_file_name => conf.header_right_img_file_name,
      :header_right_img_content_type => conf.header_right_img_content_type,
      :header_right_img_file_size => conf.header_right_img_file_size,
      :header_right_img_updated_at => conf.header_right_img_updated_at
    )
    
    remove_column :site_configurations, :logo_file_name
    remove_column :site_configurations, :logo_content_type
    remove_column :site_configurations, :logo_file_size
    remove_column :site_configurations, :logo_updated_at
    remove_column :site_configurations, :ceo_img_file_name
    remove_column :site_configurations, :ceo_img_content_type
    remove_column :site_configurations, :ceo_img_file_size
    remove_column :site_configurations, :ceo_img_updated_at
    remove_column :site_configurations, :header_left_img_file_name
    remove_column :site_configurations, :header_left_img_content_type
    remove_column :site_configurations, :header_left_img_file_size
    remove_column :site_configurations, :header_left_img_updated_at
    remove_column :site_configurations, :header_right_img_file_name
    remove_column :site_configurations, :header_right_img_content_type
    remove_column :site_configurations, :header_right_img_file_size
    remove_column :site_configurations, :header_right_img_updated_at
  end

  def down
    add_column :site_configurations, :logo_file_name, :string 
    add_column :site_configurations, :logo_content_type, :string
    add_column :site_configurations, :logo_file_size, :integer
    add_column :site_configurations, :logo_updated_at, :datetime
    add_column :site_configurations, :ceo_img_file_name, :string
    add_column :site_configurations, :ceo_img_content_type, :string
    add_column :site_configurations, :ceo_img_file_size, :integer
    add_column :site_configurations, :ceo_img_updated_at, :datetime
    add_column :site_configurations, :header_left_img_file_name, :string
    add_column :site_configurations, :header_left_img_content_type, :string
    add_column :site_configurations, :header_left_img_file_size, :integer
    add_column :site_configurations, :header_left_img_updated_at, :datetime
    add_column :site_configurations, :header_right_img_file_name, :string
    add_column :site_configurations, :header_right_img_content_type, :string
    add_column :site_configurations, :header_right_img_file_size, :integer
    add_column :site_configurations, :header_right_img_updated_at, :datetime
    
    AddSiteImageModel::SiteConfiguration.reset_column_information
    conf = AddSiteImageModel::SiteImage.last
    AddSiteImageModel::SiteConfiguration.last.update_attributes(conf.attributes)
    
    drop_table :site_images
  end
end
