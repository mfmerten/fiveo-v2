class PossibleWarrantForbidJoinTables < ActiveRecord::Migration
  class Person < ActiveRecord::Base
    has_many :warrant_possibles, class_name: "PossibleWarrantForbidJoinTables::WarrantPossible"
    has_many :forbid_possibles, class_name: "PossibleWarrantForbidJoinTables::ForbidPossible"
    has_many :pwarrants, through: :warrant_possibles, source: :warrant
    has_many :pforbids, through: :forbid_possibles, source: :forbid
    has_many :warrant_exemptions, class_name: "PossibleWarrantForbidJoinTables::WarrantExemption"
    has_many :exempted_warrants, through: :warrant_exemptions, source: :warrant
    has_many :forbid_exemptions, class_name: "PossibleWarrantForbidJoinTables::ForbidExemption"
    has_many :exempted_forbids, through: :forbid_exemptions, source: :forbid
    
    attr_encrypted :ssn
  
    scope :with_name, ->(n) { AppUtils.for_person self, n }
  
    def self.for_warrant(warrant_id)
      return none unless w = Warrant.find_by(id: warrant_id)
      return none unless w.person.nil?
      result = with_name(w.sort_name)
      unless w.encrypted_ssn.blank?
        result = result.where("people.encrypted_ssn = ? OR people.encrypted_ssn = '' OR people.encrypted_ssn IS NULL",w.encrypted_ssn)
      end
      unless w.oln.blank?
        result = result.where("LOWER(people.oln) LIKE LOWER(?) OR people.oln = ''",w.oln)
      end
      unless w.oln_state.blank?
        result = result.where("LOWER(people.oln_state) = LOWER(?) OR people.oln_state = ''",w.oln_state)
      end
      if w.dob?
        result = result.where("people.date_of_birth = ? or people.date_of_birth IS NULL", w.dob)
      end
      result
    end
  
    def self.for_forbid(forbid_id)
      return none unless f = Forbid.find_by(id: forbid_id)
      return none unless f.person.nil?
      with_name(f.sort_name)
    end
  end
  
  class Warrant < ActiveRecord::Base
    has_many :warrant_possibles, class_name: "PossibleWarrantForbidJoinTables::WarrantPossible"
    has_many :ppeople, through: :warrant_possibles, source: :person
    belongs_to :person, class_name: "PossibleWarrantForbidJoinTables::Person"
    
    attr_encrypted :ssn
  
    def sort_name
      n = Namae::Name.new(family: family_name, given: given_name, suffix: suffix)
      n.sort_order
    end
  end

  class Forbid < ActiveRecord::Base
    has_many :forbid_possibles, class_name: "PossibleWarrantForbidJoinTables::ForbidPossible"
    has_many :ppeople, through: :forbid_possibles, source: :person
    belongs_to :person, class_name: "PossibleWarrantForbidJoinTables::Person"
    
    def sort_name
      n = Namae::Name.new(family: family_name, given: given_name, suffix: suffix)
      n.sort_order
    end
  end
  
  class WarrantExemption < ActiveRecord::Base
    belongs_to :warrant, class_name: "PossibleWarrantForbidJoinTables::Warrant"
    belongs_to :person, class_name: "PossibleWarrantForbidJoinTables::Person"
  end
  
  class ForbidExemption < ActiveRecord::Base
    belongs_to :forbid, class_name: "PossibleWarrantForbidJoinTables::Forbid"
    belongs_to :person, class_name: "PossibleWarrantForbidJoinTables::Person"
  end
  
  class WarrantPossible < ActiveRecord::Base
    belongs_to :person, class_name: "PossibleWarrantForbidJoinTables::Person"
    belongs_to :warrant, class_name: "PossibleWarrantForbidJoinTables::Warrant"
  end
  
  class ForbidPossible < ActiveRecord::Base
    belongs_to :person, class_name: "PossibleWarrantForbidJoinTables::Person"
    belongs_to :forbid, class_name: "PossibleWarrantForbidJoinTables::Forbid"
  end
  
  def up
    # create warrant_possibles join table
    create_table :warrant_possibles, force: true do |t|
      t.timestamps
    end
    add_reference :warrant_possibles, :warrant, index: true, foreign_key: true
    add_reference :warrant_possibles, :person, index: true, foreign_key: true
    
    # create forbid_possibles join table
    create_table :forbid_possibles, force: true do |t|
      t.timestamps
    end
    add_reference :forbid_possibles, :forbid, index: true, foreign_key: true
    add_reference :forbid_possibles, :person, index: true, foreign_key: true
    
    say_with_time "Updating Possible Warrants..." do
      PossibleWarrantForbidJoinTables::Warrant.find_each do |w|
        matches = Person.for_warrant(w.id)
        matches.each do |p|
          w.ppeople << p unless p.exempted_warrants.include?(w)
        end
      end
    end
    
    say_with_time "Updating Possible Forbids..." do
      PossibleWarrantForbidJoinTables::Forbid.find_each do |f|
        matches = Person.for_forbid(f.id)
        matches.each do |p|
          f.ppeople << p unless p.exempted_forbids.include?(f)
        end
      end
    end
  end
  
  def down
    drop_table :warrant_possibles
    drop_table :forbid_possibles
  end
end
