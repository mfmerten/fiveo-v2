class RenameBugsTable < ActiveRecord::Migration
  def up
    rename_table :bugs, :support_tickets
    rename_table :bug_comments, :support_ticket_comments
    rename_column :support_ticket_comments, :bug_id, :support_ticket_id
    rename_column :watchers, :bug_id, :support_ticket_id
    rename_column :site_configurations, :bug_admin_id, :support_ticket_admin_id
    rename_column :users, :notify_bug_resolved, :notify_support_ticket_resolved
    rename_column :users, :notify_bug_new, :notify_support_ticket_new
    rename_column :users, :no_self_bug_messages, :no_self_support_ticket_messages
  end

  def down
    rename_table :support_tickets, :bugs
    rename_table :support_ticket_comments, :bug_comments
    rename_column :bug_comments, :support_ticket_id, :bug_id
    rename_column :watchers, :support_ticket_id, :bug_id
    rename_column :site_configurations, :support_ticket_admin_id, :bug_admin_id
    rename_column :users, :notify_support_ticket_resolved, :notify_bug_resolved
    rename_column :users, :notify_support_ticket_new, :notify_bug_new
    rename_column :users, :no_self_support_ticket_messages, :no_self_bug_messages
  end
end
