class AddCodeToStatutes < ActiveRecord::Migration
  class Statute < ActiveRecord::Base
  end
  
  def up
    add_column :statutes, :code, :string, :null => false, :default => ''
    add_index :statutes, :code
    
    AddCodeToStatutes::Statute.reset_column_information
    AddCodeToStatutes::Statute.all.each do |s|
      if s.name =~ /^[Rr]\.?\s?[Ss]\.? [0-9]+:?[0-9]*.?[0-9]* .*$/
        s.update_attribute(:code, s.name.clone.sub(/^[Rr]\.?\s?[Ss]\.? ([0-9]+:?[0-9]*.?[0-9]*) .*$/,'\1'))
      end
    end
  end
  
  def down
    remove_column :statutes, :code
  end
end
