class FixGroupHabtm < ActiveRecord::Migration
  class Group < ActiveRecord::Base
    has_and_belongs_to_many :users, :class_name => 'FixGroupHabtm::User'
    has_many :user_groups, :class_name => 'FixGroupHabtm::UserGroup'
    has_many :xusers, :through => :user_groups, :source => :user
  end
  class User < ActiveRecord::Base
    has_and_belongs_to_many :groups, :class_name => 'FixGroupHabtm::Group'
    has_many :user_groups, :class_name => 'FixGroupHabtm::UserGroup'
    has_many :xgroups, :through => :user_groups, :source => :group
  end
  class UserGroup < ActiveRecord::Base
    belongs_to :user, :class_name => 'FixGroupHabtm::User'
    belongs_to :group, :class_name => 'FixGroupHabtm::Group'
  end
  
  def up
    create_table :user_groups, :force => true do |t|
      t.integer :user_id
      t.integer :group_id
      t.timestamps
    end
    add_index :user_groups, :user_id
    add_index :user_groups, :group_id
    
    FixGroupHabtm::UserGroup.reset_column_information
    FixGroupHabtm::Group.all.each do |g|
      g.xuser_ids = g.user_ids
    end
    
    drop_table :groups_users
  end

  def down
    create_table :groups_users, :id => false, :force => true do |t|
      t.integer :user_id
      t.integer :group_id
    end
    add_index :groups_users, :user_id
    add_index :groups_users, :group_id
    
    FixGroupHabtm::Group.all.each do |g|
      g.user_ids = g.xuser_ids
    end
    
    drop_table :user_groups
  end
end
