class AddNewNotifications < ActiveRecord::Migration
  class User < ActiveRecord::Base
  end
  
  def self.up
    unless column_exists?(:users, :notify_booking_release)
      # change existing notification field for consistency with new ones
      rename_column :users, :notify_release, :notify_booking_release
      # set up new notification fields
      add_column :users, :notify_booking_new, :boolean, :default => false
      add_column :users, :notify_arrest_new, :boolean, :default => false
      add_column :users, :notify_bug_new, :boolean, :default => false
      add_column :users, :notify_bug_resolved, :boolean, :default => false
      add_column :users, :notify_call_new, :boolean, :default => false
      add_column :users, :notify_offense_new, :boolean, :default => false
      add_column :users, :notify_forbid_new, :boolean, :default => false
      add_column :users, :notify_forbid_recalled, :boolean, :default => false
      add_column :users, :notify_pawn_new, :boolean, :default => false
      add_column :users, :notify_warrant_new, :boolean, :default => false
      add_column :users, :notify_warrant_resolved, :boolean, :default => false
    
      AddNewNotifications::User.reset_column_information
    
      AddNewNotifications::User.all.each do |u|
        if u.notify_booking_release?
          u.notify_booking_new = true
        end
        if u.review_forbid?
          u.notify_forbid_new = true
          u.notify_forbid_recalled = true
        end
        if u.review_arrest?
          u.notify_arrest_new = true
        end
        if u.review_pawn?
          u.notify_pawn_new = true
        end
        if u.review_bug?
          u.notify_bug_new = true
          u.notify_bug_resolved = true
        end
        if u.review_call?
          u.notify_call_new = true
        end
        if u.review_warrant?
          u.notify_warrant_new = true
          u.notify_warrant_resolved = true
        end
        u.save
      end
      
      # remove old review columns
      remove_column :users, :last_call
      remove_column :users, :last_pawn
      remove_column :users, :last_arrest
      remove_column :users, :last_bug
      remove_column :users, :last_forbid
      remove_column :users, :review_forbid
      remove_column :users, :review_arrest
      remove_column :users, :review_pawn
      remove_column :users, :review_bug
      remove_column :users, :review_call
      remove_column :users, :last_warrant
      remove_column :users, :review_warrant
    end
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
