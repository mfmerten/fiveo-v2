class DistrictWardZoneValues < ActiveRecord::Migration
  class SiteConfiguration < ActiveRecord::Base
  end
  
  def up
    add_column :site_configurations, :district_values, :string, :null => false, :default => ''
    add_column :site_configurations, :zone_values, :string, :null => false, :default => ''
    add_column :site_configurations, :ward_values, :string, :null => false, :default => ''
    
    DistrictWardZoneValues::SiteConfiguration.reset_column_information
    conf = DistrictWardZoneValues::SiteConfiguration.last
    unless conf.district == 0
      conf.district_values = (1..conf.district).to_a.join(',')
    end
    unless conf.ward == 0
      conf.ward_values = (1..conf.ward).to_a.join(',')
    end
    unless conf.zone == 0
      conf.zone_values = (1..conf.zone).to_a.join(',')
    end
    conf.save
    
    remove_column :site_configurations, :district
    remove_column :site_configurations, :zone
    remove_column :site_configurations, :ward
    
    change_column :arrests, :district, :string, :null => false, :default => ''
    change_column :arrests, :ward, :string, :null => false, :default => ''
    change_column :arrests, :zone, :string, :null => false, :default => ''
    change_column :calls, :district, :string, :null => false, :default => ''
    change_column :calls, :ward, :string, :null => false, :default => ''
    change_column :calls, :zone, :string, :null => false, :default => ''
  end

  def down
    change_column :arrests, :district, :integer, :null => false, :default => 0
    change_column :arrests, :ward, :integer, :null => false, :default => 0
    change_column :arrests, :zone, :integer, :null => false, :default => 0
    change_column :calls, :district, :integer, :null => false, :default => 0
    change_column :calls, :ward, :integer, :null => false, :default => 0
    change_column :calls, :zone, :integer, :null => false, :default => 0
    
    add_column :site_configurations, :district, :integer, :default => 0
    add_column :site_configurations, :ward, :integer, :default => 0
    add_column :site_configurations, :zone, :integer, :default => 0
    
    DistrictWardZoneValues::SiteConfiguration.reset_column_information
    conf = DistrictWardZoneValues::SiteConfiguration.last
    if conf.district_values.blank?
      conf.district = 0
    else
      conf.district = conf.district_values.split(',').collect{|x| x.to_i}.sort.last
    end
    if conf.ward_values.blank?
      conf.ward = 0
    else
      conf.ward = conf.ward_values.split(',').collect{|x| x.to_i}.sort.last
    end
    if conf.zone_values.blank?
      conf.zone = 0
    else
      conf.zone = conf.zone_values.split(',').collect{|x| x.to_i}.sort.last
    end
    conf.save
    
    remove_column :site_configurations, :district_values
    remove_column :site_configurations, :zone_values
    remove_column :site_configurations, :ward_values
  end
end
