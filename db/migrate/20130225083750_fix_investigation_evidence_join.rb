class FixInvestigationEvidenceJoin < ActiveRecord::Migration
  class Investigation < ActiveRecord::Base
    has_many :evidences, :class_name => 'FixInvestigationEvidenceJoin::Evidence', :dependent => :nullify
    has_many :invest_evidences, :class_name => 'FixInvestigationEvidenceJoin::InvestEvidence', :dependent => :destroy
    has_many :xevidences, :through => :invest_evidences, :source => :evidence
  end
  
  class InvestCrime < ActiveRecord::Base
    has_many :evidences, :class_name => 'FixInvestigationEvidenceJoin::Evidence', :dependent => :nullify
    has_many :invest_evidences, :class_name => 'FixInvestigationEvidenceJoin::InvestEvidence', :dependent => :nullify
    has_many :xevidences, :through => :invest_evidences, :source => :evidence
  end
  
  class InvestCriminal < ActiveRecord::Base
    has_many :invest_evidences, :class_name => 'FixInvestigationEvidenceJoin::InvestEvidence', :dependent => :nullify
    has_many :xevidences, :through => :invest_evidences, :source => :evidence
  end
  
  class Evidence < ActiveRecord::Base
    belongs_to :investigation, :class_name => 'FixInvestigationEvidenceJoin::Investigation'
    belongs_to :crime, :class_name => 'FixInvestigationEvidenceJoin::InvestCrime', :foreign_key => 'invest_crime_id'
    has_many :invest_evidences, :class_name => 'FixInvestigationEvidenceJoin::Evidence', :dependent => :destroy
    has_many :investigations, :through => :invest_evidences
    has_many :xcrimes, :through => :invest_evidences, :source => :invest_crime
    has_many :criminals, :through => :invest_evidences, :source => :invest_criminals
  end
  
  class InvestEvidence < ActiveRecord::Base
    belongs_to :investigation, :class_name => 'FixInvestigationEvidenceJoin::Investigation'
    belongs_to :evidence, :class_name => 'FixInvestigationEvidenceJoin::Evidence'
    belongs_to :invest_crime, :class_name => 'FixInvestigationEvidenceJoin::InvestCrime'
    belongs_to :invest_criminal, :class_name => 'FixInvestigationEvidenceJoin::InvestCriminal'
  end
  
  def up
    create_table :invest_evidences, :force => true do |t|
      t.integer :evidence_id
      t.integer :investigation_id
      t.integer :invest_crime_id
      t.integer :invest_criminal_id
      t.timestamps
    end
    add_index :invest_evidences, :evidence_id
    add_index :invest_evidences, :investigation_id
    add_index :invest_evidences, :invest_crime_id
    add_index :invest_evidences, :invest_criminal_id
    
    FixInvestigationEvidenceJoin::Investigation.reset_column_information
    FixInvestigationEvidenceJoin::Investigation.all.each do |i|
      i.evidences.each do |e|
        i.xevidences.create(:invest_crime_id => e.invest_crime_id)
      end
    end
    
    remove_column :evidences, :investigation_id
    remove_column :evidences, :invest_crime_id
    
    # don't forget to qualify all class names!
  end

  def down
    add_column :evidences, :investigation_id, :integer
    add_column :evidences, :invest_crime_id, :integer
    FixInvestigationEvidenceJoin::Evidence.reset_column_information
    FixInvestigationEvidenceJoin::Evidence.all.each do |e|
      next if e.investigations.empty?
      e.update_attribute(:investigation_id, e.investigations.first.investigation_id)
      e.update_attribute(:invest_crime_id, e.investigations.first.invest_crime_id)
    end
    drop_table :invest_evidences
  end
end
