class JoinArrestsToInvestigations < ActiveRecord::Migration
  def up
    create_table :arrests_investigations, :id => false, :force => true do |t|
      t.integer :arrest_id
      t.integer :investigation_id
    end
    add_index :arrests_investigations, :arrest_id
    add_index :arrests_investigations, :investigation_id
  end

  def down
    drop_table :arrests_investigations
  end
end
