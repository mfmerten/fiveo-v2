class NullsAndDefaultsA < ActiveRecord::Migration
  class Absent < ActiveRecord::Base
  end
  class AbsentPrisoner < ActiveRecord::Base
  end
  class Activity < ActiveRecord::Base
  end
  class Announcement < ActiveRecord::Base
  end
  class AnnouncementIgnore < ActiveRecord::Base
  end
  class Arrest < ActiveRecord::Base
  end
  class ArrestOfficer < ActiveRecord::Base
  end
  
  def up
    NullsAndDefaultsA::Absent.delete_all("jail_id IS NULL")
    change_column :absents, :jail_id, :integer, :null => false
    NullsAndDefaultsA::Absent.update_all("escort_officer = ''", "escort_officer IS NULL")
    change_column :absents, :escort_officer, :string, :null => false, :default => ''
    NullsAndDefaultsA::Absent.update_all("escort_officer_unit = ''", "escort_officer_unit IS NULL")
    change_column :absents, :escort_officer_unit, :string, :null => false, :default => ''
    NullsAndDefaultsA::Absent.update_all("escort_officer_badge = ''", "escort_officer_badge IS NULL")
    change_column :absents, :escort_officer_badge, :string, :null => false, :default => ''
    NullsAndDefaultsA::Absent.update_all("absent_type = ''", "absent_type IS NULL")
    change_column :absents, :absent_type, :string, :null => false, :default => ''
    
    NullsAndDefaultsA::AbsentPrisoner.delete_all('absent_id IS NULL OR booking_id IS NULL')
    change_column :absent_prisoners, :absent_id, :integer, :null => false
    change_column :absent_prisoners, :booking_id, :integer, :null => false
    
    NullsAndDefaultsA::Activity.update_all("section = ''", "section IS NULL")
    change_column :activities, :section, :string, :null => false, :default => ''
    NullsAndDefaultsA::Activity.update_all("description = ''", "description IS NULL")
    change_column :activities, :description, :string, :null => false, :default => ''
    NullsAndDefaultsA::Activity.update_all("username = ''", "username IS NULL")
    change_column :activities, :username, :string, :null => false, :default => ''
    
    NullsAndDefaultsA::Announcement.update_all("subject = ''","subject IS NULL")
    change_column :announcements, :subject, :string, :null => false, :default => ''
    NullsAndDefaultsA::Announcement.update_all("body = ''", "body IS NULL")
    change_column :announcements, :body, :text, :null => false, :default => ''
    NullsAndDefaultsA::Announcement.update_all("unignorable = FALSE","unignorable IS UNKNOWN")
    change_column :announcements, :unignorable, :boolean, :null => false, :default => false
    
    NullsAndDefaultsA::AnnouncementIgnore.delete_all('announcement_id IS NULL OR user_id IS NULL')
    change_column :announcement_ignores, :announcement_id, :integer, :null => false
    change_column :announcement_ignores, :user_id, :integer, :null => false
    
    NullsAndDefaultsA::Arrest.delete_all('person_id IS NULL')
    change_column :arrests, :person_id, :integer, :null => false
    NullsAndDefaultsA::Arrest.update_all("case_no = ''", "case_no IS NULL")
    change_column :arrests, :case_no, :string, :null => false, :default => ''
    NullsAndDefaultsA::Arrest.update_all("agency = ''", "agency IS NULL")
    change_column :arrests, :agency, :string, :null => false, :default => ''
    NullsAndDefaultsA::Arrest.update_all("victim_notify = FALSE", "victim_notify IS UNKNOWN")
    change_column :arrests, :victim_notify, :boolean, :null => false, :default => false
    NullsAndDefaultsA::Arrest.update_all("dwi_test_officer = ''", "dwi_test_officer IS NULL")
    change_column :arrests, :dwi_test_officer, :string, :null => false, :default => ''
    NullsAndDefaultsA::Arrest.update_all("dwi_test_officer_unit = ''", "dwi_test_officer_unit IS NULL")
    change_column :arrests, :dwi_test_officer_unit, :string, :null => false, :default => ''
    NullsAndDefaultsA::Arrest.update_all("dwi_test_officer_badge = ''", "dwi_test_officer_badge IS NULL")
    change_column :arrests, :dwi_test_officer_badge, :string, :null => false, :default => ''
    NullsAndDefaultsA::Arrest.update_all("dwi_test_results = ''", "dwi_test_results IS NULL")
    change_column :arrests, :dwi_test_results, :string, :null => false, :default => ''
    NullsAndDefaultsA::Arrest.update_all("attorney = ''", "attorney IS NULL")
    change_column :arrests, :attorney, :string, :null => false, :default => ''
    NullsAndDefaultsA::Arrest.update_all("condition_of_bond = ''", "condition_of_bond IS NULL")
    change_column :arrests, :condition_of_bond, :string, :null => false, :default => ''
    NullsAndDefaultsA::Arrest.update_all("remarks = ''", "remarks IS NULL")
    change_column :arrests, :remarks, :text, :null => false, :default => ''
    change_column_default :arrests, :created_by, 1
    change_column_default :arrests, :updated_by, 1
    NullsAndDefaultsA::Arrest.update_all("bondable = FALSE", "bondable IS UNKNOWN")
    change_column :arrests, :bondable, :boolean, :null => false, :default => false
    NullsAndDefaultsA::Arrest.update_all("legacy = FALSE", "legacy IS UNKNOWN")
    change_column :arrests, :legacy, :boolean, :null => false, :default => false
    NullsAndDefaultsA::Arrest.update_all("atn = ''", "atn IS NULL")
    change_column :arrests, :atn, :string, :null => false, :default => ''
    NullsAndDefaultsA::Arrest.update_all("hour_72_judge = ''", "hour_72_judge IS NULL")
    change_column :arrests, :hour_72_judge, :string, :null => false, :default => ''
    
    NullsAndDefaultsA::ArrestOfficer.delete_all("arrest_id IS NULL OR officer IS NULL OR officer = ''")
    change_column :arrest_officers, :arrest_id, :integer, :null => false
    change_column :arrest_officers, :officer, :string, :null => false, :default => ''
    NullsAndDefaultsA::ArrestOfficer.update_all("officer_unit = ''", "officer_unit IS NULL")
    change_column :arrest_officers, :officer_unit, :string, :null => false, :default => ''
    NullsAndDefaultsA::ArrestOfficer.update_all("officer_badge = ''", "officer_badge IS NULL")
    change_column :arrest_officers, :officer_badge, :string, :null => false, :default => ''
  end

  def down
    # should be able to do this over and over without problems
  end
end
