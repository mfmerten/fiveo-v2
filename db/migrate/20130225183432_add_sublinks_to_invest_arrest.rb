class AddSublinksToInvestArrest < ActiveRecord::Migration
  def change
    add_column :invest_arrests, :invest_crime_id, :integer
    add_column :invest_arrests, :invest_criminal_id, :integer
    add_column :invest_arrests, :invest_report_id, :integer
    add_index :invest_arrests, :invest_crime_id
    add_index :invest_arrests, :invest_criminal_id
    add_index :invest_arrests, :invest_report_id
  end
end
