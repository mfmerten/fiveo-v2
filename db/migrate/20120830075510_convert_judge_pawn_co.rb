class ConvertJudgePawnCo < ActiveRecord::Migration
  class Option < ActiveRecord::Base
    OptionTypes = {
      30 => 'Judge',
      32 => 'Pawn Company',
      33 => 'Pawn Ticket Type',
      38 => 'Probation Status',
      39 => 'Probation Type',
      41 => 'Release Reason',
      46 => 'Subject Type',
      48 => 'Transfer Type',
      49 => 'Trial Result',
      50 => 'Vehicle Type',
      51 => 'Warrant Disposition'
    }.freeze
  end
  class Contact < ActiveRecord::Base
  end
  class Pawn < ActiveRecord::Base
    belongs_to :old_pawn_co, :class_name => "ConvertJudgePawnCo::Option", :foreign_key => 'pawn_co_id'
  end
  class Arrest < ActiveRecord::Base
    belongs_to :old_hour_72_judge, :class_name => "ConvertJudgePawnCo::Option", :foreign_key => 'hour_72_judge_id'
  end
  class Court < ActiveRecord::Base
    belongs_to :old_judge, :class_name => 'ConvertJudgePawnCo::Option', :foreign_key => 'judge_id'
    belongs_to :old_next_judge, :class_name => 'ConvertJudgePawnCo::Option', :foreign_key => 'next_judge_id'
  end
  class Revocation < ActiveRecord::Base
    belongs_to :old_judge, :class_name => 'ConvertJudgePawnCo::Option', :foreign_key => 'judge_id'
  end
  
  def up
    add_column :pawns, :pawn_co, :string
    add_column :arrests, :hour_72_judge, :string
    add_column :courts, :judge, :string
    add_column :courts, :next_judge, :string
    add_column :revocations, :judge, :string
    add_column :contacts, :judge, :boolean, :default => false
    add_column :contacts, :pawn_co, :boolean, :default => false
    
    ConvertJudgePawnCo::Contact.reset_column_information
    ConvertJudgePawnCo::Pawn.reset_column_information
    ConvertJudgePawnCo::Arrest.reset_column_information
    ConvertJudgePawnCo::Court.reset_column_information
    ConvertJudgePawnCo::Revocation.reset_column_information
    
    ConvertJudgePawnCo::Option.where(:option_type => 30).all.each do |o|
      unless o.disabled?
        ConvertJudgePawnCo::Contact.create(:fullname => o.long_name, :judge => true, :title => 'Judge', :business => false, :active => true, :remarks => 'Converted from Judge options.', :created_by => 1, :updated_by => 1)
      end
    end
    ConvertJudgePawnCo::Option.where(:option_type => 32).all.each do |o|
      unless o.disabled?
        ConvertJudgePawnCo::Contact.create(:fullname => o.long_name, :pawn_co => true, :business => true, :active => true, :remarks => 'Converted from Pawn Company options.', :created_by => 1, :updated_by => 1)
      end
    end
    
    ConvertJudgePawnCo::Pawn.all.each do |p|
      unless p.old_pawn_co.nil?
        p.update_attribute(:pawn_co, p.old_pawn_co.long_name)
      end
    end
    ConvertJudgePawnCo::Arrest.all.each do |a|
      unless a.old_hour_72_judge.nil?
        a.update_attribute(:hour_72_judge, a.old_hour_72_judge.long_name)
      end
    end
    ConvertJudgePawnCo::Court.all.each do |c|
      unless c.old_judge.nil?
        c.judge = c.old_judge.long_name
      end
      unless c.old_next_judge.nil?
        c.next_judge = c.old_next_judge.long_name
      end
      if c.changed?
        c.save
      end
    end
    ConvertJudgePawnCo::Revocation.all.each do |r|
      unless r.old_judge.nil?
        r.update_attribute(:judge, r.old_judge.long_name)
      end
    end
    
    remove_column :pawns, :pawn_co_id
    remove_column :arrests, :hour_72_judge_id
    remove_column :courts, :judge_id
    remove_column :courts, :next_judge_id
    remove_column :revocations, :judge_id

    ConvertJudgePawnCo::Option.where(:option_type => [30,32]).each{|o| o.destroy}
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
