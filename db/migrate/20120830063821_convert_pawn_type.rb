class ConvertPawnType < ActiveRecord::Migration
  class Option < ActiveRecord::Base
    OptionTypes = {
      30 => 'Judge',
      32 => 'Pawn Company',
      33 => 'Pawn Ticket Type',
      34 => 'Pawn Type',
      38 => 'Probation Status',
      39 => 'Probation Type',
      41 => 'Release Reason',
      46 => 'Subject Type',
      48 => 'Transfer Type',
      49 => 'Trial Result',
      50 => 'Vehicle Type',
      51 => 'Warrant Disposition'
    }.freeze
  end
  
  class PawnItemType < ActiveRecord::Base
  end
  
  class PawnItem < ActiveRecord::Base
    belongs_to :old_pawn_type, :class_name => "ConvertPawnType::Option", :foreign_key => 'pawn_type_id'
  end
  
  def up
    create_table :pawn_item_types, :force => true do |t|
      t.string :item_type
      t.integer :created_by, :default => 1
      t.integer :updated_by, :default => 1
      t.timestamps
    end
    add_index :pawn_item_types, :created_by
    add_index :pawn_item_types, :updated_by
    
    add_column :pawn_items, :item_type, :string
    
    ConvertPawnType::PawnItemType.reset_column_information
    ConvertPawnType::PawnItem.reset_column_information
    
    ConvertPawnType::Option.where(:option_type => 34).all.each do |o|
      unless o.disabled?
        ConvertPawnType::PawnItemType.create(:item_type => o.long_name)
      end
    end
    
    ConvertPawnType::PawnItem.all.each do |p|
      unless p.old_pawn_type.nil?
        p.update_attribute(:item_type, p.old_pawn_type.long_name)
      end
    end
    
    remove_column :pawn_items, :pawn_type_id
    
    ConvertPawnType::Option.where(:option_type => 34).all.each{|o| o.destroy}
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
