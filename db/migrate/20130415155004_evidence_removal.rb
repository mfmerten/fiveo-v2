class EvidenceRemoval < ActiveRecord::Migration
  def up
    add_column :evidences, :disposed, :date
    add_column :evidences, :disposition, :string, :nil => false, :default => ''
    add_index :evidences, :disposed
  end

  def down
    remove_column :evidences, :disposed
    remove_column :evidences, :disposition
  end
end
