class Remove3JailTables < ActiveRecord::Migration
  # part of Rails3 upgrade
  # removing partially implemented 3-jail system (bug 29)
  
  def up
    drop_table :jail1s
    drop_table :jail2s
    drop_table :jail3s
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
