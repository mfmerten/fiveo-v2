# convert arrest officers from 3 distinct fields to a subtable
class ArrestOfficersConversion < ActiveRecord::Migration
  class Arrest < ActiveRecord::Base
    has_many :officers, :class_name => 'ArrestOfficersConversion::ArrestOfficer'
  end
  class ArrestOfficer < ActiveRecord::Base
    belongs_to :arrest, :class_name => 'ArrestOfficersConversion::Arrest'
  end
  
  def up
    create_table :arrest_officers, :force => true do |t|
      t.integer :arrest_id
      t.string :officer
      t.string :officer_unit
      t.string :officer_badge
      t.integer :created_by
      t.integer :updated_by
      t.timestamps
    end
    add_index :arrest_officers, :arrest_id
    add_index :arrest_officers, :created_by
    add_index :arrest_officers, :updated_by
    
    ArrestOfficersConversion::ArrestOfficer.reset_column_information
    ArrestOfficersConversion::Arrest.all.each do |a|
      if a.officer1?
        ArrestOfficersConversion::ArrestOfficer.create(:arrest_id => a.id, :officer => a.officer1, :officer_unit => a.officer1_unit, :officer_badge => a.officer1_badge, :created_by => 1, :updated_by => 1)
      end
      if a.officer2?
        ArrestOfficersConversion::ArrestOfficer.create(:arrest_id => a.id, :officer => a.officer2, :officer_unit => a.officer2_unit, :officer_badge => a.officer2_badge, :created_by => 1, :updated_by => 1)
      end
      if a.officer3?
        ArrestOfficersConversion::ArrestOfficer.create(:arrest_id => a.id, :officer => a.officer3, :officer_unit => a.officer3_unit, :officer_badge => a.officer3_badge, :created_by => 1, :updated_by => 1)
      end
    end
    
    remove_column :arrests, :officer1
    remove_column :arrests, :officer1_unit
    remove_column :arrests, :officer1_badge
    remove_column :arrests, :officer2
    remove_column :arrests, :officer2_unit
    remove_column :arrests, :officer2_badge
    remove_column :arrests, :officer3
    remove_column :arrests, :officer3_unit
    remove_column :arrests, :officer3_badge
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
