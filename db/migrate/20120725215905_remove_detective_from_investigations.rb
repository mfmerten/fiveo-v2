class RemoveDetectiveFromInvestigations < ActiveRecord::Migration
  def up
    remove_column :investigations, :detective_id
    remove_column :investigations, :detective
    remove_column :investigations, :detective_unit
    remove_column :investigations, :detective_badge
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
