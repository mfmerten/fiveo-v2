class FixBooleanDefaults < ActiveRecord::Migration
  class Arrest < ActiveRecord::Base
  end
  class Bond < ActiveRecord::Base
  end
  class Booking < ActiveRecord::Base
  end
  class Contact < ActiveRecord::Base
  end
  class DaCharge < ActiveRecord::Base
  end
  class ExternalLink < ActiveRecord::Base
  end
  class InvestCrime < ActiveRecord::Base
  end
  class InvestReport < ActiveRecord::Base
  end
  class Medical < ActiveRecord::Base
  end
  class MessageLog < ActiveRecord::Base
  end
  class Message < ActiveRecord::Base
  end
  class Offense < ActiveRecord::Base
  end
  class Person < ActiveRecord::Base
  end
  class Revocation < ActiveRecord::Base
  end
  class SupportTicket < ActiveRecord::Base
  end
  class Warrant < ActiveRecord::Base
  end
  
  def up
    change_column_default :arrests, :victim_notify, false
    FixBooleanDefaults::Arrest.reset_column_information
    FixBooleanDefaults::Arrest.update_all('victim_notify = 0', 'victim_notify IS NULL')
    
    change_column_default :bonds, :final, false
    FixBooleanDefaults::Bond.reset_column_information
    FixBooleanDefaults::Bond.update_all('final = 0', 'final IS NULL')
    
    change_column_default :bookings, :intoxicated, false
    change_column_default :bookings, :afis, false
    FixBooleanDefaults::Booking.reset_column_information
    FixBooleanDefaults::Booking.update_all('intoxicated = 0','intoxicated IS NULL')
    FixBooleanDefaults::Booking.update_all('afis = 0','afis IS NULL')
    
    change_column_default :contacts, :officer, false
    change_column_default :contacts, :phone1_unlisted, false
    change_column_default :contacts, :phone2_unlisted, false
    change_column_default :contacts, :phone3_unlisted, false
    change_column_default :contacts, :bondsman, false
    FixBooleanDefaults::Contact.reset_column_information
    FixBooleanDefaults::Contact.update_all('officer = 0','officer IS NULL')
    FixBooleanDefaults::Contact.update_all('phone1_unlisted = 0','phone1_unlisted IS NULL')
    FixBooleanDefaults::Contact.update_all('phone2_unlisted = 0','phone2_unlisted IS NULL')
    FixBooleanDefaults::Contact.update_all('phone3_unlisted = 0','phone3_unlisted IS NULL')
    FixBooleanDefaults::Contact.update_all('bondsman = 0','bondsman IS NULL')
    
    remove_column :court_types, :disabled
    
    change_column_default :da_charges, :da_diversion, false
    FixBooleanDefaults::DaCharge.reset_column_information
    FixBooleanDefaults::DaCharge.update_all('da_diversion = 0', 'da_diversion IS NULL')
    
    remove_column :docket_types, :disabled
    
    change_column_default :external_links, :front_page, false
    FixBooleanDefaults::ExternalLink.reset_column_information
    FixBooleanDefaults::ExternalLink.update_all('front_page = 0','front_page IS NULL')
    
    change_column_default :invest_crimes, :occupied, false
    FixBooleanDefaults::InvestCrime.reset_column_information
    FixBooleanDefaults::InvestCrime.update_all('occupied = 0','occupied IS NULL')
    
    change_column_default :invest_reports, :supplemental, false
    FixBooleanDefaults::InvestReport.reset_column_information
    FixBooleanDefaults::InvestReport.update_all('supplemental = 0','supplemental IS NULL')
    
    change_column_default :medicals, :heart_disease, false
    change_column_default :medicals, :blood_pressure, false
    change_column_default :medicals, :diabetes, false
    change_column_default :medicals, :epilepsy, false
    change_column_default :medicals, :hepatitis, false
    change_column_default :medicals, :hiv, false
    change_column_default :medicals, :tb, false
    change_column_default :medicals, :ulcers, false
    change_column_default :medicals, :venereal, false
    FixBooleanDefaults::Medical.reset_column_information
    FixBooleanDefaults::Medical.update_all('heart_disease = 0','heart_disease IS NULL')
    FixBooleanDefaults::Medical.update_all('blood_pressure = 0','blood_pressure IS NULL')
    FixBooleanDefaults::Medical.update_all('diabetes = 0','diabetes IS NULL')
    FixBooleanDefaults::Medical.update_all('epilepsy = 0','epilepsy IS NULL')
    FixBooleanDefaults::Medical.update_all('hepatitis = 0','hepatitis IS NULL')
    FixBooleanDefaults::Medical.update_all('hiv = 0','hiv IS NULL')
    FixBooleanDefaults::Medical.update_all('tb = 0','tb IS NULL')
    FixBooleanDefaults::Medical.update_all('ulcers = 0','ulcers IS NULL')
    FixBooleanDefaults::Medical.update_all('venereal = 0','venereal IS NULL')
    
    change_column_default :message_logs, :receiver_deleted, false
    change_column_default :message_logs, :receiver_purged, false
    change_column_default :message_logs, :sender_deleted, false
    change_column_default :message_logs, :sender_purged, false
    FixBooleanDefaults::MessageLog.reset_column_information
    FixBooleanDefaults::MessageLog.update_all('receiver_deleted = 0','receiver_deleted IS NULL')
    FixBooleanDefaults::MessageLog.update_all('receiver_purged = 0','receiver_purged IS NULL')
    FixBooleanDefaults::MessageLog.update_all('sender_deleted = 0','sender_deleted IS NULL')
    FixBooleanDefaults::MessageLog.update_all('sender_purged = 0','sender_purged IS NULL')
    
    change_column_default :messages, :receiver_deleted, false
    change_column_default :messages, :receiver_purged, false
    change_column_default :messages, :sender_deleted, false
    change_column_default :messages, :sender_purged, false
    FixBooleanDefaults::Message.reset_column_information
    FixBooleanDefaults::Message.update_all('receiver_deleted = 0','receiver_deleted IS NULL')
    FixBooleanDefaults::Message.update_all('receiver_purged = 0','receiver_purged IS NULL')
    FixBooleanDefaults::Message.update_all('sender_deleted = 0','sender_deleted IS NULL')
    FixBooleanDefaults::Message.update_all('sender_purged = 0','sender_purged IS NULL')
    
    change_column_default :offenses, :pictures, false
    change_column_default :offenses, :restitution_form, false
    change_column_default :offenses, :victim_notification, false
    change_column_default :offenses, :perm_to_search, false
    change_column_default :offenses, :misd_summons, false
    change_column_default :offenses, :fortyeight_hour, false
    change_column_default :offenses, :medical_release, false
    FixBooleanDefaults::Offense.reset_column_information
    FixBooleanDefaults::Offense.update_all('pictures = 0','pictures IS NULL')
    FixBooleanDefaults::Offense.update_all('restitution_form = 0','restitution_form IS NULL')
    FixBooleanDefaults::Offense.update_all('victim_notification = 0','victim_notification IS NULL')
    FixBooleanDefaults::Offense.update_all('perm_to_search = 0','perm_to_search IS NULL')
    FixBooleanDefaults::Offense.update_all('misd_summons = 0','misd_summons IS NULL')
    FixBooleanDefaults::Offense.update_all('fortyeight_hour = 0','fortyeight_hour IS NULL')
    FixBooleanDefaults::Offense.update_all('medical_release = 0','medical_release IS NULL')
    
    change_column_default :people, :glasses, false
    change_column_default :people, :hepatitis, false
    change_column_default :people, :hiv, false
    change_column_default :people, :tb, false
    change_column_default :people, :deceased, false
    FixBooleanDefaults::Person.reset_column_information
    FixBooleanDefaults::Person.update_all('glasses = 0','glasses IS NULL')
    FixBooleanDefaults::Person.update_all('hepatitis = 0','hepatitis IS NULL')
    FixBooleanDefaults::Person.update_all('hiv = 0','hiv IS NULL')
    FixBooleanDefaults::Person.update_all('tb = 0','tb IS NULL')
    FixBooleanDefaults::Person.update_all('deceased = 0','deceased IS NULL')
    
    change_column_default :revocations, :revoked, false
    change_column_default :revocations, :time_served, false
    change_column_default :revocations, :doc, false
    change_column_default :revocations, :processed, false
    FixBooleanDefaults::Revocation.reset_column_information
    FixBooleanDefaults::Revocation.update_all('revoked = 0','revoked IS NULL')
    FixBooleanDefaults::Revocation.update_all('time_served = 0','time_served IS NULL')
    FixBooleanDefaults::Revocation.update_all('doc = 0','doc IS NULL')
    FixBooleanDefaults::Revocation.update_all('processed = 0','processed IS NULL')
    
    change_column_default :support_tickets, :bug_report_filed, false
    FixBooleanDefaults::SupportTicket.reset_column_information
    FixBooleanDefaults::SupportTicket.update_all('bug_report_filed = 0','bug_report_filed IS NULL')
    
    change_column_default :warrants, :phone1_unlisted, false
    change_column_default :warrants, :phone2_unlisted, false
    change_column_default :warrants, :phone3_unlisted, false
    FixBooleanDefaults::Warrant.reset_column_information
    FixBooleanDefaults::Warrant.update_all('phone1_unlisted = 0','phone1_unlisted IS NULL')
    FixBooleanDefaults::Warrant.update_all('phone2_unlisted = 0','phone2_unlisted IS NULL')
    FixBooleanDefaults::Warrant.update_all('phone3_unlisted = 0','phone3_unlisted IS NULL')
  end

  def down
    add_column :court_types, :disabled, :boolean, :default => false
    add_column :docket_types, :disabled, :boolean, :default => false
  end
end
