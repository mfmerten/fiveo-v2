class FixOwnedBy < ActiveRecord::Migration
  class Event < ActiveRecord::Base
  end
  class Evidence < ActiveRecord::Base
  end
  class InvestCase < ActiveRecord::Base
  end
  class InvestContact < ActiveRecord::Base
  end
  class InvestCrime < ActiveRecord::Base
  end
  class InvestCriminal < ActiveRecord::Base
  end
  class InvestPhoto < ActiveRecord::Base
  end
  class InvestReport < ActiveRecord::Base
  end
  class InvestVehicle < ActiveRecord::Base
  end
  class Investigation < ActiveRecord::Base
  end
  class Stolen < ActiveRecord::Base
  end
  class Warrant < ActiveRecord::Base
  end
  def up
    say_with_time "Updating Events" do
      cnt = FixOwnedBy::Event.update_all('owned_by = created_by','owned_by IS NULL')
      say "#{cnt} Records Updated"
    end
    say_with_time "Updating Evidence" do
      cnt = FixOwnedBy::Evidence.update_all('owned_by = created_by','owned_by IS NULL')
      say "#{cnt} Records Updated"
    end
    say_with_time "Updating Invest. Cases" do
      cnt = FixOwnedBy::InvestCase.update_all('owned_by = created_by','owned_by IS NULL')
      say "#{cnt} Records Updated"
    end
    say_with_time "Updating Invest. Contacts" do
      cnt = FixOwnedBy::InvestContact.update_all('owned_by = created_by','owned_by IS NULL')
      say "#{cnt} Records Updated"
    end
    say_with_time "Updating Invest. Crimes" do
      cnt = FixOwnedBy::InvestCrime.update_all('owned_by = created_by','owned_by IS NULL')
      say "#{cnt} Records Updated"
    end
    say_with_time "Updating Invest. Criminals" do
      cnt = FixOwnedBy::InvestCriminal.update_all('owned_by = created_by','owned_by IS NULL')
      say "#{cnt} Records Updated"
    end
    say_with_time "Updating Invest. Photos" do
      cnt = FixOwnedBy::InvestPhoto.update_all('owned_by = created_by','owned_by IS NULL')
      say "#{cnt} Records Updated"
    end
    say_with_time "Updating Invest. Reports" do
      cnt = FixOwnedBy::InvestReport.update_all('owned_by = created_by','owned_by IS NULL')
      say "#{cnt} Records Updated"
    end
    say_with_time "Updating Invest. Vehicles" do
      cnt = FixOwnedBy::InvestVehicle.update_all('owned_by = created_by','owned_by IS NULL')
      say "#{cnt} Records Updated"
    end
    say_with_time "Updating Investigations" do
      cnt = FixOwnedBy::Investigation.update_all('owned_by = created_by','owned_by IS NULL')
      say "#{cnt} Records Updated"
    end
    say_with_time "Updating Stolens" do
      cnt = FixOwnedBy::Stolen.update_all('owned_by = created_by','owned_by IS NULL')
      say "#{cnt} Records Updated"
    end
    say_with_time "Updating Warrants" do
      cnt = FixOwnedBy::Warrant.update_all('owned_by = created_by','owned_by IS NULL')
      say "#{cnt} Records Updated"
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
