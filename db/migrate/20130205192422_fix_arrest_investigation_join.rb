class FixArrestInvestigationJoin < ActiveRecord::Migration
  class Arrest < ActiveRecord::Base
    has_and_belongs_to_many :investigations, :class_name => 'FixArrestInvestigationJoin::Investigation'
    has_many :invest_arrests, :class_name => 'FixArrestInvestigationJoin::InvestArrest'
    has_many :invests, :through => :invest_arrests, :source => :investigation
  end
  class InvestArrest < ActiveRecord::Base
    belongs_to :arrest, :class_name => 'FixArrestInvestigationJoin::Arrest'
    belongs_to :investigation, :class_name => 'FixArrestInvestigationJoin::Investigation'
  end
  class Investigation < ActiveRecord::Base
    has_and_belongs_to_many :arrests, :class_name => 'FixArrestInvestigationJoin::Arrest'
    has_many :invest_arrests, :class_name => 'FixArrestInvestigationJoin::InvestArrest'
    has_many :iarrests, :through => :invest_arrests, :source => :arrest
  end
  
  def up
    create_table :invest_arrests, :force => true do |t|
      t.integer :arrest_id
      t.integer :investigation_id
      t.timestamps
    end
    add_index :invest_arrests, :arrest_id
    add_index :invest_arrests, :investigation_id
    
    FixArrestInvestigationJoin::Investigation.all.each do |i|
      i.iarrest_ids = i.arrest_ids
    end
    
    drop_table :arrests_investigations
    
    # drop created_by and updated_by for arrest_officers and charges
    remove_column :arrest_officers, :created_by
    remove_column :arrest_officers, :updated_by
    remove_column :charges, :created_by
    remove_column :charges, :updated_by
    remove_column :da_charges, :created_by
    remove_column :da_charges, :updated_by
    
    # add id columns needed for nested association records
    add_column :charges, :agency_id, :integer
    add_column :arrest_officers, :officer_id, :integer
  end

  def down
    create_table :arrests_investigations, :id => false, :force => true do |t|
      t.integer :arrest_id
      t.integer :investigation_id
    end
    add_index :arrests_investigations, :arrest_id
    add_index :arrests_investigations, :investigation_id
    
    FixArrestInvestigationJoin::Investigation.all.each do |i|
      i.arrest_ids = i.iarrest_ids
    end
    
    drop_table :invest_arrests
    
    add_column :arrest_officers, :created_by, :integer, :default => 1
    add_column :arrest_officers, :updated_by, :integer, :default => 1
    add_column :charges, :created_by, :integer, :default => 1
    add_column :charges, :updated_by, :integer, :default => 1
    add_column :da_charges, :created_by, :integer, :default => 1
    add_column :da_charges, :updated_by, :integer, :default => 1
    
    remove_column :charges, :agency_id
    remove_column :arrest_officers, :officer_id
  end
end
