class PartialPaperPayments < ActiveRecord::Migration
  class Paper < ActiveRecord::Base
    has_many :payments, :class_name => 'PartialPaperPayments::PaperPayment'
  end
  class PaperPayment < ActiveRecord::Base
    belongs_to :paper, :class_name => 'PartialPaperPayments::Paper'
  end
  
  def up
    add_column :papers, :paid_in_full, :boolean, :default => false
    add_column :paper_payments, :paid_in_full, :boolean, :default => false
    add_column :paper_payments, :adjustment, :boolean, :default => false
    remove_column :paper_customers, :balance_due
    
    PartialPaperPayments::Paper.reset_column_information
    PartialPaperPayments::PaperPayment.reset_column_information
    
    PartialPaperPayments::PaperPayment.all.each do |p|
      if p.paper.nil?
        # special adjustment transaction (does not affect invoice)
        p.update_attribute(:adjustment, true)
      else
        # all payments to invoices until this migration is finished
        # are 'payment in full' for the invoice if there is a payment amount.
        if p.payment? && p.payment > 0
          p.update_attribute(:paid_in_full, true)
          p.paper.update_attribute(:paid_in_full, true)
        end
        # charges do not affect invoice except for the initial (invoicing)
        # payment so we ignore them here.
      end
    end
  end
  
  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
