class ConstrainTables < ActiveRecord::Migration
  def up
    # absent
    add_foreign_key :absents, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :absents, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :absents, :jails

    # absent prisoner
    add_foreign_key :absent_prisoners, :absents
    add_foreign_key :absent_prisoners, :bookings
    
    # activity
    add_foreign_key :activities, :users
  
    # announcement
    add_foreign_key :announcements, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :announcements, :users, :column => 'updated_by', :dependent => :nullify
    
    # announcement ignore
    add_foreign_key :announcement_ignores, :announcements
    add_foreign_key :announcement_ignores, :users
  
    # arrest
    add_foreign_key :arrests, :people
    add_foreign_key :arrests, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :arrests, :users, :column => 'updated_by', :dependent => :nullify
    
    # arrest officer
    add_foreign_key :arrest_officers, :contacts, :column => 'officer_id', :dependent => :nullify
    add_foreign_key :arrest_officers, :arrests
    
    # bonds
    add_foreign_key :bonds, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :bonds, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :bonds, :arrests
    add_foreign_key :bonds, :people
    add_foreign_key :bonds, :people, :column => 'surety_id'
    
    # booking
    add_foreign_key :bookings, :jails
    add_foreign_key :bookings, :people
    add_foreign_key :bookings, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :bookings, :users, :column => 'updated_by', :dependent => :nullify
    
    # booking log
    add_foreign_key :booking_logs, :bookings
    
    # booking property
    add_foreign_key :booking_properties, :bookings
  
    # call
    add_foreign_key :calls, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :calls, :users, :column => 'updated_by', :dependent => :nullify
    
    # call subject
    add_foreign_key :call_subjects, :calls
    
    # call unit
    add_foreign_key :call_units, :calls
    add_foreign_key :call_units, :contacts, :column => 'officer_id', :dependent => :nullify
    
    # case - nothing to do
    
    # case note
    add_foreign_key :case_notes, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :case_notes, :users, :column => 'updated_by', :dependent => :nullify
    
    # charge
    add_foreign_key :charges, :arrests
    add_foreign_key :charges, :citations
    add_foreign_key :charges, :warrants
    add_foreign_key :charges, :contacts, :column => 'agency_id', :dependent => :nullify
    
    # citation
    add_foreign_key :citations, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :citations, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :citations, :people
    
    # commissary
    add_foreign_key :commissaries, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :commissaries, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :commissaries, :people
    add_foreign_key :commissaries, :jails
    
    # commissary item
    add_foreign_key :commissary_items, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :commissary_items, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :commissary_items, :jails
    
    # contact
    add_foreign_key :contacts, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :contacts, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :contacts, :contacts, :column => 'agency_id'
    
    # contact bondsman company
    add_foreign_key :contact_bondsman_companies, :contacts
    add_foreign_key :contact_bondsman_companies, :contacts, :column => 'bondsman_id'
    add_foreign_key :contact_bondsman_companies, :contacts, :column => 'company_id'
    
    # court
    add_foreign_key :courts, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :courts, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :courts, :dockets
    
    # court cost
    add_foreign_key :court_costs, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :court_costs, :users, :column => 'updated_by', :dependent => :nullify
    
    # court type
    add_foreign_key :court_types, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :court_types, :users, :column => 'updated_by', :dependent => :nullify
    
    # da charge
    add_foreign_key :da_charges, :dockets
    add_foreign_key :da_charges, :charges
    add_foreign_key :da_charges, :arrests
    
    # disposition
    add_foreign_key :dispositions, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :dispositions, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :dispositions, :people
    
    # doc
    add_foreign_key :docs, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :docs, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :docs, :transfers
    add_foreign_key :docs, :holds
    add_foreign_key :docs, :sentences
    add_foreign_key :docs, :bookings
    add_foreign_key :docs, :people
    add_foreign_key :docs, :revocations
    
    # docket
    add_foreign_key :dockets, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :dockets, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :dockets, :people
    
    # docket type
    add_foreign_key :docket_types, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :docket_types, :users, :column => 'updated_by', :dependent => :nullify
    
    # event
    add_foreign_key :events, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :events, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :events, :users, :column => 'owned_by', :dependent => :nullify
    add_foreign_key :events, :investigations
    
    # evidence
    add_foreign_key :evidences, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :evidences, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :evidences, :people, :column => 'owner_id'
    
    # evidence locations
    add_foreign_key :evidence_locations, :evidences
    
    # external links
    add_foreign_key :external_links, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :external_links, :users, :column => 'updated_by', :dependent => :nullify
    
    # fax cover
    add_foreign_key :fax_covers, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :fax_covers, :users, :column => 'updated_by', :dependent => :nullify
    
    # forbid
    add_foreign_key :forbids, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :forbids, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :forbids, :people, :column => 'forbidden_id'
    add_foreign_key :forbids, :calls
    
    # forbid exemption
    add_foreign_key :forbid_exemptions, :people
    add_foreign_key :forbid_exemptions, :forbids
    
    # forum
    add_foreign_key :forums, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :forums, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :forums, :forum_categories
    
    # forum category - nothing to do
    
    # forum post
    add_foreign_key :forum_posts, :forum_topics
    add_foreign_key :forum_posts, :users
    add_foreign_key :forum_posts, :forum_posts, :column => 'reply_to_id'
    
    # forum subscription
    add_foreign_key :forum_subscriptions, :forum_topics
    add_foreign_key :forum_subscriptions, :users, :column => 'subscriber_id'
    
    # forum topic
    add_foreign_key :forum_topics, :forums
    add_foreign_key :forum_topics, :users
    
    # forum view
    add_foreign_key :forum_views, :forums
    add_foreign_key :forum_views, :forum_topics
    add_foreign_key :forum_views, :users
    
    # garnishment
    add_foreign_key :garnishments, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :garnishments, :users, :column => 'updated_by', :dependent => :nullify
    
    # group
    add_foreign_key :groups, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :groups, :users, :column => 'updated_by', :dependent => :nullify
    
    # history - nothing to do
    
    # hold
    add_foreign_key :holds, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :holds, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :holds, :bookings
    add_foreign_key :holds, :arrests
    add_foreign_key :holds, :probations
    add_foreign_key :holds, :sentences
    add_foreign_key :holds, :transfers
    add_foreign_key :holds, :revocations
    
    # hold adjustment - nothing to do
    
    # hold blocker
    add_foreign_key :hold_blockers, :holds
    add_foreign_key :hold_blockers, :holds, :column => 'blocker_id'
    
    # hold bond
    add_foreign_key :hold_bonds, :holds
    add_foreign_key :hold_bonds, :bonds
    
    # invest arrest
    add_foreign_key :invest_arrests, :investigations
    add_foreign_key :invest_arrests, :arrests
    
    # invest case
    add_foreign_key :invest_cases, :investigations
    add_foreign_key :invest_cases, :calls
    
    # invest contact
    add_foreign_key :invest_contacts, :investigations
    add_foreign_key :invest_contacts, :contacts
  
    # invest crime
    add_foreign_key :invest_crimes, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :invest_crimes, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :invest_crimes, :users, :column => 'owned_by', :dependent => :nullify
    add_foreign_key :invest_crimes, :investigations
    
    # invest crime arrest
    add_foreign_key :invest_crime_arrests, :invest_crimes
    add_foreign_key :invest_crime_arrests, :invest_arrests
    
    # invest crime case
    add_foreign_key :invest_crime_cases, :invest_crimes
    add_foreign_key :invest_crime_cases, :invest_cases
    
    # invest crime contact
    add_foreign_key :invest_crime_contacts, :invest_crimes
    add_foreign_key :invest_crime_contacts, :invest_contacts
    
    # invest crime criminal
    add_foreign_key :invest_crime_criminals, :invest_crimes
    add_foreign_key :invest_crime_criminals, :invest_criminals
    
    # invest crime evidence
    add_foreign_key :invest_crime_evidences, :invest_crimes
    add_foreign_key :invest_crime_evidences, :invest_evidences
    
    # invest crime offense
    add_foreign_key :invest_crime_offenses, :invest_crimes
    add_foreign_key :invest_crime_offenses, :invest_offenses
    
    # invest crime photo
    add_foreign_key :invest_crime_photos, :invest_crimes
    add_foreign_key :invest_crime_photos, :invest_photos
    
    # invest crime stolen
    add_foreign_key :invest_crime_stolens, :invest_crimes
    add_foreign_key :invest_crime_stolens, :invest_stolens
    
    # invest crime vehicle
    add_foreign_key :invest_crime_vehicles, :invest_crimes
    add_foreign_key :invest_crime_vehicles, :invest_vehicles
    
    # invest crime victim
    add_foreign_key :invest_crime_victims, :invest_crimes
    add_foreign_key :invest_crime_victims, :invest_victims
    
    # invest crime warrant
    add_foreign_key :invest_crime_warrants, :invest_crimes
    add_foreign_key :invest_crime_warrants, :invest_warrants
    
    # invest crime witness
    add_foreign_key :invest_crime_witnesses, :invest_crimes
    add_foreign_key :invest_crime_witnesses, :invest_witnesses
    
    # invest note
    add_foreign_key :invest_notes, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :invest_notes, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :invest_notes, :investigations
    add_foreign_key :invest_notes, :invest_crimes
    add_foreign_key :invest_notes, :invest_criminals
    add_foreign_key :invest_notes, :invest_photos
    add_foreign_key :invest_notes, :invest_reports
    add_foreign_key :invest_notes, :invest_vehicles
    
    # invest criminal
    add_foreign_key :invest_criminals, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :invest_criminals, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :invest_criminals, :users, :column => 'owned_by', :dependent => :nullify
    add_foreign_key :invest_criminals, :investigations
    add_foreign_key :invest_criminals, :people
    
    # invest criminal arrest
    add_foreign_key :invest_criminal_arrests, :invest_criminals
    add_foreign_key :invest_criminal_arrests, :invest_arrests
    
    # invest criminal evidence
    add_foreign_key :invest_criminal_evidences, :invest_criminals
    add_foreign_key :invest_criminal_evidences, :invest_evidences
    
    # invest criminal photos
    add_foreign_key :invest_criminal_photos, :invest_criminals
    add_foreign_key :invest_criminal_photos, :invest_photos
    
    # invest criminal stolens
    add_foreign_key :invest_criminal_stolens, :invest_criminals
    add_foreign_key :invest_criminal_stolens, :invest_stolens
    
    # invest criminal vehicles
    add_foreign_key :invest_criminal_vehicles, :invest_criminals
    add_foreign_key :invest_criminal_vehicles, :invest_vehicles
    
    # invest criminal warrants
    add_foreign_key :invest_criminal_warrants, :invest_criminals
    add_foreign_key :invest_criminal_warrants, :invest_warrants
    
    # invest evidence
    add_foreign_key :invest_evidences, :investigations
    add_foreign_key :invest_evidences, :evidences
    
    # invest offense
    add_foreign_key :invest_offenses, :investigations
    add_foreign_key :invest_offenses, :offenses
    
    # invest photo
    add_foreign_key :invest_photos, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :invest_photos, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :invest_photos, :users, :column => 'owned_by', :dependent => :nullify
    add_foreign_key :invest_photos, :investigations
    
    # invest report
    add_foreign_key :invest_reports, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :invest_reports, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :invest_reports, :users, :column => 'owned_by', :dependent => :nullify
    add_foreign_key :invest_reports, :investigations
    
    # invest report arrest
    add_foreign_key :invest_report_arrests, :invest_reports
    add_foreign_key :invest_report_arrests, :invest_arrests
    
    # invest report criminal
    add_foreign_key :invest_report_criminals, :invest_reports
    add_foreign_key :invest_report_criminals, :invest_criminals
    
    # invest report offense
    add_foreign_key :invest_report_offenses, :invest_reports
    add_foreign_key :invest_report_offenses, :invest_offenses
    
    # invest report photo
    add_foreign_key :invest_report_photos, :invest_reports
    add_foreign_key :invest_report_photos, :invest_photos
    
    # invest report vehicle
    add_foreign_key :invest_report_vehicles, :invest_reports
    add_foreign_key :invest_report_vehicles, :invest_vehicles
    
    # invest report victim
    add_foreign_key :invest_report_victims, :invest_reports
    add_foreign_key :invest_report_victims, :invest_victims
    
    # invest report witness
    add_foreign_key :invest_report_witnesses, :invest_reports
    add_foreign_key :invest_report_witnesses, :invest_witnesses
    
    # invest source
    add_foreign_key :invest_sources, :investigations
    add_foreign_key :invest_sources, :people
    
    # invest stolen
    add_foreign_key :invest_stolens, :investigations
    add_foreign_key :invest_stolens, :stolens
    
    # invest vehicle
    add_foreign_key :invest_vehicles, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :invest_vehicles, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :invest_vehicles, :users, :column => 'owned_by', :dependent => :nullify
    add_foreign_key :invest_vehicles, :investigations
    add_foreign_key :invest_vehicles, :people, :column => 'owner_id'
    
    # invest vehicle photo
    add_foreign_key :invest_vehicle_photos, :invest_vehicles
    add_foreign_key :invest_vehicle_photos, :invest_photos
    
    # invest victim
    add_foreign_key :invest_victims, :investigations
    add_foreign_key :invest_victims, :people
    
    # invest warrant
    add_foreign_key :invest_warrants, :investigations
    add_foreign_key :invest_warrants, :warrants
    
    # invest witness
    add_foreign_key :invest_witnesses, :investigations
    add_foreign_key :invest_witnesses, :people
    
    # investigation
    add_foreign_key :investigations, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :investigations, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :investigations, :users, :column => 'owned_by', :dependent => :nullify
    
    # jail
    add_foreign_key :jails, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :jails, :users, :column => 'updated_by', :dependent => :nullify
    
    # jail absent type
    add_foreign_key :jail_absent_types, :jails
    
    # jail cell
    add_foreign_key :jail_cells, :jails
    
    # medical
    add_foreign_key :medicals, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :medicals, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :medicals, :bookings
    
    # medication
    add_foreign_key :medications, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :medications, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :medications, :people
    add_foreign_key :medications, :contacts, :column => 'physician_id'
    add_foreign_key :medications, :contacts, :column => 'pharmacy_id'
    
    # medication schedule
    add_foreign_key :medication_schedules, :medications
    
    # message
    add_foreign_key :messages, :users, :column => 'receiver_id'
    add_foreign_key :messages, :users, :column => 'sender_id'
    
    # message log
    add_foreign_key :message_logs, :users, :column => 'receiver_id'
    add_foreign_key :message_logs, :users, :column => 'sender_id'
    
    # offense
    add_foreign_key :offenses, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :offenses, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :offenses, :calls
    
    # offense arrest
    add_foreign_key :offense_arrests, :offenses
    add_foreign_key :offense_arrests, :arrests
    
    # offense citation
    add_foreign_key :offense_citations, :offenses
    add_foreign_key :offense_citations, :citations
    
    # offense evidence
    add_foreign_key :offense_evidences, :offenses
    add_foreign_key :offense_evidences, :evidences
    
    # offense forbid
    add_foreign_key :offense_forbids, :offenses
    add_foreign_key :offense_forbids, :forbids
    
    # offense photo
    add_foreign_key :offense_photos, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :offense_photos, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :offense_photos, :offenses
    
    # offense suspect
    add_foreign_key :offense_suspects, :offenses
    add_foreign_key :offense_suspects, :people
    
    # offense victim
    add_foreign_key :offense_victims, :offenses
    add_foreign_key :offense_victims, :people
    
    # offense witness
    add_foreign_key :offense_witnesses, :offenses
    add_foreign_key :offense_witnesses, :people
    
    # offense wrecker
    add_foreign_key :offense_wreckers, :offenses
    
    # paper
    add_foreign_key :papers, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :papers, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :papers, :paper_customers
    
    # paper type
    add_foreign_key :paper_types, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :paper_types, :users, :column => 'updated_by', :dependent => :nullify
  
    # paper customer
    add_foreign_key :paper_customers, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :paper_customers, :users, :column => 'updated_by', :dependent => :nullify
  
    # paper payment
    add_foreign_key :paper_payments, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :paper_payments, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :paper_payments, :papers
    add_foreign_key :paper_payments, :paper_customers
  
    # patrol
    add_foreign_key :patrols, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :patrols, :users, :column => 'updated_by', :dependent => :nullify
    
    # pawn
    add_foreign_key :pawns, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :pawns, :users, :column => 'updated_by', :dependent => :nullify
    
    # pawn item
    add_foreign_key :pawn_items, :pawns
    
    # pawn item type
    add_foreign_key :pawn_item_types, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :pawn_item_types, :users, :column => 'updated_by', :dependent => :nullify
    
    # perm - nothing to do
    
    # person
    add_foreign_key :people, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :people, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :people, :people, :column => 'is_alias_for'
    
    # person associate
    add_foreign_key :person_associates, :people
    
    # person mark
    add_foreign_key :person_marks, :people
    
    # probation
    add_foreign_key :probations, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :probations, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :probations, :people
    add_foreign_key :probations, :dockets
    add_foreign_key :probations, :courts
    add_foreign_key :probations, :da_charges
    
    # probation payment
    add_foreign_key :probation_payments, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :probation_payments, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :probation_payments, :probations
    
    # revocation
    add_foreign_key :revocations, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :revocations, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :revocations, :dockets
    add_foreign_key :revocations, :arrests
    
    # revocation consecutive
    add_foreign_key :revocation_consecutives, :revocations
    add_foreign_key :revocation_consecutives, :dockets
    
    # sentence
    add_foreign_key :sentences, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :sentences, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :sentences, :dockets
    add_foreign_key :sentences, :courts
    add_foreign_key :sentences, :probations
    add_foreign_key :sentences, :da_charges
    
    # sentence consecutive
    add_foreign_key :sentence_consecutives, :sentences
    add_foreign_key :sentence_consecutives, :dockets
    
    # session - nothing to do
    
    # signal Code
    add_foreign_key :signal_codes, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :signal_codes, :users, :column => 'updated_by', :dependent => :nullify
    
    # site configuration
    add_foreign_key :site_configurations, :users, :column => 'support_ticket_admin_id'
    
    # state - nothing to do
    
    # statute
    add_foreign_key :statutes, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :statutes, :users, :column => 'updated_by', :dependent => :nullify
    
    # stolen
    add_foreign_key :stolens, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :stolens, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :stolens, :people
    
    # support ticket
    add_foreign_key :support_tickets, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :support_tickets, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :support_tickets, :users, :column => 'reporter_id', :dependent => :nullify
    add_foreign_key :support_tickets, :users, :column => 'admin_id', :dependent => :nullify
    add_foreign_key :support_tickets, :users, :column => 'assigned_id', :dependent => :nullify
    
    # support ticket comment
    add_foreign_key :support_ticket_comments, :support_tickets
    add_foreign_key :support_ticket_comments, :users
    
    # support ticket watcher
    add_foreign_key :support_ticket_watchers, :support_tickets
    add_foreign_key :support_ticket_watchers, :users
    
    # transfer
    add_foreign_key :transfers, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :transfers, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :transfers, :people
    add_foreign_key :transfers, :bookings
    
    # transport
    add_foreign_key :transports, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :transports, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :transports, :people
    
    # user
    add_foreign_key :users, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :users, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :users, :contacts
    
    # user group
    add_foreign_key :user_groups, :users
    add_foreign_key :user_groups, :groups
    
    # visitor
    add_foreign_key :visitors, :bookings
    
    # warrant
    add_foreign_key :warrants, :users, :column => 'created_by', :dependent => :nullify
    add_foreign_key :warrants, :users, :column => 'updated_by', :dependent => :nullify
    add_foreign_key :warrants, :arrests
    add_foreign_key :warrants, :people
    
    # warrant exemption
    add_foreign_key :warrant_exemptions, :warrants
    add_foreign_key :warrant_exemptions, :people
    
  end

  def down
    # warrant
    remove_foreign_key :warrants, :column => 'created_by'
    remove_foreign_key :warrants, :column => 'updated_by'
    remove_foreign_key :warrants, :arrests
    remove_foreign_key :warrants, :people
    
    # warrant exemption
    remove_foreign_key :warrant_exemptions, :warrants
    remove_foreign_key :warrant_exemptions, :people
    
    # visitor
    remove_foreign_key :visitors, :bookings
    
    # transport
    remove_foreign_key :transports, :users, :column => 'created_by', :dependent => :nullify
    remove_foreign_key :transports, :users, :column => 'updated_by', :dependent => :nullify
    remove_foreign_key :transports, :people
    
    # transfer
    remove_foreign_key :transfers, :column => 'created_by'
    remove_foreign_key :transfers, :column => 'updated_by'
    remove_foreign_key :transfers, :people
    remove_foreign_key :transfers, :bookings
    
    # support ticket
    remove_foreign_key :support_tickets, :column => 'created_by'
    remove_foreign_key :support_tickets, :column => 'updated_by'
    remove_foreign_key :support_tickets, :column => 'reporter_id'
    remove_foreign_key :support_tickets, :column => 'admin_id'
    remove_foreign_key :support_tickets, :column => 'assigned_id'
    
    # support ticket comment
    remove_foreign_key :support_ticket_comments, :support_tickets
    remove_foreign_key :support_ticket_comments, :user_id
    
    # support ticket watcher
    remove_foreign_key :support_ticket_watchers, :support_tickets
    remove_foreign_key :support_ticket_watchers, :user_id
    
    # stolen
    remove_foreign_key :stolens, :column => 'created_by'
    remove_foreign_key :stolens, :column => 'updated_by'
    remove_foreign_key :stolens, :people
    
    # statute
    remove_foreign_key :statutes, :column => 'created_by'
    remove_foreign_key :statutes, :column => 'updated_by'
    
    # state - nothing to do
    
    # site configuration
    remove_foreign_key :site_configurations, :column => 'support_ticket_admin_id'
    
    # signal Code
    remove_foreign_key :signal_codes, :column => 'created_by'
    remove_foreign_key :signal_codes, :column => 'updated_by'
  
    # session - nothing to do
    
    # revocation
    remove_foreign_key :revocations, :column => 'created_by'
    remove_foreign_key :revocations, :column => 'updated_by'
    remove_foreign_key :revocations, :dockets
    remove_foreign_key :revocations, :arrests
    
    # revocation consecutive
    remove_foreign_key :revocation_consecutives, :revocations
    remove_foreign_key :revocation_consecutives, :dockets
    
    # sentence
    remove_foreign_key :sentences, :column => 'created_by'
    remove_foreign_key :sentences, :column => 'updated_by'
    remove_foreign_key :sentences, :dockets
    remove_foreign_key :sentences, :courts
    remove_foreign_key :sentences, :probations
    remove_foreign_key :sentences, :da_charges
    
    # sentence consecutive
    remove_foreign_key :sentence_consecutives, :sentences
    remove_foreign_key :sentence_consecutives, :dockets
    
    # probation
    remove_foreign_key :probations, :column => 'created_by'
    remove_foreign_key :probations, :column => 'updated_by'
    remove_foreign_key :probations, :people
    remove_foreign_key :probations, :dockets
    remove_foreign_key :probations, :courts
    remove_foreign_key :probations, :da_charges
    
    # probation payment
    remove_foreign_key :probation_payments, :column => 'created_by'
    remove_foreign_key :probation_payments, :column => 'updated_by'
    remove_foreign_key :probation_payments, :probations
    
    # person
    remove_foreign_key :people, :column => 'created_by'
    remove_foreign_key :people, :column => 'updated_by'
    remove_foreign_key :people, :people, :column => 'is_alias_for'
    
    # person associate
    remove_foreign_key :person_associates, :people
    
    # person mark
    remove_foreign_key :person_marks, :people
    
    # perm - nothing to do
    
    # pawn
    remove_foreign_key :pawns, :column => 'created_by'
    remove_foreign_key :pawns, :column => 'updated_by'
    
    # pawn item
    remove_foreign_key :pawn_items, :pawns
    
    # pawn item type
    remove_foreign_key :pawn_item_types, :column => 'created_by'
    remove_foreign_key :pawn_item_types, :column => 'updated_by'
    
    # patrol
    remove_foreign_key :patrols, :column => 'created_by'
    remove_foreign_key :patrols, :column => 'updated_by'
    
    # paper payment
    remove_foreign_key :paper_payments, :column => 'created_by'
    remove_foreign_key :paper_payments, :column => 'updated_by'
    remove_foreign_key :paper_payments, :papers
    remove_foreign_key :paper_payments, :paper_customers
  
    # paper customer
    remove_foreign_key :paper_customers, :column => 'created_by'
    remove_foreign_key :paper_customers, :column => 'updated_by'
  
    # paper type
    remove_foreign_key :paper_types, :column => 'created_by'
    remove_foreign_key :paper_types, :column => 'updated_by'
  
    # papers
    remove_foreign_key :papers, :column => 'created_by'
    remove_foreign_key :papers, :column => 'updated_by'
    remove_foreign_key :papers, :paper_customers
    
    # offense wrecker
    remove_foreign_key :offense_wreckers, :offenses
    
    # offense witness
    remove_foreign_key :offense_witnesses, :offenses
    remove_foreign_key :offense_witnesses, :people
    
    # offense victim
    remove_foreign_key :offense_victims, :offenses
    remove_foreign_key :offense_victims, :people
    
    # offense suspects
    remove_foreign_key :offense_suspects, :offenses
    remove_foreign_key :offense_suspects, :people
    
    # offense photo
    remove_foreign_key :offense_photos, :column => 'created_by'
    remove_foreign_key :offense_photos, :column => 'updated_by'
    remove_foreign_key :offense_photos, :offenses
    
    # offense forbid
    remove_foreign_key :offense_forbids, :offenses
    remove_foreign_key :offense_forbids, :forbids
    
    # offense evidence
    remove_foreign_key :offense_evidences, :offenses
    remove_foreign_key :offense_evidences, :evidences
    
    # offense citation
    remove_foreign_key :offense_citations, :offenses
    remove_foreign_key :offense_citations, :citations
    
    # offense arrest
    remove_foreign_key :offense_arrests, :offenses
    remove_foreign_key :offense_arrests, :arrests
    
    # offense
    remove_foreign_key :offenses, :column => 'created_by'
    remove_foreign_key :offenses, :column => 'updated_by'
    remove_foreign_key :offenses, :calls
    
    # message log
    remove_foreign_key :message_logs, :users, :column => 'receiver_id'
    remove_foreign_key :message_logs, :users, :column => 'sender_id'
    
    # message
    remove_foreign_key :messages, :users, :column => 'receiver_id'
    remove_foreign_key :messages, :users, :column => 'sender_id'
    
    # medication schedule
    remove_foreign_key :medication_schedules, :medications
    
    # medication
    remove_foreign_key :medications, :column => 'created_by'
    remove_foreign_key :medications, :column => 'updated_by'
    remove_foreign_key :medications, :people
    remove_foreign_key :medications, :column => 'physician_id'
    remove_foreign_key :medications, :column => 'pharmacy_id'
    
    # medical
    remove_foreign_key :medicals, :users, :column => 'created_by', :dependent => :nullify
    remove_foreign_key :medicals, :users, :column => 'updated_by', :dependent => :nullify
    remove_foreign_key :medicals, :bookings
    
    # jail cell
    remove_foreign_key :jail_cells, :jails
    
    # jail absent type
    remove_foreign_key :jail_absent_types, :jails
    
    # jail
    remove_foreign_key :jails, :column => 'created_by'
    remove_foreign_key :jails, :column => 'updated_by'
    
    # investigations
    remove_foreign_key :investigations, :column => 'created_by'
    remove_foreign_key :investigations, :column => 'updated_by'
    remove_foreign_key :investigations, :column => 'owned_by'
    
    # invest witness
    remove_foreign_key :invest_witnesses, :investigations
    remove_foreign_key :invest_witnesses, :people
    
    # invest warrant
    remove_foreign_key :invest_warrants, :investigation
    remove_foreign_key :invest_warrants, :warrants
    
    # invest victim
    remove_foreign_key :invest_victims, :investigations
    remove_foreign_key :invest_victims, :people
    
    # invest vehicle photo
    remove_foreign_key :invest_vehicle_photos, :invest_vehicles
    remove_foreign_key :invest_vehicle_photos, :invest_photos
    
    # invest vehicle
    remove_foreign_key :invest_vehicles, :column => 'created_by'
    remove_foreign_key :invest_vehicles, :column => 'updated_by'
    remove_foreign_key :invest_vehicles, :column => 'owned_by'
    remove_foreign_key :invest_vehicles, :investigations
    remove_foreign_key :invest_vehicles, :column => 'owner_id'
    
    # invest stolen
    remove_foreign_key :invest_stolens, :investigations
    remove_foreign_key :invest_stolens, :stolens
    
    # invest source
    remove_foreign_key :invest_sources, :investigations
    remove_foreign_key :invest_sources, :people
    
    # invest report witness
    remove_foreign_key :invest_report_witnesses, :invest_reports
    remove_foreign_key :invest_report_witnesses, :invest_witnesses
    
    # invest report victim
    remove_foreign_key :invest_report_victims, :invest_reports
    remove_foreign_key :invest_report_victims, :invest_victims
    
    # invest report vehicle
    remove_foreign_key :invest_report_vehicles, :invest_reports
    remove_foreign_key :invest_report_vehicles, :invest_vehicles
    
    # invest report photo
    remove_foreign_key :invest_report_photos, :invest_reports
    remove_foreign_key :invest_report_photos, :invest_photos
    
    # invest report offense
    remove_foreign_key :invest_report_offenses, :invest_reports
    remove_foreign_key :invest_report_offenses, :invest_offenses
    
    # invest report criminal
    remove_foreign_key :invest_report_criminals, :invest_reports
    remove_foreign_key :invest_report_criminals, :invest_criminals
    
    # invest report arrest
    remove_foreign_key :invest_report_arrests, :invest_reports
    remove_foreign_key :invest_report_arrests, :invest_arrests
    
    # invest report
    remove_foreign_key :invest_reports, :column => 'created_by'
    remove_foreign_key :invest_reports, :column => 'updated_by'
    remove_foreign_key :invest_reports, :column => 'owned_by'
    remove_foreign_key :invest_reports, :investigations
    
    # invest photo
    remove_foreign_key :invest_photos, :column => 'created_by'
    remove_foreign_key :invest_photos, :column => 'updated_by'
    remove_foreign_key :invest_photos, :column => 'owned_by'
    remove_foreign_key :invest_photos, :investigations
    
    # invest offense
    remove_foreign_key :invest_offenses, :investigations
    remove_foreign_key :invest_offenses, :offenses
    
    # invest evidence
    remove_foreign_key :invest_evidences, :investigations
    remove_foreign_key :invest_evidences, :evidences
    
    # invest criminal warrants
    remove_foreign_key :invest_criminal_warrants, :invest_criminals
    remove_foreign_key :invest_criminal_warrants, :invest_warrants
    
    # invest criminal vehicles
    remove_foreign_key :invest_criminal_vehicles, :invest_criminals
    remove_foreign_key :invest_criminal_vehicles, :invest_vehicles
    
    # invest criminal stolens
    remove_foreign_key :invest_criminal_stolens, :invest_criminals
    remove_foreign_key :invest_criminal_stolens, :invest_stolens
    
    # invest criminal photos
    remove_foreign_key :invest_criminal_photos, :invest_criminals
    remove_foreign_key :invest_criminal_photos, :invest_photos
    
    # invest criminal evidence
    remove_foreign_key :invest_criminal_evidences, :invest_criminals
    remove_foreign_key :invest_criminal_evidences, :invest_evidences
    
    # invest criminal arrest
    remove_foreign_key :invest_criminal_arrests, :invest_criminals
    remove_foreign_key :invest_criminal_arrests, :invest_arrests
    
    # invest criminal
    remove_foreign_key :invest_criminals, :column => 'created_by'
    remove_foreign_key :invest_criminals, :column => 'updated_by'
    remove_foreign_key :invest_criminals, :column => 'owned_by'
    remove_foreign_key :invest_criminals, :investigations
    remove_foreign_key :invest_criminals, :people
    
    # invest note
    remove_foreign_key :invest_notes, :column => 'created_by'
    remove_foreign_key :invest_notes, :column => 'updated_by'
    remove_foreign_key :invest_notes, :investigations
    remove_foreign_key :invest_notes, :invest_crimes
    remove_foreign_key :invest_notes, :invest_criminals
    remove_foreign_key :invest_notes, :invest_photos
    remove_foreign_key :invest_notes, :invest_reports
    remove_foreign_key :invest_notes, :invest_vehicles
    
    # invest crime witness
    remove_foreign_key :invest_crime_witnesses, :invest_crimes
    remove_foreign_key :invest_crime_witnesses, :invest_witnesses
    
    # invest crime warrant
    remove_foreign_key :invest_crime_warrants, :invest_crimes
    remove_foreign_key :invest_crime_warrants, :invest_warrants
    
    # invest crime victim
    remove_foreign_key :invest_crime_victims, :invest_crimes
    remove_foreign_key :invest_crime_victims, :invest_victims
    
    # invest crime vehicle
    remove_foreign_key :invest_crime_vehicles, :invest_crimes
    remove_foreign_key :invest_crime_vehicles, :invest_vehicles
    
    # invest crime stolen
    remove_foreign_key :invest_crime_stolens, :invest_crimes
    remove_foreign_key :invest_crime_stolens, :invest_stolens
    
    # invest crime photo
    remove_foreign_key :invest_crime_photos, :invest_crimes
    remove_foreign_key :invest_crime_photos, :invest_photos
    
    # invest crime offense
    remove_foreign_key :invest_crime_offenses, :invest_crimes
    remove_foreign_key :invest_crime_offenses, :invest_offenses
    
    # invest crime evidence
    remove_foreign_key :invest_crime_evidences, :invest_crimes
    remove_foreign_key :invest_crime_evidences, :invest_evidences
    
    # invest crime criminal
    remove_foreign_key :invest_crime_criminals, :invest_crimes
    remove_foreign_key :invest_crime_criminals, :invest_criminals
    
    # invest crime contact
    remove_foreign_key :invest_crime_contacts, :invest_crimes
    remove_foreign_key :invest_crime_contacts, :invest_contacts
    
    # invest crime case
    remove_foreign_key :invest_crime_cases, :invest_crimes
    remove_foreign_key :invest_crime_cases, :invest_cases
    
    # invest crime arrest
    remove_foreign_key :invest_crime_arrests, :invest_crimes
    remove_foreign_key :invest_crime_arrests, :invest_arrests
    
    # invest crime
    remove_foreign_key :invest_crimes, :column => 'created_by'
    remove_foreign_key :invest_crimes, :column => 'updated_by'
    remove_foreign_key :invest_crimes, :column => 'owned_by'
    remove_foreign_key :invest_crimes, :investigations
    
    # invest contact
    remove_foreign_key :invest_contacts, :investigations
    remove_foreign_key :invest_contacts, :contacts
    
    # invest case
    remove_foreign_key :invest_cases, :investigations
    remove_foreign_key :invest_cases, :calls
    
    # invest arrest
    remove_foreign_key :invest_arrests, :investigations
    remove_foreign_key :invest_arrests, :arrests
    
    # hold bond
    remove_foreign_key :hold_bonds, :holds
    remove_foreign_key :hold_bonds, :bonds
    
    # hold blocker
    remove_foreign_key :hold_blockers, :holds
    remove_foreign_key :hold_blockers, :column => 'blocker_id'
    
    # hold adjustment - nothing to do
    
    # hold
    remove_foreign_key :holds, :column => 'created_by'
    remove_foreign_key :holds, :column => 'updated_by'
    remove_foreign_key :holds, :bookings
    remove_foreign_key :holds, :arrests
    remove_foreign_key :holds, :probations
    remove_foreign_key :holds, :sentences
    remove_foreign_key :holds, :transfers
    remove_foreign_key :holds, :revocations
    
    # history - nothing to do
    
    # group
    remove_foreign_key :groups, :column => 'created_by'
    remove_foreign_key :groups, :column => 'updated_by'
    
    # garnishment
    remove_foreign_key :garnishments, :column => 'created_by'
    remove_foreign_key :garnishments, :column => 'updated_by'
    
    # forum view
    remove_foreign_key :forum_views, :forums
    remove_foreign_key :forum_views, :forum_topics
    remove_foreign_key :forum_views, :users
    
    # forum topic
    remove_foreign_key :forum_topics, :forums
    remove_foreign_key :forum_topics, :users
    
    # forum subscription
    remove_foreign_key :forum_subscriptions, :forum_topics
    remove_foreign_key :forum_subscriptions, :column => 'subscriber_id'
    
    # forum post
    remove_foreign_key :forum_posts, :forum_topics
    remove_foreign_key :forum_posts, :users
    remove_foreign_key :forum_posts, :column => 'reply_to_id'
    
    # forum category - nothing to do
    
    # forum
    remove_foreign_key :forums, :column => 'created_by'
    remove_foreign_key :forums, :column => 'updated_by'
    remove_foreign_key :forums, :forum_categories
    
    # forbid exemption
    remove_foreign_key :forbid_exemptions, :people
    remove_foreign_key :forbid_exemptions, :forbids
    
    # forbid
    remove_foreign_key :forbids, :column => 'created_by'
    remove_foreign_key :forbids, :column => 'updated_by'
    remove_foreign_key :forbids, :column => 'forbidden_id'
    remove_foreign_key :forbids, :calls
    
    # fax cover
    remove_foreign_key :fax_covers, :column => 'created_by'
    remove_foreign_key :fax_covers, :column => 'updated_by'
    
    # external links
    remove_foreign_key :external_links, :column => 'created_by'
    remove_foreign_key :external_links, :column => 'updated_by'
    
    # evidence locations
    remove_foreign_key :evidence_locations, :evidences
    
    # evidence
    remove_foreign_key :evidences, :column => 'created_by'
    remove_foreign_key :evidences, :column => 'updated_by'
    remove_foreign_key :evidences, :column => 'owner_id'
    
    # event
    remove_foreign_key :events, :column => 'created_by'
    remove_foreign_key :events, :column => 'updated_by'
    remove_foreign_key :events, :column => 'owned_by'
    remove_foreign_key :events, :investigations
    
    # docket type
    remove_foreign_key :docket_types, :column => 'created_by'
    remove_foreign_key :docket_types, :column => 'updated_by'
    
    # docket
    remove_foreign_key :dockets, :column => 'created_by'
    remove_foreign_key :dockets, :column => 'updated_by'
    remove_foreign_key :dockets, :people
    
    # doc
    remove_foreign_key :docs, :column => 'created_by'
    remove_foreign_key :docs, :column => 'updated_by'
    remove_foreign_key :docs, :transfers
    remove_foreign_key :docs, :holds
    remove_foreign_key :docs, :sentences
    remove_foreign_key :docs, :bookings
    remove_foreign_key :docs, :people
    remove_foreign_key :docs, :revocations
    
    # absent
    remove_foreign_key :absents, :column => 'created_by'
    remove_foreign_key :absents, :column => 'updated_by'
    remove_foreign_key :absents, :jails

    # absent prisoner
    remove_foreign_key :absent_prisoners, :absents
    remove_foreign_key :absent_prisoners, :bookings
    
    # activity
    remove_foreign_key :activities, :users
  
    # announcement
    remove_foreign_key :announcements, :column => 'created_by'
    remove_foreign_key :announcements, :column => 'updated_by'

    # announcement ignore
    remove_foreign_key :announcement_ignores, :announcements
    remove_foreign_key :announcement_ignores, :users
  
    # arrest
    remove_foreign_key :arrests, :people
    remove_foreign_key :arrests, :column => 'created_by'
    remove_foreign_key :arrests, :column => 'updated_by'

    # arrest officer
    remove_foreign_key :arrest_officers, :column => 'officer_id'
    remove_foreign_key :arrest_officers, :arrests
    
    # bonds
    remove_foreign_key :bonds, :column => 'created_by'
    remove_foreign_key :bonds, :column => 'updated_by'
    remove_foreign_key :bonds, :arrests
    remove_foreign_key :bonds, :people
    remove_foreign_key :bonds, :column => 'surety_id'
  
    # booking
    remove_foreign_key :bookings, :jails
    remove_foreign_key :bookings, :people
    remove_foreign_key :bookings, :column => 'created_by'
    remove_foreign_key :bookings, :column => 'updated_by'
    
    # booking log
    remove_foreign_key :booking_logs, :bookings
    
    # booking property
    remove_foreign_key :booking_properties, :bookings
  
    # call
    remove_foreign_key :calls, :column => 'created_by'
    remove_foreign_key :calls, :column => 'updated_by'
    
    # call subject
    remove_foreign_key :call_subjects, :calls
    
    # call unit
    remove_foreign_key :call_units, :calls
    remove_foreign_key :call_units, :column => 'officer_id'
    
    # case - nothing to do
    
    # case note
    remove_foreign_key :case_notes, :column => 'created_by'
    remove_foreign_key :case_notes, :column => 'updated_by'
    
    # charge
    remove_foreign_key :charges, :arrests
    remove_foreign_key :charges, :citation
    remove_foreign_key :charges, :warrants
    remove_foreign_key :charges, :column => 'agency_id'
    
    # citation
    remove_foreign_key :citations, :column => 'created_by'
    remove_foreign_key :citations, :column => 'updated_by'
    remove_foreign_key :citations, :people
    
    # commissary
    remove_foreign_key :commissaries, :column => 'created_by'
    remove_foreign_key :commissaries, :column => 'updated_by'
    remove_foreign_key :commissaries, :people
    remove_foreign_key :commissaries, :jail
    
    # commissary item
    remove_foreign_key :commissary_items, :column => 'created_by'
    remove_foreign_key :commissary_items, :column => 'updated_by'
    remove_foreign_key :commissary_items, :jail
    
    # contact
    remove_foreign_key :contacts, :column => 'created_by'
    remove_foreign_key :contacts, :column => 'updated_by'
    remove_foreign_key :contacts, :column => 'agency_id'
    
    # contact bondsman company
    remove_foreign_key :contact_bondsman_companies, :contacts
    remove_foreign_key :contact_bondsman_companies, :column => 'bondsman_id'
    remove_foreign_key :contact_bondsman_companies, :column => 'company_id'
    
    # court
    remove_foreign_key :courts, :column => 'created_by'
    remove_foreign_key :courts, :column => 'updated_by'
    remove_foreign_key :courts, :dockets
    
    # court cost
    remove_foreign_key :court_costs, :column => 'created_by'
    remove_foreign_key :court_costs, :column => 'updated_by'
    
    # court type
    remove_foreign_key :court_types, :column => 'created_by'
    remove_foreign_key :court_types, :column => 'updated_by'
    
    # da charge
    remove_foreign_key :da_charges, :dockets
    remove_foreign_key :da_charges, :charges
    remove_foreign_key :da_charges, :arrests
    
    # disposition
    remove_foreign_key :dispositions, :column => 'created_by'
    remove_foreign_key :dispositions, :column => 'updated_by'
    remove_foreign_key :dispositions, :people
  end
end
