class MakeAbsentTypesJailDependent < ActiveRecord::Migration
  class Jail < ActiveRecord::Base
    has_many :jail_absent_types, :class_name => "MakeAbsentTypesJailDependent::JailAbsentType"
  end
  class JailAbsentType < ActiveRecord::Base
    belongs_to :jail, :class_name => "MakeAbsentTypesJailDependent::Jail"
  end
  
  def up
    rename_table :absent_types, :jail_absent_types
    remove_column :jail_absent_types, :created_by
    remove_column :jail_absent_types, :updated_by
    add_column :jail_absent_types, :jail_id, :integer
    add_index :jail_absent_types, :jail_id
    
    # attach all current absent types to jail id 1
    MakeAbsentTypesJailDependent::JailAbsentType.reset_column_information
    MakeAbsentTypesJailDependent::JailAbsentType.all.each do |t|
      t.update_attribute(:jail_id, 1)
    end
    # duplicate all absent types to jail id 2
    MakeAbsentTypesJailDependent::Jail.find(1).jail_absent_types.each do |t|
      MakeAbsentTypesJailDependent::JailAbsentType.create(:name => t.name, :jail_id => 2)
    end
  end

  def down
    MakeAbsentTypesJailDependent::JailAbsentType.delete_all('jail_id = 2')
    remove_column :jail_absent_types, :jail_id
    add_column :jail_absent_types, :created_by, :integer, :default => 1
    add_column :jail_absent_types, :updated_by, :integer, :default => 1
    rename_table :jail_absent_types, :absent_types
    add_index :absent_types, :created_by
    add_index :absent_types, :updated_by
  end
end
