class UnPolymorphicInvestNote < ActiveRecord::Migration
  class InvestNote < ActiveRecord::Base
  end
  
  def up
    add_column :invest_notes, :investigation_id, :integer
    add_index :invest_notes, :investigation_id
    add_column :invest_notes, :invest_crime_id, :integer
    add_index :invest_notes, :invest_crime_id
    add_column :invest_notes, :invest_criminal_id, :integer
    add_index :invest_notes, :invest_criminal_id
    add_column :invest_notes, :invest_photo_id, :integer
    add_index :invest_notes, :invest_photo_id
    add_column :invest_notes, :invest_report_id, :integer
    add_index :invest_notes, :invest_report_id
    add_column :invest_notes, :invest_vehicle_id, :integer
    add_index :invest_notes, :invest_vehicle_id
    
    UnPolymorphicInvestNote::InvestNote.reset_column_information
    UnPolymorphicInvestNote::InvestNote.all.each do |n|
      case n.invest_record_type
      when 'Investigation'
        n.update_attribute(:investigation_id, n.invest_record_id)
      when 'InvestCrime'
        n.update_attribute(:invest_crime_id, n.invest_record_id)
      when 'InvestCriminal'
        n.update_attribute(:invest_criminal_id, n.invest_record_id)
      when 'InvestPhoto'
        n.update_attribute(:invest_photo_id, n.invest_record_id)
      when 'InvestReport'
        n.update_attribute(:invest_report_id, n.invest_record_id)
      when 'InvestVehicle'
        n.update_attribute(:invest_vehicle_id, n.invest_record_id)
      end
    end
    remove_column :invest_notes, :invest_record_type
    remove_column :invest_notes, :invest_record_id
    
    # forgot to add these in a previous view
    add_index :forum_views, :forum_id
    add_index :forum_views, :forum_topic_id
  end
    
  def down
    add_column :invest_notes, :invest_record_type, :string
    add_column :invest_notes, :invest_record_id, :integer
    UnPolymorphicInvestNote::InvestNote.reset_column_information
    UnPolymorphicInvestNote::InvestNote.all.each do |n|
      if n.investigation_id?
        n.update_attribute(:invest_record_type, 'Investigation')
        n.update_attribute(:invest_record_id, n.investigation_id)
      elsif n.invest_crime_id?
        n.update_attribute(:invest_record_type, 'InvestCrime')
        n.update_attribute(:invest_record_id, n.invest_crime_id)
      elsif n.invest_criminal_id?
        n.update_attribute(:invest_record_type, 'InvestCriminal')
        n.update_attribute(:invest_record_id, n.invest_criminal_id)
      elsif n.invest_photo_id?
        n.update_attribute(:invest_record_type, 'InvestPhoto')
        n.update_attribute(:invest_record_id, n.invest_photo_id)
      elsif n.invest_report_id?
        n.update_attribute(:invest_record_type, 'InvestReport')
        n.update_attribute(:invest_record_id, n.invest_report_id)
      elsif n.invest_vehicle_id?
        n.update_attribute(:invest_record_type, 'InvestVehicle')
        n.update_attribute(:invest_record_id, n.invest_vehicle_id)
      end
    end
    remove_column :investigation_id
    remove_column :invest_crime_id
    remove_column :invest_criminal_id
    remove_column :invest_photo_id
    remove_column :invest_report_id
    remove_column :invest_vehicle_id
    
    remove_index :forum_views, :forum_id
    remove_index :forum_views, :forum_topic_id
  end
end
