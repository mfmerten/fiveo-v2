# Adds jail and jail_cell tables which will take the place of the original
# facility and cell options.
#
# Note, includes a section for adding default jails for SPSO Agency and for
# updating booking to include jail_id and to convert facility_id and cell_id
# to work with the new jail and jail_cell models. That was included here
# because at this time, SPSO is the only agency using FiveO.
#
class AddJails < ActiveRecord::Migration
  class Jail < ActiveRecord::Base
    has_many :cells, :class_name => "AddJails::JailCell"
    has_many :bookings, :class_name => "AddJails::Booking"
  end
  class JailCell < ActiveRecord::Base
    belongs_to :jail, :class_name => "AddJails::Jail"
    has_many :bookings, :class_name => "AddJails::Booking"
  end
  class Option < ActiveRecord::Base
  end
  class Booking < ActiveRecord::Base
    belongs_to :jail, :class_name => "AddJails::Jail"
    belongs_to :cell, :class_name => "AddJails::JailCell"
    belongs_to :old_cell, :class_name => "AddJails::Option"
  end

  def up
    create_table :jails, :force => true do |t|
      t.string :name
      t.string :acronym
      t.string :street
      t.string :city
      t.string :state
      t.string :zip
      t.string :phone
      t.string :fax
      t.boolean :juvenile, :default => false
      t.boolean :male, :default => false
      t.boolean :female, :default => false
      t.integer :permset
      t.boolean :goodtime_by_default, :default => false
      t.decimal :goodtime_hours_per_day, :precision => 12, :scale => 2, :default => 0.0
      t.integer :warden_id, :default => 1
      t.boolean :afis_enabled, :default => false
      t.text :remarks
      t.integer :created_by, :default => 1
      t.integer :updated_by, :default => 1
      t.timestamps
    end
    add_index :jails, :permset
    add_index :jails, :warden_id
    add_index :jails, :created_by
    add_index :jails, :updated_by
    
    create_table :jail_cells, :force => true do |t|
      t.integer :jail_id
      t.string :name
      t.string :description
      t.integer :created_by, :default => 1
      t.integer :updated_by, :default => 1
      t.timestamps
    end
    add_index :jail_cells, :jail_id
    add_index :jail_cells, :created_by
    add_index :jail_cells, :updated_by
    
    ####### Begin SPSO specific section
    
    AddJails::Jail.reset_column_information
    # record 1
    AddJails::Jail.create(:name => "Sabine Parish Detention Center",
      :acronym => "SPDC",
      :street => "384 Detention Center Road",
      :city => "Many",
      :state => "LA",
      :zip => "71449",
      :phone => "318-256-0006",
      :fax => "318-256-4518",
      :juvenile => false,
      :male => true,
      :female => false,
      :permset => 1,
      :goodtime_by_default => true,
      :goodtime_hours_per_day => 24,
      :warden_id => 1,
      :afis_enabled => true,
      :created_by => 1,
      :updated_by => 1)
    # record 2
    AddJails::Jail.create(:name => "Sabine Parish Womens Facility",
      :acronym => "SPWF",
      :street => "400 North Capitol Street",
      :city => "Many",
      :state => "LA",
      :zip => "71449",
      :phone => "318-256-9241",
      :fax => "318-256-3409",
      :juvenile => false,
      :male => false,
      :female => true,
      :permset => 1,
      :goodtime_by_default => true,
      :goodtime_hours_per_day => 24,
      :warden_id => 1,
      :afis_enabled => false,
      :created_by => 1,
      :updated_by => 1)
    
    spdc = AddJails::Option.where(:short_name => "SPDC").first
    spwf = AddJails::Option.where(:short_name => "SPWF").first
    jail1 = AddJails::Jail.find(1)
    jail2 = AddJails::Jail.find(2)
    add_column :bookings, :jail_id, :integer
    rename_column :bookings, :cell_id, :old_cell_id
    add_column :bookings, :cell_id, :integer
    
    AddJails::Booking.reset_column_information
    AddJails::Booking.all.each do |b|
      b.jail_id = jail1.id if b.facility_id == spdc.id
      b.jail_id = jail2.id if b.facility_id == spwf.id
      current_jail = Jail.find(b.jail_id)
      unless b.old_cell.nil?
        cell_name = b.old_cell.long_name
        c = AddJails::JailCell.where(:jail_id => current_jail.id, :name => cell_name).first
        if c.nil?
          c = current_jail.cells.create(:name => cell_name, :created_by => 1, :updated_by => 1)
        end
        b.cell_id = c.id
        b.save
      end
    end
    
    # for now I'm leaving the facility_id and old_cell_id in bookings table.
    # once I'm certain this migration is working properly, I will add another
    # one to remove those columns.
  end
  
  def down
    railse ActiveRecord::IrreversibleMigration
  end
end
