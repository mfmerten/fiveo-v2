# virtualizing contact id fields (officer/agency)
class RemoveContactIdFields < ActiveRecord::Migration
  # note: this is the first of two or more migrations for contact ids. This
  # one only does absents and arrests and will be used to test the modifications
  # prior to fully implementing the change.
  def up
    remove_column :absents, :escort_officer_id
    remove_column :arrests, :officer1_id
    remove_column :arrests, :officer2_id
    remove_column :arrests, :officer3_id
    remove_column :arrests, :dwi_test_officer_id
    remove_column :arrests, :agency_id
  end

  def down
  end
end
