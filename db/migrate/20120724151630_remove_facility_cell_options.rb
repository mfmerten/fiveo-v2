class RemoveFacilityCellOptions < ActiveRecord::Migration
  class Option < ActiveRecord::Base
  end
  
  def up
    # remove all unused jail options (facility/cell)
    opts = RemoveFacilityCellOptions::Option.where('option_type in ( 12, 21, 27, 28, 29 )')
    opts.each{|o| o.destroy}
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
