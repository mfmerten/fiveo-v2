class ConvertAbsentType < ActiveRecord::Migration
  class Option < ActiveRecord::Base
    OptionTypes = {
      1 => 'Absent Type',
      2 => 'Anatomy',
      3 => 'Associate Type',
      4 => 'Bond Status',
      5 => 'Bond Type',
      # 6 => 'Bug Priority' -- obsolete, now defined in Bug model
      # 7 => 'Bug Resolution' -- obsolete, now defined in Bug model
      # 8 => 'Bug Status' -- obsolete, now defined in Bug model
      9 => 'Build',
      10 => 'Call Disposition',
      11 => 'Call Via',
      # 12 => 'Cell' -- obsolete, now defined per Jail
      13 => 'Commissary Type',
      14 => 'Complexion',
      15 => 'Consecutive Type',
      16 => 'Court Costs',
      17 => 'Court Type',
      18 => 'Docket Type',
      19 => 'Eye Color',
      20 => 'Facial Hair',
      # 21 => 'Facility' -- obsolete, now defined per Jail
      22 => 'Hair Color',
      23 => 'Hair Type',
      24 => 'History Type',
      25 => 'Hold Type',
      26 => 'Investigation Status',
      # 27 => 'Jail1 Cell',  Obsolete
      # 28 => 'Jail2 Cell',  Obsolete
      # 29 => 'Jail3 Cell',  Obsolete
      30 => 'Judge',
      31 => 'Noservice Type',
      32 => 'Pawn Company',
      33 => 'Pawn Ticket Type',
      34 => 'Pawn Type',
      35 => 'Payment Type',
      36 => 'Phone Type',
      37 => 'Plea Type',
      38 => 'Probation Status',
      39 => 'Probation Type',
      40 => 'Race',
      41 => 'Release Reason',
      42 => 'Release Type',
      43 => 'Service Type',
      44 => 'Signal Code',
      # 45 => 'State' -- obsolete, now defined in a pseudo model
      46 => 'Subject Type',
      47 => 'Temporary Release Reason',
      48 => 'Transfer Type',
      49 => 'Trial Result',
      50 => 'Vehicle Type',
      51 => 'Warrant Disposition'
    }.freeze

    def self.find_by_option_type(option_type)
      otype = get_option_type(option_type)
      if otype.nil?
        []
      else
        where(['option_type = ?', otype]).all
      end
    end

    def self.get_option_type(value)
      ConvertAbsentType::Option::OptionTypes.merge(ConvertAbsentType::Option::OptionTypes.invert)[value]
    end
  end
  class Absent < ActiveRecord::Base
    belongs_to :leave_type, :class_name => 'ConvertAbsentType::Option', :foreign_key => 'leave_type_id'
  end
  class AbsentType < ActiveRecord::Base
  end
  
  def up
    create_table :absent_types, :force => true do |t|
      t.string :name
      t.string :description
      t.integer :created_by, :default => 1
      t.integer :updated_by, :default => 1
      t.timestamps
    end
    add_index :absent_types, :created_by
    add_index :absent_types, :updated_by
    
    add_column :absents, :absent_type, :string
    
    ConvertAbsentType::Absent.reset_column_information
    ConvertAbsentType::AbsentType.reset_column_information
    
    ConvertAbsentType::Absent.all.each do |a|
      unless a.leave_type.nil?
        at = ConvertAbsentType::AbsentType.find_by_name(a.leave_type.long_name)
        if at.nil?
          # doesn't yet exist, create one
          ConvertAbsentType::AbsentType.create(:name => a.leave_type.long_name, :created_by => a.created_by, :updated_by => a.updated_by)
        end
        # add string to absent
        a.update_attribute(:absent_type, a.leave_type.long_name)
      end
    end
    
    remove_column :absents, :leave_type_id
    
    # update and remove remaining absent type options
    options = Option.find_by_option_type("Absent Type")
    options.each do |o|
      at = ConvertAbsentType::AbsentType.find_by_name(o.long_name)
      if at.nil?
        # doesn't exist, create one
        ConvertAbsentType::AbsentType.create(:name => o.long_name, :created_by => o.created_by, :updated_by => o.updated_by)
      end
      o.destroy
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
