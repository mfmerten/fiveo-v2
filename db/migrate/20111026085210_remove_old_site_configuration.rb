class RemoveOldSiteConfiguration < ActiveRecord::Migration
  def up
    if File.exists?("public/system/site_configuration.yml")
      `mv public/system/site_configuration.yml tmp/`
      announce("Old config file (site_configuration.yml) moved from public/system/ to tmp/ (just in case)")
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
