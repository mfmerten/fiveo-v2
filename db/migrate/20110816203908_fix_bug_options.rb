class FixBugOptions < ActiveRecord::Migration
  class Bug < ActiveRecord::Base
  end
  class Option < ActiveRecord::Base
    belongs_to :option_type, :class_name => 'FixBugOptions::OptionType'
    def self.get_option_type(option_type)
      ot = FixBugOptions::OptionType.find_by_name(option_type)
      if ot.nil?
        nil
      else
        ot.id
      end
    end
  end
  class OptionType < ActiveRecord::Base
    has_many :options, :class_name => 'FixBugOptions::Option', :dependent => :destroy
  end
  
  def self.up
    unless column_exists?(:bugs, :priority)
      add_column :bugs, :priority, :integer
      add_column :bugs, :status, :integer
      add_column :bugs, :resolution, :integer
    
      FixBugOptions::Bug.reset_column_information
    
      FixBugOptions::Bug.all.each do |b|
        if o = FixBugOptions::Option.find_by_id(b.status_id)
          case o.long_name
          when "Hold"
            b.status = 1
          when "New"
            b.status = 2
          when "Reopened"
            b.status = 3
          when "Resolved"
            b.status = 4
          when "Working On"
            b.status = 5
          else
            b.status = 0
          end
        end
        if o = FixBugOptions::Option.find_by_id(b.priority_id)
          b.priority = o.long_name.slice(0,1).to_i
        end
        if o = FixBugOptions::Option.find_by_id(b.resolution_id)
          case o.long_name
          when "Answered"
            b.resolution = 1
          when "Duplicate Bug"
            b.resolution = 2
          when "Fixed"
            b.resolution = 3
          when "Not Satisfied"
            b.resolution = 4
          else
            b.resolution = 0
          end
        end
        b.save
      end
      remove_column :bugs, :priority_id
      remove_column :bugs, :status_id
      remove_column :bugs, :resolution_id
    end
    
    if table_exists?(:option_types)
      # remove these from options
      if ot = FixBugOptions::OptionType.find_by_name("Bug Status")
        ot.destroy
      end
      if ot = FixBugOptions::OptionType.find_by_name("Bug Priority")
        ot.destroy
      end
      if ot = FixBugOptions::OptionType.find_by_name("Bug Resolution")
        ot.destroy
      end
    else
      FixBugOptions::Option.where(:option_type => FixBugOptions::Option.get_option_type('Bug Status')).all.each{|o| o.destroy}
      FixBugOptions::Option.where(:option_type => FixBugOptions::Option.get_option_type('Bug Priority')).all.each{|o| o.destroy}
      FixBugOptions::Option.where(:option_type => FixBugOptions::Option.get_option_type('Bug Resolution')).all.each{|o| o.destroy}
    end
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
