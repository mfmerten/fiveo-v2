class FixOffenseJoins < ActiveRecord::Migration
  class Offense < ActiveRecord::Base
    has_many :evidences, :class_name => 'FixOffenseJoins::Evidence'
    has_many :offense_evidences, :class_name => 'FixOffenseJoins::OffenseEvidence'
    has_many :xevidences, :through => :offense_evidences, :source => :evidence
    has_many :arrests, :class_name => 'FixOffenseJoins::Arrest'
    has_many :offense_arrests, :class_name => 'FixOffenseJoins::OffenseArrest'
    has_many :xarrests, :through => :offense_arrests, :source => :arrest
    has_and_belongs_to_many :victims, :class_name => 'FixOffenseJoins::Person', :join_table => 'victims'
    has_and_belongs_to_many :witnesses, :class_name => 'FixOffenseJoins::Person', :join_table => 'witnesses'
    has_and_belongs_to_many :suspects, :class_name => 'FixOffenseJoins::Person', :join_table => 'suspects'
    has_many :offense_victims, :class_name => 'FixOffenseJoins::OffenseVictim'
    has_many :xvictims, :through => :offense_victims, :source => :person
    has_many :offense_witnesses, :class_name => 'FixOffenseJoins::OffenseWitness'
    has_many :xwitnesses, :through => :offense_witnesses, :source => :person
    has_many :offense_suspects, :class_name => 'FixOffenseJoins::OffenseSuspect'
    has_many :xsuspects, :through => :offense_suspects, :source => :person
    has_many :citations, :class_name => 'FixOffenseJoins::Citation'
    has_many :offense_citations, :class_name => 'FixOffenseJoins::OffenseCitation'
    has_many :xcitations, :through => :offense_citations, :source => :citation
  end
  
  class Arrest < ActiveRecord::Base
    belongs_to :offense, :class_name => 'FixOffenseJoins::Offense'
    has_many :offense_arrests, :class_name => 'FixOffenseJoins::OffenseArrest'
    has_many :offenses, :through => :offense_arrests
  end
  
  class Citation < ActiveRecord::Base
    belongs_to :offense, :class_name => 'FixOffenseJoins::Offense'
    has_many :offense_citations, :class_name => 'FixOffenseJoins::OffenseCitation'
    has_many :offenses, :through => :offense_citations
  end
  
  class Evidence < ActiveRecord::Base
    belongs_to :offense, :class_name => 'FixOffenseJoins::Offense'
    has_many :offense_evidences, :class_name => 'FixOffenseJoins::OffenseEvidence'
    has_many :offenses, :through => :offense_evidences
  end
  
  class Person < ActiveRecord::Base
    has_and_belongs_to_many :offenses_as_victim, :class_name => 'FixOffenseJoins::Offense', :join_table => 'victims'
    has_and_belongs_to_many :offenses_as_witness, :class_name => 'FixOffenseJoins::Offense', :join_table => 'witnesses'
    has_and_belongs_to_many :offenses_as_suspect, :class_name => 'FixOffenseJoins::Offense', :join_table => 'suspects'
    has_many :offense_victims, :class_name => 'FixOffenseJoins::OffenseVictim'
    has_many :victim_offenses, :through => :offense_victims, :source => :offense
    has_many :offense_witnesses, :class_name => 'FixOffenseJoins::OffenseWitness'
    has_many :witness_offenses, :through => :offense_witnesses, :source => :offense
    has_many :offense_suspects, :class_name => 'FixOffenseJoins::OffenseSuspect'
    has_many :suspect_offenses, :through => :offense_suspects, :source => :offense
  end
  
  class OffenseArrest < ActiveRecord::Base
    belongs_to :offense, :class_name => 'FixOffenseJoins::Offense'
    belongs_to :arrest, :class_name => 'FixOffenseJoins::Arrest'
  end
  
  class OffenseCitation < ActiveRecord::Base
    belongs_to :offense, :class_name => 'FixOffenseJoins::Offense'
    belongs_to :citation, :class_name => 'FixOffenseJoins::Citation'
  end
  
  class OffenseEvidence < ActiveRecord::Base
    belongs_to :offense, :class_name => 'FixOffenseJoins::Offense'
    belongs_to :evidence, :class_name => 'FixOffenseJoins::Evidence'
  end
  
  class OffenseSuspect < ActiveRecord::Base
    belongs_to :offense, :class_name => 'FixOffenseJoins::Offense'
    belongs_to :person, :class_name => 'FixOffenseJoins::Person'
  end
  
  class OffenseVictim < ActiveRecord::Base
    belongs_to :offense, :class_name => 'FixOffenseJoins::Offense'
    belongs_to :person, :class_name => 'FixOffenseJoins::Person'
  end
  
  class OffenseWitness < ActiveRecord::Base
    belongs_to :offense, :class_name => 'FixOffenseJoins::Offense'
    belongs_to :person, :class_name => 'FixOffenseJoins::Person'
  end
  
  def up
    create_table :offense_evidences, :force => true do |t|
      t.integer :offense_id
      t.integer :evidence_id
      t.timestamps
    end
    add_index :offense_evidences, :offense_id
    add_index :offense_evidences, :evidence_id
    
    create_table :offense_arrests, :force => true do |t|
      t.integer :offense_id
      t.integer :arrest_id
      t.timestamps
    end
    add_index :offense_arrests, :offense_id
    add_index :offense_arrests, :arrest_id
    
    create_table :offense_suspects, :force => true do |t|
      t.integer :offense_id
      t.integer :person_id
      t.timestamps
    end
    add_index :offense_suspects, :offense_id
    add_index :offense_suspects, :person_id
    
    create_table :offense_victims, :force => true do |t|
      t.integer :offense_id
      t.integer :person_id
      t.timestamps
    end
    add_index :offense_victims, :offense_id
    add_index :offense_victims, :person_id
    
    create_table :offense_witnesses, :force => true do |t|
      t.integer :offense_id
      t.integer :person_id
      t.timestamps
    end
    add_index :offense_witnesses, :offense_id
    add_index :offense_witnesses, :person_id
    
    create_table :offense_citations, :force => true do |t|
      t.integer :offense_id
      t.integer :citation_id
      t.timestamps
    end
    add_index :offense_citations, :offense_id
    add_index :offense_citations, :citation_id
    
    FixOffenseJoins::OffenseEvidence.reset_column_information
    FixOffenseJoins::OffenseArrest.reset_column_information
    FixOffenseJoins::OffenseSuspect.reset_column_information
    FixOffenseJoins::OffenseVictim.reset_column_information
    FixOffenseJoins::OffenseWitness.reset_column_information
    FixOffenseJoins::OffenseCitation.reset_column_information
    
    FixOffenseJoins::Offense.all.each do |o|
      o.xevidence_ids = o.evidence_ids
      o.xarrest_ids = o.arrest_ids
      o.xvictim_ids = o.victim_ids
      o.xwitness_ids = o.witness_ids
      o.xsuspect_ids = o.suspect_ids
      o.xcitation_ids = o.citation_ids
    end
    
    remove_column :evidences, :offense_id
    remove_column :arrests, :offense_id
    drop_table :victims
    drop_table :witnesses
    drop_table :suspects
    remove_column :citations, :offense_id
    
    # piggyback for jail_cells
    remove_column :jail_cells, :created_by
    remove_column :jail_cells, :updated_by
  end

  def down
    add_column :evidences, :offense_id, :integer
    add_index :evidences, :offense_id
    add_column :arrests, :offense_id, :integer
    add_index :arrests, :offense_id
    
    create_table :victims, :id => false, :force => true do |t|
      t.integer :person_id
      t.integer :offense_id
    end
    add_index :victims, :person_id
    add_index :victims, :offense_id
    
    create_table :witnesses, :id => false, :force => true do |t|
      t.integer :person_id
      t.integer :offense_id
    end
    add_index :witnesses, :person_id
    add_index :witnesses, :offense_id
    
    create_table :suspects, :id => false, :force => true do |t|
      t.integer :person_id
      t.integer :offense_id
    end
    add_index :suspects, :person_id
    add_index :suspects, :offense_id
    
    add_column :citations, :offense_id, :integer
    add_index :citations, :offense_id
    
    FixOffenseJoins::Evidence.reset_column_information
    FixOffenseJoins::Arrest.reset_column_information
    FixOffenseJoins::Citation.reset_column_information
    
    FixOffenseJoins::Offense.all.each do |o|
      o.evidence_ids = o.xevidence_ids
      o.arrest_ids = o.xarrest_ids
      o.suspect_ids = o.xsuspect_ids
      o.victim_ids = o.xvictim_ids
      o.witness_ids = o.xwitness_ids
      o.citation_ids = o.xcitation_ids
    end
    
    drop_table :offense_evidences
    drop_table :offense_arrests
    drop_table :offense_suspects
    drop_table :offense_witnesses
    drop_table :offense_victims
    drop_table :offense_citations
    
    add_column :jail_cells, :created_by, :integer, :default => 1
    add_column :jail_cells, :updated_by, :integer, :default => 1
    add_index :jail_cells, :created_by
    add_index :jail_cells, :updated_by
  end
end
