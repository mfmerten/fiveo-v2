class AbsentsIndexes < ActiveRecord::Migration
  def up
    add_index :absents, :return_datetime
  end

  def down
    remove_index :absents, :return_datetime
  end
end
