class UpdateSiteDefaults < ActiveRecord::Migration
  class SiteConfiguration < ActiveRecord::Base
  end
  def up
    change_column :site_configurations, :ceo_title, :string, :default => "Midlake Technical Services"
    change_column :site_configurations, :header_subtitle1, :string, :default => "Midlake Technical Services"
    change_column :site_configurations, :header_subtitle3, :string, :default => "http://midlaketech.com - mikemerten@suddenlink.net"
    UpdateSiteDefaults::SiteConfiguration.reset_column_information
    s = UpdateSiteDefaults::SiteConfiguration.find_by_id(1)
    unless s.nil?
      s.ceo_title = "Midlake Technical Services"
      s.header_subtitle1 = "Midlake Technical Services"
      s.header_subtitle2 = "http://midlaketech.com - mikemerten@suddenlink.net"
      s.save
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
