class UpdateOptions < ActiveRecord::Migration
  class Option < ActiveRecord::Base
    belongs_to :original_option_type, :class_name => 'UpdateOptions::OptionType', :foreign_key => 'option_type_id'
    
    OptionTypes = {
      1 => 'Absent Type',
      2 => 'Anatomy',
      3 => 'Associate Type',
      4 => 'Bond Status',
      5 => 'Bond Type',
      # 6 => 'Bug Priority' -- obsolete, now defined in Bug model
      # 7 => 'Bug Resolution' -- obsolete, now defined in Bug model
      # 8 => 'Bug Status' -- obsolete, now defined in Bug model
      9 => 'Build',
      10 => 'Call Disposition',
      11 => 'Call Via',
      12 => 'Cell',
      13 => 'Commissary Type',
      14 => 'Complexion',
      15 => 'Consecutive Type',
      16 => 'Court Costs',
      17 => 'Court Type',
      18 => 'Docket Type',
      19 => 'Eye Color',
      20 => 'Facial Hair',
      21 => 'Facility',
      22 => 'Hair Color',
      23 => 'Hair Type',
      24 => 'History Type',
      25 => 'Hold Type',
      26 => 'Investigation Status',
      # 27 => 'Jail1 Cell',  Obsolete
      # 28 => 'Jail2 Cell',  Obsolete
      # 29 => 'Jail3 Cell',  Obsolete
      30 => 'Judge',
      31 => 'Noservice Type',
      32 => 'Pawn Company',
      33 => 'Pawn Ticket Type',
      34 => 'Pawn Type',
      35 => 'Payment Type',
      36 => 'Phone Type',
      37 => 'Plea Type',
      38 => 'Probation Status',
      39 => 'Probation Type',
      40 => 'Race',
      41 => 'Release Reason',
      42 => 'Release Type',
      43 => 'Service Type',
      44 => 'Signal Code',
      # 45 => 'State' -- obsolete, now an application_constant class
      46 => 'Subject Type',
      47 => 'Temporary Release Reason',
      48 => 'Transfer Type',
      49 => 'Trial Result',
      50 => 'Vehicle Type',
      51 => 'Warrant Disposition'
    }.freeze
  
    def self.get_option_type(value)
      UpdateOptions::Option::OptionTypes.merge(UpdateOptions::Option::OptionTypes.invert)[value]
    end
  end
  class OptionType < ActiveRecord::Base
    has_many :options, :class_name => 'UpdateOptions::Option'
  end
  class Bondsman < ActiveRecord::Base
    belongs_to :original_state, :class_name => 'UpdateOptions::Option', :foreign_key => 'state_id'
  end
  class Contact < ActiveRecord::Base
    belongs_to :original_state, :class_name => 'UpdateOptions::Option', :foreign_key => 'state_id'
  end
  class InvestVehicle < ActiveRecord::Base
    belongs_to :original_state, :class_name => 'UpdateOptions::Option', :foreign_key => 'license_state_id'
  end
  class Paper < ActiveRecord::Base
    belongs_to :original_state, :class_name => 'UpdateOptions::Option', :foreign_key => 'state_id'
  end
  class PaperCustomer < ActiveRecord::Base
    belongs_to :original_state, :class_name => 'UpdateOptions::Option', :foreign_key => 'state_id'
  end
  class Pawn < ActiveRecord::Base
    belongs_to :original_state, :class_name => 'UpdateOptions::Option', :foreign_key => 'oln_state_id'
  end
  class Person < ActiveRecord::Base
    belongs_to :original_phys_state, :class_name => 'UpdateOptions::Option', :foreign_key => 'phys_state_id'
    belongs_to :original_mail_state, :class_name => 'UpdateOptions::Option', :foreign_key => 'mail_state_id'
    belongs_to :original_oln_state, :class_name => 'UpdateOptions::Option', :foreign_key => 'oln_state_id'
  end
  class Transport < ActiveRecord::Base
    belongs_to :original_from_state, :class_name => 'UpdateOptions::Option', :foreign_key => 'from_state_id'
    belongs_to :original_to_state, :class_name => 'UpdateOptions::Option', :foreign_key => 'to_state_id'
  end
  class Visitor < ActiveRecord::Base
    belongs_to :original_state, :class_name => 'UpdateOptions::Option', :foreign_key => 'state_id'
  end
  class Warrant < ActiveRecord::Base
    belongs_to :original_state, :class_name => 'UpdateOptions::Option', :foreign_key => 'state_id'
    belongs_to :original_oln_state, :class_name => 'UpdateOptions::Option', :foreign_key => 'oln_state_id'
  end
  
  def up
    if table_exists?(:option_types)
      # fix statess first
      if column_exists?(:bondsmen, :state_id)
        add_column :bondsmen, :state, :string
        UpdateOptions::Bondsman.reset_column_information
        UpdateOptions::Bondsman.all.each do |record|
          unless record.original_state.nil?
            record.update_attribute(:state, record.original_state.short_name)
          end
        end
        remove_column :bondsmen, :state_id
      end
      
      if column_exists?(:contacts, :state_id)
        add_column :contacts, :state, :string
        UpdateOptions::Contact.reset_column_information
        UpdateOptions::Contact.all.each do |record|
          unless record.original_state.nil?
            record.update_attribute(:state, record.original_state.short_name)
          end
        end
        remove_column :contacts, :state_id
      end
      
      if column_exists?(:invest_vehicles, :license_state_id)
        add_column :invest_vehicles, :license_state, :string
        UpdateOptions::InvestVehicle.reset_column_information
        UpdateOptions::InvestVehicle.all.each do |record|
          unless record.original_state.nil?
            record.update_attribute(:license_state, record.original_state.short_name)
          end
        end
        remove_column :invest_vehicles, :license_state_id
      end
      
      if column_exists?(:papers, :state_id)
        add_column :papers, :state, :string
        UpdateOptions::Paper.reset_column_information
        UpdateOptions::Paper.all.each do |record|
          unless record.original_state.nil?
            record.update_attribute(:state, record.original_state.short_name)
          end
        end
        remove_column :papers, :state_id
      end
      
      if column_exists?(:paper_customers, :state_id)
        add_column :paper_customers, :state, :string
        UpdateOptions::PaperCustomer.reset_column_information
        UpdateOptions::PaperCustomer.all.each do |record|
          unless record.original_state.nil?
            record.update_attribute(:state, record.original_state.short_name)
          end
        end
        remove_column :paper_customers, :state_id
      end
      
      if column_exists?(:pawns, :oln_state_id)
        add_column :pawns, :oln_state, :string
        UpdateOptions::Pawn.reset_column_information
        UpdateOptions::Pawn.all.each do |record|
          unless record.original_state.nil?
            record.update_attribute(:oln_state, record.original_state.short_name)
          end
        end
        remove_column :pawns, :oln_state_id
      end
      
      if column_exists?(:people, :phys_state_id)
        add_column :people, :phys_state, :string
        add_column :people, :mail_state, :string
        add_column :people, :oln_state, :string
        UpdateOptions::Person.reset_column_information
        UpdateOptions::Person.all.each do |record|
          unless record.original_phys_state.nil?
            record.phys_state = record.original_phys_state.short_name
          end
          unless record.original_mail_state.nil?
            record.mail_state = record.original_mail_state.short_name
          end
          unless record.original_oln_state.nil?
            record.oln_state = record.original_oln_state.short_name
          end
          record.save
        end
        remove_column :people, :phys_state_id
        remove_column :people, :mail_state_id
        remove_column :people, :oln_state_id
      end
      
      if column_exists?(:transports, :from_state_id)
        add_column :transports, :from_state, :string
        add_column :transports, :to_state, :string
        UpdateOptions::Transport.reset_column_information
        UpdateOptions::Transport.all.each do |record|
          unless record.original_from_state.nil?
            record.from_state = record.original_from_state.short_name
          end
          unless record.original_to_state.nil?
            record.to_state = record.original_to_state.short_name
          end
          record.save
        end
        remove_column :transports, :from_state_id
        remove_column :transports, :to_state_id
      end
      
      if column_exists?(:visitors, :state_id)
        add_column :visitors, :state, :string
        UpdateOptions::Visitor.reset_column_information
        UpdateOptions::Visitor.all.each do |record|
          unless record.original_state.nil?
            record.update_attribute(:state, record.original_state.short_name)
          end
        end
        remove_column :visitors, :state_id
      end
      
      if column_exists?(:warrants, :state_id)
        add_column :warrants, :state, :string
        add_column :warrants, :oln_state, :string
        UpdateOptions::Warrant.reset_column_information
        UpdateOptions::Warrant.all.each do |record|
          unless record.original_state.nil?
            record.state = record.original_state.short_name
          end
          unless record.original_oln_state.nil?
            record.oln_state = record.original_oln_state.short_name
          end
          record.save
        end
        remove_column :warrants, :state_id
        remove_column :warrants, :oln_state_id
      end
      
      # now fix option types
      if column_exists?(:options, :option_type_id)
        add_column :options, :option_type, :integer
        UpdateOptions::Option.reset_column_information
        UpdateOptions::Option.all.each do |o|
          unless o.original_option_type.nil?
            o.update_attribute(:option_type, UpdateOptions::Option.get_option_type(o.original_option_type.name))
          end
        end
        remove_column :options, :option_type_id
      end
      
      drop_table :option_types
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
