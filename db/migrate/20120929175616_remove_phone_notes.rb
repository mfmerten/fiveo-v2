class RemovePhoneNotes < ActiveRecord::Migration
  def up
    remove_column :contacts, :phone1_note
    remove_column :contacts, :phone2_note
    remove_column :contacts, :phone3_note
    remove_column :invest_contacts, :phone1_note
    remove_column :invest_contacts, :phone2_note
    remove_column :invest_contacts, :phone3_note
    remove_column :warrants, :phone1_note
    remove_column :warrants, :phone2_note
    remove_column :warrants, :phone3_note
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
