class ConvertRemainingOptions < ActiveRecord::Migration
  class Option < ActiveRecord::Base
    OptionTypes = {
      33 => 'Pawn Ticket Type',
      38 => 'Probation Status',
      39 => 'Probation Type',
      41 => 'Release Reason',
      46 => 'Subject Type',
      48 => 'Transfer Type',
      49 => 'Trial Result',
      50 => 'Vehicle Type',
      51 => 'Warrant Disposition'
    }.freeze
  end
  class Pawn < ActiveRecord::Base
    belongs_to :old_ticket_type, :class_name => "ConvertRemainingOptions::Option", :foreign_key => "ticket_type_id"
  end
  class Probation < ActiveRecord::Base
    belongs_to :old_status, :class_name => 'ConvertRemainingOptions::Option', :foreign_key => 'status_id'
  end
  class Sentence < ActiveRecord::Base
    belongs_to :old_probation_type, :class_name => 'ConvertRemainingOptions::Option', :foreign_key => 'probation_type_id'
    belongs_to :old_result, :class_name => 'ConvertRemainingOptions::Option', :foreign_key => 'result_id'
  end
  class Booking < ActiveRecord::Base
    belongs_to :old_released_because, :class_name => 'ConvertRemainingOptions::Option', :foreign_key => 'released_because_id'
  end
  class CallSubject < ActiveRecord::Base
    belongs_to :old_subject_type, :class_name => 'ConvertRemainingOptions::Option', :foreign_key => 'subject_type_id'
  end
  class Transfer < ActiveRecord::Base
    belongs_to :old_transfer_type, :class_name => 'ConvertRemainingOptions::Option', :foreign_key => 'transfer_type_id'
  end
  class InvestVehicle < ActiveRecord::Base
    belongs_to :old_vehicle_type, :class_name => 'ConvertRemainingOptions::Option', :foreign_key => 'vehicle_type_id'
  end
  class Warrant < ActiveRecord::Base
    belongs_to :old_disposition, :class_name => "ConvertRemainingOptions::Option", :foreign_key => 'disposition_id'
  end
  
  def up
    add_column :pawns, :ticket_type, :string
    add_column :probations, :status, :string
    add_column :sentences, :probation_type, :string
    add_column :sentences, :result, :string
    add_column :bookings, :released_because, :string
    add_column :call_subjects, :subject_type, :string
    add_column :transfers, :transfer_type, :string
    add_column :invest_vehicles, :vehicle_type, :string
    add_column :warrants, :disposition, :string
    
    ConvertRemainingOptions::Pawn.reset_column_information
    ConvertRemainingOptions::Probation.reset_column_information
    ConvertRemainingOptions::Sentence.reset_column_information
    ConvertRemainingOptions::Booking.reset_column_information
    ConvertRemainingOptions::CallSubject.reset_column_information
    ConvertRemainingOptions::Transfer.reset_column_information
    ConvertRemainingOptions::InvestVehicle.reset_column_information
    ConvertRemainingOptions::Warrant.reset_column_information
    
    ConvertRemainingOptions::Pawn.all.each do |p|
      unless p.old_ticket_type.nil?
        p.update_attribute(:ticket_type, p.old_ticket_type.long_name)
      end
    end
    
    ConvertRemainingOptions::Probation.all.each do |p|
      unless p.old_status.nil?
        p.update_attribute(:status, p.old_status.long_name)
      end
    end
    
    ConvertRemainingOptions::Sentence.all.each do |s|
      unless s.old_probation_type.nil?
        s.probation_type = s.old_probation_type.long_name
      end
      unless s.old_result.nil?
        s.result = s.old_result.long_name
      end
      if s.changed?
        s.save
      end
    end
    
    ConvertRemainingOptions::Booking.all.each do |b|
      unless b.old_released_because.nil?
        b.update_attribute(:released_because, b.old_released_because.long_name)
      end
    end
    
    ConvertRemainingOptions::CallSubject.all.each do |s|
      unless s.old_subject_type.nil?
        s.update_attribute(:subject_type, s.old_subject_type.long_name)
      end
    end
    
    ConvertRemainingOptions::Transfer.all.each do |t|
      unless t.old_transfer_type.nil?
        t.update_attribute(:transfer_type, t.old_transfer_type.long_name)
      end
    end
    
    ConvertRemainingOptions::InvestVehicle.all.each do |v|
      unless v.old_vehicle_type.nil?
        v.update_attribute(:vehicle_type, v.old_vehicle_type.long_name)
      end
    end
    
    ConvertRemainingOptions::Warrant.all.each do |w|
      unless w.old_disposition.nil?
        w.update_attribute(:disposition, w.old_disposition.long_name)
      end
    end
    
    remove_column :pawns, :ticket_type_id
    remove_column :probations, :status_id
    remove_column :sentences, :probation_type_id
    remove_column :sentences, :result_id
    remove_column :bookings, :released_because_id
    remove_column :call_subjects, :subject_type_id
    remove_column :transfers, :transfer_type_id
    remove_column :invest_vehicles, :vehicle_type_id
    remove_column :warrants, :disposition_id
    
    # end of options so I can just drop table
    drop_table :options
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
