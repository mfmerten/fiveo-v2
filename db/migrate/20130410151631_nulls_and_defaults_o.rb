class NullsAndDefaultsO < ActiveRecord::Migration
  class Offense < ActiveRecord::Base
  end
  class OffenseArrest < ActiveRecord::Base
  end
  class OffenseCitation < ActiveRecord::Base
  end
  class OffenseEvidence < ActiveRecord::Base
  end
  class OffenseForbid < ActiveRecord::Base
  end
  class OffensePhoto < ActiveRecord::Base
  end
  class OffenseSuspect < ActiveRecord::Base
  end
  class OffenseVictim < ActiveRecord::Base
  end
  class OffenseWitness < ActiveRecord::Base
  end
  class OffenseWrecker < ActiveRecord::Base
  end
  
  def up
    [:location, :officer_unit, :officer_badge, :officer].each do |col|
      NullsAndDefaultsO::Offense.update_all("#{col} = ''","#{col} IS NULL")
      change_column :offenses, col, :string, :null => false, :default => ''
    end
    [:remarks, :details, :other_documents].each do |col|
      NullsAndDefaultsO::Offense.update_all("#{col} = ''","#{col} IS NULL")
      change_column :offenses, col, :text, :null => false, :default => ''
    end
    [:legacy, :pictures, :restitution_form, :victim_notification, :perm_to_search,
      :misd_summons, :fortyeight_hour, :medical_release].each do |col|
      NullsAndDefaultsO::Offense.update_all("#{col} = FALSE","#{col} IS UNKNOWN")
      change_column_null :offenses, col, false
    end

    NullsAndDefaultsO::OffenseArrest.delete_all("offense_id IS NULL OR arrest_id IS NULL")
    change_column_null :offense_arrests, :offense_id, false
    change_column_null :offense_arrests, :arrest_id, false
    
    NullsAndDefaultsO::OffenseCitation.delete_all("offense_id IS NULL OR citation_id IS NULL")
    change_column_null :offense_citations, :offense_id, false
    change_column_null :offense_citations, :citation_id, false
    
    NullsAndDefaultsO::OffenseEvidence.delete_all("offense_id IS NULL OR evidence_id IS NULL")
    change_column_null :offense_evidences, :offense_id, false
    change_column_null :offense_evidences, :evidence_id, false
  
    NullsAndDefaultsO::OffenseForbid.delete_all("offense_id IS NULL OR forbid_id IS NULL")
    change_column_null :offense_forbids, :offense_id, false
    change_column_null :offense_forbids, :forbid_id, false
  
    NullsAndDefaultsO::OffensePhoto.delete_all("offense_id IS NULL")
    change_column_null :offense_photos, :offense_id, false
    [:case_no, :description, :location_taken, :taken_by, :taken_by_unit, :taken_by_badge].each do |col|
      NullsAndDefaultsO::OffensePhoto.update_all("#{col} = ''","#{col} IS NULL")
      change_column :offense_photos, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsO::OffensePhoto.update_all("remarks = ''","remarks IS NULL")
    change_column :offense_photos, :remarks, :text, :null => false, :default => ''
  
    NullsAndDefaultsO::OffenseSuspect.delete_all("offense_id IS NULL OR person_id IS NULL")
    change_column_null :offense_suspects, :offense_id, false
    change_column_null :offense_suspects, :person_id, false
  
    NullsAndDefaultsO::OffenseVictim.delete_all("offense_id IS NULL OR person_id IS NULL")
    change_column_null :offense_victims, :offense_id, false
    change_column_null :offense_victims, :person_id, false
  
    NullsAndDefaultsO::OffenseWitness.delete_all("offense_id IS NULL OR person_id IS NULL")
    change_column_null :offense_witnesses, :offense_id, false
    change_column_null :offense_witnesses, :person_id, false
    
    NullsAndDefaultsO::OffenseWrecker.delete_all("offense_id IS NULL")
    change_column_null :offense_wreckers, :offense_id, false
    [:name, :location, :vin, :tag].each do |col|
      NullsAndDefaultsO::OffenseWrecker.update_all("#{col} = ''","#{col} IS NULL")
      change_column :offense_wreckers, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsO::OffenseWrecker.update_all("by_request = FALSE","by_request IS NULL")
    change_column_null :offense_wreckers, :by_request, false
  end

  def down
  end
end
