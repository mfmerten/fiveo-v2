class UpdatePasswordEncryption < ActiveRecord::Migration
  class User < ActiveRecord::Base
  end
  
  def up
    rename_column :users, :hashed_password, :old_hashed_password
    add_column :users, :hashed_password, :string, :default => "", :null => false
    
    User.reset_column_information
    
    # expire all passwords
    exp_date = Date.yesterday
    User.update_all :expires => exp_date
    
  end

  def down
    remove_column :users, :hashed_password
    rename_column :users, :old_hashed_password, :hashed_password
  end
end
