class PrepareEncryptWarrantPawnSsn < ActiveRecord::Migration
  def change
    rename_column :warrants, :ssn, :old_ssn
    add_column :warrants, :encrypted_ssn, :string, default: ''
    
    rename_column :pawns, :ssn, :old_ssn
    add_column :pawns, :encrypted_ssn, :string, default: ''
  end
end
