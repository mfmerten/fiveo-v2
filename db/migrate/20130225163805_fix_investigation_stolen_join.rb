class FixInvestigationStolenJoin < ActiveRecord::Migration
  class Investigation < ActiveRecord::Base
    has_many :stolens, :class_name => 'FixInvestigationStolenJoin::Stolen', :dependent => :nullify
    has_many :invest_stolens, :class_name => 'FixInvestigationStolenJoin::InvestStolen', :dependent => :destroy
    has_many :xstolens, :through => :invest_stolens, :source => :stolen
  end
  
  class InvestCrime < ActiveRecord::Base
    has_many :stolens, :class_name => 'FixInvestigationStolenJoin::Stolen', :dependent => :nullify
    has_many :invest_stolens, :class_name => 'FixInvestigationStolenJoin::InvestStolen', :dependent => :nullify
    has_many :xstolens, :through => :invest_stolens, :source => :stolen
  end
  
  class InvestCriminal < ActiveRecord::Base
    has_many :stolens, :class_name => 'FixInvestigationStolenJoin::Stolen', :dependent => :nullify
    has_many :invest_stolens, :class_name => 'FixInvestigationStolenJoin::InvestStolen', :dependent => :nullify
    has_many :xstolens, :through => :invest_stolens, :source => :stolen
  end
  
  class Stolen < ActiveRecord::Base
    belongs_to :investigation, :class_name => 'FixInvestigationStolenJoin::Investigation'
    belongs_to :crime, :class_name => 'FixInvestigationStolenJoin::InvestCrime', :foreign_key => 'invest_crime_id'
    belongs_to :criminal, :class_name => 'FixInvestigationStolenJoin::InvestCriminal', :foreign_key => 'invest_criminal_id'
    has_many :invest_stolens, :class_name => 'FixInvestigationStolenJoin::Stolen', :dependent => :destroy
    has_many :investigations, :through => :invest_stolens
    has_many :crimes, :through => :invest_stolens, :source => :invest_crime
    has_many :criminals, :through => :invest_stolens, :source => :invest_criminals
  end
  
  class InvestStolen < ActiveRecord::Base
    belongs_to :investigation, :class_name => 'FixInvestigationStolenJoin::Investigation'
    belongs_to :stolen, :class_name => 'FixInvestigationStolenJoin::Stolen'
    belongs_to :invest_crime, :class_name => 'FixInvestigationStolenJoin::InvestCrime'
    belongs_to :invest_criminal, :class_name => 'FixInvestigationStolenJoin::InvestCriminal'
  end
  
  def up
    create_table :invest_stolens, :force => true do |t|
      t.integer :stolen_id
      t.integer :investigation_id
      t.integer :invest_crime_id
      t.integer :invest_criminal_id
      t.timestamps
    end
    add_index :invest_stolens, :stolen_id
    add_index :invest_stolens, :investigation_id
    add_index :invest_stolens, :invest_crime_id
    add_index :invest_stolens, :invest_criminal_id
    
    FixInvestigationStolenJoin::Investigation.reset_column_information
    FixInvestigationStolenJoin::Investigation.all.each do |i|
      i.stolens.each do |s|
        i.xstolens.create(:invest_crime_id => s.invest_crime_id, :invest_criminal_id => s.invest_criminal_id)
      end
    end
    
    remove_column :stolens, :investigation_id
    remove_column :stolens, :invest_crime_id
    remove_column :stolens, :invest_criminal_id
    
    # don't forget to qualify all class names!
  end

  def down
    add_column :stolens, :investigation_id, :integer
    add_column :stolens, :invest_crime_id, :integer
    add_column :stolens, :invest_criminal_id, :integer
    FixInvestigationStolenJoin::Stolen.reset_column_information
    FixInvestigationStolenJoin::Stolen.all.each do |e|
      next if e.investigations.empty?
      e.update_attribute(:investigation_id, e.investigations.first.investigation_id)
      e.update_attribute(:invest_crime_id, e.investigations.first.invest_crime_id)
      e.update_attribute(:invest_criminal_id, e.investigations.first.invest_criminal_id)
    end
    drop_table :invest_stolens
  end
end
