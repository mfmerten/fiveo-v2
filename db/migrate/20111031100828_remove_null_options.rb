# added to remove old state options
class RemoveNullOptions < ActiveRecord::Migration
  class Option < ActiveRecord::Base
  end
  
  def up
    RemoveNullOptions::Option.delete_all('option_type IS NULL')
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
