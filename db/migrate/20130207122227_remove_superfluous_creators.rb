class RemoveSuperfluousCreators < ActiveRecord::Migration
  def up
    remove_column :associates, :created_by
    remove_column :associates, :updated_by
    rename_table :associates, :person_associates
    
    remove_column :call_subjects, :created_by
    remove_column :call_subjects, :updated_by
    
    remove_column :call_units, :created_by
    remove_column :call_units, :updated_by
    
    remove_column :evidence_locations, :created_by
    remove_column :evidence_locations, :updated_by
  
    remove_column :marks, :created_by
    remove_column :marks, :updated_by
    rename_table :marks, :person_marks
    
    rename_table :med_schedules, :medication_schedules
    
    remove_column :pawn_items, :created_by
    remove_column :pawn_items, :updated_by
    
    remove_column :properties, :created_by
    remove_column :properties, :updated_by
    rename_table :properties, :booking_properties
    
    remove_column :site_configurations, :created_by
    remove_column :site_configurations, :updated_by
    
    remove_column :support_ticket_comments, :created_by
    remove_column :support_ticket_comments, :updated_by
  
    remove_column :visitors, :created_by
    remove_column :visitors, :updated_by
    rename_table :visitors, :booking_visitors
  
    remove_column :wreckers, :created_by
    remove_column :wreckers, :updated_by
    rename_table :wreckers, :offense_wreckers
  end

  def down
    rename_table :person_assoicates, :associates
    add_column :associates, :created_by, :integer, :default => 1
    add_column :associates, :updated_by, :integer, :default => 1
  
    add_column :call_subjects, :created_by, :integer, :default => 1
    add_column :call_subjects, :updated_by, :integer, :default => 1
  
    add_column :call_units, :created_by, :integer, :default => 1
    add_column :call_units, :updated_by, :integer, :default => 1
    
    add_column :evidence_locations, :created_by, :integer, :default => 1
    add_column :evidence_locations, :updated_by, :integer, :default => 1
  
    rename_table :person_marks, :marks
    add_column :marks, :created_by, :integer, :default => 1
    add_column :marks, :updated_by, :integer, :default => 1
    
    rename_table :medication_schedules, :med_schedules
    
    add_column :pawn_items, :created_by, :integer, :default => 1
    add_column :pawn_items, :updated_by, :integer, :default => 1
  
    rename_table :booking_properties, :properties
    add_column :properties, :created_by, :integer, :default => 1
    add_column :properties, :updated_by, :integer, :default => 1
  
    add_column :site_configurations, :created_by, :integer, :default => 1
    add_column :site_configurations, :updated_by, :integer, :default => 1
    
    add_column :support_ticket_comments, :created_by, :integer, :default => 1
    add_column :support_ticket_comments, :updated_by, :integer, :default => 1
  
    rename_table :booking_visitors, :visitors
    add_column :properties, :created_by, :integer, :default => 1
    add_column :properties, :updated_by, :integer, :default => 1
  
    rename_table :offense_wreckers, :wreckers
    add_column :wreckers, :created_by, :integer, :default => 1
    add_column :wreckers, :updated_by, :integer, :default => 1
  end
end
