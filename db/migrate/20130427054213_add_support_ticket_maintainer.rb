class AddSupportTicketMaintainer < ActiveRecord::Migration
  def up
    add_column :site_configurations, :support_ticket_maintainer_id, :integer, :null => false, :default => 1
    add_index :site_configurations, :support_ticket_maintainer_id
    add_foreign_key :site_configurations, :users, :column => 'support_ticket_maintainer_id'
  end

  def down
    remove_foreign_key :site_configurations, :column => 'support_ticket_maintainer_id'
    remove_index :site_configurations, :support_ticket_maintainer_id
    remove_column :site_configurations, :support_ticket_maintainer_id
  end
end
