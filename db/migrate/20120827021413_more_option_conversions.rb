class MoreOptionConversions < ActiveRecord::Migration
  class ApplicationConstants
    CallDisposition = {
      'Officer Report' => 'OFC', 'Referred To Detective' => 'DET',
      'Report Only' => 'RPT'
    }.freeze
  end
  class Option < ActiveRecord::Base
    OptionTypes = {
      5 => 'Bond Type',
      10 => 'Call Disposition',
      11 => 'Call Via',
      13 => 'Commissary Type',
      14 => 'Complexion',
      15 => 'Consecutive Type',
      16 => 'Court Costs',
      17 => 'Court Type',
      18 => 'Docket Type',
      25 => 'Hold Type',
      26 => 'Investigation Status',
      30 => 'Judge',
      31 => 'Noservice Type',
      32 => 'Pawn Company',
      33 => 'Pawn Ticket Type',
      34 => 'Pawn Type',
      35 => 'Payment Type',
      36 => 'Phone Type',
      37 => 'Plea Type',
      38 => 'Probation Status',
      39 => 'Probation Type',
      41 => 'Release Reason',
      42 => 'Release Type',
      43 => 'Service Type',
      44 => 'Signal Code',
      46 => 'Subject Type',
      47 => 'Temporary Release Reason',
      48 => 'Transfer Type',
      49 => 'Trial Result',
      50 => 'Vehicle Type',
      51 => 'Warrant Disposition'
    }.freeze
  end
  class Contact < ActiveRecord::Base
    belongs_to :old_phone1_type, :class_name => 'MoreOptionConversions::Option', :foreign_key => 'phone1_type_id'
    belongs_to :old_phone2_type, :class_name => 'MoreOptionConversions::Option', :foreign_key => 'phone2_type_id'
    belongs_to :old_phone3_type, :class_name => 'MoreOptionConversions::Option', :foreign_key => 'phone3_type_id'
  end
  class InvestContact < ActiveRecord::Base
    belongs_to :old_phone1_type, :class_name => 'MoreOptionConversions::Option', :foreign_key => 'phone1_type_id'
    belongs_to :old_phone2_type, :class_name => 'MoreOptionConversions::Option', :foreign_key => 'phone2_type_id'
    belongs_to :old_phone3_type, :class_name => 'MoreOptionConversions::Option', :foreign_key => 'phone3_type_id'
  end
  class Warrant < ActiveRecord::Base
    belongs_to :old_phone1_type, :class_name => 'MoreOptionConversions::Option', :foreign_key => 'phone1_type_id'
    belongs_to :old_phone2_type, :class_name => 'MoreOptionConversions::Option', :foreign_key => 'phone2_type_id'
    belongs_to :old_phone3_type, :class_name => 'MoreOptionConversions::Option', :foreign_key => 'phone3_type_id'
  end
  class Bond < ActiveRecord::Base
    belongs_to :old_bond_type, :class_name => 'MoreOptionConversions::Option', :foreign_key => 'bond_type_id'
  end
  class Call < ActiveRecord::Base
    belongs_to :old_received_via, :class_name => "MoreOptionConversions::Option", :foreign_key => 'received_via_id'
    belongs_to :old_disposition, :class_name => "MoreOptionConversions::Option", :foreign_key => 'disposition_id'
  end
  class Commissary < ActiveRecord::Base
    belongs_to :old_category, :class_name => 'MoreOptionConversions::Option', :foreign_key => 'category_id'
  end
  
  def up
    ### Payment Type
    # nothing to do here, just remove options - no longer used
    
    ### Phone Type
    # add new columns
    add_column :contacts, :phone1_type, :string
    add_column :contacts, :phone2_type, :string
    add_column :contacts, :phone3_type, :string
    add_column :invest_contacts, :phone1_type, :string
    add_column :invest_contacts, :phone2_type, :string
    add_column :invest_contacts, :phone3_type, :string
    add_column :warrants, :phone1_type, :string
    add_column :warrants, :phone2_type, :string
    add_column :warrants, :phone3_type, :string
    
    MoreOptionConversions::Contact.reset_column_information
    MoreOptionConversions::InvestContact.reset_column_information
    MoreOptionConversions::Warrant.reset_column_information
    
    # process options
    [MoreOptionConversions::Contact, MoreOptionConversions::InvestContact, MoreOptionConversions::Warrant].each do |model|
      model.all.each do |m|
        unless m.old_phone1_type.nil?
          m.phone1_type = m.old_phone1_type.long_name
        end
        unless m.old_phone2_type.nil?
          m.phone2_type = m.old_phone2_type.long_name
        end
        unless m.old_phone3_type.nil?
          m.phone3_type = m.old_phone3_type.long_name
        end
        if m.changed?
          m.save
        end
      end
    end
    
    # remove old columns
    remove_column :contacts, :phone1_type_id
    remove_column :contacts, :phone2_type_id
    remove_column :contacts, :phone3_type_id
    remove_column :invest_contacts, :phone1_type_id
    remove_column :invest_contacts, :phone2_type_id
    remove_column :invest_contacts, :phone3_type_id
    remove_column :warrants, :phone1_type_id
    remove_column :warrants, :phone2_type_id
    remove_column :warrants, :phone3_type_id
    
    ### Bond Type
    add_column :bonds, :bond_type, :string
    MoreOptionConversions::Bond.reset_column_information
    MoreOptionConversions::Bond.all.each do |b|
      unless b.old_bond_type.nil?
        b.update_attribute(:bond_type, b.old_bond_type.long_name)
      end
    end
    remove_column :bonds, :bond_type_id
    
    ### Call Via and Call Disposition
    add_column :calls, :received_via, :string
    add_column :calls, :disposition, :string
    MoreOptionConversions::Call.reset_column_information
    MoreOptionConversions::Call.all.each do |c|
      unless c.old_received_via.nil?
        c.received_via = c.old_received_via.long_name
      end
      unless c.old_disposition.nil?
        if MoreOptionConversions::ApplicationConstants::CallDisposition.keys.include?(c.old_disposition.long_name)
          c.disposition = c.old_disposition.long_name
        else
          c.disposition = 'Report Only'
        end
      end
      if c.changed?
        c.save
      end
    end
    remove_column :calls, :received_via_id
    remove_column :calls, :disposition_id
    
    ### Commissary Type
    add_column :commissaries, :category, :string
    MoreOptionConversions::Commissary.reset_column_information
    MoreOptionConversions::Commissary.all.each do |c|
      unless c.old_category.nil?
        c.update_attribute(:category,c.old_category.long_name)
      end
    end
    remove_column :commissaries, :category_id
    
    # remove options
    MoreOptionConversions::Option.where(:option_type => [5,10,11,13,35,36]).all.each{|o| o.destroy}
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
