class StandardizeNameFields < ActiveRecord::Migration
  class Warrant < ActiveRecord::Base
  end
  class Forbid < ActiveRecord::Base
  end
  class Person < ActiveRecord::Base
  end
  
  def up
    # contacts - name is single string, just rename it
    rename_column :contacts, :fullname, :sort_name
    add_index :contacts, :sort_name
    
    # pawns - name is single string, just rename it
    rename_column :pawns, :full_name, :sort_name
    add_index :pawns, :sort_name
    
    # damages - name is single string, just rename it
    rename_column :damages, :victim_name, :sort_name
    add_index :damages, :sort_name
    
    # invest_criminals - name is single string, just rename it
    rename_column :invest_criminals, :full_name, :sort_name
    add_index :invest_criminals, :sort_name
    
    # call_subjects - name is single string, just rename it
    rename_column :call_subjects, :name, :sort_name
    add_index :call_subjects, :sort_name
    
    # garnishments - name is single string, just rename it
    rename_column :garnishments, :name, :sort_name
    add_index :garnishments, :sort_name
    
    # warrants - rename and combine
    #   firstname, middle1, middle2, middle3 -> given_name
    #   lastname -> family_name
    #   suffix stays the same
    rename_column :warrants, :lastname, :family_name
    rename_column :warrants, :firstname, :given_name
    
    StandardizeNameFields::Warrant.find_each do |w|
      w.given_name = [w.given_name, w.middle1, w.middle2, w.middle3].reject{|n| n.blank?}.join(' ')
      w.save
    end
    
    add_index :warrants, [:family_name, :given_name]
    
    remove_column :warrants, :middle1
    remove_column :warrants, :middle2
    remove_column :warrants, :middle3
    
    # forbids - rename and combine fields
    #   forbid_first, forbid_middle1, forbid_middle2, forbid_middle3 -> given_name
    #   forbid_last -> family_name
    #   forbid_suffix -> suffix
    rename_column :forbids, :forbid_last, :family_name
    rename_column :forbids, :forbid_first, :given_name
    rename_column :forbids, :forbid_suffix, :suffix
    
    StandardizeNameFields::Forbid.find_each do |f|
      f.given_name = [f.given_name, f.forbid_middle1, f.forbid_middle2, f.forbid_middle3].reject{|n| n.blank?}.join(' ')
      f.save
    end
    
    add_index :forbids, [:family_name, :given_name]
    
    remove_column :forbids, :forbid_middle1
    remove_column :forbids, :forbid_middle2
    remove_column :forbids, :forbid_middle3
    
    # people - rename and combine fields
    #   lastname -> family_name
    #   firstname, middlename -> given_name
    #   suffix - stays the same
    rename_column :people, :lastname, :family_name
    rename_column :people, :firstname, :given_name
    
    StandardizeNameFields::Person.find_each do |p|
      p.given_name = [p.given_name, p.middlename].reject{|n| n.blank?}.join(' ')
      p.save
    end
    
    add_index :people, [:family_name, :given_name]
    
    remove_column :people, :middlename 
  end
  
  def down
    remove_index :contacts, :sort_name
    rename_column :contacts, :sort_name, :fullname
    remove_index :pawns, :sort_name
    rename_column :pawns, :sort_name, :full_name
    remove_index :damages, :sort_name
    rename_column :damages, :sort_name, :victim_name
    remove_index :invest_criminals, :sort_name
    rename_column :invest_criminals, :sort_name, :full_name
    remove_index :call_subjects, :sort_name
    rename_column :call_subjects, :sort_name, :name
    remove_index :garnishments, :sort_name
    rename_column :garnishments, :sort_name, :name
    remove_index :warrants, column: [:family_name, :given_name]
    add_column :warrants, :middle1, :string, default: '', null: false
    add_column :warrants, :middle2, :string, default: '', null: false
    add_column :warrants, :middle3, :string, default: '', null: false
    rename_column :warrants, :family_name, :lastname
    rename_column :warrants, :given_name, :firstname
    StandardizeNameFields::Warrant.find_each do |w|
      parts = w.firstname.split(/\s+/,4)
      w.firstname = parts[0] || ''
      w.middle1 = parts[1] || ''
      w.middle2 = parts[2] || ''
      w.middle3 = parts[3] || ''
      w.save
    end
    remove_index :forbids, column: [:family_name, :given_name]
    add_column :forbids, :forbid_middle1, :string, default: '', null: false
    add_column :forbids, :forbid_middle2, :string, default: '', null: false
    add_column :forbids, :forbid_middle3, :string, default: '', null: false
    rename_column :forbids, :family_name, :forbid_last
    rename_column :forbids, :given_name, :forbid_first
    StandardizeNameFields::Forbid.find_each do |f|
      parts = f.firstname.split(/\s+/,4)
      f.forbid_first = parts[0] || ''
      f.forbid_middle1 = parts[1] || ''
      f.forbid_middle2 = parts[2] || ''
      f.forbid_middle3 = parts[3] || ''
      f.save
    end
    remove_index :people, column: [:family_name, :given_name]
    add_column :people, :middlename, :string, default: '', null: false
    rename_column :people, :family_name, :lastname
    rename_column :people, :given_name, :firstname
    StandardizeNameFields::Person.find_each do |p|
      parts = p.firstname.split(/\s+/,2)
      p.firstname = parts[0] || ''
      p.middlename = parts[1] || ''
      p.save
    end
  end
end
