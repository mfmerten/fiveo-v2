class AddSiteConfiguration < ActiveRecord::Migration
  class SiteConfiguration < ActiveRecord::Base
    # called to load the old site configuration file into the configuration record
    def load_site_file
      conf_file = "#{Rails.root}/public/system/site_configuration.yml"
      if File.exists?(conf_file)
        conf = YAML.load_file(conf_file)
        # remove any attributes that no longer exist
        ckeys = conf.keys
        if ckeys[0].is_a?(Symbol)
          obsolete = ckeys - self.attribute_names.collect{|a| a.to_sym}
        else
          obsolete = ckeys - self.attribute_names
        end
        obsolete.each do |key|
          conf.delete(key)
        end
        self.attributes = conf
        return true
      end
      false
    end
  end

  def up
    create_table :site_configurations, :force => true do |t|
      t.integer :district, :default => 0
      t.integer :zone, :default => 0
      t.integer :ward, :default => 0
      t.integer :bug_admin_id, :default => 1
      t.integer :message_retention, :default => 90
      t.integer :activity_retention, :default => 90
      t.integer :session_life, :default => 720
      t.integer :session_active, :default => 15
      t.integer :session_max, :default => 960
      t.integer :goodtime_hours_per_day_local, :default => 24
      t.integer :jail1_gt_hours_per_day, :default => 0
      t.integer :jail2_gt_hours_per_day, :default => 0
      t.integer :jail3_gt_hours_per_day, :default => 0
      t.string :colorscheme, :default => "blue_brown"
      t.boolean :show_history, :default => false
      t.boolean :allow_user_photos, :default => true
      t.boolean :user_update_contact, :default => true
      t.boolean :goodtime_by_default, :default => true
      t.boolean :jail1_enabled, :default => false
      t.boolean :jail1_male, :default => false
      t.boolean :jail1_female, :default => false
      t.boolean :jail1_gt_by_default, :default => false
      t.boolean :jail2_enabled, :default => false
      t.boolean :jail2_male, :default => false
      t.boolean :jail2_female, :default => false
      t.boolean :jail2_gt_by_default, :default => false
      t.boolean :jail3_enabled, :default => false
      t.boolean :jail3_male, :default => false
      t.boolean :jail3_female, :default => false
      t.boolean :jail3_gt_by_default, :default => false
      t.decimal :mileage_fee, :precision => 12, :scale => 2, :default => 0.88
      t.decimal :noservice_fee, :precision => 12, :scale => 2, :default => 5.00
      t.string :agency, :default => "FiveO"
      t.string :agency_short, :default => "FiveO"
      t.string :motto, :default => "Law Enforcement Software"
      t.string :logo_file_name
      t.string :logo_content_type
      t.integer :logo_file_size
      t.datetime :logo_updated_at
      t.string :ceo, :default => "Michael Merten"
      t.string :ceo_title, :default => "MMMEA.COM"
      t.string :ceo_img_file_name
      t.string :ceo_img_content_type
      t.integer :ceo_img_file_size
      t.datetime :ceo_img_updated_at
      t.string :local_zips
      t.string :local_agencies
      t.string :site_email
      t.string :header_title, :default => "FiveO"
      t.string :header_subtitle1, :default => "MMMEA.com"
      t.string :header_subtitle2, :default => "Michael Merten, Owner"
      t.string :header_subtitle3, :default => "http://mmmea.com - mikemerten@suddenlink.net"
      t.string :header_left_img_file_name
      t.string :header_left_img_content_type
      t.integer :header_left_img_file_size
      t.datetime :header_left_img_updated_at
      t.string :header_right_img_file_name
      t.string :header_right_img_content_type
      t.integer :header_right_img_file_size
      t.datetime :header_right_img_updated_at
      t.string :footer_text, :default => "Law Enforcement Software"
      t.string :pdf_header_bg, :default => 'ddddff'
      t.string :pdf_header_txt, :default => '000099'
      t.string :pdf_section_bg, :default => 'ddddff'
      t.string :pdf_section_txt, :default => '000099'
      t.string :pdf_em_bg, :default => 'ffdddd'
      t.string :pdf_em_txt, :default => '990000'
      t.string :pdf_row_odd, :default => 'ffffff'
      t.string :pdf_row_even, :default => 'eeeeee'
      t.string :dispatch_shift1_start, :default => '05:30'
      t.string :dispatch_shift1_stop, :default => '17:30'
      t.string :dispatch_shift2_start, :default => '17:30'
      t.string :dispatch_shift2_stop, :default => '05:30'
      t.string :dispatch_shift3_start
      t.string :dispatch_shift3_stop
      t.string :jail1_name
      t.string :jail1_short_name
      t.string :jail2_name
      t.string :jail2_short_name
      t.string :jail3_name
      t.string :jail3_short_name
      t.string :probation_fund1, :default => "Fines"
      t.string :probation_fund2, :default => "Costs"
      t.string :probation_fund3, :default => "Probation Fees"
      t.string :probation_fund4, :default => "IDF"
      t.string :probation_fund5, :default => "DARE"
      t.string :probation_fund6, :default => "Restitution"
      t.string :probation_fund7, :default => "DA Fees"
      t.string :probation_fund8
      t.string :probation_fund9
      t.string :paper_pay_type1, :default => "Cash"
      t.string :paper_pay_type2, :default => "Check"
      t.string :paper_pay_type3, :default => "Money Order"
      t.string :paper_pay_type4, :default => "Credit Card"
      t.string :paper_pay_type5
      t.boolean :enable_exception_notifications, :default => false
      t.string :default_state
      t.string :patrol_shift1_start, :default => '06:00'
      t.string :patrol_shift1_stop, :default => '18:00'
      t.string :patrol_shift2_start, :default => '18:00'
      t.string :patrol_shift2_stop, :default => '06:00'
      t.string :patrol_shift3_start
      t.string :patrol_shift3_stop
      t.string :patrol_shift1_supervisor
      t.string :patrol_shift2_supervisor
      t.string :patrol_shift3_supervisor
      t.string :patrol_shift4_start
      t.string :patrol_shift4_stop
      t.string :patrol_shift5_start
      t.string :patrol_shift5_stop
      t.string :patrol_shift6_start
      t.string :patrol_shift6_stop
      t.string :patrol_shift4_supervisor
      t.string :patrol_shift5_supervisor
      t.string :patrol_shift6_supervisor
      t.integer :created_by, :default => 1
      t.integer :updated_by, :default => 1
      t.timestamps
    end

    AddSiteConfiguration::SiteConfiguration.reset_column_information
    # create the "default" configuration (record 1)
    AddSiteConfiguration::SiteConfiguration.create
    # now create the "site" configuration (record 2)
    conf = AddSiteConfiguration::SiteConfiguration.new
    if conf.load_site_file
      if conf.save
        announce("Site Configuration Record Created Successfully!")
      else
        announce("Could Not Save Site Configuration Record! Rollback and try again with --trace")
      end
    else
      announce("Could Not Load Old Site Configuration File! Rollback and try again with --trace")
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
