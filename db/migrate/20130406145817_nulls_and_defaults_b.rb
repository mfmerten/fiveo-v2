class NullsAndDefaultsB < ActiveRecord::Migration
  class Bond < ActiveRecord::Base
  end
  class Booking < ActiveRecord::Base
  end
  class BookingLog < ActiveRecord::Base
  end
  class BookingProperty < ActiveRecord::Base
  end
  
  def up
    NullsAndDefaultsB::Bond.delete_all("person_id IS NULL")
    change_column :bonds, :person_id, :integer, :null => false
    [:power, :bond_no, :status, :bond_type, :bondsman_name, :bond_company,
      :bondsman_address1, :bondsman_address2, :bondsman_address3].each do |col|
      NullsAndDefaultsB::Bond.update_all("#{col} = ''", "#{col} IS NULL")
      change_column :bonds, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsB::Bond.update_all("remarks = ''", "remarks IS NULL")
    change_column :bonds, :remarks, :text, :null => false, :default => ''
    NullsAndDefaultsB::Bond.update_all("final = FALSE", "final IS UNKNOWN")
    change_column :bonds, :final, :boolean, :null => false, :default => false
    
    NullsAndDefaultsB::Booking.delete_all("person_id IS NULL OR jail_id IS NULL")
    change_column :bookings, :person_id, :integer, :null => false
    change_column :bookings, :jail_id, :integer, :null => false
    [:cash_receipt, :booking_officer, :booking_officer_badge, :booking_officer_unit,
      :phone_called, :release_officer, :release_officer_badge, :release_officer_unit,
      :attorney, :cell, :released_because].each do |col|
      NullsAndDefaultsB::Booking.update_all("#{col} = ''", "#{col} IS NULL")
      change_column :bookings, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsB::Booking.update_all("remarks = ''", "remarks IS NULL")
    change_column :bookings, :remarks, :text, :null => false, :default => ''
    [:intoxicated, :afis, :disable_timers, :legacy].each do |col|
      NullsAndDefaultsB::Booking.update_all("#{col} = FALSE","#{col} IS UNKNOWN")
      change_column :bookings, col, :boolean, :null => false, :default => false
    end
    NullsAndDefaultsB::Booking.update_all("good_time = FALSE","good_time IS UNKNOWN")
    change_column :bookings, :good_time, :boolean, :null => false, :default => true
    
    NullsAndDefaultsB::BookingLog.delete_all("booking_id IS NULL")
    change_column :booking_logs, :booking_id, :integer, :null => false
    [:start_officer, :start_officer_unit, :start_officer_badge, :stop_officer,
      :stop_officer_unit, :stop_officer_badge].each do |col|
      NullsAndDefaultsB::BookingLog.update_all("#{col} = ''","#{col} IS NULL")
      change_column :booking_logs, col, :string, :null => false, :default => ''
    end
    
    NullsAndDefaultsB::BookingProperty.delete_all("booking_id IS NULL")
    change_column :booking_properties, :booking_id, :integer, :null => false
    NullsAndDefaultsB::BookingProperty.update_all("quantity = 1", "quantity IS NULL")
    change_column :booking_properties, :quantity, :integer, :null => false, :default => 1
    NullsAndDefaultsB::BookingProperty.update_all("description = ''", "description IS NULL")
    change_column :booking_properties, :description, :string, :null => false, :default => ''
  end

  def down
  end
end
