class UnPolymorphicForumView < ActiveRecord::Migration
  class ForumView < ActiveRecord::Base
  end
  
  def up
    add_column :forum_views, :forum_id, :integer
    add_column :forum_views, :forum_topic_id, :integer
    UnPolymorphicForumView::ForumView.reset_column_information
    UnPolymorphicForumView::ForumView.all.each do |v|
      if v.viewable_type == 'Forum'
        v.update_attribute(:forum_id, v.viewable_id)
      end
      if v.viewable_type == 'ForumTopic'
        v.update_attribute(:forum_topic_id, v.viewable_id)
      end
    end
    remove_column :forum_views, :viewable_id
    remove_column :forum_views, :viewable_type
  end

  def down
    add_column :forum_views, :viewable_id, :integer
    add_column :forum_views, :viewable_type, :string
    UnPolymorphicForumView::ForumView.reset_column_information
    UnPolymorphicForumView::ForumView.all.each do |v|
      if v.forum_id?
        v.update_attribute(:viewable_id, v.forum_id)
        v.update_attribute(:viewable_type, 'Forum')
      elsif v.forum_topic_id?
        v.update_attribute(:viewable_id, v.forum_topic_id)
        v.update_attribute(:viewable_type, 'ForumTopic')
      end
    end
    remove_column :forum_views, :forum_id
    remove_column :forum_views, :forum_topic_id
  end
end
