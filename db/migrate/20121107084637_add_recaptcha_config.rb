class AddRecaptchaConfig < ActiveRecord::Migration
  def up
    add_column :site_configurations, :recaptcha_enabled, :boolean, :default => false
    add_column :site_configurations, :recaptcha_public_key, :string
    add_column :site_configurations, :recaptcha_private_key, :string
    add_column :site_configurations, :recaptcha_proxy, :string
  end

  def down
    remove_column :site_configurations, :recaptcha_enabled
    remove_column :site_configurations, :recaptcha_public_key
    remove_column :site_configurations, :recaptcha_private_key
    remove_column :site_configurations, :recaptcha_proxy
  end
end
