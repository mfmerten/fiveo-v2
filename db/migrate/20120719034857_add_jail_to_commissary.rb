class AddJailToCommissary < ActiveRecord::Migration
  class Commissary < ActiveRecord::Base
    belongs_to :jail, :class_name => 'AddJailToCommissary::Jail'
    belongs_to :person, :class_name => 'AddJailToCommissary::Person'
  end

  class Person < ActiveRecord::Base
    has_many :commissaries, :class_name => 'AddJailToCommissary::Commissary'
  end

  class Jail < ActiveRecord::Base
    has_many :commissaries, :class_name => 'AddJailToCommissary::Commissary'
  end
  
  def up
    add_column :commissaries, :jail_id, :integer
    add_index :commissaries, :jail_id
    
    AddJailToCommissary::Commissary.reset_column_information
    
    AddJailToCommissary::Commissary.all.each do |c|
      unless c.person.nil?
        c.update_attribute(:jail_id, c.person.jail_id)
      end
    end
    
    remove_column :people, :jail_id
  end

  def down
    add_column :people, :jail_id, :integer
    add_index :people, :jail_id
    
    AddJailToCommissary::Person.reset_column_information
    
    AddJailToCommissary::Commissary.all.each do |c|
      unless c.person.nil?
        if c.jail_id != c.person.jail_id
          c.person.update_attribute(:jail_id, c.jail_id)
        end
      end
    end
    
    remove_column :commissaries, :jail_id
  end
end
