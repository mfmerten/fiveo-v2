class AddProblemNotification < ActiveRecord::Migration
  def up
    add_column :users, :notify_booking_problems, :boolean, :default => false
    add_column :jails, :notify_frequency, :string
  end

  def down
    remove_column :users, :notify_booking_problems
    remove_column :jails, :notify_frequency
  end
end
