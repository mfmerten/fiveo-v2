class NullsAndDefaultsP < ActiveRecord::Migration
  class Paper < ActiveRecord::Base
  end
  class PaperCustomer < ActiveRecord::Base
  end
  class PaperPayment < ActiveRecord::Base
  end
  class PaperType < ActiveRecord::Base
  end
  class Patrol < ActiveRecord::Base
  end
  class Pawn < ActiveRecord::Base
  end
  class PawnItem < ActiveRecord::Base
  end
  class PawnItemType < ActiveRecord::Base
  end
  class Person < ActiveRecord::Base
  end
  class PersonAssociate < ActiveRecord::Base
  end
  class PersonMark < ActiveRecord::Base
  end
  class Probation < ActiveRecord::Base
  end
  class ProbationPayment < ActiveRecord::Base
  end
  
  def up
    [:suit_no, :plaintiff, :defendant, :serve_to, :street, :city, :zip, :phone,
      :noservice_extra, :served_to, :officer, :officer_badge, :officer_unit,
      :memo, :state, :noservice_type, :service_type, :paper_type].each do |col|
      NullsAndDefaultsP::Paper.update_all("#{col} = ''","#{col} IS NULL")
      change_column :papers, col, :string, :null => false, :default => ''
    end
    [:comments, :remarks].each do |col|
      NullsAndDefaultsP::Paper.update_all("#{col} = ''","#{col} IS NULL")
      change_column :papers, col, :text, :null => false, :default => ''
    end
    [:billable, :mileage_only, :paid_in_full].each do |col|
      NullsAndDefaultsP::Paper.update_all("#{col} = FALSE","#{col} IS UNKNOWN")
      change_column_null :papers, col, false
    end
    [:invoice_total, :noservice_fee, :mileage_fee, :service_fee, :mileage].each do |col|
      NullsAndDefaultsP::Paper.update_all("#{col} = 0.0","#{col} IS NULL")
      change_column_null :papers, col, false
    end
    
    [:name, :street, :city, :zip, :phone, :attention, :fax, :street2].each do |col|
      NullsAndDefaultsP::PaperCustomer.update_all("#{col} = ''","#{col} IS NULL")
      change_column :paper_customers, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsP::PaperCustomer.update_all("remarks = ''","remarks IS NULL")
    change_column :paper_customers, :remarks, :text, :null => false, :default => ''
    NullsAndDefaultsP::PaperCustomer.update_all("disabled = FALSE","disabled IS NULL")
    change_column_null :paper_customers, :disabled, false
    [:noservice_fee, :mileage_fee].each do |col|
      NullsAndDefaultsP::PaperCustomer.update_all("#{col} = 0.0","#{col} IS NULL")
      change_column_null :paper_customers, col, false
    end
  
    NullsAndDefaultsP::PaperPayment.delete_all("paper_customer_id IS NULL")
    change_column_null :paper_payments, :paper_customer_id, false
    [:officer, :officer_unit, :officer_badge, :receipt_no, :memo, :payment_method].each do |col|
      NullsAndDefaultsP::PaperPayment.update_all("#{col} = ''","#{col} IS NULL")
      change_column :paper_payments, col, :string, :null => false, :default => ''
    end
    [:payment, :charge].each do |col|
      NullsAndDefaultsP::PaperPayment.update_all("#{col} = 0.0","#{col} IS NULL")
      change_column_null :paper_payments, col, false
    end
    [:paid_in_full, :adjustment].each do |col|
      NullsAndDefaultsP::PaperPayment.update_all("#{col} = FALSE","#{col} IS UNKNOWN")
      change_column_null :paper_payments, col, false
    end
    
    NullsAndDefaultsP::PaperType.delete_all("name IS NULL")
    change_column :paper_types, :name, :string, :null => false, :default => ''
    NullsAndDefaultsP::PaperType.update_all("cost = 0.0","cost IS NULL")
    change_column_null :paper_types, :cost, false
    
    [:officer, :officer_unit, :officer_badge, :supervisor, :shift_start, :shift_stop].each do |col|
      NullsAndDefaultsP::Patrol.update_all("#{col} = ''","#{col} IS NULL")
      change_column :patrols, col, :string, :null => false, :default => ''
    end
    [:beginning_odometer, :ending_odometer].each do |col|
      NullsAndDefaultsP::Patrol.update_all("#{col} = 0","#{col} IS NULL")
      change_column :patrols, col, :integer, :null => false, :default => 0
    end
    [:shift, :roads_patrolled, :complaints, :traffic_stops, :citations_issued,
      :papers_served, :narcotics_arrests, :other_arrests, :public_assists,
      :warrants_served, :funeral_escorts].each do |col|
      NullsAndDefaultsP::Patrol.update_all("#{col} = 0","#{col} IS NULL")
      change_column_null :patrols, col, false
    end
    NullsAndDefaultsP::Patrol.update_all("remarks = ''","remarks IS NULL")
    change_column :patrols, :remarks, :text, :null => false, :default => ''
    
    [:ticket_no, :case_no, :full_name, :oln, :ssn, :address1, :address2,
      :oln_state, :pawn_co, :ticket_type].each do |col|
      NullsAndDefaultsP::Pawn.update_all("#{col} = ''","#{col} IS NULL")
      change_column :pawns, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsP::Pawn.update_all("remarks = ''","remarks IS NULL")
    change_column :pawns, :remarks, :text, :null => false, :default => ''
    NullsAndDefaultsP::Pawn.update_all("legacy = FALSE","legacy IS NULL")
    change_column_null :pawns, :legacy, false
    
    NullsAndDefaultsP::PawnItem.delete_all("pawn_id IS NULL")
    change_column_null :pawn_items, :pawn_id, false
    [:model_no, :serial_no, :description, :item_type].each do |col|
      NullsAndDefaultsP::PawnItem.update_all("#{col} = ''","#{col} IS NULL")
      change_column :pawn_items, col, :string, :null => false, :default => ''
    end
    
    NullsAndDefaultsP::PawnItemType.delete_all("item_type IS NULL")
    change_column :pawn_item_types, :item_type, :string, :null => false, :default => ''
    
    [:lastname, :firstname, :middlename, :suffix, :aka, :phys_street, :phys_city,
      :phys_zip, :mail_street, :mail_city, :mail_zip, :place_of_birth, :fbi,
      :sid, :ssn, :oln, :home_phone, :cell_phone, :email, :shoe_size, :occupation,
      :employer, :emergency_contact, :emergency_address, :emergency_phone,
      :med_allergies, :gang_affiliation, :doc_number, :dna_profile, :fingerprint_class,
      :phys_state, :mail_state, :oln_state, :race, :build_type, :eye_color, :hair_color,
      :skin_tone, :complexion, :hair_type, :mustache, :beard, :eyebrow, :em_relationship].each do |col|
      NullsAndDefaultsP::Person.update_all("#{col} = ''","#{col} IS NULL")
      change_column :people, col, :string, :null => false, :default => ''
    end
    [:sex, :height_ft, :height_in, :weight].each do |col|
      NullsAndDefaultsP::Person.update_all("#{col} = 0","#{col} IS NULL")
      change_column :people, col, :integer, :null => false, :default => 0
    end
    [:glasses, :hepatitis, :hiv, :tb, :deceased, :legacy, :sex_offender].each do |col|
      NullsAndDefaultsP::Person.update_all("#{col} = FALSE","#{col} IS NULL")
      change_column_null :people, col, false
    end
    NullsAndDefaultsP::Person.update_all("remarks = ''","remarks IS NULL")
    change_column :people, :remarks, :text, :null => false, :default => ''
  
    NullsAndDefaultsP::PersonAssociate.delete_all("person_id IS NULL")
    change_column_null :person_associates, :person_id, false
    [:name, :relationship].each do |col|
      NullsAndDefaultsP::PersonAssociate.update_all("#{col} = ''","#{col} IS NULL")
      change_column :person_associates, col, :string, :null => false, :default => ''
    end
    NullsAndDefaultsP::PersonAssociate.update_all("remarks = ''","remarks IS NULL")
    change_column :person_associates, :remarks, :text, :null => false, :default => ''
  
    NullsAndDefaultsP::PersonMark.delete_all("person_id IS NULL")
    change_column_null :person_marks, :person_id, false
    [:description, :location].each do |col|
      NullsAndDefaultsP::PersonMark.update_all("#{col} = ''","#{col} IS NULL")
      change_column :person_marks, col, :string, :null => false, :default => ''
    end
  
    NullsAndDefaultsP::Probation.delete_all("person_id IS NULL")
    change_column_null :probations, :person_id, false
    [:revoked_for, :probation_officer, :probation_officer_unit, :probation_officer_badge,
      :trial_result, :reverts_conditions, :docket_no, :conviction, :fund1_name,
      :fund2_name, :fund3_name, :fund4_name, :fund5_name, :fund6_name, :fund7_name,
      :fund8_name, :fund9_name, :status, :terminate_reason].each do |col|
      NullsAndDefaultsP::Probation.update_all("#{col} = ''","#{col} IS NULL")
      change_column :probations, col, :string, :null => false, :default => ''
    end
    [:notes, :remarks, :sentence_notes].each do |col|
      NullsAndDefaultsP::Probation.update_all("#{col} = ''","#{col} IS NULL")
      change_column :probations, col, :text, :null => false, :default => ''
    end
    [:hold_if_arrested, :doc, :parish_jail, :sentence_suspended, :reverts_to_unsup,
      :credit_served, :sap, :sap_complete, :driver, :driver_complete, :anger,
      :anger_complete, :sat, :sat_complete, :random_screens, :no_victim_contact,
      :art893, :art894, :art895, :sentence_death, :pay_during_prob].each do |col|
      NullsAndDefaultsP::Probation.update_all("#{col} = FALSE","#{col} IS UNKNOWN")
      change_column_null :probations, col, false
    end
    change_column_default :probations, :conviction_count, 1
    [:probation_days, :probation_months, :probation_years, :default_days, :default_months,
      :default_years, :sentence_days, :sentence_months, :sentence_years, :suspended_except_days,
      :suspended_except_months, :suspended_except_years, :service_days, :service_served,
      :probation_hours, :default_hours, :sentence_hours, :suspended_except_hours,
      :service_hours, :sentence_life, :suspended_except_life, :conviction_count].each do |col|
      NullsAndDefaultsP::Probation.update_all("#{col} = 0","#{col} IS NULL")
      change_column_null :probations, col, false
    end
    [:costs, :probation_fees, :idf_amount, :dare_amount, :restitution_amount, :fines,
      :fund1_charged, :fund2_charged, :fund3_charged, :fund4_charged, :fund5_charged,
      :fund6_charged, :fund7_charged, :fund8_charged, :fund9_charged, :fund1_paid,
      :fund2_paid, :fund3_paid, :fund4_paid, :fund5_paid, :fund6_paid, :fund7_paid,
      :fund8_paid, :fund9_paid].each do |col|
      NullsAndDefaultsP::Probation.update_all("#{col} = 0.0","#{col} IS NULL")
      change_column_null :probations, col, false
    end
  
    NullsAndDefaultsP::ProbationPayment.delete_all("probation_id IS NULL")
    change_column_null :probation_payments, :probation_id, false
    [:officer, :officer_unit, :officer_badge, :receipt_no, :memo, :fund1_name,
      :fund2_name, :fund3_name, :fund4_name, :fund5_name, :fund6_name, :fund7_name,
      :fund8_name, :fund9_name].each do |col|
      NullsAndDefaultsP::ProbationPayment.update_all("#{col} = ''","#{col} IS NULL")
      change_column :probation_payments, col, :string, :null => false, :default => ''
    end
    [:fund1_charge, :fund1_payment, :fund2_charge, :fund2_payment, :fund3_charge,
      :fund3_payment, :fund4_charge, :fund4_payment, :fund5_charge, :fund5_payment,
      :fund6_charge, :fund6_payment, :fund7_charge, :fund7_payment, :fund8_charge,
      :fund8_payment, :fund9_charge, :fund9_payment].each do |col|
      NullsAndDefaultsP::ProbationPayment.update_all("#{col} = 0.0","#{col} IS NULL")
      change_column_null :probation_payments, col, false
    end
  end

  def down
  end
end
