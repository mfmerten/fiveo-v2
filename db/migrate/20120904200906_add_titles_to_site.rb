class AddTitlesToSite < ActiveRecord::Migration
  def change
    add_column :site_configurations, :jurisdiction_title, :string, :default => 'Sabine Parish'
    add_column :site_configurations, :court_title, :string, :default => 'Eleventh Judicial District'
    add_column :site_configurations, :prosecutor_title, :string, :default => 'District Attorney'
    add_column :site_configurations, :chief_justice_title, :string, :default => 'Judge'
    add_column :site_configurations, :court_clerk_name, :string, :default => 'Michael Merten'
    add_column :site_configurations, :court_clerk_title, :string, :default => 'Owner'
  end
end
