class AddPhotoToWarrant < ActiveRecord::Migration
  def change
    add_column :warrants, :photo_file_name, :string
    add_column :warrants, :photo_content_type, :string
    add_column :warrants, :photo_file_size, :integer
    add_column :warrants, :photo_updated_at, :datetime
  end
end
