class RemoveUnencryptedSsn < ActiveRecord::Migration
  def up
    remove_column :people, :old_ssn
  end
  
  def down
    add_column :people, :old_ssn, :string, default: '', null: false
  end
end
