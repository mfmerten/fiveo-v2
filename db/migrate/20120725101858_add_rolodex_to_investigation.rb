class AddRolodexToInvestigation < ActiveRecord::Migration
  class Investigation < ActiveRecord::Base
    has_and_belongs_to_many :contacts, :class_name => 'AddRolodexToInvestigation::Contact'
    has_and_belongs_to_many :invest_contacts, :class_name => 'AddRolodexToInvestigation::InvestContact'
  end
  class Contact < ActiveRecord::Base
  end
  class InvestContact < ActiveRecord::Base
    has_and_belongs_to_many :investigations, :class_name => 'AddRolodexToInvestigation::Investigation'
  end
  
  def up
    # create invest_contacts table and join
    create_table :invest_contacts, :force => true do |t|
      t.boolean :private
      t.integer :owned_by
      t.string :title
      t.string :unit
      t.string :street
      t.string :city
      t.string :zip
      t.string :pri_email
      t.string :alt_email
      t.string :web_site
      t.string :notes
      t.boolean :officer
      t.string :street2
      t.integer :created_by, :default => 1
      t.integer :updated_by, :default => 1
      t.string :badge_no
      t.boolean :active, :default => false
      t.boolean :detective, :default => false
      t.boolean :physician, :default => false
      t.boolean :pharmacy, :default => false
      t.boolean :business, :default => false
      t.string :fullname
      t.integer :agency_id
      t.boolean :printable, :default => true
      t.string :state
      t.string :phone1
      t.string :phone2
      t.string :phone3
      t.integer :phone1_type_id
      t.integer :phone2_type_id
      t.integer :phone3_type_id
      t.boolean :phone1_unlisted
      t.boolean :phone2_unlisted
      t.boolean :phone3_unlisted
      t.string :phone1_note
      t.string :phone2_note
      t.string :phone3_note
      t.timestamps
    end
    
    add_index :invest_contacts, :agency_id
    add_index :invest_contacts, :alt_email
    add_index :invest_contacts, :city
    add_index :invest_contacts, :created_by
    add_index :invest_contacts, :notes
    add_index :invest_contacts, :pri_email
    add_index :invest_contacts, :street
    add_index :invest_contacts, :street2
    add_index :invest_contacts, :title
    add_index :invest_contacts, :unit
    add_index :invest_contacts, :updated_by
    
    create_table :invest_contacts_investigations, :id => false, :force => true do |t|
      t.integer :invest_contact_id
      t.integer :investigation_id
    end
    
    add_index :invest_contacts_investigations, :invest_contact_id
    add_index :invest_contacts_investigations, :investigation_id
    
    # look through existing investigations.  If contacts are linked, 
    # clone them to invest_contacts and link those.
    AddRolodexToInvestigation::Investigation.all.each do |i|
      unless i.contacts.empty?
        i.contacts.each do |c|
          r = AddRolodexToInvestigation::InvestContact.new
          cattr = c.attributes
          cattr.delete('id')
          cattr.delete('legacy')
          r.attributes = cattr
          r.private = true
          r.owned_by = i.owned_by
          r.save
          i.invest_contacts << r
          i.save
        end
      end
    end
    
    # get rid of the old contacts_investigations table
    drop_table :contacts_investigations
  end
  
  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
