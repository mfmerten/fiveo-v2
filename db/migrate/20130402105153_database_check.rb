class DatabaseCheck < ActiveRecord::Migration
  def up
    unless adapter_name.to_s == 'PostgreSQL'
      raise("You must convert the database to PostgreSQL before continuing!")
    end
  end

  def down
    # do not want to roll back beyond this point because of the headaches
    # involved with the changeover of the database
    raise ActiveRecord::IrreversibleMigration
  end
end
