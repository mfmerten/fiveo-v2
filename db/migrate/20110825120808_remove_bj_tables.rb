class RemoveBjTables < ActiveRecord::Migration
  def self.up
    if table_exists?('bj_config')
      drop_table :bj_config
    end
    if table_exists?('bj_job')
      drop_table :bj_job
    end
    if table_exists?('bj_job_archive')
      drop_table :bj_job_archive
    end
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
