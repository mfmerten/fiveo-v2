class BookingCellToText < ActiveRecord::Migration
  class Person < ActiveRecord::Base
    belongs_to :facility, :class_name => "BookingCellToText::Option", :foreign_key => 'commissary_facility_id'
    belongs_to :jail, :class_name => "BookingCellToText::Jail", :foreign_key => 'jail_id'
  end
  class Absent < ActiveRecord::Base
    belongs_to :facility, :class_name => "BookingCellToText::Option", :foreign_key => 'facility_id'
    belongs_to :jail, :class_name => "BookingCellToText::Jail", :foreign_key => 'jail_id'
  end
  class CommissaryItem < ActiveRecord::Base
    has_and_belongs_to_many :facilities, :class_name => "BookingCellToText::Option", :join_table => 'commissary_facilities'
    belongs_to :jail, :class_name => "BookingCellToText::Jail", :foreign_key => 'jail_id'
  end
  class Jail < ActiveRecord::Base
    has_many :people, :class_name => "BookingCellToText::Person", :foreign_key => 'jail_id'
    has_many :absents, :class_name => "BookingCellToText::Absent", :foreign_key => 'jail_id'
    has_many :commissary_items, :class_name => "BookingCellToText::CommissaryItem", :foreign_key => 'jail_id'
  end
  class Option < ActiveRecord::Base
    has_many :people, :class_name => "BookingCellToText::Person", :foreign_key => 'commissary_facility_id'
    has_many :absents, :class_name => "BookingCellToText::Absent", :foreign_key => 'facility_id'
  end
  class Booking < ActiveRecord::Base
    belongs_to :jail_cell, :class_name => "BookingCellToText::JailCell", :foreign_key => 'cell_id'
  end
  class JailCell < ActiveRecord::Base
    has_many :bookings, :foreign_key => 'cell_id'
  end
  
  def up
    add_column :bookings, :cell, :string
    
    BookingCellToText::Booking.reset_column_information
    
    BookingCellToText::Booking.all.each do |b|
      unless b.jail_cell.nil?
        b.cell = b.jail_cell.name
        b.save
      end
    end
    
    remove_column :bookings, :cell_id
    
    # this adds person.jail_id (last jail person was booked into), converts
    # person.commissary_facility_id to jail_id, then removes person.commissary_facility_id
    add_column :people, :jail_id, :integer
    
    BookingCellToText::Person.reset_column_information
    
    spdc = BookingCellToText::Option.where(:short_name => "SPDC").first
    spwf = BookingCellToText::Option.where(:short_name => "SPWF").first
    jail1 = BookingCellToText::Jail.where(:acronym => "SPDC").first
    jail2 = BookingCellToText::Jail.where(:acronym => "SPWF").first
    BookingCellToText::Person.all.each do |p|
      unless p.facility.nil?
        if p.facility.id == spdc.id
          p.jail_id = jail1.id
        else
          p.jail_id = jail2.id
        end
        p.save
      end
    end
    
    remove_column :people, :commissary_facility_id
    
    # this adds absent.jail_id, converts absent.facility_id to jail_id, then
    # removes absent.facility_id
    add_column :absents, :jail_id, :integer
    
    BookingCellToText::Absent.reset_column_information
    
    BookingCellToText::Absent.all.each do |a|
      unless a.facility.nil?
        if a.facility.id == spdc.id
          a.jail_id = jail1.id
        else
          a.jail_id = jail2.id
        end
        a.save
      end
    end
    
    remove_column :absents, :facility_id
    
    # this adds commissary_item.jail_id, converts commissary_item.facility_id
    # to jail_id, then removes commissary_item.facility_id
    # NOTE: this only works because SPSO does not use multiple facilities with
    # any commissary item!
    add_column :commissary_items, :jail_id, :integer
    
    BookingCellToText::CommissaryItem.reset_column_information
    
    BookingCellToText::CommissaryItem.all.each do |i|
      unless i.facilities.empty?
        if i.facilities.first.id == spdc.id
          i.jail_id = jail1.id
        else
          i.jail_id = jail2.id
        end
        i.save
      end
    end
    
    drop_table :commissary_facilities
    
    # add in the indexes I forgot above
    add_index :people, :jail_id
    add_index :absents, :jail_id
    add_index :commissary_items, :jail_id
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
