class RemoveWardenFromJails < ActiveRecord::Migration
  def up
    remove_column :jails, :warden_id
  end

  def down
    add_column :jails, :warden_id, :integer, :default => 1
    add_index :jails, :warden_id
  end
end
