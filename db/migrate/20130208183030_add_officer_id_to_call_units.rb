class AddOfficerIdToCallUnits < ActiveRecord::Migration
  # ok, ok, so I piggybacked case notes onto this, sue me
  def up
    add_column :call_units, :officer_id, :integer
    remove_column :case_notes, :note_datetime
  end
  
  def down
    remove_column :call_units, :officer_id
    add_column :case_notes, :note_datetime, :datetime
  end
end
