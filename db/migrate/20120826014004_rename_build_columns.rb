class RenameBuildColumns < ActiveRecord::Migration
  def up
    # ack! forgot about build method for AR
    rename_column :people, :build, :build_type
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
