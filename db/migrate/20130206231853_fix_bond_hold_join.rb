class FixBondHoldJoin < ActiveRecord::Migration
  class Bond < ActiveRecord::Base
    has_and_belongs_to_many :holds, :class_name => 'FixBondHoldJoin::Hold'
    has_many :hold_bonds, :class_name => 'FixBondHoldJoin::HoldBond'
    has_many :xholds, :through => :hold_bonds, :source => :hold
  end
  
  class Hold < ActiveRecord::Base
    has_and_belongs_to_many :bonds, :class_name => 'FixBondHoldJoin::Bond'
    has_many :hold_bonds, :class_name => 'FixBondHoldJoin::HoldBond'
    has_many :xbonds, :through => :hold_bonds, :source => :bond
  end
  
  class HoldBond < ActiveRecord::Base
    belongs_to :bond, :class_name => 'FixBondHoldJoin::Bond'
    belongs_to :hold, :class_name => 'FixBondHoldJoin::Hold'
  end
  
  def up
    create_table :hold_bonds, :force => true do |t|
      t.integer :bond_id
      t.integer :hold_id
      t.timestamps
    end
    add_index :hold_bonds, :bond_id
    add_index :hold_bonds, :hold_id
    
    FixBondHoldJoin::Bond.all.each do |b|
      b.xhold_ids = b.hold_ids
    end
    
    drop_table :bonds_holds
  end

  def down
    create_table :bonds_holds, :id => false, :force => true do |t|
      t.integer :bond_id
      t.integer :hold_id
    end
    add_index :bonds_holds, :bond_id
    add_index :bonds_holds, :hold_id
    
    FixBondHoldJoin::Bond.all.each do |b|
      b.hold_ids = b.xhold_ids
    end
    
    drop_table :hold_bonds
  end
end
