class AddAfisReports < ActiveRecord::Migration
  def up
    add_column :site_configurations, :afis_ori, :string
    add_column :site_configurations, :afis_agency, :string
    add_column :site_configurations, :afis_street, :string
    add_column :site_configurations, :afis_city, :string
    add_column :site_configurations, :afis_state, :string
    add_column :site_configurations, :afis_zip, :string
    add_column :site_configurations, :agency_street, :string
    add_column :site_configurations, :agency_city, :string
    add_column :site_configurations, :agency_state, :string
    add_column :site_configurations, :agency_zip, :string
    rename_column :site_configurations, :agency_short, :agency_acronym
    remove_column :site_configurations, :default_state
    
    add_index :arrests, :atn
    
    remove_column :dockets, :state_report_date
    remove_column :courts, :needs_state_report
    
    create_table :afis_reports, :force => true do |t|
      t.string :atn
      t.integer :person_id
      t.date :report_date
      t.text :disposition
      t.text :remarks
      t.integer :status, :default => 0
      t.integer :created_by, :default => 1
      t.integer :updated_by, :default => 1
      t.timestamps
    end
    add_index :afis_reports, :person_id
    add_index :afis_reports, :created_by
    add_index :afis_reports, :updated_by
    add_index :afis_reports, :atn
    add_index :afis_reports, :status
  end

  def down
    remove_column :site_configurations, :afis_ori
    remove_column :site_configurations, :afis_agency
    remove_column :site_configurations, :afis_street
    remove_column :site_configurations, :afis_city
    remove_column :site_configurations, :afis_state
    remove_column :site_configurations, :afis_zip
    remove_column :site_configurations, :agency_street
    remove_column :site_configurations, :agency_city
    remove_column :site_configurations, :agency_state
    remove_column :site_configurations, :agency_zip
    rename_column :site_configurations, :agency_acronym, :agency_short
    add_column :site_configurations, :default_state, :string
    
    remove_index :arrests, :atn
    
    add_column :dockets, :state_report_date, :date
    add_column :courts, :needs_state_report, :boolean, :default => false
    
    drop_table :afis_reports
  end
end
