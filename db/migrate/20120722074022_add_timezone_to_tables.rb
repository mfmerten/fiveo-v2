class AddTimezoneToTables < ActiveRecord::Migration
  class Absent < ActiveRecord::Base
  end
  class Arrest < ActiveRecord::Base
  end
  class Bond < ActiveRecord::Base
  end
  class Booking < ActiveRecord::Base
  end
  class Bug < ActiveRecord::Base
  end
  class Call < ActiveRecord::Base
  end
  class Citation < ActiveRecord::Base
  end
  class Doc < ActiveRecord::Base
  end
  class EvidenceLocation < ActiveRecord::Base
  end
  class Hold < ActiveRecord::Base
  end
  class InvestCrime < ActiveRecord::Base
  end
  class InvestReport < ActiveRecord::Base
  end
  class Investigation < ActiveRecord::Base
  end
  class Medical < ActiveRecord::Base
  end
  class Offense < ActiveRecord::Base
  end
  class Paper < ActiveRecord::Base
  end
  class Patrol < ActiveRecord::Base
  end
  class Transfer < ActiveRecord::Base
  end
  class Transport < ActiveRecord::Base
  end
  
  # accepts a Date string time value and returns
  # the corresponding Time value for the current time zone (Central)
  def get_time(date=nil,time=nil)
    return nil unless date.is_a?(Date) && (time.nil? || (time.is_a?(String) && time =~ /\d\d:\d\d/))
    time = "00:00" if time.nil?
    Time.parse("#{date} #{time}")
  end
  
  def up
    add_column :absents, :leave_datetime, :datetime
    add_column :absents, :return_datetime, :datetime
    AddTimezoneToTables::Absent.reset_column_information
    AddTimezoneToTables::Absent.all.each do |a|
      a.leave_datetime = get_time(a.leave_date, a.leave_time)
      a.return_datetime = get_time(a.return_date, a.return_time)
      a.save
    end
    remove_column :absents, :leave_date
    remove_column :absents, :leave_time
    remove_column :absents, :return_date
    remove_column :absents, :return_time
    
    add_column :arrests, :arrest_datetime, :datetime
    add_column :arrests, :hour_72_datetime, :datetime
    AddTimezoneToTables::Arrest.reset_column_information
    AddTimezoneToTables::Arrest.all.each do |a|
      a.arrest_datetime = get_time(a.arrest_date, a.arrest_time)
      a.hour_72_datetime = get_time(a.hour_72_date, a.hour_72_time)
      a.save
    end
    remove_column :arrests, :arrest_date
    remove_column :arrests, :arrest_time
    remove_column :arrests, :hour_72_date
    remove_column :arrests, :hour_72_time
    
    add_column :bonds, :issued_datetime, :datetime
    add_column :bonds, :status_datetime, :datetime
    AddTimezoneToTables::Bond.reset_column_information
    AddTimezoneToTables::Bond.all.each do |b|
      b.issued_datetime = get_time(b.issued_date, b.issued_time)
      b.status_datetime = get_time(b.status_date, b.status_time)
      b.save
    end
    remove_column :bonds, :issued_date
    remove_column :bonds, :issued_time
    remove_column :bonds, :status_date
    remove_column :bonds, :status_time
    
    add_column :bookings, :booking_datetime, :datetime
    add_column :bookings, :release_datetime, :datetime
    AddTimezoneToTables::Booking.reset_column_information
    AddTimezoneToTables::Booking.all.each do |b|
      b.booking_datetime = get_time(b.booking_date, b.booking_time)
      b.release_datetime = get_time(b.release_date, b.release_time)
      b.save
    end
    remove_column :bookings, :booking_date
    remove_column :bookings, :booking_time
    remove_column :bookings, :release_date
    remove_column :bookings, :release_time
    
    add_column :bugs, :reported_datetime, :datetime
    add_column :bugs, :resolution_datetime, :datetime
    AddTimezoneToTables::Bug.reset_column_information
    AddTimezoneToTables::Bug.all.each do |b|
      b.reported_datetime = get_time(b.reported_date, b.reported_time)
      b.resolution_datetime = get_time(b.resolution_date, b.resolution_time)
      b.save
    end
    remove_column :bugs, :reported_date
    remove_column :bugs, :reported_time
    remove_column :bugs, :resolution_date
    remove_column :bugs, :resolution_time
    
    add_column :calls, :call_datetime, :datetime
    add_column :calls, :dispatch_datetime, :datetime
    add_column :calls, :arrival_datetime, :datetime
    add_column :calls, :concluded_datetime, :datetime
    AddTimezoneToTables::Call.reset_column_information
    AddTimezoneToTables::Call.all.each do |c|
      c.call_datetime = get_time(c.call_date, c.call_time)
      c.dispatch_datetime = get_time(c.dispatch_date, c.dispatch_time)
      c.arrival_datetime = get_time(c.arrival_date, c.arrival_time)
      c.concluded_datetime = get_time(c.concluded_date, c.concluded_time)
      c.save
    end
    remove_column :calls, :call_date
    remove_column :calls, :call_time
    remove_column :calls, :dispatch_date
    remove_column :calls, :dispatch_time
    remove_column :calls, :arrival_date
    remove_column :calls, :arrival_time
    remove_column :calls, :concluded_date
    remove_column :calls, :concluded_time
    
    add_column :citations, :citation_datetime, :datetime
    AddTimezoneToTables::Citation.reset_column_information
    AddTimezoneToTables::Citation.all.each do |c|
      c.citation_datetime = get_time(c.citation_date, c.citation_time)
      c.save
    end
    remove_column :citations, :citation_date
    remove_column :citations, :citation_time
    
    add_column :docs, :transfer_datetime, :datetime
    AddTimezoneToTables::Doc.reset_column_information
    AddTimezoneToTables::Doc.all.each do |d|
      d.transfer_datetime = get_time(d.transfer_date, d.transfer_time)
      d.save
    end
    remove_column :docs, :transfer_date
    remove_column :docs, :transfer_time
    
    add_column :evidence_locations, :storage_datetime, :datetime
    AddTimezoneToTables::EvidenceLocation.reset_column_information
    AddTimezoneToTables::EvidenceLocation.all.each do |e|
      e.storage_datetime = get_time(e.storage_date, e.storage_time)
      e.save
    end
    remove_column :evidence_locations, :storage_date
    remove_column :evidence_locations, :storage_time
    
    add_column :holds, :hold_datetime, :datetime
    add_column :holds, :cleared_datetime, :datetime
    AddTimezoneToTables::Hold.reset_column_information
    AddTimezoneToTables::Hold.all.each do |h|
      h.hold_datetime = get_time(h.hold_date, h.hold_time)
      h.cleared_datetime = get_time(h.cleared_date, h.cleared_time)
      h.save
    end
    remove_column :holds, :hold_date
    remove_column :holds, :hold_time
    remove_column :holds, :cleared_date
    remove_column :holds, :cleared_time
    
    add_column :invest_crimes, :occurred_datetime, :datetime
    AddTimezoneToTables::InvestCrime.reset_column_information
    AddTimezoneToTables::InvestCrime.all.each do |c|
      c.occurred_datetime = get_time(c.occurred_date, c.occurred_time)
      c.save
    end
    remove_column :invest_crimes, :occurred_date
    remove_column :invest_crimes, :occurred_time
    
    add_column :invest_reports, :report_datetime, :datetime
    AddTimezoneToTables::InvestReport.reset_column_information
    AddTimezoneToTables::InvestReport.all.each do |r|
      r.report_datetime = get_time(r.report_date, r.report_time)
      r.save
    end
    remove_column :invest_reports, :report_date
    remove_column :invest_reports, :report_time
    
    add_column :investigations, :investigation_datetime, :datetime
    AddTimezoneToTables::Investigation.reset_column_information
    AddTimezoneToTables::Investigation.all.each do |i|
      i.investigation_datetime = get_time(i.investigation_date, i.investigation_time)
      i.save
    end
    remove_column :investigations, :investigation_date
    remove_column :investigations, :investigation_time
    
    add_column :medicals, :screen_datetime, :datetime
    AddTimezoneToTables::Medical.reset_column_information
    AddTimezoneToTables::Medical.all.each do |m|
      m.screen_datetime = get_time(m.screen_date, m.screen_time)
      m.save
    end
    remove_column :medicals, :screen_date
    remove_column :medicals, :screen_time
    
    add_column :offenses, :offense_datetime, :datetime
    AddTimezoneToTables::Offense.reset_column_information
    AddTimezoneToTables::Offense.all.each do |o|
      o.offense_datetime = get_time(o.offense_date, o.offense_time)
      o.save
    end
    remove_column :offenses, :offense_date
    remove_column :offenses, :offense_time
    
    add_column :papers, :served_datetime, :datetime
    AddTimezoneToTables::Paper.reset_column_information
    AddTimezoneToTables::Paper.all.each do |p|
      p.served_datetime = get_time(p.served_date, p.served_time)
      p.save
    end
    remove_column :papers, :served_date
    remove_column :papers, :served_time
  
    add_column :patrols, :patrol_started_datetime, :datetime
    add_column :patrols, :patrol_ended_datetime, :datetime
    AddTimezoneToTables::Patrol.reset_column_information
    AddTimezoneToTables::Patrol.all.each do |p|
      p.patrol_started_datetime = get_time(p.patrol_started_date, p.patrol_started_time)
      p.patrol_ended_datetime = get_time(p.patrol_ended_date, p.patrol_ended_time)
      p.save
    end
    remove_column :patrols, :patrol_started_date
    remove_column :patrols, :patrol_started_time
    remove_column :patrols, :patrol_ended_date
    remove_column :patrols, :patrol_ended_time
    
    add_column :transfers, :transfer_datetime, :datetime
    AddTimezoneToTables::Transfer.reset_column_information
    AddTimezoneToTables::Transfer.all.each do |t|
      t.transfer_datetime = get_time(t.transfer_date, t.transfer_time)
      t.save
    end
    remove_column :transfers, :transfer_date
    remove_column :transfers, :transfer_time
    
    add_column :transports, :begin_datetime, :datetime
    add_column :transports, :end_datetime, :datetime
    AddTimezoneToTables::Transport.reset_column_information
    AddTimezoneToTables::Transport.all.each do |t|
      t.begin_datetime = get_time(t.begin_date, t.begin_time)
      t.end_datetime = get_time(t.end_date, t.end_time)
      t.save
    end
    remove_column :transports, :begin_date
    remove_column :transports, :begin_time
    remove_column :transports, :end_date
    remove_column :transports, :end_time
  end
  
  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
