class FixColorschemes < ActiveRecord::Migration
  class SiteConfLegacy
     #include ActiveAttr::Attributes
     #include ActiveAttr::AttributeDefaults
     #include ActiveAttr::TypecastedAttributes
     #include ActiveAttr::MassAssignment
     include ActiveAttr::Model

     attribute :agency, :type => String, :default => "FiveO"
     attribute :agency_short, :type => String, :default => "FiveO"
     attribute :motto, :type => String, :default => "Law Enforcement Software"
     attribute :logo, :type => String, :default => "/images/fiveo_original.png"
     attribute :ceo, :type => String, :default => "Michael Merten"
     attribute :ceo_title, :type => String, :default => "Midlake Technical Services"
     attribute :ceo_img, :type => String, :default => "/images/ceo_original.png"
     attribute :district, :type => Integer, :default => 0
     attribute :zone, :type => Integer, :default => 0
     attribute :ward, :type => Integer, :default => 0
     attribute :local_zips, :type => String
     attribute :local_agencies, :type => String
     attribute :show_history, :type => Boolean, :default => false
     attribute :bug_admin_id, :type => Integer, :default => 1
     attribute :site_email, :type => String, :default => "monitor@midlaketech.com"
     attribute :message_retention, :type => Integer, :default => 90
     attribute :activity_retention, :type => Integer, :default => 90
     attribute :header_title, :type => String, :default => "FiveO"
     attribute :header_subtitle1, :type => String, :default => "Midlake Technical Services"
     attribute :header_subtitle2, :type => String, :default => "Michael Merten, Owner"
     attribute :header_subtitle3, :type => String, :default => "http://midlaketech.com - mikemerten@suddenlink.net"
     attribute :header_left_img, :type => String, :default => "/images/header_original.png"
     attribute :header_right_img, :type => String, :default => "/images/header_original.png"
     attribute :footer_text, :type => String, :default => "Law Enforcement Software"
     attribute :pdf_header_bg, :type => String, :default => 'ddddff'
     attribute :pdf_header_txt, :type => String, :default => '000099'
     attribute :pdf_section_bg, :type => String, :default => 'ddddff'
     attribute :pdf_section_txt, :type => String, :default => '000099'
     attribute :pdf_em_bg, :type => String, :default => 'ffdddd'
     attribute :pdf_em_txt, :type => String, :default => '990000'
     attribute :pdf_row_odd, :type => String, :default => 'ffffff'
     attribute :pdf_row_even, :type => String, :default => 'eeeeee'
     attribute :session_life, :type => Integer, :default => 720
     attribute :session_active, :type => Integer, :default => 15
     attribute :session_max, :type => Integer, :default => 960
     attribute :allow_user_photos, :type => Boolean, :default => true
     attribute :user_update_contact, :type => Boolean, :default => true
     attribute :dispatch_shift1_start, :type => String, :default => '05:30'
     attribute :dispatch_shift1_stop, :type => String, :default => '17:30'
     attribute :dispatch_shift2_start, :type => String, :default => '17:30'
     attribute :dispatch_shift2_stop, :type => String, :default => '05:30'
     attribute :dispatch_shift3_start, :type => String
     attribute :dispatch_shift3_stop, :type => String
     attribute :goodtime_by_default, :type => Boolean, :default => true
     attribute :goodtime_hours_per_day_local, :type => Integer, :default => 24
     attribute :jail1_enabled, :type => Boolean, :default => false
     attribute :jail1_name, :type => String
     attribute :jail1_short_name, :type => String
     attribute :jail1_male, :type => Boolean, :default => false
     attribute :jail1_female, :type => Boolean, :default => false
     attribute :jail1_gt_by_default, :type => Boolean, :default => false
     attribute :jail1_gt_hours_per_day, :type => Integer, :default => 0
     attribute :jail2_enabled, :type => Boolean, :default => false
     attribute :jail2_name, :type => String
     attribute :jail2_short_name, :type => String
     attribute :jail2_male, :type => Boolean, :default => false
     attribute :jail2_female, :type => Boolean, :default => false
     attribute :jail2_gt_by_default, :type => Boolean, :default => false
     attribute :jail2_gt_hours_per_day, :type => Integer, :default => 0
     attribute :jail3_enabled, :type => Boolean, :default => false
     attribute :jail3_name, :type => String
     attribute :jail3_short_name, :type => String
     attribute :jail3_male, :type => Boolean, :default => false
     attribute :jail3_female, :type => Boolean, :default => false
     attribute :jail3_gt_by_default, :type => Boolean, :default => false
     attribute :jail3_gt_hours_per_day, :type => Integer, :default => 0
     attribute :probation_fund1, :type => String, :default => "Fines"
     attribute :probation_fund2, :type => String, :default => "Costs"
     attribute :probation_fund3, :type => String, :default => "Probation Fees"
     attribute :probation_fund4, :type => String, :default => "IDF"
     attribute :probation_fund5, :type => String, :default => "DARE"
     attribute :probation_fund6, :type => String, :default => "Restitution"
     attribute :probation_fund7, :type => String, :default => "DA Fees"
     attribute :probation_fund8, :type => String
     attribute :probation_fund9, :type => String
     attribute :mileage_fee, :type => Float, :default => 0.88
     attribute :noservice_fee, :type => Float, :default => 5.00
     attribute :paper_pay_type1, :type => String, :default => "Cash"
     attribute :paper_pay_type2, :type => String, :default => "Check"
     attribute :paper_pay_type3, :type => String, :default => "Money Order"
     attribute :paper_pay_type4, :type => String, :default => "Credit Card"
     attribute :paper_pay_type5, :type => String
     attribute :colorscheme, :type => String, :default => "1"
  end
  def self.up
    if table_exists?(:colorschemes)
      drop_table :colorschemes

      siteconf = FixColorschemes::SiteConfLegacy.new
      conf_file = "#{Rails.root}/public/system/site_configuration.yml"
      if File.exists?(conf_file)
        conf = YAML.load_file(conf_file)
        ckeys = conf.keys
        if ckeys[0].is_a?(Symbol)
          obsolete = ckeys - siteconf.attributes.keys.collect{|a| a.to_sym}
        else
          obsolete = ckeys - siteconf.attributes.keys
        end
        obsolete.each do |key|
         conf.delete(key)
        end
        siteconf.attributes = conf
      end
      # update SiteConfiguration
      scheme_name = ""
      case siteconf.colorscheme.to_i
      when 1
        scheme_name = "blue_brown"
      when 2
        scheme_name = "green_gray"
      when 3
        scheme_name = "gray_green"
      when 4
        scheme_name = "yellow_gray"
      when 5
        scheme_name = "brown_blue"
      when 6
        scheme_name = "blue_gray"
      when 7
        scheme_name = "green_brown"
      else
        scheme_name = "blue_brown"
      end
      siteconf.colorscheme = scheme_name
      if conf = File.new("#{Rails.root}/public/system/site_configuration.yml","w")
        conf.puts(siteconf.attributes.to_yaml)
        conf.close
      end
    end
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration
  end
end
