class BreakReportCrimeLink < ActiveRecord::Migration
  def up
    remove_column :invest_reports, :invest_crime_id
  end

  def down
    add_column :invest_reports, :invest_crime_id, :integer
  end
end
