class AddCommissaryToSite < ActiveRecord::Migration
  def up
    add_column :site_configurations, :enable_internal_commissary, :boolean, :default => false
    add_column :site_configurations, :enable_cashless_systems, :boolean, :default => false
    add_column :site_configurations, :csi_ftp_user, :string
    add_column :site_configurations, :csi_ftp_password, :string
    add_column :site_configurations, :csi_ftp_url, :string
    add_column :site_configurations, :csi_ftp_filename, :string
    add_column :site_configurations, :csi_provide_ssn, :boolean, :default => false
    add_column :site_configurations, :csi_provide_dob, :boolean, :default => false
    add_column :site_configurations, :csi_provide_race, :boolean, :default => false
    add_column :site_configurations, :csi_provide_sex, :boolean, :default => false
    add_column :site_configurations, :csi_provide_addr, :boolean, :default => false
  end
  
  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
