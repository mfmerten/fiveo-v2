class RemoveExceptionNotificationConfig < ActiveRecord::Migration
  def up
    remove_column :site_configurations, :enable_exception_notifications
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
