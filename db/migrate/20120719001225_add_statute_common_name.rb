class AddStatuteCommonName < ActiveRecord::Migration
  def up
    add_column :statutes, :common_name, :string
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
