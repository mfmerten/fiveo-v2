class AddAdultAgeConfig < ActiveRecord::Migration
  def up
    add_column :site_configurations, :adult_age, :integer, :default => 17
  end

  def down
    remove_column :site_configurations, :adult_age
  end
end
