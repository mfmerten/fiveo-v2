class FixTableDefaultsAndIndexes < ActiveRecord::Migration
  class Bond < ActiveRecord::Base
    belongs_to :bondsman, :class_name => 'FixTableDefaultsAndIndexes::Contact', :foreign_key => 'bondsman_id'
    belongs_to :bonding_company, :class_name => 'FixTableDefaultsAndIndexes::Contact', :foreign_key => 'bonding_company_id'
  end
  class Contact < ActiveRecord::Base
    def name
      if business?
        fullname
      else
        n = Namae::Name.parse(fullname)
        n.display_order
      end
    end
    def address_line1
      street || ''
    end

    def address_line2
      street2 || ''
    end

    def address_line3
      txt = ""
      # don't return anything if only state exists
      if city? || zip?
        if city?
          txt << city
        end
        if state?
          unless txt.empty?
            txt << ", "
          end
          txt << state
        end
        if zip?
          unless txt.empty?
            txt << " "
          end
          txt << zip
        end
      end
      txt
    end
  end
  class PaperType < ActiveRecord::Base
  end
  class Paper < ActiveRecord::Base
    belongs_to :xpaper_type, :class_name => 'FixTableDefaultsAndIndexes::PaperType', :foreign_key => 'paper_type_id'
  end
  
  def up
    change_column_default :absents, :created_by, 1
    change_column_default :absents, :updated_by, 1
    
    unless column_exists?(:bonds, :bondsman_name)
      # fix bondsman and bonding_company to store strings instead of ids
      add_column :bonds, :bondsman_name, :string
      add_column :bonds, :bond_company, :string
      add_column :bonds, :bondsman_address1, :string
      add_column :bonds, :bondsman_address2, :string
      add_column :bonds, :bondsman_address3, :string
      FixTableDefaultsAndIndexes::Bond.reset_column_information
      FixTableDefaultsAndIndexes::Bond.all.each do |b|
        unless b.bondsman.nil?
          b.bondsman_name = b.bondsman.name
          b.bondsman_address1 = b.bondsman.address_line1
          b.bondsman_address2 = b.bondsman.address_line2
          b.bondsman_address3 = b.bondsman.address_line3
        end
        unless b.bonding_company.nil?
          b.bond_company = b.bonding_company.name
        end
        if b.changed?
          b.save(:validate => false)
        end
      end
      remove_column :bonds, :bondsman_id
      remove_column :bonds, :bonding_company_id
    end
  
    change_column_default :bonds, :created_by, 1
    change_column_default :bonds, :updated_by, 1
  
    change_column_default :bookings, :created_by, 1
    change_column_default :bookings, :updated_by, 1
    
    change_column_default :commissaries, :created_by, 1
    change_column_default :commissaries, :updated_by, 1
    
    change_column_default :events, :owned_by, 1
  
    change_column_default :forbids, :created_by, 1
    change_column_default :forbids, :updated_by, 1
    
    change_column_default :holds, :created_by, 1
    change_column_default :holds, :updated_by, 1
    
    change_column_default :invest_crimes, :owned_by, 1
    
    change_column_default :invest_criminals, :created_by, 1
    change_column_default :invest_criminals, :updated_by, 1
    change_column_default :invest_criminals, :owned_by, 1
    
    remove_column :invest_photos, :taken_by_id
    change_column_default :invest_photos, :owned_by, 1
    
    change_column_default :invest_reports, :owned_by, 1
    
    change_column_default :invest_vehicles, :owned_by, 1
    
    change_column_default :investigations, :owned_by, 1
    
    change_column_default :medicals, :created_by, 1
    change_column_default :medicals, :updated_by, 1
  
    change_column_default :offense_photos, :created_by, 1
    change_column_default :offense_photos, :updated_by, 1
    
    # diconnect paper type from papers
    remove_column :paper_types, :disabled
    add_column :papers, :paper_type, :string
    FixTableDefaultsAndIndexes::Paper.reset_column_information
    FixTableDefaultsAndIndexes::Paper.all.each do |p|
      unless p.xpaper_type.nil?
        p.update_attribute(:paper_type, p.xpaper_type.name)
      end
    end
    remove_column :papers, :paper_type_id
    
    add_index :statutes, :common_name
    
    change_column_default :transports, :created_by, 1
    change_column_default :transports, :updated_by, 1
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
