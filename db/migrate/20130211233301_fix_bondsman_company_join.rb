class FixBondsmanCompanyJoin < ActiveRecord::Migration
  class Contact < ActiveRecord::Base
    has_and_belongs_to_many :bonding_companies, :class_name => 'FixBondsmanCompanyJoin::Contact', :foreign_key => 'bondsman_id', :association_foreign_key => 'company_id', :join_table => 'bonding_companies'
    has_and_belongs_to_many :bondsmen, :class_name => 'FixBondsmanCompanyJoin::Contact', :foreign_key => 'company_id', :association_foreign_key => 'bondsman_id', :join_table => 'bonding_companies'
    has_many :bondsman_companies, :class_name => 'FixBondsmanCompanyJoin::ContactBondsmanCompany', :dependent => :destroy
    has_many :xbondsmen, :through => :bondsman_companies, :source => :bondsman
    has_many :xbond_companies, :through => :bondsman_companies, :source => :company
  end
  class ContactBondsmanCompany < ActiveRecord::Base
    belongs_to :contact, :class_name => 'FixBondsmanCompanyJoin::Contact'
    belongs_to :bondsman, :class_name => 'FixBondsmanCompanyJoin::Contact', :foreign_key => 'bondsman_id'
    belongs_to :company, :class_name => 'FixBondsmanCompanyJoin::Contact', :foreign_key => 'company_id'
  end
  
  def up
    create_table :contact_bondsman_companies, :force => true do |t|
      t.integer :contact_id
      t.integer :bondsman_id
      t.integer :company_id
      t.timestamps
    end
    add_index :contact_bondsman_companies, :contact_id
    add_index :contact_bondsman_companies, :bondsman_id
    add_index :contact_bondsman_companies, :company_id
    
    FixBondsmanCompanyJoin::Contact.all.each do |c|
      c.xbondsman_ids = c.bondsman_ids
      c.xbond_company_ids = c.bonding_company_ids
    end
    
    drop_table :bonding_companies
  end

  def down
    create_table :bonding_companies, :id => false, :force => true do |t|
      t.integer :bondsman_id
      t.integer :company_id
    end
    add_index :bonding_companies, :bondsman_id
    add_index :bonding_companies, :company_id
    
    FixBondsmanCompanyJoin::Contact.all.each do |c|
      c.bondsman_ids = c.xbondsman_ids
      c.bonding_company_ids = c.xbond_company_ids
    end
    
    drop_table :contact_bondsman_companies
  end
end
