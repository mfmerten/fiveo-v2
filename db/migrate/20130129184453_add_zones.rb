class AddZones < ActiveRecord::Migration
  def up
    add_column :arrests, :zone, :integer
    add_column :calls, :zone, :integer
  end

  def down
    remove_column :arrests, :zone
    remove_column :calls, :zone
  end
end
