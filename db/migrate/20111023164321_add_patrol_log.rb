class AddPatrolLog < ActiveRecord::Migration
  def up
    create_table :patrols, :force => true do |t|
      t.integer :officer_id
      t.string :officer
      t.string :officer_unit
      t.string :officer_badge
      t.date :patrol_started_date
      t.string :patrol_started_time
      t.date :patrol_ended_date
      t.string :patrol_ended_time
      t.integer :beginning_odometer
      t.integer :ending_odometer
      t.integer :shift, :default => 0
      t.string :supervisor
      t.string :shift_start
      t.string :shift_stop
      t.integer :roads_patrolled, :default => 0
      t.integer :complaints, :default => 0
      t.integer :traffic_stops, :default => 0
      t.integer :citations_issued, :default => 0
      t.integer :papers_served, :default => 0
      t.integer :narcotics_arrests, :default => 0
      t.integer :other_arrests, :default => 0
      t.integer :public_assists, :default => 0
      t.integer :warrants_served, :default => 0
      t.integer :funeral_escorts, :default => 0
      t.text :remarks
      t.integer :created_by, :default => 1
      t.integer :updated_by, :default => 1
      t.timestamps
    end
    add_index :patrols, :created_by
    add_index :patrols, :updated_by
  end

  def down
    drop_table :patrols
  end
end
