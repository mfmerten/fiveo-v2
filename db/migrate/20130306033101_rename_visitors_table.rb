class RenameVisitorsTable < ActiveRecord::Migration
  def up
    rename_table :booking_visitors, :visitors
  end

  def down
    rename_table :visitors, :booking_visitors
  end
end
