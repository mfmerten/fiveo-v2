class AddMessageRetentionPreferences < ActiveRecord::Migration
  def up
    rename_column :site_configurations, :message_retention, :message_log_retention
    add_column :site_configurations, :message_retention, :integer, :default => 0
    add_column :users, :message_retention, :integer, :default => 0
    
    # found possible old entries that needs removing
    if column_exists?(:site_configurations, :goodtime_hours_per_day_local)
      remove_column :site_configurations, :goodtime_hours_per_day_local
    end
    if column_exists?(:site_configurations, :goodtime_by_default)
      remove_column :site_configurations, :goodtime_by_default
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
