class RemoveDocketIdFromSentence < ActiveRecord::Migration
  def up
    remove_column :sentences, :docket_id
  end
  
  def down
    add_column :sentences, :docket_id, :integer
    add_index :sentences, :docket_id
    
    Sentence.find_each do |s|
      if !s.court.nil? && s.docket_id.blank? && !s.court.docket_id.blank?
        s.update_attribute(:docket_id, s.court.docket_id)
      end
    end
    Sentence.find_each do |s|
      if s.docket_id.blank?
        s.destroy
      end
    end
    
    change_column_nul :sentences, :docket_id, false
    add_foreign_key :sentences, :dockets
  end
end
