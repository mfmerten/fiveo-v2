class FixAbsentBookingJoin < ActiveRecord::Migration
  class Absent < ActiveRecord::Base
    has_and_belongs_to_many :inmates, :class_name => 'FixAbsentBookingJoin::Booking', :join_table => 'absents_bookings', :foreign_key => 'absent_id', :association_foreign_key => 'booking_id'
    has_many :absent_prisoners, :class_name => 'FixAbsentBookingJoin::AbsentPrisoner'
    has_many :bookings, :class_name => 'FixAbsentBookingJoin::Booking', :through => :absent_prisoners
  end
  
  class Booking < ActiveRecord::Base
    has_many :absent_prisoners, :class_name => 'FixAbsentBookingJoin::AbsentPrisoner'
    has_many :absents, :class_name => 'FixAbsentBookingJoin::Absent', :through => :absent_prisoners
  end
  
  class AbsentPrisoner < ActiveRecord::Base
    belongs_to :absent, :class_name => 'FixAbsentBookingJoin::Absent'
    belongs_to :booking, :class_name => 'FixAbsentBookingJoin::Booking'
  end
  
  def up
    create_table :absent_prisoners, :force => true do |t|
      t.integer :absent_id
      t.integer :booking_id
      t.timestamps
    end
    add_index :absent_prisoners, :absent_id
    add_index :absent_prisoners, :booking_id
  
    FixAbsentBookingJoin::Absent.all.each do |a|
      a.booking_ids = a.inmate_ids
    end
    
    drop_table :absents_bookings
  end

  def down
    create_table :absents_bookings, :id => false, :force => true do |t|
      t.integer :absent_id
      t.integer :booking_id
    end
    add_index :absents_bookings, :absent_id
    add_index :absents_bookings, :booking_id
    
    FixAbsentBookingJoin::Absent.all.each do |a|
      a.inmate_ids = a.booking_ids
    end
    
    drop_table :absent_prisoners
  end
end
