# Rails3 - fix improperly imported offense details
class FixOffenseDetails < ActiveRecord::Migration
  class Offense < ActiveRecord::Base
  end
  
  def up
    FixOffenseDetails::Offense.all.each do |o|
      unless o.details.blank?
        o.update_attribute(:details, o.details.sub(/^\s+/, "   ").gsub(/[\n\r]/,"\n").gsub(/<br ?\/?>\s+/,"\n   "))
      end
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
