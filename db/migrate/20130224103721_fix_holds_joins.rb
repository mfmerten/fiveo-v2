class FixHoldsJoins < ActiveRecord::Migration
  
  class Hold < ActiveRecord::Base
    has_and_belongs_to_many :blockers, :class_name => 'FixHoldsJoins::Hold', :join_table => 'hold_blocks', :foreign_key => 'blocker_id', :association_foreign_key => 'hold_id'
    has_and_belongs_to_many :blocks, :class_name => 'FixHoldsJoins::Hold', :join_table => 'hold_blocks', :foreign_key => 'hold_id', :association_foreign_key => 'blocker_id'
    has_many :hold_blockers, :class_name => 'FixHoldsJoins::HoldBlocker'
    has_many :hold_blocks, :class_name => 'FixHoldsJoins::HoldBlocker', :foreign_key => 'blocker_id'
    has_many :xblockers, :through => :hold_blockers, :source => :blocker
    has_many :xblocks, :through => :hold_blocks, :source => :hold
  end
  
  class HoldBlocker < ActiveRecord::Base
    belongs_to :blocker, :class_name => 'FixHoldsJoins::Hold', :foreign_key => 'blocker_id'
    belongs_to :hold, :class_name => 'FixHoldsJoins::Hold'
  end
  
  def up
    create_table :hold_blockers, :force => true do |t|
      t.integer :blocker_id
      t.integer :hold_id
      t.timestamps
    end
    add_index :hold_blockers, :blocker_id
    add_index :hold_blockers, :hold_id
    
    FixHoldsJoins::Hold.reset_column_information
    FixHoldsJoins::Hold.all.each do |h|
      # only want to do one side of the join here
      h.xblock_ids = h.block_ids
    end
    
    drop_table :hold_blocks
  end

  def down
    create_table :hold_blocks, :id => false, :force => true do |t|
      t.integer :hold_id
      t.integer :blocker_id
    end
    add_index :hold_blocks, :hold_id
    add_index :hold_blocks, :blocker_id
    
    FixHoldsJoins::Hold.reset_column_information
    FixHoldsJoins::Hold.all.each do |h|
      h.block_ids = h.xblock_ids
    end
    
    drop_table :hold_blockers
  end
end
