class RemoveCourtCostType < ActiveRecord::Migration
  def up
    remove_column :sentences, :court_cost_type_id
  end

  def down
    add_column :sentences, :court_cost_type_id, :integer
    add_index :sentences, :court_cost_type_id
  end
end
