# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Daley', :city => cities.first)

# create initial site_store records
conf = Site.new
conf.attributes.each do |name,value|
  SiteStore.create(setting_name: name, setting_value: value)
end

# create system directory and copy default images if necessary
unless File.directory?("public/system/default_images")
  `mkdir -p public/system/default_images`
end
`cp config/deploy/* public/system/default_images/`

# create admin user and group
user =  User.new(login: "admin", password: "admin", password_confirmation: "admin")
user.id = 1
user.save!
group = Group.create(name: 'Admin', description: 'Administrator Group.  WARNING: grant this with caution!', permissions: [Perm.get(:admin)], created_by: 1, updated_by: 1)
user.groups << group

# add blank initial record for SiteImg
SiteImage.create

# load announcements
otypes = YAML.load_file("#{Rails.root}/db/initial_data/announcements.yml")
otypes.each do |o|
  Announcement.create(subject: o[:subject], body: o[:body], unignorable: o[:unignorable], created_by: 1, updated_by: 1)
end

# load holidays
otypes = YAML.load_file("#{Rails.root}/db/initial_data/holidays.yml")
otypes.each do |o|
  Event.create(name: o[:name], start_date: Date.parse(o[:start_date]), end_date: nil, created_by: 1, updated_by: 1)
end
