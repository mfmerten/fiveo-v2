# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150810131912) do

  create_table "absent_prisoners", force: :cascade do |t|
    t.integer  "absent_id",  null: false
    t.integer  "booking_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "absent_prisoners", ["absent_id"], name: "index_absent_prisoners_on_absent_id"
  add_index "absent_prisoners", ["booking_id"], name: "index_absent_prisoners_on_booking_id"

  create_table "absents", force: :cascade do |t|
    t.string   "escort_officer",       limit: 255, default: "", null: false
    t.string   "escort_officer_unit",  limit: 255, default: "", null: false
    t.integer  "created_by",                       default: 1
    t.integer  "updated_by",                       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "escort_officer_badge", limit: 255, default: "", null: false
    t.integer  "jail_id",                                       null: false
    t.datetime "leave_datetime"
    t.datetime "return_datetime"
    t.string   "absent_type",          limit: 255, default: "", null: false
  end

  add_index "absents", ["created_by"], name: "index_absents_on_created_by"
  add_index "absents", ["jail_id"], name: "index_absents_on_jail_id"
  add_index "absents", ["return_datetime"], name: "index_absents_on_return_datetime"
  add_index "absents", ["updated_by"], name: "index_absents_on_updated_by"

  create_table "activities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "section",     limit: 255, default: "", null: false
    t.string   "description", limit: 255, default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username",    limit: 255, default: "", null: false
  end

  add_index "activities", ["user_id"], name: "index_activities_on_user_id"

  create_table "announcement_ignores", force: :cascade do |t|
    t.integer  "announcement_id", null: false
    t.integer  "user_id",         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "announcement_ignores", ["announcement_id"], name: "index_announcement_ignores_on_announcement_id"
  add_index "announcement_ignores", ["user_id"], name: "index_announcement_ignores_on_user_id"

  create_table "announcements", force: :cascade do |t|
    t.string   "subject",     limit: 255, default: "",    null: false
    t.text     "body",                    default: "",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "created_by",              default: 1
    t.integer  "updated_by",              default: 1
    t.boolean  "unignorable",             default: false, null: false
  end

  add_index "announcements", ["created_by"], name: "index_announcements_on_created_by"
  add_index "announcements", ["updated_at"], name: "index_announcements_on_updated_at"
  add_index "announcements", ["updated_by"], name: "index_announcements_on_updated_by"

  create_table "arrest_officers", force: :cascade do |t|
    t.integer  "arrest_id",                              null: false
    t.string   "officer",       limit: 255, default: "", null: false
    t.string   "officer_unit",  limit: 255, default: "", null: false
    t.string   "officer_badge", limit: 255, default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "arrest_officers", ["arrest_id"], name: "index_arrest_officers_on_arrest_id"

  create_table "arrests", force: :cascade do |t|
    t.integer  "person_id",                                                                   null: false
    t.string   "case_no",                limit: 255,                          default: "",    null: false
    t.string   "agency",                 limit: 255,                          default: "",    null: false
    t.string   "ward",                   limit: 255,                          default: "",    null: false
    t.string   "district",               limit: 255,                          default: "",    null: false
    t.boolean  "victim_notify",                                               default: false, null: false
    t.string   "dwi_test_officer",       limit: 255,                          default: "",    null: false
    t.string   "dwi_test_officer_unit",  limit: 255,                          default: "",    null: false
    t.string   "dwi_test_results",       limit: 255,                          default: "",    null: false
    t.string   "attorney",               limit: 255,                          default: "",    null: false
    t.string   "condition_of_bond",      limit: 255,                          default: "",    null: false
    t.text     "remarks",                                                     default: "",    null: false
    t.integer  "created_by",                                                  default: 1
    t.integer  "updated_by",                                                  default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "bond_amt",                           precision: 12, scale: 2, default: 0.0,   null: false
    t.boolean  "bondable",                                                    default: false, null: false
    t.string   "dwi_test_officer_badge", limit: 255,                          default: "",    null: false
    t.boolean  "legacy",                                                      default: false, null: false
    t.string   "atn",                    limit: 255,                          default: "",    null: false
    t.datetime "arrest_datetime"
    t.datetime "hour_72_datetime"
    t.string   "hour_72_judge",          limit: 255,                          default: "",    null: false
    t.string   "zone",                   limit: 255,                          default: "",    null: false
    t.integer  "warrants_count",                                              default: 0,     null: false
    t.integer  "arrest_officers_count",                                       default: 0,     null: false
    t.integer  "disposition_id"
  end

  add_index "arrests", ["atn"], name: "index_arrests_on_atn"
  add_index "arrests", ["case_no"], name: "index_arrests_on_case_no"
  add_index "arrests", ["created_by"], name: "index_arrests_on_created_by"
  add_index "arrests", ["disposition_id"], name: "index_arrests_on_disposition_id"
  add_index "arrests", ["person_id"], name: "index_arrests_on_person_id"
  add_index "arrests", ["updated_by"], name: "index_arrests_on_updated_by"

  create_table "bonds", force: :cascade do |t|
    t.integer  "surety_id"
    t.integer  "person_id",                                                              null: false
    t.integer  "arrest_id"
    t.decimal  "bond_amt",                      precision: 12, scale: 2, default: 0.0,   null: false
    t.text     "remarks",                                                default: "",    null: false
    t.string   "power",             limit: 255,                          default: "",    null: false
    t.string   "bond_no",           limit: 255,                          default: "",    null: false
    t.integer  "created_by",                                             default: 1
    t.integer  "updated_by",                                             default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "issued_datetime"
    t.datetime "status_datetime"
    t.boolean  "final",                                                  default: false, null: false
    t.string   "status",            limit: 255,                          default: "",    null: false
    t.string   "bond_type",         limit: 255,                          default: "",    null: false
    t.string   "bondsman_name",     limit: 255,                          default: "",    null: false
    t.string   "bond_company",      limit: 255,                          default: "",    null: false
    t.string   "bondsman_address1", limit: 255,                          default: "",    null: false
    t.string   "bondsman_address2", limit: 255,                          default: "",    null: false
    t.string   "bondsman_address3", limit: 255,                          default: "",    null: false
  end

  add_index "bonds", ["arrest_id"], name: "index_bonds_on_arrest_id"
  add_index "bonds", ["created_by"], name: "index_bonds_on_created_by"
  add_index "bonds", ["person_id"], name: "index_bonds_on_person_id"
  add_index "bonds", ["surety_id"], name: "index_bonds_on_surety_id"
  add_index "bonds", ["updated_by"], name: "index_bonds_on_updated_by"

  create_table "booking_logs", force: :cascade do |t|
    t.integer  "booking_id",                                   null: false
    t.datetime "start_datetime"
    t.datetime "stop_datetime"
    t.string   "start_officer",       limit: 255, default: "", null: false
    t.string   "start_officer_badge", limit: 255, default: "", null: false
    t.string   "start_officer_unit",  limit: 255, default: "", null: false
    t.string   "stop_officer",        limit: 255, default: "", null: false
    t.string   "stop_officer_badge",  limit: 255, default: "", null: false
    t.string   "stop_officer_unit",   limit: 255, default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "booking_logs", ["booking_id"], name: "index_booking_logs_on_booking_id"

  create_table "booking_properties", force: :cascade do |t|
    t.integer  "booking_id",                           null: false
    t.integer  "quantity",                default: 1,  null: false
    t.string   "description", limit: 255, default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "booking_properties", ["booking_id"], name: "index_properties_on_booking_id"

  create_table "bookings", force: :cascade do |t|
    t.integer  "person_id",                                                                  null: false
    t.decimal  "cash_at_booking",                   precision: 12, scale: 2, default: 0.0,   null: false
    t.string   "cash_receipt",          limit: 255,                          default: "",    null: false
    t.string   "booking_officer",       limit: 255,                          default: "",    null: false
    t.string   "booking_officer_unit",  limit: 255,                          default: "",    null: false
    t.text     "remarks",                                                    default: "",    null: false
    t.string   "phone_called",          limit: 255,                          default: "",    null: false
    t.boolean  "intoxicated",                                                default: false, null: false
    t.date     "sched_release_date"
    t.boolean  "afis",                                                       default: false, null: false
    t.string   "release_officer",       limit: 255,                          default: "",    null: false
    t.string   "release_officer_unit",  limit: 255,                          default: "",    null: false
    t.integer  "created_by",                                                 default: 1
    t.integer  "updated_by",                                                 default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "booking_officer_badge", limit: 255,                          default: "",    null: false
    t.string   "release_officer_badge", limit: 255,                          default: "",    null: false
    t.boolean  "good_time",                                                  default: true,  null: false
    t.boolean  "disable_timers",                                             default: false, null: false
    t.string   "attorney",              limit: 255,                          default: "",    null: false
    t.boolean  "legacy",                                                     default: false, null: false
    t.integer  "jail_id",                                                                    null: false
    t.string   "cell",                  limit: 255,                          default: "",    null: false
    t.datetime "booking_datetime"
    t.datetime "release_datetime"
    t.string   "released_because",      limit: 255,                          default: "",    null: false
  end

  add_index "bookings", ["created_by"], name: "index_bookings_on_created_by"
  add_index "bookings", ["jail_id"], name: "index_bookings_on_jail_id"
  add_index "bookings", ["person_id"], name: "index_bookings_on_person_id"
  add_index "bookings", ["updated_by"], name: "index_bookings_on_updated_by"

  create_table "call_subjects", force: :cascade do |t|
    t.integer  "call_id",                               null: false
    t.string   "sort_name",    limit: 255, default: "", null: false
    t.string   "address1",     limit: 255, default: "", null: false
    t.string   "address2",     limit: 255, default: "", null: false
    t.string   "home_phone",   limit: 255, default: "", null: false
    t.string   "cell_phone",   limit: 255, default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "work_phone",   limit: 255, default: "", null: false
    t.string   "subject_type", limit: 255, default: "", null: false
  end

  add_index "call_subjects", ["call_id"], name: "index_call_subjects_on_call_id"
  add_index "call_subjects", ["sort_name"], name: "index_call_subjects_on_sort_name"

  create_table "call_units", force: :cascade do |t|
    t.integer  "call_id",                                null: false
    t.string   "officer_unit",  limit: 255, default: "", null: false
    t.string   "officer",       limit: 255, default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "officer_badge", limit: 255, default: "", null: false
  end

  add_index "call_units", ["call_id"], name: "index_call_units_on_call_id"

  create_table "calls", force: :cascade do |t|
    t.string   "case_no",             limit: 255, default: "",    null: false
    t.string   "received_by",         limit: 255, default: "",    null: false
    t.text     "details",                         default: "",    null: false
    t.string   "dispatcher",          limit: 255, default: "",    null: false
    t.string   "ward",                limit: 255, default: "",    null: false
    t.string   "district",            limit: 255, default: "",    null: false
    t.text     "remarks",                         default: "",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "created_by",                      default: 1
    t.integer  "updated_by",                      default: 1
    t.string   "dispatcher_unit",     limit: 255, default: "",    null: false
    t.string   "received_by_unit",    limit: 255, default: "",    null: false
    t.string   "received_by_badge",   limit: 255, default: "",    null: false
    t.string   "dispatcher_badge",    limit: 255, default: "",    null: false
    t.string   "detective",           limit: 255, default: "",    null: false
    t.string   "detective_unit",      limit: 255, default: "",    null: false
    t.string   "detective_badge",     limit: 255, default: "",    null: false
    t.boolean  "legacy",                          default: false, null: false
    t.datetime "call_datetime"
    t.datetime "dispatch_datetime"
    t.datetime "arrival_datetime"
    t.datetime "concluded_datetime"
    t.string   "received_via",        limit: 255, default: "",    null: false
    t.string   "disposition",         limit: 255, default: "",    null: false
    t.string   "signal_code",         limit: 255, default: "",    null: false
    t.string   "signal_name",         limit: 255, default: "",    null: false
    t.string   "zone",                limit: 255, default: "",    null: false
    t.integer  "call_subjects_count",             default: 0,     null: false
  end

  add_index "calls", ["case_no"], name: "index_calls_on_case_no"
  add_index "calls", ["created_by"], name: "index_calls_on_created_by"
  add_index "calls", ["signal_code"], name: "index_calls_on_signal_code"
  add_index "calls", ["updated_by"], name: "index_calls_on_updated_by"

  create_table "case_notes", force: :cascade do |t|
    t.string   "case_no",    limit: 255, default: "", null: false
    t.text     "note",                   default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "created_by",             default: 1
    t.integer  "updated_by",             default: 1
  end

  add_index "case_notes", ["case_no"], name: "index_case_notes_on_case_no"
  add_index "case_notes", ["created_by"], name: "index_case_notes_on_created_by"
  add_index "case_notes", ["updated_by"], name: "index_case_notes_on_updated_by"

  create_table "cases", force: :cascade do |t|
    t.string   "assigned",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "charges", force: :cascade do |t|
    t.integer  "citation_id"
    t.string   "charge",      limit: 255, default: "", null: false
    t.integer  "count",                   default: 1,  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "warrant_id"
    t.string   "agency",      limit: 255, default: "", null: false
    t.integer  "arrest_id"
    t.string   "charge_type", limit: 255, default: "", null: false
  end

  add_index "charges", ["arrest_id"], name: "index_charges_on_arrest_id"
  add_index "charges", ["citation_id"], name: "index_charges_on_citation_id"
  add_index "charges", ["warrant_id"], name: "index_charges_on_warrant_id"

  create_table "citations", force: :cascade do |t|
    t.integer  "person_id",                                  null: false
    t.string   "officer",           limit: 255, default: "", null: false
    t.string   "officer_unit",      limit: 255, default: "", null: false
    t.string   "officer_badge",     limit: 255, default: "", null: false
    t.string   "ticket_no",         limit: 255, default: "", null: false
    t.date     "paid_date"
    t.date     "recalled_date"
    t.integer  "created_by",                    default: 1
    t.integer  "updated_by",                    default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "citation_datetime"
  end

  add_index "citations", ["created_by"], name: "index_citations_on_created_by"
  add_index "citations", ["person_id"], name: "index_citations_on_person_id"
  add_index "citations", ["updated_by"], name: "index_citations_on_updated_by"

  create_table "commissaries", force: :cascade do |t|
    t.integer  "person_id",                                                           null: false
    t.string   "officer",          limit: 255,                          default: "",  null: false
    t.string   "officer_unit",     limit: 255,                          default: "",  null: false
    t.decimal  "deposit",                      precision: 12, scale: 2, default: 0.0, null: false
    t.decimal  "withdraw",                     precision: 12, scale: 2, default: 0.0, null: false
    t.integer  "created_by",                                            default: 1
    t.integer  "updated_by",                                            default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "officer_badge",    limit: 255,                          default: "",  null: false
    t.datetime "report_datetime"
    t.string   "receipt_no",       limit: 255,                          default: "",  null: false
    t.string   "memo",             limit: 255,                          default: "",  null: false
    t.date     "transaction_date"
    t.integer  "jail_id",                                                             null: false
    t.string   "category",         limit: 255,                          default: "",  null: false
  end

  add_index "commissaries", ["created_by"], name: "index_commissaries_on_created_by"
  add_index "commissaries", ["jail_id"], name: "index_commissaries_on_jail_id"
  add_index "commissaries", ["person_id"], name: "index_commissaries_on_booking_id"
  add_index "commissaries", ["updated_by"], name: "index_commissaries_on_updated_by"

  create_table "commissary_items", force: :cascade do |t|
    t.string   "description", limit: 255,                          default: "",   null: false
    t.decimal  "price",                   precision: 12, scale: 2, default: 0.0,  null: false
    t.boolean  "active",                                           default: true, null: false
    t.integer  "created_by",                                       default: 1
    t.integer  "updated_by",                                       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "jail_id",                                                         null: false
  end

  add_index "commissary_items", ["created_by"], name: "index_commissary_items_on_created_by"
  add_index "commissary_items", ["jail_id"], name: "index_commissary_items_on_jail_id"
  add_index "commissary_items", ["updated_by"], name: "index_commissary_items_on_updated_by"

  create_table "contact_bondsman_companies", force: :cascade do |t|
    t.integer  "contact_id",  null: false
    t.integer  "bondsman_id"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "contact_bondsman_companies", ["bondsman_id"], name: "index_contact_bondsman_companies_on_bondsman_id"
  add_index "contact_bondsman_companies", ["company_id"], name: "index_contact_bondsman_companies_on_company_id"
  add_index "contact_bondsman_companies", ["contact_id"], name: "index_contact_bondsman_companies_on_contact_id"

  create_table "contacts", force: :cascade do |t|
    t.string   "title",           limit: 255, default: "",    null: false
    t.string   "unit",            limit: 255, default: "",    null: false
    t.string   "street",          limit: 255, default: "",    null: false
    t.string   "city",            limit: 255, default: "",    null: false
    t.string   "zip",             limit: 255, default: "",    null: false
    t.string   "pri_email",       limit: 255, default: "",    null: false
    t.string   "alt_email",       limit: 255, default: "",    null: false
    t.string   "web_site",        limit: 255, default: "",    null: false
    t.string   "notes",           limit: 255, default: "",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "officer",                     default: false, null: false
    t.string   "street2",         limit: 255, default: "",    null: false
    t.integer  "created_by",                  default: 1
    t.integer  "updated_by",                  default: 1
    t.string   "badge_no",        limit: 255, default: "",    null: false
    t.boolean  "active",                      default: false, null: false
    t.boolean  "detective",                   default: false, null: false
    t.boolean  "physician",                   default: false, null: false
    t.boolean  "pharmacy",                    default: false, null: false
    t.boolean  "legacy",                      default: false, null: false
    t.boolean  "business",                    default: false, null: false
    t.string   "sort_name",       limit: 255, default: "",    null: false
    t.integer  "agency_id"
    t.boolean  "printable",                   default: true,  null: false
    t.string   "state",           limit: 255, default: "",    null: false
    t.string   "phone1",          limit: 255, default: "",    null: false
    t.string   "phone2",          limit: 255, default: "",    null: false
    t.string   "phone3",          limit: 255, default: "",    null: false
    t.boolean  "phone1_unlisted",             default: false, null: false
    t.boolean  "phone2_unlisted",             default: false, null: false
    t.boolean  "phone3_unlisted",             default: false, null: false
    t.boolean  "bondsman",                    default: false, null: false
    t.text     "remarks",                     default: "",    null: false
    t.string   "phone1_type",     limit: 255, default: "",    null: false
    t.string   "phone2_type",     limit: 255, default: "",    null: false
    t.string   "phone3_type",     limit: 255, default: "",    null: false
    t.boolean  "judge",                       default: false, null: false
    t.boolean  "pawn_co",                     default: false, null: false
    t.boolean  "bonding_company",             default: false, null: false
  end

  add_index "contacts", ["agency_id"], name: "index_contacts_on_agency_id"
  add_index "contacts", ["created_by"], name: "index_contacts_on_created_by"
  add_index "contacts", ["sort_name"], name: "index_contacts_on_sort_name"
  add_index "contacts", ["updated_by"], name: "index_contacts_on_updated_by"

  create_table "court_costs", force: :cascade do |t|
    t.string   "name",       limit: 255,                          default: "",  null: false
    t.string   "category",   limit: 255,                          default: "",  null: false
    t.decimal  "amount",                 precision: 12, scale: 2, default: 0.0, null: false
    t.integer  "created_by",                                      default: 1
    t.integer  "updated_by",                                      default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "court_costs", ["created_by"], name: "index_court_costs_on_created_by"
  add_index "court_costs", ["updated_by"], name: "index_court_costs_on_updated_by"

  create_table "court_types", force: :cascade do |t|
    t.string   "name",       limit: 255, default: "", null: false
    t.integer  "created_by",             default: 1
    t.integer  "updated_by",             default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "court_types", ["created_by"], name: "index_court_types_on_created_by"
  add_index "court_types", ["updated_by"], name: "index_court_types_on_updated_by"

  create_table "courts", force: :cascade do |t|
    t.date     "court_date"
    t.date     "next_court_date"
    t.boolean  "bench_warrant_issued",               default: false, null: false
    t.boolean  "bond_forfeiture_issued",             default: false, null: false
    t.boolean  "is_upset_wo_refix",                  default: false, null: false
    t.boolean  "is_refixed",                         default: false, null: false
    t.string   "refix_reason",           limit: 255, default: "",    null: false
    t.boolean  "is_dismissed",                       default: false, null: false
    t.boolean  "is_overturned",                      default: false, null: false
    t.date     "overturned_date"
    t.integer  "created_by",                         default: 1
    t.integer  "updated_by",                         default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "psi",                                default: false, null: false
    t.boolean  "processed",                          default: false, null: false
    t.boolean  "fines_paid",                         default: false, null: false
    t.text     "notes",                              default: "",    null: false
    t.text     "remarks",                            default: "",    null: false
    t.string   "court_type",             limit: 255, default: "",    null: false
    t.string   "next_court_type",        limit: 255, default: "",    null: false
    t.string   "docket_type",            limit: 255, default: "",    null: false
    t.string   "next_docket_type",       limit: 255, default: "",    null: false
    t.string   "plea_type",              limit: 255, default: "",    null: false
    t.string   "judge",                  limit: 255, default: "",    null: false
    t.string   "next_judge",             limit: 255, default: "",    null: false
    t.boolean  "writ_of_attach",                     default: false, null: false
    t.boolean  "not_present",                        default: false, null: false
    t.integer  "docket_id",                                          null: false
  end

  add_index "courts", ["created_by"], name: "index_courts_on_created_by"
  add_index "courts", ["docket_id"], name: "index_courts_on_docket_id"
  add_index "courts", ["updated_by"], name: "index_courts_on_updated_by"

  create_table "da_charges", force: :cascade do |t|
    t.integer  "docket_id",                                 null: false
    t.integer  "charge_id"
    t.integer  "count",                     default: 1,     null: false
    t.string   "charge",        limit: 255, default: "",    null: false
    t.boolean  "dropped_by_da",             default: false, null: false
    t.boolean  "processed",                 default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "arrest_id"
    t.boolean  "info_only",                 default: false, null: false
    t.boolean  "da_diversion",              default: false, null: false
  end

  add_index "da_charges", ["arrest_id"], name: "index_da_charges_on_arrest_id"
  add_index "da_charges", ["charge_id"], name: "index_da_charges_on_charge_id"
  add_index "da_charges", ["docket_id"], name: "index_da_charges_on_docket_id"

  create_table "damage_items", force: :cascade do |t|
    t.integer  "damage_id",                                                      null: false
    t.string   "description", limit: 255,                          default: "",  null: false
    t.decimal  "estimate",                precision: 12, scale: 2, default: 0.0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "damage_items", ["damage_id"], name: "index_damage_items_on_damage_id"

  create_table "damages", force: :cascade do |t|
    t.integer  "person_id"
    t.string   "case_no",         limit: 255, default: "", null: false
    t.string   "sort_name",       limit: 255, default: "", null: false
    t.string   "victim_address",  limit: 255, default: "", null: false
    t.string   "victim_phone",    limit: 255, default: "", null: false
    t.string   "crimes",          limit: 255, default: "", null: false
    t.datetime "damage_datetime"
    t.integer  "created_by",                  default: 1
    t.integer  "updated_by",                  default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "damages", ["created_by"], name: "index_damages_on_created_by"
  add_index "damages", ["person_id"], name: "index_damages_on_person_id"
  add_index "damages", ["sort_name"], name: "index_damages_on_sort_name"
  add_index "damages", ["updated_by"], name: "index_damages_on_updated_by"

  create_table "dispositions", force: :cascade do |t|
    t.string   "atn",         limit: 255, default: "", null: false
    t.integer  "person_id",                            null: false
    t.date     "report_date"
    t.text     "disposition",             default: "", null: false
    t.text     "remarks",                 default: "", null: false
    t.integer  "status",                  default: 0,  null: false
    t.integer  "created_by",              default: 1
    t.integer  "updated_by",              default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dispositions", ["atn"], name: "index_dispositions_on_atn"
  add_index "dispositions", ["created_by"], name: "index_dispositions_on_created_by"
  add_index "dispositions", ["person_id"], name: "index_dispositions_on_person_id"
  add_index "dispositions", ["updated_by"], name: "index_dispositions_on_updated_by"

  create_table "docket_types", force: :cascade do |t|
    t.string   "name",       limit: 255, default: "", null: false
    t.integer  "created_by",             default: 1
    t.integer  "updated_by",             default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "docket_types", ["created_by"], name: "index_docket_types_on_created_by"
  add_index "docket_types", ["updated_by"], name: "index_docket_types_on_updated_by"

  create_table "dockets", force: :cascade do |t|
    t.string   "docket_no",  limit: 255, default: "",    null: false
    t.integer  "person_id",                              null: false
    t.integer  "created_by",             default: 1
    t.integer  "updated_by",             default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "processed",              default: false, null: false
    t.text     "notes",                  default: "",    null: false
    t.text     "remarks",                default: "",    null: false
    t.boolean  "misc",                   default: false, null: false
  end

  add_index "dockets", ["created_by"], name: "index_dockets_on_created_by"
  add_index "dockets", ["docket_no"], name: "index_dockets_on_docket_no"
  add_index "dockets", ["person_id"], name: "index_dockets_on_person_id"
  add_index "dockets", ["updated_by"], name: "index_dockets_on_updated_by"

  create_table "docs", force: :cascade do |t|
    t.integer  "transfer_id"
    t.integer  "sentence_days",                 default: 0,     null: false
    t.integer  "sentence_months",               default: 0,     null: false
    t.integer  "sentence_years",                default: 0,     null: false
    t.integer  "to_serve_days",                 default: 0,     null: false
    t.integer  "to_serve_months",               default: 0,     null: false
    t.integer  "to_serve_years",                default: 0,     null: false
    t.string   "conviction",        limit: 255, default: "",    null: false
    t.boolean  "billable",                      default: false, null: false
    t.date     "bill_from_date"
    t.text     "remarks",                       default: "",    null: false
    t.integer  "created_by",                    default: 1
    t.integer  "updated_by",                    default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "hold_id"
    t.integer  "sentence_id"
    t.string   "doc_number",        limit: 255, default: "",    null: false
    t.integer  "booking_id",                                    null: false
    t.integer  "person_id",                                     null: false
    t.integer  "revocation_id"
    t.integer  "sentence_hours",                default: 0,     null: false
    t.integer  "to_serve_hours",                default: 0,     null: false
    t.string   "problems",          limit: 255, default: "",    null: false
    t.integer  "sentence_life",                 default: 0,     null: false
    t.boolean  "sentence_death",                default: false, null: false
    t.integer  "to_serve_life",                 default: 0,     null: false
    t.boolean  "to_serve_death",                default: false, null: false
    t.datetime "transfer_datetime"
  end

  add_index "docs", ["booking_id"], name: "index_docs_on_booking_id"
  add_index "docs", ["created_by"], name: "index_docs_on_created_by"
  add_index "docs", ["hold_id"], name: "index_docs_on_hold_id"
  add_index "docs", ["person_id"], name: "index_docs_on_person_id"
  add_index "docs", ["revocation_id"], name: "index_docs_on_revocation_id"
  add_index "docs", ["sentence_id"], name: "index_docs_on_sentence_id"
  add_index "docs", ["transfer_id"], name: "index_docs_on_transfer_id"
  add_index "docs", ["updated_by"], name: "index_docs_on_updated_by"

  create_table "events", force: :cascade do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.string   "name",             limit: 255, default: "",    null: false
    t.text     "details",                      default: "",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "created_by",                   default: 1
    t.integer  "updated_by",                   default: 1
    t.boolean  "private",                      default: false, null: false
    t.integer  "investigation_id"
    t.integer  "owned_by",                     default: 1
  end

  add_index "events", ["created_by"], name: "index_events_on_created_by"
  add_index "events", ["investigation_id"], name: "index_events_on_investigation_id"
  add_index "events", ["owned_by"], name: "index_events_on_owned_by"
  add_index "events", ["updated_by"], name: "index_events_on_updated_by"

  create_table "evidence_locations", force: :cascade do |t|
    t.integer  "evidence_id",                               null: false
    t.string   "location_from",    limit: 255, default: "", null: false
    t.string   "location_to",      limit: 255, default: "", null: false
    t.string   "officer",          limit: 255, default: "", null: false
    t.string   "remarks",          limit: 255, default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "officer_unit",     limit: 255, default: "", null: false
    t.string   "officer_badge",    limit: 255, default: "", null: false
    t.datetime "storage_datetime"
    t.integer  "officer_id"
  end

  add_index "evidence_locations", ["evidence_id"], name: "index_evidence_locations_on_evidence_id"
  add_index "evidence_locations", ["officer_id"], name: "index_evidence_locations_on_officer_id"

  create_table "evidences", force: :cascade do |t|
    t.string   "evidence_number",             limit: 255, default: "",    null: false
    t.string   "case_no",                     limit: 255, default: "",    null: false
    t.text     "description",                             default: "",    null: false
    t.integer  "owner_id"
    t.text     "remarks",                                 default: "",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "created_by",                              default: 1
    t.integer  "updated_by",                              default: 1
    t.string   "evidence_photo_file_name",    limit: 255
    t.string   "evidence_photo_content_type", limit: 255
    t.integer  "evidence_photo_file_size"
    t.datetime "evidence_photo_updated_at"
    t.boolean  "legacy",                                  default: false, null: false
    t.string   "dna_profile",                 limit: 255, default: "",    null: false
    t.string   "fingerprint_class",           limit: 255, default: "",    null: false
    t.integer  "evidence_locations_count",                default: 0,     null: false
    t.date     "disposed"
    t.string   "disposition",                 limit: 255, default: ""
  end

  add_index "evidences", ["case_no"], name: "index_evidences_on_case_no"
  add_index "evidences", ["created_by"], name: "index_evidences_on_created_by"
  add_index "evidences", ["disposed"], name: "index_evidences_on_disposed"
  add_index "evidences", ["evidence_number"], name: "index_evidences_on_evidence_number"
  add_index "evidences", ["owner_id"], name: "index_evidences_on_owner_id"
  add_index "evidences", ["updated_by"], name: "index_evidences_on_updated_by"

  create_table "external_links", force: :cascade do |t|
    t.string   "name",        limit: 255, default: "",    null: false
    t.string   "short_name",  limit: 255, default: "",    null: false
    t.string   "url",         limit: 255, default: "",    null: false
    t.text     "description",             default: "",    null: false
    t.boolean  "front_page",              default: false, null: false
    t.integer  "created_by",              default: 1
    t.integer  "updated_by",              default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "external_links", ["created_by"], name: "index_external_links_on_created_by"
  add_index "external_links", ["updated_by"], name: "index_external_links_on_updated_by"

  create_table "fax_covers", force: :cascade do |t|
    t.string   "name",           limit: 255, default: "",   null: false
    t.string   "from_name",      limit: 255, default: "",   null: false
    t.string   "from_fax",       limit: 255, default: "",   null: false
    t.string   "from_phone",     limit: 255, default: "",   null: false
    t.boolean  "include_notice",             default: true, null: false
    t.string   "notice_title",   limit: 255, default: "",   null: false
    t.text     "notice_body",                default: "",   null: false
    t.integer  "created_by",                 default: 1
    t.integer  "updated_by",                 default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "fax_covers", ["created_by"], name: "index_fax_covers_on_created_by"
  add_index "fax_covers", ["updated_by"], name: "index_fax_covers_on_updated_by"

  create_table "forbid_exemptions", force: :cascade do |t|
    t.integer  "forbid_id",  null: false
    t.integer  "person_id",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "forbid_exemptions", ["forbid_id"], name: "index_forbid_exemptions_on_forbid_id"
  add_index "forbid_exemptions", ["person_id"], name: "index_forbid_exemptions_on_person_id"

  create_table "forbid_possibles", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "forbid_id"
    t.integer  "person_id"
  end

  add_index "forbid_possibles", ["forbid_id"], name: "index_forbid_possibles_on_forbid_id"
  add_index "forbid_possibles", ["person_id"], name: "index_forbid_possibles_on_person_id"

  create_table "forbids", force: :cascade do |t|
    t.integer  "call_id"
    t.string   "case_no",                 limit: 255, default: "",    null: false
    t.integer  "forbidden_id"
    t.date     "request_date"
    t.string   "receiving_officer",       limit: 255, default: "",    null: false
    t.string   "receiving_officer_unit",  limit: 255, default: "",    null: false
    t.date     "signed_date"
    t.string   "serving_officer",         limit: 255, default: "",    null: false
    t.string   "serving_officer_unit",    limit: 255, default: "",    null: false
    t.date     "recall_date"
    t.text     "details",                             default: "",    null: false
    t.text     "remarks",                             default: "",    null: false
    t.integer  "created_by",                          default: 1
    t.integer  "updated_by",                          default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "receiving_officer_badge", limit: 255, default: "",    null: false
    t.string   "serving_officer_badge",   limit: 255, default: "",    null: false
    t.string   "complainant",             limit: 255, default: "",    null: false
    t.string   "comp_street",             limit: 255, default: "",    null: false
    t.string   "comp_city_state_zip",     limit: 255, default: "",    null: false
    t.string   "comp_phone",              limit: 255, default: "",    null: false
    t.string   "family_name",             limit: 255, default: "",    null: false
    t.string   "given_name",              limit: 255, default: "",    null: false
    t.string   "suffix",                  limit: 255, default: "",    null: false
    t.string   "forbid_street",           limit: 255, default: "",    null: false
    t.string   "forbid_city_state_zip",   limit: 255, default: "",    null: false
    t.string   "forbid_phone",            limit: 255, default: "",    null: false
    t.boolean  "legacy",                              default: false, null: false
  end

  add_index "forbids", ["call_id"], name: "index_forbids_on_call_id"
  add_index "forbids", ["case_no"], name: "index_forbids_on_case_no"
  add_index "forbids", ["created_by"], name: "index_forbids_on_created_by"
  add_index "forbids", ["family_name", "given_name"], name: "index_forbids_on_family_name_and_given_name"
  add_index "forbids", ["forbidden_id"], name: "index_forbids_on_forbidden_id"
  add_index "forbids", ["updated_by"], name: "index_forbids_on_updated_by"

  create_table "forum_categories", force: :cascade do |t|
    t.string   "name",       limit: 255, default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "forum_categories", ["name"], name: "index_forum_categories_on_name"

  create_table "forum_posts", force: :cascade do |t|
    t.integer  "forum_topic_id",                 null: false
    t.integer  "user_id",                        null: false
    t.integer  "reply_to_id"
    t.text     "text",           default: "",    null: false
    t.boolean  "notified",       default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "forum_posts", ["forum_topic_id"], name: "index_forum_posts_on_forum_topic_id"
  add_index "forum_posts", ["reply_to_id"], name: "index_forum_posts_on_reply_to_id"
  add_index "forum_posts", ["user_id"], name: "index_forum_posts_on_user_id"

  create_table "forum_subscriptions", force: :cascade do |t|
    t.integer  "subscriber_id",  null: false
    t.integer  "forum_topic_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "forum_subscriptions", ["forum_topic_id"], name: "index_forum_subscriptions_on_forum_topic_id"
  add_index "forum_subscriptions", ["subscriber_id"], name: "index_forum_subscriptions_on_subscriber_id"

  create_table "forum_topics", force: :cascade do |t|
    t.integer  "forum_id",                                 null: false
    t.integer  "user_id",                                  null: false
    t.string   "subject",      limit: 255, default: "",    null: false
    t.boolean  "locked",                   default: false, null: false
    t.boolean  "pinned",                   default: false, null: false
    t.boolean  "hidden",                   default: false, null: false
    t.datetime "last_post_at"
    t.integer  "views_count",              default: 0,     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "forum_topics", ["forum_id"], name: "index_forum_topics_on_forum_id"
  add_index "forum_topics", ["user_id"], name: "index_forum_topics_on_user_id"

  create_table "forum_views", force: :cascade do |t|
    t.integer  "user_id",                       null: false
    t.integer  "count",             default: 0, null: false
    t.datetime "current_viewed_at"
    t.datetime "past_viewed_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "forum_id"
    t.integer  "forum_topic_id"
  end

  add_index "forum_views", ["forum_id"], name: "index_forum_views_on_forum_id"
  add_index "forum_views", ["forum_topic_id"], name: "index_forum_views_on_forum_topic_id"
  add_index "forum_views", ["updated_at"], name: "index_forum_views_on_updated_at"
  add_index "forum_views", ["user_id"], name: "index_forum_views_on_user_id"

  create_table "forums", force: :cascade do |t|
    t.string   "name",              limit: 255, default: "", null: false
    t.text     "description",                   default: "", null: false
    t.integer  "forum_category_id"
    t.integer  "views_count",                   default: 0,  null: false
    t.integer  "created_by",                    default: 1
    t.integer  "updated_by",                    default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "forums", ["created_by"], name: "index_forums_on_created_by"
  add_index "forums", ["forum_category_id"], name: "index_forums_on_forum_category_id"
  add_index "forums", ["updated_by"], name: "index_forums_on_updated_by"

  create_table "garnishments", force: :cascade do |t|
    t.string   "sort_name",      limit: 255, default: "", null: false
    t.string   "suit_no",        limit: 255, default: "", null: false
    t.string   "description",    limit: 255, default: "", null: false
    t.string   "attorney",       limit: 255, default: "", null: false
    t.text     "details",                    default: "", null: false
    t.date     "received_date"
    t.date     "satisfied_date"
    t.text     "remarks",                    default: "", null: false
    t.integer  "created_by",                 default: 1
    t.integer  "updated_by",                 default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "garnishments", ["created_by"], name: "index_garnishments_on_created_by"
  add_index "garnishments", ["sort_name"], name: "index_garnishments_on_sort_name"
  add_index "garnishments", ["updated_by"], name: "index_garnishments_on_updated_by"

  create_table "groups", force: :cascade do |t|
    t.string   "name",        limit: 255, default: "", null: false
    t.string   "description", limit: 255, default: "", null: false
    t.integer  "created_by",              default: 1
    t.integer  "updated_by",              default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "permissions", limit: 255, default: "", null: false
  end

  add_index "groups", ["created_by"], name: "index_groups_on_created_by"
  add_index "groups", ["updated_by"], name: "index_groups_on_updated_by"

  create_table "histories", force: :cascade do |t|
    t.string   "key",        limit: 255, default: "", null: false
    t.string   "name",       limit: 255, default: "", null: false
    t.integer  "type_id",                default: 0,  null: false
    t.text     "details",                default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "histories", ["key"], name: "index_histories_on_key"
  add_index "histories", ["name"], name: "index_histories_on_name"
  add_index "histories", ["type_id"], name: "index_histories_on_type_id"

  create_table "hold_blockers", force: :cascade do |t|
    t.integer  "blocker_id", null: false
    t.integer  "hold_id",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "hold_blockers", ["blocker_id"], name: "index_hold_blockers_on_blocker_id"
  add_index "hold_blockers", ["hold_id"], name: "index_hold_blockers_on_hold_id"

  create_table "hold_bonds", force: :cascade do |t|
    t.integer  "bond_id",    null: false
    t.integer  "hold_id",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "hold_bonds", ["bond_id"], name: "index_hold_bonds_on_bond_id"
  add_index "hold_bonds", ["hold_id"], name: "index_hold_bonds_on_hold_id"

  create_table "holds", force: :cascade do |t|
    t.integer  "booking_id",                                                                  null: false
    t.string   "hold_by",                limit: 255,                          default: "",    null: false
    t.string   "hold_by_unit",           limit: 255,                          default: "",    null: false
    t.string   "cleared_by",             limit: 255,                          default: "",    null: false
    t.string   "cleared_by_unit",        limit: 255,                          default: "",    null: false
    t.text     "remarks",                                                     default: "",    null: false
    t.integer  "created_by",                                                  default: 1
    t.integer  "updated_by",                                                  default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "arrest_id"
    t.string   "hold_by_badge",          limit: 255,                          default: "",    null: false
    t.string   "cleared_by_badge",       limit: 255,                          default: "",    null: false
    t.boolean  "doc",                                                         default: false, null: false
    t.integer  "probation_id"
    t.date     "billing_report_date"
    t.string   "agency",                 limit: 255,                          default: "",    null: false
    t.string   "transfer_officer",       limit: 255,                          default: "",    null: false
    t.string   "transfer_officer_badge", limit: 255,                          default: "",    null: false
    t.string   "transfer_officer_unit",  limit: 255,                          default: "",    null: false
    t.decimal  "fines",                              precision: 12, scale: 2, default: 0.0,   null: false
    t.datetime "datetime_counted"
    t.boolean  "doc_billable",                                                default: false, null: false
    t.integer  "hours_served",                                                default: 0,     null: false
    t.float    "hours_goodtime",                                              default: 0.0,   null: false
    t.integer  "sentence_id"
    t.integer  "transfer_id"
    t.string   "explanation",            limit: 255,                          default: "",    null: false
    t.date     "bill_from_date"
    t.boolean  "legacy",                                                      default: false, null: false
    t.integer  "revocation_id"
    t.date     "billed_retroactive"
    t.integer  "hours_total",                                                 default: 0,     null: false
    t.integer  "hours_time_served",                                           default: 0,     null: false
    t.boolean  "deny_goodtime",                                               default: false, null: false
    t.boolean  "other_billable",                                              default: false, null: false
    t.boolean  "death_sentence",                                              default: false, null: false
    t.datetime "hold_datetime"
    t.datetime "cleared_datetime"
    t.string   "hold_type",              limit: 255,                          default: "",    null: false
    t.string   "cleared_because",        limit: 255,                          default: "",    null: false
    t.string   "temp_release_reason",    limit: 255,                          default: "",    null: false
  end

  add_index "holds", ["arrest_id"], name: "index_holds_on_arrest_id"
  add_index "holds", ["booking_id"], name: "index_holds_on_booking_id"
  add_index "holds", ["created_by"], name: "index_holds_on_created_by"
  add_index "holds", ["probation_id"], name: "index_holds_on_probation_id"
  add_index "holds", ["revocation_id"], name: "index_holds_on_revocation_id"
  add_index "holds", ["sentence_id"], name: "index_holds_on_sentence_id"
  add_index "holds", ["transfer_id"], name: "index_holds_on_transfer_id"
  add_index "holds", ["updated_by"], name: "index_holds_on_updated_by"

  create_table "invest_arrests", force: :cascade do |t|
    t.integer  "arrest_id",        null: false
    t.integer  "investigation_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_arrests", ["arrest_id"], name: "index_invest_arrests_on_arrest_id"
  add_index "invest_arrests", ["investigation_id"], name: "index_invest_arrests_on_investigation_id"

  create_table "invest_cases", force: :cascade do |t|
    t.integer  "investigation_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "call_id",          null: false
  end

  add_index "invest_cases", ["call_id"], name: "index_invest_cases_on_call_id"
  add_index "invest_cases", ["investigation_id"], name: "index_invest_cases_on_investigation_id"

  create_table "invest_contacts", force: :cascade do |t|
    t.integer  "investigation_id", null: false
    t.integer  "contact_id",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_contacts", ["contact_id"], name: "index_invest_contacts_on_contact_id"
  add_index "invest_contacts", ["investigation_id"], name: "index_invest_contacts_on_investigation_id"

  create_table "invest_crime_arrests", force: :cascade do |t|
    t.integer  "invest_crime_id",  null: false
    t.integer  "invest_arrest_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_crime_arrests", ["invest_arrest_id"], name: "index_invest_crime_arrests_on_invest_arrest_id"
  add_index "invest_crime_arrests", ["invest_crime_id"], name: "index_invest_crime_arrests_on_invest_crime_id"

  create_table "invest_crime_cases", force: :cascade do |t|
    t.integer  "invest_crime_id", null: false
    t.integer  "invest_case_id",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_crime_cases", ["invest_case_id"], name: "index_invest_crime_cases_on_invest_case_id"
  add_index "invest_crime_cases", ["invest_crime_id"], name: "index_invest_crime_cases_on_invest_crime_id"

  create_table "invest_crime_contacts", force: :cascade do |t|
    t.integer  "invest_crime_id",   null: false
    t.integer  "invest_contact_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_crime_contacts", ["invest_contact_id"], name: "index_invest_crime_contacts_on_invest_contact_id"
  add_index "invest_crime_contacts", ["invest_crime_id"], name: "index_invest_crime_contacts_on_invest_crime_id"

  create_table "invest_crime_criminals", force: :cascade do |t|
    t.integer  "invest_crime_id",    null: false
    t.integer  "invest_criminal_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_crime_criminals", ["invest_crime_id"], name: "index_invest_crime_criminals_on_invest_crime_id"
  add_index "invest_crime_criminals", ["invest_criminal_id"], name: "index_invest_crime_criminals_on_invest_criminal_id"

  create_table "invest_crime_evidences", force: :cascade do |t|
    t.integer  "invest_crime_id",    null: false
    t.integer  "invest_evidence_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_crime_evidences", ["invest_crime_id"], name: "index_invest_crime_evidences_on_invest_crime_id"
  add_index "invest_crime_evidences", ["invest_evidence_id"], name: "index_invest_crime_evidences_on_invest_evidence_id"

  create_table "invest_crime_offenses", force: :cascade do |t|
    t.integer  "invest_crime_id",   null: false
    t.integer  "invest_offense_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_crime_offenses", ["invest_crime_id"], name: "index_invest_crime_offenses_on_invest_crime_id"
  add_index "invest_crime_offenses", ["invest_offense_id"], name: "index_invest_crime_offenses_on_invest_offense_id"

  create_table "invest_crime_photos", force: :cascade do |t|
    t.integer  "invest_crime_id", null: false
    t.integer  "invest_photo_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_crime_photos", ["invest_crime_id"], name: "index_invest_crime_photos_on_invest_crime_id"
  add_index "invest_crime_photos", ["invest_photo_id"], name: "index_invest_crime_photos_on_invest_photo_id"

  create_table "invest_crime_stolens", force: :cascade do |t|
    t.integer  "invest_crime_id",  null: false
    t.integer  "invest_stolen_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_crime_stolens", ["invest_crime_id"], name: "index_invest_crime_stolens_on_invest_crime_id"
  add_index "invest_crime_stolens", ["invest_stolen_id"], name: "index_invest_crime_stolens_on_invest_stolen_id"

  create_table "invest_crime_vehicles", force: :cascade do |t|
    t.integer  "invest_crime_id",   null: false
    t.integer  "invest_vehicle_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_crime_vehicles", ["invest_crime_id"], name: "index_invest_crime_vehicles_on_invest_crime_id"
  add_index "invest_crime_vehicles", ["invest_vehicle_id"], name: "index_invest_crime_vehicles_on_invest_vehicle_id"

  create_table "invest_crime_victims", force: :cascade do |t|
    t.integer  "invest_crime_id",  null: false
    t.integer  "invest_victim_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_crime_victims", ["invest_crime_id"], name: "index_invest_crime_victims_on_invest_crime_id"
  add_index "invest_crime_victims", ["invest_victim_id"], name: "index_invest_crime_victims_on_invest_victim_id"

  create_table "invest_crime_warrants", force: :cascade do |t|
    t.integer  "invest_crime_id",   null: false
    t.integer  "invest_warrant_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_crime_warrants", ["invest_crime_id"], name: "index_invest_crime_warrants_on_invest_crime_id"
  add_index "invest_crime_warrants", ["invest_warrant_id"], name: "index_invest_crime_warrants_on_invest_warrant_id"

  create_table "invest_crime_witnesses", force: :cascade do |t|
    t.integer  "invest_crime_id",   null: false
    t.integer  "invest_witness_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_crime_witnesses", ["invest_crime_id"], name: "index_invest_crime_witnesses_on_invest_crime_id"
  add_index "invest_crime_witnesses", ["invest_witness_id"], name: "index_invest_crime_witnesses_on_invest_witness_id"

  create_table "invest_crimes", force: :cascade do |t|
    t.integer  "investigation_id",                                null: false
    t.boolean  "private",                         default: true,  null: false
    t.string   "motive",              limit: 255, default: "",    null: false
    t.string   "location",            limit: 255, default: "",    null: false
    t.boolean  "occupied",                        default: false, null: false
    t.string   "entry_gained",        limit: 255, default: "",    null: false
    t.string   "weapons_used",        limit: 255, default: "",    null: false
    t.text     "facts_prior",                     default: "",    null: false
    t.text     "facts_after",                     default: "",    null: false
    t.text     "targets",                         default: "",    null: false
    t.string   "security",            limit: 255, default: "",    null: false
    t.string   "impersonated",        limit: 255, default: "",    null: false
    t.text     "actions_taken",                   default: "",    null: false
    t.text     "threats_made",                    default: "",    null: false
    t.text     "details",                         default: "",    null: false
    t.text     "remarks",                         default: "",    null: false
    t.integer  "created_by",                      default: 1
    t.integer  "updated_by",                      default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "owned_by",                        default: 1
    t.datetime "occurred_datetime"
    t.string   "victim_relationship", limit: 255, default: "",    null: false
  end

  add_index "invest_crimes", ["created_by"], name: "index_invest_crimes_on_created_by"
  add_index "invest_crimes", ["investigation_id"], name: "index_invest_crimes_on_investigation_id"
  add_index "invest_crimes", ["owned_by"], name: "index_invest_crimes_on_owned_by"
  add_index "invest_crimes", ["updated_by"], name: "index_invest_crimes_on_updated_by"

  create_table "invest_criminal_arrests", force: :cascade do |t|
    t.integer  "invest_criminal_id", null: false
    t.integer  "invest_arrest_id",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_criminal_arrests", ["invest_arrest_id"], name: "index_invest_criminal_arrests_on_invest_arrest_id"
  add_index "invest_criminal_arrests", ["invest_criminal_id"], name: "index_invest_criminal_arrests_on_invest_criminal_id"

  create_table "invest_criminal_evidences", force: :cascade do |t|
    t.integer  "invest_criminal_id"
    t.integer  "invest_evidence_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_criminal_evidences", ["invest_criminal_id"], name: "index_invest_criminal_evidences_on_invest_criminal_id"
  add_index "invest_criminal_evidences", ["invest_evidence_id"], name: "index_invest_criminal_evidences_on_invest_evidence_id"

  create_table "invest_criminal_photos", force: :cascade do |t|
    t.integer  "invest_criminal_id", null: false
    t.integer  "invest_photo_id",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_criminal_photos", ["invest_criminal_id"], name: "index_invest_criminal_photos_on_invest_criminal_id"
  add_index "invest_criminal_photos", ["invest_photo_id"], name: "index_invest_criminal_photos_on_invest_photo_id"

  create_table "invest_criminal_stolens", force: :cascade do |t|
    t.integer  "invest_criminal_id", null: false
    t.integer  "invest_stolen_id",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_criminal_stolens", ["invest_criminal_id"], name: "index_invest_criminal_stolens_on_invest_criminal_id"
  add_index "invest_criminal_stolens", ["invest_stolen_id"], name: "index_invest_criminal_stolens_on_invest_stolen_id"

  create_table "invest_criminal_vehicles", force: :cascade do |t|
    t.integer  "invest_criminal_id", null: false
    t.integer  "invest_vehicle_id",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_criminal_vehicles", ["invest_criminal_id"], name: "index_invest_criminal_vehicles_on_invest_criminal_id"
  add_index "invest_criminal_vehicles", ["invest_vehicle_id"], name: "index_invest_criminal_vehicles_on_invest_vehicle_id"

  create_table "invest_criminal_warrants", force: :cascade do |t|
    t.integer  "invest_criminal_id", null: false
    t.integer  "invest_warrant_id",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_criminal_warrants", ["invest_criminal_id"], name: "index_invest_criminal_warrants_on_invest_criminal_id"
  add_index "invest_criminal_warrants", ["invest_warrant_id"], name: "index_invest_criminal_warrants_on_invest_warrant_id"

  create_table "invest_criminals", force: :cascade do |t|
    t.integer  "investigation_id",                             null: false
    t.integer  "person_id"
    t.string   "sort_name",         limit: 255, default: "",   null: false
    t.string   "description",       limit: 255, default: "",   null: false
    t.string   "height",            limit: 255, default: "",   null: false
    t.string   "weight",            limit: 255, default: "",   null: false
    t.string   "shoes",             limit: 255, default: "",   null: false
    t.string   "fingerprint_class", limit: 255, default: "",   null: false
    t.string   "dna_profile",       limit: 255, default: "",   null: false
    t.text     "remarks",                       default: "",   null: false
    t.integer  "created_by",                    default: 1
    t.integer  "updated_by",                    default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "private",                       default: true, null: false
    t.integer  "owned_by",                      default: 1
  end

  add_index "invest_criminals", ["created_by"], name: "index_invest_criminals_on_created_by"
  add_index "invest_criminals", ["investigation_id"], name: "index_invest_criminals_on_investigation_id"
  add_index "invest_criminals", ["owned_by"], name: "index_invest_criminals_on_owned_by"
  add_index "invest_criminals", ["person_id"], name: "index_invest_criminals_on_person_id"
  add_index "invest_criminals", ["sort_name"], name: "index_invest_criminals_on_sort_name"
  add_index "invest_criminals", ["updated_by"], name: "index_invest_criminals_on_updated_by"

  create_table "invest_evidences", force: :cascade do |t|
    t.integer  "evidence_id",      null: false
    t.integer  "investigation_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_evidences", ["evidence_id"], name: "index_invest_evidences_on_evidence_id"
  add_index "invest_evidences", ["investigation_id"], name: "index_invest_evidences_on_investigation_id"

  create_table "invest_notes", force: :cascade do |t|
    t.text     "note",               default: "", null: false
    t.integer  "created_by",         default: 1
    t.integer  "updated_by",         default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "investigation_id"
    t.integer  "invest_crime_id"
    t.integer  "invest_criminal_id"
    t.integer  "invest_photo_id"
    t.integer  "invest_report_id"
    t.integer  "invest_vehicle_id"
  end

  add_index "invest_notes", ["created_by"], name: "index_invest_notes_on_created_by"
  add_index "invest_notes", ["invest_crime_id"], name: "index_invest_notes_on_invest_crime_id"
  add_index "invest_notes", ["invest_criminal_id"], name: "index_invest_notes_on_invest_criminal_id"
  add_index "invest_notes", ["invest_photo_id"], name: "index_invest_notes_on_invest_photo_id"
  add_index "invest_notes", ["invest_report_id"], name: "index_invest_notes_on_invest_report_id"
  add_index "invest_notes", ["invest_vehicle_id"], name: "index_invest_notes_on_invest_vehicle_id"
  add_index "invest_notes", ["investigation_id"], name: "index_invest_notes_on_investigation_id"
  add_index "invest_notes", ["updated_by"], name: "index_invest_notes_on_updated_by"

  create_table "invest_offenses", force: :cascade do |t|
    t.integer  "investigation_id", null: false
    t.integer  "offense_id",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_offenses", ["investigation_id"], name: "index_invest_offenses_on_investigation_id"
  add_index "invest_offenses", ["offense_id"], name: "index_invest_offenses_on_offense_id"

  create_table "invest_photos", force: :cascade do |t|
    t.integer  "investigation_id",                                     null: false
    t.string   "invest_photo_file_name",    limit: 255
    t.string   "invest_photo_content_type", limit: 255
    t.integer  "invest_photo_file_size"
    t.datetime "invest_photo_updated_at"
    t.boolean  "private",                               default: true, null: false
    t.string   "description",               limit: 255, default: "",   null: false
    t.date     "date_taken"
    t.string   "location_taken",            limit: 255, default: "",   null: false
    t.string   "taken_by",                  limit: 255, default: "",   null: false
    t.string   "taken_by_unit",             limit: 255, default: "",   null: false
    t.string   "taken_by_badge",            limit: 255, default: "",   null: false
    t.text     "remarks",                               default: "",   null: false
    t.integer  "created_by",                            default: 1
    t.integer  "updated_by",                            default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "owned_by",                              default: 1
  end

  add_index "invest_photos", ["created_by"], name: "index_invest_photos_on_created_by"
  add_index "invest_photos", ["investigation_id"], name: "index_invest_photos_on_investigation_id"
  add_index "invest_photos", ["owned_by"], name: "index_invest_photos_on_owned_by"
  add_index "invest_photos", ["updated_by"], name: "index_invest_photos_on_updated_by"

  create_table "invest_report_arrests", force: :cascade do |t|
    t.integer  "invest_report_id", null: false
    t.integer  "invest_arrest_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_report_arrests", ["invest_arrest_id"], name: "index_invest_report_arrests_on_invest_arrest_id"
  add_index "invest_report_arrests", ["invest_report_id"], name: "index_invest_report_arrests_on_invest_report_id"

  create_table "invest_report_criminals", force: :cascade do |t|
    t.integer  "invest_report_id",   null: false
    t.integer  "invest_criminal_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_report_criminals", ["invest_criminal_id"], name: "index_invest_report_criminals_on_invest_criminal_id"
  add_index "invest_report_criminals", ["invest_report_id"], name: "index_invest_report_criminals_on_invest_report_id"

  create_table "invest_report_offenses", force: :cascade do |t|
    t.integer  "invest_report_id",  null: false
    t.integer  "invest_offense_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_report_offenses", ["invest_offense_id"], name: "index_invest_report_offenses_on_invest_offense_id"
  add_index "invest_report_offenses", ["invest_report_id"], name: "index_invest_report_offenses_on_invest_report_id"

  create_table "invest_report_photos", force: :cascade do |t|
    t.integer  "invest_report_id", null: false
    t.integer  "invest_photo_id",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_report_photos", ["invest_photo_id"], name: "index_invest_report_photos_on_invest_photo_id"
  add_index "invest_report_photos", ["invest_report_id"], name: "index_invest_report_photos_on_invest_report_id"

  create_table "invest_report_vehicles", force: :cascade do |t|
    t.integer  "invest_report_id",  null: false
    t.integer  "invest_vehicle_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_report_vehicles", ["invest_report_id"], name: "index_invest_report_vehicles_on_invest_report_id"
  add_index "invest_report_vehicles", ["invest_vehicle_id"], name: "index_invest_report_vehicles_on_invest_vehicle_id"

  create_table "invest_report_victims", force: :cascade do |t|
    t.integer  "invest_report_id", null: false
    t.integer  "invest_victim_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_report_victims", ["invest_report_id"], name: "index_invest_report_victims_on_invest_report_id"
  add_index "invest_report_victims", ["invest_victim_id"], name: "index_invest_report_victims_on_invest_victim_id"

  create_table "invest_report_witnesses", force: :cascade do |t|
    t.integer  "invest_report_id",  null: false
    t.integer  "invest_witness_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_report_witnesses", ["invest_report_id"], name: "index_invest_report_witnesses_on_invest_report_id"
  add_index "invest_report_witnesses", ["invest_witness_id"], name: "index_invest_report_witnesses_on_invest_witness_id"

  create_table "invest_reports", force: :cascade do |t|
    t.integer  "investigation_id",                 null: false
    t.text     "details",          default: "",    null: false
    t.text     "remarks",          default: "",    null: false
    t.boolean  "private",          default: true,  null: false
    t.integer  "created_by",       default: 1
    t.integer  "updated_by",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "owned_by",         default: 1
    t.datetime "report_datetime"
    t.boolean  "supplemental",     default: false, null: false
  end

  add_index "invest_reports", ["created_by"], name: "index_invest_reports_on_created_by"
  add_index "invest_reports", ["investigation_id"], name: "index_invest_reports_on_investigation_id"
  add_index "invest_reports", ["owned_by"], name: "index_invest_reports_on_owned_by"
  add_index "invest_reports", ["updated_by"], name: "index_invest_reports_on_updated_by"

  create_table "invest_sources", force: :cascade do |t|
    t.integer  "person_id",        null: false
    t.integer  "investigation_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_sources", ["investigation_id"], name: "index_invest_sources_on_investigation_id"
  add_index "invest_sources", ["person_id"], name: "index_invest_sources_on_person_id"

  create_table "invest_stolens", force: :cascade do |t|
    t.integer  "stolen_id",        null: false
    t.integer  "investigation_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_stolens", ["investigation_id"], name: "index_invest_stolens_on_investigation_id"
  add_index "invest_stolens", ["stolen_id"], name: "index_invest_stolens_on_stolen_id"

  create_table "invest_vehicle_photos", force: :cascade do |t|
    t.integer  "invest_vehicle_id", null: false
    t.integer  "invest_photo_id",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_vehicle_photos", ["invest_photo_id"], name: "index_invest_vehicle_photos_on_invest_photo_id"
  add_index "invest_vehicle_photos", ["invest_vehicle_id"], name: "index_invest_vehicle_photos_on_invest_vehicle_id"

  create_table "invest_vehicles", force: :cascade do |t|
    t.integer  "investigation_id",                            null: false
    t.boolean  "private",                      default: true, null: false
    t.integer  "owner_id"
    t.string   "make",             limit: 255, default: "",   null: false
    t.string   "model",            limit: 255, default: "",   null: false
    t.string   "vin",              limit: 255, default: "",   null: false
    t.string   "license",          limit: 255, default: "",   null: false
    t.string   "color",            limit: 255, default: "",   null: false
    t.string   "tires",            limit: 255, default: "",   null: false
    t.string   "model_year",       limit: 255, default: "",   null: false
    t.text     "description",                  default: "",   null: false
    t.text     "remarks",                      default: "",   null: false
    t.integer  "created_by",                   default: 1
    t.integer  "updated_by",                   default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "owned_by",                     default: 1
    t.string   "license_state",    limit: 255, default: "",   null: false
    t.string   "vehicle_type",     limit: 255, default: "",   null: false
  end

  add_index "invest_vehicles", ["created_by"], name: "index_invest_vehicles_on_created_by"
  add_index "invest_vehicles", ["investigation_id"], name: "index_invest_vehicles_on_investigation_id"
  add_index "invest_vehicles", ["owned_by"], name: "index_invest_vehicles_on_owned_by"
  add_index "invest_vehicles", ["owner_id"], name: "index_invest_vehicles_on_owner_id"
  add_index "invest_vehicles", ["updated_by"], name: "index_invest_vehicles_on_updated_by"

  create_table "invest_victims", force: :cascade do |t|
    t.integer  "person_id",        null: false
    t.integer  "investigation_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_victims", ["investigation_id"], name: "index_invest_victims_on_investigation_id"
  add_index "invest_victims", ["person_id"], name: "index_invest_victims_on_person_id"

  create_table "invest_warrants", force: :cascade do |t|
    t.integer  "investigation_id", null: false
    t.integer  "warrant_id",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_warrants", ["investigation_id"], name: "index_invest_warrants_on_investigation_id"
  add_index "invest_warrants", ["warrant_id"], name: "index_invest_warrants_on_warrant_id"

  create_table "invest_witnesses", force: :cascade do |t|
    t.integer  "person_id",        null: false
    t.integer  "investigation_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "invest_witnesses", ["investigation_id"], name: "index_invest_witnesses_on_investigation_id"
  add_index "invest_witnesses", ["person_id"], name: "index_invest_witnesses_on_person_id"

  create_table "investigations", force: :cascade do |t|
    t.boolean  "private",                            default: true, null: false
    t.text     "details",                            default: "",   null: false
    t.text     "remarks",                            default: "",   null: false
    t.integer  "created_by",                         default: 1
    t.integer  "updated_by",                         default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "owned_by",                           default: 1
    t.datetime "investigation_datetime"
    t.string   "status",                 limit: 255, default: "",   null: false
  end

  add_index "investigations", ["created_by"], name: "index_investigations_on_created_by"
  add_index "investigations", ["owned_by"], name: "index_investigations_on_owned_by"
  add_index "investigations", ["updated_by"], name: "index_investigations_on_updated_by"

  create_table "jail_absent_types", force: :cascade do |t|
    t.string   "name",        limit: 255, default: "", null: false
    t.string   "description", limit: 255, default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "jail_id",                              null: false
  end

  add_index "jail_absent_types", ["jail_id"], name: "index_jail_absent_types_on_jail_id"

  create_table "jail_cells", force: :cascade do |t|
    t.integer  "jail_id",                              null: false
    t.string   "name",        limit: 255, default: "", null: false
    t.string   "description", limit: 255, default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "jail_cells", ["jail_id"], name: "index_jail_cells_on_jail_id"

  create_table "jails", force: :cascade do |t|
    t.string   "name",                   limit: 255,                          default: "",    null: false
    t.string   "acronym",                limit: 255,                          default: "",    null: false
    t.string   "street",                 limit: 255,                          default: "",    null: false
    t.string   "city",                   limit: 255,                          default: "",    null: false
    t.string   "state",                  limit: 255,                          default: "",    null: false
    t.string   "zip",                    limit: 255,                          default: "",    null: false
    t.string   "phone",                  limit: 255,                          default: "",    null: false
    t.string   "fax",                    limit: 255,                          default: "",    null: false
    t.boolean  "juvenile",                                                    default: false, null: false
    t.boolean  "male",                                                        default: false, null: false
    t.boolean  "female",                                                      default: false, null: false
    t.integer  "permset",                                                     default: 1,     null: false
    t.boolean  "goodtime_by_default",                                         default: false, null: false
    t.decimal  "goodtime_hours_per_day",             precision: 12, scale: 2, default: 0.0,   null: false
    t.boolean  "afis_enabled",                                                default: false, null: false
    t.text     "remarks",                                                     default: "",    null: false
    t.integer  "created_by",                                                  default: 1
    t.integer  "updated_by",                                                  default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "notify_frequency",       limit: 255,                          default: "",    null: false
  end

  add_index "jails", ["created_by"], name: "index_jails_on_created_by"
  add_index "jails", ["updated_by"], name: "index_jails_on_updated_by"

  create_table "medicals", force: :cascade do |t|
    t.integer  "booking_id",                                  null: false
    t.string   "arrest_injuries", limit: 255, default: "",    null: false
    t.string   "officer",         limit: 255, default: "",    null: false
    t.string   "officer_unit",    limit: 255, default: "",    null: false
    t.string   "disposition",     limit: 255, default: "",    null: false
    t.string   "rec_med_tmnt",    limit: 255, default: "",    null: false
    t.string   "rec_dental_tmnt", limit: 255, default: "",    null: false
    t.string   "rec_mental_tmnt", limit: 255, default: "",    null: false
    t.string   "current_meds",    limit: 255, default: "",    null: false
    t.string   "allergies",       limit: 255, default: "",    null: false
    t.string   "injuries",        limit: 255, default: "",    null: false
    t.string   "infestation",     limit: 255, default: "",    null: false
    t.string   "alcohol",         limit: 255, default: "",    null: false
    t.string   "drug",            limit: 255, default: "",    null: false
    t.string   "withdrawal",      limit: 255, default: "",    null: false
    t.string   "attempt_suicide", limit: 255, default: "",    null: false
    t.string   "suicide_risk",    limit: 255, default: "",    null: false
    t.string   "danger",          limit: 255, default: "",    null: false
    t.string   "pregnant",        limit: 255, default: "",    null: false
    t.string   "deformities",     limit: 255, default: "",    null: false
    t.boolean  "heart_disease",               default: false, null: false
    t.boolean  "blood_pressure",              default: false, null: false
    t.boolean  "diabetes",                    default: false, null: false
    t.boolean  "epilepsy",                    default: false, null: false
    t.boolean  "hepatitis",                   default: false, null: false
    t.boolean  "hiv",                         default: false, null: false
    t.boolean  "tb",                          default: false, null: false
    t.boolean  "ulcers",                      default: false, null: false
    t.boolean  "venereal",                    default: false, null: false
    t.integer  "created_by",                  default: 1
    t.integer  "updated_by",                  default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "officer_badge",   limit: 255, default: "",    null: false
    t.datetime "screen_datetime"
  end

  add_index "medicals", ["booking_id"], name: "index_medicals_on_booking_id"
  add_index "medicals", ["created_by"], name: "index_medicals_on_created_by"
  add_index "medicals", ["updated_by"], name: "index_medicals_on_updated_by"

  create_table "medication_schedules", force: :cascade do |t|
    t.integer  "medication_id",                          null: false
    t.string   "dose",          limit: 255, default: "", null: false
    t.string   "time",          limit: 255, default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "medication_schedules", ["medication_id"], name: "index_med_schedules_on_medication_id"

  create_table "medications", force: :cascade do |t|
    t.integer  "person_id",                                null: false
    t.string   "prescription_no", limit: 255, default: "", null: false
    t.integer  "physician_id"
    t.integer  "pharmacy_id"
    t.string   "drug_name",       limit: 255, default: "", null: false
    t.string   "dosage",          limit: 255, default: "", null: false
    t.string   "warnings",        limit: 255, default: "", null: false
    t.integer  "created_by",                  default: 1
    t.integer  "updated_by",                  default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "medications", ["created_by"], name: "index_medications_on_created_by"
  add_index "medications", ["person_id"], name: "index_medications_on_person_id"
  add_index "medications", ["pharmacy_id"], name: "index_medications_on_pharmacy_id"
  add_index "medications", ["physician_id"], name: "index_medications_on_physician_id"
  add_index "medications", ["updated_by"], name: "index_medications_on_updated_by"

  create_table "message_logs", force: :cascade do |t|
    t.boolean  "receiver_deleted",             default: false, null: false
    t.boolean  "receiver_purged",              default: false, null: false
    t.boolean  "sender_deleted",               default: false, null: false
    t.boolean  "sender_purged",                default: false, null: false
    t.datetime "read_at"
    t.integer  "receiver_id",                                  null: false
    t.integer  "sender_id",                                    null: false
    t.string   "subject",          limit: 255, default: "",    null: false
    t.text     "body",                         default: "",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "message_logs", ["receiver_id"], name: "index_message_logs_on_receiver_id"
  add_index "message_logs", ["sender_id"], name: "index_message_logs_on_sender_id"

  create_table "messages", force: :cascade do |t|
    t.boolean  "receiver_deleted",             default: false, null: false
    t.boolean  "receiver_purged",              default: false, null: false
    t.boolean  "sender_deleted",               default: false, null: false
    t.boolean  "sender_purged",                default: false, null: false
    t.datetime "read_at"
    t.integer  "receiver_id",                                  null: false
    t.integer  "sender_id",                                    null: false
    t.string   "subject",          limit: 255, default: "",    null: false
    t.text     "body",                         default: "",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "messages", ["receiver_id"], name: "index_messages_on_receiver_id"
  add_index "messages", ["sender_id"], name: "index_messages_on_sender_id"

  create_table "offense_arrests", force: :cascade do |t|
    t.integer  "offense_id", null: false
    t.integer  "arrest_id",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "offense_arrests", ["arrest_id"], name: "index_offense_arrests_on_arrest_id"
  add_index "offense_arrests", ["offense_id"], name: "index_offense_arrests_on_offense_id"

  create_table "offense_citations", force: :cascade do |t|
    t.integer  "offense_id",  null: false
    t.integer  "citation_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "offense_citations", ["citation_id"], name: "index_offense_citations_on_citation_id"
  add_index "offense_citations", ["offense_id"], name: "index_offense_citations_on_offense_id"

  create_table "offense_evidences", force: :cascade do |t|
    t.integer  "offense_id",  null: false
    t.integer  "evidence_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "offense_evidences", ["evidence_id"], name: "index_offense_evidences_on_evidence_id"
  add_index "offense_evidences", ["offense_id"], name: "index_offense_evidences_on_offense_id"

  create_table "offense_forbids", force: :cascade do |t|
    t.integer  "forbid_id",  null: false
    t.integer  "offense_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "offense_forbids", ["forbid_id"], name: "index_offense_forbids_on_forbid_id"
  add_index "offense_forbids", ["offense_id"], name: "index_offense_forbids_on_offense_id"

  create_table "offense_photos", force: :cascade do |t|
    t.integer  "offense_id",                                              null: false
    t.string   "case_no",                        limit: 255, default: "", null: false
    t.string   "crime_scene_photo_file_name",    limit: 255
    t.string   "crime_scene_photo_content_type", limit: 255
    t.integer  "crime_scene_photo_file_size"
    t.datetime "crime_scene_photo_updated_at"
    t.string   "description",                    limit: 255, default: "", null: false
    t.date     "date_taken"
    t.string   "location_taken",                 limit: 255, default: "", null: false
    t.string   "taken_by",                       limit: 255, default: "", null: false
    t.string   "taken_by_unit",                  limit: 255, default: "", null: false
    t.text     "remarks",                                    default: "", null: false
    t.integer  "created_by",                                 default: 1
    t.integer  "updated_by",                                 default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "taken_by_badge",                 limit: 255, default: "", null: false
  end

  add_index "offense_photos", ["case_no"], name: "index_offense_photos_on_case_no"
  add_index "offense_photos", ["created_by"], name: "index_offense_photos_on_created_by"
  add_index "offense_photos", ["offense_id"], name: "index_offense_photos_on_offense_id"
  add_index "offense_photos", ["updated_by"], name: "index_offense_photos_on_updated_by"

  create_table "offense_suspects", force: :cascade do |t|
    t.integer  "offense_id", null: false
    t.integer  "person_id",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "offense_suspects", ["offense_id"], name: "index_offense_suspects_on_offense_id"
  add_index "offense_suspects", ["person_id"], name: "index_offense_suspects_on_person_id"

  create_table "offense_victims", force: :cascade do |t|
    t.integer  "offense_id", null: false
    t.integer  "person_id",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "offense_victims", ["offense_id"], name: "index_offense_victims_on_offense_id"
  add_index "offense_victims", ["person_id"], name: "index_offense_victims_on_person_id"

  create_table "offense_witnesses", force: :cascade do |t|
    t.integer  "offense_id", null: false
    t.integer  "person_id",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "offense_witnesses", ["offense_id"], name: "index_offense_witnesses_on_offense_id"
  add_index "offense_witnesses", ["person_id"], name: "index_offense_witnesses_on_person_id"

  create_table "offense_wreckers", force: :cascade do |t|
    t.integer  "offense_id",                             null: false
    t.string   "name",       limit: 255, default: "",    null: false
    t.boolean  "by_request",             default: false, null: false
    t.string   "location",   limit: 255, default: "",    null: false
    t.string   "vin",        limit: 255, default: "",    null: false
    t.string   "tag",        limit: 255, default: "",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "offense_wreckers", ["offense_id"], name: "index_wreckers_on_offense_id"

  create_table "offenses", force: :cascade do |t|
    t.integer  "call_id"
    t.string   "location",            limit: 255, default: "",    null: false
    t.text     "details",                         default: "",    null: false
    t.boolean  "pictures",                        default: false, null: false
    t.boolean  "restitution_form",                default: false, null: false
    t.boolean  "victim_notification",             default: false, null: false
    t.boolean  "perm_to_search",                  default: false, null: false
    t.boolean  "misd_summons",                    default: false, null: false
    t.boolean  "fortyeight_hour",                 default: false, null: false
    t.boolean  "medical_release",                 default: false, null: false
    t.text     "other_documents",                 default: "",    null: false
    t.string   "officer",             limit: 255, default: "",    null: false
    t.text     "remarks",                         default: "",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "created_by",                      default: 1
    t.integer  "updated_by",                      default: 1
    t.string   "officer_unit",        limit: 255, default: "",    null: false
    t.string   "officer_badge",       limit: 255, default: "",    null: false
    t.boolean  "legacy",                          default: false, null: false
    t.datetime "offense_datetime"
  end

  add_index "offenses", ["call_id"], name: "index_offenses_on_call_id"
  add_index "offenses", ["created_by"], name: "index_offenses_on_created_by"
  add_index "offenses", ["updated_by"], name: "index_offenses_on_updated_by"

  create_table "paper_customers", force: :cascade do |t|
    t.string   "name",          limit: 255,                          default: "",    null: false
    t.string   "street",        limit: 255,                          default: "",    null: false
    t.string   "city",          limit: 255,                          default: "",    null: false
    t.string   "zip",           limit: 255,                          default: "",    null: false
    t.string   "phone",         limit: 255,                          default: "",    null: false
    t.string   "attention",     limit: 255,                          default: "",    null: false
    t.text     "remarks",                                            default: "",    null: false
    t.boolean  "disabled",                                           default: false, null: false
    t.integer  "created_by",                                         default: 1
    t.integer  "updated_by",                                         default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "noservice_fee",             precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "mileage_fee",               precision: 12, scale: 2, default: 0.0,   null: false
    t.string   "fax",           limit: 255,                          default: "",    null: false
    t.string   "street2",       limit: 255,                          default: "",    null: false
    t.string   "state",         limit: 255
  end

  add_index "paper_customers", ["created_by"], name: "index_paper_customers_on_created_by"
  add_index "paper_customers", ["name"], name: "index_paper_customers_on_name"
  add_index "paper_customers", ["updated_by"], name: "index_paper_customers_on_updated_by"

  create_table "paper_payments", force: :cascade do |t|
    t.integer  "paper_id"
    t.integer  "paper_customer_id",                                                      null: false
    t.date     "transaction_date"
    t.string   "officer",           limit: 255,                          default: "",    null: false
    t.string   "officer_unit",      limit: 255,                          default: "",    null: false
    t.string   "officer_badge",     limit: 255,                          default: "",    null: false
    t.string   "receipt_no",        limit: 255,                          default: "",    null: false
    t.decimal  "payment",                       precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "charge",                        precision: 12, scale: 2, default: 0.0,   null: false
    t.datetime "report_datetime"
    t.string   "memo",              limit: 255,                          default: "",    null: false
    t.integer  "created_by",                                             default: 1
    t.integer  "updated_by",                                             default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "payment_method",    limit: 255,                          default: "",    null: false
    t.boolean  "paid_in_full",                                           default: false, null: false
    t.boolean  "adjustment",                                             default: false, null: false
  end

  add_index "paper_payments", ["created_by"], name: "index_paper_payments_on_created_by"
  add_index "paper_payments", ["paper_customer_id"], name: "index_paper_payments_on_paper_customer_id"
  add_index "paper_payments", ["paper_id"], name: "index_paper_payments_on_paper_id"
  add_index "paper_payments", ["updated_by"], name: "index_paper_payments_on_updated_by"

  create_table "paper_types", force: :cascade do |t|
    t.string   "name",       limit: 255,                          default: "",  null: false
    t.decimal  "cost",                   precision: 12, scale: 2, default: 0.0, null: false
    t.integer  "created_by",                                      default: 1
    t.integer  "updated_by",                                      default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "paper_types", ["created_by"], name: "index_paper_types_on_created_by"
  add_index "paper_types", ["updated_by"], name: "index_paper_types_on_updated_by"

  create_table "papers", force: :cascade do |t|
    t.string   "suit_no",           limit: 255,                          default: "",    null: false
    t.string   "plaintiff",         limit: 255,                          default: "",    null: false
    t.string   "defendant",         limit: 255,                          default: "",    null: false
    t.integer  "paper_customer_id"
    t.string   "serve_to",          limit: 255,                          default: "",    null: false
    t.string   "street",            limit: 255,                          default: "",    null: false
    t.string   "city",              limit: 255,                          default: "",    null: false
    t.string   "zip",               limit: 255,                          default: "",    null: false
    t.string   "phone",             limit: 255,                          default: "",    null: false
    t.date     "received_date"
    t.date     "returned_date"
    t.string   "noservice_extra",   limit: 255,                          default: "",    null: false
    t.string   "served_to",         limit: 255,                          default: "",    null: false
    t.string   "officer",           limit: 255,                          default: "",    null: false
    t.string   "officer_badge",     limit: 255,                          default: "",    null: false
    t.string   "officer_unit",      limit: 255,                          default: "",    null: false
    t.text     "comments",                                               default: "",    null: false
    t.boolean  "billable",                                               default: true,  null: false
    t.datetime "invoice_datetime"
    t.date     "paid_date"
    t.decimal  "invoice_total",                 precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "noservice_fee",                 precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "mileage_fee",                   precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "service_fee",                   precision: 12, scale: 2, default: 0.0,   null: false
    t.string   "memo",              limit: 255,                          default: "",    null: false
    t.text     "remarks",                                                default: "",    null: false
    t.integer  "created_by",                                             default: 1
    t.integer  "updated_by",                                             default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "court_date"
    t.decimal  "mileage",                       precision: 5,  scale: 1, default: 0.0,   null: false
    t.datetime "report_datetime"
    t.boolean  "mileage_only",                                           default: false, null: false
    t.string   "state",             limit: 255,                          default: "",    null: false
    t.datetime "served_datetime"
    t.string   "noservice_type",    limit: 255,                          default: "",    null: false
    t.string   "service_type",      limit: 255,                          default: "",    null: false
    t.boolean  "paid_in_full",                                           default: false, null: false
    t.string   "paper_type",        limit: 255,                          default: "",    null: false
  end

  add_index "papers", ["created_by"], name: "index_papers_on_created_by"
  add_index "papers", ["paper_customer_id"], name: "index_papers_on_paper_customer_id"
  add_index "papers", ["updated_by"], name: "index_papers_on_updated_by"

  create_table "patrols", force: :cascade do |t|
    t.string   "officer",                 limit: 255, default: "", null: false
    t.string   "officer_unit",            limit: 255, default: "", null: false
    t.string   "officer_badge",           limit: 255, default: "", null: false
    t.integer  "beginning_odometer",                  default: 0,  null: false
    t.integer  "ending_odometer",                     default: 0,  null: false
    t.integer  "shift",                               default: 0,  null: false
    t.string   "supervisor",              limit: 255, default: "", null: false
    t.string   "shift_start",             limit: 255, default: "", null: false
    t.string   "shift_stop",              limit: 255, default: "", null: false
    t.integer  "roads_patrolled",                     default: 0,  null: false
    t.integer  "complaints",                          default: 0,  null: false
    t.integer  "traffic_stops",                       default: 0,  null: false
    t.integer  "citations_issued",                    default: 0,  null: false
    t.integer  "papers_served",                       default: 0,  null: false
    t.integer  "narcotics_arrests",                   default: 0,  null: false
    t.integer  "other_arrests",                       default: 0,  null: false
    t.integer  "public_assists",                      default: 0,  null: false
    t.integer  "warrants_served",                     default: 0,  null: false
    t.integer  "funeral_escorts",                     default: 0,  null: false
    t.text     "remarks",                             default: "", null: false
    t.integer  "created_by",                          default: 1
    t.integer  "updated_by",                          default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "patrol_started_datetime"
    t.datetime "patrol_ended_datetime"
  end

  add_index "patrols", ["created_by"], name: "index_patrols_on_created_by"
  add_index "patrols", ["updated_by"], name: "index_patrols_on_updated_by"

  create_table "pawn_item_types", force: :cascade do |t|
    t.string   "item_type",  limit: 255, default: "", null: false
    t.integer  "created_by",             default: 1
    t.integer  "updated_by",             default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pawn_item_types", ["created_by"], name: "index_pawn_item_types_on_created_by"
  add_index "pawn_item_types", ["updated_by"], name: "index_pawn_item_types_on_updated_by"

  create_table "pawn_items", force: :cascade do |t|
    t.integer  "pawn_id",                              null: false
    t.string   "model_no",    limit: 255, default: "", null: false
    t.string   "serial_no",   limit: 255, default: "", null: false
    t.string   "description", limit: 255, default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "item_type",   limit: 255, default: "", null: false
  end

  add_index "pawn_items", ["pawn_id"], name: "index_pawn_items_on_pawn_id"

  create_table "pawns", force: :cascade do |t|
    t.string   "ticket_no",     limit: 255, default: "",    null: false
    t.date     "ticket_date"
    t.string   "case_no",       limit: 255, default: "",    null: false
    t.text     "remarks",                   default: "",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "created_by",                default: 1
    t.integer  "updated_by",                default: 1
    t.string   "sort_name",     limit: 255, default: "",    null: false
    t.string   "oln",           limit: 255, default: "",    null: false
    t.string   "address1",      limit: 255, default: "",    null: false
    t.string   "address2",      limit: 255, default: "",    null: false
    t.boolean  "legacy",                    default: false, null: false
    t.string   "oln_state",     limit: 255, default: "",    null: false
    t.string   "pawn_co",       limit: 255, default: "",    null: false
    t.string   "ticket_type",   limit: 255, default: "",    null: false
    t.string   "encrypted_ssn",             default: ""
  end

  add_index "pawns", ["case_no"], name: "index_pawns_on_case_no"
  add_index "pawns", ["created_by"], name: "index_pawns_on_created_by"
  add_index "pawns", ["sort_name"], name: "index_pawns_on_sort_name"
  add_index "pawns", ["updated_by"], name: "index_pawns_on_updated_by"

  create_table "people", force: :cascade do |t|
    t.string   "family_name",                limit: 255, default: "",    null: false
    t.string   "given_name",                 limit: 255, default: "",    null: false
    t.string   "suffix",                     limit: 255, default: "",    null: false
    t.string   "aka",                        limit: 255, default: "",    null: false
    t.integer  "is_alias_for"
    t.string   "phys_street",                limit: 255, default: "",    null: false
    t.string   "phys_city",                  limit: 255, default: "",    null: false
    t.string   "phys_zip",                   limit: 255, default: "",    null: false
    t.string   "mail_street",                limit: 255, default: "",    null: false
    t.string   "mail_city",                  limit: 255, default: "",    null: false
    t.string   "mail_zip",                   limit: 255, default: "",    null: false
    t.date     "date_of_birth"
    t.string   "place_of_birth",             limit: 255, default: "",    null: false
    t.integer  "sex",                                    default: 0,     null: false
    t.string   "fbi",                        limit: 255, default: "",    null: false
    t.string   "sid",                        limit: 255, default: "",    null: false
    t.string   "oln",                        limit: 255, default: "",    null: false
    t.string   "home_phone",                 limit: 255, default: "",    null: false
    t.string   "cell_phone",                 limit: 255, default: "",    null: false
    t.string   "email",                      limit: 255, default: "",    null: false
    t.integer  "height_ft",                              default: 0,     null: false
    t.integer  "height_in",                              default: 0,     null: false
    t.integer  "weight",                                 default: 0,     null: false
    t.boolean  "glasses",                                default: false, null: false
    t.string   "shoe_size",                  limit: 255, default: "",    null: false
    t.string   "occupation",                 limit: 255, default: "",    null: false
    t.string   "employer",                   limit: 255, default: "",    null: false
    t.string   "emergency_contact",          limit: 255, default: "",    null: false
    t.string   "emergency_address",          limit: 255, default: "",    null: false
    t.string   "emergency_phone",            limit: 255, default: "",    null: false
    t.string   "med_allergies",              limit: 255, default: "",    null: false
    t.boolean  "hepatitis",                              default: false, null: false
    t.boolean  "hiv",                                    default: false, null: false
    t.boolean  "tb",                                     default: false, null: false
    t.boolean  "deceased",                               default: false, null: false
    t.date     "dna_swab_date"
    t.string   "gang_affiliation",           limit: 255, default: "",    null: false
    t.text     "remarks",                                default: "",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "doc_number",                 limit: 255, default: "",    null: false
    t.integer  "created_by",                             default: 1
    t.integer  "updated_by",                             default: 1
    t.string   "front_mugshot_file_name",    limit: 255
    t.string   "side_mugshot_file_name",     limit: 255
    t.string   "front_mugshot_content_type", limit: 255
    t.string   "side_mugshot_content_type",  limit: 255
    t.integer  "front_mugshot_file_size"
    t.integer  "side_mugshot_file_size"
    t.datetime "front_mugshot_updated_at"
    t.datetime "side_mugshot_updated_at"
    t.boolean  "legacy",                                 default: false, null: false
    t.date     "dna_blood_date"
    t.string   "dna_profile",                limit: 255, default: "",    null: false
    t.string   "fingerprint_class",          limit: 255, default: "",    null: false
    t.string   "phys_state",                 limit: 255, default: "",    null: false
    t.string   "mail_state",                 limit: 255, default: "",    null: false
    t.string   "oln_state",                  limit: 255, default: "",    null: false
    t.string   "race",                       limit: 255, default: "",    null: false
    t.string   "build_type",                 limit: 255, default: "",    null: false
    t.string   "eye_color",                  limit: 255, default: "",    null: false
    t.string   "hair_color",                 limit: 255, default: "",    null: false
    t.string   "skin_tone",                  limit: 255, default: "",    null: false
    t.string   "complexion",                 limit: 255, default: "",    null: false
    t.string   "hair_type",                  limit: 255, default: "",    null: false
    t.string   "mustache",                   limit: 255, default: "",    null: false
    t.string   "beard",                      limit: 255, default: "",    null: false
    t.string   "eyebrow",                    limit: 255, default: "",    null: false
    t.string   "em_relationship",            limit: 255, default: "",    null: false
    t.boolean  "sex_offender",                           default: false, null: false
    t.string   "encrypted_ssn",                          default: ""
  end

  add_index "people", ["created_by"], name: "index_people_on_created_by"
  add_index "people", ["family_name", "given_name"], name: "index_people_on_family_name_and_given_name"
  add_index "people", ["is_alias_for"], name: "index_people_on_is_alias_for"
  add_index "people", ["updated_by"], name: "index_people_on_updated_by"

  create_table "person_associates", force: :cascade do |t|
    t.integer  "person_id",                             null: false
    t.string   "name",         limit: 255, default: "", null: false
    t.text     "remarks",                  default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "relationship", limit: 255, default: "", null: false
  end

  add_index "person_associates", ["person_id"], name: "index_associates_on_person_id"

  create_table "person_marks", force: :cascade do |t|
    t.integer  "person_id",                            null: false
    t.string   "description", limit: 255, default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "location",    limit: 255, default: "", null: false
  end

  add_index "person_marks", ["person_id"], name: "index_marks_on_person_id"

  create_table "probation_payments", force: :cascade do |t|
    t.integer  "probation_id",                                                        null: false
    t.date     "transaction_date"
    t.string   "officer",          limit: 255,                          default: "",  null: false
    t.string   "officer_unit",     limit: 255,                          default: "",  null: false
    t.string   "officer_badge",    limit: 255,                          default: "",  null: false
    t.integer  "created_by",                                            default: 1
    t.integer  "updated_by",                                            default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "receipt_no",       limit: 255,                          default: "",  null: false
    t.datetime "report_datetime"
    t.string   "memo",             limit: 255,                          default: "",  null: false
    t.string   "fund1_name",       limit: 255,                          default: "",  null: false
    t.decimal  "fund1_charge",                 precision: 12, scale: 2, default: 0.0, null: false
    t.decimal  "fund1_payment",                precision: 12, scale: 2, default: 0.0, null: false
    t.string   "fund2_name",       limit: 255,                          default: "",  null: false
    t.decimal  "fund2_charge",                 precision: 12, scale: 2, default: 0.0, null: false
    t.decimal  "fund2_payment",                precision: 12, scale: 2, default: 0.0, null: false
    t.string   "fund3_name",       limit: 255,                          default: "",  null: false
    t.decimal  "fund3_charge",                 precision: 12, scale: 2, default: 0.0, null: false
    t.decimal  "fund3_payment",                precision: 12, scale: 2, default: 0.0, null: false
    t.string   "fund4_name",       limit: 255,                          default: "",  null: false
    t.decimal  "fund4_charge",                 precision: 12, scale: 2, default: 0.0, null: false
    t.decimal  "fund4_payment",                precision: 12, scale: 2, default: 0.0, null: false
    t.string   "fund5_name",       limit: 255,                          default: "",  null: false
    t.decimal  "fund5_charge",                 precision: 12, scale: 2, default: 0.0, null: false
    t.decimal  "fund5_payment",                precision: 12, scale: 2, default: 0.0, null: false
    t.string   "fund6_name",       limit: 255,                          default: "",  null: false
    t.decimal  "fund6_charge",                 precision: 12, scale: 2, default: 0.0, null: false
    t.decimal  "fund6_payment",                precision: 12, scale: 2, default: 0.0, null: false
    t.string   "fund7_name",       limit: 255,                          default: "",  null: false
    t.decimal  "fund7_charge",                 precision: 12, scale: 2, default: 0.0, null: false
    t.decimal  "fund7_payment",                precision: 12, scale: 2, default: 0.0, null: false
    t.string   "fund8_name",       limit: 255,                          default: "",  null: false
    t.decimal  "fund8_charge",                 precision: 12, scale: 2, default: 0.0, null: false
    t.decimal  "fund8_payment",                precision: 12, scale: 2, default: 0.0, null: false
    t.string   "fund9_name",       limit: 255,                          default: "",  null: false
    t.decimal  "fund9_charge",                 precision: 12, scale: 2, default: 0.0, null: false
    t.decimal  "fund9_payment",                precision: 12, scale: 2, default: 0.0, null: false
  end

  add_index "probation_payments", ["created_by"], name: "index_probation_payments_on_created_by"
  add_index "probation_payments", ["probation_id"], name: "index_probation_payments_on_probation_id"
  add_index "probation_payments", ["report_datetime"], name: "index_probation_payments_on_report_datetime"
  add_index "probation_payments", ["updated_by"], name: "index_probation_payments_on_updated_by"

  create_table "probations", force: :cascade do |t|
    t.integer  "person_id",                                                                    null: false
    t.date     "start_date"
    t.date     "end_date"
    t.date     "completed_date"
    t.date     "revoked_date"
    t.string   "revoked_for",             limit: 255,                          default: "",    null: false
    t.boolean  "hold_if_arrested",                                             default: true,  null: false
    t.integer  "probation_days",                                               default: 0,     null: false
    t.integer  "probation_months",                                             default: 0,     null: false
    t.integer  "probation_years",                                              default: 0,     null: false
    t.decimal  "costs",                               precision: 12, scale: 2, default: 0.0,   null: false
    t.text     "notes",                                                        default: "",    null: false
    t.text     "remarks",                                                      default: "",    null: false
    t.integer  "created_by",                                                   default: 1
    t.integer  "updated_by",                                                   default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "probation_fees",                      precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "idf_amount",                          precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "dare_amount",                         precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "restitution_amount",                  precision: 12, scale: 2, default: 0.0,   null: false
    t.string   "probation_officer",       limit: 255,                          default: "",    null: false
    t.string   "probation_officer_unit",  limit: 255,                          default: "",    null: false
    t.string   "probation_officer_badge", limit: 255,                          default: "",    null: false
    t.decimal  "fines",                               precision: 12, scale: 2, default: 0.0,   null: false
    t.integer  "docket_id"
    t.integer  "court_id"
    t.integer  "da_charge_id"
    t.boolean  "doc",                                                          default: false, null: false
    t.boolean  "parish_jail",                                                  default: false, null: false
    t.string   "trial_result",            limit: 255,                          default: "",    null: false
    t.integer  "default_days",                                                 default: 0,     null: false
    t.integer  "default_months",                                               default: 0,     null: false
    t.integer  "default_years",                                                default: 0,     null: false
    t.date     "pay_by_date"
    t.integer  "sentence_days",                                                default: 0,     null: false
    t.integer  "sentence_months",                                              default: 0,     null: false
    t.integer  "sentence_years",                                               default: 0,     null: false
    t.boolean  "sentence_suspended",                                           default: false, null: false
    t.integer  "suspended_except_days",                                        default: 0,     null: false
    t.integer  "suspended_except_months",                                      default: 0,     null: false
    t.integer  "suspended_except_years",                                       default: 0,     null: false
    t.boolean  "reverts_to_unsup",                                             default: false, null: false
    t.string   "reverts_conditions",      limit: 255,                          default: "",    null: false
    t.boolean  "credit_served",                                                default: false, null: false
    t.integer  "service_days",                                                 default: 0,     null: false
    t.integer  "service_served",                                               default: 0,     null: false
    t.boolean  "sap",                                                          default: false, null: false
    t.boolean  "sap_complete",                                                 default: false, null: false
    t.boolean  "driver",                                                       default: false, null: false
    t.boolean  "driver_complete",                                              default: false, null: false
    t.boolean  "anger",                                                        default: false, null: false
    t.boolean  "anger_complete",                                               default: false, null: false
    t.boolean  "sat",                                                          default: false, null: false
    t.boolean  "sat_complete",                                                 default: false, null: false
    t.boolean  "random_screens",                                               default: false, null: false
    t.boolean  "no_victim_contact",                                            default: false, null: false
    t.boolean  "art893",                                                       default: false, null: false
    t.boolean  "art894",                                                       default: false, null: false
    t.boolean  "art895",                                                       default: false, null: false
    t.text     "sentence_notes",                                               default: "",    null: false
    t.string   "docket_no",               limit: 255,                          default: "",    null: false
    t.string   "conviction",              limit: 255,                          default: "",    null: false
    t.integer  "conviction_count",                                             default: 1,     null: false
    t.date     "restitution_date"
    t.integer  "probation_hours",                                              default: 0,     null: false
    t.integer  "default_hours",                                                default: 0,     null: false
    t.integer  "sentence_hours",                                               default: 0,     null: false
    t.integer  "suspended_except_hours",                                       default: 0,     null: false
    t.integer  "service_hours",                                                default: 0,     null: false
    t.string   "fund1_name",              limit: 255,                          default: "",    null: false
    t.string   "fund2_name",              limit: 255,                          default: "",    null: false
    t.string   "fund3_name",              limit: 255,                          default: "",    null: false
    t.string   "fund4_name",              limit: 255,                          default: "",    null: false
    t.string   "fund5_name",              limit: 255,                          default: "",    null: false
    t.string   "fund6_name",              limit: 255,                          default: "",    null: false
    t.string   "fund7_name",              limit: 255,                          default: "",    null: false
    t.string   "fund8_name",              limit: 255,                          default: "",    null: false
    t.string   "fund9_name",              limit: 255,                          default: "",    null: false
    t.decimal  "fund1_charged",                       precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund2_charged",                       precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund3_charged",                       precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund4_charged",                       precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund5_charged",                       precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund6_charged",                       precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund7_charged",                       precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund8_charged",                       precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund9_charged",                       precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund1_paid",                          precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund2_paid",                          precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund3_paid",                          precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund4_paid",                          precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund5_paid",                          precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund6_paid",                          precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund7_paid",                          precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund8_paid",                          precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "fund9_paid",                          precision: 12, scale: 2, default: 0.0,   null: false
    t.integer  "sentence_life",                                                default: 0,     null: false
    t.boolean  "sentence_death",                                               default: false, null: false
    t.integer  "suspended_except_life",                                        default: 0,     null: false
    t.string   "status",                  limit: 255,                          default: "",    null: false
    t.boolean  "pay_during_prob",                                              default: false, null: false
    t.string   "terminate_reason",        limit: 255,                          default: "",    null: false
  end

  add_index "probations", ["court_id"], name: "index_probations_on_court_id"
  add_index "probations", ["created_by"], name: "index_probations_on_created_by"
  add_index "probations", ["da_charge_id"], name: "index_probations_on_da_charge_id"
  add_index "probations", ["docket_id"], name: "index_probations_on_docket_id"
  add_index "probations", ["person_id"], name: "index_probations_on_person_id"
  add_index "probations", ["updated_by"], name: "index_probations_on_updated_by"

  create_table "revocation_consecutives", force: :cascade do |t|
    t.integer  "docket_id",     null: false
    t.integer  "revocation_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "revocation_consecutives", ["docket_id"], name: "index_revocation_consecutives_on_docket_id"
  add_index "revocation_consecutives", ["revocation_id"], name: "index_revocation_consecutives_on_revocation_id"

  create_table "revocations", force: :cascade do |t|
    t.date     "revocation_date"
    t.integer  "arrest_id"
    t.boolean  "revoked",                      default: false, null: false
    t.boolean  "time_served",                  default: false, null: false
    t.integer  "total_days",                   default: 0,     null: false
    t.integer  "total_months",                 default: 0,     null: false
    t.integer  "total_years",                  default: 0,     null: false
    t.boolean  "doc",                          default: false, null: false
    t.integer  "parish_days",                  default: 0,     null: false
    t.integer  "parish_months",                default: 0,     null: false
    t.integer  "parish_years",                 default: 0,     null: false
    t.boolean  "processed",                    default: false, null: false
    t.text     "remarks",                      default: "",    null: false
    t.integer  "created_by",                   default: 1
    t.integer  "updated_by",                   default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "process_status",   limit: 255, default: "",    null: false
    t.integer  "total_hours",                  default: 0,     null: false
    t.integer  "parish_hours",                 default: 0,     null: false
    t.boolean  "delay_execution",              default: false, null: false
    t.boolean  "deny_goodtime",                default: false, null: false
    t.integer  "total_life",                   default: 0,     null: false
    t.boolean  "total_death",                  default: false, null: false
    t.string   "consecutive_type", limit: 255, default: "",    null: false
    t.string   "judge",            limit: 255, default: "",    null: false
    t.integer  "docket_id",                                    null: false
  end

  add_index "revocations", ["arrest_id"], name: "index_revocations_on_arrest_id"
  add_index "revocations", ["created_by"], name: "index_revocations_on_created_by"
  add_index "revocations", ["docket_id"], name: "index_revocations_on_docket_id"
  add_index "revocations", ["updated_by"], name: "index_revocations_on_updated_by"

  create_table "sentence_consecutives", force: :cascade do |t|
    t.integer  "docket_id",   null: false
    t.integer  "sentence_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sentence_consecutives", ["docket_id"], name: "index_sentence_consecutives_on_docket_id"
  add_index "sentence_consecutives", ["sentence_id"], name: "index_sentence_consecutives_on_sentence_id"

  create_table "sentences", force: :cascade do |t|
    t.integer  "court_id",                                                                         null: false
    t.integer  "probation_id"
    t.integer  "da_charge_id",                                                                     null: false
    t.boolean  "doc",                                                              default: false, null: false
    t.decimal  "fines",                                   precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "costs",                                   precision: 12, scale: 2, default: 0.0,   null: false
    t.integer  "default_days",                                                     default: 0,     null: false
    t.integer  "default_months",                                                   default: 0,     null: false
    t.integer  "default_years",                                                    default: 0,     null: false
    t.date     "pay_by_date"
    t.integer  "sentence_days",                                                    default: 0,     null: false
    t.integer  "sentence_months",                                                  default: 0,     null: false
    t.integer  "sentence_years",                                                   default: 0,     null: false
    t.boolean  "suspended",                                                        default: false, null: false
    t.integer  "suspended_except_days",                                            default: 0,     null: false
    t.integer  "suspended_except_months",                                          default: 0,     null: false
    t.integer  "suspended_except_years",                                           default: 0,     null: false
    t.integer  "probation_days",                                                   default: 0,     null: false
    t.integer  "probation_months",                                                 default: 0,     null: false
    t.integer  "probation_years",                                                  default: 0,     null: false
    t.boolean  "credit_time_served",                                               default: false, null: false
    t.integer  "community_service_days",                                           default: 0,     null: false
    t.boolean  "substance_abuse_program",                                          default: false, null: false
    t.boolean  "driver_improvement",                                               default: false, null: false
    t.decimal  "probation_fees",                          precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "idf_amount",                              precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "dare_amount",                             precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "restitution_amount",                      precision: 12, scale: 2, default: 0.0,   null: false
    t.boolean  "anger_management",                                                 default: false, null: false
    t.boolean  "substance_abuse_treatment",                                        default: false, null: false
    t.boolean  "random_drug_screens",                                              default: false, null: false
    t.boolean  "no_victim_contact",                                                default: false, null: false
    t.boolean  "art_893",                                                          default: false, null: false
    t.boolean  "art_894",                                                          default: false, null: false
    t.boolean  "art_895",                                                          default: false, null: false
    t.boolean  "probation_reverts",                                                default: false, null: false
    t.string   "probation_revert_conditions", limit: 255,                          default: "",    null: false
    t.boolean  "wo_benefit",                                                       default: false, null: false
    t.boolean  "sentence_consecutive",                                             default: false, null: false
    t.string   "sentence_notes",              limit: 255,                          default: "",    null: false
    t.boolean  "fines_consecutive",                                                default: false, null: false
    t.string   "fines_notes",                 limit: 255,                          default: "",    null: false
    t.boolean  "costs_consecutive",                                                default: false, null: false
    t.string   "costs_notes",                 limit: 255,                          default: "",    null: false
    t.text     "notes",                                                            default: "",    null: false
    t.text     "remarks",                                                          default: "",    null: false
    t.boolean  "processed",                                                        default: false, null: false
    t.integer  "created_by",                                                       default: 1
    t.integer  "updated_by",                                                       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "parish_jail",                                                      default: false, null: false
    t.string   "conviction",                  limit: 255,                          default: "",    null: false
    t.integer  "conviction_count",                                                 default: 1,     null: false
    t.date     "restitution_date"
    t.boolean  "legacy",                                                           default: false, null: false
    t.string   "process_status",              limit: 255,                          default: "",    null: false
    t.integer  "default_hours",                                                    default: 0,     null: false
    t.integer  "sentence_hours",                                                   default: 0,     null: false
    t.integer  "suspended_except_hours",                                           default: 0,     null: false
    t.integer  "probation_hours",                                                  default: 0,     null: false
    t.boolean  "delay_execution",                                                  default: false, null: false
    t.integer  "community_service_hours",                                          default: 0,     null: false
    t.boolean  "deny_goodtime",                                                    default: false, null: false
    t.integer  "sentence_life",                                                    default: 0,     null: false
    t.boolean  "sentence_death",                                                   default: false, null: false
    t.integer  "suspended_except_life",                                            default: 0,     null: false
    t.string   "consecutive_type",            limit: 255,                          default: "",    null: false
    t.string   "probation_type",              limit: 255,                          default: "",    null: false
    t.string   "result",                      limit: 255,                          default: "",    null: false
    t.boolean  "pay_during_prob",                                                  default: false, null: false
  end

  add_index "sentences", ["court_id"], name: "index_sentences_on_court_id"
  add_index "sentences", ["created_by"], name: "index_sentences_on_created_by"
  add_index "sentences", ["da_charge_id"], name: "index_sentences_on_charge_id"
  add_index "sentences", ["probation_id"], name: "index_sentences_on_probation_id"
  add_index "sentences", ["updated_by"], name: "index_sentences_on_updated_by"

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", limit: 255, null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at"

  create_table "signal_codes", force: :cascade do |t|
    t.string   "code",       limit: 255, default: "", null: false
    t.string   "name",       limit: 255, default: "", null: false
    t.integer  "created_by",             default: 1
    t.integer  "updated_by",             default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "signal_codes", ["code"], name: "index_signal_codes_on_code"
  add_index "signal_codes", ["created_by"], name: "index_signal_codes_on_created_by"
  add_index "signal_codes", ["updated_by"], name: "index_signal_codes_on_updated_by"

  create_table "site_configurations", force: :cascade do |t|
    t.integer  "message_log_retention",                                           default: 90
    t.integer  "activity_retention",                                              default: 90
    t.integer  "session_life",                                                    default: 720
    t.integer  "session_active",                                                  default: 15
    t.integer  "session_max",                                                     default: 960
    t.string   "colorscheme",                limit: 255,                          default: "blue_brown"
    t.boolean  "show_history",                                                    default: false
    t.boolean  "allow_user_photos",                                               default: true
    t.boolean  "user_update_contact",                                             default: true
    t.decimal  "mileage_fee",                            precision: 12, scale: 2, default: 0.88
    t.decimal  "noservice_fee",                          precision: 12, scale: 2, default: 5.0
    t.string   "agency",                     limit: 255,                          default: "FiveO"
    t.string   "agency_acronym",             limit: 255,                          default: "FiveO"
    t.string   "motto",                      limit: 255,                          default: "Law Enforcement Software"
    t.string   "ceo",                        limit: 255,                          default: "Michael Merten"
    t.string   "ceo_title",                  limit: 255,                          default: "Midlake Technical Services"
    t.string   "local_zips",                 limit: 255
    t.string   "local_agencies",             limit: 255
    t.string   "site_email",                 limit: 255
    t.string   "header_title",               limit: 255,                          default: "FiveO"
    t.string   "header_subtitle1",           limit: 255,                          default: "Midlake Technical Services"
    t.string   "header_subtitle2",           limit: 255,                          default: "Michael Merten, Owner"
    t.string   "header_subtitle3",           limit: 255,                          default: "http://midlaketech.com - mike@midlaketech.com"
    t.string   "footer_text",                limit: 255,                          default: "Law Enforcement Software"
    t.string   "pdf_header_bg",              limit: 255,                          default: "ddddff"
    t.string   "pdf_header_txt",             limit: 255,                          default: "000099"
    t.string   "pdf_section_bg",             limit: 255,                          default: "ddddff"
    t.string   "pdf_section_txt",            limit: 255,                          default: "000099"
    t.string   "pdf_em_bg",                  limit: 255,                          default: "ffdddd"
    t.string   "pdf_em_txt",                 limit: 255,                          default: "990000"
    t.string   "pdf_row_odd",                limit: 255,                          default: "ffffff"
    t.string   "pdf_row_even",               limit: 255,                          default: "eeeeee"
    t.string   "dispatch_shift1_start",      limit: 255,                          default: "05:30"
    t.string   "dispatch_shift1_stop",       limit: 255,                          default: "17:30"
    t.string   "dispatch_shift2_start",      limit: 255,                          default: "17:30"
    t.string   "dispatch_shift2_stop",       limit: 255,                          default: "05:30"
    t.string   "dispatch_shift3_start",      limit: 255
    t.string   "dispatch_shift3_stop",       limit: 255
    t.string   "probation_fund1",            limit: 255,                          default: "Fines"
    t.string   "probation_fund2",            limit: 255,                          default: "Costs"
    t.string   "probation_fund3",            limit: 255,                          default: "Probation Fees"
    t.string   "probation_fund4",            limit: 255,                          default: "IDF"
    t.string   "probation_fund5",            limit: 255,                          default: "DARE"
    t.string   "probation_fund6",            limit: 255,                          default: "Restitution"
    t.string   "probation_fund7",            limit: 255,                          default: "DA Fees"
    t.string   "probation_fund8",            limit: 255
    t.string   "probation_fund9",            limit: 255
    t.string   "paper_pay_type1",            limit: 255,                          default: "Cash"
    t.string   "paper_pay_type2",            limit: 255,                          default: "Check"
    t.string   "paper_pay_type3",            limit: 255,                          default: "Money Order"
    t.string   "paper_pay_type4",            limit: 255,                          default: "Credit Card"
    t.string   "paper_pay_type5",            limit: 255
    t.string   "patrol_shift1_start",        limit: 255,                          default: "06:00"
    t.string   "patrol_shift1_stop",         limit: 255,                          default: "18:00"
    t.string   "patrol_shift2_start",        limit: 255,                          default: "18:00"
    t.string   "patrol_shift2_stop",         limit: 255,                          default: "06:00"
    t.string   "patrol_shift3_start",        limit: 255
    t.string   "patrol_shift3_stop",         limit: 255
    t.string   "patrol_shift1_supervisor",   limit: 255
    t.string   "patrol_shift2_supervisor",   limit: 255
    t.string   "patrol_shift3_supervisor",   limit: 255
    t.string   "patrol_shift4_start",        limit: 255
    t.string   "patrol_shift4_stop",         limit: 255
    t.string   "patrol_shift5_start",        limit: 255
    t.string   "patrol_shift5_stop",         limit: 255
    t.string   "patrol_shift6_start",        limit: 255
    t.string   "patrol_shift6_stop",         limit: 255
    t.string   "patrol_shift4_supervisor",   limit: 255
    t.string   "patrol_shift5_supervisor",   limit: 255
    t.string   "patrol_shift6_supervisor",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "pdf_heading_bg",             limit: 255,                          default: "dddddd"
    t.string   "pdf_heading_txt",            limit: 255,                          default: "000000"
    t.string   "afis_ori",                   limit: 255
    t.string   "afis_agency",                limit: 255
    t.string   "afis_street",                limit: 255
    t.string   "afis_city",                  limit: 255
    t.string   "afis_state",                 limit: 255
    t.string   "afis_zip",                   limit: 255
    t.string   "agency_street",              limit: 255
    t.string   "agency_city",                limit: 255
    t.string   "agency_state",               limit: 255
    t.string   "agency_zip",                 limit: 255
    t.boolean  "enable_internal_commissary",                                      default: false
    t.boolean  "enable_cashless_systems",                                         default: false
    t.string   "csi_ftp_user",               limit: 255
    t.string   "csi_ftp_password",           limit: 255
    t.string   "csi_ftp_url",                limit: 255
    t.string   "csi_ftp_filename",           limit: 255
    t.boolean  "csi_provide_ssn",                                                 default: false
    t.boolean  "csi_provide_dob",                                                 default: false
    t.boolean  "csi_provide_race",                                                default: false
    t.boolean  "csi_provide_sex",                                                 default: false
    t.boolean  "csi_provide_addr",                                                default: false
    t.text     "message_policy"
    t.integer  "message_retention",                                               default: 0
    t.string   "jurisdiction_title",         limit: 255,                          default: "Sabine Parish"
    t.string   "court_title",                limit: 255,                          default: "Eleventh Judicial District"
    t.string   "prosecutor_title",           limit: 255,                          default: "District Attorney"
    t.string   "chief_justice_title",        limit: 255,                          default: "Judge"
    t.string   "court_clerk_name",           limit: 255,                          default: "Michael Merten"
    t.string   "court_clerk_title",          limit: 255,                          default: "Owner"
    t.boolean  "recaptcha_enabled",                                               default: false
    t.string   "recaptcha_public_key",       limit: 255
    t.string   "recaptcha_private_key",      limit: 255
    t.string   "recaptcha_proxy",            limit: 255
    t.integer  "adult_age",                                                       default: 17
    t.string   "district_values",            limit: 255,                          default: "",                                              null: false
    t.string   "zone_values",                limit: 255,                          default: "",                                              null: false
    t.string   "ward_values",                limit: 255,                          default: "",                                              null: false
  end

  create_table "site_images", force: :cascade do |t|
    t.string   "logo_file_name",                limit: 255
    t.string   "logo_content_type",             limit: 255
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "ceo_img_file_name",             limit: 255
    t.string   "ceo_img_content_type",          limit: 255
    t.integer  "ceo_img_file_size"
    t.datetime "ceo_img_updated_at"
    t.string   "header_left_img_file_name",     limit: 255
    t.string   "header_left_img_content_type",  limit: 255
    t.integer  "header_left_img_file_size"
    t.datetime "header_left_img_updated_at"
    t.string   "header_right_img_file_name",    limit: 255
    t.string   "header_right_img_content_type", limit: 255
    t.integer  "header_right_img_file_size"
    t.datetime "header_right_img_updated_at"
  end

  create_table "site_stores", force: :cascade do |t|
    t.string   "setting_name",  limit: 255, null: false
    t.text     "setting_value",             null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "site_stores", ["setting_name"], name: "index_site_stores_on_setting_name"

  create_table "statutes", force: :cascade do |t|
    t.string   "name",        limit: 255, default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "created_by",              default: 1
    t.integer  "updated_by",              default: 1
    t.string   "common_name", limit: 255, default: "", null: false
    t.string   "code",        limit: 255, default: "", null: false
  end

  add_index "statutes", ["code"], name: "index_statutes_on_code"
  add_index "statutes", ["common_name"], name: "index_statutes_on_common_name"
  add_index "statutes", ["created_by"], name: "index_statutes_on_created_by"
  add_index "statutes", ["name"], name: "index_statutes_on_name"
  add_index "statutes", ["updated_by"], name: "index_statutes_on_updated_by"

  create_table "stolens", force: :cascade do |t|
    t.string   "case_no",          limit: 255,                          default: "",  null: false
    t.integer  "person_id",                                                           null: false
    t.date     "stolen_date"
    t.string   "model_no",         limit: 255,                          default: "",  null: false
    t.string   "serial_no",        limit: 255,                          default: "",  null: false
    t.decimal  "item_value",                   precision: 12, scale: 2, default: 0.0, null: false
    t.string   "item",             limit: 255,                          default: "",  null: false
    t.text     "item_description",                                      default: "",  null: false
    t.date     "recovered_date"
    t.date     "returned_date"
    t.text     "remarks",                                               default: "",  null: false
    t.integer  "created_by",                                            default: 1
    t.integer  "updated_by",                                            default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "stolens", ["case_no"], name: "index_stolens_on_case_no"
  add_index "stolens", ["created_by"], name: "index_stolens_on_created_by"
  add_index "stolens", ["person_id"], name: "index_stolens_on_person_id"
  add_index "stolens", ["updated_by"], name: "index_stolens_on_updated_by"

  create_table "transfers", force: :cascade do |t|
    t.integer  "booking_id",                                  null: false
    t.integer  "person_id",                                   null: false
    t.string   "from_agency",        limit: 255, default: "", null: false
    t.string   "from_officer",       limit: 255, default: "", null: false
    t.string   "from_officer_unit",  limit: 255, default: "", null: false
    t.string   "from_officer_badge", limit: 255, default: "", null: false
    t.string   "to_agency",          limit: 255, default: "", null: false
    t.string   "to_officer",         limit: 255, default: "", null: false
    t.string   "to_officer_unit",    limit: 255, default: "", null: false
    t.string   "to_officer_badge",   limit: 255, default: "", null: false
    t.text     "remarks",                        default: "", null: false
    t.datetime "reported_datetime"
    t.integer  "created_by",                     default: 1
    t.integer  "updated_by",                     default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "transfer_datetime"
    t.string   "transfer_type",      limit: 255, default: "", null: false
  end

  add_index "transfers", ["booking_id"], name: "index_transfers_on_booking_id"
  add_index "transfers", ["created_by"], name: "index_transfers_on_created_by"
  add_index "transfers", ["person_id"], name: "index_transfers_on_person_id"
  add_index "transfers", ["updated_by"], name: "index_transfers_on_updated_by"

  create_table "transports", force: :cascade do |t|
    t.integer  "person_id",                                null: false
    t.string   "officer1",        limit: 255, default: "", null: false
    t.string   "officer1_unit",   limit: 255, default: "", null: false
    t.string   "officer2",        limit: 255, default: "", null: false
    t.string   "officer2_unit",   limit: 255, default: "", null: false
    t.integer  "begin_miles",                 default: 0,  null: false
    t.integer  "end_miles",                   default: 0,  null: false
    t.integer  "estimated_miles",             default: 0,  null: false
    t.string   "from",            limit: 255, default: "", null: false
    t.string   "from_street",     limit: 255, default: "", null: false
    t.string   "from_city",       limit: 255, default: "", null: false
    t.string   "from_zip",        limit: 255, default: "", null: false
    t.string   "to",              limit: 255, default: "", null: false
    t.string   "to_street",       limit: 255, default: "", null: false
    t.string   "to_city",         limit: 255, default: "", null: false
    t.string   "to_zip",          limit: 255, default: "", null: false
    t.text     "remarks",                     default: "", null: false
    t.integer  "created_by",                  default: 1
    t.integer  "updated_by",                  default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "officer1_badge",  limit: 255, default: "", null: false
    t.string   "officer2_badge",  limit: 255, default: "", null: false
    t.string   "from_state",      limit: 255, default: "", null: false
    t.string   "to_state",        limit: 255, default: "", null: false
    t.datetime "begin_datetime"
    t.datetime "end_datetime"
  end

  add_index "transports", ["created_by"], name: "index_transports_on_created_by"
  add_index "transports", ["person_id"], name: "index_transports_on_person_id"
  add_index "transports", ["updated_by"], name: "index_transports_on_updated_by"

  create_table "user_groups", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.integer  "group_id",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_groups", ["group_id"], name: "index_user_groups_on_group_id"
  add_index "user_groups", ["user_id"], name: "index_user_groups_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "login",                   limit: 255, default: "",    null: false
    t.boolean  "locked",                              default: false, null: false
    t.string   "old_hashed_password",     limit: 255, default: "",    null: false
    t.string   "salt",                    limit: 255, default: "",    null: false
    t.integer  "items_per_page",                      default: 15,    null: false
    t.date     "expires"
    t.datetime "created_at"
    t.integer  "contact_id"
    t.integer  "created_by",                          default: 1
    t.integer  "updated_by",                          default: 1
    t.datetime "updated_at"
    t.string   "disabled_text",           limit: 255, default: "",    null: false
    t.string   "user_photo_file_name",    limit: 255
    t.string   "user_photo_content_type", limit: 255
    t.integer  "user_photo_file_size"
    t.datetime "user_photo_updated_at"
    t.boolean  "send_email",                          default: false, null: false
    t.boolean  "show_tips",                           default: true,  null: false
    t.boolean  "notify_booking_release",              default: false, null: false
    t.boolean  "notify_booking_new",                  default: false, null: false
    t.boolean  "notify_arrest_new",                   default: false, null: false
    t.boolean  "notify_call_new",                     default: false, null: false
    t.boolean  "notify_offense_new",                  default: false, null: false
    t.boolean  "notify_forbid_new",                   default: false, null: false
    t.boolean  "notify_forbid_recalled",              default: false, null: false
    t.boolean  "notify_pawn_new",                     default: false, null: false
    t.boolean  "notify_warrant_new",                  default: false, null: false
    t.boolean  "notify_warrant_resolved",             default: false, null: false
    t.integer  "message_retention",                   default: 0,     null: false
    t.boolean  "notify_booking_problems",             default: false, null: false
    t.boolean  "forum_auto_subscribe",                default: false, null: false
    t.string   "hashed_password",         limit: 255, default: "",    null: false
  end

  add_index "users", ["contact_id"], name: "index_users_on_contact_id"
  add_index "users", ["created_by"], name: "index_users_on_created_by"
  add_index "users", ["login"], name: "index_users_on_login"
  add_index "users", ["updated_by"], name: "index_users_on_updated_by"

  create_table "visitors", force: :cascade do |t|
    t.integer  "booking_id",                                null: false
    t.string   "name",          limit: 255, default: "",    null: false
    t.string   "street",        limit: 255, default: "",    null: false
    t.string   "city",          limit: 255, default: "",    null: false
    t.string   "zip",           limit: 255, default: "",    null: false
    t.string   "phone",         limit: 255, default: "",    null: false
    t.date     "visit_date"
    t.string   "sign_in_time",  limit: 255, default: "",    null: false
    t.string   "sign_out_time", limit: 255, default: "",    null: false
    t.text     "remarks",                   default: "",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "legacy",                    default: false, null: false
    t.string   "state",         limit: 255, default: "",    null: false
    t.string   "relationship",  limit: 255, default: "",    null: false
  end

  add_index "visitors", ["booking_id"], name: "index_visitors_on_booking_id"

  create_table "warrant_exemptions", force: :cascade do |t|
    t.integer  "warrant_id", null: false
    t.integer  "person_id",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "warrant_exemptions", ["person_id"], name: "index_warrant_exemptions_on_person_id"
  add_index "warrant_exemptions", ["warrant_id"], name: "index_warrant_exemptions_on_warrant_id"

  create_table "warrant_possibles", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "warrant_id"
    t.integer  "person_id"
  end

  add_index "warrant_possibles", ["person_id"], name: "index_warrant_possibles_on_person_id"
  add_index "warrant_possibles", ["warrant_id"], name: "index_warrant_possibles_on_warrant_id"

  create_table "warrants", force: :cascade do |t|
    t.integer  "person_id"
    t.string   "warrant_no",         limit: 255,                          default: "",    null: false
    t.date     "received_date"
    t.date     "issued_date"
    t.date     "dispo_date"
    t.text     "remarks",                                                 default: "",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "bond_amt",                       precision: 12, scale: 2, default: 0.0,   null: false
    t.decimal  "payable",                        precision: 12, scale: 2, default: 0.0,   null: false
    t.integer  "created_by",                                              default: 1
    t.integer  "updated_by",                                              default: 1
    t.date     "dob"
    t.string   "street",             limit: 255,                          default: "",    null: false
    t.string   "oln",                limit: 255,                          default: "",    null: false
    t.string   "given_name",         limit: 255,                          default: "",    null: false
    t.string   "family_name",        limit: 255,                          default: "",    null: false
    t.string   "suffix",             limit: 255,                          default: "",    null: false
    t.integer  "sex",                                                     default: 0,     null: false
    t.integer  "special_type",                                            default: 0,     null: false
    t.string   "city",               limit: 255,                          default: "",    null: false
    t.string   "zip",                limit: 255,                          default: "",    null: false
    t.boolean  "legacy",                                                  default: false, null: false
    t.string   "state",              limit: 255,                          default: "",    null: false
    t.string   "oln_state",          limit: 255,                          default: "",    null: false
    t.string   "jurisdiction",       limit: 255,                          default: "",    null: false
    t.string   "phone1",             limit: 255,                          default: "",    null: false
    t.string   "phone2",             limit: 255,                          default: "",    null: false
    t.string   "phone3",             limit: 255,                          default: "",    null: false
    t.boolean  "phone1_unlisted",                                         default: false, null: false
    t.boolean  "phone2_unlisted",                                         default: false, null: false
    t.boolean  "phone3_unlisted",                                         default: false, null: false
    t.string   "race",               limit: 255,                          default: "",    null: false
    t.string   "phone1_type",        limit: 255,                          default: "",    null: false
    t.string   "phone2_type",        limit: 255,                          default: "",    null: false
    t.string   "phone3_type",        limit: 255,                          default: "",    null: false
    t.string   "disposition",        limit: 255,                          default: "",    null: false
    t.string   "charge_type",        limit: 255,                          default: "",    null: false
    t.integer  "arrest_id"
    t.string   "photo_file_name",    limit: 255
    t.string   "photo_content_type", limit: 255
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "encrypted_ssn",                                           default: ""
  end

  add_index "warrants", ["arrest_id"], name: "index_warrants_on_arrest_id"
  add_index "warrants", ["created_by"], name: "index_warrants_on_created_by"
  add_index "warrants", ["family_name", "given_name"], name: "index_warrants_on_family_name_and_given_name"
  add_index "warrants", ["person_id"], name: "index_warrants_on_person_id"
  add_index "warrants", ["updated_by"], name: "index_warrants_on_updated_by"
  add_index "warrants", ["warrant_no"], name: "index_warrants_on_warrant_no"

end
